package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminArchiveResponse;
import com.techedge.mp.core.business.interfaces.ArchiveTransactionResult;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailHistoryBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherHistoryBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionEventHistoryBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.TransactionStatusHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventHistoryBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminTransactionArchiveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @EJB
    private LoggerService loggerService;

    public AdminTransactionArchiveAction() {}

    public AdminArchiveResponse execute(String adminTicketId, String requestId, long time, String transactionID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        AdminArchiveResponse adminArchiveResponse = new AdminArchiveResponse();

        try {
            userTransaction.begin();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                adminArchiveResponse.setStatusCode(ResponseHelper.ADMIN_ARCHIVE_INVALID_TICKET);
                return adminArchiveResponse;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                adminArchiveResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return adminArchiveResponse;
            }

            Date now = new Date();
            Date finalDay = new Date(now.getTime() - time);

            //Prepaid
            List<TransactionBean> transactionBeanList = new ArrayList<TransactionBean>();

            //PostPaid
            List<PostPaidTransactionBean> postPaidTransactionBeanList = new ArrayList<PostPaidTransactionBean>();

            //Oggetto ID - TIPO - STATUS
            List<ArchiveTransactionResult> listArchiveTransactionResults = new ArrayList<ArchiveTransactionResult>();

            if (transactionID == null) {
                System.out.println("Archiviazione transazioni pi� vecchie del: " + finalDay);

                //PrePaid
                List<TransactionBean> transactionBeanList1 = QueryRepository.findTransactionsByFinalStatusTypeAndNotificationDate(em_crud,
                        StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL, true, finalDay);
                List<TransactionBean> transactionBeanList2 = QueryRepository.findTransactionsByFinalStatusTypeDate(em_crud, StatusHelper.FINAL_STATUS_TYPE_FAILED, finalDay);

                //PostPaid
                List<PostPaidTransactionBean> postPaidTransactionBeanList1 = QueryRepository.findPostPaidTransactionToHistory(em_crud, finalDay);
                //List<PostPaidTransactionBean> postPaidTransactionBeanList1 = QueryRepository.findPostPaidTransactionToHistoryByStatusType(em_crud); //findPostPaidTransactionBeanByMPId(em_crud, "OTk2NzhCQ0JDRDAzMzUwNkRFQjE3N0U4");

                //Aggiungo gli elementi alle liste
                if (transactionBeanList1 != null) {
                    transactionBeanList.addAll(transactionBeanList1);
                }
                if (transactionBeanList2 != null) {
                    transactionBeanList.addAll(transactionBeanList2);
                }
                if (postPaidTransactionBeanList1 != null) {
                    postPaidTransactionBeanList.addAll(postPaidTransactionBeanList1);
                }
            }
            else {
                System.out.println("Archiviazione transazione con transactionID: " + transactionID);

                TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em_crud, transactionID);

                if (transactionBean != null) {
                    transactionBeanList.add(transactionBean);
                }
                else {
                    PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em_crud, transactionID);
                    if (postPaidTransactionBean != null) {
                        postPaidTransactionBeanList.add(postPaidTransactionBean);
                    }
                }
            }

            // Per ogni transazione prepaid estratta
            for (TransactionBean transactionBean : transactionBeanList) {

                TransactionHistoryBean transactionHistoryBean = new TransactionHistoryBean(transactionBean);
                transactionHistoryBean.setArchivingDate(new Date());
                em_crud.persist(transactionHistoryBean);
                boolean taskStatus = false;
                boolean taskEvent = false;
                boolean taskVoucher = false;

                System.out.println("Creazione ed inserimento transazione Pre-Paid da archiviare: " + transactionBean.getTransactionID());

                for (TransactionStatusHistoryBean transactionStatusHistoryBean : transactionHistoryBean.getTransactionStatusHistoryBeanData()) {
                    em_crud.persist(transactionStatusHistoryBean);
                    taskStatus = true;
                }

                if (taskStatus) {
                    System.out.println("Inserimento di eventuali status da archiviare collegati alla transazione Pre-Paid");
                }

                for (TransactionEventHistoryBean transactionEventHistoryBean : transactionHistoryBean.getTransactionEventHistoryBeanData()) {
                    em_crud.persist(transactionEventHistoryBean);
                    taskEvent = true;
                }

                if (taskEvent) {
                    System.out.println("Inserimento di eventuali eventi da archiviare collegati alla transazione Pre-Paid");
                }

                for (PrePaidConsumeVoucherHistoryBean prePaidConsumeVoucherHistoryBean : transactionHistoryBean.getPrePaidConsumeVoucherHistoryBeanList()) {
                    em_crud.persist(prePaidConsumeVoucherHistoryBean);
                    for (PrePaidConsumeVoucherDetailHistoryBean prePaidConsumeVoucherDetail : prePaidConsumeVoucherHistoryBean.getPrePaidConsumeVoucherDetailHistoryBean()) {
                        prePaidConsumeVoucherDetail.setPrePaidConsumeVoucherHistoryBean(prePaidConsumeVoucherHistoryBean);
                        em_crud.persist(prePaidConsumeVoucherDetail);
                    }
                    taskVoucher = true;
                }

                if (taskVoucher) {
                    System.out.println("Inserimento di eventuali voucher consumati da archiviare collegati alla transazione Pre-Paid");
                }

                for (TransactionStatusBean transactionStatusBean : transactionBean.getTransactionStatusBeanData()) {
                    em_crud.remove(transactionStatusBean);
                }

                if (taskStatus) {
                    System.out.println("Rimozione di eventuali status originari collegati alla transazione Pre-Paid");
                }

                for (TransactionEventBean transactionEventBean : transactionBean.getTransactionEventBeanData()) {
                    em_crud.remove(transactionEventBean);
                }

                if (taskEvent) {
                    System.out.println("Rimozione di eventuali eventi originari collegati alla transazione Pre-Paid");
                }

                for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {
                    for (PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetail : prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailBean()) {
                        em_crud.remove(prePaidConsumeVoucherDetail);
                    }
                    em_crud.remove(prePaidConsumeVoucherBean);
                }

                if (taskVoucher) {
                    System.out.println("Rimozione di eventuali voucher consumati originari collegati alla transazione Pre-Paid");
                }

                em_crud.remove(transactionBean);

                System.out.println("Rimozione transazione Pre-Paid originaria");

                ArchiveTransactionResult archiveTransactionResult = new ArchiveTransactionResult(transactionBean.getTransactionID(), "PrePaid Transaction",
                        ResponseHelper.ADMIN_ARCHIVE_SUCCESS);

                listArchiveTransactionResults.add(archiveTransactionResult);

            }

            // Per ogni transazione PostPaid estratta
            for (PostPaidTransactionBean postPaidTransactionBean : postPaidTransactionBeanList) {

                boolean taskRefuel = false;
                boolean taskCart = false;
                boolean taskEvent = false;
                boolean taskEventPayment = false;
                boolean taskVoucher = false;
                boolean taskLoyalty = false;
                PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = new PostPaidTransactionHistoryBean(postPaidTransactionBean);
                postPaidTransactionHistoryBean.setArchivingDate(Calendar.getInstance().getTime());
                em_crud.persist(postPaidTransactionHistoryBean);
                System.out.println("Creazione ed inserimento transazione Post-Paid da archiviare: " + postPaidTransactionBean.getMpTransactionID());

                for (PostPaidRefuelHistoryBean postPaidRefuelHistoryBean : postPaidTransactionHistoryBean.getRefuelHistoryBean()) {
                    em_crud.persist(postPaidRefuelHistoryBean);
                    taskRefuel = true;
                }

                if (taskRefuel) {
                    System.out.println("Inserimento di eventuali dati refuel da archiviare collegati alla transazione Post-Paid");
                }

                for (PostPaidCartHistoryBean postPaidCartHistoryBean : postPaidTransactionHistoryBean.getCartHistoryBean()) {
                    em_crud.persist(postPaidCartHistoryBean);
                    taskCart = true;
                }

                if (taskCart) {
                    System.out.println("Inserimento di eventuali dati shop da archiviare collegati alla transazione Post-Paid");
                }

                for (PostPaidTransactionEventHistoryBean postPaidTransactionEventHistoryBean : postPaidTransactionHistoryBean.getPostPaidTransactionEventHistoryBean()) {
                    em_crud.persist(postPaidTransactionEventHistoryBean);
                    taskEvent = true;
                }

                if (taskEvent) {
                    System.out.println("Inserimento di eventuali eventi da archiviare collegati alla transazione Post-Paid");
                }

                for (PostPaidTransactionPaymentEventHistoryBean postPaidTransactionPaymentEventHistoryBean : postPaidTransactionHistoryBean.getPostPaidTransactionPaymentEventHistoryBean()) {
                    em_crud.persist(postPaidTransactionPaymentEventHistoryBean);
                    taskEventPayment = true;
                }

                if (taskEventPayment) {
                    System.out.println("Inserimento di eventuali stati di pagamento da archiviare collegati alla transazione Post-Paid");
                }

                for (PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean : postPaidTransactionHistoryBean.getPostPaidConsumeVoucherBeanList()) {
                    em_crud.persist(postPaidConsumeVoucherHistoryBean);
                    for (PostPaidConsumeVoucherDetailHistoryBean postPaidConsumeVoucherDetail : postPaidConsumeVoucherHistoryBean.getPostPaidConsumeVoucherDetailHistoryBean()) {
                        postPaidConsumeVoucherDetail.setPostPaidConsumeVoucherHistoryBean(postPaidConsumeVoucherHistoryBean);
                        em_crud.persist(postPaidConsumeVoucherDetail);
                    }
                    taskVoucher = true;
                }

                if (taskVoucher) {
                    System.out.println("Inserimento di eventuali voucher consumati da archiviare collegati alla transazione Post-Paid");
                }

                for (PostPaidLoadLoyaltyCreditsHistoryBean postPaidLoadLoyaltyCreditsHistoryBean : postPaidTransactionHistoryBean.getPostPaidLoadLoyaltyCreditsBeanList()) {
                    em_crud.persist(postPaidLoadLoyaltyCreditsHistoryBean);
                    taskLoyalty = true;
                }

                if (taskLoyalty) {
                    System.out.println("Inserimento di eventuali punti loyalty da archiviare collegati alla transazione Post-Paid");
                }

                for (PostPaidRefuelBean postPaidRefuelBean : postPaidTransactionBean.getRefuelBean()) {
                    em_crud.remove(postPaidRefuelBean);
                }

                if (taskRefuel) {
                    System.out.println("Rimozione di eventuali dati refuel originari collegati alla transazione Post-Paid");
                }

                for (PostPaidCartBean postPaidCartBean : postPaidTransactionBean.getCartBean()) {
                    em_crud.remove(postPaidCartBean);
                }

                if (taskCart) {
                    System.out.println("Rimozione di eventuali dati shop originari collegati alla transazione Post-Paid");
                }

                for (PostPaidTransactionEventBean postPaidTransactionEventBean : postPaidTransactionBean.getPostPaidTransactionEventBean()) {
                    em_crud.remove(postPaidTransactionEventBean);
                }

                if (taskEvent) {
                    System.out.println("Rimozione di eventuali eventi originari collegati alla transazione Post-Paid");
                }

                for (PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean : postPaidTransactionBean.getPostPaidTransactionPaymentEventBean()) {
                    em_crud.remove(postPaidTransactionPaymentEventBean);
                }

                if (taskEventPayment) {
                    System.out.println("Rimozione di eventuali stati di pagamento originari collegati alla transazione Post-Paid");
                }

                for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {
                    for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetail : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {
                        em_crud.remove(postPaidConsumeVoucherDetail);
                    }
                    em_crud.remove(postPaidConsumeVoucherBean);
                }

                if (taskVoucher) {
                    System.out.println("Rimozione di eventuali voucher consumati originari collegati alla transazione Post-Paid");
                }

                for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : postPaidTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {
                    em_crud.remove(postPaidLoadLoyaltyCreditsBean);
                }

                if (taskLoyalty) {
                    System.out.println("Rimozione di eventuali punti loyalty originari collegati alla transazione Post-Paid");
                }

                em_crud.remove(postPaidTransactionBean);

                System.out.println("Rimozione transazione Post-Paid originaria");

                ArchiveTransactionResult archiveTransactionResult = new ArchiveTransactionResult(postPaidTransactionBean.getMpTransactionID(), "PostPaid Transaction",
                        ResponseHelper.ADMIN_POPARCHIVE_SUCCESS);

                listArchiveTransactionResults.add(archiveTransactionResult);
            }

            userTransaction.commit();

            adminArchiveResponse.setArchiveTransactionResultList(listArchiveTransactionResults);
            adminArchiveResponse.setStatusCode(ResponseHelper.ADMIN_ARCHIVE_SUCCESS);

            return adminArchiveResponse;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED transaction archiving with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }
}
