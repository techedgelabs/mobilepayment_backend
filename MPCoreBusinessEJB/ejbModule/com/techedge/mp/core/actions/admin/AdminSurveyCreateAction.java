package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SurveyQuestion;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.SurveyBean;
import com.techedge.mp.core.business.model.SurveyQuestionBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminSurveyCreateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminSurveyCreateAction() {}

    public String execute(String adminTicketId, String requestId, String code, String description, String note, Integer status, Date startDate, Date endDate,
            ArrayList<SurveyQuestion> questions) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            SurveyBean surveyBean = QueryRepository.findSurveyByCode(em, code);

            if (surveyBean != null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Survey code " + code + " already exists");

                userTransaction.commit();

                return ResponseHelper.ADMIN_SURVEY_CREATE_INVALID_CODE;
            }

            surveyBean = new SurveyBean();
            surveyBean.setCode(code);
            surveyBean.setDescription(description);
            surveyBean.setNote(note);
            surveyBean.setStartDate(startDate);
            surveyBean.setEndDate(endDate);
            surveyBean.setStatus(status);

            em.persist(surveyBean);

            if (questions != null && !questions.isEmpty()) {
                for (SurveyQuestion question : questions) {
                    SurveyQuestionBean surveyQuestionBean = new SurveyQuestionBean();
                    surveyQuestionBean.setCode(question.getCode());
                    surveyQuestionBean.setSequence(question.getSequence());
                    surveyQuestionBean.setText(question.getText());
                    surveyQuestionBean.setType(question.getType());
                    surveyQuestionBean.setSurveyBean(surveyBean);
                    em.persist(surveyQuestionBean);
                }
            }

            userTransaction.commit();

            return ResponseHelper.ADMIN_SURVEY_CREATE_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
