package com.techedge.mp.core.actions.admin;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.util.Date;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.xml.bind.DatatypeConverter;

import sun.misc.BASE64Encoder;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.AdminRoleEnum;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PaymentMethodResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.ShopTransactionDataBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltySessionBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminRefuelingUserInsertMulticardAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserCategoryService userCategoryService;

    public AdminRefuelingUserInsertMulticardAction() {}

    public PaymentMethodResponse execute(String ticketId, String requestId, String username, Integer pinCheckMaxAttempts, Integer loyaltySessionExpiryTime, String plainKey, String notifyOkUrl,
            String notifyKoUrl, String depositCardMulticardBoUrl) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        
        PaymentMethodResponse paymentMethodResponse = new PaymentMethodResponse();
        
        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, ticketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                paymentMethodResponse.setStatusCode(ResponseHelper.ADMIN_INSERT_PAY_METH_REFUELING_USER_INVALID_TICKET);
                return paymentMethodResponse;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())
                    && !CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REFUELING_MANAGER)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                paymentMethodResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return paymentMethodResponse;
            }

            // Verifica l'utente esista
            UserBean userBean = QueryRepository.findNotCancelledUserCustomerByEmail(em, username);

            if (userBean == null) {

                // Non esiste nessun utente con l'email indicata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not exists");

                userTransaction.commit();

                paymentMethodResponse.setStatusCode(ResponseHelper.ADMIN_INSERT_PAY_METH_REFUELING_USER_USER_NOT_EXISTS);
                return paymentMethodResponse;
            }

            if (!userBean.getUserType().equals(User.USER_TYPE_REFUELING_NEW_ACQUIRER) && !userBean.getUserType().equals(User.USER_TYPE_REFUELING_OAUTH2)) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not refueling");

                userTransaction.commit();

                paymentMethodResponse.setStatusCode(ResponseHelper.ADMIN_INSERT_PAY_METH_REFUELING_USER_USER_NOT_REFUELING);
                return paymentMethodResponse;
            }

            // TODO: Verifica che l'utente non abbia gi� associato una multicard
            
            // Genera il token della sessione loyalty per l'utente refueling

            Date creationTimestamp = new Date();
            Date lastUsedTimestamp = creationTimestamp;
            Date expiryTimestamp = DateHelper.addMinutesToDate(loyaltySessionExpiryTime, creationTimestamp);
            
            // Per l'utente refueling il campo fiscalCode deve essere valorizzato con l'email
            String fiscalCode = userBean.getPersonalDataBean().getSecurityDataEmail();
            
            String sessionId = new IdGenerator().generateId(16).substring(0, 32);

            LoyaltySessionBean loyaltySessionBean = new LoyaltySessionBean(sessionId, userBean, creationTimestamp, lastUsedTimestamp, expiryTimestamp, fiscalCode);

            em.persist(loyaltySessionBean);

            
            String message = username + "||" + sessionId + "||" + notifyOkUrl + "||" + notifyKoUrl;
            //String message = "max.zaniratoreply@gmail.com||hsdfjhfsdfisufsiofusiodfusdoifusdio||www.teskok.com||testko.com";
            
            System.out.println("message: " + message);
            
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] byteKey = digest.digest(plainKey.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec secretKeySpec = new SecretKeySpec(byteKey, "AES");
            
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            
            byte[] hexEncodedCipher = cipher.doFinal(message.getBytes());
            BASE64Encoder encoder = new BASE64Encoder();
            String ciphertext = encoder.encode(hexEncodedCipher);
            
            System.out.println("ciphertext: " + ciphertext);
            
            if (ciphertext == null || ciphertext.isEmpty()) {

                // Errore nella codifica
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Error encrypt");

                userTransaction.commit();

                paymentMethodResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_FAILED);
                return paymentMethodResponse;
            }
            
            
            Boolean defaultMethod = true;
            if (!userBean.getPaymentData().isEmpty()) {
                for (PaymentInfoBean paymentInfoBeanTemp : userBean.getPaymentData()) {
                    if (paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                        if (paymentInfoBeanTemp.getDefaultMethod() == true) {
                            defaultMethod = false;
                            break;
                        }
                    }
                }
            }

            if (defaultMethod == true) {
                System.out.println("Non � stato trovato un metodo di pagamento di default");
            }
            
            // Genera uno shopTransactionID
            String shopTransactionId = new IdGenerator().generateId(16).substring(0, 33);
            
            PaymentInfoBean paymentInfoBean = userBean.addNewPendingPaymentInfo(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD, null, defaultMethod, pinCheckMaxAttempts, null, null, null, null, shopTransactionId);

            System.out.println("Salvataggio nuovo metodo di pagamento multicard");

            // Memorizza su db l'informazione sulla carta
            em.persist(paymentInfoBean);
            em.merge(userBean);

            // Registra su db l'associazione tra lo shopTransactionID e l'utente
            ShopTransactionDataBean shopTransactionData = ShopTransactionDataBean.createNewShopTransactionDataBean(shopTransactionId, paymentInfoBean);

            em.merge(shopTransactionData);

            userTransaction.commit();

            String encodedRedirectUrl = depositCardMulticardBoUrl + URLEncoder.encode(ciphertext, "UTF-8");

            paymentMethodResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_SUCCESS);
            paymentMethodResponse.setRedirectURL(encodedRedirectUrl);
            paymentMethodResponse.getPaymentMethod().setId(paymentInfoBean.getId());
            paymentMethodResponse.getPaymentMethod().setType(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);

            return paymentMethodResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED multicard insert enjoy user with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
