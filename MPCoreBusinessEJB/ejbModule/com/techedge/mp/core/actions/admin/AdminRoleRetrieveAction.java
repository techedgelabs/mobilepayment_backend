package com.techedge.mp.core.actions.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminRolesResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminRoleBean;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminRoleRetrieveAction {
    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;
    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    public AdminRoleRetrieveAction() {}

    public AdminRolesResponse execute(String adminTicketId, String requestID) throws EJBException {
        userTransaction = context.getUserTransaction();
        AdminRolesResponse adminRolesResponse = new AdminRolesResponse();
        List<String> stringRoles = new ArrayList<String>(0);
        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();
                adminRolesResponse.setStatusCode(ResponseHelper.ADMIN_ADMIN_ROLE_RETRIEVE_INVALID_TICKET);
                return adminRolesResponse;
            }
            /*
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                adminRolesResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return adminRolesResponse;
            }
            */
            
            List<AdminRoleBean> adminRoleBeanList = new ArrayList<AdminRoleBean>(0);

            adminRoleBeanList = QueryRepository.findAllAdminRolePeriods(em);

            for (AdminRoleBean item : adminRoleBeanList) {
                stringRoles.add(item.getName());
            }

            adminRolesResponse.setStatusCode(ResponseHelper.ADMIN_ADMIN_ROLE_RETRIEVE_SUCCESS);
            adminRolesResponse.setAdminRoles(stringRoles);
            userTransaction.commit();

            return adminRolesResponse;

        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return adminRolesResponse;

    }

    public boolean isThisDateValid(String dateToValidate, String dateFromat) {

        if (dateToValidate == null) {
            System.out.println("Data non valida");
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
        sdf.setLenient(false);

        try {
            Date date = sdf.parse(dateToValidate);
            System.out.println(date);
        }
        catch (ParseException e) {
            System.out.println("Data non valida");
            return false;
        }
        System.out.println("Data valida");
        return true;
    }

}