package com.techedge.mp.core.actions.admin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveStatisticsData;
import com.techedge.mp.core.business.interfaces.StatisticInfo;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminStatisticsRetrieveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminStatisticsRetrieveAction() {}

    public RetrieveStatisticsData execute(String adminTicketId, String requestId) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            RetrieveStatisticsData retrieveStatisticsData = new RetrieveStatisticsData();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                retrieveStatisticsData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_STATISTICS_INVALID_TICKET);

                return retrieveStatisticsData;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                retrieveStatisticsData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return retrieveStatisticsData;
            }
            
            

            Integer activeTransactions = QueryRepository.countActiveTransactions(em_crud);
            Integer activeTickets = QueryRepository.countActiveTickets(em_crud);

            List<StatisticInfo> statistiInfoList = new ArrayList<StatisticInfo>(0);

            StatisticInfo statisticInfo = null;
            
            // Calcolo timestamp
            
            SimpleDateFormat sdfFull = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");

            Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
            Date todayEndDate = calendar.getTime();
            int todayCurrentHour   = calendar.get(Calendar.HOUR_OF_DAY);
            int todayCurrentMinute = calendar.get(Calendar.MINUTE);
            int todayCurrentSecond = calendar.get(Calendar.SECOND);
            
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date todayStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date yesterdayEndDate = calendar.getTime();
            calendar.set(Calendar.HOUR_OF_DAY, todayCurrentHour);
            calendar.set(Calendar.MINUTE, todayCurrentMinute);
            calendar.set(Calendar.SECOND, todayCurrentSecond);
            Date yesterdayCurrentTime = calendar.getTime();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date yesterdayStartDate = calendar.getTime();

            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -7);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date lastWeekStartDate = calendar.getTime();
            calendar.set(Calendar.HOUR_OF_DAY, todayCurrentHour);
            calendar.set(Calendar.MINUTE, todayCurrentMinute);
            calendar.set(Calendar.SECOND, todayCurrentSecond);
            Date lastWeekCurrentTime = calendar.getTime();
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date lastWeekEndDate = calendar.getTime();
            
            System.out.println("Today:     " + sdfFull.format(todayStartDate) + " - " + sdfFull.format(todayEndDate));
            System.out.println("Yesterday: " + sdfFull.format(yesterdayStartDate) + " - " + sdfFull.format(yesterdayCurrentTime) + " - " + sdfFull.format(yesterdayEndDate));
            System.out.println("Last week: " + sdfFull.format(lastWeekStartDate) + " - " + sdfFull.format(lastWeekCurrentTime) + " - " + sdfFull.format(lastWeekEndDate));
                        
            // Transazioni prepaid
            
            Integer prepaidTransaction7Total   = QueryRepository.statisticsBOPrepaidTransactions(em_crud, lastWeekStartDate, lastWeekEndDate);
            Integer prepaidTransaction7Partial = QueryRepository.statisticsBOPrepaidTransactions(em_crud, lastWeekStartDate, lastWeekCurrentTime);
            Integer prepaidTransaction1Total   = QueryRepository.statisticsBOPrepaidTransactions(em_crud, yesterdayStartDate, yesterdayEndDate);
            Integer prepaidTransaction1Partial = QueryRepository.statisticsBOPrepaidTransactions(em_crud, yesterdayStartDate, yesterdayCurrentTime);
            Integer prepaidTransactionActual   = QueryRepository.statisticsBOPrepaidTransactions(em_crud, todayStartDate, todayEndDate);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("PREPAID_TRANSACTION_7_TOTAL");
            statisticInfo.setValue(String.valueOf(prepaidTransaction7Total));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("PREPAID_TRANSACTION_7_PARTIAL");
            statisticInfo.setValue(String.valueOf(prepaidTransaction7Partial));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("PREPAID_TRANSACTION_1_TOTAL");
            statisticInfo.setValue(String.valueOf(prepaidTransaction1Total));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("PREPAID_TRANSACTION_1_PARTIAL");
            statisticInfo.setValue(String.valueOf(prepaidTransaction1Partial));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("PREPAID_TRANSACTION_ACTUAL");
            statisticInfo.setValue(String.valueOf(prepaidTransactionActual));
            statistiInfoList.add(statisticInfo);
            
            
            // Transazioni prepaid refueling
            
            Integer refuelingTransaction7Total   = QueryRepository.statisticsBORefuelingTransactions(em_crud, lastWeekStartDate, lastWeekEndDate);
            Integer refuelingTransaction7Partial = QueryRepository.statisticsBORefuelingTransactions(em_crud, lastWeekStartDate, lastWeekCurrentTime);
            Integer refuelingTransaction1Total   = QueryRepository.statisticsBORefuelingTransactions(em_crud, yesterdayStartDate, yesterdayEndDate);
            Integer refuelingTransaction1Partial = QueryRepository.statisticsBORefuelingTransactions(em_crud, yesterdayStartDate, yesterdayCurrentTime);
            Integer refuelingTransactionActual   = QueryRepository.statisticsBORefuelingTransactions(em_crud, todayStartDate, todayEndDate);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("REFUELING_TRANSACTION_7_TOTAL");
            statisticInfo.setValue(String.valueOf(refuelingTransaction7Total));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("REFUELING_TRANSACTION_7_PARTIAL");
            statisticInfo.setValue(String.valueOf(refuelingTransaction7Partial));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("REFUELING_TRANSACTION_1_TOTAL");
            statisticInfo.setValue(String.valueOf(refuelingTransaction1Total));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("REFUELING_TRANSACTION_1_PARTIAL");
            statisticInfo.setValue(String.valueOf(refuelingTransaction1Partial));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("REFUELING_TRANSACTION_ACTUAL");
            statisticInfo.setValue(String.valueOf(refuelingTransactionActual));
            statistiInfoList.add(statisticInfo);
            
            // Transazioni postpaid
            
            Integer postpaidTransaction7Total   = QueryRepository.statisticsBOPostPaidTransactions(em_crud, lastWeekStartDate, lastWeekEndDate);
            Integer postpaidTransaction7Partial = QueryRepository.statisticsBOPostPaidTransactions(em_crud, lastWeekStartDate, lastWeekCurrentTime);
            Integer postpaidTransaction1Total   = QueryRepository.statisticsBOPostPaidTransactions(em_crud, yesterdayStartDate, yesterdayEndDate);
            Integer postpaidTransaction1Partial = QueryRepository.statisticsBOPostPaidTransactions(em_crud, yesterdayStartDate, yesterdayCurrentTime);
            Integer postpaidTransactionActual   = QueryRepository.statisticsBOPostPaidTransactions(em_crud, todayStartDate, todayEndDate);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("POSTPAID_TRANSACTION_7_TOTAL");
            statisticInfo.setValue(String.valueOf(postpaidTransaction7Total));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("POSTPAID_TRANSACTION_7_PARTIAL");
            statisticInfo.setValue(String.valueOf(postpaidTransaction7Partial));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("POSTPAID_TRANSACTION_1_TOTAL");
            statisticInfo.setValue(String.valueOf(postpaidTransaction1Total));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("POSTPAID_TRANSACTION_1_PARTIAL");
            statisticInfo.setValue(String.valueOf(postpaidTransaction1Partial));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("POSTPAID_TRANSACTION_ACTUAL");
            statisticInfo.setValue(String.valueOf(postpaidTransactionActual));
            statistiInfoList.add(statisticInfo);
            
            
            // Utenti customer iscritti
            
            Integer customerUser7Total   = QueryRepository.statisticsBOCustomerUsers(em_crud, lastWeekStartDate, lastWeekEndDate);
            Integer customerUser7Partial = QueryRepository.statisticsBOCustomerUsers(em_crud, lastWeekStartDate, lastWeekCurrentTime);
            Integer customerUser1Total   = QueryRepository.statisticsBOCustomerUsers(em_crud, yesterdayStartDate, yesterdayEndDate);
            Integer customerUser1Partial = QueryRepository.statisticsBOCustomerUsers(em_crud, yesterdayStartDate, yesterdayCurrentTime);
            Integer customerUserActual   = QueryRepository.statisticsBOCustomerUsers(em_crud, todayStartDate, todayEndDate);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("CUSTOMER_USER_7_TOTAL");
            statisticInfo.setValue(String.valueOf(customerUser7Total));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("CUSTOMER_USER_7_PARTIAL");
            statisticInfo.setValue(String.valueOf(customerUser7Partial));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("CUSTOMER_USER_1_TOTAL");
            statisticInfo.setValue(String.valueOf(customerUser1Total));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("CUSTOMER_USER_1_PARTIAL");
            statisticInfo.setValue(String.valueOf(customerUser1Partial));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("CUSTOMER_USER_ACTUAL");
            statisticInfo.setValue(String.valueOf(customerUserActual));
            statistiInfoList.add(statisticInfo);
            
            
            // Utenti business iscritti
            
            Integer businessUser7Total   = QueryRepository.statisticsBOBusinessUsers(em_crud, lastWeekStartDate, lastWeekEndDate);
            Integer businessUser7Partial = QueryRepository.statisticsBOBusinessUsers(em_crud, lastWeekStartDate, lastWeekCurrentTime);
            Integer businessUser1Total   = QueryRepository.statisticsBOBusinessUsers(em_crud, yesterdayStartDate, yesterdayEndDate);
            Integer businessUser1Partial = QueryRepository.statisticsBOBusinessUsers(em_crud, yesterdayStartDate, yesterdayCurrentTime);
            Integer businessUserActual   = QueryRepository.statisticsBOBusinessUsers(em_crud, todayStartDate, todayEndDate);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("BUSINESS_USER_7_TOTAL");
            statisticInfo.setValue(String.valueOf(businessUser7Total));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("BUSINESS_USER_7_PARTIAL");
            statisticInfo.setValue(String.valueOf(businessUser7Partial));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("BUSINESS_USER_1_TOTAL");
            statisticInfo.setValue(String.valueOf(businessUser1Total));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("BUSINESS_USER_1_PARTIAL");
            statisticInfo.setValue(String.valueOf(businessUser1Partial));
            statistiInfoList.add(statisticInfo);
            
            statisticInfo = new StatisticInfo();
            statisticInfo.setKey("BUSINESS_USER_ACTUAL");
            statisticInfo.setValue(String.valueOf(businessUserActual));
            statistiInfoList.add(statisticInfo);
            

            retrieveStatisticsData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_STATISTICS_SUCCESS);
            retrieveStatisticsData.setStatisticsList(statistiInfoList);

            userTransaction.commit();

            return retrieveStatisticsData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin retrieve activity log with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
