package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminTransactionUpdateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminTransactionUpdateAction() {}

    public String execute(String adminTicketId, String requestId, String transactionId, String finalStatusType, Boolean GFGNotification, Boolean confirmed,
            Integer reconciliationAttemptsLeft) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            String response = null;

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_UPDATE_TRANSACTION_INVALID_TICKET;

                return response;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            userTransaction.commit();

            userTransaction.begin();

            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em_crud, transactionId);

            if (transactionBean == null) {

                // Transazione non trovata
                TransactionHistoryBean transactionHistoryBean = QueryRepository.findTransactionHistoryBeanById(em_crud, transactionId);

                if (transactionHistoryBean == null) {

                    // Transazione non trovata in history
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                    userTransaction.commit();

                    response = ResponseHelper.ADMIN_UPDATE_TRANSACTION_FAILURE;

                    return response;
                }
                else {

                    transactionHistoryBean.setFinalStatusType(finalStatusType);
                    transactionHistoryBean.setGFGNotification(GFGNotification);
                    transactionHistoryBean.setConfirmed(confirmed);
                    transactionHistoryBean.setReconciliationAttemptsLeft(reconciliationAttemptsLeft);

                    userTransaction.commit();

                    response = ResponseHelper.ADMIN_UPDATE_TRANSACTION_SUCCESS;

                    return response;
                }
            }
            else {

                transactionBean.setFinalStatusType(finalStatusType);
                transactionBean.setGFGNotification(GFGNotification);
                transactionBean.setConfirmed(confirmed);
                transactionBean.setReconciliationAttemptsLeft(reconciliationAttemptsLeft);

                userTransaction.commit();

                response = ResponseHelper.ADMIN_UPDATE_TRANSACTION_SUCCESS;

                return response;
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin update transaction with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
