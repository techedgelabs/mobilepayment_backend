package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.techedge.mp.core.business.AdminService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StationAdmin;
import com.techedge.mp.core.business.interfaces.StationsAdminData;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminStationsCreateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminStationsCreateAction() {}

    public StationsAdminData execute(String adminTicketId, String requestId, StationsAdminData stationAdminData, AdminService adminService) throws EJBException {

        //UserTransaction userTransaction = context.getUserTransaction();

        try {
            //	userTransaction.begin();

            StationsAdminData stationAdminDataResponse = new StationsAdminData();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                //	userTransaction.commit();

                stationAdminDataResponse.setStatusCode(ResponseHelper.ADMIN_ADD_STATIONS_INVALID_TICKET);

                return stationAdminDataResponse;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                stationAdminDataResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return stationAdminDataResponse;
            }

            List<StationAdmin> stationAdminList = stationAdminData.getStationAdminList();
            List<StationAdmin> stationAdminListResponse = new ArrayList<StationAdmin>(0);

            if (stationAdminList == null) {

                stationAdminDataResponse.setStatusCode(ResponseHelper.ADMIN_ADD_STATIONS_SUCCESS);

                return stationAdminDataResponse;

            }

            String response = null;
            StationAdmin stationAdmin;
            for (StationAdmin sa : stationAdminList) {

                response = adminService.adminAddStation(adminTicketId, requestId, sa);
                stationAdmin = new StationAdmin();
                stationAdmin.setStationID(sa.getStationID());
                stationAdmin.setOilAcquirerID(sa.getOilAcquirerID());
                stationAdmin.setOilShopLogin(sa.getOilShopLogin());
                stationAdmin.setNoOilAcquirerID(sa.getNoOilAcquirerID());
                stationAdmin.setNoOilShopLogin(sa.getNoOilShopLogin());

                if (response.contains("200")) {
                    stationAdmin.setGenerated(true);
                }
                else {
                    stationAdmin.setGenerated(false);
                }
                stationAdmin.setResponse(response);

                stationAdminListResponse.add(stationAdmin);

            }

            stationAdminDataResponse.setStatusCode(ResponseHelper.ADMIN_ADD_STATIONS_SUCCESS);
            stationAdminDataResponse.setStationAdminList(stationAdminListResponse);
            return stationAdminDataResponse;

        }
        catch (Exception ex2) {

            String message = "FAILED admin add stations with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);
            throw new EJBException(ex2);
        }
    }
}
