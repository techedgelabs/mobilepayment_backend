package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminPoPArchiveResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventHistoryBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminPoPTransactionArchiveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @EJB
    private LoggerService loggerService;

    public AdminPoPTransactionArchiveAction() {}

    public AdminPoPArchiveResponse execute(String adminTicketId, String requestId, Date startDate, Date endDate) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        AdminPoPArchiveResponse adminPoPArchiveResponse = new AdminPoPArchiveResponse();

        try {
            userTransaction.begin();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                adminPoPArchiveResponse.setStatusCode(ResponseHelper.ADMIN_POPARCHIVE_INVALID_TICKET);
                return adminPoPArchiveResponse;
            }
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                adminPoPArchiveResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return adminPoPArchiveResponse;
            }
            Integer totalSize = 0;

            List<PostPaidTransactionBean> postPaidTransactionBeanList1 = QueryRepository.findPostPaidTransactionToHistoryByStatusType(em_crud);
            List<PostPaidTransactionBean> postPaidTransactionBeanList2 = null;

            if (postPaidTransactionBeanList1 != null) {
                totalSize += postPaidTransactionBeanList1.size();
            }

            if (startDate != null && endDate != null) {
                postPaidTransactionBeanList2 = QueryRepository.findPostPaidTransactionsPaidByDate(em_crud, startDate, endDate);

                if (postPaidTransactionBeanList2 != null) {
                    totalSize += postPaidTransactionBeanList2.size();
                }
            }

            userTransaction.commit();

            List<PostPaidTransactionBean> postPaidTransactionBeanList = new ArrayList<PostPaidTransactionBean>(totalSize);

            if (postPaidTransactionBeanList1 != null) {
                postPaidTransactionBeanList.addAll(postPaidTransactionBeanList1);
            }
            if (postPaidTransactionBeanList2 != null) {
                postPaidTransactionBeanList.addAll(postPaidTransactionBeanList2);
            }

            // Per ogni transazione estratta
            for (PostPaidTransactionBean postPaidTransactionBean : postPaidTransactionBeanList) {

                userTransaction.begin();

                // 0) Ricarica i dati nel nuovo blocco transazione
                postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em_crud, postPaidTransactionBean.getMpTransactionID());

                // 1) Crea la transazione archiviata
                PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = new PostPaidTransactionHistoryBean(postPaidTransactionBean);

                postPaidTransactionHistoryBean.setArchivingDate(new Date());

                // 2) Persisti le informazioni nelle tabelle archivio

                em_crud.persist(postPaidTransactionHistoryBean);

                // 3) Persistenza dei refuel collegati alla transazione archivio
                for (PostPaidRefuelHistoryBean postPaidRefuelHistoryBean : postPaidTransactionHistoryBean.getRefuelHistoryBean()) {

                    em_crud.persist(postPaidRefuelHistoryBean);
                }

                // 4) Persistenza dei shop collegati alla transazione archivio
                for (PostPaidCartHistoryBean postPaidCartHistoryBean : postPaidTransactionHistoryBean.getCartHistoryBean()) {

                    em_crud.persist(postPaidCartHistoryBean);
                }

                // 5) Persistenza dei event collegati alla transazione archivio
                for (PostPaidTransactionEventHistoryBean postPaidTransactionEventHistoryBean : postPaidTransactionHistoryBean.getPostPaidTransactionEventHistoryBean()) {

                    em_crud.persist(postPaidTransactionEventHistoryBean);
                }

                // 6) Persistenza dei payment event collegati alla transazione archivio
                for (PostPaidTransactionPaymentEventHistoryBean postPaidTransactionPaymentEventHistoryBean : postPaidTransactionHistoryBean.getPostPaidTransactionPaymentEventHistoryBean()) {

                    em_crud.persist(postPaidTransactionPaymentEventHistoryBean);
                }

                // 7) Eliminazione dei refuel collegati alla transazione
                for (PostPaidRefuelBean postPaidRefuelBean : postPaidTransactionBean.getRefuelBean()) {

                    em_crud.remove(postPaidRefuelBean);
                }

                // 8) Eliminazione dei shop collegati alla transazione
                for (PostPaidCartBean postPaidCartBean : postPaidTransactionBean.getCartBean()) {

                    em_crud.remove(postPaidCartBean);
                }

                // 9) Eliminazione dei event collegati alla transazione
                for (PostPaidTransactionEventBean postPaidTransactionEventBean : postPaidTransactionBean.getPostPaidTransactionEventBean()) {

                    em_crud.remove(postPaidTransactionEventBean);
                }

                // 10) Eliminazione dei payment event collegati alla transazione
                for (PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean : postPaidTransactionBean.getPostPaidTransactionPaymentEventBean()) {

                    em_crud.remove(postPaidTransactionPaymentEventBean);
                }

                // 11) Eliminazione della transazione
                em_crud.remove(postPaidTransactionBean);

                userTransaction.commit();

            }
            adminPoPArchiveResponse.setArchivedPoPTransaction(totalSize);
            adminPoPArchiveResponse.setStatusCode(ResponseHelper.ADMIN_POPARCHIVE_SUCCESS);
            return adminPoPArchiveResponse;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED transaction archiving with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }
}
