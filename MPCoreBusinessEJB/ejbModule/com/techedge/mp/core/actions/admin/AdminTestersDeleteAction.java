package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.techedge.mp.core.business.AdminService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.DeleteTestersData;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TesterUser;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminTestersDeleteAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminTestersDeleteAction() {}

    public DeleteTestersData execute(String adminTicketId, String requestId, String emailPrefix, String emailSuffix, Integer startNumber, Integer userCount,
            AdminService adminService) throws EJBException {

        //		UserTransaction userTransaction = context.getUserTransaction();

        try {
            //			userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {
                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                //userTransaction.commit();
                DeleteTestersData deleteTestersData = new DeleteTestersData();
                deleteTestersData.setStatusCode(ResponseHelper.ADMIN_DELETE_TESTERS_INVALID_TICKET);
                return deleteTestersData;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                DeleteTestersData deleteTestersData = new DeleteTestersData();
                deleteTestersData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return deleteTestersData;
            }

            TesterUser testerUser;
            List<TesterUser> testerUserList = new ArrayList<TesterUser>(0);

            String email;
            String response;
            for (int i = startNumber; i < (startNumber + userCount); i++) {

                email = emailPrefix + String.format("%09d", i) + emailSuffix;
                testerUser = new TesterUser();
                testerUser.setEmail(email);
                //	testerUser.setPassword(password);
                //	testerUser.setPin(pin);
                //	testerUser.setEncryptedPassword(EncoderHelper.encode(password));
                //	testerUser.setEncryptedPin(EncoderHelper.encode(pin));
                //				
                UserBean userBean = QueryRepository.findUserCustomerByEmail(em, email);

                if (userBean != null) {
                    response = adminService.adminUserDelete(adminTicketId, requestId, userBean.getId(), new Date(), new Date());
                    if (response.contains("200")) {
                        testerUser.setDeleted(true);
                        testerUser.setResponse(response);
                    }
                    else {
                        testerUser.setDeleted(false);
                        testerUser.setResponse(response);
                    }
                    testerUserList.add(testerUser);
                }
            }
            //userTransaction.commit();

            DeleteTestersData deleteTestersData = new DeleteTestersData();
            deleteTestersData.setTesterList(testerUserList);
            deleteTestersData.setStatusCode(ResponseHelper.ADMIN_DELETE_TESTERS_SUCCESS);
            return deleteTestersData;
        }
        catch (Exception ex2) {

            String message = "FAILED testers deletion with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

}
