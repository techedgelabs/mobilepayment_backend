package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminTermsOfServiceForceUpdateAction {
    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminTermsOfServiceForceUpdateAction() {}

    public String execute(String adminTicketId, String requestId, String keyval, Long personalDataId, Boolean valid) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_TERMS_OF_SERVICE_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            if (valid == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "valid is null");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_TERMS_OF_SERVICE_FAILURE;
            }

            PersonalDataBean personalDataBean = QueryRepository.findAllPersonalDataBean(em_crud, personalDataId);

            userTransaction.commit();

            userTransaction.begin();

            List<TermsOfServiceBean> termsOfServiceBeanList = new ArrayList<TermsOfServiceBean>(0);

            if (keyval != null && personalDataId == null) {
                termsOfServiceBeanList = QueryRepository.findTermOfServiceByKeyVal(em_crud, keyval);
            }

            else if (keyval == null && personalDataId != null) {
                termsOfServiceBeanList = QueryRepository.findTermOfServiceByPersonalDataId(em_crud, personalDataBean);
            }

            else if (keyval != null && personalDataId != null) {
                termsOfServiceBeanList = QueryRepository.findTermOfServiceByKeyValAndId(em_crud, keyval, personalDataBean);
            }

            else {
                termsOfServiceBeanList = QueryRepository.findTermOfServiceAll(em_crud);
            }

            if (termsOfServiceBeanList == null || termsOfServiceBeanList.isEmpty()) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "terOfServiceList is empty");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_TERMS_OF_SERVICE_FAILURE;
            }

            for (TermsOfServiceBean termsOfServiceBean : termsOfServiceBeanList) {
                termsOfServiceBean.setValid(valid);
                em_crud.merge(termsOfServiceBean);
            }

            userTransaction.commit();

            String response = ResponseHelper.ADMIN_UPDATE_TERMS_OF_SERVICE_SUCCESS;

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin force update terms of service with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
