package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.xml.bind.DatatypeConverter;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.AdminRoleEnum;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PaymentMethodResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.ShopTransactionDataBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.RandomGenerator;
import com.techedge.mp.payment.adapter.business.GSServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GenerateRedirectUrlResponse;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminRefuelingUserInsertPaymentMethodAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserCategoryService userCategoryService;

    public AdminRefuelingUserInsertPaymentMethodAction() {}

    public PaymentMethodResponse execute(String ticketId, String requestId, String username, Integer pinCheckMaxAttempts, String apiKey,
            String checkAmountValue, Double maxCheckAmount, String uicCode, String groupAcquirer, String encodedSecretKey) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        PaymentMethodResponse response = new PaymentMethodResponse();
        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, ticketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.ADMIN_INSERT_PAY_METH_REFUELING_USER_INVALID_TICKET);
                return response;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())
                    && !CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REFUELING_MANAGER)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return response;
            }

            // Verifica che non esista gi� un utente con quella email
            UserBean userBean = QueryRepository.findUserCustomerByEmail(em, username);

            if (userBean == null) {

                // Esiste gi� un utente con quella email
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not exists");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.ADMIN_INSERT_PAY_METH_REFUELING_USER_USER_NOT_EXISTS);
                return response;
            }

            if (!userBean.getUserType().equals(User.USER_TYPE_REFUELING_NEW_ACQUIRER) && !userBean.getUserType().equals(User.USER_TYPE_REFUELING_OAUTH2)) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not refueling");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.ADMIN_INSERT_PAY_METH_REFUELING_USER_USER_NOT_REFUELING);
                return response;
            }

            
            // Ottieni il valore della secureString
            GSServiceRemote gsService = null;

            try {
                gsService = EJBHomeCache.getInstance().getGsService();
            }
            catch (InterfaceNotFoundException ex) {

                try {
                    userTransaction.rollback();
                }
                catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (SecurityException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (SystemException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                String message = "FAILED manage payment with message (" + ex.getMessage() + ")";
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

                throw new EJBException(ex);
            }

            Boolean defaultMethod = false;
            if (userBean.findDefaultPaymentInfoBean() == null) {
                defaultMethod = true;
            }

            Double checkAmount = 0.12; // valore di default
            if (checkAmountValue.equals("rand")) {

                try {
                    checkAmount = RandomGenerator.generateRandom(maxCheckAmount);
                }
                catch (Exception e) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Error parsing checkAmount: " + e.getMessage());
                    checkAmount = 0.12;
                }
            }
            else {

                try {
                    checkAmount = Double.valueOf(checkAmountValue);
                }
                catch (Exception e) {
                    checkAmount = 0.12;
                }
            }

            // Genera uno shopTransactionID
            String shopTransactionId = new IdGenerator().generateId(16).substring(0, 32);

            String stringCheckAmount = String.valueOf(checkAmount);

            // TODO gestire le eccezioni
            GenerateRedirectUrlResponse generateRedirectUrlResponse = gsService.generateRedirectUrl(requestId, shopTransactionId, apiKey, stringCheckAmount, uicCode,
                    groupAcquirer, encodedSecretKey, true);

            String bankTransactionId = "";
            String currency = "";
            String shopTransactionIdUpd = "";
            String redirectURL = "";

            if (generateRedirectUrlResponse != null) {

                redirectURL = generateRedirectUrlResponse.getRedirectUrl();
                currency = generateRedirectUrlResponse.getCurrency();
                shopTransactionIdUpd = generateRedirectUrlResponse.getShopTransactionId();
            }

            if (redirectURL == null) {

                // Errore nella comunicazione
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Connection error with payment system");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_CONNECTION_ERROR);
                response.setRedirectURL(redirectURL);
                return response;
            }
            PaymentInfoBean paymentInfoBean = null;

            paymentInfoBean = userBean.addNewPendingPaymentInfo(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD, null, defaultMethod, pinCheckMaxAttempts, checkAmount, bankTransactionId,
                    currency, apiKey, shopTransactionIdUpd);
            
            // Aggiorna le informazioni sui metodi di pagamento su db
            if (defaultMethod == true) {

                for (PaymentInfoBean paymentInfoBeanTemp : userBean.getPaymentData()) {

                    if (paymentInfoBeanTemp.getId() != paymentInfoBean.getId()) {

                        em.persist(paymentInfoBeanTemp);
                    }
                }
            }

            System.out.println("Salvataggio nuovo metodo di pagamento");

            // Memorizza su db l'informazione sulla carta
            em.persist(paymentInfoBean);
            em.merge(userBean);

            // Registra su db l'associazione tra lo shopTransactionID e l'utente
            ShopTransactionDataBean shopTransactionData = ShopTransactionDataBean.createNewShopTransactionDataBean(shopTransactionIdUpd, paymentInfoBean);

            em.merge(shopTransactionData);

            userTransaction.commit();

            String encodedRedirectUrl = DatatypeConverter.printBase64Binary(redirectURL.getBytes());

            response.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_SUCCESS);
            response.setRedirectURL(encodedRedirectUrl);
            response.getPaymentMethod().setId(paymentInfoBean.getId());
            response.getPaymentMethod().setType(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD);

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
