package com.techedge.mp.core.actions.admin;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminUserNotVerifiedDeleteResult;
import com.techedge.mp.core.business.interfaces.AdminUserNotVerifiedDeleteSingleResult;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VerificationCodeBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUserNotVerifiedDeleteAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminUserNotVerifiedDeleteAction() {}

    public AdminUserNotVerifiedDeleteResult execute(String adminTicketId, String requestId, Long userId, Date creationStart, Date creationEnd) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            AdminUserNotVerifiedDeleteResult adminUserNotVerifiedDeleteResult = new AdminUserNotVerifiedDeleteResult();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                adminUserNotVerifiedDeleteResult.setStatusCode(ResponseHelper.ADMIN_UPDATE_USER_INVALID_TICKET);

                return adminUserNotVerifiedDeleteResult;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                
                adminUserNotVerifiedDeleteResult.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return adminUserNotVerifiedDeleteResult;
            }

            userTransaction.commit();
            

            if (userId != null) {
                
                AdminUserNotVerifiedDeleteSingleResult adminUserNotVerifiedDeleteSingleResult = this.deleteUser(userId, userTransaction);
                
                adminUserNotVerifiedDeleteResult.getResults().add(adminUserNotVerifiedDeleteSingleResult);
                adminUserNotVerifiedDeleteResult.setStatusCode(ResponseHelper.ADMIN_UPDATE_USER_SUCCESS);
                
                return adminUserNotVerifiedDeleteResult;

            }
            else {
                
                if (creationStart != null && creationEnd != null) {

                    List<UserBean> userBeanList = QueryRepository.findUserNotVerifiedByCreationDate(em_crud, creationStart, creationEnd);
    
                    for (UserBean userBean : userBeanList) {
                        
                        AdminUserNotVerifiedDeleteSingleResult adminUserNotVerifiedDeleteSingleResult = this.deleteUser(userBean.getId(), userTransaction);
                        adminUserNotVerifiedDeleteResult.getResults().add(adminUserNotVerifiedDeleteSingleResult);
                    }
                    
                    adminUserNotVerifiedDeleteResult.setStatusCode(ResponseHelper.ADMIN_UPDATE_USER_SUCCESS);
                    
                    return adminUserNotVerifiedDeleteResult;
                }
                else {
    
                    // Parametri non validi
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid parameters");
    
                    adminUserNotVerifiedDeleteResult.setStatusCode(ResponseHelper.ADMIN_UPDATE_USER_FAILURE);
                    
                    return adminUserNotVerifiedDeleteResult;
                }
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin delete user not verified with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
    
    private AdminUserNotVerifiedDeleteSingleResult deleteUser(Long userId, UserTransaction userTransaction) {
        
        AdminUserNotVerifiedDeleteSingleResult adminUserNotVerifiedDeleteSingleResult = new AdminUserNotVerifiedDeleteSingleResult();
        
        try {
            
            userTransaction.begin();
            
            UserBean userBean = QueryRepository.findUserById(em_crud, userId);
    
            if (userBean == null) {
                adminUserNotVerifiedDeleteSingleResult.setId(null);
                adminUserNotVerifiedDeleteSingleResult.setMessage("Utente con id " + userId + " non trovato");
                
                return adminUserNotVerifiedDeleteSingleResult;
            }
            
            if (userBean.getUserStatus() != User.USER_STATUS_NEW) {
                adminUserNotVerifiedDeleteSingleResult.setId(userBean.getId());
                adminUserNotVerifiedDeleteSingleResult.setMessage("Utente con id " + userId + " in stato non valido per cancellazione: " + userBean.getUserStatus());
                
                return adminUserNotVerifiedDeleteSingleResult;
            }
        
            String email = userBean.getPersonalDataBean().getSecurityDataEmail();
        
            List<TermsOfServiceBean> termsOfServiceList = QueryRepository.findTermOfServiceByPersonalDataId(em_crud, userBean.getPersonalDataBean());
            if (termsOfServiceList != null && !termsOfServiceList.isEmpty()) {
                for(TermsOfServiceBean termsOfServiceBean : termsOfServiceList) {
                    em_crud.remove(termsOfServiceBean);
                }
            }
            
            List<MobilePhoneBean> mobilePhoneBeanList = QueryRepository.findMobilePhoneByUserBean(em_crud, userBean);
            if (mobilePhoneBeanList != null && !mobilePhoneBeanList.isEmpty()) {
                for(MobilePhoneBean mobilePhoneBean : mobilePhoneBeanList) {
                    em_crud.remove(mobilePhoneBean);
                }
            }
            
            List<VerificationCodeBean> VerificationCodeBeanList = QueryRepository.findVerificationCodeBeanByUser(em_crud, userBean);
            if (VerificationCodeBeanList != null && !VerificationCodeBeanList.isEmpty()) {
                for (VerificationCodeBean verificationCodeBean : VerificationCodeBeanList) {
                    em_crud.remove(verificationCodeBean);
                }
            }
            
            List<LoyaltyCardBean> loyaltyCardBeanList = QueryRepository.findLoyaltyCardBeanByUserBean(em_crud, userBean);
            if (loyaltyCardBeanList != null && !loyaltyCardBeanList.isEmpty()) {
                for (LoyaltyCardBean loyaltyCardBean : loyaltyCardBeanList) {
                    em_crud.remove(loyaltyCardBean);
                }
            }
            
            em_crud.remove(userBean.getPersonalDataBean());
            
            em_crud.remove(userBean);
            
            userTransaction.commit();
            
            adminUserNotVerifiedDeleteSingleResult.setId(userBean.getId());
            adminUserNotVerifiedDeleteSingleResult.setMessage("Utente " + email + " (id:" + userId + ") cancellato con successo");
            
            return adminUserNotVerifiedDeleteSingleResult;
            
        }
        catch (Exception ex) {
            
            ex.printStackTrace();
            
            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            adminUserNotVerifiedDeleteSingleResult.setId(userId);
            adminUserNotVerifiedDeleteSingleResult.setMessage("Errore nella cancellazione dell'utente " + userId + " - eccezione: " + ex.getLocalizedMessage());
            
            return adminUserNotVerifiedDeleteSingleResult;
        }
    }
}
