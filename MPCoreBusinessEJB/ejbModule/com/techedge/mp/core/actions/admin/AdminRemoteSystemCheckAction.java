package com.techedge.mp.core.actions.admin;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminRemoteSystemCheckResult;
import com.techedge.mp.core.business.interfaces.AdminRoleEnum;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.RemoteSystem;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.GetOffersResult;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.Response;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.model.AdminBean;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.DWHAdapterResult;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.GetCitiesResult;
import com.techedge.mp.parking.integration.business.interfaces.StatusCodeHelper;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminRemoteSystemCheckAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminRemoteSystemCheckAction() {}

    public List<AdminRemoteSystemCheckResult> execute(String adminTicketId, String requestID, List<RemoteSystem> remoteSystemList, ForecourtInfoServiceRemote forecourtInfoService, 
            CRMAdapterServiceRemote crmAdapterService, FidelityServiceRemote fidelityService, DWHAdapterServiceRemote dwhAdapterService, 
            RefuelingNotificationServiceRemote refuelingNotificationService, ParkingServiceRemote parkingService, CRMServiceRemote crmService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        List<AdminRemoteSystemCheckResult> resultList = new ArrayList<AdminRemoteSystemCheckResult>();

        try {
            userTransaction.begin();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                AdminRemoteSystemCheckResult result = new AdminRemoteSystemCheckResult();
                result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_INVALID_TICKET);
                result.setStatusMessage("Invalid ticket");
                resultList.add(result);

                return resultList;
            }
            
            AdminBean adminBean = adminTicketBean.getAdminBean();
            if (adminBean == null) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin user not found");

                userTransaction.commit();

                AdminRemoteSystemCheckResult result = new AdminRemoteSystemCheckResult();
                result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_INVALID_TICKET);
                result.setStatusMessage("Admin user not found");
                resultList.add(result);

                return resultList;
            }
            
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())
                    && !CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REMOTE_SYSTEM_CHECK)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                AdminRemoteSystemCheckResult result = new AdminRemoteSystemCheckResult();
                result.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                result.setStatusMessage("Admin not authotized (not correct role)");
                resultList.add(result);

                return resultList;
            }
            
            for (RemoteSystem remoteSystem : remoteSystemList) {
                
                if (remoteSystem == null || remoteSystem.getTypeSystemId() == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid parameter remoteSystem");

                    userTransaction.commit();

                    AdminRemoteSystemCheckResult result = new AdminRemoteSystemCheckResult();
                    result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_FAILURE);
                    result.setStatusMessage("Invalid parameter remoteSystem");
                    resultList.add(result);

                    return resultList;
                }

                AdminRemoteSystemCheckResult result = new AdminRemoteSystemCheckResult();
                result.setSystemId(remoteSystem.getTypeSystemId());
                
                Date timestamp = new Date();
                long requestTimestamp = timestamp.getTime();
                String requestId = String.valueOf(requestTimestamp);
                
                switch (remoteSystem.getTypeSystemId()) {
                    case GFG:
                        try {
                            GetStationDetailsResponse response = forecourtInfoService.getStationDetails(requestId, "00000", null, false);
                            result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS);
                            result.setStatusMessage("Service status code: " + response.getStatusCode());
                        }
                        catch (Exception ex) {
                            result.setStatusCode(ResponseHelper.SYSTEM_ERROR);
                            result.setStatusMessage("Errore nella creazione dell'istanza del servizio di verifica impianto: " + ex.getMessage());
                            System.err.println("Errore nella creazione dell'istanza del servizio di verifica impianto: " + ex.getMessage());
                        }

                        break;
/*
                    case CRM:
                        try {
                            String sessionID = new IdGenerator().generateId(16).substring(0, 32);
                            String audienceID = "999";
                            String fiscalCode = "9999999999999999";
                            String surname = "";
                            String pv = "17131";
                            String product = "GG";
                            boolean flagPayment = true;
                            Integer credits = 0;
                            Double refuelQuantity = 0.0;
                            Double amount = 0.0;
                            String refuelMode = "Servito";
                            boolean flagNotification = false;
                            boolean flagCreditCard = false;
                            String cardBrand = null;
                            ClusterType cluster = ClusterType.ENIPAY;

                            UserProfile userProfile = UserProfile.getUserProfileForEventRefueling(fiscalCode, timestamp, surname, pv, product, flagPayment, credits, refuelQuantity, amount, refuelMode,
                                    flagNotification, flagCreditCard, cardBrand, cluster);

                            List<Response> responseBatch = crmAdapterService.executeBatchGetOffers(sessionID, fiscalCode, audienceID, InteractionPointType.REFUELING.getPoint(),
                                    userProfile.getParameters());

                            Response responseOffers = responseBatch.get(1);
                            result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS);
                            result.setStatusMessage("Service status code: " + responseOffers.getStatusCode());
                        }
                        catch (Exception ex) {
                            result.setStatusCode(ResponseHelper.SYSTEM_ERROR);
                            result.setStatusMessage("Errore nell'esecuzione del servizio di verifica connessione backend CRM: " + ex.getMessage());
                            System.err.println("Errore nell'esecuzione del servizio di verifica connessione backend CRM: " + ex.getMessage());
                        }
                        
                        break;
*/
                    case CRM_SF_NOTIFYEVENT:

                        try {
                            NotifyEventResponse notifyEventResult = new NotifyEventResponse();
                            String crmRequestId = new IdGenerator().generateId(16).substring(0, 32); 
                            DateFormat formatBirthDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
                            Date date = new Date();
                            Date birthDate = null;
                            try {
                                birthDate = formatBirthDate.parse("1980-01-01");
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            notifyEventResult = crmService.sendNotifySfEventOffer(
                                crmRequestId,
                                "9999999999999999", 
                                date,
                                "1",
                                "Gasolio",
                                Boolean.TRUE, 
                                new Integer(10),
                                new Double(10.0),
                                "Self",
                                "Test", 
                                "Test",
                                "test@test.test",
                                birthDate,
                                Boolean.TRUE, 
                                Boolean.TRUE,
                                "",
                                "1",
                                new Double(1.0), 
                                Boolean.TRUE,
                                Boolean.TRUE,
                                "3330000000", 
                                "0000000000000000000",
                                "4",
                                "", 
                                "",
                                "");
                            
                            result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS);
                            result.setStatusMessage("Service status code: " + notifyEventResult.getSuccess());
                        }
                        catch (Exception ex) {
                            result.setStatusCode(ResponseHelper.SYSTEM_ERROR);
                            result.setStatusMessage("Errore nell'esecuzione del servizio di verifica connessione backend CRM SF (notifyEvent): " + ex.getMessage());
                            System.err.println("Errore nell'esecuzione del servizio di verifica connessione backend CRM SF (notifyEvent): " + ex.getMessage());
                        }
                        
                        break;
                        
                    case CRM_SF_GETOFFERS:
                        try {
                            
                            GetOffersResult getOffersResult = new GetOffersResult();
                            String crmRequestId = new IdGenerator().generateId(16).substring(0, 32); 
                            Date date = new Date();
                            getOffersResult = crmService.getMissionsSf(
                                    crmRequestId,
                                    "9999999999999999",
                                    date, 
                                    Boolean.TRUE,
                                    Boolean.TRUE,
                                    "carta", 
                                    "cluster");
                            
                            result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS);
                            result.setStatusMessage("Service status code: " + getOffersResult.getSuccess());
                        }
                        catch (Exception ex) {
                            result.setStatusCode(ResponseHelper.SYSTEM_ERROR);
                            result.setStatusMessage("Errore nell'esecuzione del servizio di verifica connessione backend CRM SF (getOffers): " + ex.getMessage());
                            System.err.println("Errore nell'esecuzione del servizio di verifica connessione backend CRM SF (getOffers): " + ex.getMessage());
                        }
                        
                        break;

                    case DWH:
                        try {
                            String email = "test@email.com";
                            String password = "test";

                            DWHAdapterResult response = dwhAdapterService.checkLegacyPasswordPlus(email, password, requestId);
                            System.out.println("response DWH: " + response.getMessageCode() + " (" + response.getStatusCode() + ")");
                            result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS);
                            result.setStatusMessage("Service status code: " + response.getStatusCode());
                        }
                        catch (Exception ex) {
                            result.setStatusCode(com.techedge.mp.dwh.adapter.utilities.StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
                            result.setStatusMessage("Errore nell'esecuzione del servizio di verifica connessione backend di Serijakala: " + ex.getMessage());
                            System.err.println("Errore nell'esecuzione del servizio di verifica connessione backend di Serijakala: " + ex.getMessage());
                        }

                        break;
/*
                    case ENJOY:
                        try {
                            requestId = "ENJ-000000000000";
                            String srcTransactionID = "COMMUNICATION_SYSTEM";
                            String mpTransactionID = "0000000000000000000000000000000";
                            String mpTransactionStatus = "ERROR";
                            String startRefuel = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                            RefuelDetail refuelDetail = new RefuelDetail();
                            refuelDetail.setAmount(0.0);
                            refuelDetail.setTimestampStartRefuel(startRefuel);
                            refuelDetail.setTimestampEndRefuel(startRefuel);
                            refuelDetail.setAuthorizationCode("00000");
                            refuelDetail.setFuelQuantity(0.0);
                            refuelDetail.setFuelType("Gasolio");
                            refuelDetail.setProductID("GG");
                            refuelDetail.setProductDescription("Gasolio");

                            refuelingNotificationService = EJBHomeCache.getInstance().getRefuelingNotificationService();
                            String response = refuelingNotificationService.sendMPTransactionNotification(requestId, srcTransactionID, mpTransactionID, mpTransactionStatus, refuelDetail);
                            result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS);
                            
                            if (response.equals("SYSTEM_ERROR_500")) {
                                result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_FAILURE);
                            }
                            
                            result.setStatusMessage("Service status code: " + response);
                        }
                        catch (Exception ex) {
                            result.setStatusCode(ResponseHelper.SYSTEM_ERROR);
                            result.setStatusMessage("Errore nella creazione dell'istanza del servizio di refueling notification: " + ex.getMessage());
                            System.err.println("Errore nella creazione dell'istanza del servizio di refueling notification: " + ex.getMessage());
                        }
                        
                        break;
*/
                    case ENJOY2:
                        try {
                            requestId = "ENJ-000000000000";
                            String srcTransactionID = "COMMUNICATION_SYSTEM";
                            String mpTransactionID = "0000000000000000000000000000000";
                            String mpTransactionStatus = "ERROR";
                            String startRefuel = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                            RefuelDetail refuelDetail = new RefuelDetail();
                            refuelDetail.setAmount(0.0);
                            refuelDetail.setTimestampStartRefuel(startRefuel);
                            refuelDetail.setTimestampEndRefuel(startRefuel);
                            refuelDetail.setAuthorizationCode("00000");
                            refuelDetail.setFuelQuantity(0.0);
                            refuelDetail.setFuelType("Gasolio");
                            refuelDetail.setProductID("GG");
                            refuelDetail.setProductDescription("Gasolio");

                            //refuelingNotificationService = EJBHomeCache.getInstance().getRefuelingNotificationService();
                            RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2 = EJBHomeCache.getInstance().getRefuelingNotificationServiceOAuth2();
                            String response = refuelingNotificationServiceOAuth2.sendMPTransactionNotification(requestId, srcTransactionID, mpTransactionID, mpTransactionStatus, refuelDetail);
                            result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS);
                            
                            if (response.equals("SYSTEM_ERROR_500")) {
                                result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_FAILURE);
                            }
                            
                            result.setStatusMessage("Service status code: " + response);
                        }
                        catch (Exception ex) {
                            result.setStatusCode(ResponseHelper.SYSTEM_ERROR);
                            result.setStatusMessage("Errore nella creazione dell'istanza del servizio di refueling notification: " + ex.getMessage());
                            System.err.println("Errore nella creazione dell'istanza del servizio di refueling notification: " + ex.getMessage());
                        }
                        
                        break;
                        
                    case LOYALTY:
                        try {
                            String operationID = new IdGenerator().generateId(16).substring(0, 33);
                            PartnerType partnerType = PartnerType.MP;
                            VoucherConsumerType voucherConsumerType = VoucherConsumerType.ENI;

                            List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

                            VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                            voucherCodeDetail.setVoucherCode("0000000000");
                            voucherCodeList.add(voucherCodeDetail);

                            CheckVoucherResult checkVoucherResult = fidelityService.checkVoucher(operationID, voucherConsumerType, partnerType, requestTimestamp, voucherCodeList);
                            result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS);
                            result.setStatusMessage("Service status code: " + checkVoucherResult.getStatusCode());
                        }
                        catch (FidelityServiceException ex) {
                            result.setStatusCode(ResponseHelper.SYSTEM_ERROR);
                            result.setStatusMessage("Impossibile raggiungere il sistema di backend di Quenit: " + ex.getMessage());
                            System.err.println("Impossibile raggiungere il sistema di backend di Quenit: " + ex.getMessage());
                        }
                        
                        break;

                    case MYCICERO:
                        try {
                            String lang = "it-IT";
                            GetCitiesResult citiesResult = parkingService.getCities(lang);
                            
                            if (citiesResult.getStatusCode().equals(StatusCodeHelper.RETRIEVE_PARKING_ZONES_OK)) {
                                result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS);
                                result.setStatusMessage("Service status code: " + citiesResult.getStatusCode());
                            }
                            else {
                                result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_FAILURE);
                            }                            
                        }
                        catch (Exception ex) {
                            result.setStatusCode(ResponseHelper.SYSTEM_ERROR);
                            result.setStatusMessage("Impossibile raggiungere il sistema di backend di MyCicero: " + ex.getMessage());
                            System.err.println("Impossibile raggiungere il sistema di backend di MyCicero: " + ex.getMessage());
                        }
                        
                        break;
                        
                    default:
                        result.setStatusCode(ResponseHelper.ADMIN_SYSTEM_CHECK_FAILURE);
                        result.setStatusMessage("Invalid parameter remoteSystem");
                        break;
                }
                
                resultList.add(result);
            }
            
            userTransaction.commit();

            return resultList;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin remote system check with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}
