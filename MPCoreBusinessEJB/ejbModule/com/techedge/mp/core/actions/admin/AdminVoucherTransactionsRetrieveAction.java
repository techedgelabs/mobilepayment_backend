package com.techedge.mp.core.actions.admin;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveVoucherTransactionListData;
import com.techedge.mp.core.business.interfaces.voucher.VoucherTransaction;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminVoucherTransactionsRetrieveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminVoucherTransactionsRetrieveAction() {}

    public RetrieveVoucherTransactionListData execute(String adminTicketId, String requestId, String voucherTransactionId, Long userId, String voucherCode,
            Date creationTimestampStart, Date creationTimestampEnd, String finalStatusType, Integer maxResults) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            RetrieveVoucherTransactionListData retrieveVoucherTransactionListData = new RetrieveVoucherTransactionListData();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                retrieveVoucherTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_VOUCHER_TRANSACTIONS_INVALID_TICKET);

                return retrieveVoucherTransactionListData;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                retrieveVoucherTransactionListData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return retrieveVoucherTransactionListData;
            }

            // Se viene fornito il transactionID viene effettuata una ricerca mirata
            if (voucherTransactionId != null) {

                System.out.println("Ricerca per voucherTransactionId");

                VoucherTransactionBean voucherTransactionBean = QueryRepository.findVoucherTransactionBeanByID(em_crud, voucherTransactionId);
                List<VoucherTransaction> voucherTransactionList = new ArrayList<VoucherTransaction>(0);

                if (voucherTransactionBean != null) {

                    // Se i dati della transazione estratta corrispondono ai parametri di input, allora si inserisce nel risultato
                    if (AdminVoucherTransactionsRetrieveAction.matchParams(voucherTransactionBean, userId, voucherCode, creationTimestampStart, creationTimestampEnd,
                            finalStatusType)) {

                        VoucherTransaction transaction = voucherTransactionBean.toVoucherTransaction();
                        voucherTransactionList.add(transaction);
                    }
                }
                retrieveVoucherTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_VOUCHER_TRANSACTIONS_LIST_SUCCESS);
                retrieveVoucherTransactionListData.setVoucherTransactionList(voucherTransactionList);
            }
            else {

                if (creationTimestampStart == null) {
                    creationTimestampStart = new Timestamp(0);
                }
                if (creationTimestampEnd == null) {
                    creationTimestampEnd = new Timestamp(4102444800000l);
                }

                System.out.println("Ricerca con parametri");

                List<VoucherTransactionBean> voucherTransactionBeanList = QueryRepository.searchVoucherTransactions(em_crud, userId, voucherCode, creationTimestampStart,
                        creationTimestampEnd, finalStatusType, maxResults);
                List<VoucherTransaction> voucherTransactionList = new ArrayList<VoucherTransaction>(0);

                System.out.println("Ricerca con parametri ok");

                for (VoucherTransactionBean VoucherTransactionBean : voucherTransactionBeanList) {

                    VoucherTransaction transaction = VoucherTransactionBean.toVoucherTransaction();
                    voucherTransactionList.add(transaction);

                }

                retrieveVoucherTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_TRANSACTION_LIST_SUCCESS);
                retrieveVoucherTransactionListData.setVoucherTransactionList(voucherTransactionList);
            }

            userTransaction.commit();

            return retrieveVoucherTransactionListData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin retrieve activity log with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private static Boolean matchParams(VoucherTransactionBean voucherTransactionBean, Long userId, String voucherCode, Date creationTimestampStart, Date creationTimestampEnd,
            String finalStatusType) {

        Boolean match = true;

        if (match && (userId != null && voucherTransactionBean.getUserBean().getId() != userId)) {
            match = false;
        }
        if (match && (voucherCode != null && !voucherTransactionBean.getStationBean().getStationID().equals(voucherCode))) {
            match = false;
        }
        if (match && (finalStatusType != null && !voucherTransactionBean.getFinalStatusType().equals(finalStatusType))) {
            match = false;
        }
        if (match && (creationTimestampStart != null && voucherTransactionBean.getCreationTimestamp().getTime() < creationTimestampStart.getTime())) {
            match = false;
        }
        if (match && (creationTimestampEnd != null && voucherTransactionBean.getCreationTimestamp().getTime() > creationTimestampEnd.getTime())) {
            match = false;
        }

        return match;
    }

}
