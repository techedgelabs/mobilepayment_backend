package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.DataAcquirerBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminStationCreateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminStationCreateAction() {}

    public String execute(String adminTicketId, String requestId, Station station, String secretKey) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            String response = null;

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_ADD_STATION_INVALID_TICKET;

                return response;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            userTransaction.commit();

            userTransaction.begin();

            StationBean stationBean = QueryRepository.findStationBeanById(em_crud, station.getStationID());

            if (stationBean != null) {

                // Utente non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Station exists");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_ADD_STATION_FAILURE;

                return response;
            }

            stationBean = new StationBean();
            
            stationBean.setStationID(station.getStationID());
            stationBean.setAddress(station.getAddress());
            stationBean.setCity(station.getCity());
            stationBean.setProvince(station.getProvince());
            stationBean.setCountry(station.getCountry());
            stationBean.setLatitude(station.getLatitude());
            stationBean.setLongitude(station.getLongitude());
            stationBean.setNoOilAcquirerID(station.getNoOilAcquirerID());
            stationBean.setNoOilShopLogin(station.getNoOilShopLogin());
            stationBean.setOilAcquirerID(station.getOilAcquirerID());
            stationBean.setOilShopLogin(station.getOilShopLogin());
            stationBean.setStationStatus(station.getStationStatus());
            stationBean.setPostpaidActive(station.getPostpaidActive());
            stationBean.setPrepaidActive(station.getPrepaidActive());
            stationBean.setShopActive(station.getShopActive());
            stationBean.setNewAcquirerActive(station.getNewAcquirerActive());
            stationBean.setRefuelingActive(station.getRefuelingActive());
            stationBean.setLoyaltyActive(station.getLoyaltyActive());
            stationBean.setBusinessActive(station.getBusinessActive());
            stationBean.setVoucherActive(station.getVoucherActive());
            if (station.getDataAcquirer() != null) {
                
                if (station.getDataAcquirer().getApiKey()           != null ||
                    station.getDataAcquirer().getAcquirerID()       != null ||
                    station.getDataAcquirer().getCurrency()         != null ||
                    station.getDataAcquirer().getGroupAcquirer()    != null ||
                    station.getDataAcquirer().getEncodedSecretKey() != null) {
                    
                    EncryptionAES encryptionAES = new EncryptionAES();
                    encryptionAES.loadSecretKey(secretKey);
                    if (stationBean.getDataAcquirer() == null) {
                        stationBean.setDataAcquirer(new DataAcquirerBean());
                    }
                    if (station.getDataAcquirer().getApiKey() != null) {
                        stationBean.getDataAcquirer().setApiKey(station.getDataAcquirer().getApiKey());
                    }
                    if (station.getDataAcquirer().getAcquirerID() != null) {
                        stationBean.getDataAcquirer().setAcquirerID(station.getDataAcquirer().getAcquirerID());
                    }
                    if (station.getDataAcquirer().getCurrency() != null) {
                        stationBean.getDataAcquirer().setCurrency(station.getDataAcquirer().getCurrency());
                    }
                    if (station.getDataAcquirer().getGroupAcquirer() != null) {
                        stationBean.getDataAcquirer().setGroupAcquirer(station.getDataAcquirer().getGroupAcquirer());
                    }
                    if (station.getDataAcquirer().getEncodedSecretKey() != null) {
                        stationBean.getDataAcquirer().setEncodedSecretKey(encryptionAES.encrypt(station.getDataAcquirer().getEncodedSecretKey()));
                    }
                }
            }
            // Salva l'utente su db
            em_crud.persist(stationBean);

            em_crud.merge(adminTicketBean);

            userTransaction.commit();

            response = ResponseHelper.ADMIN_ADD_STATION_SUCCESS;

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin add station with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
