package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.QueryRepository;


public abstract class AdminBaseAction {

    @Resource
    protected EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    protected EntityManager em_crud;

    @EJB
    protected LoggerService loggerService;
    
    String requestId;
    
    AdminTicketBean adminTicketBean;
    
    /**
     * Chiamato a runtime dalla classe che lo implementa. Contiene l'effettiva implementazione
     * 
     * @param userTransaction
     * @param objects Lista di parametri per quella specifica chiamata
     * @return L'oggetto che vogliamo avere
     * @throws Exception
     */
    public abstract Object execute(UserTransaction userTransaction, String secretKey, Object...objects) throws Exception;
    
    
    /**
     * Punto di accesso comune per tutte le chiamate Admin
     * 
     * @param adminTicketId ticket dell'Admin
     * @param requestId richiesta 
     * @param objects lista di paramentri che serve alla classe concreta
     * @return
     */
    public Object commonExeceute(String adminTicketId, String requestId, String secretKey, Object...objects){
        
        UserTransaction userTransaction = context.getUserTransaction();
        setRequestId(requestId);
        
        try {
            userTransaction.begin();

            adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "commonExeceute", this.requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_INVALID_TICKET;
            }

            
           return execute(userTransaction, secretKey, objects);
            
        } 
        catch (Exception ex) {

            try {
                userTransaction.rollback();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin add voucher to promotion  (" + ex.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "commonExeceute", requestId, null, message );

            throw new EJBException(ex);
        }
    }

    protected String getRequestId() {
        return requestId;
    }

    protected void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    protected AdminTicketBean getAdminTicketBean() {
        return adminTicketBean;
    }

    protected void setAdminTicketBean(AdminTicketBean adminTicketBean) {
        this.adminTicketBean = adminTicketBean;
    }
    
    

}
