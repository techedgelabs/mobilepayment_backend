package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.MailingListBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminMailingListUpdateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminMailingListUpdateAction() {}

    public String execute(String adminTicketId, String requestID, Long id, String email, String name, Integer status, String template) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_MAILING_LIST_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            if (id == null && email == null && name == null && status == null && template == null) {

                // parametri non validi
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid parameters");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_MAILING_LIST_FAILURE;
            }

            // Estrazione dei dati da aggiornare
            List<MailingListBean> mailingListBeanToUpdate = new ArrayList<MailingListBean>(0);

            if (id != null) {

                MailingListBean mailingListBean = QueryRepository.findMailingListById(em, id);

                if (mailingListBean != null) {

                    mailingListBeanToUpdate.add(mailingListBean);
                }
            }
            else {

                mailingListBeanToUpdate = QueryRepository.findAllMailingList(em);
            }

            for (MailingListBean mailingListBean : mailingListBeanToUpdate) {

                System.out.println("Elaborazione id " + mailingListBean.getId());

                Boolean updateFound = false;

                if (email != null && !email.equals(mailingListBean.getEmail())) {
                    mailingListBean.setEmail(email);
                    updateFound = true;
                }

                if (name != null && !name.equals(mailingListBean.getName())) {
                    mailingListBean.setName(name);
                    updateFound = true;
                }

                if (status != null && status != mailingListBean.getStatus()) {
                    mailingListBean.setStatus(status);
                    updateFound = true;
                }

                if (template != null && !template.equals(mailingListBean.getTemplate())) {
                    mailingListBean.setTemplate(template);
                    updateFound = true;
                }

                if (updateFound) {

                    System.out.println("Aggiornamento id " + mailingListBean.getId());

                    mailingListBean.setTimestamp(new Date());
                    em.merge(mailingListBean);
                }
                else {

                    System.out.println("Nessuna modifica rilevata per id " + mailingListBean.getId());
                }

            }

            userTransaction.commit();

            return ResponseHelper.ADMIN_UPDATE_MAILING_LIST_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED updating mailing list with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}
