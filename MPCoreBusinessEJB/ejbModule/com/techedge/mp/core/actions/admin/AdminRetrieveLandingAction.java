package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminRetrieveDataResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LandingData;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveLandingResponse;
import com.techedge.mp.core.business.model.AdminBean;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.LandingDataBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminRetrieveLandingAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminRetrieveLandingAction() {}

    public RetrieveLandingResponse execute(String ticketId, String requestId) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        
        RetrieveLandingResponse retrieveLandingResponse = new RetrieveLandingResponse();
        
        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, ticketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                retrieveLandingResponse.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_LANDING_INVALID_TICKET);
                return retrieveLandingResponse;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                retrieveLandingResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return retrieveLandingResponse;
            }

            AdminBean adminBean = adminTicketBean.getAdminBean();
            if (adminBean == null) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                
                retrieveLandingResponse.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_LANDING_INVALID_TICKET);
                return retrieveLandingResponse;
            }


            List<LandingDataBean> landingDataBeanList = QueryRepository.findAllLandingPages(em);
            if (landingDataBeanList != null && !landingDataBeanList.isEmpty()) {
                for(LandingDataBean landingDataBean : landingDataBeanList) {
                    
                    LandingData landingData = landingDataBean.toLandingData();
                    if (landingDataBean.getUserCategoryBean() != null) {
                        landingData.setUserCategory(landingDataBean.getUserCategoryBean().toUserCategory());
                    }
                    else {
                        landingData.setUserCategory(null);
                    }
                    
                    retrieveLandingResponse.getLandingPages().add(landingData);
                }
            }

            retrieveLandingResponse.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_SUCCESS);

            // Rinnova il ticket
            adminTicketBean.renew();
            em.merge(adminTicketBean);

            userTransaction.commit();

            return retrieveLandingResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieving landing pages with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
