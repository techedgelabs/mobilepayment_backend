package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.EmailDomainBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminEmailDomainRetrieveInBlacklistAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public List<String> execute(String adminTicketId, String requestID, String email) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            List<EmailDomainBean> emailDomainBeanList = new ArrayList<EmailDomainBean>();
            List<String> emailDomainBeanListResult = new ArrayList<String>();

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return emailDomainBeanListResult;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return emailDomainBeanListResult;
            }

            // Verifica che non esista gi� un utente con quella email

            if (email == null) {
                emailDomainBeanList = QueryRepository.findAllEmailDomainBean(em);
            }
            else {
                EmailDomainBean emailDomainBean = QueryRepository.findEmailDomainByEmail(em, email);
                emailDomainBeanList.add(emailDomainBean);
            }

            if (emailDomainBeanList.size() == 0) {

                // Esiste gi� un utente con quella email
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Email exists");

                userTransaction.commit();

                return emailDomainBeanListResult;
            }
            else {
                for (EmailDomainBean item : emailDomainBeanList) {
                    emailDomainBeanListResult.add(item.getEmail());
                }

                userTransaction.commit();

                return emailDomainBeanListResult;
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED mail creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }

    }
}
