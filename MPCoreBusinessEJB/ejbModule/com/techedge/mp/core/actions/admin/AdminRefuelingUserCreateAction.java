package com.techedge.mp.core.actions.admin;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.AdminRoleEnum;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.EmailDomainBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminRefuelingUserCreateAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserCategoryService userCategoryService;

    public AdminRefuelingUserCreateAction() {}

    public String execute(String ticketId, String requestId, String username, String password) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, ticketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_CREATE_REFUELING_USER_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())
                    && !CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REFUELING_MANAGER)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            // Verifica che non esista gi� un utente con quella email
            UserBean oldUserBean = QueryRepository.findUserCustomerByEmail(em, username);

            if (oldUserBean != null) {

                // Esiste gi� un utente con quella email
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User exists");

                userTransaction.commit();

                return ResponseHelper.ADMIN_CREATE_REFUELING_USER_USER_EXISTS;
            }

            String firstname = "Refueling";
            String lastname = "Enjoy";
            String sex = "M";

            Calendar birthDate = new GregorianCalendar(1970, 01, 01);

            //CONTROLLO MAIL IN BLACKLIST
            String[] splits = username.split("@");

            EmailDomainBean emailDomainBean = null;
            if (splits[1] != null) {
                emailDomainBean = QueryRepository.findEmailDomainByEmail(em, splits[1]);
            }
            if (emailDomainBean != null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid mail");

                userTransaction.commit();

                return ResponseHelper.ADMIN_CREATE_REFUELING_USER_INVALID_MAIL;
            }

            User user = new User();
            user.setPersonalData(new PersonalData());
            user.getPersonalData().setFirstName(firstname);
            user.getPersonalData().setLastName(lastname);
            user.getPersonalData().setBirthDate(birthDate.getTime());
            user.getPersonalData().setBirthProvince("RM");
            user.getPersonalData().setSex(sex);
            user.getPersonalData().setBirthMunicipality("Roma");
            user.getPersonalData().setFiscalCode("XXXXXXXXXXXXXXXX");
            user.getPersonalData().setLanguage("IT");
            user.getPersonalData().setSecurityDataEmail(username);
            user.getPersonalData().setSecurityDataPassword(EncoderHelper.encode(password));
            user.setUserStatus(User.USER_STATUS_VERIFIED);
            user.setUserStatusRegistrationCompleted(Boolean.TRUE);
            user.setUserType(User.USER_TYPE_REFUELING_OAUTH2);
            user.setCapAvailable(0.1);
            user.setCapEffective(0.1);
            Calendar createDate = new GregorianCalendar();
            user.setCreateDate(createDate.getTime());
            user.setUseVoucher(Boolean.FALSE);
            user.setVirtualizationAttemptsLeft(5);

            UserBean userBean = new UserBean(user);

            // Salva l'utente su db
            em.persist(userBean);

            userTransaction.commit();

            return ResponseHelper.ADMIN_CREATE_REFUELING_USER_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
