package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.StationAdmin;
import com.techedge.mp.core.business.interfaces.StationsAdminData;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminStationRetrieveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminStationRetrieveAction() {}

    public StationsAdminData execute(String adminTicketId, String requestId, Station station, String secretKey) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            StationsAdminData stationsAdminDataResponse = new StationsAdminData();

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                stationsAdminDataResponse.setStatusCode(ResponseHelper.ADMIN_SEARCH_STATION_INVALID_TICKET);
                //response = ResponseHelper.ADMIN_SEARCH_STATION_INVALID_TICKET;

                return stationsAdminDataResponse;
            }
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                stationsAdminDataResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return stationsAdminDataResponse;
            }
            userTransaction.commit();
            userTransaction.begin();

            List<StationBean> stationBeanList = QueryRepository.searchStationBean(em_crud, station);

            List<StationAdmin> stationAdminListResponse = new ArrayList<StationAdmin>();

            StationAdmin stationAdmin;
            for (StationBean sa : stationBeanList) {

                stationAdmin = new StationAdmin();
                stationAdmin.setId(sa.getId());
                stationAdmin.setStationID(sa.getStationID());

                stationAdmin.setAddress(sa.getAddress());
                stationAdmin.setCity(sa.getCity());
                stationAdmin.setCountry(sa.getCountry());
                stationAdmin.setProvince(sa.getProvince());
                stationAdmin.setLatitude(sa.getLatitude());
                stationAdmin.setLongitude(sa.getLongitude());
                stationAdmin.setOilShopLogin(sa.getOilShopLogin());
                stationAdmin.setOilAcquirerID(sa.getOilAcquirerID());
                stationAdmin.setNoOilShopLogin(sa.getNoOilShopLogin());
                stationAdmin.setNoOilAcquirerID(sa.getNoOilAcquirerID());

                stationAdmin.setStationStatus(sa.getStationStatus());
                stationAdmin.setPrepaidActive(sa.getPrepaidActive());
                stationAdmin.setPostpaidActive(sa.getPostpaidActive());
                stationAdmin.setShopActive(sa.getShopActive());
                stationAdmin.setNewAcquirerActive(sa.getNewAcquirerActive());
                stationAdmin.setRefuelingActive(sa.getRefuelingActive());
                stationAdmin.setLoyaltyActive(sa.getLoyaltyActive());
                stationAdmin.setBusinessActive(sa.getBusinessActive());
                stationAdmin.setVoucherActive(sa.getVoucherActive());
                if (sa.getDataAcquirer() != null) {
                    EncryptionAES encryptionAES = new EncryptionAES();
                    encryptionAES.loadSecretKey(secretKey);

                    stationAdmin.setDataAcquirer(sa.getDataAcquirer().toDataAcquired());
                    stationAdmin.getDataAcquirer().setEncodedSecretKey(null);
                    if (sa.getDataAcquirer().getEncodedSecretKey() != null) {
                        stationAdmin.getDataAcquirer().setEncodedSecretKey(encryptionAES.decrypt(sa.getDataAcquirer().getEncodedSecretKey()));
                    }
                }
                stationAdminListResponse.add(stationAdmin);

            }

            stationsAdminDataResponse.setStatusCode(ResponseHelper.ADMIN_SEARCH_STATION_SUCCESS);
            stationsAdminDataResponse.setStationAdminList(stationAdminListResponse);
            em_crud.merge(adminTicketBean);
            userTransaction.commit();
            return stationsAdminDataResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin delete station with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
