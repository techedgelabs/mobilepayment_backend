package com.techedge.mp.core.actions.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.UnavailabilityPeriod;
import com.techedge.mp.core.business.interfaces.UnavailabilityPeriodResponse;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.UnavailabilityPeriodBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminBlockPeriodRetrieveAction {
    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;
    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    public AdminBlockPeriodRetrieveAction() {}

    public UnavailabilityPeriodResponse execute(String adminTicketId, String requestID, String code, Boolean active) throws EJBException {
        userTransaction = context.getUserTransaction();
        UnavailabilityPeriodResponse unavailabilityPeriodResponse = new UnavailabilityPeriodResponse();
        List<UnavailabilityPeriod> unavailabilityPeriodList = new ArrayList<UnavailabilityPeriod>(0);
        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();
                unavailabilityPeriodResponse.setStatusCode(ResponseHelper.ADMIN_BLOCK_PERIOD_RETRIEVE_INVALID_TICKET);
                return unavailabilityPeriodResponse;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                unavailabilityPeriodResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return unavailabilityPeriodResponse;
            }

            List<UnavailabilityPeriodBean> unavailabilityPeriodBeanList = new ArrayList<UnavailabilityPeriodBean>(0);
            List<UnavailabilityPeriodBean> unavailabilityPeriodBeanResultList = new ArrayList<UnavailabilityPeriodBean>(0);

            UnavailabilityPeriodBean unavailabilityPeriodBean = null;

            //Se non setto il codice da trovare
            if (code == null) {
                if (active == null) {
                    unavailabilityPeriodBeanList = QueryRepository.findAllUnavailabilityPeriods(em);
                }
                else {
                    unavailabilityPeriodBeanList = QueryRepository.findUnavailabilityPeriodsByActive(em, active);
                }
                //Se active � stato passato
                if ((unavailabilityPeriodBeanList != null && !unavailabilityPeriodBeanList.isEmpty()) && active != null) {
                    for (UnavailabilityPeriodBean item : unavailabilityPeriodBeanList) {
                        if (item.getActive() == active) {
                            unavailabilityPeriodBeanResultList.add(item);
                        }
                    }
                }
                else if (unavailabilityPeriodBeanList != null && !unavailabilityPeriodBeanList.isEmpty()) {
                    unavailabilityPeriodBeanResultList = unavailabilityPeriodBeanList;
                }
            }

            //Se ho settato il codice da trovare
            else {
                unavailabilityPeriodBean = QueryRepository.findUnavailabilityPeriodByCode(em, code);

                //Se non esiste quel codice
                if (unavailabilityPeriodBean == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Code exists");

                    userTransaction.commit();
                    unavailabilityPeriodResponse.setStatusCode(ResponseHelper.ADMIN_BLOCK_PERIOD_RETRIEVE_NOT_EXIST_CODE);
                    return unavailabilityPeriodResponse;
                }

                //Se esiste quel codice
                if (active != null && unavailabilityPeriodBean.getActive() == active) {
                    unavailabilityPeriodBeanResultList.add(unavailabilityPeriodBean);
                }
                else if (active == null) {
                    unavailabilityPeriodBeanResultList.add(unavailabilityPeriodBean);
                }
            }

            for (UnavailabilityPeriodBean item : unavailabilityPeriodBeanResultList) {
                UnavailabilityPeriod unavailabilityPeriod = new UnavailabilityPeriod();
                unavailabilityPeriod.setActive(item.getActive());
                unavailabilityPeriod.setCode(item.getCode());
                unavailabilityPeriod.setEndDate(item.getEndDate());
                unavailabilityPeriod.setEndTime(item.getEndTime());
                unavailabilityPeriod.setOperation(item.getOperation());
                unavailabilityPeriod.setStartDate(item.getStartDate());
                unavailabilityPeriod.setStartTime(item.getStartTime());
                unavailabilityPeriod.setStatusCode(item.getStatusCode());
                unavailabilityPeriod.setStatusMessage(item.getStatusMessage());
                unavailabilityPeriodList.add(unavailabilityPeriod);
            }

            unavailabilityPeriodResponse.setStatusCode(ResponseHelper.ADMIN_BLOCK_PERIOD_RETRIEVE_SUCCESS);
            unavailabilityPeriodResponse.setUnavailabilityPeriodList(unavailabilityPeriodList);
            userTransaction.commit();

            return unavailabilityPeriodResponse;

        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return unavailabilityPeriodResponse;

    }

    public boolean isThisDateValid(String dateToValidate, String dateFromat) {

        if (dateToValidate == null) {
            System.out.println("Data non valida");
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
        sdf.setLenient(false);

        try {
            Date date = sdf.parse(dateToValidate);
            System.out.println(date);
        }
        catch (ParseException e) {
            System.out.println("Data non valida");
            return false;
        }
        System.out.println("Data valida");
        return true;
    }

}