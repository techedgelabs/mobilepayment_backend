package com.techedge.mp.core.actions.admin;

import java.util.HashSet;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminRole;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminBean;
import com.techedge.mp.core.business.model.AdminRoleBean;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUpdateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminUpdateAction() {}

    public String execute(String ticketId, String requestId, Admin admin) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, ticketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_INVALID_TICKET;
            }

            AdminBean adminBean = adminTicketBean.getAdminBean();
            if (adminBean == null) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_INVALID_TICKET;
            }
            /*
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }
            */
            // Verifica lo stato dell'utente
            Integer adminStatus = adminBean.getStatus();
            if (adminStatus != Admin.ADMIN_STATUS_VERIFIED) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update admin in status " + adminStatus);

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_UNAUTHORIZED;
            }

            if (admin.getId() == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "ID is null");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_FAILURE;
            }

            AdminBean adminToUpdate = QueryRepository.findAdminByID(em, admin.getId());

            if (adminToUpdate == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "ID is not valid");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_FAILURE;
            }

            if (admin.getEmail() != null && !(admin.getEmail().isEmpty())) {
                adminToUpdate.setEmail(admin.getEmail());
            }

            if (admin.getStatus() != null
                    && (admin.getStatus() != Admin.ADMIN_STATUS_BLOCKED || admin.getStatus() != Admin.ADMIN_STATUS_NEW || admin.getStatus() != Admin.ADMIN_STATUS_VERIFIED)) {
                adminToUpdate.setEmail(admin.getEmail());
            }

            if (admin.getFirstName() != null && !(admin.getFirstName().isEmpty())) {
                adminToUpdate.setFirstname(admin.getFirstName());
            }

            if (admin.getLastName() != null && !(admin.getLastName().isEmpty())) {
                adminToUpdate.setLastname(admin.getLastName());
            }

            if (admin.getRoles() != null) {
                for (AdminRole item : admin.getRoles()) {
                    AdminRoleBean adminRoleBean = QueryRepository.findAdminRoleByName(em, item.getName());
                    if (adminRoleBean == null) {
                        // Esiste gi� un admin con quella email
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Role not valid");

                        userTransaction.commit();

                        return ResponseHelper.ADMIN_UPDATE_FAILURE;
                    }
                    adminToUpdate.setRoles(new HashSet<AdminRoleBean>(0));
                    adminToUpdate.getRoles().add(adminRoleBean);
                }
            }

            // Aggiorna i dati dell'utente
            em.merge(adminToUpdate);

            // Rinnova il ticket
            adminTicketBean.renew();
            em.merge(adminTicketBean);

            userTransaction.commit();

            return ResponseHelper.ADMIN_UPDATE_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
