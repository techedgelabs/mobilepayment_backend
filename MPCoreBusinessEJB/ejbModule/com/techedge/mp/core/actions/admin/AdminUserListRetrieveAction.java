package com.techedge.mp.core.actions.admin;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminRoleEnum;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.RetrieveUserListData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUserListRetrieveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminUserListRetrieveAction() {}

    public RetrieveUserListData execute(String adminTicketId, String requestId, Long id,
            String firstName, 					//LIKE |OPT
            String lastName,  					//LIKE |OPT
            String fiscalCode,					//LIKE |OPT
            String securityDataEmail,			//LIKE |OPT
            Integer userStatus,					//EQUALS |OPT
            String externalUserId,				//LIKE |OPT
            Double capAvailableMin, Double capAvailableMax, Double capEffectiveMin, Double capEffectiveMax, Date creationDateMin, Date creationDateMax, Integer maxResults,
            Double maxCapValue, Integer type) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            RetrieveUserListData retrieveUserListData = new RetrieveUserListData();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                retrieveUserListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_USER_LIST_INVALID_TICKET);

                return retrieveUserListData;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())
                    && !CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REFUELING_MANAGER)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                retrieveUserListData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return retrieveUserListData;
            }

            userTransaction.commit();

            if (id != null) {

                userTransaction.begin();

                UserBean userBean = QueryRepository.findUserById(em_crud, id);

                List<User> userList = new ArrayList<User>(0);

                if (userBean != null) {

                    User user = userBean.toUser();

                    userList.add(user);
                }

                retrieveUserListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_USER_LIST_SUCCESS);
                retrieveUserListData.setUserList(userList);
            }
            else {

                if (creationDateMin == null) {
                    creationDateMin = new Timestamp(0);
                }
                if (creationDateMax == null) {
                    creationDateMax = new Timestamp(4102444800000l);
                }

                if (capAvailableMin == null) {
                    capAvailableMin = 0.0;
                }

                if (capAvailableMax == null) {
                    capAvailableMax = maxCapValue;
                }

                if (capEffectiveMin == null) {
                    capEffectiveMin = 0.0;
                }

                if (capEffectiveMax == null) {
                    capEffectiveMax = maxCapValue;
                }
                List<UserBean> userBeanList = new ArrayList<UserBean>(0);
                userTransaction.begin();
                if (CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REFUELING_MANAGER)) {
                    List<UserBean> userBeanRefuelingList = QueryRepository.searchUser(em_crud, this.buildSearchString(lastName), this.buildSearchString(firstName), this.buildSearchString(fiscalCode),
                            this.buildSearchString(securityDataEmail), userStatus, externalUserId, capAvailableMin, capAvailableMax, capEffectiveMin, capEffectiveMax,
                            creationDateMin, creationDateMax, maxResults, User.USER_TYPE_REFUELING);
                    List<UserBean> userBeanRefuelingNewList = QueryRepository.searchUser(em_crud, this.buildSearchString(lastName), this.buildSearchString(firstName), this.buildSearchString(fiscalCode),
                            this.buildSearchString(securityDataEmail), userStatus, externalUserId, capAvailableMin, capAvailableMax, capEffectiveMin, capEffectiveMax,
                            creationDateMin, creationDateMax, maxResults, User.USER_TYPE_REFUELING_NEW_ACQUIRER);
                    List<UserBean> userBeanRefuelingOAuth2List = QueryRepository.searchUser(em_crud, this.buildSearchString(lastName), this.buildSearchString(firstName), this.buildSearchString(fiscalCode),
                            this.buildSearchString(securityDataEmail), userStatus, externalUserId, capAvailableMin, capAvailableMax, capEffectiveMin, capEffectiveMax,
                            creationDateMin, creationDateMax, maxResults, User.USER_TYPE_REFUELING_OAUTH2);
                    
                    userBeanList.addAll(userBeanRefuelingList);
                    userBeanList.addAll(userBeanRefuelingNewList);
                    userBeanList.addAll(userBeanRefuelingOAuth2List);
                }
                else {
                    userBeanList = QueryRepository.searchUser(em_crud, this.buildSearchString(lastName), this.buildSearchString(firstName), this.buildSearchString(fiscalCode),
                            this.buildSearchString(securityDataEmail), userStatus, externalUserId, capAvailableMin, capAvailableMax, capEffectiveMin, capEffectiveMax,
                            creationDateMin, creationDateMax, maxResults, type);

                }

                List<User> userList = new ArrayList<User>(0);

                for (UserBean userBean : userBeanList) {
                    User user = userBean.toUser();
                    userList.add(user);
                }

                retrieveUserListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_USER_LIST_SUCCESS);

                    retrieveUserListData.setUserList(userList);
            }

            userTransaction.commit();

            return retrieveUserListData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin retrieve activity log with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private String buildSearchString(String searchS) {

        if (searchS == null || searchS == "") {
            return "%";
        }
        else {
            return "%" + searchS + "%";
        }

    }
}
