package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminManagerUpdateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminManagerUpdateAction() {}

    public String execute(String adminTicketId, String requestId, Manager manager) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            // Seleziona il manager in base al suo id
            ManagerBean managerBean = QueryRepository.findManagerById(em, manager.getId());

            if (managerBean == null) {

                // Esiste gi� un manager con quella username
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Manager id does not exist");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_MANAGER_USER_NOT_FOUND;
            }
            else {
                // Verifica che non esista gi� un manager con quella username o email
                ManagerBean managerBean2 = QueryRepository.findManagerByUsernameOrEmail(em, manager.getUsername(), manager.getEmail());

                if (managerBean2 != null) {
                    if (managerBean2.getId() != manager.getId()) {

                        // Esiste gi� un manager con quella username
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Username or Email already exist");

                        userTransaction.commit();

                        return ResponseHelper.ADMIN_UPDATE_MANAGER_USERNAME_OR_EMAIL_EXIST;
                    }
                }
            }

            // Aggiorna i dati del manager
            managerBean.updateManager(manager);

            // Salva l'utente su db
            em.persist(managerBean);

            userTransaction.commit();

            return ResponseHelper.ADMIN_UPDATE_MANAGER_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED manager updating with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
