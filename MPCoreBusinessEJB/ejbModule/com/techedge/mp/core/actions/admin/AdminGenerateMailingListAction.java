package com.techedge.mp.core.actions.admin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MailingList;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.MailingListBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminGenerateMailingListAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminGenerateMailingListAction() {}

    public String execute(String adminTicketId, String requestID, String selectionType) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_GENERATE_MAILING_LIST_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            if (selectionType == null || selectionType.equals("")) {

                // selection type non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid selection type");

                userTransaction.commit();

                return ResponseHelper.ADMIN_GENERATE_MAILING_LIST_FAILURE;
            }

            if (selectionType.equals("REGISTRATION_COMPLETED")) {

                System.out.println("Inizializzazione tabella MAILING_LIST");

                List<MailingListBean> mailingListBeanList = QueryRepository.findAllMailingList(em);

                for (MailingListBean mailingListBean : mailingListBeanList) {

                    em.remove(mailingListBean);
                }

                System.out.println("Inizio inserimento righe in tabella MAILING_LIST");

                // Estrai tutti gli utenti da inserire nella lista
                List<UserBean> userBeanList = QueryRepository.findCustomerUserWithRegistrationCompleted(em);

                Integer count = 0;

                for (UserBean userBean : userBeanList) {

                    String email = userBean.getPersonalDataBean().getSecurityDataEmail();
                    String name = userBean.getPersonalDataBean().getFirstName();
                    String template = "NEW_APP_VERSION";
                    Date timestamp = new Date();

                    MailingListBean mailingListBean = new MailingListBean();
                    mailingListBean.setEmail(email);
                    mailingListBean.setName(name);
                    mailingListBean.setStatus(MailingList.MAILING_LIST_PENDING);
                    mailingListBean.setTemplate(template);
                    mailingListBean.setTimestamp(timestamp);
                    em.persist(mailingListBean);

                    count++;
                }

                System.out.println("Inseriti " + count + " utenti nella tabella MAILING_LIST");

            }
            else if (selectionType.equals("NOT_MIGRATED")) {

                System.out.println("Inizializzazione tabella MAILING_LIST");

                List<MailingListBean> mailingListBeanList = QueryRepository.findAllMailingList(em);

                for (MailingListBean mailingListBean : mailingListBeanList) {

                    em.remove(mailingListBean);
                }

                System.out.println("Inizio inserimento righe in tabella MAILING_LIST");

                // Estrai tutti gli utenti di tipo customer con flag di registrazione completata a true
                String dateString = "2016-11-23 08:00:00";
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ITALIAN);
                Date createDateLimit = format.parse(dateString);

                List<UserBean> userBeanList = QueryRepository.findCustomerUserNotMigrated(em, createDateLimit);

                Integer count = 0;

                for (UserBean userBean : userBeanList) {

                    String email = userBean.getPersonalDataBean().getSecurityDataEmail();
                    String name = userBean.getPersonalDataBean().getFirstName();
                    String template = "NEW_APP_VERSION";
                    Date timestamp = new Date();

                    MailingListBean mailingListBean = new MailingListBean();
                    mailingListBean.setEmail(email);
                    mailingListBean.setName(name);
                    mailingListBean.setStatus(MailingList.MAILING_LIST_PENDING);
                    mailingListBean.setTemplate(template);
                    mailingListBean.setTimestamp(timestamp);
                    em.persist(mailingListBean);

                    count++;
                }

                System.out.println("Inseriti " + count + " utenti nella tabella MAILING_LIST");

            }
            else if (selectionType.equals("CLEAR_LIST")) {

                System.out.println("Inizializzazione tabella MAILING_LIST");

                List<MailingListBean> mailingListBeanList = QueryRepository.findAllMailingList(em);

                for (MailingListBean mailingListBean : mailingListBeanList) {

                    em.remove(mailingListBean);
                }
            }
            else {

                // selection type inesistente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Selection type not found");

                userTransaction.commit();

                return ResponseHelper.ADMIN_GENERATE_MAILING_LIST_FAILURE;
            }

            userTransaction.commit();

            return ResponseHelper.ADMIN_GENERATE_MAILING_LIST_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED creating mailing list with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}
