package com.techedge.mp.core.actions.admin;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminBean;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminAuthenticationAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public AdminAuthenticationAction() {
    }
    
    
    public AdminAuthenticationResponse execute(
    		String email,
			String password,
			String requestId,
			Integer ticketExpiryTime
			) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		AdminAuthenticationResponse adminAuthenticationResponse = null;
	    	
	    	AdminBean adminBean = null;
			
			// Controlla se esiste un amministratore con username e password inseriti
	    	adminBean = QueryRepository.findAdminByEmail(em, email);
			
			if (adminBean == null) {
				
				// Se l'amministratore non esiste resitituisci il messaggio di errore
				this.loggerService.log(ErrorLevel.ADMIN, this.getClass().getSimpleName(), "execute", requestId, null, "Admin " + email + " not found");
				adminAuthenticationResponse = new AdminAuthenticationResponse();
				adminAuthenticationResponse.setStatusCode(ResponseHelper.ADMIN_AUTH_LOGIN_ERROR);
				
				userTransaction.commit();
				
				return adminAuthenticationResponse;
			}
			else {
				
				// Verifica lo stato dell'amministratore
				if ( !adminBean.canLogin() ) {
					
					// Un amministratore che si trova in questo stato non pu� effettuare il login
					this.loggerService.log(ErrorLevel.ADMIN, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: Admin " + email);
					adminAuthenticationResponse = new AdminAuthenticationResponse();
					adminAuthenticationResponse.setStatusCode(ResponseHelper.ADMIN_AUTH_UNAUTHORIZED);
					
					userTransaction.commit();
					
					return adminAuthenticationResponse;
				}
				
				if ( !adminBean.isPasswordValid(password) ) {
					
					// La password inserita non � corretta
					this.loggerService.log(ErrorLevel.ADMIN, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong password for admin " + email );
					adminAuthenticationResponse = new AdminAuthenticationResponse();
					adminAuthenticationResponse.setStatusCode(ResponseHelper.ADMIN_AUTH_LOGIN_ERROR);
					
					userTransaction.commit();
					
					return adminAuthenticationResponse;
				}
				else {
				
					// Crea un nuovo ticket
					Admin admin = adminBean.toAdmin();
					
					Date now = new Date();
					Date lastUsed = now;
					Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);
					
					String newTicketId = new IdGenerator().generateId(16).substring(0, 32);
					
					AdminTicketBean adminTicketBean = new AdminTicketBean(
							newTicketId,
							adminBean,
							AdminTicketBean.TICKET_TYPE_ADMIN,
							now,
							lastUsed,
							expiryDate);
					
					em.persist(adminTicketBean);
					
					// 4) restituisci il risultato
					adminAuthenticationResponse = new AdminAuthenticationResponse();
					
					adminAuthenticationResponse.setStatusCode(ResponseHelper.ADMIN_AUTH_SUCCESS);
					adminAuthenticationResponse.setTicketId(adminTicketBean.getTicketId());
					adminAuthenticationResponse.setAdmin(admin);
					
					userTransaction.commit();
					
					return adminAuthenticationResponse;
				}
			}
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED admin authentication with message (" + ex2.getMessage() + ")";
    		this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
