package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.AdminService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GenerateTestersData;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TesterUser;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserDevice;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminGenerateTestersAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminGenerateTestersAction() {}

    public GenerateTestersData execute(String adminTicketId, String requestID, String emailPrefix, String emailSuffix, Integer startNumber, Integer userCount, String token,
            Integer userType, AdminService adminService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                GenerateTestersData generateTestersData = new GenerateTestersData();
                generateTestersData.setStatusCode(ResponseHelper.ADMIN_GENERATE_TESTERS_INVALID_TICKET);
                return generateTestersData;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                GenerateTestersData generateTestersData = new GenerateTestersData();

                generateTestersData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                
                userTransaction.commit();
                
                return generateTestersData;
            }
            
            userTransaction.commit();

            TesterUser testerUser;
            List<TesterUser> testerUserList = new ArrayList<TesterUser>(0);

            String email;
            String password;
            String pin;
            int i_pin = 0;
            String response;
            for (int i = startNumber; i < (startNumber + userCount); i++) {

                email = emailPrefix + String.format("%09d", i) + emailSuffix;
                password = String.format("%09d", i);
                pin = String.format("%04d", i_pin++);
                if (i_pin == 9999)
                    i = 0;

                response = adminService.adminCustomerUserCreate(adminTicketId, requestID, this.compileUser(email, password, token, EncoderHelper.encode(pin), userType));

                testerUser = new TesterUser();
                testerUser.setEmail(email);
                testerUser.setPassword(password);
                testerUser.setPin(pin);
                testerUser.setEncryptedPassword(EncoderHelper.encode(password));
                testerUser.setEncryptedPin(EncoderHelper.encode(pin));
                if (response.contains("200")) {
                    testerUser.setGenerated(true);
                }
                else {
                    testerUser.setGenerated(false);
                }
                testerUser.setResponse(response);

                testerUserList.add(testerUser);

            }

            //userTransaction.commit();

            GenerateTestersData generateTestersData = new GenerateTestersData();
            generateTestersData.setTesterList(testerUserList);
            generateTestersData.setStatusCode(ResponseHelper.ADMIN_GENERATE_TESTERS_SUCCESS);

            return generateTestersData;
        }
        catch (Exception ex2) {

            //			try {
            //				userTransaction.rollback();
            //			} catch (IllegalStateException e) {
            //				// TODO Auto-generated catch block
            //				e.printStackTrace();
            //			} catch (SecurityException e) {
            //				// TODO Auto-generated catch block
            //				e.printStackTrace();
            //			} catch (SystemException e) {
            //				// TODO Auto-generated catch block
            //				e.printStackTrace();
            //			}

            String message = "FAILED testers generation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
  
    private User compileUser(String email, String password, String token, String pin, Integer userType) {
        User user = new User();
        user.setUserType(userType);
        user.setUserStatusRegistrationCompleted(true);
        user.setVirtualizationCompleted(true);
        user.setUserStatus(User.USER_STATUS_VERIFIED);
        user.setOldUser(false);

        PersonalData personalData = new PersonalData();

        personalData.setFirstName("TEST_Name");
        personalData.setLastName("TEST_Surname");
        personalData.setFiscalCode("XXXXXXXXXXXXXXXX");
        personalData.setBirthDate(new Date());
        personalData.setBirthMunicipality("TEST_birthdate");
        personalData.setBirthProvince("TEST_birthprovince");
        personalData.setLanguage("IT");
        personalData.setSex("M");
        personalData.setSecurityDataEmail(email);
        personalData.setSecurityDataPassword(password);

        personalData.setAddress(null);
        personalData.setBillingAddress(null);

        user.setPersonalData(personalData);

        Set<PaymentInfo> paymentInfoList = new HashSet<PaymentInfo>(0);
        PaymentInfo paymentInfoCreditCard = new PaymentInfo();
        paymentInfoCreditCard.setDefaultMethod(true);
        paymentInfoCreditCard.setType(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD);
        paymentInfoCreditCard.setPan("PAN");
        paymentInfoCreditCard.setToken(token);
        paymentInfoCreditCard.setPin(pin);
        paymentInfoCreditCard.setInsertTimestamp(new Date());
        paymentInfoCreditCard.setPinCheckAttemptsLeft(5);
        paymentInfoCreditCard.setStatus(2);
        paymentInfoList.add(paymentInfoCreditCard);

        PaymentInfo paymentInfoCreditVoucher = new PaymentInfo();
        paymentInfoCreditVoucher.setDefaultMethod(false);
        paymentInfoCreditVoucher.setType(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER);
        paymentInfoCreditVoucher.setPin(pin);
        paymentInfoCreditVoucher.setInsertTimestamp(new Date());
        paymentInfoCreditVoucher.setPinCheckAttemptsLeft(5);
        paymentInfoCreditVoucher.setStatus(2);
        paymentInfoList.add(paymentInfoCreditVoucher);
        
        user.setPaymentData(paymentInfoList);
        
        MobilePhone mobilePhone = new MobilePhone();
        mobilePhone.setCreationTimestamp(new Date());
        mobilePhone.setExpirationTimestamp(new Date());
        mobilePhone.setLastUsedTimestamp(new Date());
        mobilePhone.setNumber("300000000000");
        mobilePhone.setPrefix("+39");
        mobilePhone.setStatus(1);
        mobilePhone.setVerificationCode("11111111");
        
        user.getMobilePhoneList().add(mobilePhone);
        
        UserDevice userDevice = new UserDevice();
        userDevice.setAssociationTimestamp(new Date());
        userDevice.setCreationTimestamp(new Date());
        userDevice.setDeviceEndpoint("");
        userDevice.setDeviceFamily("");
        userDevice.setDeviceId("");
        userDevice.setDeviceName("");
        userDevice.setDeviceToken("");
        userDevice.setStatus(3);
        userDevice.setVerificationCode("11111111");
        
        user.getUserDeviceList().add(userDevice);

        return user;

    }
}
