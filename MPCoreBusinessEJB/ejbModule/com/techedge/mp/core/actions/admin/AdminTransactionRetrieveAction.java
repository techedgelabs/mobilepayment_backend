package com.techedge.mp.core.actions.admin;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveTransactionListData;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionHistory;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminTransactionRetrieveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminTransactionRetrieveAction() {}

    public RetrieveTransactionListData execute(String adminTicketId, String requestId, String transactionId, Long userId, String stationId, String pumpId, String finalStatusType,
            Boolean GFGNotification, Boolean confirmed, Date creationTimestampStart, Date creationTimestampEnd, String productID, Integer maxResults, Boolean transactionHistoryFlag)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            RetrieveTransactionListData retrieveTransactionListData = new RetrieveTransactionListData();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                retrieveTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_TRANSACTION_LIST_INVALID_TICKET);

                return retrieveTransactionListData;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                retrieveTransactionListData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return retrieveTransactionListData;
            }

            userTransaction.commit();

            // Se viene fornito il transactionID viene effettuata una ricerca mirata
            if (transactionId != null) {

                System.out.println("Ricerca per transactionID");

                userTransaction.begin();

                TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em_crud, transactionId);

                List<TransactionHistory> transactionHistoryList = new ArrayList<TransactionHistory>(0);

                if (transactionBean != null) {

                    // Se i dati della transazione estratta corrispondono ai parametri di input, allora si inserisce nel risultato
                    if (AdminTransactionRetrieveAction.matchParams(transactionBean, userId, stationId, pumpId, finalStatusType, GFGNotification, confirmed, creationTimestampStart,
                            creationTimestampEnd)) {

                        Transaction transaction = transactionBean.toTransaction();
                        transactionHistoryList.add(TransactionHistory.moveTransaction(transaction));
                    }
                }
                else {

                    System.out.println("Ricerca per transactionID in history");

                    TransactionHistoryBean transactionHistoryBean = QueryRepository.findTransactionHistoryBeanById(em_crud, transactionId);

                    if (transactionHistoryBean != null) {

                        // Se i dati della transazione estratta corrispondono ai parametri di input, allora si inserisce nel risultato
                        if (AdminTransactionRetrieveAction.matchParams2(transactionHistoryBean, userId, stationId, pumpId, finalStatusType, GFGNotification, confirmed,
                                creationTimestampStart, creationTimestampEnd)) {

                            TransactionHistory transactionHistory = transactionHistoryBean.toTransactionHistory();
                            transactionHistoryList.add(transactionHistory);
                        }
                    }
                }

                retrieveTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_TRANSACTION_LIST_SUCCESS);
                retrieveTransactionListData.setTransactionHistoryList(transactionHistoryList);
            }
            else {

                if (creationTimestampStart == null) {
                    creationTimestampStart = new Timestamp(0);
                }
                if (creationTimestampEnd == null) {
                    creationTimestampEnd = new Timestamp(4102444800000l);
                }

                userTransaction.begin();

                System.out.println("Ricerca con parametri");

                List<TransactionHistory> transactionHistoryList = new ArrayList<TransactionHistory>(0);

                List<TransactionBean> transactionBeanList = QueryRepository.searchTransactions(em_crud, userId, stationId, this.buildSearchString(pumpId),
                        this.buildSearchString(finalStatusType), GFGNotification, confirmed, creationTimestampStart, creationTimestampEnd, this.buildSearchString(productID),
                        maxResults);

                System.out.println("Ricerca con parametri ok");

                for (TransactionBean transactionBean : transactionBeanList) {

                    Transaction transaction = transactionBean.toTransaction();
                    transactionHistoryList.add(TransactionHistory.moveTransaction(transaction));
                }

                if (transactionHistoryFlag) {
                    System.out.println("Ricerca con parametri in history richiesta");
                    List<TransactionHistoryBean> transactionHistoryBeanList = QueryRepository.searchTransactionsHistory(em_crud, userId, stationId, this.buildSearchString(pumpId),
                            this.buildSearchString(finalStatusType), GFGNotification, confirmed, creationTimestampStart, creationTimestampEnd, this.buildSearchString(productID),
                            maxResults);
                    System.out.println("Ricerca con parametri in history positiva");

                    for (TransactionHistoryBean transactionHistoryBean : transactionHistoryBeanList) {
                        transactionHistoryList.add(transactionHistoryBean.toTransactionHistory());
                    }
                }
                else {
                    System.out.println("Ricerca con parametri in history non richiesta");
                }

                retrieveTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_TRANSACTION_LIST_SUCCESS);
                retrieveTransactionListData.setTransactionHistoryList(transactionHistoryList);
            }

            userTransaction.commit();

            return retrieveTransactionListData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin retrieve activity log with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private String buildSearchString(String searchS) {

        if (searchS == null || searchS == "") {
            return "%";
        }
        else {
            return "%" + searchS + "%";
        }

    }

    private static Boolean matchParams(TransactionBean transactionBean, Long userId, String stationId, String pumpId, String finalStatusType, Boolean GFGNotification,
            Boolean confirmed, Date creationTimestampStart, Date creationTimestampEnd) {

        Boolean match = true;

        if (match && (userId != null && transactionBean.getUserBean().getId() != userId)) {
            match = false;
        }
        if (match && (stationId != null && !transactionBean.getStationBean().getStationID().equals(stationId))) {
            match = false;
        }
        if (match && (pumpId != null && !transactionBean.getPumpID().equals(pumpId))) {
            match = false;
        }
        if (match && (finalStatusType != null && !transactionBean.getFinalStatusType().equals(finalStatusType))) {
            match = false;
        }
        if (match && (GFGNotification != null && !transactionBean.getGFGNotification() != GFGNotification)) {
            match = false;
        }
        if (match && (confirmed != null && !transactionBean.getConfirmed() != confirmed)) {
            match = false;
        }
        if (match && (creationTimestampStart != null && transactionBean.getCreationTimestamp().getTime() < creationTimestampStart.getTime())) {
            match = false;
        }
        if (match && (creationTimestampEnd != null && transactionBean.getCreationTimestamp().getTime() > creationTimestampEnd.getTime())) {
            match = false;
        }

        return match;
    }

    private static Boolean matchParams2(TransactionHistoryBean transactionHistoryBean, Long userId, String stationId, String pumpId, String finalStatusType,
            Boolean GFGNotification, Boolean confirmed, Date creationTimestampStart, Date creationTimestampEnd) {

        Boolean match = true;

        if (match && (userId != null && transactionHistoryBean.getUserBean().getId() != userId)) {
            match = false;
        }
        if (match && (stationId != null && !transactionHistoryBean.getStationBean().getStationID().equals(stationId))) {
            match = false;
        }
        if (match && (pumpId != null && !transactionHistoryBean.getPumpID().equals(pumpId))) {
            match = false;
        }
        if (match && (finalStatusType != null && !transactionHistoryBean.getFinalStatusType().equals(finalStatusType))) {
            match = false;
        }
        if (match && (GFGNotification != null && !transactionHistoryBean.getGFGNotification() != GFGNotification)) {
            match = false;
        }
        if (match && (confirmed != null && !transactionHistoryBean.getConfirmed() != confirmed)) {
            match = false;
        }
        if (match && (creationTimestampStart != null && transactionHistoryBean.getCreationTimestamp().getTime() < creationTimestampStart.getTime())) {
            match = false;
        }
        if (match && (creationTimestampEnd != null && transactionHistoryBean.getCreationTimestamp().getTime() > creationTimestampEnd.getTime())) {
            match = false;
        }

        return match;
    }
}
