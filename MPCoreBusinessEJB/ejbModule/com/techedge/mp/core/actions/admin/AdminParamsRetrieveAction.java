package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ParamInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveParamsData;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.ParameterBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminParamsRetrieveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminParamsRetrieveAction() {}

    public RetrieveParamsData execute(String adminTicketId, String requestId, String param) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            RetrieveParamsData retrieveParamsData = new RetrieveParamsData();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                retrieveParamsData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_PARAMS_INVALID_TICKET);

                return retrieveParamsData;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                retrieveParamsData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return retrieveParamsData;
            }

            //userTransaction.commit();

            List<ParameterBean> parameterBeanList = QueryRepository.findParameterBeanByName(em_crud, this.buildSearchString(param));
            //List <TicketBean> ticketBeanList = QueryRepository.findTicketsActive(em_crud);

            List<ParamInfo> paramInfoList = new ArrayList<ParamInfo>(0);

            ParamInfo paramInfo;
            for (ParameterBean pb : parameterBeanList) {
                paramInfo = new ParamInfo();
                paramInfo.setKey(pb.getName());
                paramInfo.setValue(pb.getValue());
                paramInfoList.add(paramInfo);
            }

            retrieveParamsData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_PARAMS_SUCCESS);
            retrieveParamsData.setParamsList(paramInfoList);

            userTransaction.commit();

            return retrieveParamsData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin retrieve activity log with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private String buildSearchString(String searchS) {

        if (searchS == null || searchS == "") {
            return "%";
        }
        else {
            return "%" + searchS + "%";
        }

    }

}
