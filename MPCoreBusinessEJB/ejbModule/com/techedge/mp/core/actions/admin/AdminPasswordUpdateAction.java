package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminBean;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminPasswordUpdateAction {

    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;

    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    public AdminPasswordUpdateAction() {}

    public String execute(String adminTicketId, String requestID, String oldPassword, String newPassword) throws EJBException {
        userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_PASSWORD_INVALID_TICKET;
            }

            AdminBean adminBean = adminTicketBean.getAdminBean();
            if (adminBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not found");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_INVALID_TICKET;
            }

            if (adminBean.getEmail().equals(newPassword)) {

                // La nuova password coincide con l'email dell'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "New password = email");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_PASSWORD_USERNAME_PASSWORD_EQUALS;
            }

            if (!adminBean.getPassword().equals(oldPassword)) {

                // La old password non coincide con quella dell'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Old password wrong");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_PASSWORD_OLD_PASSWORD_WRONG;

            }

            // Codifica la password
            String encodedPassword = EncoderHelper.encode(newPassword);
            if (adminBean.getEmail().equals(encodedPassword)) {

                // La nuova password coincide con quella vecchia
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "New password = old password");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_PASSWORD_FAILURE;
            }

            // Aggiorna la password
            adminBean.setPassword(encodedPassword);

            // Aggiorna i dati dell'utente
            em.merge(adminBean);

            // Rinnova il ticket
            adminTicketBean.renew();
            em.merge(adminTicketBean);

            userTransaction.commit();

            return ResponseHelper.ADMIN_UPDATE_PASSWORD_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED password update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}
