package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MappingErrorData;
import com.techedge.mp.core.business.interfaces.RefuelingMappingError;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.RefuelingMappingErrorBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminErrorRetriveAction {
    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;
    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    public AdminErrorRetriveAction() {}

    public MappingErrorData execute(String adminTicketId, String requestID, String errorCode) throws EJBException {
        userTransaction = context.getUserTransaction();
        MappingErrorData refuelingMappingError = new MappingErrorData();
        List<RefuelingMappingErrorBean> refuelingMappingErrorBeanList = new ArrayList<RefuelingMappingErrorBean>(0);
        List<RefuelingMappingError> refuelingMappingErrorList = new ArrayList<RefuelingMappingError>(0);

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");
                refuelingMappingError.setStatusCode(ResponseHelper.ADMIN_MAPPING_ERROR_GET__INVALID_TICKET);
                userTransaction.commit();

                return refuelingMappingError;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                refuelingMappingError.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                userTransaction.commit();

                return refuelingMappingError;
            }

            if (errorCode != null) {
                RefuelingMappingErrorBean refuelingMappingErrorBean = QueryRepository.findErrorRefuelingByErrorCode(em, errorCode);
                if (refuelingMappingErrorBean != null) {
                    refuelingMappingErrorBeanList.add(refuelingMappingErrorBean);
                }
            }
            else {
                refuelingMappingErrorBeanList.addAll(QueryRepository.findAllErrorRefueling(em));
            }

            for (RefuelingMappingErrorBean refuelingMappingErrorItem : refuelingMappingErrorBeanList) {
                RefuelingMappingError item = new RefuelingMappingError();
                item.setErrorCode(refuelingMappingErrorItem.getErrorCode());
                item.setStatusCode(refuelingMappingErrorItem.getStatusCode());
                refuelingMappingErrorList.add(item);
            }
            refuelingMappingError.setRefuelingMappingError(refuelingMappingErrorList);
            userTransaction.commit();
        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return refuelingMappingError;
    }

}