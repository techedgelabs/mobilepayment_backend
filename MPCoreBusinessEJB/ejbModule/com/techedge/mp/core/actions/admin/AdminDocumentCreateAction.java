package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.DocumentAttribute;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminDocumentCreateAction {
    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;
    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    public AdminDocumentCreateAction() {}

    public String execute(String adminTicketId, String requestID, String documentKey, Integer position, String templateFile, String title, String subtitle, List<DocumentAttribute> attributes,
            String userCategory, String groupCategory) throws EJBException {
        userTransaction = context.getUserTransaction();
        String result = "";

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_DOCUMENT_CREATE_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            DocumentBean document = QueryRepository.findDocumentByKey(em, documentKey);

            if (document != null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Documento gi� esistente");

                userTransaction.commit();

                return ResponseHelper.ADMIN_DOCUMENT_CREATE_CHECK_FAILURE;
            }
            document = new DocumentBean();
            document.setDocumentkey(documentKey);
            document.setPosition(position);

            if (templateFile == null) {
                document.setTemplateFile("");
            }
            else {
                document.setTemplateFile(templateFile);
            }

            if (title == null) {
                document.setTitle("");
            }
            else {
                document.setTitle(title);
            }

            if (subtitle == null) {
                document.setSubTitle("");
            }
            else {
                document.setSubTitle(subtitle);
            }
            
            if (attributes != null && !attributes.isEmpty()) {
                List<DocumentAttributeBean> listDocumentAttributeBean = new ArrayList<DocumentAttributeBean>(0);
                for (DocumentAttribute item : attributes) {
                    DocumentAttributeBean doc = new DocumentAttributeBean();
                    doc.setCheckKey(item.getCheckKey());
                    doc.setConditionText(item.getConditionText());
                    doc.setExtendedConditionText(item.getExtendedConditionText());
                    doc.setMandatory(item.getMandatory());
                    doc.setPosition(item.getPosition());
                    em.persist(doc);
                    listDocumentAttributeBean.add(doc);
                }
                document.setDocumentCheck(listDocumentAttributeBean);

            }

            if (userCategory != null) {
                document.setUserCategory(userCategory);
            }
            
            if (groupCategory != null) {
                document.setGroupCategory(groupCategory);
            }

            em.persist(document);
            userTransaction.commit();

            return ResponseHelper.ADMIN_DOCUMENT_CREATE_SUCCESS;

        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }
}