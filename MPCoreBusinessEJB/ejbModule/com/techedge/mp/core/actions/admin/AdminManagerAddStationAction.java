package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminManagerAddStationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminManagerAddStationAction() {}

    public String execute(String adminTicketId, String requestId, Long managerId, Long stationId) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ADDSTATION_MANAGER_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            ManagerBean managerBean = QueryRepository.findManagerById(em, managerId);

            if (managerBean == null) {

                // Recupero il manager tramite la sua ID
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Manager id not found");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ADDSTATION_MANAGER_MANAGERID_NOT_FOUND;
            }

            if (managerBean.getStations() != null && !managerBean.getStations().isEmpty()) {
                for (StationBean stationBean : managerBean.getStations()) {
                    if (stationBean.getId() == stationId.longValue()) {
                        // Recupero il manager tramite la sua ID
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Manager id not found");

                        userTransaction.commit();

                        return ResponseHelper.ADMIN_ADDSTATION_MANAGER_STATION_ALREADY_ADDED;
                    }
                }
            }

            StationBean stationBean = QueryRepository.findStationBeanByPrimaryKey(em, stationId);

            if (stationBean == null) {

                // Recupero il manager tramite la sua ID
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Station id not found");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ADDSTATION_MANAGER_STATIONID_NOT_FOUND;
            }

            // Aggiungo la stazione al manager ed il manager alla stazione
            managerBean.getStations().add(stationBean);
            stationBean.getManagers().add(managerBean);

            em.merge(managerBean);
            em.merge(stationBean);

            userTransaction.commit();

            return ResponseHelper.ADMIN_ADDSTATION_MANAGER_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED manager creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
