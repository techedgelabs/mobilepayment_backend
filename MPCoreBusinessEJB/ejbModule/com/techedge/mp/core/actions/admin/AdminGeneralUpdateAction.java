package com.techedge.mp.core.actions.admin;

import java.lang.reflect.Field;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TypeData;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.FunctionsHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminGeneralUpdateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminGeneralUpdateAction() {}

    public String execute(String adminTicketId, String requestID, String type, Long id, List<TypeData> fields) throws EJBException, ClassNotFoundException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_GENERAL_UPDATE_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            Class typeObject = null;

            try {
                typeObject = Class.forName("com.techedge.mp.core.business.model." + type);
            }
            catch (ClassNotFoundException e) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid Class");

                userTransaction.commit();

                return ResponseHelper.ADMIN_GENERAL_UPDATE_FAILURE;
            }

            if (typeObject == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid Class");

                userTransaction.commit();

                return ResponseHelper.ADMIN_GENERAL_UPDATE_FAILURE;
            }

            Object obj = em.find(typeObject, id);

            if (obj == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ID not valid");

                userTransaction.commit();

                return ResponseHelper.ADMIN_GENERAL_UPDATE_NOT_EXISTS;
            }

            for (TypeData item : fields) {
                if (item.getField() == null || item.getField().isEmpty() || item.getValue() == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid Class");

                    userTransaction.commit();

                    return ResponseHelper.ADMIN_GENERAL_UPDATE_FAILURE;
                }
                Boolean isFound = false;
                for (Field field : typeObject.getDeclaredFields()) {
                    if (field.getName().equals(item.getField())) {
                        isFound = true;
                        Object trasform = FunctionsHelper.toObject(field.getType(), item.getValue(), em);
                        if (trasform != null) {
                            FunctionsHelper.set(obj, item.getField(), FunctionsHelper.toObject(field.getType(), item.getValue(), em));
                            break;
                        }
                        else {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error in transformation about " + field.getType()
                                    + " : " + item.getValue());

                            userTransaction.commit();

                            return ResponseHelper.ADMIN_GENERAL_UPDATE_ERROR_FIND_ID;
                        }
                    }

                }
                if (!isFound) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Field " + item.getField() + " not exists");

                    userTransaction.commit();

                    return ResponseHelper.ADMIN_GENERAL_UPDATE_FAILURE;
                }

            }
            for (Field field : typeObject.getDeclaredFields()) {
                Object test = FunctionsHelper.getObject(obj, field.getName());
                if (typeObject.getDeclaredField(field.getName()).getAnnotation(Column.class) != null
                        && !typeObject.getDeclaredField(field.getName()).getAnnotation(Column.class).nullable() && test == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Mandatory Field " + field.getName() + " is null");

                    userTransaction.commit();

                    return ResponseHelper.ADMIN_GENERAL_UPDATE_ERROR_MANDATORY_FIELD_NULL;
                }
                else if (typeObject.getDeclaredField(field.getName()).getAnnotation(Column.class) == null) {
                    if (typeObject.getDeclaredField(field.getName()).getAnnotation(JoinColumn.class) != null
                            && !typeObject.getDeclaredField(field.getName()).getAnnotation(JoinColumn.class).nullable() && test == null) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Mandatory Field " + field.getName() + " is null");

                        userTransaction.commit();

                        return ResponseHelper.ADMIN_GENERAL_UPDATE_ERROR_MANDATORY_FIELD_NULL;
                    }
                }
            }
            try {
                em.persist(obj);
            }
            catch (Exception ex) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, ex.getMessage());
                throw new EJBException(ex);
            }
            userTransaction.commit();

            return ResponseHelper.ADMIN_GENERAL_UPDATE_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }

    }
}