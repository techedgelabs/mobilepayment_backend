package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.AdminRoleEnum;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentDataList;
import com.techedge.mp.core.business.interfaces.PaymentDataResponse;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminRefuelingUserRetrevePaymentDataAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserCategoryService userCategoryService;

    public AdminRefuelingUserRetrevePaymentDataAction() {}

    public PaymentDataResponse execute(String ticketId, String requestId, String username, Long id) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        PaymentDataResponse response = new PaymentDataResponse();
        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, ticketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.ADMIN_RETRY_PAY_DATA_REFUELING_USER_INVALID_TICKET);
                return response;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())
                    && !CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REFUELING_MANAGER)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return response;
            }

            // Verifica che non esista gi� un utente con quella email
            UserBean userBean = QueryRepository.findUserCustomerByEmail(em, username);

            if (userBean == null) {

                // Esiste gi� un utente con quella email
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not exists");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.ADMIN_RETRY_PAY_DATA_REFUELING_USER_USER_NOT_EXISTS);
                return response;
            }

            if (!userBean.getUserType().equals(User.USER_TYPE_REFUELING_NEW_ACQUIRER) && !userBean.getUserType().equals(User.USER_TYPE_REFUELING_OAUTH2)) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not refueling");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.ADMIN_RETRY_PAY_DATA_REFUELING_USER_USER_NOT_REFUELING);
                return response;
            }

            /*
            if (userBean.getPaymentData() != null) {
                for (PaymentInfoBean item : userBean.getPaymentData()) {
                    if (item.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || item.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                        if (id != null && id == item.getId()) {
                            PaymentDataList paymentDataList = new PaymentDataList();
                            paymentDataList.setId(item.getId());
                            paymentDataList.setBrand(item.getBrand());
                            paymentDataList.setDefaultMethod(item.getDefaultMethod().toString());
                            paymentDataList.setExpirationDate(item.getExpirationDate());
                            paymentDataList.setInsertDate(item.getInsertTimestamp());
                            paymentDataList.setMessage(item.getMessage());
                            paymentDataList.setStatus(item.getStatus());
                            response.getPaymentMethodList().add(paymentDataList);
                            break;
                        }
                        else if (id == null) {
                            PaymentDataList paymentDataList = new PaymentDataList();
                            paymentDataList.setId(item.getId());
                            paymentDataList.setBrand(item.getBrand());
                            paymentDataList.setDefaultMethod(item.getDefaultMethod().toString());
                            paymentDataList.setExpirationDate(item.getExpirationDate());
                            paymentDataList.setInsertDate(item.getInsertTimestamp());
                            paymentDataList.setMessage(item.getMessage());
                            paymentDataList.setStatus(item.getStatus());
                            response.getPaymentMethodList().add(paymentDataList);
                        }
                    }
                }
            }
            */
            
            // Restituisci il primo metodo di pagamento (credit_card o multicard) in stato 1 o 2
            if (userBean.getPaymentData() != null) {
                for (PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                    if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                        
                        PaymentDataList paymentDataList = new PaymentDataList();
                        paymentDataList.setId(paymentInfoBean.getId());
                        paymentDataList.setBrand(paymentInfoBean.getBrand());
                        paymentDataList.setDefaultMethod(paymentInfoBean.getDefaultMethod().toString());
                        paymentDataList.setExpirationDate(paymentInfoBean.getExpirationDate());
                        paymentDataList.setInsertDate(paymentInfoBean.getInsertTimestamp());
                        paymentDataList.setMessage(paymentInfoBean.getMessage());
                        paymentDataList.setStatus(paymentInfoBean.getStatus());
                        response.getPaymentMethodList().add(paymentDataList);
                        break;
                    }
                }
            }
            
            adminTicketBean.renew();
            em.merge(adminTicketBean);

            userTransaction.commit();

            response.setStatusCode(ResponseHelper.ADMIN_RETRY_PAY_DATA_REFUELING_USER_SUCCESS);
            return response;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
