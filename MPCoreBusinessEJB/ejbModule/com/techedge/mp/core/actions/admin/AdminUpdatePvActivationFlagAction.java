package com.techedge.mp.core.actions.admin;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PvGenerationData;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.StationActivationData;
import com.techedge.mp.core.business.interfaces.UpdatePvActivationFlagResult;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.StationDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUpdatePvActivationFlagAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;
    
    private static final String DEFAULT_ACQUIRER_ID = "BANCASELLA";
    private static final String DEFAULT_SHOP_LOGIN  = "GESPAYXXXXX";
    
    private static final String DEFAULT_ADDRESS     = "ADDRESS";
    private static final String DEFAULT_CITY        = "CITY";
    private static final String DEFAULT_COUNTRY     = "COUNTRY";
    private static final Double DEFAULT_LATITUDE    = 0.0;
    private static final Double DEFAULT_LONGITUDE   = 0.0;
    private static final String DEFAULT_PROVINCE    = "XX";

    public AdminUpdatePvActivationFlagAction() {}

    public UpdatePvActivationFlagResult execute(String adminTicketId, String requestID, List<StationActivationData> stationActivationList, ForecourtInfoServiceRemote forecourtInfoServiceRemote) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                UpdatePvActivationFlagResult updatePvActivationFlagResult = new UpdatePvActivationFlagResult();
                updatePvActivationFlagResult.setStatusCode(ResponseHelper.ADMIN_GENERATE_PV_INVALID_TICKET);

                return updatePvActivationFlagResult;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                
                UpdatePvActivationFlagResult updatePvActivationFlagResult = new UpdatePvActivationFlagResult();
                updatePvActivationFlagResult.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return updatePvActivationFlagResult;
            }

            UpdatePvActivationFlagResult updatePvActivationFlagResult = new UpdatePvActivationFlagResult();

            for (StationActivationData stationActivation : stationActivationList) {

                System.out.println("Update flag stationId: " + stationActivation.getStationID());
                
                System.out.println("General status:      " + stationActivation.getStatus());
                System.out.println("New acquirer active: " + stationActivation.getNewAcquirerActive());
                System.out.println("Refueling active:    " + stationActivation.getRefuelingActive());
                System.out.println("Loyalty active:      " + stationActivation.getLoyaltyActive());
                
                PvGenerationData pvGenerationData = new PvGenerationData();

                // Ottieni le informazioni sul PV dal gestionale
                GetStationDetailsResponse getStationDetailsResponse = forecourtInfoServiceRemote.getStationDetails(requestID, stationActivation.getStationID(), null, false);

                if (getStationDetailsResponse != null && getStationDetailsResponse.getStatusCode().equals("STATION_PUMP_FOUND_200")
                        && getStationDetailsResponse.getStationDetail() != null) {
    
                    System.out.println("Dati PV recuperati con successo dal gestionale");
                        
                    // Controlla se il PV esiste gi� sul backend
                    StationBean stationBean = QueryRepository.findStationBeanById(em, stationActivation.getStationID());

                    if (stationBean == null) {

                        System.out.println("Il PV non esiste sul backend");

                        try {

                            System.out.println("Creazione del PV sul backend");

                            StationDetail stationDetail = getStationDetailsResponse.getStationDetail();

                            Double latitudeDouble = Double.valueOf(stationDetail.getLatitude());
                            Double longitudeDouble = Double.valueOf(stationDetail.getLongitude());

                            StationBean newStationBean = new StationBean();
                            newStationBean.setAddress(stationDetail.getAddress());
                            newStationBean.setBeaconCode(null);
                            newStationBean.setCity(stationDetail.getCity());
                            newStationBean.setCountry(stationDetail.getCountry());
                            newStationBean.setLatitude(latitudeDouble);
                            newStationBean.setLongitude(longitudeDouble);
                            newStationBean.setProvince(stationDetail.getProvince());
                            newStationBean.setStationID(stationDetail.getStationID());
                            newStationBean.setNoOilAcquirerID(DEFAULT_ACQUIRER_ID);
                            newStationBean.setNoOilShopLogin(DEFAULT_SHOP_LOGIN);
                            newStationBean.setOilAcquirerID(DEFAULT_ACQUIRER_ID);
                            newStationBean.setOilShopLogin(DEFAULT_SHOP_LOGIN);
                            newStationBean.setPostpaidActive(true);
                            newStationBean.setPrepaidActive(true);
                            newStationBean.setShopActive(true);
                            newStationBean.setStationStatus(stationActivation.getStatus());
                            newStationBean.setDataAcquirer(null);
                            newStationBean.setNewAcquirerActive(stationActivation.getNewAcquirerActive());
                            newStationBean.setRefuelingActive(stationActivation.getRefuelingActive());
                            newStationBean.setLoyaltyActive(stationActivation.getLoyaltyActive());

                            em.persist(newStationBean);

                            pvGenerationData.setStationId(stationActivation.getStationID());
                            pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_SUCCESS);
                        }
                        catch (Exception ex) {

                            System.out.println("Si � verificato un errore nella creazione del PV sul backend");

                            pvGenerationData.setStationId(stationActivation.getStationID());
                            pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_FAILURE);
                        }
                    }
                    else {

                        // La stazione esiste gi� sul backend e va aggiornata con i dati ricevuti
                        try {

                            System.out.println("Aggiornamento dei dati del PV sul backend");

                            StationDetail stationDetail = getStationDetailsResponse.getStationDetail();

                            Double latitudeDouble = Double.valueOf(stationDetail.getLatitude());
                            Double longitudeDouble = Double.valueOf(stationDetail.getLongitude());

                            stationBean.setAddress(stationDetail.getAddress());
                            stationBean.setCity(stationDetail.getCity());
                            stationBean.setCountry(stationDetail.getCountry());
                            stationBean.setLatitude(latitudeDouble);
                            stationBean.setLongitude(longitudeDouble);
                            stationBean.setProvince(stationDetail.getProvince());
                            stationBean.setStationID(stationDetail.getStationID());
                            stationBean.setStationStatus(stationActivation.getStatus());
                            stationBean.setNewAcquirerActive(stationActivation.getNewAcquirerActive());
                            stationBean.setRefuelingActive(stationActivation.getRefuelingActive());
                            stationBean.setLoyaltyActive(stationActivation.getLoyaltyActive());
                            
                            em.merge(stationBean);

                            pvGenerationData.setStationId(stationActivation.getStationID());
                            pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_SUCCESS);
                        }
                        catch (Exception ex) {

                            ex.printStackTrace();
                            
                            System.out.println("Si � verificato un errore nell'aggiornamento dei dati del PV sul backend");

                            pvGenerationData.setStationId(stationActivation.getStationID());
                            pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_FAILURE);
                        }
                    }
                }
                else {

                    if (stationActivation.getStatus().equals(Station.STATION_STATUS_ACTIVE) &&
                            stationActivation.getNewAcquirerActive() == Boolean.FALSE &&
                            stationActivation.getRefuelingActive() == Boolean.FALSE &&
                            stationActivation.getLoyaltyActive() == Boolean.TRUE) {
    
                        // Se il PV � attivo solo in modalit� Loyalty
                        
                        // Controlla se il PV esiste gi� sul backend
                        StationBean stationBean = QueryRepository.findStationBeanById(em, stationActivation.getStationID());
    
                        if (stationBean == null) {
    
                            System.out.println("Il PV non esiste sul backend");
    
                            try {

                                System.out.println("Creazione del PV sul backend con dati fittizi");

                                StationBean newStationBean = new StationBean();
                                
                                newStationBean.setAddress(DEFAULT_ADDRESS);
                                newStationBean.setBeaconCode(null);
                                newStationBean.setCity(DEFAULT_CITY);
                                newStationBean.setCountry(DEFAULT_COUNTRY);
                                newStationBean.setLatitude(DEFAULT_LATITUDE);
                                newStationBean.setLongitude(DEFAULT_LONGITUDE);
                                newStationBean.setProvince(DEFAULT_PROVINCE);
                                newStationBean.setStationID(stationActivation.getStationID());
                                newStationBean.setNoOilAcquirerID(DEFAULT_ACQUIRER_ID);
                                newStationBean.setNoOilShopLogin(DEFAULT_SHOP_LOGIN);
                                newStationBean.setOilAcquirerID(DEFAULT_ACQUIRER_ID);
                                newStationBean.setOilShopLogin(DEFAULT_SHOP_LOGIN);
                                newStationBean.setPostpaidActive(true);
                                newStationBean.setPrepaidActive(true);
                                newStationBean.setShopActive(true);
                                newStationBean.setStationStatus(stationActivation.getStatus());
                                newStationBean.setDataAcquirer(null);
                                newStationBean.setNewAcquirerActive(stationActivation.getNewAcquirerActive());
                                newStationBean.setRefuelingActive(stationActivation.getRefuelingActive());
                                newStationBean.setLoyaltyActive(stationActivation.getLoyaltyActive());
                                
                                em.persist(newStationBean);

                                pvGenerationData.setStationId(stationActivation.getStationID());
                                pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_SUCCESS);
                            }
                            catch (Exception ex) {

                                System.out.println("Si � verificato un errore nella creazione del PV sul backend");

                                pvGenerationData.setStationId(stationActivation.getStationID());
                                pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_FAILURE);
                            }
                        }
                        else {
    
                            // Il PV esiste gi� sul backend e va aggiornato con i dati ricevuti
                            
                            try {
    
                                System.out.println("Aggiornamento dei dati del PV sul backend");
    
                                stationBean.setStationStatus(stationActivation.getStatus());
                                stationBean.setNewAcquirerActive(stationActivation.getNewAcquirerActive());
                                stationBean.setRefuelingActive(stationActivation.getRefuelingActive());
                                stationBean.setLoyaltyActive(stationActivation.getLoyaltyActive());
                                
                                em.merge(stationBean);
    
                                pvGenerationData.setStationId(stationActivation.getStationID());
                                pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_SUCCESS);
                            }
                            catch (Exception ex) {
    
                                ex.printStackTrace();
                                
                                System.out.println("Si � verificato un errore nell'aggiornamento dei dati del PV sul backend");
    
                                pvGenerationData.setStationId(stationActivation.getStationID());
                                pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_FAILURE);
                            }
                        }
                    }
                    else {
                        
                        System.out.println("Non � stato possibile recuperare i dati del PV dal gestionale");
    
                        pvGenerationData.setStationId(stationActivation.getStationID());
                        pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_NOT_FOUND);
                    }
                }

                updatePvActivationFlagResult.getPvGenerationDataList().add(pvGenerationData);
            }

            updatePvActivationFlagResult.setStatusCode(ResponseHelper.ADMIN_GENERATE_PV_SUCCESS);

            userTransaction.commit();

            return updatePvActivationFlagResult;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED updating PV activation flag with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}
