package com.techedge.mp.core.actions.parking.v2;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.EstimateParkingPriceResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PlateNumberBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionStatusBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.CitiesParkingZone;
import com.techedge.mp.parking.integration.business.interfaces.ParkingZone;
import com.techedge.mp.parking.integration.business.interfaces.StatusCodeHelper;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2EstimateParkingPriceAction extends ParkingV2ParkingPaymentAction {

    @Resource
    private EJBContext         context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager      em;

    @EJB
    private LoggerService      loggerService;

    @EJB
    private ParametersService  parameterService;
    public final static String PARAM_PARKING_NOTICE_SPLITTER_SYMBOL = "PARKING_NOTICE_SPLITTER_SYMBOL";
    

    public ParkingV2EstimateParkingPriceAction() {}

    public EstimateParkingPriceResult execute(ParkingServiceRemote parkingService, String ticketId, String requestId, String plateNumber, Date requestedEndTime,
            String parkingZoneId, String stallCode, String cityId, String lang, String acquirerID, String currency, String groupAcquirer, String serverName,
            String shopLogin, Double maxParkingAmount, Integer reconciliationMaxAttempts) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        EstimateParkingPriceResult estimateParkingPriceResult = new EstimateParkingPriceResult();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_INVALID_TICKET);
                return estimateParkingPriceResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_INVALID_TICKET);
                return estimateParkingPriceResult;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User in status " + userStatus + " unauthorized");

                userTransaction.commit();
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_UNAUTHORIZED);
                return estimateParkingPriceResult;
            }
            
            PaymentInfoBean voucherPaymentMethod = userBean.getVoucherPaymentMethod();
            if (voucherPaymentMethod == null) {

                // L'utente non ha definito il pin
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not defined");

                userTransaction.commit();
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_PIN_NOT_FOUND);
                return estimateParkingPriceResult;
            }
            
            PaymentInfoBean paymentInfoBean = userBean.findDefaultPaymentInfoBean();
            if (paymentInfoBean == null) {

                // L'utente non ha una carta di credito associata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "No payment method available");

                userTransaction.commit();
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_NO_PAYMENT_METHOD_FOUND);
                return estimateParkingPriceResult;
            }
            
            // La data di fine sosta deve essere nel futuro
            Date now = new Date();
            long delta = 60000l; // si utilizza un minuto per compensare gli enventuali disallienamenti del timestamp delle macchine
            if (requestedEndTime.getTime() + delta < now.getTime()) {
                
                // La data di fine sosta � nel passato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid requestedEndTime: " + requestedEndTime.getTime());

                userTransaction.commit();
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_INVALID_REQUESTED_END_TIME);
                return estimateParkingPriceResult;
            }
            
            // I campi cityId e parkingZoneId sono obbligatori
            if (cityId == null || cityId.isEmpty() || parkingZoneId == null || parkingZoneId.isEmpty()) {
                
                // cityId o parkingZoneId non valorizzati
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid cityId (" + cityId + ") or parkingZoneId (" + parkingZoneId + ")");

                userTransaction.commit();
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE);
                return estimateParkingPriceResult;
            }
            
            // Verifica esistenza area di sosta
            com.techedge.mp.parking.integration.business.interfaces.GetParkingZonesByCityResult getParkingZonesByCityResult = 
                    parkingService.getParkingZonesByCityResult(lang, cityId);
            
            if (getParkingZonesByCityResult == null || !getParkingZonesByCityResult.getStatusCode().equals(StatusCodeHelper.RETRIEVE_PARKING_ZONES_OK)) {
                
                // errore nella verifica dell'area di sosta
                if (getParkingZonesByCityResult == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Parking area check failure: getParkingZonesByCityResult null");
                }
                else {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Parking area check failure: " + getParkingZonesByCityResult.getStatusCode());
                }

                userTransaction.commit();
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE);
                return estimateParkingPriceResult;
            }
            
            if (getParkingZonesByCityResult.getCitiesParkingZones().isEmpty()) {
                
                // Citt� non trovata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "No city found with cityId " + cityId);

                userTransaction.commit();
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE);
                return estimateParkingPriceResult;
            }
            
            CitiesParkingZone cityParkingZone = getParkingZonesByCityResult.getCitiesParkingZones().get(0);
            
            if (cityParkingZone.getParkingZoneList().isEmpty()) {
                
                // Zona non trovata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "No zone found for cityId " + cityId);

                userTransaction.commit();
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE);
                return estimateParkingPriceResult;
            }
            
            ParkingZone parkingZone = null;
                    
            for (ParkingZone parkingZoneElement : cityParkingZone.getParkingZoneList()) {
                
                if (parkingZoneElement.getId().equals(parkingZoneId)) {
                    
                    parkingZone = parkingZoneElement;
                    break;
                }
            }
            
            System.out.println("Codice stallo richiesto? " + parkingZone.getStallCodeRequired());
            
            if (parkingZone.getStallCodeRequired()) {
                
                if (stallCode == null || stallCode.isEmpty()) {
                    
                    // Codice stallo non inserito per zona che lo richiede
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Stall code not provided");

                    userTransaction.commit();
                    estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE);
                    return estimateParkingPriceResult;
                }
            }
            else {
                
                if (stallCode != null && !stallCode.isEmpty()) {
                    
                    // Codice stallo inserito per zona che non lo richiede
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Stall code not required");

                    userTransaction.commit();
                    estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE);
                    return estimateParkingPriceResult;
                }
            }
            
            ParkingTransactionBean parkingTransactionBean = null;
            ParkingTransactionItemBean parkingTransactionItemBean = null;

            List<ParkingTransactionItemBean> parkingTransactionItemBeanList = QueryRepository.findActiveParkingTransactionItemByUser(em, userBean,
                    PARKING_TRANSACTION_STATUS.ENDED.getValue(), PARKING_TRANSACTION_STATUS.FAILED.getValue(), PARKING_TRANSACTION_STATUS.NOT_RECONCILIABLE.getValue());

            
            System.out.println("Inizio verifica esistenza sosta attiva per utente in sessione (se presente vengono restituiti i dati della sosta in corso)");
            
            if (!parkingTransactionItemBeanList.isEmpty()) {

                parkingTransactionItemBean = parkingTransactionItemBeanList.get(0);
                parkingTransactionBean = parkingTransactionItemBean.getParkingTransactionBean();

                //Esclusione dei casi di inconsistenza degli stati
                if (!(parkingTransactionBean.getStatus().equals(PARKING_TRANSACTION_STATUS.CREATED.getValue()) && parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue()))) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                            "Trovata transazione attiva in stato " + parkingTransactionBean.getStatus() + " e stato item " + parkingTransactionItemBean.getParkingItemStatus());

                    estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE);

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_PARKING_PRICE_TRANSACTION, ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE,
                            "Trovata transazione attiva in stato " + parkingTransactionBean.getStatus() + " e stato item " + parkingTransactionItemBean.getParkingItemStatus());
                    em.persist(parkingTransactionStatusBean);
                    userTransaction.commit();
                    return estimateParkingPriceResult;
                }
                
                System.out.println("Trovata sosta " + parkingTransactionBean.getParkingTransactionId());
            }
            else {
                
                parkingTransactionBean = new ParkingTransactionBean();
                parkingTransactionItemBean = new ParkingTransactionItemBean();
                
                String parkingTransactionId = new IdGenerator().generateId(16).substring(0, 32);
                parkingTransactionBean.setParkingTransactionId(parkingTransactionId);
                parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.CREATED.getValue());
                
                String parkingTransactionItemId = new IdGenerator().generateId(16).substring(0, 30);
                parkingTransactionItemBean.setParkingTransactionItemId(parkingTransactionItemId);
                parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue());
                
                System.out.println("Sosta non trovata: generato id " + parkingTransactionBean.getParkingTransactionId());
            }
            
            System.out.println("Fine verifica esistenza sosta attiva per utente in sessione");
            
            com.techedge.mp.parking.integration.business.interfaces.EstimateParkingPriceResult estimateParkingPriceRemoteResult = 
                    parkingService.estimateParkingPrice(lang, plateNumber, requestedEndTime, parkingZoneId, stallCode);

            if (estimateParkingPriceRemoteResult == null || estimateParkingPriceRemoteResult.getPrice() == null) {

                // Gestione errore comunicazione con EJB

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EJB communication error");
                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE);
                
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_PARKING_PRICE_TRANSACTION, ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE,
                        "cannot retrieve price from remote service");
                em.persist(parkingTransactionStatusBean);

                userTransaction.commit();
                return estimateParkingPriceResult;
            }

            if (!estimateParkingPriceRemoteResult.getStatusCode().equals(StatusCodeHelper.ESTIMATE_PARKING_PRICE_OK)) {

                // Gestione errore applicativo

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Application error: " + estimateParkingPriceResult.getStatusCode());

                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE);
                
                /*
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_PARKING_PRICE_TRANSACTION, ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_FAILURE,
                        "cannot retrieve data from remote service");
                em.persist(parkingTransactionStatusBean);
                */
                userTransaction.commit();
                return estimateParkingPriceResult;
            }
            
            // Estrai la descrizione della targa
            String plateNumberDescription = "";
            if (userBean.getPlateNumberList() != null && !userBean.getPlateNumberList().isEmpty()) {
                for(PlateNumberBean plateNumberBean : userBean.getPlateNumberList()) {
                    if (plateNumberBean.getPlateNumber().equals(estimateParkingPriceRemoteResult.getPlateNumber())) {
                        plateNumberDescription = plateNumberBean.getDescription();
                    }
                }
            }
            parkingTransactionBean.setUserBean(ticketBean.getUser());
            parkingTransactionBean.setParkingZoneId(parkingZone.getId());
            parkingTransactionBean.setParkingZoneName(parkingZone.getName());
            parkingTransactionBean.setParkingZoneDescription(parkingZone.getDescription());
            parkingTransactionBean.setPlateNumber(estimateParkingPriceRemoteResult.getPlateNumber());
            parkingTransactionBean.setPlateNumberDescription(plateNumberDescription);
            parkingTransactionBean.setStallCodeRequired(parkingZone.getStallCodeRequired());
            parkingTransactionBean.setStallCode(estimateParkingPriceRemoteResult.getStallCode());
            parkingTransactionBean.setTimestamp(new Timestamp(Calendar.getInstance().getTimeInMillis()));
            
            parkingTransactionBean.setCityId(cityId);
            parkingTransactionBean.setCityName(cityParkingZone.getCity().getName());
            parkingTransactionBean.setAdministrativeAreaLevel2Code(cityParkingZone.getCity().getAdministrativeAreaLevel2Code());
            parkingTransactionBean.setStickerRequired(cityParkingZone.getCity().getStickerRequired());
            parkingTransactionBean.setStickerRequiredNotice(cityParkingZone.getCity().getStickerRequiredNotice());
            parkingTransactionBean.setUserNotified(Boolean.FALSE);
            
            String noticeParsed = "";
            int counter = 0;
            for (String noticeItem : estimateParkingPriceRemoteResult.getNotice()) {

                noticeParsed += (counter != estimateParkingPriceRemoteResult.getNotice().size() - 1 ? noticeItem.concat(this.parameterService.getParamValue(PARAM_PARKING_NOTICE_SPLITTER_SYMBOL))
                        : noticeItem);
                counter++;
            }
            parkingTransactionBean.setParkingZoneNotice(noticeParsed);
            parkingTransactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
            
            em.persist(parkingTransactionBean);
            
            
            if (estimateParkingPriceRemoteResult.getPrice().doubleValue() == 0.0)
            {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Application error: estimate price 0.0");

                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_AMOUNT_ZERO);
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_PARKING_PRICE_TRANSACTION, ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_AMOUNT_ZERO,
                        "Parking Price 0.0");
                em.persist(parkingTransactionStatusBean);
                userTransaction.commit();
                return estimateParkingPriceResult;
            }
            
            if (estimateParkingPriceRemoteResult.getPrice().doubleValue() > maxParkingAmount.doubleValue())
            {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Application error: " + estimateParkingPriceResult.getStatusCode());

                estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_AMOUNT_OVER_THRESHOLD);
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_PARKING_PRICE_TRANSACTION, ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_AMOUNT_OVER_THRESHOLD,
                        "parking price amount over threshold");
                em.persist(parkingTransactionStatusBean);
                userTransaction.commit();
                return estimateParkingPriceResult;
            }
            /*
            // Estrai la descrizione della targa
            String plateNumberDescription = "";
            if (userBean.getPlateNumberList() != null && !userBean.getPlateNumberList().isEmpty()) {
                for(PlateNumberBean plateNumberBean : userBean.getPlateNumberList()) {
                    if (plateNumberBean.getPlateNumber().equals(estimateParkingPriceRemoteResult.getPlateNumber())) {
                        plateNumberDescription = plateNumberBean.getDescription();
                    }
                }
            }
            
            parkingTransactionBean.setUserBean(ticketBean.getUser());
            parkingTransactionBean.setParkingZoneId(parkingZone.getId());
            parkingTransactionBean.setParkingZoneName(parkingZone.getName());
            parkingTransactionBean.setParkingZoneDescription(parkingZone.getDescription());
            parkingTransactionBean.setPlateNumber(estimateParkingPriceRemoteResult.getPlateNumber());
            parkingTransactionBean.setPlateNumberDescription(plateNumberDescription);
            parkingTransactionBean.setStallCodeRequired(parkingZone.getStallCodeRequired());
            parkingTransactionBean.setStallCode(estimateParkingPriceRemoteResult.getStallCode());
            parkingTransactionBean.setTimestamp(new Timestamp(Calendar.getInstance().getTimeInMillis()));
            
            parkingTransactionBean.setCityId(cityId);
            parkingTransactionBean.setCityName(cityParkingZone.getCity().getName());
            parkingTransactionBean.setAdministrativeAreaLevel2Code(cityParkingZone.getCity().getAdministrativeAreaLevel2Code());
            parkingTransactionBean.setStickerRequired(cityParkingZone.getCity().getStickerRequired());
            parkingTransactionBean.setStickerRequiredNotice(cityParkingZone.getCity().getStickerRequiredNotice());
            parkingTransactionBean.setUserNotified(Boolean.FALSE);
            
            String noticeParsed = "";
            int counter = 0;
            for (String noticeItem : estimateParkingPriceRemoteResult.getNotice()) {

                noticeParsed += (counter != estimateParkingPriceRemoteResult.getNotice().size() - 1 ? noticeItem.concat(this.parameterService.getParamValue(PARAM_PARKING_NOTICE_SPLITTER_SYMBOL))
                        : noticeItem);
                counter++;
            }
            parkingTransactionBean.setParkingZoneNotice(noticeParsed);
            parkingTransactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
            
            em.persist(parkingTransactionBean);
            */
            parkingTransactionItemBean.setPrice(estimateParkingPriceRemoteResult.getPrice());
            parkingTransactionItemBean.setParkingStartTime(estimateParkingPriceRemoteResult.getParkingStartTime());
            parkingTransactionItemBean.setParkingEndTime(estimateParkingPriceRemoteResult.getParkingEndTime());
            parkingTransactionItemBean.setRequestedEndTime(estimateParkingPriceRemoteResult.getRequestedEndTime());
            //parkingTransactionItemBean.setParkingTimeCorrection(Integer.valueOf(estimateParkingPriceRemoteResult.getParkingTimeCorrection().toString()));
            parkingTransactionItemBean.setAcquirerID(acquirerID);
            parkingTransactionItemBean.setCurrency(currency);
            parkingTransactionItemBean.setGroupAcquirer(groupAcquirer);
            parkingTransactionItemBean.setServerName(serverName);
            parkingTransactionItemBean.setShopLogin(shopLogin);
            
            parkingTransactionItemBean.setParkingTransactionBean(parkingTransactionBean);

            em.persist(parkingTransactionItemBean);

            estimateParkingPriceResult.setTransactionId(parkingTransactionBean.getParkingTransactionId());

            estimateParkingPriceResult.setNotice(estimateParkingPriceRemoteResult.getNotice());
            estimateParkingPriceResult.setParkingEndTime(estimateParkingPriceRemoteResult.getParkingEndTime());
            estimateParkingPriceResult.setParkingStartTime(estimateParkingPriceRemoteResult.getParkingStartTime());
            estimateParkingPriceResult.setParkingTimeCorrection(estimateParkingPriceRemoteResult.getParkingTimeCorrection());
            
            estimateParkingPriceResult.setParkingZoneId(parkingZone.getId());
            estimateParkingPriceResult.setParkingZoneName(parkingZone.getName());
            estimateParkingPriceResult.setParkingZoneDescription(parkingZone.getDescription());
            estimateParkingPriceResult.setCityId(cityId);
            estimateParkingPriceResult.setCityName(cityParkingZone.getCity().getName());
            estimateParkingPriceResult.setAdministrativeAreaLevel2Code(cityParkingZone.getCity().getAdministrativeAreaLevel2Code());
            
            estimateParkingPriceResult.setPlateNumber(estimateParkingPriceRemoteResult.getPlateNumber());
            estimateParkingPriceResult.setPrice(estimateParkingPriceRemoteResult.getPrice());
            estimateParkingPriceResult.setRequestedEndTime(estimateParkingPriceRemoteResult.getRequestedEndTime());
            estimateParkingPriceResult.setStallCode(estimateParkingPriceRemoteResult.getStallCode());
            estimateParkingPriceResult.setStatusCode(estimateParkingPriceRemoteResult.getStatusCode());

            estimateParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_SUCCESS);
            ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                    PARKING_TRANSACTION_STATUS_CONST.INFO, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_PARKING_PRICE_TRANSACTION, ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_SUCCESS,
                    "Estimate parking price successfully created");
            em.persist(parkingTransactionStatusBean);
            userTransaction.commit();

            return estimateParkingPriceResult;

        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED extimate parking price with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }
}
