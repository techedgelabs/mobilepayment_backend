package com.techedge.mp.core.actions.parking.v2;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.parking.ApproveParkingTransactionResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemEventBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionStatusBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.StatusCodeHelper;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2ApproveParkingTransactionAction extends ParkingV2ParkingPaymentAction {

    @Resource
    private EJBContext                             context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager                          em;

    @EJB
    private LoggerService                          loggerService;

    @EJB
    private ParametersService                      parameterService;

    private Integer                                sequenceID      = 0;
    private PARKING_TRANSACTION_ITEM_STATUS_STATUS newStatus       = null;
    private PARKING_TRANSACTION_ITEM_STATUS_STATUS oldStatus       = null;
    private PaymentInfoBean                        paymentInfoBean = null;

    public ParkingV2ApproveParkingTransactionAction() {}

    public ApproveParkingTransactionResult execute(ParkingServiceRemote parkingService, GPServiceRemote gpService, String ticketId, String requestId, String encodedPin,
            Integer pinCheckMaxAttempts, String transactionId, Long paymentMethodId, String paymentMethodType, Double defaultAmount, String lang, String myCiceroDecodedSecretKey,
            Double parkingMaxAmount) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        ApproveParkingTransactionResult approveParkingTransactionResult = new ApproveParkingTransactionResult();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_INVALID_TICKET);
                return approveParkingTransactionResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_INVALID_TICKET);
                return approveParkingTransactionResult;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User in status " + userStatus + " unauthorized");

                userTransaction.commit();
                approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_UNAUTHORIZED);
                return approveParkingTransactionResult;
            }

            PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(em, paymentMethodId, paymentMethodType);

            if (paymentInfoBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment Method not found");

                userTransaction.commit();

                approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_PAYMENT_WRONG);
                return approveParkingTransactionResult;
            }

            if (paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {

                /*
                 * verifica cap da implementare
                 * double capAvailable = 0.0;
                 * 
                 * if (userBean.getCapAvailable() != null) {
                 * capAvailable = userBean.getCapAvailable().doubleValue();
                 * }
                 * 
                 * if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {
                 * 
                 * // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
                 * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                 * "Payment not verified and transaction amount exceeding the cap");
                 * 
                 * userTransaction.commit();
                 * 
                 * approveParkingTransactionResult.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_CAP_FAILURE);
                 * return approveParkingTransactionResult;
                 * }
                 */
            }
            else {

                // Non � possibile pagare una sosta con voucher

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment Method not valid: " + paymentMethodType);

                userTransaction.commit();

                approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_PAYMENT_WRONG);
                return approveParkingTransactionResult;
            }

            approveParkingTransactionResult = this.checkPaymentMethod(requestId, userTransaction, userBean, paymentMethodId, paymentMethodType, encodedPin, pinCheckMaxAttempts);

            if (approveParkingTransactionResult.getStatusCode() != null) {
                return approveParkingTransactionResult;
            }

            System.out.println("Ricerca transazione da avviare");

            ParkingTransactionItemBean parkingTransactionItemBean = null;
            ParkingTransactionBean parkingTransactionBean = null;
            List<ParkingTransactionItemBean> parkingTransactionBeanItemList = QueryRepository.findParkingTransactionItemByTransactionId(em, transactionId,
                    PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue());

            if (parkingTransactionBeanItemList.isEmpty()) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Parking Transaction Item not found");

                userTransaction.commit();

                approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_NOT_FOUND);
                return approveParkingTransactionResult;

            }
            else {
                parkingTransactionItemBean = parkingTransactionBeanItemList.get(0);
                parkingTransactionBean = parkingTransactionItemBean.getParkingTransactionBean();

                if (!(parkingTransactionBean.getStatus().equals(PARKING_TRANSACTION_STATUS.CREATED.getValue()) && parkingTransactionItemBean.getParkingItemStatus().equals(
                        PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue()))) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Start Parking Transaction Failure. Status: "
                            + parkingTransactionBean.getStatus());

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_INVALID_STATUS, "Start Parking Transaction Failure. Status: " + parkingTransactionBean.getStatus());
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();

                    approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_INVALID_STATUS);
                    return approveParkingTransactionResult;
                }

            }

            // Pagamento con carta di credito
            System.out.println("Pagamento con carta di credito");

            Integer eventSequenceID = 1;
            Integer statusSequenceID = sequenceID;

            Double amount = parkingTransactionItemBean.getPrice().doubleValue();

            if (amount.doubleValue() > parkingMaxAmount.doubleValue()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Start Parking Transaction Failure. Status: "
                        + parkingTransactionBean.getStatus());

                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION,
                        ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_AMOUNT_OVER_THRESHOLD,
                        "parking amount: " + amount.doubleValue() + " > " + parkingMaxAmount.doubleValue());
                em.persist(parkingTransactionStatusBean);

                userTransaction.commit();

                approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_AMOUNT_OVER_THRESHOLD);
                return approveParkingTransactionResult;
            }

            GestPayData gestPayDataAUTHResponse = this.doPaymentAuth(parkingTransactionItemBean, paymentInfoBean, userBean, paymentMethodId, amount, statusSequenceID, requestId,
                    eventSequenceID, gpService, myCiceroDecodedSecretKey);

            if (gestPayDataAUTHResponse.getTransactionResult().equals("ERROR")) {

                parkingTransactionBean.setToReconcile(true);
                parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.FAILED.getValue());
                parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue());
                em.merge(parkingTransactionItemBean);
                em.merge(parkingTransactionBean);
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Start Parking Transaction Failure");

                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION,
                        ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_PAYMENT_COMUNICATION_FAILURE, gestPayDataAUTHResponse.getErrorDescription());
                em.persist(parkingTransactionStatusBean);

                userTransaction.commit();

                approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_PAYMENT_COMUNICATION_FAILURE);
                approveParkingTransactionResult.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

                return approveParkingTransactionResult;
            }

            if (gestPayDataAUTHResponse.getTransactionResult().equals("KO")) {

                parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.FAILED.getValue());
                parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue());
                em.merge(parkingTransactionItemBean);
                em.merge(parkingTransactionBean);
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Start Parking Transaction Failure");

                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION,
                        ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_AUTH_KO, gestPayDataAUTHResponse.getErrorDescription());

                approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_AUTH_KO);
                approveParkingTransactionResult.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

                if (insufficientFunds(gestPayDataAUTHResponse)) {
                    approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_INSUFFICIENT_AMOUNT);
                    parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId, PARKING_TRANSACTION_STATUS_CONST.ERROR,
                            PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_INSUFFICIENT_AMOUNT,
                            gestPayDataAUTHResponse.getErrorDescription());
                    em.persist(parkingTransactionStatusBean);
                    userTransaction.commit();
                    return approveParkingTransactionResult;

                }
                em.persist(parkingTransactionStatusBean);
                userTransaction.commit();
                return approveParkingTransactionResult;
            }

            System.out.println("Pagamento con carta di credito effettuato con successo");

            // Avvio sosta
            System.out.println("Invio richiesta di avvio sosta");

            com.techedge.mp.parking.integration.business.interfaces.StartParkingResult startParkingRemoteResult = null;

            String clientOperationId = new IdGenerator().generateId(16).substring(0, 32);

            int attemptsLeft = Integer.valueOf(parameterService.getParamValue(PARKING_PARAM_ATTEMPTS));
            long intervalRetry = Long.valueOf(parameterService.getParamValue(PARKING_PARAM_INTERVAL_RETRY));

            while (attemptsLeft > 0) {

                attemptsLeft--;
                System.out.println("StartParking attempts left: " + attemptsLeft);

                startParkingRemoteResult = parkingService.startParking(lang, parkingTransactionBean.getPlateNumber(), parkingTransactionItemBean.getRequestedEndTime(),
                        parkingTransactionBean.getParkingZoneId(), parkingTransactionBean.getStallCode(), clientOperationId);

                if (startParkingRemoteResult == null) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EJB communication error");

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_FAILURE, "cannot retrieve data from remote service");
                    em.persist(parkingTransactionStatusBean);

                    invalidatePendingParkingTransactionItems(parkingTransactionBean, userBean, requestId, gpService, myCiceroDecodedSecretKey);
                    approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_FAILURE);

                    userTransaction.commit();
                    return approveParkingTransactionResult;
                }

                if (startParkingRemoteResult.getStatusCode().equals(StatusCodeHelper.START_PARKING_INVALID_STALL_CODE)) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                            "Application error: " + approveParkingTransactionResult.getStatusCode());

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_INVALID_STALL_CODE, "invalid stall code or already in use");
                    em.persist(parkingTransactionStatusBean);

                    invalidatePendingParkingTransactionItems(parkingTransactionBean, userBean, requestId, gpService, myCiceroDecodedSecretKey);

                    approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_INVALID_STALL_CODE);

                    userTransaction.commit();
                    return approveParkingTransactionResult;
                }

                if (startParkingRemoteResult.getStatusCode().equals(StatusCodeHelper.START_PARKING_PLATE_NUMBER_IN_USE)) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                            "Application error: " + approveParkingTransactionResult.getStatusCode());

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_PLATE_NUMBER_ALREADY_IN_USE, "Plate number already in use");
                    em.persist(parkingTransactionStatusBean);

                    invalidatePendingParkingTransactionItems(parkingTransactionBean, userBean, requestId, gpService, myCiceroDecodedSecretKey);

                    approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_PLATE_NUMBER_ALREADY_IN_USE);

                    userTransaction.commit();
                    return approveParkingTransactionResult;
                }

                if (!startParkingRemoteResult.getStatusCode().equals(StatusCodeHelper.START_PARKING_OK)) {

                    if (attemptsLeft == 0) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Application error: "
                                + approveParkingTransactionResult.getStatusCode());

                        ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION,
                                ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_FAILURE, "no more attempts");
                        em.persist(parkingTransactionStatusBean);

                        invalidatePendingParkingTransactionItems(parkingTransactionBean, userBean, requestId, gpService, myCiceroDecodedSecretKey);

                        approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_FAILURE);

                        userTransaction.commit();
                        return approveParkingTransactionResult;
                    }

                }
                else {
                    break;
                }

                Thread.sleep(intervalRetry);
            }

            //AGGIORNAMENTO STATI ENTITIES (ParkingTransactionbean => STARTED, ParkingTransactionItemBean => AUTHORIZED) CASO POSITIVO 
            //AGGIORNAMENTO PREZZO PREAUTORIZZATO
            //INSERIMENTO RECORD IN ENTITY DI CONTABILIZZAZIONE

            System.out.println("Sosta avviata con successo");

            parkingTransactionBean.setParkingId(startParkingRemoteResult.getParkingId());
            parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.STARTED.getValue());
            parkingTransactionBean.setEstimatedParkingEndTime(startParkingRemoteResult.getParkingEndTime());

            String noticeParsed = "";
            int counter = 0;
            for (String noticeItem : startParkingRemoteResult.getNotice()) {

                noticeParsed += (counter != startParkingRemoteResult.getNotice().size() - 1 ? noticeItem.concat(this.parameterService.getParamValue(ParkingV2EstimateParkingPriceAction.PARAM_PARKING_NOTICE_SPLITTER_SYMBOL))
                        : noticeItem);
                counter++;
            }

            parkingTransactionBean.setParkingZoneNotice(noticeParsed);
            em.merge(parkingTransactionBean);

            parkingTransactionItemBean.setPrice(startParkingRemoteResult.getPrice());
            parkingTransactionItemBean.setParkingStartTime(startParkingRemoteResult.getParkingStartTime());
            parkingTransactionItemBean.setParkingEndTime(startParkingRemoteResult.getParkingEndTime());
            parkingTransactionItemBean.setRequestedEndTime(startParkingRemoteResult.getRequestedEndTime());
            parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue());

            String pan = paymentInfoBean.getPan();
            parkingTransactionItemBean.setPan(pan);

            em.merge(parkingTransactionItemBean);

            approveParkingTransactionResult.setTransactionId(parkingTransactionBean.getParkingTransactionId());

            approveParkingTransactionResult.setNotice(startParkingRemoteResult.getNotice());
            approveParkingTransactionResult.setParkingEndTime(startParkingRemoteResult.getParkingEndTime());
            approveParkingTransactionResult.setParkingStartTime(startParkingRemoteResult.getParkingStartTime());
            approveParkingTransactionResult.setParkingTimeCorrection(startParkingRemoteResult.getParkingTimeCorrection());

            approveParkingTransactionResult.setPlateNumber(startParkingRemoteResult.getPlateNumber());
            approveParkingTransactionResult.setPrice(startParkingRemoteResult.getPrice());
            approveParkingTransactionResult.setRequestedEndTime(startParkingRemoteResult.getRequestedEndTime());
            approveParkingTransactionResult.setStallCode(startParkingRemoteResult.getStallCode());
            approveParkingTransactionResult.setStatusCode(startParkingRemoteResult.getStatusCode());
            approveParkingTransactionResult.setTransactionId(parkingTransactionBean.getParkingTransactionId());
            approveParkingTransactionResult.setPaymentMethodId(parkingTransactionItemBean.getPaymentMethodId());

            approveParkingTransactionResult.setParkingZoneId(startParkingRemoteResult.getParkingZoneId());
            approveParkingTransactionResult.setParkingZoneName(parkingTransactionBean.getParkingZoneName());
            approveParkingTransactionResult.setParkingZoneDescription(parkingTransactionBean.getParkingZoneDescription());
            approveParkingTransactionResult.setCityId(parkingTransactionBean.getCityId());
            approveParkingTransactionResult.setCityName(parkingTransactionBean.getCityName());
            approveParkingTransactionResult.setAdministrativeAreaLevel2Code(parkingTransactionBean.getAdministrativeAreaLevel2Code());

            approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_SUCCESS);
            ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                    PARKING_TRANSACTION_STATUS_CONST.INFO, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_SUCCESS,
                    "Parking successfully started");
            em.persist(parkingTransactionStatusBean);
            userTransaction.commit();

            return approveParkingTransactionResult;

        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED approving parking transaction with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }

    private GestPayData doPaymentAuthDelete(ParkingTransactionItemBean parkingTransactionItemBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double amount, Integer statusSequenceID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = null;
        ParkingTransactionStatusBean parkingTransactionItemStatusBean = null;

        newStatus = PARKING_TRANSACTION_ITEM_STATUS_STATUS.PAYMENT_AUTHORIZATION_DELETED;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        PARKING_TRANSACTION_ITEM_EVENT_STATUS transactionEvent = PARKING_TRANSACTION_ITEM_EVENT_STATUS.CANCELLED;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataDELETEResponse = gpService.deletePagam(amount, parkingTransactionItemBean.getParkingTransactionItemId(), parkingTransactionItemBean.getShopLogin(),
                parkingTransactionItemBean.getCurrency(), parkingTransactionItemBean.getBankTansactionID(), null, parkingTransactionItemBean.getAcquirerID(),
                parkingTransactionItemBean.getGroupAcquirer(), encodedSecretKey);

        if (gestPayDataDELETEResponse == null) {
            gestPayDataDELETEResponse = new GestPayData();
            gestPayDataDELETEResponse.setTransactionResult("ERROR");
            gestPayDataDELETEResponse.setErrorCode("9999");
            gestPayDataDELETEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            // TODO Definire il flusso della riconciliazione
            //poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH DEL: " + gestPayDataDELETEResponse.getTransactionResult());

        errorCode = gestPayDataDELETEResponse.getErrorCode();
        errorDescription = gestPayDataDELETEResponse.getErrorDescription();
        eventResult = gestPayDataDELETEResponse.getTransactionResult();

        parkingTransactionItemStatusBean = this.generateParkingTransactionStatusEvent(parkingTransactionItemBean.getParkingTransactionBean(), requestID,
                errorCode.equals("0") ? PARKING_TRANSACTION_STATUS_CONST.INFO : PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION,
                newStatus.getValue(), eventResult);

        em.persist(parkingTransactionItemStatusBean);

        parkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(amount, eventSequenceID, parkingTransactionItemBean, gestPayDataDELETEResponse,
                transactionEvent.getValue());

        em.persist(parkingTransactionItemEventBean);

        oldStatus = newStatus;

        return gestPayDataDELETEResponse;
    }

    private void invalidatePendingParkingTransactionItems(ParkingTransactionBean parkingTransactionBean, UserBean userBean, String requestId, GPServiceRemote gpService,
            String myCiceroDecodedSecretKey) {
        System.out.println("****************** invalidatePendingParkingTransactionItems starting... ************************");
        parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.FAILED.getValue());
        Iterator<ParkingTransactionItemBean> it = parkingTransactionBean.getParkingTransactionItemList().iterator();
        while (it.hasNext()) {
            ParkingTransactionItemBean currentParkingTransactionItemBean = it.next();
            currentParkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue());
            em.merge(currentParkingTransactionItemBean);

            //Inizio Invalidazione preautorizzazione pagamento
            Iterator<ParkingTransactionItemEventBean> it2 = currentParkingTransactionItemBean.getParkingTransactionItemEventList().iterator();
            System.out.println("Inizio Invalidazione preautorizzazione pagamento per parking transaction item ID :: "
                    + currentParkingTransactionItemBean.getParkingTransactionItemId());

            while (it2.hasNext()) {
                ParkingTransactionItemEventBean currentParkingTransactionItemEventBean = it2.next();
                if (currentParkingTransactionItemEventBean.getEventType().equals(PARKING_TRANSACTION_ITEM_EVENT_STATUS.AUTHORIZED.getValue())) {
                    System.out.println("Trovato ParkingTransactionItemEventBean ID :: " + currentParkingTransactionItemEventBean.getId() + " amount :: "
                            + currentParkingTransactionItemEventBean.getEventAmount().doubleValue());
                    Integer eventSequenceID = currentParkingTransactionItemBean.getLastEventSequenceID() + 1;
                    Integer statusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;

                    GestPayData gestPayDataDELETEResponse = doPaymentAuthDelete(currentParkingTransactionItemBean, paymentInfoBean, userBean, paymentInfoBean.getId(),
                            currentParkingTransactionItemEventBean.getEventAmount().doubleValue(), statusSequenceID, requestId, eventSequenceID, gpService,
                            myCiceroDecodedSecretKey);

                    System.out.println("Importo di Preautorizzazione :: " + currentParkingTransactionItemEventBean.getEventAmount().doubleValue());
                    System.out.println("Tentativo di Storno => importo :: " + currentParkingTransactionItemEventBean.getEventAmount().doubleValue());

                    if (gestPayDataDELETEResponse.getTransactionResult().equals("ERROR")) {

                        parkingTransactionBean.setToReconcile(true);
                        ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH ERROR",
                                gestPayDataDELETEResponse.getErrorDescription());
                        em.persist(parkingTransactionStatusBean);
                    }

                    if (gestPayDataDELETEResponse.getTransactionResult().equals("KO")) {

                        parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.NOT_RECONCILIABLE.getValue());
                        ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH KO",
                                gestPayDataDELETEResponse.getErrorDescription());
                        em.persist(parkingTransactionStatusBean);

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                                "Deleting payment authorization :: ApproveParking failure");

                    }

                }
            }

            ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                    PARKING_TRANSACTION_STATUS_CONST.INFO, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH OK", "Auth invalidation (OK)");
            em.persist(parkingTransactionStatusBean);

            System.out.println("Fine Invalidazione preautorizzazione pagamento per parking transaction item ID :: "
                    + currentParkingTransactionItemBean.getParkingTransactionItemId());

        }
        em.merge(parkingTransactionBean);

        System.out.println("****************** invalidatePendingParkingTransactionItems finished ************************");

    }

    private ApproveParkingTransactionResult checkPaymentMethod(String requestID, UserTransaction userTransaction, UserBean userBean, Long paymentMethodId,
            String paymentMethodType, String encodedPin, Integer pinCheckMaxAttempts) throws Exception {

        System.out.println("Controllo del metodo di pagamento dell'utente");

        ApproveParkingTransactionResult approveParkingTransactionResult = new ApproveParkingTransactionResult();

        PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();

        if (paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("")) {

            // Errore Pin non ancora inserito
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");

            userTransaction.commit();
            approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_UNAUTHORIZED);
            return approveParkingTransactionResult;
        }

        if (paymentMethodId == null && paymentMethodType == null) {

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found");

            paymentInfoBean = userBean.findDefaultPaymentInfoBean();
        }
        else {

            paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
        }

        // Verifica che il metodo di pagamento selezionato sia in uno stato valido
        if (paymentInfoBean == null) {

            // Il metodo di pagamento selezionato non esiste
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

            userTransaction.commit();

            approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_PAYMENT_WRONG);
            return approveParkingTransactionResult;
        }

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                + paymentMethodType);

        if (!paymentInfoBean.isValid()) {

            // Il metodo di pagamento selezionato non pu� essere utilizzato
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

            userTransaction.commit();

            approveParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_PAYMENT_WRONG);
            return approveParkingTransactionResult;
        }

        /*
         * Boolean creditCardFound = Boolean.FALSE;
         * Boolean voucherFound = Boolean.FALSE;
         * 
         * for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {
         * if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) && (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED ||
         * paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED)) {
         * creditCardFound = Boolean.TRUE;
         * System.out.println("Credit Card found");
         * break;
         * }
         * }
         * 
         * if (!creditCardFound) {
         * 
         * if (!userBean.getVoucherList().isEmpty()) {
         * 
         * for(VoucherBean voucherBean : userBean.getVoucherList()) {
         * if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {
         * voucherFound = Boolean.TRUE;
         * System.out.println("Voucher found");
         * break;
         * }
         * }
         * }
         * 
         * if (!voucherFound) {
         * // Il metodo di pagamento selezionato non pu� essere utilizzato
         * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No credit card or valid voucher found");
         * 
         * userTransaction.commit();
         * 
         * poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PAYMENT_WRONG);
         * return poPApproveShopTransactionResponse;
         * }
         * }
         */

        if (!encodedPin.equals(paymentInfoVoucherBean.getPin())) {

            // Il pin inserito non � valido
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");

            String response = ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_ERROR_PIN;

            // Si sottrae uno al numero di tentativi residui
            Integer pinCheckAttemptsLeft = paymentInfoVoucherBean.getPinCheckAttemptsLeft();
            if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                pinCheckAttemptsLeft--;
            }
            else {
                pinCheckAttemptsLeft = 0;
            }

            if (pinCheckAttemptsLeft == 0) {

                // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                paymentInfoVoucherBean.setDefaultMethod(false);
                paymentInfoVoucherBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                for (PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {

                    if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                            && (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                        paymentInfoBean.setDefaultMethod(false);
                        em.merge(paymentInfoBean);
                    }
                }
            }
            else {

                if (pinCheckAttemptsLeft == 1) {

                    response = ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_ERROR_PIN_LAST_ATTEMPT;
                }
                else {

                    response = ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_ERROR_PIN_ATTEMPTS_LEFT;
                }
            }

            paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

            em.merge(paymentInfoVoucherBean);

            userTransaction.commit();

            approveParkingTransactionResult.setPinCheckMaxAttempts(paymentInfoVoucherBean.getPinCheckAttemptsLeft());
            approveParkingTransactionResult.setStatusCode(response);

            return approveParkingTransactionResult;
        }

        // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
        paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);

        em.merge(paymentInfoVoucherBean);

        return approveParkingTransactionResult;
    }

    private GestPayData doPaymentAuth(ParkingTransactionItemBean parkingTransactionItemBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double amount, Integer statusSequenceID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = null;
        ParkingTransactionStatusBean parkingTransactionItemStatusBean = null;

        newStatus = PARKING_TRANSACTION_ITEM_STATUS_STATUS.PAYMENT_AUTHORIZED;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        PARKING_TRANSACTION_ITEM_EVENT_STATUS transactionEvent = PARKING_TRANSACTION_ITEM_EVENT_STATUS.AUTHORIZED;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        String paymentMethodExpiration = "";

        if (paymentInfoBean != null) {

            CardDepositTransactionBean cardDepositTransactionBean = QueryRepository.findCardDepositTransactionByShopTransactionID(em, paymentInfoBean.getCheckShopTransactionID());
            if (cardDepositTransactionBean != null) {

                String tokenExpiryMonth = cardDepositTransactionBean.getTokenExpiryMonth();
                String tokenExpiryYear = cardDepositTransactionBean.getTokenExpiryYear();

                paymentMethodExpiration = "20" + tokenExpiryYear + tokenExpiryMonth;
            }
        }

        Extension[] extension_array = {};

        GestPayData gestPayDataAUTHResponse = gpService.callPagam(amount, parkingTransactionItemBean.getParkingTransactionItemId(), parkingTransactionItemBean.getShopLogin(),
                parkingTransactionItemBean.getCurrency(), paymentInfoBean.getToken(), null, parkingTransactionItemBean.getAcquirerID(),
                parkingTransactionItemBean.getGroupAcquirer(), encodedSecretKey, paymentMethodExpiration, extension_array, "CUSTOMER");

        if (gestPayDataAUTHResponse == null) {
            gestPayDataAUTHResponse = new GestPayData();
            gestPayDataAUTHResponse.setTransactionResult("ERROR");
            gestPayDataAUTHResponse.setErrorCode("9999");
            gestPayDataAUTHResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            // TODO Definire il flusso della riconciliazione
            //poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH: " + gestPayDataAUTHResponse.getTransactionResult());

        errorCode = gestPayDataAUTHResponse.getErrorCode();
        errorDescription = gestPayDataAUTHResponse.getErrorDescription();
        eventResult = gestPayDataAUTHResponse.getTransactionResult();

        parkingTransactionItemStatusBean = this.generateParkingTransactionStatusEvent(parkingTransactionItemBean.getParkingTransactionBean(), requestID,
                errorCode.equals("0") ? PARKING_TRANSACTION_STATUS_CONST.INFO : PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION,
                newStatus.getValue(), eventResult);
        em.persist(parkingTransactionItemStatusBean);

        parkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(amount, eventSequenceID, parkingTransactionItemBean, gestPayDataAUTHResponse,
                transactionEvent.getValue());

        em.persist(parkingTransactionItemEventBean);

        parkingTransactionItemBean.setPaymentMethodId(paymentMethodId);
        parkingTransactionItemBean.setPaymentMethodType(paymentInfoBean.getType());

        parkingTransactionItemBean.setPaymentToken(paymentInfoBean.getToken());
        parkingTransactionItemBean.setBankTansactionID(gestPayDataAUTHResponse.getBankTransactionID());
        parkingTransactionItemBean.setAuthorizationCode(gestPayDataAUTHResponse.getAuthorizationCode());

        oldStatus = newStatus;

        return gestPayDataAUTHResponse;
    }

}
