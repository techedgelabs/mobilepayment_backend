package com.techedge.mp.core.actions.parking.v2;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.parking.ApproveExtendParkingTransactionResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemEventBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionStatusBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.StatusCodeHelper;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2ApproveExtendParkingAction extends ParkingV2ParkingPaymentAction {

    @Resource
    private EJBContext                             context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager                          em;

    @EJB
    private LoggerService                          loggerService;

    @EJB
    private ParametersService                      parameterService;
    public final static String                     PARAM_PARKING_NOTICE_SPLITTER_SYMBOL = "PARKING_NOTICE_SPLITTER_SYMBOL";

    private Integer                                sequenceID                           = 0;
    private PARKING_TRANSACTION_ITEM_STATUS_STATUS newStatus                            = null;
    private PARKING_TRANSACTION_ITEM_STATUS_STATUS oldStatus                            = null;
    private PaymentInfoBean                        paymentInfoBean                      = null;

    public ParkingV2ApproveExtendParkingAction() {}

    public ApproveExtendParkingTransactionResult execute(ParkingServiceRemote parkingService, GPServiceRemote gpService, String ticketId, String requestId, String encodedPin,
            Integer pinCheckMaxAttempts, String parkingTransactionId, Long paymentMethodId, String paymentMethodType, String lang, String myCiceroDecodedSecretKey,
            Double parkingMaxAmount, Double parkingPreauthAmount) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        ApproveExtendParkingTransactionResult approveExtendParkingTransactionResult = new ApproveExtendParkingTransactionResult();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_INVALID_TICKET);
                return approveExtendParkingTransactionResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_INVALID_TICKET);
                return approveExtendParkingTransactionResult;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User in status " + userStatus + " unauthorized");

                userTransaction.commit();
                approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_UNAUTHORIZED);
                return approveExtendParkingTransactionResult;
            }

            ParkingTransactionBean parkingTransactionBean = null;
            ParkingTransactionItemBean parkingTransactionItemBean = null;

            List<ParkingTransactionItemBean> parkingTransactionItemBeanList = QueryRepository.findActiveParkingTransactionItemByTransactionId(em, parkingTransactionId,
                    PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue());

            /*************************************************************************************/

            //Controllo congruenza degli stati

            if (parkingTransactionItemBeanList.isEmpty()) {
                userTransaction.commit();
                approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE);
                return approveExtendParkingTransactionResult;

            }
            else {
                parkingTransactionItemBean = parkingTransactionItemBeanList.get(0);
                parkingTransactionBean = parkingTransactionItemBean.getParkingTransactionBean();

                if (!(parkingTransactionBean.getStatus().equals(PARKING_TRANSACTION_STATUS.STARTED.getValue()) && parkingTransactionItemBean.getParkingItemStatus().equals(
                        PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue()))) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Extend Parking Transaction Failure");
                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE, "Parking transaction in stato " + parkingTransactionBean.getStatus()
                                    + " e stato item " + parkingTransactionItemBean.getParkingItemStatus());
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();
                    approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE);
                    return approveExtendParkingTransactionResult;
                }

            }

            /*************************************************************************************/

            if (paymentMethodId == null && paymentMethodType == null) {
                //recupero della modalit� di pagamento dalla transazione precedente

                List<ParkingTransactionItemBean> result = QueryRepository.findActiveParkingTransactionItemByTransactionId(em, parkingTransactionId,
                        PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue());
                ParkingTransactionItemBean parkingTransactionItemBeanAuthorized = result.get(0);

                paymentMethodId = parkingTransactionItemBeanAuthorized.getPaymentMethodId();
                paymentMethodType = parkingTransactionItemBeanAuthorized.getPaymentMethodType();

            }

            /****************************************** Check Payment *****************************/

            PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(em, paymentMethodId, paymentMethodType);

            if (paymentInfoBean == null) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Payment Method not found");
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                        ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE, "For current user, Payment Method not found.");
                em.persist(parkingTransactionStatusBean);

                userTransaction.commit();

                approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE);
                return approveExtendParkingTransactionResult;
            }

            if (paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {

                /*
                 * verifica cap da implementare
                 * double capAvailable = 0.0;
                 * 
                 * if (userBean.getCapAvailable() != null) {
                 * capAvailable = userBean.getCapAvailable().doubleValue();
                 * }
                 * 
                 * if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {
                 * 
                 * // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
                 * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                 * "Payment not verified and transaction amount exceeding the cap");
                 * 
                 * userTransaction.commit();
                 * 
                 * approveParkingTransactionResult.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_CAP_FAILURE);
                 * return approveParkingTransactionResult;
                 * }
                 */
            }
            else {

                // Non � possibile pagare una sosta con voucher

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment Method not valid: " + paymentMethodType);
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                        ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE, "Payment with voucher. Payment Method not valid.");
                em.persist(parkingTransactionStatusBean);

                userTransaction.commit();

                approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE);
                return approveExtendParkingTransactionResult;
            }

            approveExtendParkingTransactionResult = this.checkPaymentMethod(requestId, userTransaction, userBean, paymentMethodId, paymentMethodType, encodedPin,
                    pinCheckMaxAttempts);

            if (approveExtendParkingTransactionResult.getStatusCode() != null) {
                return approveExtendParkingTransactionResult;
            }

            System.out.println("Ricerca transazione da avviare");

            /****************************************** END CheckPayment ******************************/

            /****************************************** Payment Flow ********************************/
            // Pagamento con carta di credito
            System.out.println("Pagamento con carta di credito");

            Integer eventSequenceID = 1;
            Integer statusSequenceID = sequenceID;

            Double extendedParkingAmount = parkingTransactionItemBean.getPrice().doubleValue();
            if (extendedParkingAmount.doubleValue() > parkingMaxAmount.doubleValue()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Extended Parking Price Amount :: "
                        + parkingTransactionItemBean.getPrice().doubleValue() + " Exceeds Max Amount :: " + parkingMaxAmount);
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                        ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE, "Extended Parking Price Amount :: "
                                + parkingTransactionItemBean.getPrice().doubleValue() + " greater than Max Amount :: " + parkingMaxAmount);
                em.persist(parkingTransactionStatusBean);

                userTransaction.commit();
                approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE);
                return approveExtendParkingTransactionResult;
            }

            Double amount = parkingTransactionItemBean.getPriceDifference().doubleValue();
            if (amount > parkingPreauthAmount) {

                GestPayData gestPayDataAUTHResponse = this.doPaymentAuth(parkingTransactionItemBean, paymentInfoBean, userBean, paymentMethodId, amount, statusSequenceID,
                        requestId, eventSequenceID, gpService, myCiceroDecodedSecretKey);

                if (gestPayDataAUTHResponse.getTransactionResult().equals("ERROR")) {

                    // TODO Impostare transazione in stato pending
                    parkingTransactionBean.setToReconcile(Boolean.TRUE);
                    parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue());
                    em.merge(parkingTransactionItemBean);
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Extend Parking Transaction Failure");

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_COMUNICATION_FAILURE,
                            "Gest pay Authorization Error :: " + gestPayDataAUTHResponse.getErrorDescription());
                    em.persist(parkingTransactionStatusBean);
                    userTransaction.commit();

                    approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_COMUNICATION_FAILURE);
                    approveExtendParkingTransactionResult.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                    return approveExtendParkingTransactionResult;

                }

                if (gestPayDataAUTHResponse.getTransactionResult().equals("KO")) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Extend Parking Transaction Failure");
                    parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue());
                    em.merge(parkingTransactionItemBean);
                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_AUTH_KO,
                            "Gest pay Authorization KO Error :: " + gestPayDataAUTHResponse.getErrorDescription());
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();

                    approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_AUTH_KO);
                    approveExtendParkingTransactionResult.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

                    if (insufficientFunds(gestPayDataAUTHResponse)) {
                        //userBean.setUserStatus(User.USER_STATUS_BLOCKED);
                        //em.merge(userBean);
                        ParkingTransactionStatusBean parkingTransactionStatusBeanUserBlocked = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                                ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_INSUFFICIENT_AMOUNT,
                                "Gest pay error insufficient funds :: " + gestPayDataAUTHResponse.getErrorDescription());
                        em.persist(parkingTransactionStatusBeanUserBlocked);

                        userTransaction.commit();
                        approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_INSUFFICIENT_AMOUNT);
                        return approveExtendParkingTransactionResult;

                    }

                    return approveExtendParkingTransactionResult;
                }
                System.out.println("Pagamento con carta di credito effettuato con successo");

            }

            /****************************************** END Payment Flow ********************************/

            com.techedge.mp.parking.integration.business.interfaces.ExtendParkingResult extendParkingResult = null;

            String clientOperationID = new IdGenerator().generateId(16).substring(0, 32);

            int attemptsLeft = Integer.valueOf(parameterService.getParamValue(PARKING_PARAM_ATTEMPTS));
            long intervalRetry = Long.valueOf(parameterService.getParamValue(PARKING_PARAM_INTERVAL_RETRY));
            
            while (attemptsLeft > 0) {

                attemptsLeft--;
                System.out.println("ExtendParking attempts left: " + attemptsLeft);

                extendParkingResult = parkingService.extendParking(lang, parkingTransactionBean.getParkingId(), parkingTransactionItemBean.getRequestedEndTime(), clientOperationID);

                if (extendParkingResult == null) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EJB communication error");

                    approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE);
                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE, "ExtendParkingResult not existing. EJB communication error.");
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();
                    return approveExtendParkingTransactionResult;
                }
                
                // Gestione tentativo estensione su sosta terminata
                if(extendParkingResult.getStatusCode().equals(StatusCodeHelper.PARKING_NOT_FOUND)){
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Application error: "
                            + approveExtendParkingTransactionResult.getStatusCode());

                    approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_ALREADY_CLOSE);
                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_ALREADY_CLOSE, "ExtendParkingResult failure caused by existing parking closed.");
                    em.persist(parkingTransactionStatusBean);
                    userTransaction.commit();
                    return approveExtendParkingTransactionResult;
                }


                if (!extendParkingResult.getStatusCode().equals(StatusCodeHelper.EXTEND_PARKING_OK)) {

                    if (attemptsLeft == 0) {

                        // Gestione errore applicativo

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Application error: "
                                + approveExtendParkingTransactionResult.getStatusCode());

                        approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE);
                        ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                                ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE, "ExtendParkingResult failure caused by application error.");
                        em.persist(parkingTransactionStatusBean);

                        userTransaction.commit();
                        return approveExtendParkingTransactionResult;
                    }
                }
                else {
                    break;
                }

                Thread.sleep(intervalRetry);
            }

            parkingTransactionBean.setEstimatedParkingEndTime(extendParkingResult.getParkingEndTime());
            em.merge(parkingTransactionBean);

            parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue());
            parkingTransactionItemBean.setRequestedEndTime(extendParkingResult.getRequestedEndTime());
            parkingTransactionItemBean.setParkingStartTime(extendParkingResult.getParkingStartTime());
            parkingTransactionItemBean.setParkingEndTime(extendParkingResult.getParkingEndTime());
            parkingTransactionItemBean.setParkingTimeCorrection(extendParkingResult.getParkingTimeCorrection());
            parkingTransactionItemBean.setPrice(extendParkingResult.getPrice());
            parkingTransactionItemBean.setPreviousPrice(extendParkingResult.getPreviousPrice());
            parkingTransactionItemBean.setPriceDifference(extendParkingResult.getPriceDifference());
            parkingTransactionItemBean.setParkingTransactionBean(parkingTransactionBean);

            String pan = paymentInfoBean.getPan();
            parkingTransactionItemBean.setPan(pan);

            em.merge(parkingTransactionItemBean);

            approveExtendParkingTransactionResult.setTransactionId(parkingTransactionBean.getParkingTransactionId());

            approveExtendParkingTransactionResult.setParkingId(extendParkingResult.getParkingId());
            approveExtendParkingTransactionResult.setStallCode(extendParkingResult.getStallCode());
            approveExtendParkingTransactionResult.setPlateNumber(extendParkingResult.getPlateNumber());
            approveExtendParkingTransactionResult.setRequestedEndTime(extendParkingResult.getRequestedEndTime());
            approveExtendParkingTransactionResult.setParkingStartTime(extendParkingResult.getParkingStartTime());
            approveExtendParkingTransactionResult.setParkingEndTime(extendParkingResult.getParkingEndTime());
            approveExtendParkingTransactionResult.setParkingTimeCorrection(extendParkingResult.getParkingTimeCorrection());
            approveExtendParkingTransactionResult.setPrice(extendParkingResult.getPrice());
            approveExtendParkingTransactionResult.setNotice(extendParkingResult.getNotice());
            approveExtendParkingTransactionResult.setPaymentMethodId(parkingTransactionItemBean.getPaymentMethodId());

            approveExtendParkingTransactionResult.setParkingZoneId(extendParkingResult.getParkingZoneId());
            approveExtendParkingTransactionResult.setParkingZoneName(parkingTransactionBean.getParkingZoneName());
            approveExtendParkingTransactionResult.setParkingZoneDescription(parkingTransactionBean.getParkingZoneDescription());
            approveExtendParkingTransactionResult.setCityId(parkingTransactionBean.getCityId());
            approveExtendParkingTransactionResult.setCityName(parkingTransactionBean.getCityName());
            approveExtendParkingTransactionResult.setAdministrativeAreaLevel2Code(parkingTransactionBean.getAdministrativeAreaLevel2Code());

            approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_SUCCESS);

            ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                    PARKING_TRANSACTION_STATUS_CONST.INFO, PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION,
                    ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_SUCCESS, "Approve extend parking successfully created.");
            em.persist(parkingTransactionStatusBean);

            userTransaction.commit();

            return approveExtendParkingTransactionResult;

        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED extimate parking price with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }

    private ApproveExtendParkingTransactionResult checkPaymentMethod(String requestID, UserTransaction userTransaction, UserBean userBean, Long paymentMethodId,
            String paymentMethodType, String encodedPin, Integer pinCheckMaxAttempts) throws Exception {

        System.out.println("Controllo del metodo di pagamento dell'utente");

        ApproveExtendParkingTransactionResult approveExtendParkingTransactionResult = new ApproveExtendParkingTransactionResult();

        PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();

        if (paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("")) {

            // Errore Pin non ancora inserito
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");

            userTransaction.commit();
            approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_UNAUTHORIZED);
            return approveExtendParkingTransactionResult;
        }

        if (paymentMethodId == null && paymentMethodType == null) {

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found");

            paymentInfoBean = userBean.findDefaultPaymentInfoBean();
        }
        else {

            paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
        }

        // Verifica che il metodo di pagamento selezionato sia in uno stato valido
        if (paymentInfoBean == null) {

            // Il metodo di pagamento selezionato non esiste
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

            userTransaction.commit();

            approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_PAYMENT_WRONG);
            return approveExtendParkingTransactionResult;
        }

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                + paymentMethodType);

        if (!paymentInfoBean.isValid()) {

            // Il metodo di pagamento selezionato non pu� essere utilizzato
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

            userTransaction.commit();

            approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_PAYMENT_WRONG);
            return approveExtendParkingTransactionResult;
        }

        if (!encodedPin.equals(paymentInfoVoucherBean.getPin())) {

            // Il pin inserito non � valido
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");

            String response = ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_CREATE_ERROR_PIN;

            // Si sottrae uno al numero di tentativi residui
            Integer pinCheckAttemptsLeft = paymentInfoVoucherBean.getPinCheckAttemptsLeft();
            if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                pinCheckAttemptsLeft--;
            }
            else {
                pinCheckAttemptsLeft = 0;
            }

            if (pinCheckAttemptsLeft == 0) {

                // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                paymentInfoVoucherBean.setDefaultMethod(false);
                paymentInfoVoucherBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                for (PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {

                    if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                            && (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                        paymentInfoBean.setDefaultMethod(false);
                        em.merge(paymentInfoBean);
                    }
                }
            }
            else {

                if (pinCheckAttemptsLeft == 1) {

                    response = ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_ERROR_PIN_LAST_ATTEMPT;
                }
                else {

                    response = ResponseHelper.PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_PIN_ATTEMPTS_LEFT;
                }
            }

            paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

            em.merge(paymentInfoVoucherBean);

            userTransaction.commit();

            approveExtendParkingTransactionResult.setPinCheckMaxAttempts(paymentInfoVoucherBean.getPinCheckAttemptsLeft());
            approveExtendParkingTransactionResult.setStatusCode(response);

            return approveExtendParkingTransactionResult;
        }

        // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
        paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);

        em.merge(paymentInfoVoucherBean);

        return approveExtendParkingTransactionResult;
    }

    private GestPayData doPaymentAuth(ParkingTransactionItemBean parkingTransactionItemBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double amount, Integer statusSequenceID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = null;
        ParkingTransactionStatusBean parkingTransactionItemStatusBean = null;

        newStatus = PARKING_TRANSACTION_ITEM_STATUS_STATUS.PAYMENT_AUTHORIZED;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        PARKING_TRANSACTION_ITEM_EVENT_STATUS transactionEvent = PARKING_TRANSACTION_ITEM_EVENT_STATUS.AUTHORIZED;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        String paymentMethodExpiration = "";

        if (paymentInfoBean != null) {

            CardDepositTransactionBean cardDepositTransactionBean = QueryRepository.findCardDepositTransactionByShopTransactionID(em, paymentInfoBean.getCheckShopTransactionID());
            if (cardDepositTransactionBean != null) {

                String tokenExpiryMonth = cardDepositTransactionBean.getTokenExpiryMonth();
                String tokenExpiryYear = cardDepositTransactionBean.getTokenExpiryYear();

                paymentMethodExpiration = "20" + tokenExpiryYear + tokenExpiryMonth;
            }
        }

        Extension[] extension_array = {};

        GestPayData gestPayDataAUTHResponse = gpService.callPagam(amount, parkingTransactionItemBean.getParkingTransactionItemId(), parkingTransactionItemBean.getShopLogin(),
                parkingTransactionItemBean.getCurrency(), paymentInfoBean.getToken(), null, parkingTransactionItemBean.getAcquirerID(),
                parkingTransactionItemBean.getGroupAcquirer(), encodedSecretKey, paymentMethodExpiration, extension_array, "CUSTOMER");

        if (gestPayDataAUTHResponse == null) {
            gestPayDataAUTHResponse = new GestPayData();
            gestPayDataAUTHResponse.setTransactionResult("ERROR");
            gestPayDataAUTHResponse.setErrorCode("9999");
            gestPayDataAUTHResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            // TODO Definire il flusso della riconciliazione
            //poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH: " + gestPayDataAUTHResponse.getTransactionResult());

        errorCode = gestPayDataAUTHResponse.getErrorCode();
        errorDescription = gestPayDataAUTHResponse.getErrorDescription();
        eventResult = gestPayDataAUTHResponse.getTransactionResult();

        parkingTransactionItemStatusBean = generateParkingTransactionStatusEvent(parkingTransactionItemBean.getParkingTransactionBean(), requestID,
                errorCode.equals("0") ? PARKING_TRANSACTION_STATUS_CONST.INFO : PARKING_TRANSACTION_STATUS_CONST.ERROR,
                PARKING_TRANSACTION_STATUS_CONST.EXTEND_PARKING_TRANSACTION, newStatus.getValue(), eventResult);

        em.persist(parkingTransactionItemStatusBean);

        parkingTransactionItemEventBean = generateParkingTransactionItemEvent(amount, eventSequenceID, parkingTransactionItemBean, gestPayDataAUTHResponse,
                transactionEvent.getValue());

        em.persist(parkingTransactionItemEventBean);

        parkingTransactionItemBean.setPaymentMethodId(paymentMethodId);
        parkingTransactionItemBean.setPaymentMethodType(paymentInfoBean.getType());

        parkingTransactionItemBean.setPaymentToken(paymentInfoBean.getToken());
        parkingTransactionItemBean.setBankTansactionID(gestPayDataAUTHResponse.getBankTransactionID());
        parkingTransactionItemBean.setAuthorizationCode(gestPayDataAUTHResponse.getAuthorizationCode());

        oldStatus = newStatus;

        return gestPayDataAUTHResponse;
    }

}
