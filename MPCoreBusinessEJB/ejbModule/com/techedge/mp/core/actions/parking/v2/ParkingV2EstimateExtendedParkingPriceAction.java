package com.techedge.mp.core.actions.parking.v2;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.EstimateExtendedParkingPriceResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionStatusBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.StatusCodeHelper;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2EstimateExtendedParkingPriceAction extends ParkingV2ParkingPaymentAction {

    @Resource
    private EJBContext         context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager      em;

    @EJB
    private LoggerService      loggerService;

    @EJB
    private ParametersService  parameterService;
    public final static String PARAM_PARKING_NOTICE_SPLITTER_SYMBOL = "PARKING_NOTICE_SPLITTER_SYMBOL";

    public ParkingV2EstimateExtendedParkingPriceAction() {}

    public EstimateExtendedParkingPriceResult execute(ParkingServiceRemote parkingService, String ticketId, String requestId, String parkingTransactionId, Date requestedEndTime,
            String lang, String acquirerID, String currency, String groupAcquirer, String serverName, String shopLogin, Double parkingMaxAmount) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        EstimateExtendedParkingPriceResult estimateExtendedParkingPriceResult = new EstimateExtendedParkingPriceResult();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_INVALID_TICKET);
                return estimateExtendedParkingPriceResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_INVALID_TICKET);
                return estimateExtendedParkingPriceResult;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User in status " + userStatus + " unauthorized");

                userTransaction.commit();
                estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_UNAUTHORIZED);
                return estimateExtendedParkingPriceResult;
            }

            ParkingTransactionBean parkingTransactionBean = null;
            ParkingTransactionItemBean parkingTransactionItemBean = null;

            List<ParkingTransactionItemBean> parkingTransactionItemBeanList = QueryRepository.findActiveParkingTransactionItemByTransactionId(em, parkingTransactionId,
                    PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue());

            /*************************************************************************************/

            //Controllo congruenza degli stati
            Long delta = 60000l;

            if (!parkingTransactionItemBeanList.isEmpty()) {

                if (parkingTransactionItemBeanList.size() > 1) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Parking Transaction Item not found");

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION,
                            ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_FAILURE, "Parking Transaction Item not found");
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();
                    estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_FAILURE);
                    return estimateExtendedParkingPriceResult;
                }

                parkingTransactionItemBean = parkingTransactionItemBeanList.get(0);
                parkingTransactionBean = parkingTransactionItemBean.getParkingTransactionBean();

                // Ricerca il massimo parkingEndTime degli item: se si richiede la stima di una sosta con termine precedente a questo timestamp bisogna restituire un errore
                Date parkingEndTime = null;
                for (ParkingTransactionItemBean parkingTransactionItemBeanElement : parkingTransactionBean.getParkingTransactionItemList()) {
                    if (parkingTransactionItemBeanElement.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue())
                            || parkingTransactionItemBeanElement.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue()))
                        continue;
                    if (parkingEndTime == null || parkingEndTime.getTime() < parkingTransactionItemBeanElement.getParkingEndTime().getTime()) {
                        parkingEndTime = parkingTransactionItemBeanElement.getParkingEndTime();
                    }
                }

                System.out.println("requestedEndTime :: " + new Date(requestedEndTime.getTime()) + " parkingEndTime :: " + new Date(parkingEndTime.getTime()));
                if (requestedEndTime.getTime() < parkingEndTime.getTime() + delta) {

                    // E' stata richiesta la stima dell'estensione di una sosta con una data precedente a quella gi� avviata
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "requestedEndTime (" + requestedEndTime.getTime()
                            + ") < parkingEndTime (" + parkingEndTime.getTime() + ")");

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION,
                            ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_INVALID_REQUESTED_END_TIME, "requestedEndTime :: " + new Date(requestedEndTime.getTime())
                                    + " parkingEndTime :: " + new Date(parkingEndTime.getTime()));
                    em.persist(parkingTransactionStatusBean);
                    userTransaction.commit();
                    estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_INVALID_REQUESTED_END_TIME);
                    return estimateExtendedParkingPriceResult;
                }
            }
            else {
                parkingTransactionItemBean = new ParkingTransactionItemBean();
                parkingTransactionBean = QueryRepository.findParkingTransactionByParkingTransactionId(em, userBean, parkingTransactionId);

                Date now = new Date();
                if (now.getTime() > parkingTransactionBean.getEstimatedParkingEndTime().getTime() + delta) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "now (" + now + ") > parkingEstimatedParkingEndTime ("
                            + parkingTransactionBean.getEstimatedParkingEndTime() + ")");

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION,
                            ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_ALREADY_CLOSED,
                            "now (" + now + ") > parkingEstimatedParkingEndTime (" + parkingTransactionBean.getEstimatedParkingEndTime() + ")");
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();
                    estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_ALREADY_CLOSED);
                    return estimateExtendedParkingPriceResult;
                }

                if (requestedEndTime.getTime() < parkingTransactionBean.getEstimatedParkingEndTime().getTime() + delta) {

                    // E' stata richiesta la stima dell'estensione di una sosta con una data precedente a quella gi� avviata
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "requestedEndTime (" + requestedEndTime
                            + ") < parkingEndTime (" + parkingTransactionBean.getEstimatedParkingEndTime() + ")");

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION,
                            ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_INVALID_REQUESTED_END_TIME, "requestedEndTime (" + requestedEndTime + ") < parkingEndTime ("
                                    + parkingTransactionBean.getEstimatedParkingEndTime() + ")");
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();
                    estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_INVALID_REQUESTED_END_TIME);
                    return estimateExtendedParkingPriceResult;
                }

                String parkingTransactionItemId = new IdGenerator().generateId(16).substring(0, 30);
                parkingTransactionItemBean.setParkingTransactionItemId(parkingTransactionItemId);
                parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue());

                //                if (!(parkingTransactionBean.getStatus().equals(PARKING_TRANSACTION_STATUS.STARTED.getValue()) && parkingTransactionItemBean.getParkingItemStatus().equals(
                //                        PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue()))) {
                //                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Estimate Extend Parking Transaction Failure");
                //                    userTransaction.commit();
                //                    estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_FAILURE);
                //                    return estimateExtendedParkingPriceResult;
                //                }

            }

            /*************************************************************************************/

            com.techedge.mp.parking.integration.business.interfaces.EstimateExtendedParkingPriceResult estimateExtendedParkingPriceRemoteResult = parkingService.estimateExtendedParkingPrice(
                    lang, parkingTransactionBean.getParkingId(), requestedEndTime);

            if (estimateExtendedParkingPriceRemoteResult == null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EJB communication error");

                estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_FAILURE);

                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION,
                        ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_FAILURE, "cannot retrieve data from remote service");
                em.persist(parkingTransactionStatusBean);

                userTransaction.commit();
                return estimateExtendedParkingPriceResult;
            }

            if (!estimateExtendedParkingPriceRemoteResult.getStatusCode().equals(StatusCodeHelper.ESTIMATE_EXTEND_PARKING_PRICE_OK)) {

                // Gestione errore applicativo

                if (estimateExtendedParkingPriceRemoteResult.getStatusCode().equals(StatusCodeHelper.ESTIMATE_EXTEND_PARKING_PRICE_CLOSED)) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                            "Application error: " + estimateExtendedParkingPriceResult.getStatusCode());

                    estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_ALREADY_CLOSED);

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION,
                            ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_ALREADY_CLOSED, "Parking already closed");
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();
                    return estimateExtendedParkingPriceResult;
                }

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Application error: " + estimateExtendedParkingPriceResult.getStatusCode());

                estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_FAILURE);

                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION,
                        ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_FAILURE, "");
                em.persist(parkingTransactionStatusBean);

                userTransaction.commit();
                return estimateExtendedParkingPriceResult;
            }

            if (estimateExtendedParkingPriceRemoteResult.getPrice().doubleValue() > parkingMaxAmount.doubleValue()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Application error: " + estimateExtendedParkingPriceResult.getStatusCode());

                estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_EXCEEDED);
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION,
                        ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_EXCEEDED, "parking price amount " + estimateExtendedParkingPriceRemoteResult.getPrice().doubleValue()
                                + " > parkingMaxAmount " + parkingMaxAmount.doubleValue());
                em.persist(parkingTransactionStatusBean);
                userTransaction.commit();
                return estimateExtendedParkingPriceResult;
            }

            Long lastPaymentMethodId = null;
            for (ParkingTransactionItemBean parkingTransactionItemBeanElement : parkingTransactionBean.getParkingTransactionItemList()) {
                if (parkingTransactionItemBeanElement.getPaymentMethodId() != null) {
                    lastPaymentMethodId = parkingTransactionItemBeanElement.getPaymentMethodId();
                }
            }

            parkingTransactionItemBean.setPrice(estimateExtendedParkingPriceRemoteResult.getPrice());
            parkingTransactionItemBean.setCurrentPrice(estimateExtendedParkingPriceRemoteResult.getCurrentPrice());
            parkingTransactionItemBean.setPriceDifference(estimateExtendedParkingPriceRemoteResult.getPriceDifference());

            parkingTransactionItemBean.setRequestedEndTime(estimateExtendedParkingPriceRemoteResult.getRequestedEndTime());
            parkingTransactionItemBean.setParkingEndTime(estimateExtendedParkingPriceRemoteResult.getParkingEndTime());
            parkingTransactionItemBean.setParkingStartTime(estimateExtendedParkingPriceRemoteResult.getParkingStartTime());
            parkingTransactionItemBean.setParkingTimeCorrection(estimateExtendedParkingPriceRemoteResult.getParkingTimeCorrection());

            parkingTransactionItemBean.setAcquirerID(acquirerID);
            parkingTransactionItemBean.setCurrency(currency);
            parkingTransactionItemBean.setGroupAcquirer(groupAcquirer);
            parkingTransactionItemBean.setServerName(serverName);
            parkingTransactionItemBean.setShopLogin(shopLogin);

            parkingTransactionItemBean.setParkingTransactionBean(parkingTransactionBean);
            em.persist(parkingTransactionItemBean);

            estimateExtendedParkingPriceResult.setTransactionId(parkingTransactionBean.getParkingTransactionId());

            estimateExtendedParkingPriceResult.setParkingId(estimateExtendedParkingPriceRemoteResult.getParkingId());
            estimateExtendedParkingPriceResult.setStallCode(estimateExtendedParkingPriceRemoteResult.getStallCode());
            estimateExtendedParkingPriceResult.setPlateNumber(estimateExtendedParkingPriceRemoteResult.getPlateNumber());
            estimateExtendedParkingPriceResult.setRequestedEndTime(estimateExtendedParkingPriceRemoteResult.getRequestedEndTime());
            estimateExtendedParkingPriceResult.setParkingStartTime(estimateExtendedParkingPriceRemoteResult.getParkingStartTime());
            estimateExtendedParkingPriceResult.setParkingEndTime(estimateExtendedParkingPriceRemoteResult.getParkingEndTime());
            estimateExtendedParkingPriceResult.setParkingTimeCorrection(estimateExtendedParkingPriceRemoteResult.getParkingTimeCorrection());
            estimateExtendedParkingPriceResult.setPrice(estimateExtendedParkingPriceRemoteResult.getPrice());
            estimateExtendedParkingPriceResult.setNotice(estimateExtendedParkingPriceRemoteResult.getNotice());
            estimateExtendedParkingPriceResult.setPaymentMethodId(lastPaymentMethodId);

            estimateExtendedParkingPriceResult.setParkingZoneId(estimateExtendedParkingPriceRemoteResult.getParkingZoneId());
            estimateExtendedParkingPriceResult.setParkingZoneName(parkingTransactionBean.getParkingZoneName());
            estimateExtendedParkingPriceResult.setParkingZoneDescription(parkingTransactionBean.getParkingZoneDescription());
            estimateExtendedParkingPriceResult.setCityId(parkingTransactionBean.getCityId());
            estimateExtendedParkingPriceResult.setCityName(parkingTransactionBean.getCityName());
            estimateExtendedParkingPriceResult.setAdministrativeAreaLevel2Code(parkingTransactionBean.getAdministrativeAreaLevel2Code());

            estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_SUCCESS);
            ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                    PARKING_TRANSACTION_STATUS_CONST.INFO, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION,
                    ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_SUCCESS, "estimate extended parking price successfully created");
            em.persist(parkingTransactionStatusBean);

            userTransaction.commit();

            return estimateExtendedParkingPriceResult;

        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED extimate parking price with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }
}
