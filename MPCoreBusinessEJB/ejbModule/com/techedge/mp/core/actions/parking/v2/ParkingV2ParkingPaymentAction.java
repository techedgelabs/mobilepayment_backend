package com.techedge.mp.core.actions.parking.v2;

import java.sql.Timestamp;
import java.util.Date;

import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemEventBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionStatusBean;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public class ParkingV2ParkingPaymentAction {
    protected static final String PAYMENT_REJECTED_ERROR_CODE    = "19";
    protected static final String PAYMENT_NOT_SUFFICIENT_FUNDS_1 = "not sufficient funds";
    protected static final String PAYMENT_NOT_SUFFICIENT_FUNDS_2 = "not+sufficient+funds";
    protected static final String PARKING_PARAM_ATTEMPTS = "PARKING_ATTEMPTS";     
    protected static final String PARKING_PARAM_INTERVAL_RETRY = "PARKING_INTERVAL_RETRY"; 


    public ParkingV2ParkingPaymentAction() {}

    protected ParkingTransactionStatusBean generateParkingTransactionStatusEvent(ParkingTransactionBean parkingTransactionBean,
            String requestID, String level, String status, String subStatus, String subStatusDescription) {

        System.out.println("Scrittura stato " + status + ", substatus " + subStatus + ", description " + subStatusDescription);
        
        ParkingTransactionStatusBean parkingTransactionItemStatusBean = new ParkingTransactionStatusBean();
        Integer statusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;

        parkingTransactionItemStatusBean.setSequenceID(statusSequenceID);
        parkingTransactionItemStatusBean.setRequestID(requestID);
        parkingTransactionItemStatusBean.setTimestamp(new Timestamp(new Date().getTime()));
        parkingTransactionItemStatusBean.setStatus(status);
        parkingTransactionItemStatusBean.setSubStatus(subStatus);
        parkingTransactionItemStatusBean.setSubStatusDescription(subStatusDescription);
        parkingTransactionItemStatusBean.setParkingTransactionBean(parkingTransactionBean);
        parkingTransactionItemStatusBean.setLevel(level);
        
        parkingTransactionBean.getParkingTransactionStatusList().add(parkingTransactionItemStatusBean);

        return parkingTransactionItemStatusBean;
    }

    protected ParkingTransactionItemEventBean generateParkingTransactionItemEvent(Double amount, Integer eventSequenceID, ParkingTransactionItemBean parkingTransactionItemBean,
            GestPayData gestPayData, String event) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = new ParkingTransactionItemEventBean();

        parkingTransactionItemEventBean.setSequenceID(eventSequenceID);
        parkingTransactionItemEventBean.setErrorCode(gestPayData.getErrorCode());
        parkingTransactionItemEventBean.setErrorDescription(gestPayData.getErrorDescription());
        parkingTransactionItemEventBean.setParkingTransactionItemBean(parkingTransactionItemBean);
        parkingTransactionItemEventBean.setTransactionResult(gestPayData.getTransactionResult());
        parkingTransactionItemEventBean.setEventType(event);
        parkingTransactionItemEventBean.setEventAmount(amount);

        return parkingTransactionItemEventBean;
    }

    protected boolean insufficientFunds(GestPayData gestPayDataAUTHResponse) {
        return gestPayDataAUTHResponse.getErrorCode().equals(PAYMENT_REJECTED_ERROR_CODE)
                && (gestPayDataAUTHResponse.getErrorDescription().equals(PAYMENT_NOT_SUFFICIENT_FUNDS_1) || gestPayDataAUTHResponse.getErrorDescription().equals(
                        PAYMENT_NOT_SUFFICIENT_FUNDS_2));
    }
}
