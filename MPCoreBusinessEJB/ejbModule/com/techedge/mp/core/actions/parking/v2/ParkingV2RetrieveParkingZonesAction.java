package com.techedge.mp.core.actions.parking.v2;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.ParkingCity;
import com.techedge.mp.core.business.interfaces.parking.ParkingCityZoneData;
import com.techedge.mp.core.business.interfaces.parking.ParkingZone;
import com.techedge.mp.core.business.interfaces.parking.RetrieveParkingZonesResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.CitiesParkingZone;
import com.techedge.mp.parking.integration.business.interfaces.StatusCodeHelper;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2RetrieveParkingZonesAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public ParkingV2RetrieveParkingZonesAction() {}

    public RetrieveParkingZonesResult execute(ParkingServiceRemote parkingService, String ticketId, String requestId, double latitude, double longitude, String lang,
            BigDecimal accuracy) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        RetrieveParkingZonesResult retrieveParkingZonesResult = new RetrieveParkingZonesResult();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                retrieveParkingZonesResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PARKING_ZONES_INVALID_TICKET);
                return retrieveParkingZonesResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                retrieveParkingZonesResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PARKING_ZONES_INVALID_TICKET);
                return retrieveParkingZonesResult;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User in status " + userStatus + " unauthorized");

                userTransaction.commit();
                retrieveParkingZonesResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PARKING_ZONES_UNAUTHORIZED);
                return retrieveParkingZonesResult;
            }

            BigDecimal lat = new BigDecimal(latitude);
            BigDecimal lon = new BigDecimal(longitude);
            
            com.techedge.mp.parking.integration.business.interfaces.RetrieveParkingZonesResult retrieveParkingZonesRemoteResult = parkingService.retrieveParkingZones(lang, lat,
                    lon, accuracy);

            if (retrieveParkingZonesRemoteResult == null) {

                // Gestione errore comunicazione con EJB

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EJB communication error");

                retrieveParkingZonesResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PARKING_ZONES_FAILURE);
                userTransaction.commit();
                return retrieveParkingZonesResult;
            }

            if (!retrieveParkingZonesRemoteResult.getStatusCode().equals(StatusCodeHelper.RETRIEVE_PARKING_ZONES_OK)) {

                // Gestione errore applicativo

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Application error: " + retrieveParkingZonesRemoteResult.getStatusCode());

                retrieveParkingZonesResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PARKING_ZONES_FAILURE);
                userTransaction.commit();
                return retrieveParkingZonesResult;
            }
            
            Boolean zoneFound = Boolean.FALSE;

            for (CitiesParkingZone citiesParkingZoneRemote : retrieveParkingZonesRemoteResult.getCitiesParkingZones()) {
                
                ParkingCityZoneData parkingCityZoneData = new ParkingCityZoneData();
                
                com.techedge.mp.parking.integration.business.interfaces.ParkingCity parkingCityRemote = citiesParkingZoneRemote.getCity();
                ParkingCity parkingCity = new ParkingCity();
                parkingCity.setAdministrativeAreaLevel2Code(parkingCityRemote.getAdministrativeAreaLevel2Code());
                parkingCity.setCityId(parkingCityRemote.getCityId());
                parkingCity.setId(parkingCityRemote.getId());
                parkingCity.setLatitude(parkingCityRemote.getLatitude());
                parkingCity.setLongitude(parkingCityRemote.getLongitude());
                parkingCity.setName(parkingCityRemote.getName());
                parkingCity.setStickerRequired(parkingCityRemote.getStickerRequired());
                parkingCity.setStickerRequiredNotice(parkingCityRemote.getStickerRequiredNotice());
                parkingCity.setUrl(parkingCityRemote.getUrl());
                parkingCityZoneData.setParkingCity(parkingCity);
                
                List<com.techedge.mp.parking.integration.business.interfaces.ParkingZone> parkingZonesRemote = citiesParkingZoneRemote.getParkingZoneList();
                List<ParkingZone> parkingZones = new LinkedList<ParkingZone>();
                for (com.techedge.mp.parking.integration.business.interfaces.ParkingZone parkingZoneRemote : parkingZonesRemote) {
                    ParkingZone parkingZone = new ParkingZone();

                    parkingZone.setDescription(parkingZoneRemote.getDescription());
                    parkingZone.setId(parkingZoneRemote.getId());
                    parkingZone.setName(parkingZoneRemote.getName());
                    parkingZone.setNotice(parkingZoneRemote.getNotice());
                    parkingZone.setStallCodeRequired(parkingZoneRemote.getStallCodeRequired());
                    parkingZone.setVendorId(parkingZoneRemote.getVendorId());
                    parkingZone.setVendorName(parkingZoneRemote.getVendorName());
                    parkingZone.setVendorUrl(parkingZoneRemote.getVendorUrl());
                    parkingZone.setCityId(parkingCityRemote.getCityId());
                    parkingZone.setStickerRequired(parkingCityRemote.getStickerRequired());
                    parkingZone.setStickerRequiredNotice(parkingCityRemote.getStickerRequiredNotice());
                    parkingZones.add(parkingZone);
                    
                    zoneFound = Boolean.TRUE;
                }
                parkingCityZoneData.setParkingZoneList(parkingZones);
                retrieveParkingZonesResult.getParkingCityZoneData().add(parkingCityZoneData);
                
            }
            
            retrieveParkingZonesResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PARKING_ZONES_SUCCESS);
            
            if (!zoneFound) {
                retrieveParkingZonesResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PARKING_ZONES_NOT_FOUND);
            }
            
            userTransaction.commit();
            return retrieveParkingZonesResult;

        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED retrieve parking zones with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }

}
