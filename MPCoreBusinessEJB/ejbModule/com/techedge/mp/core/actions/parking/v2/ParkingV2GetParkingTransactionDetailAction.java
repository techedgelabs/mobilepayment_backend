package com.techedge.mp.core.actions.parking.v2;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.GetParkingTransactionDetailResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2GetParkingTransactionDetailAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public ParkingV2GetParkingTransactionDetailAction() {}

    public GetParkingTransactionDetailResult execute(String ticketID, String requestID, String parkingTransactionId) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            GetParkingTransactionDetailResult getParkingTransactionDetailResult = null;

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                getParkingTransactionDetailResult = new GetParkingTransactionDetailResult();
                getParkingTransactionDetailResult.setStatusCode(ResponseHelper.PARKING_GET_PARKING_TRANSACTION_DETAIL_INVALID_TICKET);
                return getParkingTransactionDetailResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                getParkingTransactionDetailResult = new GetParkingTransactionDetailResult();
                getParkingTransactionDetailResult.setStatusCode(ResponseHelper.PARKING_GET_PARKING_TRANSACTION_DETAIL_INVALID_TICKET);
                return getParkingTransactionDetailResult;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to retrieve pending refuel in status " + userStatus);

                userTransaction.commit();

                getParkingTransactionDetailResult = new GetParkingTransactionDetailResult();
                getParkingTransactionDetailResult.setStatusCode(ResponseHelper.PARKING_GET_PARKING_TRANSACTION_DETAIL_UNAUTHORIZED);
                return getParkingTransactionDetailResult;
            }

            // Ricerca la transazione di sosta
            ParkingTransactionBean parkingTransactionBean = QueryRepository.findParkingTransactionByParkingTransactionId(em, userBean, parkingTransactionId);

            if (parkingTransactionBean == null) {

                // Non esiste una transazione attiva
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Transaction not found");

                userTransaction.commit();

                getParkingTransactionDetailResult = new GetParkingTransactionDetailResult();
                getParkingTransactionDetailResult.setStatusCode(ResponseHelper.PARKING_GET_PARKING_TRANSACTION_DETAIL_FAILURE);
                return getParkingTransactionDetailResult;
            }

            getParkingTransactionDetailResult = new GetParkingTransactionDetailResult();
            getParkingTransactionDetailResult.setParkingTransaction(parkingTransactionBean.toParkingTransaction());
            getParkingTransactionDetailResult.setStatusCode(ResponseHelper.PARKING_GET_PARKING_TRANSACTION_DETAIL_SUCCESS);
            
            parkingTransactionBean.setUserNotified(Boolean.TRUE);
            em.merge(parkingTransactionBean);

            userTransaction.commit();

            return getParkingTransactionDetailResult;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieving parking transaction detail with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}