package com.techedge.mp.core.actions.parking.v2;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.EstimateEndParkingPriceResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.StatusCodeHelper;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2EstimateEndParkingPriceAction extends ParkingV2ParkingPaymentAction {

    @Resource
    private EJBContext         context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager      em;

    @EJB
    private LoggerService      loggerService;

    @EJB
    private ParametersService  parameterService;
    public final static String PARAM_PARKING_NOTICE_SPLITTER_SYMBOL = "PARKING_NOTICE_SPLITTER_SYMBOL";

    public ParkingV2EstimateEndParkingPriceAction() {}

    public EstimateEndParkingPriceResult execute(ParkingServiceRemote parkingService, String ticketId, String requestId, String parkingTransactionId, String lang)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        EstimateEndParkingPriceResult estimateEndParkingPriceResult = new EstimateEndParkingPriceResult();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_INVALID_TICKET);
                return estimateEndParkingPriceResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_INVALID_TICKET);
                return estimateEndParkingPriceResult;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User in status " + userStatus + " unauthorized");

                userTransaction.commit();
                estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_UNAUTHORIZED);
                return estimateEndParkingPriceResult;
            }

            ParkingTransactionBean parkingTransactionBean = null;

            List<ParkingTransactionItemBean> parkingTransactionItemBeanList = QueryRepository.findParkingTransactionItemByTransactionId(em, parkingTransactionId,
                    PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue());

            /*************************************************************************************/

            //Controllo congruenza degli stati
            
            ParkingTransactionItemBean parkingTransactionItemBean = null;

            if (parkingTransactionItemBeanList.isEmpty()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Parking Transaction Item not found");
                userTransaction.commit();
                estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_END_PARKING_ALREADY_CLOSED);
                return estimateEndParkingPriceResult;

            }
            else {
                parkingTransactionItemBean = parkingTransactionItemBeanList.get(0);
                parkingTransactionBean = parkingTransactionItemBean.getParkingTransactionBean();
                
                if (!parkingTransactionBean.getStatus().equals(PARKING_TRANSACTION_STATUS.STARTED.getValue())) {
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "End Parking Error: ParkingId closed");
                    
                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_END_PARKING_PRICE_TRANSACTION, ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_FAILURE,
                            "End Parking Error: ParkingId closed.");
                    em.persist(parkingTransactionStatusBean);
                    
                    userTransaction.commit();
                    
                    estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_FAILURE);
                    return estimateEndParkingPriceResult;
                }
                
                Boolean unauthorizedItemFound = Boolean.FALSE;
                
                for(ParkingTransactionItemBean parkingTransactionItemBeanCheck : parkingTransactionItemBeanList) {
                    
                    System.out.println("ParkingItemStatus: " + parkingTransactionItemBeanCheck.getParkingItemStatus());
                    
                    if (parkingTransactionItemBeanCheck.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue())|| 
                        parkingTransactionItemBeanCheck.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue())) {
                        //em.remove(parkingTransactionItemBeanCheck);
                    }
                    else {
                        if (!parkingTransactionItemBeanCheck.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue())) {
                            unauthorizedItemFound = Boolean.TRUE;
                        }
                    }
                }

                if (unauthorizedItemFound) {
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "End Parking Transaction Failure");
                    
                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_END_PARKING_PRICE_TRANSACTION, ResponseHelper.PARKING_END_PARKING_FAILURE,
                            "Unauthorized Item Found End Parking Transaction Failure.");
                    em.persist(parkingTransactionStatusBean);
                    
                    userTransaction.commit();
                    
                    estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_FAILURE);
                    return estimateEndParkingPriceResult;
                }

                /*
                if (!(parkingTransactionBean.getStatus().equals(PARKING_TRANSACTION_STATUS.STARTED.getValue()) && parkingTransactionItemBean.getParkingItemStatus().equals(
                        PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue()))) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Estimate End Parking Transaction Failure");
                    userTransaction.commit();
                    estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_FAILURE);
                    return estimateEndParkingPriceResult;
                }
                */
            }

            /*************************************************************************************/

            com.techedge.mp.parking.integration.business.interfaces.EstimateEndParkingPriceResult estimateEndParkingPriceRemoteResult = parkingService.estimateEndParkingPrice(
                    lang, parkingTransactionBean.getParkingId());

            if (estimateEndParkingPriceRemoteResult == null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EJB communication error");

                estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_FAILURE);
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_END_PARKING_PRICE_TRANSACTION, ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_FAILURE,
                        "Not possible retrieve Estimate End Parking Price EJB communication error.");
                em.persist(parkingTransactionStatusBean);
                
                userTransaction.commit();
                return estimateEndParkingPriceResult;
            }
            
            BigDecimal finalPrice = estimateEndParkingPriceRemoteResult.getPrice();
            
            if (estimateEndParkingPriceRemoteResult.getStatusCode().equals(StatusCodeHelper.ESTIMATE_END_PARKING_PRICE_OK)) {
            
                // Se il servizio di chiusura ha risposto con successo allora utilizza i dati restituiti per creare la risposta
                
                estimateEndParkingPriceResult.setTransactionId(parkingTransactionBean.getParkingTransactionId());

                estimateEndParkingPriceResult.setParkingId(estimateEndParkingPriceRemoteResult.getParkingId());
                estimateEndParkingPriceResult.setStallCode(estimateEndParkingPriceRemoteResult.getStallCode());
                estimateEndParkingPriceResult.setPlateNumber(estimateEndParkingPriceRemoteResult.getPlateNumber());
                estimateEndParkingPriceResult.setRequestedEndTime(estimateEndParkingPriceRemoteResult.getRequestedEndTime());
                estimateEndParkingPriceResult.setParkingStartTime(estimateEndParkingPriceRemoteResult.getParkingStartTime());
                estimateEndParkingPriceResult.setParkingEndTime(estimateEndParkingPriceRemoteResult.getParkingEndTime());
                estimateEndParkingPriceResult.setParkingTimeCorrection(estimateEndParkingPriceRemoteResult.getParkingTimeCorrection());
                estimateEndParkingPriceResult.setPrice(estimateEndParkingPriceRemoteResult.getPrice());
                estimateEndParkingPriceResult.setCurrentPrice(estimateEndParkingPriceRemoteResult.getCurrentPrice());
                estimateEndParkingPriceResult.setPriceDifference(estimateEndParkingPriceRemoteResult.getPriceDifference());
                estimateEndParkingPriceResult.setNotice(estimateEndParkingPriceRemoteResult.getNotice());
                estimateEndParkingPriceResult.setPaymentMethodId(parkingTransactionItemBean.getPaymentMethodId());
                
                estimateEndParkingPriceResult.setParkingZoneId(estimateEndParkingPriceRemoteResult.getParkingZoneId());
                estimateEndParkingPriceResult.setParkingZoneName(parkingTransactionBean.getParkingZoneName());
                estimateEndParkingPriceResult.setParkingZoneDescription(parkingTransactionBean.getParkingZoneDescription());
                estimateEndParkingPriceResult.setCityId(parkingTransactionBean.getCityId());
                estimateEndParkingPriceResult.setCityName(parkingTransactionBean.getCityName());
                estimateEndParkingPriceResult.setAdministrativeAreaLevel2Code(parkingTransactionBean.getAdministrativeAreaLevel2Code());
                
                estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_SUCCESS);
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.INFO, PARKING_TRANSACTION_STATUS_CONST.ESTIMATE_END_PARKING_PRICE_TRANSACTION, ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_SUCCESS,
                        "Estimate end parking price successfully created.");
                em.persist(parkingTransactionStatusBean);
                
                userTransaction.commit();

                return estimateEndParkingPriceResult;
            }
            else {
                
                ParkingTransactionItemBean lastParkingTransactionItemBean = null;
                
                if (estimateEndParkingPriceRemoteResult.getStatusCode().equals(StatusCodeHelper.END_PARKING_CLOSED)) {
                    
                    // Se il servizio ha risposto che la sosta era gi� chiusa allora l'importo totale della sosta � quello preautorizzato
                    finalPrice = new BigDecimal("0");
                    for(ParkingTransactionItemBean parkingTransactionItemBeanElement : parkingTransactionBean.getParkingTransactionItemList()) {
                        if (parkingTransactionItemBeanElement.getPrice().compareTo(finalPrice) == 1) {
                            finalPrice = parkingTransactionItemBeanElement.getPrice();
                            lastParkingTransactionItemBean = parkingTransactionItemBean;
                        }
                    }
                }
                else {
    
                    // Gestione errore applicativo
    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Application error: " + estimateEndParkingPriceResult.getStatusCode());
    
                    estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_FAILURE);
                    
                    userTransaction.commit();
                    
                    return estimateEndParkingPriceResult;
                }
                
                estimateEndParkingPriceResult.setTransactionId(parkingTransactionBean.getParkingTransactionId());

                estimateEndParkingPriceResult.setParkingId(parkingTransactionBean.getParkingId());
                estimateEndParkingPriceResult.setStallCode(parkingTransactionBean.getStallCode());
                estimateEndParkingPriceResult.setPlateNumber(parkingTransactionBean.getPlateNumber());
                estimateEndParkingPriceResult.setRequestedEndTime(lastParkingTransactionItemBean.getRequestedEndTime());
                estimateEndParkingPriceResult.setParkingStartTime(lastParkingTransactionItemBean.getParkingStartTime());
                estimateEndParkingPriceResult.setParkingEndTime(lastParkingTransactionItemBean.getParkingEndTime());
                estimateEndParkingPriceResult.setParkingTimeCorrection(lastParkingTransactionItemBean.getParkingTimeCorrection());
                estimateEndParkingPriceResult.setPrice(finalPrice);
                estimateEndParkingPriceResult.setCurrentPrice(lastParkingTransactionItemBean.getCurrentPrice());
                estimateEndParkingPriceResult.setPriceDifference(lastParkingTransactionItemBean.getPriceDifference());
                estimateEndParkingPriceResult.getNotice().add("");
                estimateEndParkingPriceResult.setPaymentMethodId(parkingTransactionItemBean.getPaymentMethodId());
                
                estimateEndParkingPriceResult.setParkingZoneId(parkingTransactionBean.getParkingZoneId());
                estimateEndParkingPriceResult.setParkingZoneName(parkingTransactionBean.getParkingZoneName());
                estimateEndParkingPriceResult.setParkingZoneDescription(parkingTransactionBean.getParkingZoneDescription());
                estimateEndParkingPriceResult.setCityId(parkingTransactionBean.getCityId());
                estimateEndParkingPriceResult.setCityName(parkingTransactionBean.getCityName());
                estimateEndParkingPriceResult.setAdministrativeAreaLevel2Code(parkingTransactionBean.getAdministrativeAreaLevel2Code());
                
                estimateEndParkingPriceResult.setStatusCode(ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_SUCCESS);
                
                userTransaction.commit();

                return estimateEndParkingPriceResult;
            }
        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED extimate end parking price with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }
}
