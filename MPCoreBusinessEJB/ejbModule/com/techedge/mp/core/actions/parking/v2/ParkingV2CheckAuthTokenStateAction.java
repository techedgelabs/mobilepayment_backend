package com.techedge.mp.core.actions.parking.v2;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.parking.Authentication;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.parking.ParkingAuthTokenBean;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2CheckAuthTokenStateAction {

    @Resource
    private EJBContext                             context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager                          em;

    @EJB
    private LoggerService                          loggerService;

    @EJB
    private ParametersService                      parameterService;
    public final static String                     PARAM_PARKING_NOTICE_SPLITTER_SYMBOL = "PARKING_NOTICE_SPLITTER_SYMBOL";

    private Integer                                sequenceID                           = 0;
    private PARKING_TRANSACTION_ITEM_STATUS_STATUS newStatus                            = null;
    private PARKING_TRANSACTION_ITEM_STATUS_STATUS oldStatus                            = null;
    private PaymentInfoBean                        paymentInfoBean                      = null;

    public ParkingV2CheckAuthTokenStateAction() {}

    public Authentication execute() throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        Authentication authentication = null;

        try {

            userTransaction.begin();
            Query query = em.createNamedQuery(ParkingAuthTokenBean.GET_TOKEN_DATA);

            List<ParkingAuthTokenBean> result = query.getResultList();
            if (result == null || result.isEmpty()) {

                userTransaction.commit();
                return authentication;
            }

            ParkingAuthTokenBean parkingAuthTokenBean = result.get(0);
            Date now = Calendar.getInstance().getTime();
            authentication = new Authentication();
            authentication.setAccess_token(parkingAuthTokenBean.getAccessToken());
            authentication.setRefresh_token(parkingAuthTokenBean.getRefreshToken());
            authentication.setExpires_in(parkingAuthTokenBean.getExpires_in());
            authentication.setRefresh_expires_in(parkingAuthTokenBean.getRefresh_expires_in());
            if (now.before(parkingAuthTokenBean.getAccessTokenExpirationDate())) {
                authentication.setExpired(false);
            }
            else {
                authentication.setExpired(true);
            }
            userTransaction.commit();

        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED extimate parking price with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }
        return authentication;
    }

}
