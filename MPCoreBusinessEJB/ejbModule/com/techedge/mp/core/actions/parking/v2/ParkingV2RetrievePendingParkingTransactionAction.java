package com.techedge.mp.core.actions.parking.v2;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParkingTransactionV2Service;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.EndParkingResult;
import com.techedge.mp.core.business.interfaces.parking.RetrievePendingParkingTransactionResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2RetrievePendingParkingTransactionAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public ParkingV2RetrievePendingParkingTransactionAction() {
    }

    
    public RetrievePendingParkingTransactionResult execute(
    		String ticketID,
			String requestID,
			ParkingTransactionV2Service parkingTransactionV2Service) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		RetrievePendingParkingTransactionResult retrievePendingParkingTransactionResult = null;
    		
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			
    			retrievePendingParkingTransactionResult = new RetrievePendingParkingTransactionResult();
    			retrievePendingParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_INVALID_TICKET);
    			return retrievePendingParkingTransactionResult;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if ( userBean == null ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found" );
				
    			userTransaction.commit();
    			
    			retrievePendingParkingTransactionResult = new RetrievePendingParkingTransactionResult();
    			retrievePendingParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_INVALID_TICKET);
    			return retrievePendingParkingTransactionResult;
    		}
    		
    		
    		// Verifica lo stato dell'utente
    		Integer userStatus = userBean.getUserStatus();
    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
    			
    			// Un utente che si trova in questo stato non pu� invocare questo servizio
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to retrieve pending refuel in status " + userStatus );
				
    			userTransaction.commit();
    			
    			retrievePendingParkingTransactionResult = new RetrievePendingParkingTransactionResult();
    			retrievePendingParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_UNAUTHORIZED);
    			return retrievePendingParkingTransactionResult;
    		}
    		
    		
    		// Ricerca la transazione di sosta attiva
    		List<ParkingTransactionBean> parkingTransactionBeanList = QueryRepository.findActiveParkingTransaction(em, userBean);
		    
		    if ( parkingTransactionBeanList.isEmpty() ) {
    			
    			// Non esiste una transazione attiva
		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction not found" );
				
    			userTransaction.commit();
    			
    			retrievePendingParkingTransactionResult = new RetrievePendingParkingTransactionResult();
    			retrievePendingParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_FAILURE);
    			return retrievePendingParkingTransactionResult;
    		}
		    else {

		        ParkingTransactionBean pendingParkingTransactionBean = null;

				for( ParkingTransactionBean parkingTransactionBean : parkingTransactionBeanList ) {

					// Si estrae la prima
					pendingParkingTransactionBean = parkingTransactionBean;
				}

				if ( pendingParkingTransactionBean == null ) {
					
					// Non esiste una transazione attiva
			    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pending parking transaction not found" );
					
	    			userTransaction.commit();
	    			
	    			retrievePendingParkingTransactionResult = new RetrievePendingParkingTransactionResult();
	    			retrievePendingParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_FAILURE);
					return retrievePendingParkingTransactionResult;
				}
				else {
					
				    // Se la sosta � terminata ma non � stata chiusa dal sistema bisogna chiuderla prima di restituire i dati
				    
				    if (!pendingParkingTransactionBean.getStatus().equals(PARKING_TRANSACTION_STATUS.ENDED.getValue())) {
				        
    				    Date now = new Date();
    				    Date parkingEndTime = null;
    	                for(ParkingTransactionItemBean parkingTransactionItemBeanElement : pendingParkingTransactionBean.getParkingTransactionItemList()) {
    	                    if (parkingTransactionItemBeanElement.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue())) {
        	                    if (parkingEndTime == null || parkingEndTime.getTime() < parkingTransactionItemBeanElement.getParkingEndTime().getTime()) {
        	                        parkingEndTime = parkingTransactionItemBeanElement.getParkingEndTime();
        	                    }
    	                    }
    	                }
    	                
    	                if (now.getTime() > parkingEndTime.getTime()) {
    	                    
    	                    // Il timestamp di fine sosta � precedente alla data attuale
    	                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "now (" + now.getTime() + ") > parkingEndTime (" + parkingEndTime.getTime() + ")");
    
    	                    System.out.println("Richiesto termine sosta");
    	                    
    	                    EndParkingResult endParkingResult = parkingTransactionV2Service.endParking(ticketID, requestID, pendingParkingTransactionBean.getParkingTransactionId());
    	                    
    	                    System.out.println("Restituzione transazione pending");
    	                    
    	                    userTransaction.commit();
    	                    
    	                    return parkingTransactionV2Service.retrievePendingParkingTransaction(ticketID, requestID);
    	                }
				    }
				    
					retrievePendingParkingTransactionResult = new RetrievePendingParkingTransactionResult();
					retrievePendingParkingTransactionResult.setParkingTransaction(pendingParkingTransactionBean.toParkingTransaction());
					retrievePendingParkingTransactionResult.setStatusCode(ResponseHelper.PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_SUCCESS);
					
					userTransaction.commit();
					
					return retrievePendingParkingTransactionResult;
				}
			}
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED retrieve pending parking transaction with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}