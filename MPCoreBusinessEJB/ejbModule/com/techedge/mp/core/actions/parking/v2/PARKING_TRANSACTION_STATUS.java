package com.techedge.mp.core.actions.parking.v2;

public enum PARKING_TRANSACTION_STATUS {
    CREATED("CREATED"), STARTED("STARTED"), ENDED("ENDED"), FAILED("FAILED"), NOT_RECONCILIABLE("NOT_RECONCILIABLE");

    private final String name;

    private PARKING_TRANSACTION_STATUS(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {

        return name.equals(otherName);
    }

    public String getValue() {
        return this.name;
    }
}
