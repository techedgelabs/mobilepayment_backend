package com.techedge.mp.core.actions.parking.v2;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.parking.EndParkingResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemEventBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionStatusBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.StatusCodeHelper;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2EndParkingAction extends ParkingV2ParkingPaymentAction {

    @Resource
    private EJBContext                             context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager                          em;

    @EJB
    private LoggerService                          loggerService;

    @EJB
    private ParametersService                      parameterService;

    private Integer                                sequenceID      = 0;
    private PARKING_TRANSACTION_ITEM_STATUS_STATUS newStatus       = null;
    private PARKING_TRANSACTION_ITEM_STATUS_STATUS oldStatus       = null;
    private PaymentInfoBean                        paymentInfoBean = null;

    public ParkingV2EndParkingAction() {}

    public EndParkingResult execute(ParkingServiceRemote parkingService, GPServiceRemote gpService, Boolean receiptEmailActive, EmailSenderRemote emailSender, String ticketId,
            String requestId, String transactionId, String lang, String myCiceroDecodedSecretKey, String proxyHost, String proxyPort, String proxyNoHosts,
            StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        EndParkingResult endParkingResult = new EndParkingResult();

        try {
            userTransaction.begin();

            UserBean userBean = null;

            if (ticketId == null) {

                // Il servizio � stato richiamato dal job per la chiusura automatica delle transazioni

            }
            else {

                TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

                if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                    userTransaction.commit();
                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_INVALID_TICKET);
                    return endParkingResult;
                }

                userBean = ticketBean.getUser();
                if (userBean == null) {

                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                    userTransaction.commit();
                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_INVALID_TICKET);
                    return endParkingResult;
                }

                // Verifica lo stato dell'utente
                Integer userStatus = userBean.getUserStatus();
                if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                    // Un utente che si trova in questo stato non pu� invocare questo servizio
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User in status " + userStatus + " unauthorized");

                    userTransaction.commit();
                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_UNAUTHORIZED);
                    return endParkingResult;
                }
            }

            ParkingTransactionItemBean parkingTransactionItemBean = null;
            ParkingTransactionBean parkingTransactionBean = null;
            List<ParkingTransactionItemBean> parkingTransactionBeanItemList = QueryRepository.findParkingTransactionItemByTransactionId(em, transactionId,
                    PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue());
            if (parkingTransactionBeanItemList.isEmpty()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Parking Transaction Item not found");
                userTransaction.commit();
                endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_INVALID_TICKET);
                return endParkingResult;

            }
            else {
                parkingTransactionItemBean = parkingTransactionBeanItemList.get(0);
                parkingTransactionBean = parkingTransactionItemBean.getParkingTransactionBean();

                if (!parkingTransactionBean.getStatus().equals(PARKING_TRANSACTION_STATUS.STARTED.getValue())) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "End Parking Error: ParkingId closed");

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(
                            parkingTransactionBean,
                            requestId,
                            PARKING_TRANSACTION_STATUS_CONST.WARN,
                            PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_END_PARKING_FAILURE,
                            "ParkingId closed parking transaction in stato " + parkingTransactionBean.getStatus() + " e stato item "
                                    + parkingTransactionItemBean.getParkingItemStatus());
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();

                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_FAILURE);
                    return endParkingResult;
                }

                Boolean unauthorizedItemFound = Boolean.FALSE;

                for (ParkingTransactionItemBean parkingTransactionItemBeanCheck : parkingTransactionBeanItemList) {

                    System.out.println("ParkingItemStatus: " + parkingTransactionItemBeanCheck.getParkingItemStatus());

                    if (parkingTransactionItemBeanCheck.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue())
                            || parkingTransactionItemBeanCheck.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue())) {
                        //em.remove(parkingTransactionItemBeanCheck);
                    }
                    else {
                        if (!parkingTransactionItemBeanCheck.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue())) {
                            unauthorizedItemFound = Boolean.TRUE;
                        }
                    }
                }

                if (unauthorizedItemFound) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "End Parking Transaction Failure");

                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_FAILURE,
                            "Unauthorized Item Found");
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();

                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_FAILURE);
                    return endParkingResult;
                }
            }

            if (userBean != null) {

                if (userBean.getId() != parkingTransactionBean.getUserBean().getId()) {

                    // La transazione non appartiene all'utente
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User unauthorized");
                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_UNAUTHORIZED,
                            "User unauthorized error");
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();
                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_UNAUTHORIZED);
                    return endParkingResult;
                }
            }
            else {
                userBean = parkingTransactionBean.getUserBean();
            }

            com.techedge.mp.parking.integration.business.interfaces.EndParkingResult endParkingRemoteResult = null;

            String clientOperationID = new IdGenerator().generateId(16).substring(0, 32);

            int attemptsLeft = Integer.valueOf(parameterService.getParamValue(PARKING_PARAM_ATTEMPTS));
            long intervalRetry = Long.valueOf(parameterService.getParamValue(PARKING_PARAM_INTERVAL_RETRY));

            BigDecimal finalPrice = new BigDecimal("0");
            Date parkingEndTime = null;

            while (attemptsLeft > 0) {

                attemptsLeft--;
                System.out.println("EndParking attempts left: " + attemptsLeft);

                endParkingRemoteResult = parkingService.endParking(lang, parkingTransactionBean.getParkingId(), clientOperationID);

                if (endParkingRemoteResult == null) {

                    // Gestione errore comunicazione con EJB

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EJB communication error");

                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_FAILURE);
                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_FAILURE,
                            "Cannot retrieve price from remote service");
                    em.persist(parkingTransactionStatusBean);

                    userTransaction.commit();
                    return endParkingResult;
                }

                finalPrice = endParkingRemoteResult.getPrice();

                if (endParkingRemoteResult.getStatusCode().equals(StatusCodeHelper.END_PARKING_OK)) {

                    // Se il servizio di chiusura ha risposto con successo allora l'importo totale della sosta � quello restituito dal servizio
                    finalPrice = endParkingRemoteResult.getPrice();
                    parkingEndTime = endParkingRemoteResult.getParkingEndTime();
                    break;
                }
                else {

                    if (endParkingRemoteResult.getStatusCode().equals(StatusCodeHelper.END_PARKING_CLOSED)) {

                        // Se il servizio di chiusura ha risposto che la sosta era gi� chiusa allora l'importo totale della sosta � quello preautorizzato
                        finalPrice = new BigDecimal("0");
                        for (ParkingTransactionItemBean parkingTransactionItemBeanElement : parkingTransactionBean.getParkingTransactionItemList()) {
                            if (parkingTransactionItemBeanElement.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue())) {
                                if (parkingTransactionItemBeanElement.getPrice().compareTo(finalPrice) == 1) {
                                    finalPrice = parkingTransactionItemBeanElement.getPrice();
                                    parkingEndTime = parkingTransactionItemBeanElement.getParkingEndTime();
                                }
                            }
                        }
                        break;
                    }
                    else {

                        if (attemptsLeft == 0) {

                            // Gestione errore applicativo

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                                    "Application error: " + endParkingResult.getStatusCode());

                            endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_FAILURE);
                            ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                    PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_FAILURE,
                                    "Cannot retrieve price cause attemptsLeft = 0");
                            em.persist(parkingTransactionStatusBean);

                            userTransaction.commit();
                            return endParkingResult;
                        }
                    }
                }

                Thread.sleep(intervalRetry);
            }

            System.out.println("Importo totale sosta: " + finalPrice);

            String noticeParsed = "";
            int counter = 0;
            for (String noticeItem : endParkingRemoteResult.getNotice()) {

                noticeParsed += (counter != endParkingRemoteResult.getNotice().size() - 1 ? noticeItem.concat(this.parameterService.getParamValue(ParkingV2EstimateParkingPriceAction.PARAM_PARKING_NOTICE_SPLITTER_SYMBOL))
                        : noticeItem);
                counter++;
            }

            parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.ENDED.getValue());
            parkingTransactionBean.setParkingZoneNotice(noticeParsed);
            parkingTransactionBean.setFinalPrice(finalPrice);
            parkingTransactionBean.setFinalParkingEndTime(parkingEndTime);
            em.persist(parkingTransactionBean);
            ParkingTransactionItemEventBean bean = null;

            System.out.println("Id:                       " + parkingTransactionItemBean.getId());
            System.out.println("ParkingTransactionItemId: " + parkingTransactionItemBean.getParkingTransactionItemId());

            Integer eventSequenceID = parkingTransactionItemBean.getLastEventSequenceID() + 1;
            Integer statusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;

            PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(em, parkingTransactionItemBean.getPaymentMethodId(),
                    parkingTransactionItemBean.getPaymentMethodType());

            if (paymentInfoBean == null) {

                // Gestione errore applicativo

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Metodo di pagamento non trovato "
                        + parkingTransactionItemBean.getPaymentMethodId() + " - " + parkingTransactionItemBean.getPaymentMethodType());

                endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_FAILURE);
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_FAILURE,
                        "Cannot find payment method.");
                em.persist(parkingTransactionStatusBean);

                userTransaction.commit();
                return endParkingResult;
            }

            String paymentError = executePaymentTransactions(finalPrice.doubleValue(), parkingTransactionBean, paymentInfoBean, userBean, statusSequenceID, requestId,
                    eventSequenceID, gpService, myCiceroDecodedSecretKey);

            switch (paymentError) {
                case ResponseHelper.PARKING_END_PARKING_PAYMENT_COMUNICATION_FAILURE:

                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_PAYMENT_COMUNICATION_FAILURE);
                    ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION,
                            ResponseHelper.PARKING_END_PARKING_PAYMENT_COMUNICATION_FAILURE, "Payment comunication system error.");
                    em.persist(parkingTransactionStatusBean);
                    parkingTransactionBean.setToReconcile(Boolean.TRUE);
                    em.merge(parkingTransactionBean);
                    userTransaction.commit();

                    return endParkingResult;

                case ResponseHelper.PARKING_END_PARKING_PAYMENT_MOV_KO:

                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_PAYMENT_MOV_KO);
                    ParkingTransactionStatusBean parkingTransactionStatusBeanMovKo = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_PAYMENT_MOV_KO,
                            "Payment comunication system ko.");
                    em.persist(parkingTransactionStatusBeanMovKo);
                    parkingTransactionBean.setFinalStatus(PARKING_TRANSACTION_STATUS.NOT_RECONCILIABLE.getValue());
                    em.merge(parkingTransactionBean);
                    userTransaction.commit();

                    return endParkingResult;

                case ResponseHelper.PARKING_END_PARKING_FAILURE:

                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_FAILURE);
                    ParkingTransactionStatusBean parkingTransactionStatusBeanFailure = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_FAILURE,
                            "Payment failure.");
                    em.persist(parkingTransactionStatusBeanFailure);
                    userTransaction.commit();

                    return endParkingResult;

                case ResponseHelper.PARKING_END_PARKING_USER_LOCKED:
                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_USER_LOCKED);
                    ParkingTransactionStatusBean parkingTransactionStatusBeanUserLocked = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_USER_LOCKED,
                            "Payment failed, user locked.");
                    em.persist(parkingTransactionStatusBeanUserLocked);
                    userTransaction.commit();

                    return endParkingResult;

                case ResponseHelper.PARKING_END_PARKING_PAYMENT_CAN_KO:
                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_FAILURE);
                    ParkingTransactionStatusBean parkingTransactionStatusBeanCanKo = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_PAYMENT_CAN_KO,
                            "Payment failed, ko deleting payment authorization.");
                    em.persist(parkingTransactionStatusBeanCanKo);
                    parkingTransactionBean.setFinalStatus(PARKING_TRANSACTION_STATUS.NOT_RECONCILIABLE.getValue());
                    em.merge(parkingTransactionBean);
                    userTransaction.commit();

                    return endParkingResult;

                case ResponseHelper.PARKING_END_PARKING_PAYMENT_CAN_ERROR:
                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_FAILURE);
                    ParkingTransactionStatusBean parkingTransactionStatusBeanCanError = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_PAYMENT_CAN_ERROR,
                            "Payment failed, error deleting payment authorization.");
                    em.persist(parkingTransactionStatusBeanCanError);
                    parkingTransactionBean.setToReconcile(Boolean.TRUE);
                    em.merge(parkingTransactionBean);
                    userTransaction.commit();

                    return endParkingResult;

                default:

                    endParkingResult.setTransactionId(parkingTransactionBean.getParkingTransactionId());

                    endParkingResult.setNotice(endParkingRemoteResult.getNotice());
                    endParkingResult.setParkingEndTime(parkingTransactionItemBean.getParkingEndTime());
                    endParkingResult.setParkingStartTime(parkingTransactionItemBean.getParkingStartTime());
                    endParkingResult.setParkingZoneName(parkingTransactionBean.getParkingZoneName());
                    endParkingResult.setPaymentMethodId(parkingTransactionItemBean.getPaymentMethodId());

                    endParkingResult.setParkingZoneId(parkingTransactionBean.getParkingZoneId());
                    endParkingResult.setParkingZoneName(parkingTransactionBean.getParkingZoneName());
                    endParkingResult.setParkingZoneDescription(parkingTransactionBean.getParkingZoneDescription());
                    endParkingResult.setCityId(parkingTransactionBean.getCityId());
                    endParkingResult.setCityName(parkingTransactionBean.getCityName());
                    endParkingResult.setAdministrativeAreaLevel2Code(parkingTransactionBean.getAdministrativeAreaLevel2Code());

                    endParkingResult.setPlateNumber(endParkingRemoteResult.getPlateNumber());
                    endParkingResult.setPrice(parkingTransactionItemBean.getPrice());
                    endParkingResult.setFinalPrice(parkingTransactionItemBean.getPrice());

                    //endParkingResult.setStallCode(startParkingRemoteResult.getStallCode());
                    //endParkingResult.setStatusCode(startParkingRemoteResult.getStatusCode());

                    if (userBean.getUserType() == User.USER_TYPE_CUSTOMER || userBean.getUserType() == User.USER_TYPE_VOUCHER_TESTER
                            || userBean.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER) {

                        // Invio della mail con lo scontrino del rifornimento
                        if (emailSender != null) {
                            Email.sendParkingSummary(emailSender, parkingTransactionBean, proxyHost, proxyPort, proxyNoHosts, stringSubstitution);
                        }
                    }

                    endParkingResult.setStatusCode(ResponseHelper.PARKING_END_PARKING_SUCCESS);
                    ParkingTransactionStatusBean parkingTransactionStatusBeanSuccess = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.INFO, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_SUCCESS,
                            "Parking transaction successfully ended.");
                    em.persist(parkingTransactionStatusBeanSuccess);
                    userTransaction.commit();

                    return endParkingResult;

            }

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED ending parking with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }

    private String executePaymentTransactions(Double parkingPrice, ParkingTransactionBean parkingTransactionBean, PaymentInfoBean paymentInfoBean, UserBean userBean,
            Integer statusSequenceID, String requestId, Integer eventSequenceID, GPServiceRemote gpService, String myCiceroDecodedSecretKey) {

        Double _parkingPrice = parkingPrice;

        // Ordina gli item per timestamp
        List<ParkingTransactionItemBean> orderedParkingTransactionItemList = new ArrayList<ParkingTransactionItemBean>(0);
        orderedParkingTransactionItemList.addAll(parkingTransactionBean.getParkingTransactionItemList());

        Collections.sort(orderedParkingTransactionItemList, new Comparator<ParkingTransactionItemBean>() {
            @Override
            public int compare(ParkingTransactionItemBean parkingTransactionItemBean1, ParkingTransactionItemBean parkingTransactionItemBean2) {

                return (parkingTransactionItemBean1.getCreationTimestamp().compareTo(parkingTransactionItemBean2.getCreationTimestamp()));
            }
        });

        ParkingTransactionItemBean lastParkingAuthorizedTransactionItemBean = null;

        // Se il parkingPrice � zero bisogna effettuare la cancellazione di tutte le autorizzazioni
        //_parkingPrice > 0.0
        if (_parkingPrice > 0.0) {

            System.out.println("Importo sosta " + _parkingPrice);

            // Altimenti si movimentano tutte le autorizzazioni scalando l'importo movimentato dal parkingPrice

            boolean stopLooping = false;
            for (ParkingTransactionItemBean parkingTransactionItemBean : orderedParkingTransactionItemList) {

                if (stopLooping)
                    break;

                System.out.println("parkingPrice residuo            : " + _parkingPrice);
                _parkingPrice = (double) Math.round(_parkingPrice * 100) / 100;
                System.out.println("parkingPrice residuo arrotondato: " + _parkingPrice);

                Iterator<ParkingTransactionItemEventBean> it = parkingTransactionItemBean.getParkingTransactionItemEventList().iterator();

                while (it.hasNext()) {

                    ParkingTransactionItemEventBean parkingTransactionItemEventBean = it.next();

                    if (!parkingTransactionItemEventBean.getEventType().equals(PARKING_TRANSACTION_ITEM_EVENT_STATUS.AUTHORIZED.getValue()))
                        continue;
                    else
                    //ITEM con Event Type <==> AUT
                    {

                        System.out.println("Rilevato event AUT da " + parkingTransactionItemEventBean.getEventAmount());

                        //Importo totale della sosta inferiore all'importo preautorizzato
                        if (_parkingPrice <= parkingTransactionItemEventBean.getEventAmount()) {

                            System.out.println("Movimentazione di " + _parkingPrice + " di " + parkingTransactionItemEventBean.getEventAmount() + " autorizzati");

                            GestPayData gestPayDataSETTLEResponse = this.doPaymentSettle(parkingTransactionItemBean, paymentInfoBean, userBean, paymentInfoBean.getId(),
                                    _parkingPrice, statusSequenceID, requestId, eventSequenceID, gpService, myCiceroDecodedSecretKey);

                            if (gestPayDataSETTLEResponse.getTransactionResult().equals("ERROR")) {

                                parkingTransactionBean.setToReconcile(true);
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment settle failure");
                                return ResponseHelper.PARKING_END_PARKING_PAYMENT_COMUNICATION_FAILURE;
                            }

                            if (gestPayDataSETTLEResponse.getTransactionResult().equals("KO")) {

                                if (gestPayDataSETTLEResponse.getErrorCode().equals("100")
                                        && gestPayDataSETTLEResponse.getErrorDescription().equals("Impossibile eseguire la Contabilizzazione")) {
                                    ParkingTransactionStatusBean parkingTransactionStatusBeanMovKo = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                            PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION,
                                            ResponseHelper.PARKING_END_PARKING_PAYMENT_MOV_KO, gestPayDataSETTLEResponse.getErrorDescription());
                                    em.persist(parkingTransactionStatusBeanMovKo);
                                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment settle failure :: ERROR CODE "
                                            + gestPayDataSETTLEResponse.getErrorCode());

                                    return ResponseHelper.PARKING_END_PARKING_SUCCESS;
                                }
                                parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.NOT_RECONCILIABLE.getValue());

                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment settle failure");

                                return ResponseHelper.PARKING_END_PARKING_PAYMENT_MOV_KO;
                            }

                            System.out.println("Movimentazione con carta di credito effettuata con successo");

                            //Imposto lo status del parkingTransactionItem a SETTLED se la transazione avviene con successo
                            parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue());
                            _parkingPrice = 0.0;

                            eventSequenceID++;
                            statusSequenceID++;

                            stopLooping = true;
                            break;
                        }
                        else
                        //l'importo preautorizzato dell'item � inferiore al valore dell'importo sosta parkingPrice
                        {
                            //movimentazione dell'importo pari a parkingPrice
                            System.out.println("Movimentazione di " + parkingTransactionItemEventBean.getEventAmount() + " di " + parkingTransactionItemEventBean.getEventAmount()
                                    + " autorizzati");

                            GestPayData gestPayDataSETTLEResponse = this.doPaymentSettle(parkingTransactionItemBean, paymentInfoBean, userBean, paymentInfoBean.getId(),
                                    parkingTransactionItemEventBean.getEventAmount(), statusSequenceID, requestId, eventSequenceID, gpService, myCiceroDecodedSecretKey);

                            if (gestPayDataSETTLEResponse.getTransactionResult().equals("ERROR")) {

                                parkingTransactionBean.setToReconcile(true);
                                return ResponseHelper.PARKING_END_PARKING_PAYMENT_COMUNICATION_FAILURE;
                            }

                            if (gestPayDataSETTLEResponse.getTransactionResult().equals("KO")) {

                                if (gestPayDataSETTLEResponse.getErrorCode().equals("100")
                                        && gestPayDataSETTLEResponse.getErrorDescription().equals("Impossibile eseguire la Contabilizzazione")) {
                                    ParkingTransactionStatusBean parkingTransactionStatusBeanMovKo = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                            PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION,
                                            ResponseHelper.PARKING_END_PARKING_PAYMENT_MOV_KO, gestPayDataSETTLEResponse.getErrorDescription());
                                    em.persist(parkingTransactionStatusBeanMovKo);
                                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment settle failure :: ERROR CODE "
                                            + gestPayDataSETTLEResponse.getErrorCode());

                                    return ResponseHelper.PARKING_END_PARKING_SUCCESS;
                                }
                                parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.NOT_RECONCILIABLE.getValue());

                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment settle failure");

                                return ResponseHelper.PARKING_END_PARKING_PAYMENT_MOV_KO;
                            }

                            //Imposto lo status del parkingTransactionItem a SETTLED se la transazione avviene con successo
                            parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue());

                            //aggiornamento del prezzo sosta residuo
                            _parkingPrice = _parkingPrice - parkingTransactionItemEventBean.getEventAmount();
                            stopLooping = false;
                        }

                    }

                }
            }

            ParkingTransactionItemBean parkingTransactionItemBean = orderedParkingTransactionItemList.get(orderedParkingTransactionItemList.size() - 1);

            //Creazione di un nuovo Item (ParkingTransactionItem) in copia dall'ultimo Item con stato AUTHORIZED necessario per la preaturizzazione e la movimentazione del credito residuo
            lastParkingAuthorizedTransactionItemBean = new ParkingTransactionItemBean();
            lastParkingAuthorizedTransactionItemBean.setAcquirerID(parkingTransactionItemBean.getAcquirerID());
            lastParkingAuthorizedTransactionItemBean.setAuthorizationCode(parkingTransactionItemBean.getAuthorizationCode());
            lastParkingAuthorizedTransactionItemBean.setBankTansactionID(parkingTransactionItemBean.getBankTansactionID());
            lastParkingAuthorizedTransactionItemBean.setCreationTimestamp(parkingTransactionItemBean.getCreationTimestamp());
            lastParkingAuthorizedTransactionItemBean.setCurrency(parkingTransactionItemBean.getCurrency());
            lastParkingAuthorizedTransactionItemBean.setCurrentPrice(parkingTransactionItemBean.getCurrentPrice());
            lastParkingAuthorizedTransactionItemBean.setGroupAcquirer(parkingTransactionItemBean.getGroupAcquirer());
            lastParkingAuthorizedTransactionItemBean.setPan(parkingTransactionItemBean.getPan());
            lastParkingAuthorizedTransactionItemBean.setParkingEndTime(parkingTransactionItemBean.getParkingEndTime());
            lastParkingAuthorizedTransactionItemBean.setParkingItemStatus(parkingTransactionItemBean.getParkingItemStatus());

            lastParkingAuthorizedTransactionItemBean.setParkingStartTime(parkingTransactionItemBean.getParkingStartTime());
            lastParkingAuthorizedTransactionItemBean.setParkingTimeCorrection(parkingTransactionItemBean.getParkingTimeCorrection());
            lastParkingAuthorizedTransactionItemBean.setParkingTransactionBean(parkingTransactionItemBean.getParkingTransactionBean());
            lastParkingAuthorizedTransactionItemBean.setParkingTransactionItemId(parkingTransactionItemBean.getParkingTransactionItemId());
            lastParkingAuthorizedTransactionItemBean.setPaymentMethodId(parkingTransactionItemBean.getPaymentMethodId());
            lastParkingAuthorizedTransactionItemBean.setPaymentMethodType(parkingTransactionItemBean.getPaymentMethodType());
            lastParkingAuthorizedTransactionItemBean.setPaymentToken(parkingTransactionItemBean.getPaymentToken());
            lastParkingAuthorizedTransactionItemBean.setPreviousPrice(parkingTransactionItemBean.getPreviousPrice());
            lastParkingAuthorizedTransactionItemBean.setPrice(parkingTransactionItemBean.getPrice());
            lastParkingAuthorizedTransactionItemBean.setPriceDifference(parkingTransactionItemBean.getPriceDifference());
            lastParkingAuthorizedTransactionItemBean.setReconciliationAttemptsLeft(parkingTransactionItemBean.getReconciliationAttemptsLeft());
            lastParkingAuthorizedTransactionItemBean.setRequestedEndTime(parkingTransactionItemBean.getRequestedEndTime());
            lastParkingAuthorizedTransactionItemBean.setServerName(parkingTransactionItemBean.getServerName());
            lastParkingAuthorizedTransactionItemBean.setShopLogin(parkingTransactionItemBean.getShopLogin());

            System.out.println("End Loop ==> Last Parking Authorized Item ID :: " + lastParkingAuthorizedTransactionItemBean.getId() + " Transaction Item ID :: "
                    + lastParkingAuthorizedTransactionItemBean.getParkingTransactionItemId());
        }

        try {
            String result = invalidatePreauthorizedItems(_parkingPrice, orderedParkingTransactionItemList, userBean, requestId, myCiceroDecodedSecretKey, gpService);
            if (result.equals("KO")) {
                return ResponseHelper.PARKING_END_PARKING_PAYMENT_CAN_KO;
            }
            if (result.equals("ERROR")) {
                return ResponseHelper.PARKING_END_PARKING_PAYMENT_CAN_ERROR;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "invalidatePreauthorizedItems", requestId, null, "Cannot invalidate preauthorized items");

            return ResponseHelper.PARKING_END_PARKING_PAYMENT_CAN_ERROR;
        }

        // Se dopo tutte le movimentazioni il parkingPrice � ancora non nullo bisogna effetutare una autorizzazione e una movimentazione del residuo
        System.out.println("parkingPrice residuo: " + _parkingPrice);

        if (_parkingPrice > 0.0) {

            System.out.println("Autorizzazione e movimentazione dell'importo residuo " + _parkingPrice);

            if (lastParkingAuthorizedTransactionItemBean == null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Residual amount and no record to auth and mov found");

                return ResponseHelper.PARKING_END_PARKING_FAILURE;
            }

            lastParkingAuthorizedTransactionItemBean.setParkingTransactionBean(parkingTransactionBean);
            parkingTransactionBean.getParkingTransactionItemList().add(lastParkingAuthorizedTransactionItemBean);

            //Generazione di un nuovo parkingTransactionItemID per effettuare una nuova autorizzazione e una nuova movimentazione
            String parkingTransactionItemId = new IdGenerator().generateId(16).substring(0, 30);
            System.out.println("Generazione di un nuovo parkingTransactionItemID per effettuare una nuova autorizzazione e una nuova movimentazione");
            System.out.println("nuovo setParkingTransactionItemId " + parkingTransactionItemId);

            lastParkingAuthorizedTransactionItemBean.setParkingTransactionItemId(parkingTransactionItemId);

            //Preautorizzazione di importo pari al residuo (parkingPrice)
            GestPayData gestPayDataAUTHResponse = this.doPaymentAuth(lastParkingAuthorizedTransactionItemBean, paymentInfoBean, userBean, paymentInfoBean.getId(), _parkingPrice,
                    statusSequenceID, requestId, eventSequenceID, gpService, myCiceroDecodedSecretKey);

            if (gestPayDataAUTHResponse.getTransactionResult().equals("ERROR")) {

                parkingTransactionBean.setToReconcile(true);
                em.merge(parkingTransactionBean);
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment settle failure");
                return ResponseHelper.PARKING_END_PARKING_PAYMENT_COMUNICATION_FAILURE;
            }

            if (gestPayDataAUTHResponse.getTransactionResult().equals("KO")) {

                parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.NOT_RECONCILIABLE.getValue());
                em.merge(parkingTransactionBean);

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment authorization failure");

                if (insufficientFunds(gestPayDataAUTHResponse)) {
                    userBean.setUserStatus(User.USER_STATUS_BLOCKED);
                    em.merge(userBean);
                    return ResponseHelper.PARKING_END_PARKING_USER_LOCKED;
                }

                return ResponseHelper.PARKING_END_PARKING_PAYMENT_MOV_KO;
            }

            System.out.println("Pagamento con carta di credito effettuato con successo");

            //Movimentazione dell'importo pari a parkingPrice
            System.out.println("Movimentazione di " + _parkingPrice + " di " + _parkingPrice + " autorizzati");

            GestPayData gestPayDataSETTLEResponse = this.doPaymentSettle(lastParkingAuthorizedTransactionItemBean, paymentInfoBean, userBean, paymentInfoBean.getId(),
                    _parkingPrice, statusSequenceID, requestId, eventSequenceID, gpService, myCiceroDecodedSecretKey);

            if (gestPayDataSETTLEResponse.getTransactionResult().equals("ERROR")) {

                parkingTransactionBean.setToReconcile(true);
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment settle failure");

                return ResponseHelper.PARKING_END_PARKING_PAYMENT_COMUNICATION_FAILURE;
            }

            if (gestPayDataSETTLEResponse.getTransactionResult().equals("KO")) {

                if (gestPayDataSETTLEResponse.getErrorCode().equals("100") && gestPayDataSETTLEResponse.getErrorDescription().equals("Impossibile eseguire la Contabilizzazione")) {
                    ParkingTransactionStatusBean parkingTransactionStatusBeanMovKo = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                            PARKING_TRANSACTION_STATUS_CONST.WARN, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, ResponseHelper.PARKING_END_PARKING_PAYMENT_MOV_KO,
                            gestPayDataSETTLEResponse.getErrorDescription());
                    em.persist(parkingTransactionStatusBeanMovKo);
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment settle failure :: ERROR CODE "
                            + gestPayDataSETTLEResponse.getErrorCode());

                    return ResponseHelper.PARKING_END_PARKING_SUCCESS;
                }

                parkingTransactionBean.setStatus(PARKING_TRANSACTION_STATUS.NOT_RECONCILIABLE.getValue());
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment settle failure");
                return ResponseHelper.PARKING_END_PARKING_PAYMENT_MOV_KO;
            }

            //Imposto lo status del parkingTransactionItem a SETTLED se la transazione avviene con successo
            lastParkingAuthorizedTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue());

        }
        return ResponseHelper.PARKING_END_PARKING_SUCCESS;
    }

    private String invalidatePreauthorizedItems(Double _parkingPrice, List<ParkingTransactionItemBean> orderedParkingTransactionItemList, UserBean userBean, String requestId,
            String myCiceroDecodedSecretKey, GPServiceRemote gpService) throws Exception {
        System.out.println("invalidatePreauthorizedItems starting......");
        for (ParkingTransactionItemBean parkingTransactionItemBean : orderedParkingTransactionItemList) {
            if (!parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue()))
                continue;

            //Imposto lo stato ad ANNULLATO
            parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue());
            System.out.println(" ParkingTransactionItem ID  :: " + parkingTransactionItemBean.getId() + " setting status to " + PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue());
            //Verifico che per questo item sia stato effettuato almeno un movimento contabile
            if (parkingTransactionItemBean.getAuthorizationCode() != null) {
                Iterator<ParkingTransactionItemEventBean> parkingTransactionItemEventIterator = parkingTransactionItemBean.getParkingTransactionItemEventList().iterator();
                while (parkingTransactionItemEventIterator.hasNext()) {
                    ParkingTransactionItemEventBean currentParkingTransactionItemEventBean = parkingTransactionItemEventIterator.next();

                    if (currentParkingTransactionItemEventBean.getEventType().equals("AUT") && currentParkingTransactionItemEventBean.getTransactionResult().equals("OK")) {
                        Integer eventSequenceID = parkingTransactionItemBean.getLastEventSequenceID() + 1;
                        Integer statusSequenceID = parkingTransactionItemBean.getParkingTransactionBean().getLastStatusSequenceID() + 1;

                        System.out.println("trovato item con ID :: " + currentParkingTransactionItemEventBean.getId() + " in stato AUT ");

                        PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(parkingTransactionItemBean.getPaymentMethodId(),
                                parkingTransactionItemBean.getPaymentMethodType());
                        if (paymentInfoBean == null) {

                            // TODO Impostare transazione in stato non riconciliabile
                            parkingTransactionItemBean.getParkingTransactionBean().setStatus(PARKING_TRANSACTION_STATUS.NOT_RECONCILIABLE.getValue());
                        }

                        GestPayData gestPayDataDELETEResponse = doPaymentAuthDelete(parkingTransactionItemBean, paymentInfoBean, userBean, paymentInfoBean.getId(),
                                currentParkingTransactionItemEventBean.getEventAmount().doubleValue(), statusSequenceID, requestId, eventSequenceID, gpService,
                                myCiceroDecodedSecretKey);

                        System.out.println("Importo di Preautorizzazione :: " + currentParkingTransactionItemEventBean.getEventAmount().doubleValue());
                        System.out.println("Tentativo di Storno => importo :: " + currentParkingTransactionItemEventBean.getEventAmount().doubleValue());

                        if (gestPayDataDELETEResponse.getTransactionResult().equals("ERROR")) {

                            // TODO Impostare transazione in stato pending
                            return "ERROR";
                        }

                        if (gestPayDataDELETEResponse.getTransactionResult().equals("KO")) {

                            // TODO Impostare transazione in stato pending

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                                    "Deleting payment authorization in EndParking failure");

                            return "KO";
                        }

                    }

                }
            }
            em.merge(parkingTransactionItemBean);

        }
        System.out.println("invalidatePreauthorizedItems finished..");

        return "OK";
    }

    private GestPayData doPaymentAuth(ParkingTransactionItemBean parkingTransactionItemBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double amount, Integer statusSequenceID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = null;
        ParkingTransactionStatusBean parkingTransactionItemStatusBean = null;

        newStatus = PARKING_TRANSACTION_ITEM_STATUS_STATUS.PAYMENT_AUTHORIZED;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        PARKING_TRANSACTION_ITEM_EVENT_STATUS transactionEvent = PARKING_TRANSACTION_ITEM_EVENT_STATUS.AUTHORIZED;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        String paymentMethodExpiration = "";

        if (paymentInfoBean != null) {

            CardDepositTransactionBean cardDepositTransactionBean = QueryRepository.findCardDepositTransactionByShopTransactionID(em, paymentInfoBean.getCheckShopTransactionID());
            if (cardDepositTransactionBean != null) {

                String tokenExpiryMonth = cardDepositTransactionBean.getTokenExpiryMonth();
                String tokenExpiryYear = cardDepositTransactionBean.getTokenExpiryYear();

                paymentMethodExpiration = "20" + tokenExpiryYear + tokenExpiryMonth;
            }
        }

        Extension[] extension_array = {};

        GestPayData gestPayDataAUTHResponse = gpService.callPagam(amount, parkingTransactionItemBean.getParkingTransactionItemId(), parkingTransactionItemBean.getShopLogin(),
                parkingTransactionItemBean.getCurrency(), paymentInfoBean.getToken(), null, parkingTransactionItemBean.getAcquirerID(),
                parkingTransactionItemBean.getGroupAcquirer(), encodedSecretKey, paymentMethodExpiration, extension_array, "CUSTOMER");

        if (gestPayDataAUTHResponse == null) {
            gestPayDataAUTHResponse = new GestPayData();
            gestPayDataAUTHResponse.setTransactionResult("ERROR");
            gestPayDataAUTHResponse.setErrorCode("9999");
            gestPayDataAUTHResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            // TODO Definire il flusso della riconciliazione
            //poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH: " + gestPayDataAUTHResponse.getTransactionResult());

        errorCode = gestPayDataAUTHResponse.getErrorCode();
        errorDescription = gestPayDataAUTHResponse.getErrorDescription();
        eventResult = gestPayDataAUTHResponse.getTransactionResult();

        parkingTransactionItemStatusBean = this.generateParkingTransactionStatusEvent(parkingTransactionItemBean.getParkingTransactionBean(), requestID,
                errorCode.equals("0") ? PARKING_TRANSACTION_STATUS_CONST.INFO : PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION,
                newStatus.getValue(), eventResult);

        em.persist(parkingTransactionItemStatusBean);

        parkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(amount, eventSequenceID, parkingTransactionItemBean, gestPayDataAUTHResponse,
                transactionEvent.getValue());

        em.persist(parkingTransactionItemEventBean);

        parkingTransactionItemBean.setPaymentMethodId(paymentMethodId);
        parkingTransactionItemBean.setPaymentToken(paymentInfoBean.getToken());
        parkingTransactionItemBean.setBankTansactionID(gestPayDataAUTHResponse.getBankTransactionID());
        parkingTransactionItemBean.setAuthorizationCode(gestPayDataAUTHResponse.getAuthorizationCode());

        oldStatus = newStatus;

        return gestPayDataAUTHResponse;
    }

    private GestPayData doPaymentSettle(ParkingTransactionItemBean parkingTransactionItemBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double amount, Integer statusSequenceID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = null;
        ParkingTransactionStatusBean parkingTransactionItemStatusBean = null;

        newStatus = PARKING_TRANSACTION_ITEM_STATUS_STATUS.PAYMENT_SETTLED;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        PARKING_TRANSACTION_ITEM_EVENT_STATUS transactionEvent = PARKING_TRANSACTION_ITEM_EVENT_STATUS.SETTLED;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataSETTLEResponse = gpService.callSettle(amount, parkingTransactionItemBean.getParkingTransactionItemId(), parkingTransactionItemBean.getShopLogin(),
                parkingTransactionItemBean.getCurrency(), parkingTransactionItemBean.getAcquirerID(), parkingTransactionItemBean.getGroupAcquirer(), encodedSecretKey,
                paymentInfoBean.getToken(), parkingTransactionItemBean.getAuthorizationCode(), parkingTransactionItemBean.getBankTansactionID(), null, null, 0.0, 0.0);

        if (gestPayDataSETTLEResponse == null) {
            gestPayDataSETTLEResponse = new GestPayData();
            gestPayDataSETTLEResponse.setTransactionResult("ERROR");
            gestPayDataSETTLEResponse.setErrorCode("9999");
            gestPayDataSETTLEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            // TODO Definire il flusso della riconciliazione
            //poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay SETTLE: " + gestPayDataSETTLEResponse.getTransactionResult());

        errorCode = gestPayDataSETTLEResponse.getErrorCode();
        errorDescription = gestPayDataSETTLEResponse.getErrorDescription();
        eventResult = gestPayDataSETTLEResponse.getTransactionResult();

        parkingTransactionItemStatusBean = this.generateParkingTransactionStatusEvent(parkingTransactionItemBean.getParkingTransactionBean(), requestID,
                errorCode.equals("0") ? PARKING_TRANSACTION_STATUS_CONST.INFO : PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION,
                newStatus.getValue(), eventResult);

        em.persist(parkingTransactionItemStatusBean);

        parkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(amount, eventSequenceID, parkingTransactionItemBean, gestPayDataSETTLEResponse,
                transactionEvent.getValue());
        em.persist(parkingTransactionItemEventBean);

        parkingTransactionItemBean.getParkingTransactionItemEventList().add(parkingTransactionItemEventBean);

        oldStatus = newStatus;

        return gestPayDataSETTLEResponse;
    }

    private GestPayData doPaymentAuthDelete(ParkingTransactionItemBean parkingTransactionItemBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double amount, Integer statusSequenceID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = null;
        ParkingTransactionStatusBean parkingTransactionItemStatusBean = null;

        newStatus = PARKING_TRANSACTION_ITEM_STATUS_STATUS.PAYMENT_AUTHORIZATION_DELETED;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        PARKING_TRANSACTION_ITEM_EVENT_STATUS transactionEvent = PARKING_TRANSACTION_ITEM_EVENT_STATUS.CANCELLED;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataDELETEResponse = gpService.deletePagam(amount, parkingTransactionItemBean.getParkingTransactionItemId(), parkingTransactionItemBean.getShopLogin(),
                parkingTransactionItemBean.getCurrency(), parkingTransactionItemBean.getBankTansactionID(), null, parkingTransactionItemBean.getAcquirerID(),
                parkingTransactionItemBean.getGroupAcquirer(), encodedSecretKey);

        if (gestPayDataDELETEResponse == null) {
            gestPayDataDELETEResponse = new GestPayData();
            gestPayDataDELETEResponse.setTransactionResult("ERROR");
            gestPayDataDELETEResponse.setErrorCode("9999");
            gestPayDataDELETEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            // TODO Definire il flusso della riconciliazione
            //poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH DEL: " + gestPayDataDELETEResponse.getTransactionResult());

        errorCode = gestPayDataDELETEResponse.getErrorCode();
        errorDescription = gestPayDataDELETEResponse.getErrorDescription();
        eventResult = gestPayDataDELETEResponse.getTransactionResult();

        parkingTransactionItemStatusBean = this.generateParkingTransactionStatusEvent(parkingTransactionItemBean.getParkingTransactionBean(), requestID,
                errorCode.equals("0") ? PARKING_TRANSACTION_STATUS_CONST.INFO : PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION,
                newStatus.getValue(), eventResult);
        em.persist(parkingTransactionItemStatusBean);

        parkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(amount, eventSequenceID, parkingTransactionItemBean, gestPayDataDELETEResponse,
                transactionEvent.getValue());

        em.persist(parkingTransactionItemEventBean);

        oldStatus = newStatus;

        return gestPayDataDELETEResponse;
    }
}
