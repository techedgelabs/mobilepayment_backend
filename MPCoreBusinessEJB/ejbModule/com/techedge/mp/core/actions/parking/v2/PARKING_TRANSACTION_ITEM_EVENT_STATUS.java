package com.techedge.mp.core.actions.parking.v2;

public enum PARKING_TRANSACTION_ITEM_EVENT_STATUS {
    AUTHORIZED("AUT"), SETTLED("MOV"), CANCELLED("CAN"), REVERSED("STO");

    private final String name;

    private PARKING_TRANSACTION_ITEM_EVENT_STATUS(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {

        return name.equals(otherName);
    }

    public String getValue() {
        return this.name;
    }
}
