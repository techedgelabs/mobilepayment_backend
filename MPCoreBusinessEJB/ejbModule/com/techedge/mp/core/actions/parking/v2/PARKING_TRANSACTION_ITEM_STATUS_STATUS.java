package com.techedge.mp.core.actions.parking.v2;

public enum PARKING_TRANSACTION_ITEM_STATUS_STATUS {
    PAYMENT_AUTHORIZED("PAYMENT_AUTHORIZED"), PARKING_STARTED("PARKING_STARTED"), PAYMENT_SETTLED("PAYMENT_SETTLED"), PAYMENT_AUTHORIZATION_DELETED("PAYMENT_AUTHORIZATION_DELETED");

    private final String name;

    private PARKING_TRANSACTION_ITEM_STATUS_STATUS(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {

        return name.equals(otherName);
    }

    public String getValue() {
        return this.name;
    }
}
