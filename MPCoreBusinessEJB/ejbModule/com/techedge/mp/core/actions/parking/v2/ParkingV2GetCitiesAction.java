package com.techedge.mp.core.actions.parking.v2;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.GetCitiesResult;
import com.techedge.mp.core.business.interfaces.parking.ParkingCity;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.StatusCodeHelper;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2GetCitiesAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public ParkingV2GetCitiesAction() {}

    public GetCitiesResult execute(ParkingServiceRemote parkingService, String ticketId, String requestId, String lang) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        GetCitiesResult getCitiesResult = new GetCitiesResult();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                getCitiesResult.setStatusCode(ResponseHelper.PARKING_GETCITIES_INVALID_TICKET);
                return getCitiesResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                getCitiesResult.setStatusCode(ResponseHelper.PARKING_GETCITIES_INVALID_TICKET);
                return getCitiesResult;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User in status " + userStatus + " unauthorized");

                userTransaction.commit();
                getCitiesResult.setStatusCode(ResponseHelper.PARKING_GETCITIES_UNAUTHORIZED);
                return getCitiesResult;
            }

            com.techedge.mp.parking.integration.business.interfaces.GetCitiesResult getCitiesRemoteResult = parkingService.getCities(lang);

            if (getCitiesRemoteResult == null) {

                // Gestione errore comunicazione con EJB

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EJB communication error");

                getCitiesResult.setStatusCode(ResponseHelper.PARKING_GETCITIES_FAILURE);
                userTransaction.commit();
                return getCitiesResult;
            }

            if (!getCitiesRemoteResult.getStatusCode().equals(StatusCodeHelper.RETRIEVE_PARKING_ZONES_OK)) {

                // Gestione errore applicativo

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Application error: " + getCitiesRemoteResult.getStatusCode());

                getCitiesResult.setStatusCode(ResponseHelper.PARKING_GETCITIES_FAILURE);
                userTransaction.commit();
                return getCitiesResult;
            }

            Boolean cityFound = Boolean.FALSE;
            for (com.techedge.mp.parking.integration.business.interfaces.ParkingCity parkingCityRemote : getCitiesRemoteResult.getParkingCityList()) {

                ParkingCity parkingCity = new ParkingCity();
                parkingCity.setAdministrativeAreaLevel2Code(parkingCityRemote.getAdministrativeAreaLevel2Code());
                parkingCity.setCityId(parkingCityRemote.getCityId());
                parkingCity.setId(parkingCityRemote.getId());
                parkingCity.setLatitude(parkingCityRemote.getLatitude());
                parkingCity.setLongitude(parkingCityRemote.getLongitude());
                parkingCity.setName(parkingCityRemote.getName());
                parkingCity.setStickerRequired(parkingCityRemote.getStickerRequired());
                parkingCity.setStickerRequiredNotice(parkingCityRemote.getStickerRequiredNotice());
                parkingCity.setUrl(parkingCityRemote.getUrl());

                cityFound = Boolean.TRUE;
                getCitiesResult.getParkingCities().add(parkingCity);
            }

            getCitiesResult.setStatusCode(ResponseHelper.PARKING_GETCITIES_SUCCESS);

            if (!cityFound) {
                getCitiesResult.setStatusCode(ResponseHelper.PARKING_GETCITIES_NOT_FOUND);
            }

            userTransaction.commit();
            return getCitiesResult;

        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED retrieve parking zones with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }

}
