package com.techedge.mp.core.actions.parking.v2;

import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.parking.Authentication;
import com.techedge.mp.core.business.model.parking.ParkingAuthTokenBean;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingV2PersistAuthTokenAction {

    @Resource
    private EJBContext         context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager      em;

    @EJB
    private LoggerService      loggerService;

    @EJB
    private ParametersService  parameterService;
    public final static String PARAM_PARKING_NOTICE_SPLITTER_SYMBOL = "PARKING_NOTICE_SPLITTER_SYMBOL";

    public ParkingV2PersistAuthTokenAction() {}

    public Authentication execute(Authentication auth) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        Authentication authentication = null;
        try {
            userTransaction.begin();

            Query query = em.createNamedQuery(ParkingAuthTokenBean.GET_TOKEN_DATA);

            List<ParkingAuthTokenBean> result = query.getResultList();
            ParkingAuthTokenBean parkingAuthTokenBean = null;
            if (!result.isEmpty())
                parkingAuthTokenBean = result.get(0);
            else
                parkingAuthTokenBean = new ParkingAuthTokenBean();

            parkingAuthTokenBean.setAccessToken(auth.getAccess_token());
            parkingAuthTokenBean.setRefreshToken(auth.getRefresh_token());
            parkingAuthTokenBean.setExpires_in(auth.getExpires_in());
            parkingAuthTokenBean.setRefresh_expires_in(auth.getRefresh_expires_in());

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.SECOND, auth.getExpires_in());
            Calendar cal2 = Calendar.getInstance();
            cal2.add(Calendar.SECOND, auth.getRefresh_expires_in());
            parkingAuthTokenBean.setAccessTokenExpirationDate(cal.getTime());
            parkingAuthTokenBean.setRefreshTokenExpirationDate(cal2.getTime());
            em.persist(parkingAuthTokenBean);
            authentication = new Authentication();
            authentication.setAccess_token(parkingAuthTokenBean.getAccessToken());
            authentication.setRefresh_token(parkingAuthTokenBean.getRefreshToken());
            authentication.setExpired(false);

            userTransaction.commit();

        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED extimate parking price with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }
        return authentication;
    }

}
