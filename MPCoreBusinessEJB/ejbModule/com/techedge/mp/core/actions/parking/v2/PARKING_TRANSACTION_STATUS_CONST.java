package com.techedge.mp.core.actions.parking.v2;

public final class PARKING_TRANSACTION_STATUS_CONST {

    public static final String ESTIMATE_PARKING_PRICE_TRANSACTION        = "ESTIMATE_PARKING_PRICE_TRANSACTION";
    public static final String ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION = "ESTIMATE_EXTEND_PARKING_PRICE_TRANSACTION";
    public static final String ESTIMATE_END_PARKING_PRICE_TRANSACTION    = "ESTIMATE_END_PARKING_PRICE_TRANSACTION";
    public static final String START_PARKING_TRANSACTION                 = "START_PARKING_TRANSACTION";
    public static final String EXTEND_PARKING_TRANSACTION                = "EXTEND_PARKING_TRANSACTION";
    public static final String END_PARKING_TRANSACTION                   = "END_PARKING_TRANSACTION";
    public static final String RECONCILIATION                            = "RECONCILIATION";

    public static final String INFO                                      = "INFO";
    public static final String WARN                                      = "WARN";
    public static final String ERROR                                     = "ERROR";

}
