package com.techedge.mp.core.actions.dwh;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.EventNotificationServiceRemote;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.EsPromotionBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.pushnotification.adapter.interfaces.StatusCode;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ActivatePromotionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public ActivatePromotionAction() {}

    public String execute(String promotionID, String fiscalCode, EmailSenderRemote emailSender, PushNotificationServiceRemote pushNotificationService, Integer maxRetryAttemps, String landingWelcomeVodafone, Long promoVodafoneStart, Long promoVodafoneEnd) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        
        SimpleDateFormat sdfFull = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        
        Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
        Date creationDate = calendar.getTime();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date now = calendar.getTime();
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        Date firstDayOfMonth = calendar.getTime();
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        Date lastDayOfMonth = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date firstDayOfNextMonth = calendar.getTime();
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        Date lastDayOfNextMonth = calendar.getTime();
        
        System.out.println("Data attuale                      : " + sdfFull.format(now));
        System.out.println("Primo giorno del mese attuale     : " + sdfFull.format(firstDayOfMonth));
        System.out.println("Ultimo giorno del mese attuale    : " + sdfFull.format(lastDayOfMonth));
        System.out.println("Primo giorno del prossimo mese    : " + sdfFull.format(firstDayOfNextMonth));
        System.out.println("Ultimo giorno del prossimo attuale: " + sdfFull.format(lastDayOfNextMonth));

        try {
            userTransaction.begin();

            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(promoVodafoneStart);         
            Date promoVodafoneStartDate = (Date) c.getTime();
            c.setTimeInMillis(promoVodafoneEnd);         
            Date promoVodafoneEndDate = (Date) c.getTime();
            
            System.out.println("Data inizio promozione: " + sdfFull.format(promoVodafoneStartDate));
            System.out.println("Data fine promozione:   " + sdfFull.format(promoVodafoneEndDate));
            
            Date currentDate = new Date();
            
            System.out.println("Data corrente                     : " + sdfFull.format(currentDate));
            
            if (currentDate.getTime() < promoVodafoneStart) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", fiscalCode, null, "Promotion not started (" + currentDate.getTime() + " < " + promoVodafoneStart + ")");
                userTransaction.commit();
                return ResponseHelper.DWH_ACTIVATE_PROMOTION_SYSTEM_ERROR;
            }
            
            if (currentDate.getTime() > promoVodafoneEnd) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", fiscalCode, null, "Promotion ended (" + currentDate.getTime() + " > " + promoVodafoneEnd + ")");
                userTransaction.commit();
                return ResponseHelper.DWH_ACTIVATE_PROMOTION_SYSTEM_ERROR;
            }
            
            
            UserBean userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);

            //verifico la presenza dell'utente
            if (userBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", fiscalCode, null, "User not found");
                userTransaction.commit();
                return ResponseHelper.DWH_ACTIVATE_PROMOTION_USER_NOT_FOUND;
            }

            //Verifica lo stato dell'utente
            if (!userBean.getUserStatus().equals(User.USER_STATUS_VERIFIED) || !userBean.getUserStatusRegistrationCompleted()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", fiscalCode, null, "User not active");
                userTransaction.commit();
                return ResponseHelper.DWH_ACTIVATE_PROMOTION_USER_NOT_FOUND;
            }
            
            //Verifica che l'utente abbia una la carta di credito associata
            PaymentInfoBean paymentInfoCreditCard = userBean.findPaymentInfoCreditCardBean();
            if (paymentInfoCreditCard == null) {
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", fiscalCode, null, "Credit Card not found");
                userTransaction.commit();
                return ResponseHelper.DWH_ACTIVATE_PROMOTION_CARD_NOT_FOUND;
            }

            //estrazione promozioni attive per il codice fiscale e il mese corrente
            List<EsPromotionBean> listaPromotion = QueryRepository.findEsPromotionByFiscalCode(em, fiscalCode, firstDayOfMonth);
            
            if(listaPromotion.size() == 0) {
            	
                //attivo la promozione dal giorno corrente alla fine del mese corrente e per tutto il mese successivo
            	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", fiscalCode, null, "Create promotion for periods: " + sdfFull.format(currentDate) + " - " + sdfFull.format(lastDayOfMonth) + " && " + sdfFull.format(firstDayOfNextMonth) + " - " + sdfFull.format(lastDayOfNextMonth));
            	EsPromotionBean promotionBean = new EsPromotionBean(promotionID, fiscalCode, currentDate, lastDayOfMonth, TypePromotionEnum.ATTIVAZIONE.getValue(), null, creationDate, userBean);
            	em.persist(promotionBean);
            	promotionBean = new EsPromotionBean(promotionID, fiscalCode, firstDayOfNextMonth, lastDayOfNextMonth, TypePromotionEnum.RINNOVO.getValue(), null, creationDate, userBean);
            	em.persist(promotionBean);
            }
            else if (listaPromotion.size() == 1) {
                
            	if(listaPromotion.get(0).getType().equalsIgnoreCase(TypePromotionEnum.ATTIVAZIONE.getValue())) {
            		
            	    //la promozione � gi� attiva per il mese corrente
            		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", fiscalCode, null, "Promotion already exists for current month!");
            		userTransaction.commit();
            		return ResponseHelper.DWH_ACTIVATE_PROMOTION_ALREADY_EXISTS;
            	}
            	else if(listaPromotion.get(0).getType().equalsIgnoreCase(TypePromotionEnum.RINNOVO.getValue())) {
            		
            	    //rinnovo la promozione per il mese successivo
            		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", fiscalCode, null, "Create Promotion renewal for period: " + sdfFull.format(firstDayOfNextMonth) + " - " + sdfFull.format(lastDayOfNextMonth));
            		EsPromotionBean promotionBean = new EsPromotionBean(promotionID, fiscalCode, firstDayOfNextMonth, lastDayOfNextMonth, TypePromotionEnum.RINNOVO.getValue(), null, creationDate, userBean);
            		em.persist(promotionBean);
            	}
            }
            else if (listaPromotion.size() == 2) {
            	
                //if(listaPromotion.get(0).getType().equalsIgnoreCase(TypePromotionEnum.ATTIVAZIONE.getValue()))
            	//anomalia nella tabella es_promotion
            	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", fiscalCode, null, "Promotion already exists for current month! Two record Found!");
            	userTransaction.commit();
            	return ResponseHelper.DWH_ACTIVATE_PROMOTION_ALREADY_EXISTS;
            }
            else {
                
            	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", fiscalCode, null, "ERROR anomaly in es_promotion table, record found: "+listaPromotion.size());
            	userTransaction.commit();
            	return ResponseHelper.DWH_ACTIVATE_PROMOTION_SYSTEM_ERROR;
            }
            
            
            // Se l'utente ha accettato di ricevere comunicazioni da parte di Eni deve anche ricevere una notifica push e una dem
            Boolean sendCommunications = Boolean.FALSE;
            for(TermsOfServiceBean termsOfServiceBean : userBean.getPersonalDataBean().getTermsOfServiceBeanData()) {
                if (termsOfServiceBean.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                    if (termsOfServiceBean.getValid() && termsOfServiceBean.getAccepted()) {
                        sendCommunications = Boolean.TRUE;
                    }
                }
            }
            
            if (sendCommunications) {
                
                System.out.println("Invio notifica push");
                sendNotificationPush(pushNotificationService, userBean, maxRetryAttemps, landingWelcomeVodafone);
                
                System.out.println("Invio dem");
                sendDem(userBean, emailSender);
            }
            else {
                
                System.out.println("Notifica e dem non inviate");
            }
            

            userTransaction.commit();

            return ResponseHelper.DWH_ACTIVATE_PROMOTION_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED activate promotion (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", promotionID, null, message);

            throw new EJBException(ex2);
        }
    }
    
    
    private enum TypePromotionEnum {

        ATTIVAZIONE("A"), 
        RINNOVO("R");

        private String value;

        private TypePromotionEnum(String value) {

            this.value = value;
        }

        public String getValue() {

            return value;
        }

        public void setValue(String value) {

            this.value = value;
        }

    }
    
    
    private void sendNotificationPush(PushNotificationServiceRemote pushNotificationService, UserBean userBean, Integer maxRetryAttemps, String landingWelcomeVodafone){
        
        String arnEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();
        PushNotificationContentType contentType = PushNotificationContentType.TEXT;
        PushNotificationStatusType statusCode = null;
        String statusMessage = null;
        String publishMessageID = null;
        Date sendingTimestamp = new Date();
        Long userID = userBean.getId();
        PushNotificationSourceType source = PushNotificationSourceType.CRM_ES;
        String notificationText = "Benvenuto nell�iniziativa Eni Station + per \"Happy Black\"";
        String message = landingWelcomeVodafone;
        String title = "Eni Station +";

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sendingTimestamp);

        PushNotificationBean pushNotificationBean = new PushNotificationBean();
        pushNotificationBean.setTitle(title);
        pushNotificationBean.setSendingTimestamp(sendingTimestamp);
        pushNotificationBean.setExpiringTimestamp(calendar.getTime());
        pushNotificationBean.setSource(source);
        pushNotificationBean.setContentType(contentType);
        pushNotificationBean.setUser(userBean);
        pushNotificationBean.setStatusCode(PushNotificationStatusType.PENDING);
        pushNotificationBean.setRetryAttemptsLeft(maxRetryAttemps);
        
        em.persist(pushNotificationBean);

        PushNotificationMessage notificationMessage = new PushNotificationMessage();
        
        notificationMessage.setMessage(pushNotificationBean.getId(), notificationText, title);
        
        PushNotificationResult pushNotificationResult = pushNotificationService.publishMessage(arnEndpoint, notificationMessage, false, false);
        
        publishMessageID = pushNotificationResult.getMessageId();
        sendingTimestamp = pushNotificationResult.getRequestTimestamp();
        
        if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS)) {
            statusCode = PushNotificationStatusType.DELIVERED;
        }
        else if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_ARN_NOT_FOUND)) {
            statusCode = PushNotificationStatusType.ARN_NOT_FOUND;
        }
        else {
            statusCode = PushNotificationStatusType.ERROR;
        }
        
        pushNotificationBean.setStatusCode(statusCode);
        pushNotificationBean.setEndpoint(arnEndpoint);
        pushNotificationBean.setPublishMessageID(publishMessageID);
        pushNotificationBean.setSendingTimestamp(sendingTimestamp);
        pushNotificationBean.setMessage(message);

        pushNotificationBean.setStatusMessage(pushNotificationResult.getMessage());
        
        if (statusCode.equals(PushNotificationStatusType.ERROR)) {
            pushNotificationBean.setToReconcilie(true);
        }
        else {
            pushNotificationBean.setToReconcilie(false);
        }

        em.merge(pushNotificationBean);
    }
    
    
    private void sendDem(UserBean userBean, EmailSenderRemote emailSender) {
        
        EmailType emailType = EmailType.WELCOME_VODAFONE;
        
        String keyFrom = emailSender.getSender();
        String to      = userBean.getPersonalDataBean().getSecurityDataEmail();
        
        List<Parameter> parameters = new ArrayList<Parameter>(0);
        parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));

        System.out.println("Send email to " + to);

        String sendEmailResult = emailSender.sendEmail(emailType, keyFrom, to, parameters);

        System.out.println("SendEmailResult: " + sendEmailResult);
    }
}
