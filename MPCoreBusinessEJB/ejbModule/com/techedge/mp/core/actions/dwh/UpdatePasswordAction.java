package com.techedge.mp.core.actions.dwh;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.DWHOperationType;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.HistoryPasswordBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.dwh.DWHEventOperationBean;
import com.techedge.mp.core.business.model.dwh.DWHEventBean;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.DWHAdapterResult;
import com.techedge.mp.dwh.adapter.utilities.StatusCode;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UpdatePasswordAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UpdatePasswordAction() {}

    public String execute(String operationID, Long requestTimestamp, String fiscalCode, String email, String oldPassword, String newPassword, Integer passwordHistoryLenght,
            Integer reconciliationMaxAttempts, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            UserBean userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);

            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", operationID, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.DWH_UPDATE_PASSWORD_USER_NOT_FOUND;
            }
            
            if(userBean.getSocialProvider()!=null)
            {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", operationID, null, "Social User found :: operation not allowed");

                userTransaction.commit();

                return ResponseHelper.DWH_UPDATE_PASSWORD_USER_UNAUTHORIZED;
            }

            Integer userType = userBean.getUserType();
            
            Boolean isNewAcquirerFlow = false;
            isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());

            if (!isNewAcquirerFlow) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User is not in new aquirer flow category: " + userType);

                userTransaction.commit();

                return ResponseHelper.DWH_UPDATE_PASSWORD_USER_UNAUTHORIZED;
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User found in new aquirer flow category");
            }
            
            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", operationID, null, "Unable to update user password in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.DWH_UPDATE_PASSWORD_USER_UNAUTHORIZED;
            }

            if (!userBean.getPersonalDataBean().getSecurityDataEmail().equals(email)) {
                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", operationID, null, "Unable to update user password wrong email " + email);

                userTransaction.commit();

                return ResponseHelper.DWH_UPDATE_PASSWORD_EMAIL_NOT_FOUND;
            }

            // Verifica la nuova password
            // TODO

            if (userBean.getPersonalDataBean().getSecurityDataEmail().equals(newPassword)) {

                // La nuova password coincide con l'email dell'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", operationID, null, "New password = email");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_USERNAME_PASSWORD_EQUALS;
            }

            Date checkDate = new Date();

            if (userBean.getPersonalDataBean().getExpirationDateRescuePassword() != null) {

                System.out.println("Expiration date rescue password: " + userBean.getPersonalDataBean().getExpirationDateRescuePassword());
                System.out.println("CheckDate: " + checkDate);

                if (userBean.getPersonalDataBean().getExpirationDateRescuePassword().before(checkDate)) {

                    System.out.println("before");
                }
                else {

                    System.out.println("after");
                }
            }

            if (!userBean.getPersonalDataBean().getSecurityDataPassword().equals(oldPassword)) {

                // La old password non coincide con quella dell'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", operationID, null, "Old password wrong");

                userTransaction.commit();

                return ResponseHelper.DWH_UPDATE_PASSWORD_OLD_PASSWORD_WRONG;

            }

            // Codifica la password
            String encodedPassword = EncoderHelper.encode(newPassword);

            String statusValidator = passwordValidator(newPassword);

            if (!statusValidator.equals(ResponseHelper.DWH_UPDATE_PASSWORD_SUCCESS)) {
                // La old password non coincide con quella dell'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", operationID, null, "Errore nella validazione della password: "
                        + statusValidator);

                userTransaction.commit();

                return statusValidator;
            }

            if (userBean.getPersonalDataBean().getSecurityDataPassword().equals(encodedPassword)) {

                // La nuova password coincide con quella vecchia
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", operationID, null, "New password = old password");

                userTransaction.commit();

                return ResponseHelper.DWH_UPDATE_PASSWORD_OLD_AND_NEW_EQUALS;
            }
            ////////////////////////////////////

            Set<HistoryPasswordBean> historyPasswordBeanData = userBean.getPersonalDataBean().getHistoryPasswordBeanData();
            Set<HistoryPasswordBean> historyPasswordBeanDataTemp = new HashSet<HistoryPasswordBean>(0);

            HistoryPasswordBean historyPasswordBeanTemp = new HistoryPasswordBean();
            //HistoryPasswordBean historyPasswordBeanOld = new HistoryPasswordBean();

            HistoryPasswordBean historyPasswordBeanNew;

            if (historyPasswordBeanData.size() >= passwordHistoryLenght) {
                
                for (int i = 0; i < passwordHistoryLenght; i++) {

                    historyPasswordBeanTemp = null;

                    for (HistoryPasswordBean historyPasswordBean : historyPasswordBeanData) {

                        if (historyPasswordBeanTemp == null || historyPasswordBeanTemp.getId() < historyPasswordBean.getId()) {
                            historyPasswordBeanTemp = historyPasswordBean;
                        }
                    }

                    historyPasswordBeanDataTemp.add(historyPasswordBeanTemp);

                    historyPasswordBeanData.remove(historyPasswordBeanTemp);

                    if (historyPasswordBeanTemp.getPassword().equals(encodedPassword)) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", operationID, null, "New password = history password");

                        userTransaction.commit();

                        return ResponseHelper.DWH_UPDATE_PASSWORD_OLD_AND_NEW_EQUALS;
                    }

                    if (i == passwordHistoryLenght - 1) {

                        em.remove(historyPasswordBeanTemp);

                        historyPasswordBeanNew = new HistoryPasswordBean();
                        historyPasswordBeanNew.setPersonalDataBean(userBean.getPersonalDataBean());
                        historyPasswordBeanNew.setPassword(encodedPassword);

                        em.persist(historyPasswordBeanNew);

                    }
                }
            }
            else {

                for (HistoryPasswordBean historyPasswordBean : historyPasswordBeanData) {

                    if (historyPasswordBean.getPassword().equals(encodedPassword)) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", operationID, null, "New password = history password");

                        userTransaction.commit();

                        return ResponseHelper.DWH_UPDATE_PASSWORD_OLD_AND_NEW_EQUALS;
                    }
                }

                historyPasswordBeanNew = new HistoryPasswordBean();
                historyPasswordBeanNew.setPersonalDataBean(userBean.getPersonalDataBean());
                historyPasswordBeanNew.setPassword(encodedPassword);

                em.persist(historyPasswordBeanNew);

            }

            // Aggiorna la password
            userBean.getPersonalDataBean().setSecurityDataPassword(encodedPassword);

            if (userBean.getUserStatus() == User.USER_STATUS_TEMPORARY_PASSWORD) {
                userBean.setUserStatus(User.USER_STATUS_VERIFIED);
            }
            
            // Aggiorna i dati dell'utente
            em.merge(userBean);
            
            if (userBean.getVirtualizationCompleted()) {

                String requestID = String.valueOf(new Date().getTime());
                String response = ResponseHelper.DWH_UPDATE_PASSWORD_SUCCESS;
                String propagationStatus = StatusHelper.DWH_EVENT_STATUS_SUCCESS;
                
                DWHAdapterServiceRemote dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();

                DWHAdapterResult result = dwhAdapterService.changeUserPasswordPlus(userBean.getPersonalDataBean().getSecurityDataEmail(),
                        userBean.getPersonalDataBean().getSecurityDataPassword(), requestID, requestTimestamp);

                if (!result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS)) {
                    
                    // L'utente non esiste
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                            "DWH error: " + result.getMessageCode() + " (" + result.getStatusCode() + ")");

                    response = ResponseHelper.DWH_UPDATE_PASSWORD_FAILURE;
                    
                    if (result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR)) {
                        propagationStatus = StatusHelper.DWH_EVENT_STATUS_ERROR;
                    }
                    else {
                        propagationStatus = StatusHelper.DWH_EVENT_STATUS_FAILED;
                    }
                   
                    userTransaction.rollback();

                    userTransaction.begin();
                }

                DWHEventBean dWHEventBean = new DWHEventBean();
                dWHEventBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
                dWHEventBean.setToReconcilie(false);
                dWHEventBean.setEventStatus(propagationStatus);
                dWHEventBean.setOperation(DWHOperationType.CHANGE_PASSWORD);
                dWHEventBean.setUserBean(userBean);
                
                em.persist(dWHEventBean);
                
                DWHEventOperationBean dWHEventOperationBean = new DWHEventOperationBean();
                dWHEventOperationBean.setRequestTimestamp(new Date());
                dWHEventOperationBean.setDWHEventBean(dWHEventBean);
                dWHEventOperationBean.setRequestId(requestID);
                dWHEventOperationBean.setStatusCode(result.getStatusCode());
                dWHEventOperationBean.setMessageCode(result.getMessageCode());
                
                em.persist(dWHEventOperationBean);
                
                userTransaction.commit();
                
                return response;
            }

            userTransaction.commit();

            return ResponseHelper.DWH_UPDATE_PASSWORD_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED password update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", operationID, null, message);

            throw new EJBException(ex2);
        }
    }

    private String passwordValidator(String password) {

        String regex_password_maiuscola = ".*[A-Z].*";
        String regex_password_minuscola = ".*[a-z].*";
        String regex_password_numerica = ".*[0-9].*";

        if (password == null || password.length() > 40) {
            return ResponseHelper.DWH_UPDATE_PASSWORD_NEW_PASSWORD_WRONG;
        }
        else {
            if (password.length() < 8) {
                return ResponseHelper.DWH_UPDATE_PASSWORD_PASSWORD_SHORT;
            }

            Pattern patternPasswordMaiuscola = Pattern.compile(regex_password_maiuscola);
            Matcher matchePasswordMaiuscola = patternPasswordMaiuscola.matcher(password);
            Pattern patternPasswordMinuscola = Pattern.compile(regex_password_minuscola);
            Matcher matchePasswordMinuscola = patternPasswordMinuscola.matcher(password);
            Pattern patternPasswordNumerica = Pattern.compile(regex_password_numerica);
            Matcher matchePasswordNumerica = patternPasswordNumerica.matcher(password);

            if (!matchePasswordMaiuscola.matches()) {
                return ResponseHelper.DWH_UPDATE_PASSWORD_UPPER_LESS;
            }

            if (!matchePasswordMinuscola.matches()) {
                return ResponseHelper.DWH_UPDATE_PASSWORD_LOWER_LESS;
            }

            if (!matchePasswordNumerica.matches()) {
                return ResponseHelper.DWH_UPDATE_PASSWORD_NUMBER_LESS;
            }
        }

        return ResponseHelper.DWH_UPDATE_PASSWORD_SUCCESS;

    }
}
