package com.techedge.mp.core.actions.voucher;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ConstantsHelper;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.voucher.RetrieveVoucherTransactionDataResponse;
import com.techedge.mp.core.business.interfaces.voucher.VoucherTransactionOperation;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionOperationBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionStatusBean;
import com.techedge.mp.core.business.utilities.PanHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.TransactionFinalStatusConverter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class RetrieveVoucherTransactionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public RetrieveVoucherTransactionAction() {}

    public RetrieveVoucherTransactionDataResponse execute(String requestID, String ticketID, String voucherTransactionID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();
            RetrieveVoucherTransactionDataResponse retrieveVoucherTransactionDetailResponse = new RetrieveVoucherTransactionDataResponse();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                retrieveVoucherTransactionDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_VOUCHER_TRANSACTION_DETAIL_INVALID_TICKET);
                return retrieveVoucherTransactionDetailResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                retrieveVoucherTransactionDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_VOUCHER_TRANSACTION_DETAIL_INVALID_TICKET);
                return retrieveVoucherTransactionDetailResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus);

                userTransaction.commit();

                retrieveVoucherTransactionDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_VOUCHER_TRANSACTION_DETAIL_INVALID_TICKET);
                return retrieveVoucherTransactionDetailResponse;
            }

            // Controlla se esiste una transazione di acquisto voucher con id voucherTransactionID

            VoucherTransactionBean voucherTransactionBean = QueryRepository.findVoucherTransactionBeanByID(em, voucherTransactionID);

            if (voucherTransactionBean == null) {

                // Il voucherTransactionID inserito non corrisponde a nessuna transazione valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No voucher transaction for voucherTransactionID "
                        + voucherTransactionID);

                userTransaction.commit();

                retrieveVoucherTransactionDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_VOUCHER_TRANSACTION_DETAIL_NOT_RECOGNIZED);
                return retrieveVoucherTransactionDetailResponse;
            }
            else {

                // Trovata una transazione voucher
                retrieveVoucherTransactionDetailResponse.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_VOUCHER);
                retrieveVoucherTransactionDetailResponse.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_VOUCHER);

                retrieveVoucherTransactionDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_VOUCHER_TRANSACTION_DETAIL_SUCCESS);

                retrieveVoucherTransactionDetailResponse.setVoucherTransactionID(voucherTransactionBean.getVoucherTransactionID());

                retrieveVoucherTransactionDetailResponse.setCreationTimestamp(voucherTransactionBean.getCreationTimestamp());
                retrieveVoucherTransactionDetailResponse.setAmount(voucherTransactionBean.getAmount());
                retrieveVoucherTransactionDetailResponse.setBankTansactionID(voucherTransactionBean.getBankTansactionID());
                retrieveVoucherTransactionDetailResponse.setAuthorizationCode(voucherTransactionBean.getAuthorizationCode());
                retrieveVoucherTransactionDetailResponse.setPanCode(PanHelper.maskPan(voucherTransactionBean.getToken()));
                retrieveVoucherTransactionDetailResponse.setPaymentMethodId(voucherTransactionBean.getPaymentMethodId());
                retrieveVoucherTransactionDetailResponse.setPaymentMethodType(voucherTransactionBean.getPaymentMethodType());
                retrieveVoucherTransactionDetailResponse.setPaymentType(voucherTransactionBean.getPaymentType());
                
                if( voucherTransactionBean.getPaymentTokenPackageBean() != null ) {
                    retrieveVoucherTransactionDetailResponse.setPaymentTokenPackage(voucherTransactionBean.getPaymentTokenPackageBean().toPaymentTokenPackage());
                }

                // Si estrae lo stato pi� recente associato alla transazione
                VoucherTransactionStatusBean lastVoucherTransactionStatusBean = voucherTransactionBean.getLastTransactionStatus();

                if (lastVoucherTransactionStatusBean != null) {

                    retrieveVoucherTransactionDetailResponse.setStatus(lastVoucherTransactionStatusBean.getStatus());
                    retrieveVoucherTransactionDetailResponse.setSubStatus(lastVoucherTransactionStatusBean.getSubStatus());
                }

                String statusTitle = null;
                String statusDescription = null;

                if (voucherTransactionBean.getFinalStatusType() != null) {
                    
                    String status = voucherTransactionBean.getFinalStatusType();
                    String subStatus = (lastVoucherTransactionStatusBean != null ? lastVoucherTransactionStatusBean.getStatus() : null);
                    statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, subStatus);
                    statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, subStatus);
                }

                retrieveVoucherTransactionDetailResponse.setAmount(voucherTransactionBean.getAmount());
                retrieveVoucherTransactionDetailResponse.setStatusTitle(statusTitle);
                retrieveVoucherTransactionDetailResponse.setStatusDescription(statusDescription);

                if (!voucherTransactionBean.getTransactionOperationBeanList().isEmpty()) {

                    System.out.println("Inizio lettura dati operazioni voucher");

                    for (VoucherTransactionOperationBean voucherTransactionOperationBean : voucherTransactionBean.getTransactionOperationBeanList()) {

                        VoucherTransactionOperation voucherTransactionOperation = new VoucherTransactionOperation();
                        
                        voucherTransactionOperation.setAmount(voucherTransactionOperationBean.getAmount());
                        voucherTransactionOperation.setCsTransactionID(voucherTransactionOperationBean.getCsTransactionID());
                        voucherTransactionOperation.setId(voucherTransactionOperationBean.getId());
                        voucherTransactionOperation.setMarketingMsg(voucherTransactionOperationBean.getMarketingMsg());
                        voucherTransactionOperation.setMessageCode(voucherTransactionOperationBean.getMessageCode());
                        voucherTransactionOperation.setOperationID(voucherTransactionOperationBean.getOperationID());
                        voucherTransactionOperation.setOperationIDReversed(voucherTransactionOperationBean.getOperationIDReversed());
                        voucherTransactionOperation.setOperationType(voucherTransactionOperationBean.getOperationType());
                        voucherTransactionOperation.setRequestTimestamp(voucherTransactionOperationBean.getRequestTimestamp());
                        voucherTransactionOperation.setStatusCode(voucherTransactionOperationBean.getStatusCode());
                        voucherTransactionOperation.setWarningMsg(voucherTransactionOperationBean.getWarningMsg());

                        retrieveVoucherTransactionDetailResponse.getVoucherOperationList().add(voucherTransactionOperation);
                    }
                    
                    System.out.println("Fine lettura dati voucher");
                }
                
                Voucher voucher = new Voucher();
                voucher.setCode(voucherTransactionBean.getVoucherCode());
                
                for (VoucherBean voucherBean : voucherTransactionBean.getUserBean().getVoucherList()) {
                    if (voucherBean.getCode().equals(voucherTransactionBean.getVoucherCode())) {
                        voucher.setConsumedValue(voucherBean.getConsumedValue());
                        voucher.setExpirationDate(voucherBean.getExpirationDate());
                        voucher.setInitialValue(voucherBean.getInitialValue());
                        voucher.setIsCombinable(voucherBean.getIsCombinable());
                        voucher.setMinAmount(voucherBean.getMinAmount());
                        voucher.setMinQuantity(voucherBean.getMinQuantity());
                        voucher.setPromoCode(voucherBean.getPromoCode());
                        voucher.setPromoDescription(voucherBean.getPromoDescription());
                        voucher.setPromoDoc(voucherBean.getPromoDoc());
                        voucher.setPromoPartner(voucherBean.getPromoPartner());
                        voucher.setStatus(voucherBean.getStatus());
                        voucher.setType(voucherBean.getType());
                        voucher.setValidPV(voucherBean.getValidPV());
                        voucher.setValue(voucherBean.getValue());
                        voucher.setVoucherBalanceDue(voucherBean.getVoucherBalanceDue());
                        
                        break;
                    }
                }
                
                retrieveVoucherTransactionDetailResponse.setVoucher(voucher);

            }
                        
            userTransaction.commit();

            return retrieveVoucherTransactionDetailResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieving voucher transaction with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}
