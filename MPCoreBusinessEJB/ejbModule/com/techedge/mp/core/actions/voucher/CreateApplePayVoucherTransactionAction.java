package com.techedge.mp.core.actions.voucher;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.interfaces.voucher.CreateApplePayVoucherTransactionResult;
import com.techedge.mp.core.business.interfaces.voucher.CreateVoucherTransactionResult;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.ApplePayPurchaseVoucher;
import com.techedge.mp.core.business.voucher.PurchaseVoucher;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class CreateApplePayVoucherTransactionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public CreateApplePayVoucherTransactionAction() {}

    public CreateApplePayVoucherTransactionResult execute(String ticketId, String requestId, Double amount, String shopLogin, String acquirerID, String currency, String applePayPKPaymentToken,
            String paymentType, Integer reconciliationMaxAttemps, Integer pinCheckMaxAttempts, List<String> userBlockExceptionList, UserCategoryService userCategoryService, EmailSenderRemote emailSender, 
            GPServiceRemote gpService, FidelityServiceRemote fidelityService, UnavailabilityPeriodService unavailabilityPeriodService, String proxyHost, String proxyPort, String proxyNoHosts) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        
        CreateApplePayVoucherTransactionResult createApplePayVoucherTransactionResult = new CreateApplePayVoucherTransactionResult();

        try {
            userTransaction.begin();
            
            // Verifica parametro shopLogin
            if ( shopLogin == null || shopLogin.equals("") ) {
                
                // Servizio non disponibile
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "CreateVoucherService unavailable");

                userTransaction.commit();

                createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_FAILURE);
                return createApplePayVoucherTransactionResult;
            }

            // Verifica il ticket
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                
                createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_INVALID_TICKET);
                return createApplePayVoucherTransactionResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_INVALID_TICKET);
                return createApplePayVoucherTransactionResult;
            }
            
            if ( userBlockExceptionList.contains(userBean.getPersonalDataBean().getSecurityDataEmail()) ) {
                
                System.out.println(userBean.getPersonalDataBean().getSecurityDataEmail() + " presente in lista utenti con eccezioni blocco");
            }
            else {
                // Controllo su disponibilit� del servizio di creazione transazioni prepaid
                ServiceAvailabilityData serviceAvailabilityData = unavailabilityPeriodService.retrieveServiceAvailability("CREATE_TRANSACTION_VOUCHER",  new Date());
                if ( serviceAvailabilityData != null ) {
                    
                 // Servizio non disponibile
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "CreateVoucherService unavailable");
    
                    userTransaction.commit();
    
                    createApplePayVoucherTransactionResult.setStatusCode(serviceAvailabilityData.getStatusCode());
                    return createApplePayVoucherTransactionResult;
                }
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User is unable to purchase voucher in status " + userStatus);

                userTransaction.commit();

                createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_INVALID_USER_STATUS);
                return createApplePayVoucherTransactionResult;
            }

            Integer userType = userBean.getUserType();
            Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());

            if (!useNewFlow) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User is unable to purchase voucher in type " + userType);

                userTransaction.commit();

                createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_INVALID_USER_TYPE);
                return createApplePayVoucherTransactionResult;
            }

            if (!userBean.checkPaymentVoucherType()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "User is unable to purchase voucher. Payment voucher type required");

                userTransaction.commit();

                createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_PAYMENT_VOUCHER_TYPE_REQUIRED);
                return createApplePayVoucherTransactionResult;
            }

            double capAvailable = 0.0;

            if (userBean.getCapAvailable() != null) {
                capAvailable = userBean.getCapAvailable().doubleValue();
            }

            /*
             * Veirficare se serve mantenere il check sul cap
             */
            /*
            if (paymentInfoBean.getType() == PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER && paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Credit voucher method not verified");

                Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean);

                userTransaction.commit();

                createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_PAYMENT_DATA_WRONG);
                return createApplePayVoucherTransactionResult;
            }

            if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {

                // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Payment not verified and transaction amount exceeding the cap");

                Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean);

                userTransaction.commit();

                createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_CAP_FAILURE);
                return createApplePayVoucherTransactionResult;
            }
            */
            
            // Verifica che l'utente abbia gi� associato il pin
            PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();
            
            if ( paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("") ) {
                
                // Errore Pin non ancora inserito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not inserted");
            
                userTransaction.commit();
                
                createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_FAILURE);
                return createApplePayVoucherTransactionResult;
            }
            
            
            ApplePayPurchaseVoucher applePayPurchaseVoucher = new ApplePayPurchaseVoucher(em, gpService, fidelityService, emailSender, proxyHost, proxyPort, proxyNoHosts);
            VoucherTransactionBean voucherTransactionBean = applePayPurchaseVoucher.buy(userBean, amount, shopLogin, acquirerID, currency, applePayPKPaymentToken,
                    paymentType, reconciliationMaxAttemps);

            if (voucherTransactionBean.getFinalStatusType().startsWith("ERROR_")) {
                userTransaction.commit();
                createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_ERROR);
                createApplePayVoucherTransactionResult.setVoucherCode(voucherTransactionBean.getVoucherCode());
                createApplePayVoucherTransactionResult.setVoucherTransactionID(voucherTransactionBean.getVoucherTransactionID());
                return createApplePayVoucherTransactionResult;
            }

            if (voucherTransactionBean.getFinalStatusType().startsWith("FAILED_")) {
                userTransaction.commit();
                if (voucherTransactionBean.getFinalStatusType().equals("FAILED_PAY_AUTH") || voucherTransactionBean.getFinalStatusType().equals("FAILED_PAY_MOV")) {
                    createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_PAYMENT_KO);
                }
                else {
                    createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_FAILURE);
                }
                createApplePayVoucherTransactionResult.setVoucherCode(voucherTransactionBean.getVoucherCode());
                createApplePayVoucherTransactionResult.setVoucherTransactionID(voucherTransactionBean.getVoucherTransactionID());
                return createApplePayVoucherTransactionResult;
            }

            userTransaction.commit();

            createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_APPLE_PAY_CREATE_SUCCESS);
            createApplePayVoucherTransactionResult.setVoucherCode(voucherTransactionBean.getVoucherCode());
            createApplePayVoucherTransactionResult.setVoucherTransactionID(voucherTransactionBean.getVoucherTransactionID());
            return createApplePayVoucherTransactionResult;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED voucher creation with apple pay with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
