package com.techedge.mp.core.actions.voucher;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.interfaces.voucher.CreateVoucherTransactionResult;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.PurchaseVoucher;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class CreateVoucherTransactionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public CreateVoucherTransactionAction() {}

    public CreateVoucherTransactionResult execute(String ticketId, String requestId, String encodedPin, Double amount, String shopLogin, String acquirerID, String currency, Long paymentMethodId,
            String paymentMethodType, String paymentType, Integer reconciliationMaxAttemps, Integer pinCheckMaxAttempts, List<String> userBlockExceptionList, UserCategoryService userCategoryService, EmailSenderRemote emailSender, 
            GPServiceRemote gpService, FidelityServiceRemote fidelityService, UnavailabilityPeriodService unavailabilityPeriodService, String proxyHost, String proxyPort, String proxyNoHosts) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        CreateVoucherTransactionResult createVoucherTransactionResult = new CreateVoucherTransactionResult();

        try {
            userTransaction.begin();
            
            // Verifica parametro shopLogin
            if ( shopLogin == null || shopLogin.equals("") ) {
                
                // Servizio non disponibile
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "CreateVoucherService unavailable");

                userTransaction.commit();

                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_FAILURE);
                return createVoucherTransactionResult;
            }

            // Verifica il ticket
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_INVALID_TICKET);
                return createVoucherTransactionResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_INVALID_TICKET);
                return createVoucherTransactionResult;
            }
            
            if ( userBlockExceptionList.contains(userBean.getPersonalDataBean().getSecurityDataEmail()) ) {
                
                System.out.println(userBean.getPersonalDataBean().getSecurityDataEmail() + " presente in lista utenti con eccezioni blocco");
            }
            else {
                // Controllo su disponibilit� del servizio di creazione transazioni prepaid
                ServiceAvailabilityData serviceAvailabilityData = unavailabilityPeriodService.retrieveServiceAvailability("CREATE_TRANSACTION_VOUCHER",  new Date());
                if ( serviceAvailabilityData != null ) {
                    
                 // Servizio non disponibile
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "CreateVoucherService unavailable");
    
                    userTransaction.commit();
    
                    createVoucherTransactionResult.setStatusCode(serviceAvailabilityData.getStatusCode());
                    return createVoucherTransactionResult;
                }
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User is unable to purchase voucher in status " + userStatus);

                userTransaction.commit();

                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_INVALID_USER_STATUS);
                return createVoucherTransactionResult;
            }

            Integer userType = userBean.getUserType();
            Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());
            useNewFlow = Boolean.TRUE;
            if (!useNewFlow) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User is unable to purchase voucher in type " + userType);

                userTransaction.commit();

                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_INVALID_USER_TYPE);
                return createVoucherTransactionResult;
            }

            if (!userBean.checkPaymentVoucherType()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "User is unable to purchase voucher. Payment voucher type required");

                userTransaction.commit();

                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_PAYMENT_VOUCHER_TYPE_REQUIRED);
                return createVoucherTransactionResult;
            }

            // Se il medodo di pagamento non � specificato utilizza quello di default
            PaymentInfoBean paymentInfoBean = null;
            if (paymentMethodId == null && paymentMethodType == null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Default method found");

                paymentInfoBean = userBean.findDefaultPaymentInfoBean();
            }
            else {

                paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
            }

            // Verifica che il metodo di pagamento selezionato sia in uno stato valido
            if (paymentInfoBean == null) {

                // Il metodo di pagamento selezionato non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong payment data");

                userTransaction.commit();
                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_PAYMENT_DATA_WRONG);
                return createVoucherTransactionResult;
            }

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                    + paymentMethodType);

            if (!paymentInfoBean.isValid()) {

                // Il metodo di pagamento selezionato non pu� essere utilizzato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

                userTransaction.commit();

                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_PAYMENT_DATA_WRONG);
                return createVoucherTransactionResult;
            }

            double capAvailable = 0.0;

            if (userBean.getCapAvailable() != null) {
                capAvailable = userBean.getCapAvailable().doubleValue();
            }

            if (paymentInfoBean.getType() == PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER && paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Credit voucher method not verified");

                Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean, null, null);

                userTransaction.commit();

                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_PAYMENT_DATA_WRONG);
                return createVoucherTransactionResult;
            }

            if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {

                // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Payment not verified and transaction amount exceeding the cap");

                Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean, null, null);

                userTransaction.commit();

                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_CAP_FAILURE);
                return createVoucherTransactionResult;
            }
            
            // Verifica che l'utente abbia gi� associato il pin
            PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();
            
            if ( paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("") ) {
                
                // Errore Pin non ancora inserito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not inserted");
            
                userTransaction.commit();
                
                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_FAILURE);
                return createVoucherTransactionResult;
            }
            
            // Verifica che il pin indicato per il rifornimento coincida con quello inserito in iscrizione
            if (!encodedPin.equals(paymentInfoVoucherBean.getPin())) {

                // Il pin inserito non � valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong pin");

                // Si sottrae uno al numero di tentativi residui
                Integer pinCheckAttemptsLeft = paymentInfoVoucherBean.getPinCheckAttemptsLeft();
                if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                    pinCheckAttemptsLeft--;
                }
                else {
                    pinCheckAttemptsLeft = 0;
                }

                if (pinCheckAttemptsLeft == 0) {

                    // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                    paymentInfoVoucherBean.setDefaultMethod(false);
                    paymentInfoVoucherBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
                    
                    // e tutti i metodi di pagamento verificati e non verificati sono cancellati
                    for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {

                        if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                                && (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                            paymentInfo.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                            paymentInfo.setDefaultMethod(false);
                            em.merge(paymentInfo);
                        }
                    }
                }

                paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

                em.merge(paymentInfoVoucherBean);

                userTransaction.commit();

                createVoucherTransactionResult.setPinCheckMaxAttempts(paymentInfoVoucherBean.getPinCheckAttemptsLeft());
                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_PIN_WRONG);
                return createVoucherTransactionResult;
            }
            
            // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
            paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);

            em.merge(paymentInfoVoucherBean);

            PurchaseVoucher purchaseVoucher = new PurchaseVoucher(em, gpService, fidelityService, emailSender, proxyHost, proxyPort, proxyNoHosts);
            VoucherTransactionBean voucherTransactionBean = purchaseVoucher.buy(userBean, amount, shopLogin, acquirerID, currency, paymentInfoBean, 
                    paymentMethodType, paymentType, reconciliationMaxAttemps);

            if (voucherTransactionBean.getFinalStatusType().startsWith("ERROR_")) {
                userTransaction.commit();
                createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_ERROR);
                createVoucherTransactionResult.setVoucherCode(voucherTransactionBean.getVoucherCode());
                createVoucherTransactionResult.setVoucherTransactionID(voucherTransactionBean.getVoucherTransactionID());
                return createVoucherTransactionResult;
            }

            if (voucherTransactionBean.getFinalStatusType().startsWith("FAILED_")) {
                userTransaction.commit();
                if (voucherTransactionBean.getFinalStatusType().equals("FAILED_PAY_AUTH") || voucherTransactionBean.getFinalStatusType().equals("FAILED_PAY_MOV")) {
                    createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_PAYMENT_KO);
                }
                else {
                    createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_FAILURE);
                }
                createVoucherTransactionResult.setVoucherCode(voucherTransactionBean.getVoucherCode());
                createVoucherTransactionResult.setVoucherTransactionID(voucherTransactionBean.getVoucherTransactionID());
                return createVoucherTransactionResult;
            }

            userTransaction.commit();

            createVoucherTransactionResult.setStatusCode(ResponseHelper.VOUCHER_CREATE_SUCCESS);
            createVoucherTransactionResult.setVoucherCode(voucherTransactionBean.getVoucherCode());
            createVoucherTransactionResult.setVoucherTransactionID(voucherTransactionBean.getVoucherTransactionID());
            return createVoucherTransactionResult;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
