package com.techedge.mp.core.actions.transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.EventInfo;
import com.techedge.mp.core.business.interfaces.PaymentRefuelDetailResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveTransactionEventsData;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionEventHistoryBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.TransactionStatusHistoryBean;
import com.techedge.mp.core.business.utilities.PanHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionRetrieveEventsDataAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public TransactionRetrieveEventsDataAction() {
    }

    
    public RetrieveTransactionEventsData execute(
    		String requestID,
    		String transactionID) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		RetrieveTransactionEventsData retrieveTransactionEventsData = new RetrieveTransactionEventsData();
    		
    		PaymentRefuelDetailResponse paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
    		
    		TransactionBean transactionBean               = QueryRepository.findTransactionBeanById(em, transactionID);
    		TransactionHistoryBean transactionHistoryBean = QueryRepository.findTransactionHistoryBeanById(em, transactionID);
    		
    		EventInfo eventInfo;
    		List<EventInfo> transactionEventList = new ArrayList<EventInfo>(0);
    		
    		if (transactionBean == null) {
    			
    			if (transactionHistoryBean == null) {
    				
    				retrieveTransactionEventsData.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_EVENTS_FAILURE);
    				retrieveTransactionEventsData.setMessageCode("Transaction Not Recognized");
    				
    				userTransaction.commit();
    				
    				return  retrieveTransactionEventsData;
    			}
    			else {
    				
    				// Si estrae lo stato pi� recente associato alla transazione
    				Integer lastSequenceID = 0;
    				TransactionStatusHistoryBean lastTransactionStatusBean = null;
    				Set<TransactionStatusHistoryBean> transactionStatusData = transactionHistoryBean.getTransactionStatusHistoryBeanData();
    				for( TransactionStatusHistoryBean transactionStatusBean : transactionStatusData ) {
    					Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
    					if ( lastSequenceID < transactionStatusBeanSequenceID ) {
    						lastSequenceID = transactionStatusBeanSequenceID;
    						lastTransactionStatusBean = transactionStatusBean;
    					}
    				}
    				
    				paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_SUCCESS);
    				paymentRefuelDetailResponse.setRefuelID(transactionHistoryBean.getTransactionID());
    				paymentRefuelDetailResponse.setStatus(lastTransactionStatusBean.getStatus());
    				paymentRefuelDetailResponse.setSubStatus(lastTransactionStatusBean.getSubStatus());
    				paymentRefuelDetailResponse.setUseVoucher(transactionHistoryBean.getUseVoucher());
    				paymentRefuelDetailResponse.setDate(lastTransactionStatusBean.getTimestamp());
    				paymentRefuelDetailResponse.setInitialAmount(transactionHistoryBean.getInitialAmount());
    				paymentRefuelDetailResponse.setFinalAmount(transactionHistoryBean.getFinalAmount());
    				paymentRefuelDetailResponse.setFuelAmount(transactionHistoryBean.getFuelAmount());
    				paymentRefuelDetailResponse.setFuelQuantity(transactionHistoryBean.getFuelQuantity());

    				paymentRefuelDetailResponse.setBankTransactionID(transactionHistoryBean.getBankTansactionID());
    				paymentRefuelDetailResponse.setShopLogin(transactionHistoryBean.getShopLogin());
    				paymentRefuelDetailResponse.setCurrency(transactionHistoryBean.getCurrency());
    				String maskedPan = PanHelper.maskPan(transactionHistoryBean.getToken());
    				paymentRefuelDetailResponse.setMaskedPan(maskedPan);
    				paymentRefuelDetailResponse.setAuthCode(transactionHistoryBean.getAuthorizationCode());

    				paymentRefuelDetailResponse.setSelectedStationID(transactionHistoryBean.getStationBean().getStationID());
    				// TODO - il nome della stazione non viene restituito
    				paymentRefuelDetailResponse.setSelectedStationName("");
    				paymentRefuelDetailResponse.setSelectedStationAddress(transactionHistoryBean.getStationBean().getAddress());
    				paymentRefuelDetailResponse.setSelectedStationLatitude(transactionHistoryBean.getStationBean().getLatitude());
    				paymentRefuelDetailResponse.setSelectedStationLongitude(transactionHistoryBean.getStationBean().getLongitude());
    				paymentRefuelDetailResponse.setSelectedPumpID(transactionHistoryBean.getPumpID());
    				paymentRefuelDetailResponse.setSelectedPumpNumber(transactionHistoryBean.getPumpNumber());
    				paymentRefuelDetailResponse.setSelectedPumpFuelType(transactionHistoryBean.getProductDescription());
    				
    				Set<TransactionEventHistoryBean>  transactionEventBeanList = transactionHistoryBean.getTransactionEventHistoryBeanData();
    				
    				for( TransactionEventHistoryBean transactionEventBean : transactionEventBeanList ) {
    					
    					eventInfo = new EventInfo();
    					
    					eventInfo.setTransactionResult(transactionEventBean.getTransactionResult());
    					eventInfo.setErrorCode(transactionEventBean.getErrorCode());
    					eventInfo.setErrorDescription(transactionEventBean.getErrorDescription());
    					eventInfo.setEventAmount(transactionEventBean.getEventAmount());
    					eventInfo.setEventType(transactionEventBean.getEventType());
    					eventInfo.setSequence(transactionEventBean.getSequenceID().toString());
    					
    					transactionEventList.add(eventInfo);
    				}
    				
    				retrieveTransactionEventsData.setAcquirerID(transactionHistoryBean.getAcquirerID());
    				retrieveTransactionEventsData.setPaymentMode(transactionHistoryBean.getPaymentType());
    				retrieveTransactionEventsData.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_EVENTS_SUCCESS);
    				retrieveTransactionEventsData.setMessageCode("Message Received");
    				
    			}
    		}
    		else {
    			
    			TransactionBean pendingTransactionBean = transactionBean;

    			// Si estrae lo stato pi� recente associato alla transazione
    			Integer lastSequenceID = 0;
    			TransactionStatusBean lastTransactionStatusBean = null;
    			Set<TransactionStatusBean> transactionStatusData = pendingTransactionBean.getTransactionStatusBeanData();
    			for( TransactionStatusBean transactionStatusBean : transactionStatusData ) {
    				Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
    				if ( lastSequenceID < transactionStatusBeanSequenceID ) {
    					lastSequenceID = transactionStatusBeanSequenceID;
    					lastTransactionStatusBean = transactionStatusBean;
    				}
    			}
    			
    			paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_SUCCESS);
    			paymentRefuelDetailResponse.setRefuelID(pendingTransactionBean.getTransactionID());
    			paymentRefuelDetailResponse.setStatus(lastTransactionStatusBean.getStatus());
    			paymentRefuelDetailResponse.setSubStatus(lastTransactionStatusBean.getSubStatus());
    			paymentRefuelDetailResponse.setUseVoucher(pendingTransactionBean.getUseVoucher());
    			paymentRefuelDetailResponse.setDate(lastTransactionStatusBean.getTimestamp());
    			paymentRefuelDetailResponse.setInitialAmount(pendingTransactionBean.getInitialAmount());
    			paymentRefuelDetailResponse.setFinalAmount(pendingTransactionBean.getFinalAmount());
    			paymentRefuelDetailResponse.setFuelAmount(pendingTransactionBean.getFuelAmount());
    			paymentRefuelDetailResponse.setFuelQuantity(pendingTransactionBean.getFuelQuantity());

    			paymentRefuelDetailResponse.setBankTransactionID(pendingTransactionBean.getBankTansactionID());
    			paymentRefuelDetailResponse.setShopLogin(pendingTransactionBean.getShopLogin());
    			paymentRefuelDetailResponse.setCurrency(pendingTransactionBean.getCurrency());
    			String maskedPan = PanHelper.maskPan(pendingTransactionBean.getToken());
    			paymentRefuelDetailResponse.setMaskedPan(maskedPan);
    			paymentRefuelDetailResponse.setAuthCode(pendingTransactionBean.getAuthorizationCode());

    			paymentRefuelDetailResponse.setSelectedStationID(pendingTransactionBean.getStationBean().getStationID());
    			// TODO - il nome della stazione non viene restituito
    			paymentRefuelDetailResponse.setSelectedStationName("");
    			paymentRefuelDetailResponse.setSelectedStationAddress(pendingTransactionBean.getStationBean().getAddress());
    			paymentRefuelDetailResponse.setSelectedStationLatitude(pendingTransactionBean.getStationBean().getLatitude());
    			paymentRefuelDetailResponse.setSelectedStationLongitude(pendingTransactionBean.getStationBean().getLongitude());
    			paymentRefuelDetailResponse.setSelectedPumpID(pendingTransactionBean.getPumpID());
    			paymentRefuelDetailResponse.setSelectedPumpNumber(pendingTransactionBean.getPumpNumber());
    			paymentRefuelDetailResponse.setSelectedPumpFuelType(pendingTransactionBean.getProductDescription());
    			
    			Set<TransactionEventBean>  transactionEventBeanList = transactionBean.getTransactionEventBeanData();
    			
    			for( TransactionEventBean transactionEventBean : transactionEventBeanList ) {
    				
    				eventInfo = new EventInfo();
    				
    				eventInfo.setTransactionResult(transactionEventBean.getTransactionResult());
    				eventInfo.setErrorCode(transactionEventBean.getErrorCode());
    				eventInfo.setErrorDescription(transactionEventBean.getErrorDescription());
    				eventInfo.setEventAmount(transactionEventBean.getEventAmount());
    				eventInfo.setEventType(transactionEventBean.getEventType());
    				eventInfo.setSequence(transactionEventBean.getSequenceID().toString());
    				
    				transactionEventList.add(eventInfo);
    			}
    			
    			retrieveTransactionEventsData.setAcquirerID(transactionBean.getAcquirerID());
    			retrieveTransactionEventsData.setPaymentMode(transactionBean.getPaymentType());
    			retrieveTransactionEventsData.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_EVENTS_SUCCESS);
    			retrieveTransactionEventsData.setMessageCode("Message Received");
    			
    		}
    		
    		retrieveTransactionEventsData.setTransactionEvents(transactionEventList);
    		retrieveTransactionEventsData.setPaymentRefuelDetailResponse(paymentRefuelDetailResponse);
    		
    		userTransaction.commit();
    		
    		return retrieveTransactionEventsData;
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED retrieve events data with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}