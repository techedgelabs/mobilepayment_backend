package com.techedge.mp.core.actions.transaction.v2;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.TransactionV2Service;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.CreateMulticardRefuelResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionAdditionalDataBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionOperationBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.voucher.PaymentTokenPackageBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CodeEnum;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnumStatusType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteAuthorizationResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.KeyValueInfo;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetailInfo;
import com.techedge.mp.fidelity.adapter.business.utilities.AmountConverter;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionV2CreateMulticardRefuelAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionV2CreateMulticardRefuelAction() {}

    public CreateMulticardRefuelResponse execute(String ticketID, String requestID, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, String currency, Long paymentMethodId, String paymentType, String paymentCryptogram, String outOfRange,
            String refuelMode, String defaultShopLogin, String defaultAcquirerID, Integer pinCheckMaxAttempts, Integer maxStatusAttempts, Integer reconciliationMaxAttempts, 
            Integer transactionAmountFullMulticard, String serverName, List<String> userBlockExceptionList, EmailSenderRemote emailSender, UserCategoryService userCategoryService, TransactionV2Service transactionV2Service, GPServiceRemote gpService,
            PaymentServiceRemote paymentService, UnavailabilityPeriodService unavailabilityPeriodService, String proxyHost, String proxyPort, String proxyNoHosts)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();
            
            CreateMulticardRefuelResponse createMulticardRefuelResponse = new CreateMulticardRefuelResponse();
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ServerName: " + serverName);

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                createMulticardRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return createMulticardRefuelResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                createMulticardRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return createMulticardRefuelResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus);

                userTransaction.commit();

                createMulticardRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_UNAUTHORIZED);
                return createMulticardRefuelResponse;
            }

            // Controlla se l'utente ha gi� una transazione attiva
            List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);

            if (!transactionBeanList.isEmpty()) {

                // Esiste una transazione associata all'utente non ancora completata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction found");

                userTransaction.commit();

                createMulticardRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
                return createMulticardRefuelResponse;
            }
            
            // Controllo sull'importo richiesto
            Double transactionAmountFullMulticardDouble = AmountConverter.toInternal(transactionAmountFullMulticard);
            
            if (amount > transactionAmountFullMulticardDouble) {
                
                // L'importo richiesto � maggiore di quello consentito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid amount");

                userTransaction.commit();

                createMulticardRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_MULTICARD_INVALID_AMOUNT);
                return createMulticardRefuelResponse;
            }
            
            createMulticardRefuelResponse = this.createMulticardRefuelTransaction(
                    requestID,
                    userTransaction,
                    userBean,
                    paymentCryptogram,
                    paymentMethodId,
                    amount,
                    stationID,
                    pumpID,
                    pinCheckMaxAttempts,
                    defaultShopLogin,
                    defaultAcquirerID,
                    pumpNumber,
                    currency,
                    paymentType,
                    serverName,
                    maxStatusAttempts,
                    reconciliationMaxAttempts,
                    outOfRange,
                    refuelMode,
                    userCategoryService,
                    emailSender,
                    gpService,
                    paymentService, 
                    proxyHost,
                    proxyPort,
                    proxyNoHosts);

            return createMulticardRefuelResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED multicard prepaid transaction creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
    
    
    private CreateMulticardRefuelResponse createMulticardRefuelTransaction (
            String requestID,
            UserTransaction userTransaction,
            UserBean userBean,
            String paymentCryptogram,
            Long paymentMethodId,
            Double amount,
            String stationID,
            String pumpID,
            Integer pinCheckMaxAttempts,
            String defaultShopLogin,
            String defaultAcquirerID,
            Integer pumpNumber,
            String currency,
            String paymentType,
            String serverName,
            Integer maxStatusAttempts,
            Integer reconciliationMaxAttempts,
            String outOfRange,
            String refuelMode,
            UserCategoryService userCategoryService,
            EmailSenderRemote emailSender,
            GPServiceRemote gpService,
            PaymentServiceRemote paymentService, 
            String proxyHost,
            String proxyPort,
            String proxyNoHosts) throws Exception {
        
        System.out.println("Creazione transazione prepaid Multicard");
        
        CreateMulticardRefuelResponse createMulticardRefuelResponse = new CreateMulticardRefuelResponse();
        
        // Verifica lo stationID
        StationBean stationBean = QueryRepository.findStationBeanById(em, stationID);

        if (stationBean == null) {

            // Lo stationID inserito non corrisponde a nessuna stazione valida
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID);

            userTransaction.commit();

            createMulticardRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createMulticardRefuelResponse;
        }
        
        
        // Verifica che l'utente abbia gi� associato il pin (il controllo non deve essere effettuato se l'utente � di tipo Multicard)
        if (userBean.getSource() != null && !userBean.getSource().equals(User.USER_MULTICARD)) {
            
            PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();
            
            if ( paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("") ) {
                
                // Errore Pin non ancora inserito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");
            
                userTransaction.commit();
                
                createMulticardRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
                return createMulticardRefuelResponse;
            }
        }
        

        // Verifica il metodo utilizzato per il pagamento
        System.out.println("Verifica metodo di pagamento");

        if (paymentMethodId == null) {

            // Se il medodo di pagamento non � specificato restituisci un errore
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "PaymentMethodId not inserted");
            
            userTransaction.commit();
            
            createMulticardRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createMulticardRefuelResponse;
        }
        
        
        PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);

        // Verifica che il metodo di pagamento selezionato sia valido
        if (paymentInfoBean == null) {

            // Il metodo di pagamento selezionato non esiste
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

            userTransaction.commit();

            createMulticardRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
            return createMulticardRefuelResponse;
        }
        

        if (paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

            // Il metodo di pagamento selezionato non pu� essere utilizzato
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());
            
            String statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG;
            
            if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_MULTICARD_NOT_ACTIVE;
            }
            
            userTransaction.commit();
            
            createMulticardRefuelResponse.setStatusCode(statusCode);
            return createMulticardRefuelResponse;
        }
        
        
        if ( paymentCryptogram == null ) {
            
            // Errore PaymentCryptogram non inserito
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "PaymentCryptogram not inserted");
        
            userTransaction.commit();
            
            createMulticardRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createMulticardRefuelResponse;
        }
        
        Date now = new Date();
        
        String mcCardDpan = paymentInfoBean.getToken();
        Long requestTimestamp = now.getTime();
        
        String shopLogin  = defaultShopLogin;
        String acquirerID = "MULTICARD";
        
        shopLogin  = "ENIPAY" + stationBean.getStationID();

        String transactionID = new IdGenerator().generateId(16).substring(0, 32);
        
        Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());
        
        PaymentTokenPackageBean paymentTokenPackageBean = new PaymentTokenPackageBean();
        paymentTokenPackageBean.setData(paymentCryptogram);
        paymentTokenPackageBean.setType(PaymentTokenPackageBean.TYPE_MULTICARD);
        
        
        // Genera un operationID da utilizzare nella chiamata al servizio di preautorizzazione
        // String operationID = "000000" + new IdGenerator().generateId(16).substring(0, 26); //32);
        String operationID = new IdGenerator().generateId(16).substring(0, 32);
        
        System.out.println("Preautorizzazione credito con Multicard");
        
        Integer integerAmount = AmountConverter.toMobile(amount);
        
        ExecuteAuthorizationResult executeAuthorizationResult = paymentService.executeAuthorization(
                operationID,
                integerAmount,
                paymentCryptogram,
                currency,
                mcCardDpan,
                stationID,
                PartnerType.EM,
                requestTimestamp);
        
        if (executeAuthorizationResult == null) {
            
            // Gestione stato null
            
            // Crea la transazione con stato in errore per consentire la cancellazione dell'autorizzazione
            
            TransactionBean transactionBean = new TransactionBean();
            transactionBean.setTransactionID(transactionID);
            transactionBean.setUserBean(userBean);
            transactionBean.setStationBean(stationBean);
            transactionBean.setUseVoucher(false);
            transactionBean.setCreationTimestamp(creationTimestamp);
            transactionBean.setEndRefuelTimestamp(null);
            transactionBean.setInitialAmount(amount);
            transactionBean.setFinalAmount(null);
            transactionBean.setFuelAmount(null);
            transactionBean.setFuelQuantity(null);
            transactionBean.setPumpID(pumpID);
            transactionBean.setPumpNumber(pumpNumber);
            transactionBean.setCurrency(currency);
            transactionBean.setShopLogin(shopLogin);
            transactionBean.setAcquirerID(acquirerID);
            transactionBean.setBankTansactionID(null);
            transactionBean.setToken(mcCardDpan);
            transactionBean.setPaymentType(paymentType);
            transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_ERROR);
            transactionBean.setGFGNotification(false);
            transactionBean.setConfirmed(false);
            transactionBean.setPaymentMethodId(paymentInfoBean.getId());
            transactionBean.setPaymentMethodType(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);
            transactionBean.setServerName(serverName);
            transactionBean.setStatusAttemptsLeft(maxStatusAttempts);
            transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
            transactionBean.setOutOfRange(outOfRange);
            transactionBean.setRefuelMode(refuelMode);
            transactionBean.setVoucherReconciliation(false);
            transactionBean.setAuthorizationCode(null);
            transactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);
            transactionBean.setPaymentTokenPackageBean(paymentTokenPackageBean);
            
            em.persist(paymentTokenPackageBean);
            em.persist(transactionBean);
            
            this.persistTransactionErrorData(em, requestID, transactionBean, creationTimestamp, amount, StatusHelper.STATUS_PAYMENT_NOT_AUTHORIZED, StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_FAULT, "Negative Response (TransactionResult=ERROR)", "9999", "executeAuthorizationResult null", "ERROR", operationID, requestTimestamp, executeAuthorizationResult);
            
            // Errore di sistema nella chiamata al servizio di preautorizzazione con multicard
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "executeAuthorizationResult null");
        
            userTransaction.commit();
            
            createMulticardRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createMulticardRefuelResponse;
        }
        
        if (!executeAuthorizationResult.getResult().getStatus().value().equals(EnumStatusType.SUCCESS.value())) {
            
            // Gestione stato != SUCCESS
            CodeEnum code  = executeAuthorizationResult.getResult().getCode();
            String message = executeAuthorizationResult.getResult().getMessage();
            
            // Gestione errori in preautorizzazione
            
            if (code.getValue().equals(CodeEnum.TIMEOUT_OPERATION.getValue()) ||
                code.getValue().equals(CodeEnum.OPERATION_NOT_COMPLETED.getValue()) ||
                code.getValue().equals(CodeEnum.GENERIC_ERROR.getValue())) {
                
                String statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE;
                if (code.getValue().equals(CodeEnum.TIMEOUT_OPERATION.getValue())) {
                    statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_TIMEOUT_OPERATION;
                }
                else if (code.getValue().equals(CodeEnum.OPERATION_NOT_COMPLETED.getValue())) {
                    statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_OPERATION_NOT_COMPLETED;
                }
                else if (code.getValue().equals(CodeEnum.GENERIC_ERROR.getValue())) {
                    statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_GENERIC_ERROR;
                }
                
                // Crea la transazione con stato in errore per consentire la cancellazione dell'autorizzazione
                
                TransactionBean transactionBean = new TransactionBean();
                transactionBean.setTransactionID(transactionID);
                transactionBean.setUserBean(userBean);
                transactionBean.setStationBean(stationBean);
                transactionBean.setUseVoucher(false);
                transactionBean.setCreationTimestamp(creationTimestamp);
                transactionBean.setEndRefuelTimestamp(null);
                transactionBean.setInitialAmount(amount);
                transactionBean.setFinalAmount(null);
                transactionBean.setFuelAmount(null);
                transactionBean.setFuelQuantity(null);
                transactionBean.setPumpID(pumpID);
                transactionBean.setPumpNumber(pumpNumber);
                transactionBean.setCurrency(currency);
                transactionBean.setShopLogin(shopLogin);
                transactionBean.setAcquirerID(acquirerID);
                transactionBean.setBankTansactionID(null);
                transactionBean.setToken(mcCardDpan);
                transactionBean.setPaymentType(paymentType);
                transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_ERROR);
                transactionBean.setGFGNotification(false);
                transactionBean.setConfirmed(false);
                transactionBean.setPaymentMethodId(paymentInfoBean.getId());
                transactionBean.setPaymentMethodType(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);
                transactionBean.setServerName(serverName);
                transactionBean.setStatusAttemptsLeft(maxStatusAttempts);
                transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
                transactionBean.setOutOfRange(outOfRange);
                transactionBean.setRefuelMode(refuelMode);
                transactionBean.setVoucherReconciliation(false);
                transactionBean.setAuthorizationCode(null);
                transactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);
                transactionBean.setPaymentTokenPackageBean(paymentTokenPackageBean);
                
                em.persist(paymentTokenPackageBean);
                em.persist(transactionBean);
                
                this.persistTransactionErrorData(em, requestID, transactionBean, creationTimestamp, amount, StatusHelper.STATUS_PAYMENT_NOT_AUTHORIZED, StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_FAULT, "Negative Response (TransactionResult=ERROR)", code.getValue(), message, "ERROR", operationID, requestTimestamp, executeAuthorizationResult);
                
                // Errore di sistema nella chiamata al servizio di preautorizzazione con multicard
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "executeAuthorizationResult null");
            
                userTransaction.commit();
                
                createMulticardRefuelResponse.setStatusCode(statusCode);
                return createMulticardRefuelResponse;
            }
            
            
            // Crea la transazione con stato ko e mostra il messaggio di errore all'utente
            
            TransactionBean transactionBean = new TransactionBean();
            transactionBean.setTransactionID(transactionID);
            transactionBean.setUserBean(userBean);
            transactionBean.setStationBean(stationBean);
            transactionBean.setUseVoucher(false);
            transactionBean.setCreationTimestamp(creationTimestamp);
            transactionBean.setEndRefuelTimestamp(null);
            transactionBean.setInitialAmount(amount);
            transactionBean.setFinalAmount(null);
            transactionBean.setFuelAmount(null);
            transactionBean.setFuelQuantity(null);
            transactionBean.setPumpID(pumpID);
            transactionBean.setPumpNumber(pumpNumber);
            transactionBean.setCurrency(currency);
            transactionBean.setShopLogin(shopLogin);
            transactionBean.setAcquirerID(acquirerID);
            transactionBean.setBankTansactionID(null);
            transactionBean.setToken(mcCardDpan);
            transactionBean.setPaymentType(paymentType);
            transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_FAILED);
            transactionBean.setGFGNotification(false);
            transactionBean.setConfirmed(false);
            transactionBean.setPaymentMethodId(paymentInfoBean.getId());
            transactionBean.setPaymentMethodType(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);
            transactionBean.setServerName(serverName);
            transactionBean.setStatusAttemptsLeft(maxStatusAttempts);
            transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
            transactionBean.setOutOfRange(outOfRange);
            transactionBean.setRefuelMode(refuelMode);
            transactionBean.setVoucherReconciliation(false);
            transactionBean.setAuthorizationCode(null);
            transactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);
            transactionBean.setPaymentTokenPackageBean(paymentTokenPackageBean);
            
            em.persist(paymentTokenPackageBean);
            em.persist(transactionBean);
            
            this.persistTransactionErrorData(em, requestID, transactionBean, creationTimestamp, amount, StatusHelper.STATUS_PAYMENT_NOT_AUTHORIZED, "", "Negative Response (TransactionResult=KO)", code.getValue(), message, "KO", operationID, requestTimestamp, executeAuthorizationResult);
            
            String statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE;
            
            if (code.getValue().equals(CodeEnum.SUCCESS.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_PARAMETERS;
            }
            if (code.getValue().equals(CodeEnum.INVALID_PARAMETERS.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_PARAMETERS;
            }
            if (code.getValue().equals(CodeEnum.INVALID_OPERATION.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_OPERATION;
            }
            if (code.getValue().equals(CodeEnum.DPAN_NOT_FOUND.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_DPAN_NOT_FOUND;
            }
            if (code.getValue().equals(CodeEnum.OTP_NOT_FOUND.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_OTP_NOT_FOUND;
            }
            if (code.getValue().equals(CodeEnum.TOTP_NOT_FOUND.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_TOTP_NOT_FOUND;
            }
            if (code.getValue().equals(CodeEnum.INVALID_TOTP_OR_DPAN.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_TOTP_OR_DPAN;
            }
            if (code.getValue().equals(CodeEnum.INVALID_LOYALTY_CARD.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_LOYALTY_CARD;
            }
            if (code.getValue().equals(CodeEnum.INVALID_PARTNER_TYPE.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_PARTNER_TYPE;
            }
            if (code.getValue().equals(CodeEnum.CARD_ALREADY_ENABLED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_ALREADY_ENABLED;
            }
            if (code.getValue().equals(CodeEnum.MSGREASONCODE_MANDATORY.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_MSGREASONCODE_MANDATORY;
            }
            if (code.getValue().equals(CodeEnum.PAN_NOT_FOUND.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAN_NOT_FOUND;
            }
            if (code.getValue().equals(CodeEnum.PAN_ALREADY_USED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAN_ALREADY_USED;
            }
            if (code.getValue().equals(CodeEnum.EMAIL_ALREADY_USED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_EMAIL_ALREADY_USED;
            }
            if (code.getValue().equals(CodeEnum.PAYMENT_NOT_FOUND_FOR_OPERATION.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_NOT_FOUND_FOR_OPERATION;
            }
            if (code.getValue().equals(CodeEnum.USER_NOT_FOUND.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_USER_NOT_FOUND;
            }
            if (code.getValue().equals(CodeEnum.PAYMENT_WITH_ANOTHER_DEVICE.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_WITH_ANOTHER_DEVICE;
            }
            if (code.getValue().equals(CodeEnum.INVALID_CRIPTOGRAM.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_CRIPTOGRAM;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZATION_NOT_FOUND.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZATION_NOT_FOUND;
            }
            if (code.getValue().equals(CodeEnum.PAYMENT_NOT_FOUND_FOR_REVERSAL.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_NOT_FOUND_FOR_REVERSAL;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZED_0.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZED_0;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZED_1.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZED_1;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZED_PARTIAL_AMOUNT_2.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZED_PARTIAL_AMOUNT_2;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZED_3.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZED_3;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZED_5.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZED_5;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZED_PARTIAL_AMOUNT_6.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZED_PARTIAL_AMOUNT_6;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZED_7.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZED_7;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZED_10.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZED_10;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZED_80.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZED_80;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZED_81.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZED_81;
            }
            if (code.getValue().equals(CodeEnum.AUTHORIZATION_DENIED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_AUTHORIZATION_DENIED;
            }
            if (code.getValue().equals(CodeEnum.CARD_EXPIRED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_EXPIRED;
            }
            if (code.getValue().equals(CodeEnum.CONTACT_CALL_CENTER.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CONTACT_CALL_CENTER;
            }
            if (code.getValue().equals(CodeEnum.CARD_WITH_RESTRICTIONS.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_WITH_RESTRICTIONS;
            }
            if (code.getValue().equals(CodeEnum.PIN_ATTEMPTS_EXPIRED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_ATTEMPTS_EXPIRED;
            }
            if (code.getValue().equals(CodeEnum.CONTACT_ISSUER.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CONTACT_ISSUER;
            }
            if (code.getValue().equals(CodeEnum.INVALID_OPERATOR.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_OPERATOR;
            }
            if (code.getValue().equals(CodeEnum.INVALID_AMOUNT.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_AMOUNT;
            }
            if (code.getValue().equals(CodeEnum.INVALID_CARD_NUMBER.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_CARD_NUMBER;
            }
            if (code.getValue().equals(CodeEnum.PIN_REQUIRED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_REQUIRED;
            }
            if (code.getValue().equals(CodeEnum.INVALID_ACCOUNT.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_ACCOUNT;
            }
            if (code.getValue().equals(CodeEnum.UNSUPPORTED_FUNCTION.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_UNSUPPORTED_FUNCTION;
            }
            if (code.getValue().equals(CodeEnum.MAX_LIMIT_REACHED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_MAX_LIMIT_REACHED;
            }
            if (code.getValue().equals(CodeEnum.WRONG_PIN.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_WRONG_PIN;
            }
            if (code.getValue().equals(CodeEnum.CARD_NOT_FOUND.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_NOT_FOUND;
            }
            if (code.getValue().equals(CodeEnum.TRANSACTION_NOT_ENABLED_TO_HOLDER.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_TRANSACTION_NOT_ENABLED_TO_HOLDER;
            }
            if (code.getValue().equals(CodeEnum.TRANSACTION_NOT_ENABLED_TO_POS.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_TRANSACTION_NOT_ENABLED_TO_POS;
            }
            if (code.getValue().equals(CodeEnum.PLAFOND_EXCEEDED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PLAFOND_EXCEEDED;
            }
            if (code.getValue().equals(CodeEnum.LIMITATION_TIME_EXCEEDED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_LIMITATION_TIME_EXCEEDED;
            }
            if (code.getValue().equals(CodeEnum.INACTIVE_CARD.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INACTIVE_CARD;
            }
            if (code.getValue().equals(CodeEnum.INVALID_PIN_BLOCK.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_PIN_BLOCK;
            }
            if (code.getValue().equals(CodeEnum.WRONG_PIN_LENGTH.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_WRONG_PIN_LENGTH;
            }
            if (code.getValue().equals(CodeEnum.PIN_KEY_ERROR.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_KEY_ERROR;
            }
            if (code.getValue().equals(CodeEnum.OUTDOOR_TRANSACTION_FORBIDDEN.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_OUTDOOR_TRANSACTION_FORBIDDEN;
            }
            if (code.getValue().equals(CodeEnum.PUMP_NOT_CONNECTED_TRANSACTION_FORBIDDEN.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PUMP_NOT_CONNECTED_TRANSACTION_FORBIDDEN;
            }
            if (code.getValue().equals(CodeEnum.WRONG_MILEAGE.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_WRONG_MILEAGE;
            }
            if (code.getValue().equals(CodeEnum.PARTNER_NOT_ENABLED_IN_PV.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PARTNER_NOT_ENABLED_IN_PV;
            }
            if (code.getValue().equals(CodeEnum.PV_NOT_ENABLED_FOR_CARD.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PV_NOT_ENABLED_FOR_CARD;
            }
            if (code.getValue().equals(CodeEnum.DAY_NOT_ENABLED_FOR_CARD.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_DAY_NOT_ENABLED_FOR_CARD;
            }
            if (code.getValue().equals(CodeEnum.TIME_NOT_ENABLED_FOR_CARD.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_TIME_NOT_ENABLED_FOR_CARD;
            }
            if (code.getValue().equals(CodeEnum.MANUAL_TRANSACTION_FORBIDDEN.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_MANUAL_TRANSACTION_FORBIDDEN;
            }
            if (code.getValue().equals(CodeEnum.IPERSELF_FORBIDDEN.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_IPERSELF_FORBIDDEN;
            }
            if (code.getValue().equals(CodeEnum.FAIDATE_FORBIDDEN.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_FAIDATE_FORBIDDEN;
            }
            if (code.getValue().equals(CodeEnum.OVERRIDE_FORBIDDEN.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_OVERRIDE_FORBIDDEN;
            }
            if (code.getValue().equals(CodeEnum.BOOKING_REQUIRED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_BOOKING_REQUIRED;
            }
            if (code.getValue().equals(CodeEnum.INVALID_BOOKING_FOR_PRODUCT.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_BOOKING_FOR_PRODUCT;
            }
            if (code.getValue().equals(CodeEnum.INVALID_BOOKING_FOR_PV.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_BOOKING_FOR_PV;
            }
            if (code.getValue().equals(CodeEnum.INVALID_BOOKING_FOR_DAY.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_BOOKING_FOR_DAY;
            }
            if (code.getValue().equals(CodeEnum.DRIVER_BLOCKED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_DRIVER_BLOCKED;
            }
            if (code.getValue().equals(CodeEnum.PRODUCT_NOT_REGISTERED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PRODUCT_NOT_REGISTERED;
            }
            if (code.getValue().equals(CodeEnum.INVALID_PRICE.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_PRICE;
            }
            if (code.getValue().equals(CodeEnum.UNKNOWN_POS.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_UNKNOWN_POS;
            }
            if (code.getValue().equals(CodeEnum.BLOCKED_POS.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_BLOCKED_POS;
            }
            if (code.getValue().equals(CodeEnum.CARD_NOT_ENABLED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_NOT_ENABLED;
            }
            if (code.getValue().equals(CodeEnum.CARD_BLOCKED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_BLOCKED;
            }
            if (code.getValue().equals(CodeEnum.BLOCKED_CLIENT.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_BLOCKED_CLIENT;
            }
            if (code.getValue().equals(CodeEnum.BLOCKED_CARD_ACCOUNT.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_BLOCKED_CARD_ACCOUNT;
            }
            if (code.getValue().equals(CodeEnum.BLOCKED_ISSUER.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_BLOCKED_ISSUER;
            }
            if (code.getValue().equals(CodeEnum.PRODUCT_NOT_ENABLED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PRODUCT_NOT_ENABLED;
            }
            if (code.getValue().equals(CodeEnum.PIN_ATTEMPTS_EXHAUSTED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_ATTEMPTS_EXHAUSTED;
            }
            if (code.getValue().equals(CodeEnum.INVALID_AMOUNT_FOR_COMPANY.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INVALID_AMOUNT_FOR_COMPANY;
            }
            if (code.getValue().equals(CodeEnum.CARD_NOT_ENABLED_0.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_NOT_ENABLED_0;
            }
            if (code.getValue().equals(CodeEnum.CARD_NOT_ENABLED_1.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_NOT_ENABLED_1;
            }
            if (code.getValue().equals(CodeEnum.CARD_NOT_ENABLED_2.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_NOT_ENABLED_2;
            }
            if (code.getValue().equals(CodeEnum.PRODUCT_PLAFOND_EXCEEDED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PRODUCT_PLAFOND_EXCEEDED;
            }
            if (code.getValue().equals(CodeEnum.INSUFFICIENT_CREDIT.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_INSUFFICIENT_CREDIT;
            }
            if (code.getValue().equals(CodeEnum.PLAFOND_EXCEEDED_1.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PLAFOND_EXCEEDED_1;
            }
            if (code.getValue().equals(CodeEnum.EXPIRED_CARD.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_EXPIRED_CARD;
            }
            if (code.getValue().equals(CodeEnum.EXPIRED_CARD_PICKUP.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_EXPIRED_CARD_PICKUP;
            }
            if (code.getValue().equals(CodeEnum.SUSPECTED_FRAUD.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_SUSPECTED_FRAUD;
            }
            if (code.getValue().equals(CodeEnum.CALL_ACQUIRER.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CALL_ACQUIRER;
            }
            if (code.getValue().equals(CodeEnum.CARD_WITH_RESTRICTIONS_0.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_WITH_RESTRICTIONS_0;
            }
            if (code.getValue().equals(CodeEnum.PIN_ATTEMPTS_EXHAUSTED_PICKUP.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_ATTEMPTS_EXHAUSTED_PICKUP;
            }
            if (code.getValue().equals(CodeEnum.LOST_CARD.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_LOST_CARD;
            }
            if (code.getValue().equals(CodeEnum.STOLEN_CARD.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_STOLEN_CARD;
            }
            if (code.getValue().equals(CodeEnum.WRONG_FORMAT.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_WRONG_FORMAT;
            }
            if (code.getValue().equals(CodeEnum.CLOSURE_IN_PROGRESS.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CLOSURE_IN_PROGRESS;
            }
            if (code.getValue().equals(CodeEnum.CENTRAL_SYSTEM_NOT_WORKING.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CENTRAL_SYSTEM_NOT_WORKING;
            }
            if (code.getValue().equals(CodeEnum.SYSTEM_MALFUNCTION.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_SYSTEM_MALFUNCTION;
            }
            if (code.getValue().equals(CodeEnum.TIME_OUT_CARD_EMITTER.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_TIME_OUT_CARD_EMITTER;
            }
            if (code.getValue().equals(CodeEnum.CARD_EMITTER_UNREACHABLE.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_CARD_EMITTER_UNREACHABLE;
            }
            if (code.getValue().equals(CodeEnum.WRONG_MAC.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_WRONG_MAC;
            }
            if (code.getValue().equals(CodeEnum.ERROR_SINC_MAC_KEY.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_SINC_MAC_KEY;
            }
            if (code.getValue().equals(CodeEnum.DECRYPTION_ERROR.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_DECRYPTION_ERROR;
            }
            if (code.getValue().equals(CodeEnum.SECURITY_ERROR.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_SECURITY_ERROR;
            }
            if (code.getValue().equals(CodeEnum.MESSAGE_OUT_OF_SEQUENCE.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_MESSAGE_OUT_OF_SEQUENCE;
            }
            if (code.getValue().equals(CodeEnum.WRONG_OPERATOR.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_WRONG_OPERATOR;
            }
            if (code.getValue().equals(CodeEnum.WRONG_COMPANY.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_WRONG_COMPANY;
            }
            
            /*
            if (code.getValue().equals(CodeEnum.PAYMENT_FAILED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR;
            }
            else if (code.getValue().equals(CodeEnum.INVALID_PARAMETERS.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_WRONG_INPUT_PARAMETERS;
            }
            else if (code.getValue().equals(CodeEnum.INVALID_PARTNER_TYPE .getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_INVALID_PARTNER_TYPE;
            }
            else if (code.getValue().equals(CodeEnum.PAN_NOT_FOUND.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PAN_NOT_FOUND;
            }
            else if (code.getValue().equals(CodeEnum.PAYMENT_WITH_ANOTHER_DEVICE.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_ANOTHER_DEVICE_USED;
            }
            else if (code.getValue().equals(CodeEnum.INVALID_CRIPTOGRAM.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_INVALID_CRYPTOGRAM_PARAMETERS;
            }
            else if (code.getValue().equals(CodeEnum.PIN_NOT_VERIFIED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PIN_NOT_VERIFIED;
            }
            else if (code.getValue().equals(CodeEnum.CARD_EXPIRED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_CARD_EXPIRED;
            }
            else if (code.getValue().equals(CodeEnum.PAYMENT_FAILED_MERCHANT.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PAYMENT_FAILED_MERCHANT;
            }
            else if (code.getValue().equals(CodeEnum.PAYMENT_FAILED_SECURITY.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PAYMENT_FAILED_SECURITY;
            }
            else if (code.getValue().equals(CodeEnum.PAYMENT_FAILED_TERMINAL.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PAYMENT_FAILED_TERMINAL;
            }
            else if (code.getValue().equals(CodeEnum.PAYMENT_FAILED_CUSTOMER.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PAYMENT_FAILED_CUSTOMER;
            }
            else if (code.getValue().equals(CodeEnum.INSUFFICIENT_CREDIT.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_INSUFFICIENT_CREDIT;
            }
            else if (code.getValue().equals(CodeEnum.AUTHCODE_NOT_FOUND.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_AUTHCODE_NOT_FOUND;
            }
            else if (code.getValue().equals(CodeEnum.AMOUNT_GREATER_THAN_AUTHORIZED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_AMOUNT_GREATER_THAN_AUTHORIZED;
            }
            else if (code.getValue().equals(CodeEnum.AMOUNT_GREATER_THAN_SETTLED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_AMOUNT_GREATER_THAN_SETTLED;
            }
            else if (code.getValue().equals(CodeEnum.MAX_LIMIT_REACHED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_MAX_LIMIT_REACHED;
            }
            else if (code.getValue().equals(CodeEnum.REVERSAL_FAILED.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_REVERSAL_FAILED;
            }
            else if (code.getValue().equals(CodeEnum.INVALID_AMOUNT.getValue())) {
                statusCode = ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_INVALID_AMOUNT;
            }
            */
            
            // Errore applicativo nella chiamata al servizio di preautorizzazione con multicard
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "executeAuthorizationResult - code: " + code.getValue() + ", message: " + message);
            
            userTransaction.commit();
            
            createMulticardRefuelResponse.setStatusCode(statusCode);
            return createMulticardRefuelResponse;
        }
        
        System.out.println("RetrievalRefNumber: " + executeAuthorizationResult.getRetrievalRefNumber());
        System.out.println("AuthCode:           " + executeAuthorizationResult.getAuthCode());
        System.out.println("AmountAuthorized:   " + executeAuthorizationResult.getAmountAuthorized());
        
        System.out.println("Preautorizzazione Multicard con esito positivo");

        Integer amountAuthorized = executeAuthorizationResult.getAmountAuthorized();
        
        Double doubleAmountAuthorized = AmountConverter.toInternal(amountAuthorized);
        
        // Memorizzazione lista prodotti abilitati
        List<String> productIDList          = this.getProductIDFromEnabledProductList(executeAuthorizationResult.getFuelEnabledProductList());
        
        String productIDListString = "";
        int productCount = 0;
        for(String productID : productIDList) {
          if (!productIDListString.contains(productID)) {
            if (productCount > 0)
              productIDListString += "|";
            productIDListString += productID;
            productCount++;
          }
        }
        
        System.out.println("productIDListString: " + productIDListString);
        
        
        // Crea la transazione con i dati ricevuti in input
        TransactionBean transactionBean = new TransactionBean();
        transactionBean.setTransactionID(transactionID);
        transactionBean.setUserBean(userBean);
        transactionBean.setStationBean(stationBean);
        transactionBean.setUseVoucher(false);
        transactionBean.setCreationTimestamp(creationTimestamp);
        transactionBean.setEndRefuelTimestamp(null);
        transactionBean.setInitialAmount(doubleAmountAuthorized);
        transactionBean.setFinalAmount(null);
        transactionBean.setFuelAmount(null);
        transactionBean.setFuelQuantity(null);
        transactionBean.setPumpID(pumpID);
        transactionBean.setPumpNumber(pumpNumber);
        transactionBean.setProductID("");
        transactionBean.setProductDescription("");
        transactionBean.setCurrency(currency);
        transactionBean.setShopLogin(shopLogin);
        transactionBean.setAcquirerID(acquirerID);
        transactionBean.setBankTansactionID(executeAuthorizationResult.getRetrievalRefNumber());
        transactionBean.setToken(mcCardDpan);
        transactionBean.setPaymentType(paymentType);
        transactionBean.setFinalStatusType(null);
        transactionBean.setGFGNotification(false);
        transactionBean.setConfirmed(false);
        transactionBean.setPaymentMethodId(paymentInfoBean.getId());
        transactionBean.setPaymentMethodType(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);
        transactionBean.setServerName(serverName);
        transactionBean.setStatusAttemptsLeft(maxStatusAttempts);
        transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
        transactionBean.setOutOfRange(outOfRange);
        transactionBean.setRefuelMode(refuelMode);
        transactionBean.setVoucherReconciliation(false);
        transactionBean.setAuthorizationCode(executeAuthorizationResult.getAuthCode());
        transactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);
        transactionBean.setPaymentTokenPackageBean(paymentTokenPackageBean);
        
        if (!executeAuthorizationResult.getReceiptElementList().isEmpty()) {
            for(KeyValueInfo keyValueInfo : executeAuthorizationResult.getReceiptElementList()) {
                TransactionAdditionalDataBean transactionAdditionalDataBean = new TransactionAdditionalDataBean();
                transactionAdditionalDataBean.setDataKey(keyValueInfo.getKey());
                transactionAdditionalDataBean.setDataValue(keyValueInfo.getValue());
                transactionAdditionalDataBean.setTransactionBean(transactionBean);
                transactionBean.getTransactionAdditionalDataBeanList().add(transactionAdditionalDataBean);
            }
        }
        
        // Crea la riga con la lista dei prodotti abilitati
        TransactionAdditionalDataBean transactionAdditionalDataBean = new TransactionAdditionalDataBean();
        transactionAdditionalDataBean.setDataKey("productIDListString");
        transactionAdditionalDataBean.setDataValue(productIDListString);
        transactionAdditionalDataBean.setTransactionBean(transactionBean);
        transactionBean.getTransactionAdditionalDataBeanList().add(transactionAdditionalDataBean);

        // Crea lo stato iniziale della transazione
        TransactionStatusBean transactionStatusBean = new TransactionStatusBean();
        transactionStatusBean.setTransactionBean(transactionBean);
        transactionStatusBean.setSequenceID(1);
        transactionStatusBean.setTimestamp(creationTimestamp);
        transactionStatusBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
        transactionStatusBean.setSubStatus(null);
        transactionStatusBean.setSubStatusDescription("Started");
        transactionStatusBean.setRequestID(requestID);
        
        // Crea il log dell'operazione di autorizzazione con multicard
        TransactionOperationBean transactionOperationBean = new TransactionOperationBean();
        transactionOperationBean.setCode(executeAuthorizationResult.getResult().getCode().getValue());
        transactionOperationBean.setMessage(executeAuthorizationResult.getResult().getMessage());
        transactionOperationBean.setStatus(executeAuthorizationResult.getResult().getStatus().value());
        transactionOperationBean.setRemoteTransactionId(executeAuthorizationResult.getResult().getTransactionId());
        transactionOperationBean.setOperationType("AUT");
        transactionOperationBean.setSequenceID(1);
        transactionOperationBean.setOperationId(operationID);
        transactionOperationBean.setRequestTimestamp(requestTimestamp);
        transactionOperationBean.setTransactionBean(transactionBean);
        transactionOperationBean.setAmount(executeAuthorizationResult.getAmountAuthorized());
        
        em.persist(paymentTokenPackageBean);
        em.persist(transactionBean);
        em.persist(transactionStatusBean);
        em.persist(transactionOperationBean);

        createMulticardRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_SUCCESS);
        createMulticardRefuelResponse.setTransactionID(transactionID);
        createMulticardRefuelResponse.setShopLogin(shopLogin);
        createMulticardRefuelResponse.setAcquirerID(acquirerID);
        createMulticardRefuelResponse.setCurrency(currency);
        createMulticardRefuelResponse.setPaymentType(paymentType);
        createMulticardRefuelResponse.setAmountAuthorized(doubleAmountAuthorized);
        
        userTransaction.commit();
        
        
        return createMulticardRefuelResponse;
    }
    
    
    private void persistTransactionErrorData(EntityManager em, String requestID, TransactionBean transactionBean, Timestamp creationTimestamp, Double amount, String paymentStatus, String paymentSubStatus, String paymentSubStatusDescription, String errorCode, String errorDescription, String result,
            String operationId, Long requestTimestamp, ExecuteAuthorizationResult executeAuthorizationResult) {
        
        Integer integerAmount = AmountConverter.toMobile(amount);
        
        // Crea lo stato iniziale della transazione
        TransactionStatusBean transactionStatusCreateBean = new TransactionStatusBean();
        transactionStatusCreateBean.setTransactionBean(transactionBean);
        transactionStatusCreateBean.setSequenceID(1);
        transactionStatusCreateBean.setTimestamp(creationTimestamp);
        transactionStatusCreateBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
        transactionStatusCreateBean.setSubStatus(null);
        transactionStatusCreateBean.setSubStatusDescription("Started");
        transactionStatusCreateBean.setRequestID(requestID);
        
        // Crea lo stato relativo al pagamento
        TransactionStatusBean transactionStatusPaymentBean = new TransactionStatusBean();
        transactionStatusPaymentBean.setTransactionBean(transactionBean);
        transactionStatusPaymentBean.setSequenceID(2);
        transactionStatusPaymentBean.setTimestamp(creationTimestamp);
        transactionStatusPaymentBean.setStatus(paymentStatus);
        transactionStatusPaymentBean.setSubStatus(paymentSubStatus);
        transactionStatusPaymentBean.setSubStatusDescription(paymentSubStatusDescription);
        transactionStatusPaymentBean.setRequestID(requestID);
        
        // Crea l'evento di autorizzazione pagamento in errore
        TransactionEventBean transactionEventBean = new TransactionEventBean();
        transactionEventBean.setErrorCode(errorCode);
        transactionEventBean.setErrorDescription(errorDescription);
        transactionEventBean.setEventAmount(amount);
        transactionEventBean.setEventType("AUT");
        transactionEventBean.setSequenceID(1);
        transactionEventBean.setTransactionBean(transactionBean);
        transactionEventBean.setTransactionResult(result);
        
        // Crea il log dell'operazione di autorizzazione con multicard
        TransactionOperationBean transactionOperationBean = new TransactionOperationBean();
        if (executeAuthorizationResult == null) {
            transactionOperationBean.setCode("");
            transactionOperationBean.setMessage("");
            transactionOperationBean.setStatus("");
            transactionOperationBean.setRemoteTransactionId("");
        }
        else {
            transactionOperationBean.setCode(executeAuthorizationResult.getResult().getCode().getValue());
            transactionOperationBean.setMessage(executeAuthorizationResult.getResult().getMessage());
            transactionOperationBean.setStatus(executeAuthorizationResult.getResult().getStatus().value());
            transactionOperationBean.setRemoteTransactionId(executeAuthorizationResult.getResult().getTransactionId());
        }
        transactionOperationBean.setOperationType("AUT");
        transactionOperationBean.setSequenceID(1);
        transactionOperationBean.setOperationId(operationId);
        transactionOperationBean.setRequestTimestamp(requestTimestamp);
        transactionOperationBean.setTransactionBean(transactionBean);
        transactionOperationBean.setAmount(integerAmount);
        
        em.persist(transactionStatusCreateBean);
        em.persist(transactionStatusPaymentBean);
        em.persist(transactionEventBean);
        em.persist(transactionOperationBean);
    }

    
    private List<String> getProductIDFromEnabledProductList(List<ProductDetailInfo> productDetailInfoList) {
        
        // Gestione multiprodotto
        
        List<String> productIDList = new ArrayList<String>(0);

        System.out.println("Rilevata lista prodotti abilitati");
        
        for(ProductDetailInfo productDetailInfo : productDetailInfoList) {
            System.out.println("ProductCode: " + productDetailInfo.getProductCode());
            String productID = convertProductCodeToProductID(productDetailInfo.getProductCode());
            System.out.println("ProductID: " + productID);
            if (productID != null && !productID.equals("")) {
                productIDList.add(productID);
            }
        }
        
        return productIDList;
    }
    
    private String convertProductCodeToProductID(String productCode) {
        // Impostare la trascodifica del codici prodotto
        /*
         * 021 BENZSP -> SP SenzaPb
         * 022 BENZSP -> SP SenzaPb
         * 023 BENZSP -> BS BluSuper
         * 029 BENZSP -> BS BluSuper
         * 030 DIESEL -> GG Gasolio
         * 032 DIESL+ -> BD BluDiesel
         * 033 DIESL+ -> BD BluDiesel
         * 039 METANO -> MT Metano
         * 034 G.P.L. -> GP GPL
         * AD ADBLU 
         */
        
        /*
        021 27101245 BENZSP          L
        022 27101245 BENZSP          L
        023 27101249 SP98            L
        029 27101249 SP98            L
        030 27102011 DIESEL          L
        032 27101943 DIESL+          L
        033 27101943 DIESL+          L
        034 ENI0034 G.P.L.           L
        039 ENI0039 METANO           L
        */

        String productID = "";
        
        switch(productCode) {
            
            case "021":
                productID = "SP";
                break;
            
            case "022":
                productID = "SP";
                break;
            
            case "023":
                productID = "BS";
                break;
            
            case "029":
                productID = "BS";
                break;
            
            case "030":
                productID = "GG";
                break;
            
            case "032":
                productID = "BD";
                break;
            
            case "033":
                productID = "BD";
                break;
            
            case "034":
                productID = "GP";
                break;
            
            case "039":
                productID = "MT";
                break;
                
        }
        
        return productID;
    }
    
}
