package com.techedge.mp.core.actions.transaction;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.utilities.CoordsHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionRetrieveStationIdByCoordsAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public TransactionRetrieveStationIdByCoordsAction() {
    }

    
    public String execute(Double latitude, Double longitude, Double outRangeThreshold) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		// Ricerca l'impianto associato al beaconCode
    		List<StationBean> stationBeanList = QueryRepository.getAllActiveStationBeans(em);
    		
    		StationBean proxStationBean = null;
    		Double proxDistance         = outRangeThreshold;
    		
    		System.out.println("outRangeThreshold: " + outRangeThreshold);
    		
    		for(StationBean stationBean: stationBeanList) {
    			
    			double distance = CoordsHelper.calculateDistance(latitude, longitude, stationBean.getLatitude(), stationBean.getLongitude());
    			
    			System.out.println("station: " + stationBean.getStationID() + " - distance: " + distance);
    			
    			if ( distance < proxDistance ) {
    				
    				proxDistance    = distance;
    				proxStationBean = stationBean;
    			}
    		}
		    
    		if (proxStationBean == null) {

    			// Transaction not found
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Station not found" );
				
    			userTransaction.commit();
    			
    			return null;
    		}
    		else {

    			String stationId = proxStationBean.getStationID();
    			
    			userTransaction.commit();
    			
    			return stationId;
    		}
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED retrieve station id with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
