package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.RefuelingMappingErrorBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionOperationBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionOperationBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.RefuelDetailConverter;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionPersistTransactionOperationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionPersistTransactionOperationAction() {}

    public String execute(String requestID, String transactionID, String code, String message, String status, String remoteTransactionId, String operationType,
            String operationId, Integer amount, Long requestTimestamp) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Ricerca la transazione associata al transactionID tra le prepaid
            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);

            if (transactionBean == null) {

                // Ricerca la transazione associata al transactionID tra le postpaid
                PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, transactionID);

                if (postPaidTransactionBean == null) {

                    // Transaction not found
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionID, null, "Transaction not found");

                    userTransaction.commit();

                    return StatusHelper.PERSIST_TRANSACTION_OPERATION_SUCCESS;
                }
                
                // Crea il nuovo stato
                Integer operationSequenceID = 0;
                for (PostPaidTransactionOperationBean postPaidTransactionOperationBean : postPaidTransactionBean.getPostPaidTransactionOperationBean()) {
                    Integer postPaidTransactionOperationBeanSequenceID = postPaidTransactionOperationBean.getSequenceID();
                    if (operationSequenceID < postPaidTransactionOperationBeanSequenceID) {
                        operationSequenceID = postPaidTransactionOperationBeanSequenceID;
                    }
                }
                operationSequenceID = operationSequenceID + 1;

                PostPaidTransactionOperationBean postPaidTransactionOperationBean = new PostPaidTransactionOperationBean();
                
                postPaidTransactionOperationBean.setCode(code);
                postPaidTransactionOperationBean.setMessage(message);
                postPaidTransactionOperationBean.setStatus(status);
                postPaidTransactionOperationBean.setRemoteTransactionId(remoteTransactionId);
                postPaidTransactionOperationBean.setOperationType(operationType);
                postPaidTransactionOperationBean.setSequenceID(operationSequenceID);
                postPaidTransactionOperationBean.setOperationId(operationId);
                postPaidTransactionOperationBean.setRequestTimestamp(requestTimestamp);
                postPaidTransactionOperationBean.setPostPaidTransactionBean(postPaidTransactionBean);
                postPaidTransactionOperationBean.setAmount(amount);

                em.merge(postPaidTransactionBean);
                em.persist(postPaidTransactionOperationBean);
                
                userTransaction.commit();

                return StatusHelper.PERSIST_PAYMENT_AUTHORIZATION_STATUS_SUCCESS;
            }

            // Crea il nuovo stato
            Integer operationSequenceID = 0;
            for (TransactionOperationBean transactionOperationBean : transactionBean.getTransactionOperationBeanData()) {
                Integer transactionOperationBeanSequenceID = transactionOperationBean.getSequenceID();
                if (operationSequenceID < transactionOperationBeanSequenceID) {
                    operationSequenceID = transactionOperationBeanSequenceID;
                }
            }
            operationSequenceID = operationSequenceID + 1;

            TransactionOperationBean transactionOperationBean = new TransactionOperationBean();
            
            transactionOperationBean.setCode(code);
            transactionOperationBean.setMessage(message);
            transactionOperationBean.setStatus(status);
            transactionOperationBean.setRemoteTransactionId(remoteTransactionId);
            transactionOperationBean.setOperationType(operationType);
            transactionOperationBean.setSequenceID(operationSequenceID);
            transactionOperationBean.setOperationId(operationId);
            transactionOperationBean.setRequestTimestamp(requestTimestamp);
            transactionOperationBean.setTransactionBean(transactionBean);
            transactionOperationBean.setAmount(amount);

            em.merge(transactionBean);
            em.persist(transactionOperationBean);
            
            userTransaction.commit();

            return StatusHelper.PERSIST_PAYMENT_AUTHORIZATION_STATUS_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String logMessage = "FAILED persisting transaction operation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", transactionID, null, logMessage);

            throw new EJBException(ex2);
        }
    }

}
