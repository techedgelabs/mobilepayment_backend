package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.RefuelDetailConverter;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionPersistPaymentDeletionStatusAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionPersistPaymentDeletionStatusAction() {}

    public String execute(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId,
            EmailSenderRemote emailSender, RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, 
            String proxyHost, String proxyPort, String proxyNoHosts, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Ricerca la transazione associata al transactionID
            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);

            if (transactionBean == null) {

                // Transaction not found
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionID, null, "Transaction not found");

                userTransaction.commit();

                return StatusHelper.PERSIST_PAYMENT_DELETION_STATUS_ERROR;
            }

            // Crea il nuovo stato
            Integer sequenceID = 0;
            Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
            for (TransactionStatusBean transactionStatusBean : transactionStatusData) {
                Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                if (sequenceID < transactionStatusBeanSequenceID) {
                    sequenceID = transactionStatusBeanSequenceID;
                }
            }
            sequenceID = sequenceID + 1;

            Date now = new Date();
            Timestamp timestamp = new java.sql.Timestamp(now.getTime());
            String requestID = String.valueOf(now.getTime());

            TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

            transactionStatusBean.setTransactionBean(transactionBean);
            transactionStatusBean.setSequenceID(sequenceID);
            transactionStatusBean.setTimestamp(timestamp);
            transactionStatusBean.setRequestID(requestID);

            Integer eventSequenceID = 0;
            Set<TransactionEventBean> transactionEventData = transactionBean.getTransactionEventBeanData();
            for (TransactionEventBean transactionEventBean : transactionEventData) {
                Integer transactionEventBeanSequenceID = transactionEventBean.getSequenceID();
                if (eventSequenceID < transactionEventBeanSequenceID) {
                    eventSequenceID = transactionEventBeanSequenceID;
                }
            }
            eventSequenceID = eventSequenceID + 1;

            TransactionEventBean transactionEventBean = new TransactionEventBean();
            transactionEventBean.setEventType("CAN");
            transactionEventBean.setSequenceID(eventSequenceID);
            transactionEventBean.setTransactionBean(transactionBean);

            if (statusCode.equals("ok") || statusCode.equals("ok_za")) {
                transactionEventBean.setTransactionResult("OK");
            }
            else {
                transactionEventBean.setTransactionResult("KO");
            }

            transactionEventBean.setErrorCode(errorCode);
            transactionEventBean.setErrorDescription(errorMessage);
            transactionEventBean.setEventAmount(transactionBean.getInitialAmount());

            String transactionResult = "";
            if (statusCode != null) {
                try {
                    transactionResult = statusCode.substring(0, 2);
                }
                catch (IndexOutOfBoundsException ex) {
                    transactionResult = "";
                }
            }
            transactionEventBean.setTransactionResult(transactionResult.toUpperCase());

            String subStatusDescription = "";
            
            Boolean isRefuelingOAuth2 = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.UTENTI_REFUELING_OAUTH2.getCode());

            if (statusCode.toLowerCase().equals("ok")) {

                // Cerca lo stato StatusHelper.STATUS_DELETE_REFUELING_REQUEST
                Boolean undoRequestFound = false;
                for (TransactionStatusBean transactionStatusBeanTemp : transactionBean.getTransactionStatusBeanData()) {
                    if (transactionStatusBeanTemp.getStatus().equals(StatusHelper.STATUS_DELETE_REFUELING_REQUEST)) {
                        undoRequestFound = true;
                    }
                }

                if (undoRequestFound) {
                    subStatusCode = StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_USER_REQUEST;
                    subStatusDescription = StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_USER_REQUEST_DESCRIPTION;
                }
                else {
                    subStatusCode = StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_PUMP_ERROR;
                    subStatusDescription = StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_PUMP_ERROR_DESCRIPTION;
                }

                transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED);
                transactionStatusBean.setSubStatus(subStatusCode);

                // La stringa viene troncata se pi� lunga di 254 caratteri
                if (subStatusDescription.length() > 254)
                    subStatusDescription = subStatusDescription.substring(0, 254);
                transactionStatusBean.setSubStatusDescription(subStatusDescription);
                transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_FAILED);
            }
            else {

                if (statusCode.toLowerCase().equals("ok_za")) {

                    transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED);
                    transactionStatusBean.setSubStatus(subStatusCode);
                    transactionStatusBean.setSubStatusDescription("Payment authorization deleted (TransactionResult=OK) (Amount zero)");
                    transactionBean.setFinalAmount(0.00);
                    transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);

                    //chiamata al servizio di notifica per transazioni refueling

                    String srcTransactionID = transactionBean.getSrcTransactionID();
                    if (srcTransactionID != null && !srcTransactionID.equals("")) {

                        System.out.println("Trovata transazione refueling -> invio notifica");

                        RefuelDetail fuelDeatail = RefuelDetailConverter.createRefuelDetail(transactionBean);

                        String status = transactionBean.getLastTransactionStatus().getStatus();

                        System.out.println("Status: " + status);

                        String mpTransactionStatus = "END_REFUELING";

                        System.out.println("Inizio chiamata servizio di notifica per Rifornimento completato con successo con importo nullo");

                        Runnable r = new TransactionRefuelSendNotification(requestID, srcTransactionID, transactionID, mpTransactionStatus, fuelDeatail,
                                refuelingNotificationService, refuelingNotificationServiceOAuth2, isRefuelingOAuth2, loggerService);

                        new Thread(r).start();
                    }
                }
                else {

                    if (statusCode.toLowerCase().equals("ko")) {

                        // TODO mappare tutti gli stati della risposta GestPay
                        Boolean undoRequestFound = false;
                        for (TransactionStatusBean transactionStatusBeanTemp : transactionBean.getTransactionStatusBeanData()) {
                            if (transactionStatusBeanTemp.getStatus() != null && transactionStatusBeanTemp.getStatus().equals(StatusHelper.STATUS_DELETE_REFUELING_REQUEST)) {
                                undoRequestFound = true;
                            }
                        }

                        if (undoRequestFound) {
                            subStatusCode = StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_NOT_DELETED_USER_REQUEST;
                            subStatusDescription = StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_NOT_DELETED_USER_REQUEST_DESCRIPTION;
                        }
                        else {
                            subStatusCode = StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_NOT_DELETED_REFUSED;
                            subStatusDescription = StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_NOT_DELETED_REFUSED_DESCRIPTION;
                        }

                        transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_NOT_DELETED);
                        transactionStatusBean.setSubStatus(subStatusCode);

                        // La stringa viene troncata se pi� lunga di 254 caratteri
                        if (subStatusDescription.length() > 254)
                            subStatusDescription = subStatusDescription.substring(0, 254);
                        transactionStatusBean.setSubStatusDescription(subStatusDescription);
                        transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL);
                    }
                    else {

                        if (statusCode.toLowerCase().equals("ko_za")) {

                            // TODO mappare tutti gli stati della risposta GestPay
                            transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_TRANSACTION_NOT_DELETED);
                            transactionStatusBean.setSubStatus(subStatusCode);
                            transactionStatusBean.setSubStatusDescription("Fault (application error or system error)");
                            transactionBean.setFinalAmount(0.00);
                            transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_AFTER_REFUEL);
                        }
                    }

                    //chiamata al servizio di notifica per transazioni refueling

                    String srcTransactionID = transactionBean.getSrcTransactionID();
                    if (srcTransactionID != null && !srcTransactionID.equals("")) {

                        System.out.println("Trovata transazione refueling -> invio notifica");

                        RefuelDetail fuelDeatail = RefuelDetailConverter.createRefuelDetail(transactionBean);

                        String status = transactionBean.getLastTransactionStatus().getStatus();

                        System.out.println("Status: " + status);

                        String mpTransactionStatus = "RECONCILIATION";

                        System.out.println("Inizio chiamata servizio di notifica per Errore cancellazione autorizzazione pagamento");

                        Runnable r = new TransactionRefuelSendNotification(requestID, srcTransactionID, transactionID, mpTransactionStatus, fuelDeatail,
                                refuelingNotificationService, refuelingNotificationServiceOAuth2, isRefuelingOAuth2, loggerService);

                        new Thread(r).start();
                    }
                }
            }

            // TODO differenziare il tipo di stato finale in base ai parametri di input
            // transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL);

            UserBean userBean = transactionBean.getUserBean();

            em.merge(transactionBean);

            em.persist(transactionStatusBean);
            em.persist(transactionEventBean);

            Boolean isNewFlow = false;

            isNewFlow = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_PAYMENT_FLOW.getCode());
            
            if ( userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode())) {
                isNewFlow = Boolean.TRUE;
            }
            
            if (isNewFlow) {

                System.out.println("Invio mail ad utente nuovo flusso");
            }
            else {

                System.out.println("Invio mail ad utente vecchio flusso");
            }

            if ((userBean.getUserType() == User.USER_TYPE_CUSTOMER || userBean.getUserType() == User.USER_TYPE_VOUCHER_TESTER || userBean.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER) && emailSender != null) {
                Email.sendPrePaidSummary(emailSender, transactionBean, transactionStatusBean, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
            }

            userTransaction.commit();

            return StatusHelper.PERSIST_PAYMENT_DELETION_STATUS_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED persisting payment deletion status with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", transactionID, null, message);

            throw new EJBException(ex2);
        }
    }

    /*
     * private RefuelDetail createRefuelDetail(TransactionBean transactionBean) {
     * 
     * //RefuelDetail di MPRefuelingAdapterEJBClient
     * RefuelDetail fuelDeatail = new RefuelDetail();
     * 
     * fuelDeatail.setAmount(transactionBean.getFinalAmount());
     * 
     * fuelDeatail.setTimestampStartRefuel(convertDateToString(transactionBean.getCreationTimestamp()));
     * fuelDeatail.setTimestampEndRefuel(convertDateToString(transactionBean.getEndRefuelTimestamp()));
     * 
     * fuelDeatail.setFuelQuantity(transactionBean.getFuelQuantity());
     * fuelDeatail.setProductID(transactionBean.getProductID());
     * fuelDeatail.setProductDescription(transactionBean.getProductDescription());
     * 
     * RefuelingFuelTypeMapping rftm = new RefuelingFuelTypeMapping();
     * fuelDeatail.setFuelType(rftm.getFuelType(transactionBean.getProductID()));
     * return fuelDeatail;
     * }
     * 
     * private String convertDateToString(Date refuelDate) {
     * 
     * if (refuelDate == null) {
     * return "";
     * }
     * 
     * String dateString = null;
     * SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
     * 
     * try {
     * dateString = sdfr.format(refuelDate);
     * }
     * catch (Exception ex) {
     * ex.printStackTrace();
     * }
     * 
     * return dateString;
     * }
     */
}