package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.RefuelDetailConverter;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionPersistPumpEnableStatusAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionPersistPumpEnableStatusAction() {}

    public String execute(String statusCode, String messageCode, String transactionID, String requestID, RefuelingNotificationServiceRemote refuelingNotificationService, 
            RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, UserCategoryService userCategoryService)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Ricerca la transazione associata al transactionID
            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);

            if (transactionBean == null) {

                // Transaction not found
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Transaction not found");

                userTransaction.commit();

                return StatusHelper.PERSIST_PUMP_ENABLE_STATUS_ERROR;
            }

            // Crea il nuovo stato
            Integer sequenceID = 0;
            Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
            for (TransactionStatusBean transactionStatusBean : transactionStatusData) {
                Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                if (sequenceID < transactionStatusBeanSequenceID) {
                    sequenceID = transactionStatusBeanSequenceID;
                }
            }
            sequenceID = sequenceID + 1;

            Date now = new Date();
            Timestamp timestamp = new java.sql.Timestamp(now.getTime());

            TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

            transactionStatusBean.setTransactionBean(transactionBean);
            transactionStatusBean.setSequenceID(sequenceID);
            transactionStatusBean.setTimestamp(timestamp);
            transactionStatusBean.setRequestID(requestID);
            
            Boolean isRefuelingOAuth2 = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.UTENTI_REFUELING_OAUTH2.getCode());
            
            if (statusCode.contains("200")) {

                transactionStatusBean.setStatus(StatusHelper.STATUS_PUMP_ENABLED);
                transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_PUMP_ENABLED);
                transactionStatusBean.setSubStatusDescription("Pump Enabled");
                

                //chiamata al servizio di notifica per transazioni refueling

                String srcTransactionID = transactionBean.getSrcTransactionID();
                if (srcTransactionID != null && !srcTransactionID.equals("")) {

                    System.out.println("Trovata transazione refueling -> invio notifica");

                    RefuelDetail fuelDeatail = RefuelDetailConverter.createRefuelDetail(transactionBean);

                    String status = transactionBean.getLastTransactionStatus().getStatus();

                    System.out.println("Status: " + status);

                    String mpTransactionStatus = "START_REFUELING";

                    System.out.println("Inizio chiamata servizio di notifica per erogatore abilitato");

                    Runnable r = new TransactionRefuelSendNotification(requestID, srcTransactionID, transactionID, mpTransactionStatus, fuelDeatail, refuelingNotificationService,
                            refuelingNotificationServiceOAuth2, isRefuelingOAuth2, loggerService);

                    new Thread(r).start();
                }
            }
            else {

                //chiamata al servizio di notifica per transazioni refueling

                String srcTransactionID = transactionBean.getSrcTransactionID();
                if (srcTransactionID != null && !srcTransactionID.equals("")) {

                    System.out.println("Trovata transazione refueling -> invio notifica");

                    RefuelDetail fuelDeatail = RefuelDetailConverter.createRefuelDetail(transactionBean);

                    String status = transactionBean.getLastTransactionStatus().getStatus();

                    System.out.println("Status: " + status);

                    String mpTransactionStatus = "ERROR";

                    System.out.println("Inizio chiamata servizio di notifica per Errore abilitazione erogatore");

                    Runnable r = new TransactionRefuelSendNotification(requestID, srcTransactionID, transactionID, mpTransactionStatus, fuelDeatail, refuelingNotificationService,
                            refuelingNotificationServiceOAuth2, isRefuelingOAuth2, loggerService);

                    new Thread(r).start();
                }

                transactionStatusBean.setStatus(StatusHelper.STATUS_PUMP_NOT_ENABLED);

                if (statusCode.equals(StatusHelper.SUBSTATUS_PUMP_BUSY)) {

                    // Inserire lo stato PUMP_BUSY solo se non � gi� stato inserito in precedenza

                    Boolean statusPumpBusyFound = false;

                    for (TransactionStatusBean transactionStatusBeanCheck : transactionBean.getTransactionStatusBeanData()) {

                        if (transactionStatusBeanCheck.getStatus().equals(StatusHelper.STATUS_PUMP_NOT_ENABLED)
                                && transactionStatusBeanCheck.getSubStatus().equals(StatusHelper.SUBSTATUS_PUMP_BUSY)) {

                            statusPumpBusyFound = true;
                        }
                    }

                    if (statusPumpBusyFound == false) {

                        transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_PUMP_BUSY);
                        transactionStatusBean.setSubStatusDescription("Pump Busy");

                        em.persist(transactionStatusBean);

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pump busy status inserted");

                        userTransaction.commit();

                        return StatusHelper.PERSIST_PUMP_ENABLE_STATUS_SUCCESS;
                    }
                    else {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pump busy status found");

                        userTransaction.commit();

                        return StatusHelper.PERSIST_PUMP_ENABLE_STATUS_SUCCESS;
                    }
                }

                if (statusCode.equals(StatusHelper.SUBSTATUS_PUMP_NOT_AVAILABLE)) {
                    transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_PUMP_NOT_AVAILABLE);
                    transactionStatusBean.setSubStatusDescription("Pump Not Available");
                }

                if (statusCode.equals(StatusHelper.SUBSTATUS_PUMP_STATUS_NOT_AVAILABLE)) {
                    transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_PUMP_STATUS_NOT_AVAILABLE);
                    transactionStatusBean.setSubStatusDescription("Pump Not Reachable");
                }

                if (statusCode.equals(StatusHelper.SUBSTATUS_PUMP_NOT_FOUND)) {
                    transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_PUMP_NOT_FOUND);
                    transactionStatusBean.setSubStatusDescription("Pump Not Found");
                }

                if (statusCode.equals(StatusHelper.SUBSTATUS_STATION_NOT_FOUND)) {
                    transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_STATION_NOT_FOUND);
                    transactionStatusBean.setSubStatusDescription("Station Not Found");
                }

                if (statusCode.equals(StatusHelper.SUBSTATUS_STATION_PUMP_NOT_FOUND)) {
                    transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_STATION_PUMP_NOT_FOUND);
                    transactionStatusBean.setSubStatusDescription("Station AND Pump Not Found");
                }

                if (statusCode.equals("GENERIC_FAULT_500")) {
                    transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_STATION_PUMP_GENERIC_FAULT);
                    // La stringa viene troncata se pi� lunga di 254 caratteri
                    if (messageCode.length() > 254)
                        messageCode = messageCode.substring(0, 254);
                    transactionStatusBean.setSubStatusDescription(messageCode);
                }
            }

            em.persist(transactionStatusBean);

            userTransaction.commit();

            return StatusHelper.PERSIST_PUMP_ENABLE_STATUS_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED persist pump enable with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    /*
    private RefuelDetail createRefuelDetail(TransactionBean transactionBean) {

        //RefuelDetail di MPRefuelingAdapterEJBClient
        RefuelDetail fuelDeatail = new RefuelDetail();

        fuelDeatail.setAmount(transactionBean.getFinalAmount());
        
        Date startRefuelTimestamp = null;
        Date endRefuelTimestamp   = null;

        fuelDeatail.setTimestampStartRefuel(convertDateToString(startRefuelTimestamp));
        fuelDeatail.setTimestampEndRefuel(convertDateToString(endRefuelTimestamp));

        fuelDeatail.setFuelQuantity(transactionBean.getFuelQuantity());
        fuelDeatail.setProductID(transactionBean.getProductID());
        fuelDeatail.setProductDescription(transactionBean.getProductDescription());

        RefuelingFuelTypeMapping rftm = new RefuelingFuelTypeMapping();
        fuelDeatail.setFuelType(rftm.getFuelType(transactionBean.getProductID()));
        return fuelDeatail;
    }

    private String convertDateToString(Date refuelDate) {

        if (refuelDate == null) {
            return "";
        }

        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        try {
            dateString = sdfr.format(refuelDate);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return dateString;
    }
    */
}