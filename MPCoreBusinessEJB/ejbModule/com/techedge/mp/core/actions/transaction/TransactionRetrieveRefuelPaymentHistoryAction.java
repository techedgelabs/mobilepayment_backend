package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ConstantsHelper;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentRefuelDetailResponse;
import com.techedge.mp.core.business.interfaces.PaymentRefuelHistoryResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.TransactionStatusHistoryBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.utilities.PanHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionRetrieveRefuelPaymentHistoryAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionRetrieveRefuelPaymentHistoryAction() {}

    @SuppressWarnings("unchecked")
    public PaymentRefuelHistoryResponse execute(String requestID, String ticketID, int pageNumber, int itemForPage, Date startDate, Date endDate, Boolean detail)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            PaymentRefuelHistoryResponse paymentRefuelHistoryResponse = new PaymentRefuelHistoryResponse();

            PaymentRefuelDetailResponse paymentRefuelDetailResponse;

            List<PaymentRefuelDetailResponse> paymentRefuelDetailResponseList = new ArrayList<PaymentRefuelDetailResponse>(0);

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                paymentRefuelHistoryResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return paymentRefuelHistoryResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                paymentRefuelHistoryResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return paymentRefuelHistoryResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo
                // servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to retrieve refuel payment history in status "
                        + userStatus);

                userTransaction.commit();

                paymentRefuelHistoryResponse.setStatusCode(ResponseHelper.USER_UPDATE_UNAUTHORIZED);
                return paymentRefuelHistoryResponse;
            }

            // TODO
            int page_number = 1;
            boolean error_flag = true;

            // Estrazione transazioni live o non completamente chiuse (table
            // TRANSACTIONS);
            List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsByUserBeanAndDate(em, userBean, startDate, this.addDays(endDate, 1), itemForPage, detail);

            if (transactionBeanList.isEmpty()) {

            }
            else {

                TransactionBean pendingTransactionBean = null;

                boolean flagFirst = true;

                for (TransactionBean transactionBean : transactionBeanList) {

                    // Si estrae la prima
                    pendingTransactionBean = transactionBean;

                    if (flagFirst == true && pendingTransactionBean == null) {
                        flagFirst = false;
                    }
                    else {
                        error_flag = false;

                        // Si estrae lo stato pi� recente associato alla transazione
                        paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();

                        Integer lastSequenceID = 0;
                        TransactionStatusBean lastTransactionStatusBean = null;
                        Set<TransactionStatusBean> transactionStatusData = pendingTransactionBean.getTransactionStatusBeanData();
                        for (TransactionStatusBean transactionStatusBean : transactionStatusData) {
                            Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                            if (lastSequenceID < transactionStatusBeanSequenceID) {
                                lastSequenceID = transactionStatusBeanSequenceID;
                                lastTransactionStatusBean = transactionStatusBean;
                            }
                        }

                        paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_SUCCESS);
                        paymentRefuelDetailResponse.setRefuelID(pendingTransactionBean.getTransactionID());
                        paymentRefuelDetailResponse.setStatus(lastTransactionStatusBean.getStatus());
                        paymentRefuelDetailResponse.setSubStatus(lastTransactionStatusBean.getSubStatus());
                        paymentRefuelDetailResponse.setUseVoucher(pendingTransactionBean.getUseVoucher());
                        paymentRefuelDetailResponse.setDate(lastTransactionStatusBean.getTimestamp());
                        paymentRefuelDetailResponse.setInitialAmount(pendingTransactionBean.getInitialAmount());
                        paymentRefuelDetailResponse.setFinalAmount(pendingTransactionBean.getFinalAmount());
                        paymentRefuelDetailResponse.setFuelAmount(pendingTransactionBean.getFuelAmount());
                        paymentRefuelDetailResponse.setFuelQuantity(pendingTransactionBean.getFuelQuantity());

                        paymentRefuelDetailResponse.setBankTransactionID(pendingTransactionBean.getBankTansactionID());
                        paymentRefuelDetailResponse.setShopLogin(pendingTransactionBean.getShopLogin());
                        paymentRefuelDetailResponse.setCurrency(pendingTransactionBean.getCurrency());
                        String maskedPan = PanHelper.maskPan(pendingTransactionBean.getToken());
                        paymentRefuelDetailResponse.setMaskedPan(maskedPan);
                        paymentRefuelDetailResponse.setAuthCode(pendingTransactionBean.getAuthorizationCode());

                        paymentRefuelDetailResponse.setSelectedStationID(pendingTransactionBean.getStationBean().getStationID());
                        // TODO - il nome della stazione non viene restituito
                        paymentRefuelDetailResponse.setSelectedStationName("");
                        paymentRefuelDetailResponse.setSelectedStationAddress(pendingTransactionBean.getStationBean().getAddress());
                        paymentRefuelDetailResponse.setSelectedStationCity(pendingTransactionBean.getStationBean().getCity());
                        paymentRefuelDetailResponse.setSelectedStationProvince(pendingTransactionBean.getStationBean().getProvince());
                        paymentRefuelDetailResponse.setSelectedStationCountry(pendingTransactionBean.getStationBean().getCountry());
                        paymentRefuelDetailResponse.setSelectedStationLatitude(pendingTransactionBean.getStationBean().getLatitude());
                        paymentRefuelDetailResponse.setSelectedStationLongitude(pendingTransactionBean.getStationBean().getLongitude());
                        paymentRefuelDetailResponse.setSelectedPumpID(pendingTransactionBean.getPumpID());
                        paymentRefuelDetailResponse.setSelectedPumpNumber(pendingTransactionBean.getPumpNumber());
                        paymentRefuelDetailResponse.setSelectedPumpFuelType(pendingTransactionBean.getProductDescription());

                        paymentRefuelDetailResponse.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_FUEL);
                        paymentRefuelDetailResponse.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_PRE_PAY);

                        paymentRefuelDetailResponseList.add(paymentRefuelDetailResponse);
                    }
                }
            }

            // Estrazione transazioni storicizzate (table TRANSACTIONS_HISTORY);
            List<TransactionHistoryBean> transactionHistoryBeanList = QueryRepository.findTransactionsHistoryByUserBeanAndDate(em, userBean, startDate, this.addDays(endDate, 1), itemForPage, detail);

            if (transactionHistoryBeanList.isEmpty()) {

            }
            else {

                TransactionHistoryBean pendingTransactionHistoryBean = null;

                boolean flagFirst = true;

                for (TransactionHistoryBean transactionHistoryBean : transactionHistoryBeanList) {

                    // Si estrae la prima
                    pendingTransactionHistoryBean = transactionHistoryBean;

                    if (flagFirst == true && pendingTransactionHistoryBean == null) {

                        flagFirst = false;

                    }
                    else {

                        error_flag = false;

                        paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();

                        // Si estrae lo stato pi� recente associato alla transazione

                        Integer lastSequenceID = 0;
                        TransactionStatusHistoryBean lastTransactionStatusBean = null;
                        Set<TransactionStatusHistoryBean> transactionStatusData = pendingTransactionHistoryBean.getTransactionStatusHistoryBeanData();
                        for (TransactionStatusHistoryBean transactionStatusBean : transactionStatusData) {
                            Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                            if (lastSequenceID < transactionStatusBeanSequenceID) {
                                lastSequenceID = transactionStatusBeanSequenceID;
                                lastTransactionStatusBean = transactionStatusBean;
                            }
                        }

                        paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_SUCCESS);
                        paymentRefuelDetailResponse.setRefuelID(pendingTransactionHistoryBean.getTransactionID());
                        paymentRefuelDetailResponse.setStatus(lastTransactionStatusBean.getStatus());
                        paymentRefuelDetailResponse.setSubStatus(lastTransactionStatusBean.getSubStatus());
                        paymentRefuelDetailResponse.setUseVoucher(pendingTransactionHistoryBean.getUseVoucher());
                        paymentRefuelDetailResponse.setDate(lastTransactionStatusBean.getTimestamp());
                        paymentRefuelDetailResponse.setInitialAmount(pendingTransactionHistoryBean.getInitialAmount());
                        paymentRefuelDetailResponse.setFinalAmount(pendingTransactionHistoryBean.getFinalAmount());
                        paymentRefuelDetailResponse.setFuelAmount(pendingTransactionHistoryBean.getFuelAmount());
                        paymentRefuelDetailResponse.setFuelQuantity(pendingTransactionHistoryBean.getFuelQuantity());

                        paymentRefuelDetailResponse.setBankTransactionID(pendingTransactionHistoryBean.getBankTansactionID());
                        paymentRefuelDetailResponse.setShopLogin(pendingTransactionHistoryBean.getShopLogin());
                        paymentRefuelDetailResponse.setCurrency(pendingTransactionHistoryBean.getCurrency());
                        String maskedPan = PanHelper.maskPan(pendingTransactionHistoryBean.getToken());
                        paymentRefuelDetailResponse.setMaskedPan(maskedPan);
                        paymentRefuelDetailResponse.setAuthCode(pendingTransactionHistoryBean.getAuthorizationCode());

                        paymentRefuelDetailResponse.setSelectedStationID(pendingTransactionHistoryBean.getStationBean().getStationID());
                        // TODO - il nome della stazione non viene restituito
                        paymentRefuelDetailResponse.setSelectedStationName("");
                        paymentRefuelDetailResponse.setSelectedStationAddress(pendingTransactionHistoryBean.getStationBean().getAddress());
                        paymentRefuelDetailResponse.setSelectedStationLatitude(pendingTransactionHistoryBean.getStationBean().getLatitude());
                        paymentRefuelDetailResponse.setSelectedStationLongitude(pendingTransactionHistoryBean.getStationBean().getLongitude());
                        paymentRefuelDetailResponse.setSelectedPumpID(pendingTransactionHistoryBean.getPumpID());
                        paymentRefuelDetailResponse.setSelectedPumpNumber(pendingTransactionHistoryBean.getPumpNumber());
                        paymentRefuelDetailResponse.setSelectedPumpFuelType(pendingTransactionHistoryBean.getProductDescription());

                        paymentRefuelDetailResponse.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_FUEL);
                        paymentRefuelDetailResponse.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_PRE_PAY);

                        paymentRefuelDetailResponseList.add(paymentRefuelDetailResponse);

                    }

                }
            }

            // Estrazione transazioni post-paid (table POSTPAIDTRANSACTIONS);
            List<PostPaidTransactionBean> poPTransactionBeanList = QueryRepository.findPostPaidTransactionsByUserBeanAndDate(em, userBean, startDate, this.addDays(endDate, 1), itemForPage, detail);

            if (poPTransactionBeanList.isEmpty()) {

            }
            else {

                PostPaidTransactionBean pendingPostPaidTransactionBean = null;

                boolean flagFirst = true;

                for (PostPaidTransactionBean postPaidTransactionBean : poPTransactionBeanList) {

                    // Si estrae la prima
                    pendingPostPaidTransactionBean = postPaidTransactionBean;

                    if (flagFirst == true && pendingPostPaidTransactionBean == null) {
                        flagFirst = false;
                    }
                    else {
                        error_flag = false;

                        // Si estrae lo stato pi� recente associato alla transazione
                        paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();

                        paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_SUCCESS);
                        paymentRefuelDetailResponse.setRefuelID(postPaidTransactionBean.getMpTransactionID());
                        paymentRefuelDetailResponse.setStatus(postPaidTransactionBean.getMpTransactionStatus());
                        // paymentRefuelDetailResponse.setSubStatus(postPaidTransactionBean.getSubStatus());

                        // ATTENZIONE DA IMPLEMENTARE!!!
                        paymentRefuelDetailResponse.setUseVoucher(false);

                        paymentRefuelDetailResponse.setDate(Timestamp.valueOf(postPaidTransactionBean.getCreationTimestamp().toString()));

                        paymentRefuelDetailResponse.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_POST_PAY);

                        if (postPaidTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {
                            paymentRefuelDetailResponse.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_FUEL);
                        }

                        if (postPaidTransactionBean.getSource().equals("CASH REGISTER")) {
                            paymentRefuelDetailResponse.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_SHOP);
                        }

                        /*
                         * if (!postPaidTransactionBean.getRefuelBean().isEmpty()) {
                         * paymentRefuelDetailResponse
                         * .setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_FUEL);
                         * PostPaidRefuelBean refuelBean =
                         * postPaidTransactionBean.getRefuelBean().iterator().next();
                         * paymentRefuelDetailResponse
                         * .setSelectedPumpID(refuelBean.getPumpId());
                         * paymentRefuelDetailResponse
                         * .setSelectedPumpNumber(refuelBean.getPumpNumber());
                         * paymentRefuelDetailResponse
                         * .setSelectedPumpFuelType(refuelBean.getProductDescription());
                         * paymentRefuelDetailResponse
                         * .setFuelAmount(refuelBean.getFuelAmount());
                         * paymentRefuelDetailResponse
                         * .setFuelQuantity(refuelBean.getFuelQuantity()); }
                         * 
                         * if (!postPaidTransactionBean.getCartBean().isEmpty()) {
                         * paymentRefuelDetailResponse
                         * .setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_SHOP);
                         * PostPaidCartBean cartBean =
                         * postPaidTransactionBean.getCartBean().iterator().next(); }
                         */

                        paymentRefuelDetailResponse.setInitialAmount(postPaidTransactionBean.getAmount());
                        paymentRefuelDetailResponse.setFinalAmount(postPaidTransactionBean.getAmount());

                        paymentRefuelDetailResponse.setBankTransactionID(postPaidTransactionBean.getBankTansactionID());
                        paymentRefuelDetailResponse.setShopLogin(postPaidTransactionBean.getShopLogin());
                        paymentRefuelDetailResponse.setCurrency(postPaidTransactionBean.getCurrency());
                        String maskedPan = PanHelper.maskPan(postPaidTransactionBean.getToken());
                        paymentRefuelDetailResponse.setMaskedPan(maskedPan);
                        paymentRefuelDetailResponse.setAuthCode(postPaidTransactionBean.getAuthorizationCode());

                        paymentRefuelDetailResponse.setSelectedStationID(postPaidTransactionBean.getStationBean().getStationID());
                        // TODO - il nome della stazione non viene restituito
                        paymentRefuelDetailResponse.setSelectedStationName("");
                        paymentRefuelDetailResponse.setSelectedStationAddress(postPaidTransactionBean.getStationBean().getAddress());
                        paymentRefuelDetailResponse.setSelectedStationCity(postPaidTransactionBean.getStationBean().getCity());
                        paymentRefuelDetailResponse.setSelectedStationProvince(postPaidTransactionBean.getStationBean().getProvince());
                        paymentRefuelDetailResponse.setSelectedStationCountry(postPaidTransactionBean.getStationBean().getCountry());
                        paymentRefuelDetailResponse.setSelectedStationLatitude(postPaidTransactionBean.getStationBean().getLatitude());
                        paymentRefuelDetailResponse.setSelectedStationLongitude(postPaidTransactionBean.getStationBean().getLongitude());

                        paymentRefuelDetailResponseList.add(paymentRefuelDetailResponse);
                    }
                }
            }

            // Estrazione transazioni storicizzate post-paid (table
            // POSTPAIDTRANSACTIONS_HISTORY);
            List<PostPaidTransactionHistoryBean> poPTransactionHistoryBeanList = QueryRepository.findPostPaidTransactionsHistoryByUserBeanAndDate(em, userBean, startDate, this.addDays(endDate, 1), itemForPage, detail);

            if (poPTransactionHistoryBeanList.isEmpty()) {

            }
            else {

                PostPaidTransactionHistoryBean pendingPostPaidTransactionHistoryBean = null;

                boolean flagFirst = true;

                for (PostPaidTransactionHistoryBean postPaidTransactionHistoryBean : poPTransactionHistoryBeanList) {

                    // Si estrae la prima
                    pendingPostPaidTransactionHistoryBean = postPaidTransactionHistoryBean;

                    if (flagFirst == true && pendingPostPaidTransactionHistoryBean == null) {
                        flagFirst = false;
                    }
                    else {
                        error_flag = false;

                        // Si estrae lo stato pi� recente associato alla transazione
                        paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();

                        paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_SUCCESS);
                        paymentRefuelDetailResponse.setRefuelID(postPaidTransactionHistoryBean.getMpTransactionID());
                        paymentRefuelDetailResponse.setStatus(postPaidTransactionHistoryBean.getMpTransactionStatus());
                        // paymentRefuelDetailResponse.setSubStatus(postPaidTransactionBean.getSubStatus());

                        // ATTENZIONE DA IMPLEMENTARE!!!
                        paymentRefuelDetailResponse.setUseVoucher(false);

                        paymentRefuelDetailResponse.setDate(Timestamp.valueOf(postPaidTransactionHistoryBean.getCreationTimestamp().toString()));

                        paymentRefuelDetailResponse.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_POST_PAY);

                        if (postPaidTransactionHistoryBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {
                            paymentRefuelDetailResponse.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_FUEL);
                        }

                        if (postPaidTransactionHistoryBean.getSource().equals("CASH REGISTER")) {
                            paymentRefuelDetailResponse.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_SHOP);
                        }

                        /*
                         * if (!postPaidTransactionHistoryBean.getRefuelBean().isEmpty()) {
                         * paymentRefuelDetailResponse
                         * .setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_FUEL);
                         * PostPaidRefuelBean refuelBean =
                         * postPaidTransactionHistoryBean.getRefuelBean().iterator().next();
                         * paymentRefuelDetailResponse
                         * .setSelectedPumpID(refuelBean.getPumpId());
                         * paymentRefuelDetailResponse
                         * .setSelectedPumpNumber(refuelBean.getPumpNumber());
                         * paymentRefuelDetailResponse
                         * .setSelectedPumpFuelType(refuelBean.getProductDescription());
                         * paymentRefuelDetailResponse
                         * .setFuelAmount(refuelBean.getFuelAmount());
                         * paymentRefuelDetailResponse
                         * .setFuelQuantity(refuelBean.getFuelQuantity()); }
                         * 
                         * if (!postPaidTransactionHistoryBean.getCartBean().isEmpty()) {
                         * paymentRefuelDetailResponse
                         * .setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_SHOP);
                         * PostPaidCartBean cartBean =
                         * postPaidTransactionHistoryBean.getCartBean().iterator().next(); }
                         */

                        paymentRefuelDetailResponse.setInitialAmount(postPaidTransactionHistoryBean.getAmount());
                        paymentRefuelDetailResponse.setFinalAmount(postPaidTransactionHistoryBean.getAmount());

                        paymentRefuelDetailResponse.setBankTransactionID(postPaidTransactionHistoryBean.getBankTansactionID());
                        paymentRefuelDetailResponse.setShopLogin(postPaidTransactionHistoryBean.getShopLogin());
                        paymentRefuelDetailResponse.setCurrency(postPaidTransactionHistoryBean.getCurrency());
                        String maskedPan = PanHelper.maskPan(postPaidTransactionHistoryBean.getToken());
                        paymentRefuelDetailResponse.setMaskedPan(maskedPan);
                        paymentRefuelDetailResponse.setAuthCode(postPaidTransactionHistoryBean.getAuthorizationCode());

                        paymentRefuelDetailResponse.setSelectedStationID(postPaidTransactionHistoryBean.getStationBean().getStationID());
                        // TODO - il nome della stazione non viene restituito
                        paymentRefuelDetailResponse.setSelectedStationName("");
                        paymentRefuelDetailResponse.setSelectedStationAddress(postPaidTransactionHistoryBean.getStationBean().getAddress());
                        paymentRefuelDetailResponse.setSelectedStationCity(postPaidTransactionHistoryBean.getStationBean().getCity());
                        paymentRefuelDetailResponse.setSelectedStationProvince(postPaidTransactionHistoryBean.getStationBean().getProvince());
                        paymentRefuelDetailResponse.setSelectedStationCountry(postPaidTransactionHistoryBean.getStationBean().getCountry());
                        paymentRefuelDetailResponse.setSelectedStationLatitude(postPaidTransactionHistoryBean.getStationBean().getLatitude());
                        paymentRefuelDetailResponse.setSelectedStationLongitude(postPaidTransactionHistoryBean.getStationBean().getLongitude());

                        paymentRefuelDetailResponseList.add(paymentRefuelDetailResponse);
                    }
                }
            }

            if (error_flag == true) {

                paymentRefuelHistoryResponse.setPageTotal(page_number);
                paymentRefuelHistoryResponse.setPageOffset(pageNumber);
                paymentRefuelHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_HISTORY_SUCCESS);

                userTransaction.commit();

                return paymentRefuelHistoryResponse;
            }
            else {
                Collections.sort(paymentRefuelDetailResponseList, new Comparator() {
                    public int compare(Object synchronizedListOne, Object synchronizedListTwo) {
                        // use instanceof to verify the references are indeed of the type in
                        // question
                        return ((PaymentRefuelDetailResponse) synchronizedListOne).getDate().compareTo(((PaymentRefuelDetailResponse) synchronizedListTwo).getDate());
                    }
                });
                Collections.reverse(paymentRefuelDetailResponseList);
                int size_list = paymentRefuelDetailResponseList.size();

                if (size_list > itemForPage) {

                    page_number = size_list / itemForPage;

                    if (size_list % itemForPage > 0) {
                        page_number = page_number + 1;
                    }
                }

                int from_subset;
                int to_subset;

                if (pageNumber >= 0 && itemForPage > 0) {

                    from_subset = (pageNumber) * itemForPage;
                    to_subset = from_subset + itemForPage;

                    if (to_subset < size_list) {

                        paymentRefuelHistoryResponse.setPaymentRefuelDetailHistory(new ArrayList<PaymentRefuelDetailResponse>(paymentRefuelDetailResponseList.subList(from_subset, to_subset)));
                        paymentRefuelHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_HISTORY_SUCCESS);

                    }
                    else {
                        if (from_subset < size_list) {

                            paymentRefuelHistoryResponse.setPaymentRefuelDetailHistory(new ArrayList<PaymentRefuelDetailResponse>(paymentRefuelDetailResponseList.subList(from_subset, size_list)));
                            paymentRefuelHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_HISTORY_SUCCESS);
                        }
                        else {

                            paymentRefuelHistoryResponse.setPaymentRefuelDetailHistory(paymentRefuelDetailResponseList);
                            paymentRefuelHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_HISTORY_SUCCESS);
                            pageNumber = 0;
                            page_number = 1;
                        }
                    }
                }
                else {
                    page_number = 0;
                    pageNumber = size_list;
                    paymentRefuelHistoryResponse.setPaymentRefuelDetailHistory(paymentRefuelDetailResponseList);
                    paymentRefuelHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_HISTORY_SUCCESS);
                }

                paymentRefuelHistoryResponse.setPageTotal(page_number);
                paymentRefuelHistoryResponse.setPageOffset(pageNumber);

                userTransaction.commit();

                return paymentRefuelHistoryResponse;
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieving refuel payment history with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    private Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); // minus number would decrement the days
        return cal.getTime();
    }
}