package com.techedge.mp.core.actions.transaction;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionCheckUndoOperationAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public TransactionCheckUndoOperationAction() {
    }

    
    public Boolean execute(String transactionID) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		// Ricerca la transazione associata al transactionID
    		TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);
		    
    		if (transactionBean == null) {

    			// Transaction not found
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionID, null, "Transaction not found" );
				
    			userTransaction.commit();
    			
    			return StatusHelper.CHECK_UNDO_OPERATION_NOT_FOUND;
    		}
    		
    		Boolean undoRequestFound = false;
    		for ( TransactionStatusBean transactionStatusBean : transactionBean.getTransactionStatusBeanData() ) {
    			if ( transactionStatusBean.getStatus().equals(StatusHelper.STATUS_DELETE_REFUELING_REQUEST) ) {
    				undoRequestFound = true;
    			}
    		}

    		if (undoRequestFound == true) {

    			userTransaction.commit();
    			
    			return StatusHelper.CHECK_UNDO_OPERATION_FOUND;
    		}
    		else {

    			userTransaction.commit();
    			
    			return StatusHelper.CHECK_UNDO_OPERATION_NOT_FOUND;

    		}
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED check undo operation status with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", transactionID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
