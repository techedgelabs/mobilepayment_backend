package com.techedge.mp.core.actions.transaction.v2;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.TransactionService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.CreateRefuelResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.TransactionPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.utilities.AmountConverter;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionV2CreateRefuelAction {

    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;

    @EJB
    private LoggerService   loggerService;

    private PaymentInfoBean paymentInfoBean = null;

    public TransactionV2CreateRefuelAction() {}

    public CreateRefuelResponse execute(String ticketID, String requestID, String encodedPin, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, String paymentType, Long paymentMethodId, String paymentMethodType, String outOfRange, String refuelMode,
            String defaultShopLogin, String defaultAcquirerID, Integer pinCheckMaxAttempts, Integer maxStatusAttempts, Integer reconciliationMaxAttempts, Integer transactionAmountFull,
            String serverName, List<String> userBlockExceptionList, EmailSenderRemote emailSender, UserCategoryService userCategoryService, TransactionService transactionService, 
            GPServiceRemote gpService, FidelityServiceRemote fidelityService, UnavailabilityPeriodService unavailabilityPeriodService,
            String proxyHost, String proxyPort, String proxyNoHosts, String currency, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ServerName: " + serverName);

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return createRefuelResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return createRefuelResponse;
            }

            if (userBlockExceptionList.contains(userBean.getPersonalDataBean().getSecurityDataEmail())) {

                System.out.println(userBean.getPersonalDataBean().getSecurityDataEmail() + " presente in lista utenti con eccezioni blocco");
            }
            else {
                // Controllo su disponibilit� del servizio di creazione transazioni prepaid
                ServiceAvailabilityData serviceAvailabilityData = unavailabilityPeriodService.retrieveServiceAvailability("CREATE_TRANSACTION_PREPAID", new Date());
                if (serviceAvailabilityData != null) {

                    // Servizio non disponibile
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "CreateRefuelService unavailable");

                    userTransaction.commit();

                    createRefuelResponse.setStatusCode(serviceAvailabilityData.getStatusCode());
                    return createRefuelResponse;
                }
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus);

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_UNAUTHORIZED);
                return createRefuelResponse;
            }

            // Controlla se l'utente ha gi� una transazione attiva
            List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);

            if (!transactionBeanList.isEmpty()) {

                // Esiste una transazione associata all'utente non ancora completata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction found");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
                return createRefuelResponse;
            }
            
            // Controllo su abilitazione PV
            StationBean stationBean = QueryRepository.findActiveStationBeanById(em, stationID);
            
            if (stationBean == null || !stationBean.getNewAcquirerActive()) {
                
                // Il PV non risulta abilitato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "PV not enabled");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PV_NOT_ENABLED);
                return createRefuelResponse;
            }
            
            // Controllo sull'importo richiesto
            Double transactionAmountFullDouble = AmountConverter.toInternal(transactionAmountFull);
            
            if (amount > transactionAmountFullDouble) {
                
                // L'importo richiesto � maggiore di quello consentito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid amount");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_CREDIT_CARD_INVALID_AMOUNT);
                return createRefuelResponse;
            }

            createRefuelResponse = checkPaymentMethod(userBean, paymentMethodId, paymentMethodType, encodedPin, pinCheckMaxAttempts, requestID, userTransaction);

            if (createRefuelResponse.getStatusCode() != null) {
                return createRefuelResponse;
            }

            Integer userType = userBean.getUserType();
            Boolean useNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            Boolean useGuestFlow       = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.GUEST_FLOW.getCode());

            if (useNewAcquirerFlow || useGuestFlow) {

                if (paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Create pre-paid voucher transaction");

                    createRefuelResponse = this.createRefuelTransactionCreditCard(requestID, userTransaction, userBean, amount, encodedPin, stationID, pumpID, pinCheckMaxAttempts,
                            defaultShopLogin, defaultAcquirerID, pumpNumber, productID, productDescription, paymentType, serverName, maxStatusAttempts, reconciliationMaxAttempts,
                            outOfRange, refuelMode, userCategoryService, emailSender, stringSubstitution);
                }
                else if (paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {
                    createRefuelResponse = this.createRefuelTransactionVoucher(requestID, userTransaction, userBean, amount, encodedPin, stationID, pumpID, pinCheckMaxAttempts,
                            defaultShopLogin, defaultAcquirerID, pumpNumber, productID, productDescription, currency, paymentType, serverName, maxStatusAttempts,
                            reconciliationMaxAttempts, outOfRange, refuelMode, userCategoryService, emailSender, transactionService, gpService, fidelityService, proxyHost,
                            proxyPort, proxyNoHosts);
                }
                else {
                    // Il metodo di pagamento selezionato non esiste
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodType not valid: " + paymentMethodType);

                    userTransaction.commit();

                    createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
                    return createRefuelResponse;
                }
            }
            else {

                // Utente non abilitato all'operazione
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "UserType " + userType
                        + " not enabled to execute this operation (V2/createRefuelTransaction)");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
                return createRefuelResponse;
            }

            return createRefuelResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED transaction creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    private CreateRefuelResponse checkPaymentMethod(UserBean userBean, Long paymentMethodId, String paymentMethodType, String encodedPin, Integer pinCheckMaxAttempts,
            String requestID, UserTransaction userTransaction) throws Exception {

        // Verifica che l'utente abbia gi� associato il pin
        PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();
        CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();

        if (paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("")) {

            // Errore Pin non ancora inserito
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");

            userTransaction.commit();
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }

        // Verifica il metodo utilizzato per il pagamento
        System.out.println("Verifica metodo di pagamento");

        // Se il medodo di pagamento non � specificato utilizza quello di default
        if (paymentMethodId == null && paymentMethodType == null) {

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found");

            paymentInfoBean = userBean.findDefaultPaymentInfoBean();
        }
        else {

            paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
        }

        // Verifica che il metodo di pagamento selezionato sia in uno stato valido
        if (paymentInfoBean == null
                || (!paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) && !paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER))) {

            // Il metodo di pagamento selezionato non esiste
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
            return createRefuelResponse;
        }
        
        // Se l'utente ha scelto di pagare con voucher ma ha una carta di credito valida associata, bisogna restituire un messaggio di errore
        if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {
            
            System.out.println("L'utente ha scelto di pagare con voucher. Controllo esistenza carta di credito valida.");
            
            if (userBean.getPaymentData() != null && !userBean.getPaymentData().isEmpty()) {
                
                for(PaymentInfoBean paymentInfoBeanCheck : userBean.getPaymentData()) {
                    
                    if (paymentInfoBeanCheck.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) &&
                            (paymentInfoBeanCheck.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED ||
                             paymentInfoBeanCheck.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {
                        
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Valid credit card found -> unable to use voucher flow");

                        userTransaction.commit();

                        createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_USE_CREDIT_CARD);
                        return createRefuelResponse;
                    }
                }
            }
        }

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                + paymentMethodType);

        if (paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

            // Il metodo di pagamento selezionato non pu� essere utilizzato
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
            return createRefuelResponse;
        }

        // Verifica che il pin indicato per il rifornimento coincida con quello inserito in iscrizione
        if (!encodedPin.equals(paymentInfoVoucherBean.getPin())) {

            // Il pin inserito non � valido
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");
            
            String response = ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_PIN;

            // Si sottrae uno al numero di tentativi residui
            Integer pinCheckAttemptsLeft = paymentInfoVoucherBean.getPinCheckAttemptsLeft();
            if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                pinCheckAttemptsLeft--;
            }
            else {
                pinCheckAttemptsLeft = 0;
            }

            if (pinCheckAttemptsLeft == 0) {

                // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                paymentInfoVoucherBean.setDefaultMethod(false);
                paymentInfoVoucherBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                // e tutti i metodi di pagamento verificati e non verificati sono cancellati
                for (PaymentInfoBean tmpPaymentInfoBean : userBean.getPaymentData()) {

                    if (tmpPaymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                            && (tmpPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || tmpPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                        tmpPaymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                        tmpPaymentInfoBean.setDefaultMethod(false);
                        em.merge(tmpPaymentInfoBean);
                    }
                }
            }
            else {
                
                if ( pinCheckAttemptsLeft == 1 ) {
                    
                    response = ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_PIN_LAST_ATTEMPT;
                }
                else {
                    
                    response = ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_PIN_ATTEMPTS_LEFT;
                }
            }

            paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

            em.merge(paymentInfoVoucherBean);

            userTransaction.commit();

            createRefuelResponse.setPinCheckMaxAttempts(paymentInfoVoucherBean.getPinCheckAttemptsLeft());
            createRefuelResponse.setStatusCode(response);
            return createRefuelResponse;
        }
        
        // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
        paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);

        em.merge(paymentInfoVoucherBean);

        return createRefuelResponse;
    }

    private CreateRefuelResponse createRefuelTransactionCreditCard(String requestID, UserTransaction userTransaction, UserBean userBean, Double amount, String encodedPin,
            String stationID, String pumpID, Integer pinCheckMaxAttempts, String defaultShopLogin, String defaultAcquirerID, Integer pumpNumber, String productID,
            String productDescription, String paymentType, String serverName, Integer maxStatusAttempts, Integer reconciliationMaxAttempts, String outOfRange, String refuelMode,
            UserCategoryService userCategoryService, EmailSenderRemote emailSender, StringSubstitution stringSubstitution) throws Exception {

        System.out.println("Utilizzo flusso nuovo acquirer");

        CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();
        /*
        PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();

        if (paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("")) {

            // Errore Pin non ancora inserito
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");

            userTransaction.commit();
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
            return createRefuelResponse;
        }

        // Se il medodo di pagamento non � specificato utilizza quello di default
        PaymentInfoBean paymentInfoBean = null;

        if (paymentMethodId == null && paymentMethodType == null) {

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found");

            paymentInfoBean = userBean.findDefaultPaymentInfoBean();
        }
        else {

            paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
        }

        // Verifica che il metodo di pagamento selezionato sia in uno stato valido
        if (paymentInfoBean == null || !paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {

            // Il metodo di pagamento selezionato non esiste
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
            return createRefuelResponse;
        }

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                + paymentMethodType);

        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_BLOCKED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_ERROR
                || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING) {

            // Il metodo di pagamento selezionato non pu� essere utilizzato
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
            return createRefuelResponse;
        }

        double capAvailable = 0.0;

        if (userBean.getCapAvailable() != null) {
            capAvailable = userBean.getCapAvailable().doubleValue();
        }

        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {

            // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Payment not verified and transaction amount exceeding the cap");

            Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean);

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_CAP_FAILURE);
            return createRefuelResponse;
        }
        
        // Verifica che il pin indicato per il rifornimento coincida con quello inserito in iscrizione
        if (!encodedPin.equals(paymentInfoVoucherBean.getPin())) {

            // Il pin inserito non � valido
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");

            // Si sottrae uno al numero di tentativi residui
            Integer pinCheckAttemptsLeft = paymentInfoVoucherBean.getPinCheckAttemptsLeft();
            if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                pinCheckAttemptsLeft--;
            }
            else {
                pinCheckAttemptsLeft = 0;
            }

            if (pinCheckAttemptsLeft == 0) {

                // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                paymentInfoVoucherBean.setDefaultMethod(false);
                paymentInfoVoucherBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                // e tutti i metodi di pagamento verificati e non verificati sono cancellati
                for (PaymentInfoBean tmpPaymentInfoBean : userBean.getPaymentData()) {

                    if (tmpPaymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                            && (tmpPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || tmpPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                        tmpPaymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                        tmpPaymentInfoBean.setDefaultMethod(false);
                        em.merge(tmpPaymentInfoBean);
                    }
                }
            }

            paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

            em.merge(paymentInfoVoucherBean);

            userTransaction.commit();

            createRefuelResponse.setPinCheckMaxAttempts(paymentInfoVoucherBean.getPinCheckAttemptsLeft());
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_WRONG);
            return createRefuelResponse;
        }

        // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
        paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);

        em.merge(paymentInfoVoucherBean);
        
        */

        double capAvailable = 0.0;

        if (userBean.getCapAvailable() != null) {
            capAvailable = userBean.getCapAvailable().doubleValue();
        }

        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {

            // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Payment not verified and transaction amount exceeding the cap");

            Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean, userCategoryService, stringSubstitution);

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_CAP_FAILURE);
            return createRefuelResponse;
        }

        StationBean stationBean = QueryRepository.findStationBeanById(em, stationID);

        if (stationBean == null) {

            // Lo stationID inserito non corrisponde a nessuna stazione valida
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID);

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }

        if (stationBean.getDataAcquirer() == null) {

            // DataAcquired non trovato per il PV
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No DataAcquirer for stationID " + stationID);

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }

        String acquirerID = stationBean.getDataAcquirer().getAcquirerID();
        String apiKey = stationBean.getDataAcquirer().getApiKey();
        String currency = stationBean.getDataAcquirer().getCurrency();
        String encodedSecretKey = stationBean.getDataAcquirer().getEncodedSecretKey();
        String groupAcquirer = stationBean.getDataAcquirer().getGroupAcquirer();

        String transactionID = new IdGenerator().generateId(16).substring(0, 32);

        Date now = new Date();
        Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

        // Crea la transazione con i dati ricevuti in input
        TransactionBean transactionBean = new TransactionBean();
        transactionBean.setTransactionID(transactionID);
        transactionBean.setUserBean(userBean);
        transactionBean.setStationBean(stationBean);
        transactionBean.setUseVoucher(Boolean.TRUE);
        transactionBean.setCreationTimestamp(creationTimestamp);
        transactionBean.setEndRefuelTimestamp(null);
        transactionBean.setInitialAmount(amount);
        transactionBean.setFinalAmount(null);
        transactionBean.setFuelAmount(null);
        transactionBean.setFuelQuantity(null);
        transactionBean.setPumpID(pumpID);
        transactionBean.setPumpNumber(pumpNumber);
        transactionBean.setProductID(productID);
        transactionBean.setProductDescription(productDescription);
        transactionBean.setCurrency(currency);
        transactionBean.setShopLogin(apiKey);
        transactionBean.setAcquirerID(acquirerID);
        transactionBean.setBankTansactionID(null);
        transactionBean.setToken(paymentInfoBean.getToken());
        transactionBean.setProductID(null);
        transactionBean.setPaymentType(paymentType);
        transactionBean.setFinalStatusType(null);
        transactionBean.setGFGNotification(false);
        transactionBean.setConfirmed(false);
        transactionBean.setPaymentMethodId(paymentInfoBean.getId());
        transactionBean.setPaymentMethodType(paymentInfoBean.getType());
        transactionBean.setServerName(serverName);
        transactionBean.setStatusAttemptsLeft(maxStatusAttempts);
        transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
        transactionBean.setOutOfRange(outOfRange);
        transactionBean.setRefuelMode(refuelMode);
        transactionBean.setVoucherReconciliation(false);
        transactionBean.setEncodedSecretKey(encodedSecretKey);
        transactionBean.setGroupAcquirer(groupAcquirer);
        transactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);

        // Crea lo stato iniziale della transazione
        TransactionStatusBean transactionStatusBean = new TransactionStatusBean();
        transactionStatusBean.setTransactionBean(transactionBean);
        transactionStatusBean.setSequenceID(1);
        transactionStatusBean.setTimestamp(creationTimestamp);
        transactionStatusBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
        transactionStatusBean.setSubStatus(null);
        transactionStatusBean.setSubStatusDescription("Started");
        transactionStatusBean.setRequestID(requestID);

        em.persist(transactionBean);
        em.persist(transactionStatusBean);

        createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_SUCCESS);
        createRefuelResponse.setTokenValue(paymentInfoBean.getToken());
        createRefuelResponse.setTransactionID(transactionID);
        createRefuelResponse.setShopLogin(apiKey);
        createRefuelResponse.setAcquirerID(acquirerID);
        createRefuelResponse.setCurrency(currency);
        createRefuelResponse.setPaymentType(paymentType);
        createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

        userTransaction.commit();

        return createRefuelResponse;
    }

    private CreateRefuelResponse createRefuelTransactionVoucher(String requestID, UserTransaction userTransaction, UserBean userBean, Double amount, String encodedPin,
            String stationID, String pumpID, Integer pinCheckMaxAttempts, String defaultShopLogin, String defaultAcquirerID, Integer pumpNumber, String productID,
            String productDescription, String currency, String paymentType, String serverName, Integer maxStatusAttempts, Integer reconciliationMaxAttempts, String outOfRange,
            String refuelMode, UserCategoryService userCategoryService, EmailSenderRemote emailSender, TransactionService transactionService, GPServiceRemote gpService,
            FidelityServiceRemote fidelityService, String proxyHost, String proxyPort, String proxyNoHosts) throws Exception {

        System.out.println("Utilizzo nuovo flusso voucher");

        CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();

        // Verifica lo stationID
        StationBean stationBean = QueryRepository.findStationBeanById(em, stationID);

        if (stationBean == null) {

            // Lo stationID inserito non corrisponde a nessuna stazione valida
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID);

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }
        /*
        double capAvailable = 0.0;

        if (userBean.getCapAvailable() != null) {
            capAvailable = userBean.getCapAvailable().doubleValue();
        }

        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {

            // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Payment not verified and transaction amount exceeding the cap");

            Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean);

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_CAP_FAILURE);
            return createRefuelResponse;
        }
        */

        // Genera un transactionID da utilizzare nella chiamata al servizio di preautorizzazione consumo voucher
        String transactionID = new IdGenerator().generateId(16).substring(0, 32);

        // Preautorizza credito voucher
        System.out.println("Preautorizzazione credito voucher");

        TransactionPreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucher = transactionService.preAuthorizationConsumeVoucherNoTransaction(userBean.getId(),
                transactionID, stationID, amount);

        if (preAuthorizationConsumeVoucher == null) {

            // Errore nella preautorizzazione dell'importo tramite voucher
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                    "Error in preAuthorizationConsumeVoucherNoTransaction: preAuthorizationConsumeVoucher null");

            userTransaction.commit();
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }

        if (preAuthorizationConsumeVoucher.getStatusCode().equals(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_PV_NOT_ENABLED)) {

            // Errore nella preautorizzazione dell'importo tramite voucher
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                    "Error in preAuthorizationConsumeVoucherNoTransaction: statusCode " + preAuthorizationConsumeVoucher.getStatusCode());

            userTransaction.commit();
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PV_NOT_ENABLED);
            return createRefuelResponse;
        }

        if (!preAuthorizationConsumeVoucher.getStatusCode().equals(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS)) {

            // Errore nella preautorizzazione dell'importo tramite voucher
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                    "Error in preAuthorizationConsumeVoucherNoTransaction: statusCode " + preAuthorizationConsumeVoucher.getStatusCode());

            userTransaction.commit();
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }
        else {

            if (preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getStatusCode().equals(
                    FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER)
                    || preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getStatusCode().equals(
                            FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER2)) {

                System.out.println("Importo voucher non sufficiente");

                // L'utente non ha sufficiente credito voucher
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                        "Error in preAuthorizationConsumeVoucherNoTransaction: Importo voucher non sufficiente");

                userTransaction.commit();
                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_INSUFFICIENT_VOUCHER_AMOUNT);
                return createRefuelResponse;
            }
        }

        System.out.println("Preautorizzazione credito voucher con esito positivo");

        String shopLogin = defaultShopLogin;
        String acquirerID = "QUENIT"; //defaultAcquirerID;

        shopLogin = "ENIPAY" + stationBean.getStationID();

        Date now = new Date();
        Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

        // Crea la transazione con i dati ricevuti in input
        TransactionBean transactionBean = new TransactionBean();
        transactionBean.setTransactionID(transactionID);
        transactionBean.setUserBean(userBean);
        transactionBean.setStationBean(stationBean);
        transactionBean.setUseVoucher(true);
        transactionBean.setCreationTimestamp(creationTimestamp);
        transactionBean.setEndRefuelTimestamp(null);
        transactionBean.setInitialAmount(amount);
        transactionBean.setFinalAmount(null);
        transactionBean.setFuelAmount(null);
        transactionBean.setFuelQuantity(null);
        transactionBean.setPumpID(pumpID);
        transactionBean.setPumpNumber(pumpNumber);
        transactionBean.setProductID(productID);
        transactionBean.setProductDescription(productDescription);
        transactionBean.setCurrency(currency);
        transactionBean.setShopLogin(shopLogin);
        transactionBean.setAcquirerID(acquirerID);
        transactionBean.setBankTansactionID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getPreAuthOperationID());
        transactionBean.setToken(paymentInfoBean.getToken());
        transactionBean.setProductID(null);
        transactionBean.setPaymentType(paymentType);
        transactionBean.setFinalStatusType(null);
        transactionBean.setGFGNotification(false);
        transactionBean.setConfirmed(false);
        transactionBean.setPaymentMethodId(paymentInfoBean.getId());
        transactionBean.setPaymentMethodType(paymentInfoBean.getType());
        transactionBean.setServerName(serverName);
        transactionBean.setStatusAttemptsLeft(maxStatusAttempts);
        transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
        transactionBean.setOutOfRange(outOfRange);
        transactionBean.setRefuelMode(refuelMode);
        transactionBean.setVoucherReconciliation(false);
        transactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);
        transactionBean.setAuthorizationCode(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getPreAuthOperationID());

        // Crea lo stato iniziale della transazione
        TransactionStatusBean transactionStatusBean = new TransactionStatusBean();
        transactionStatusBean.setTransactionBean(transactionBean);
        transactionStatusBean.setSequenceID(1);
        transactionStatusBean.setTimestamp(creationTimestamp);
        transactionStatusBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
        transactionStatusBean.setSubStatus(null);
        transactionStatusBean.setSubStatusDescription("Started");
        transactionStatusBean.setRequestID(requestID);

        // Crea la riga di log di preautorizzazione voucher
        PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
        prePaidConsumeVoucherBean.setOperationID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getOperationID());
        prePaidConsumeVoucherBean.setRequestTimestamp(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getRequestTimestamp());
        prePaidConsumeVoucherBean.setOperationType("PRE-AUTHORIZATION");
        prePaidConsumeVoucherBean.setAmount(amount);
        prePaidConsumeVoucherBean.setTransactionBean(transactionBean);
        prePaidConsumeVoucherBean.setCsTransactionID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getCsTransactionID());
        prePaidConsumeVoucherBean.setMessageCode(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getMessageCode());
        prePaidConsumeVoucherBean.setStatusCode(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getStatusCode());
        prePaidConsumeVoucherBean.setTotalConsumed(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getAmount());
        prePaidConsumeVoucherBean.setMarketingMsg(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getMarketingMsg());
        prePaidConsumeVoucherBean.setPreAuthOperationID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getPreAuthOperationID());

        transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

        em.persist(transactionBean);
        em.persist(transactionStatusBean);
        em.persist(prePaidConsumeVoucherBean);

        createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_SUCCESS);
        createRefuelResponse.setTokenValue(paymentInfoBean.getToken());
        createRefuelResponse.setTransactionID(transactionID);
        createRefuelResponse.setShopLogin(shopLogin);
        createRefuelResponse.setAcquirerID(acquirerID);
        createRefuelResponse.setCurrency(currency);
        createRefuelResponse.setPaymentType(paymentType);
        createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

        userTransaction.commit();

        return createRefuelResponse;
    }

}
