package com.techedge.mp.core.actions.transaction;

import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;

public class TransactionRefuelSendNotification implements Runnable {

    private String                             requestID;
    private String                             srcTransactionID;
    private String                             transactionID;
    private String                             mpTransactionStatus;
    private RefuelDetail                       fuelDeatail;
    private RefuelingNotificationServiceRemote refuelingNotificationService;
    private RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2;
    private Boolean                            isRefuelingOAUth2;
    private LoggerServiceRemote                loggerService;

    public TransactionRefuelSendNotification(String requestID, String srcTransactionID, String transactionID, String mpTransactionStatus, RefuelDetail fuelDeatail, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, Boolean isRefuelingOAUth2, 
            LoggerServiceRemote loggerService) {

        this.requestID = requestID;
        this.srcTransactionID = srcTransactionID;
        this.transactionID = transactionID;
        this.mpTransactionStatus = mpTransactionStatus;
        this.fuelDeatail = fuelDeatail;
        this.refuelingNotificationService = refuelingNotificationService;
        this.refuelingNotificationServiceOAuth2 = refuelingNotificationServiceOAuth2;
        this.isRefuelingOAUth2 = isRefuelingOAUth2;
        this.loggerService = loggerService;
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            if (loggerService != null) {
                this.loggerService.log(level, className, methodName, groupId, phaseId, message);    
            }
            else {
                System.out.println(level + " [" + className + "] " + methodName + " -- " + message);
            }
        }
        catch (Exception ex) {

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    @Override
    public void run() {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", this.requestID));
        inputParameters.add(new Pair<String, String>("srcTransactionID", this.srcTransactionID));
        inputParameters.add(new Pair<String, String>("transactionID", this.transactionID));
        inputParameters.add(new Pair<String, String>("mpTransactionStatus", this.mpTransactionStatus));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "run", this.srcTransactionID, "opening", ActivityLog.createLogMessage(inputParameters));

        try {

            String response;
            
            if (isRefuelingOAUth2) {
                response = this.refuelingNotificationServiceOAuth2.sendMPTransactionNotification(this.requestID, this.srcTransactionID, this.transactionID, this.mpTransactionStatus, this.fuelDeatail);
            }
            else {
                response = this.refuelingNotificationService.sendMPTransactionNotification(this.requestID, this.srcTransactionID, this.transactionID, this.mpTransactionStatus, this.fuelDeatail);
            }

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("response", response));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "run", this.srcTransactionID, "closing", ActivityLog.createLogMessage(outputParameters));

        }
        catch (Exception ex) {

            ex.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "run", this.srcTransactionID, null, "Exception message: " + ex.getMessage());

        }
    }
}
