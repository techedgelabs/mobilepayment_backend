package com.techedge.mp.core.actions.transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionUseVoucherResponse;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.ConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionUseVoucherAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionUseVoucherAction() {}

    public TransactionUseVoucherResponse execute(String transactionID, Double amount, FidelityServiceRemote fidelityService) throws EJBException {

        TransactionUseVoucherResponse transactionUseVoucherResponse = new TransactionUseVoucherResponse();

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Ricerca la transazione associata al transactionID
            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);

            if (transactionBean == null) {

                // Transaction not found
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionID, null, "Transaction not found");

                userTransaction.commit();
                transactionUseVoucherResponse.setTransactionID(transactionID);
                transactionUseVoucherResponse.setInitialAmount(amount);
                transactionUseVoucherResponse.setVoucherUsed(false);

                return transactionUseVoucherResponse;

            }
            else {
                boolean useVoucher = transactionBean.getUserBean().getUseVoucher();

                ////////////////
                // Se l'utente ha scelto di utilizzare i voucher bisogna effettuare la chiamata al servizio consumeVoucher del fidelityAdapter
                transactionBean.setUseVoucher(useVoucher);
                transactionBean.setVoucherReconciliation(false);
                em.merge(transactionBean);

                if (useVoucher == true && amount > 0) {

                    UserBean userBean = transactionBean.getUserBean();
                    PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(transactionBean.getPaymentMethodId(), transactionBean.getPaymentMethodType());

                    System.out.println("Pagamento con voucher");

                    // Chimamata al servizio consumeVoucher

                    String operationID = new IdGenerator().generateId(16).substring(0, 33);
                    PartnerType partnerType = PartnerType.MP;
                    VoucherConsumerType voucherType = VoucherConsumerType.ENI;
                    Long requestTimestamp = new Date().getTime();
                    String stationID = transactionBean.getStationBean().getStationID();
                    String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;

                    String BIN = paymentInfoBean.getCardBin();

                    if (BIN != null && QueryRepository.findCardBinExists(em, BIN)) {
                        System.out.println("BIN valido (" + BIN + ")");
                        paymentMode = FidelityConstants.PAYMENT_METHOD_ENI_CARD;
                    }
                    else {
                        System.out.println("BIN Non Valido (" + BIN + ")");
                    }

                    List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

                    Boolean noVoucher = true;
                    
                    Integer maxVoucherCount = 200;
                    Integer voucherCount = 0;

                    for (VoucherBean voucherBean : userBean.getVoucherList()) {

                        //System.out.println("Trovato voucher " + voucherBean.getCode() + " con stato " + voucherBean.getStatus());

                        if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {

                            System.out.println("Voucher inserito in richiesta");

                            VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                            voucherCodeDetail.setVoucherCode(voucherBean.getCode());
                            voucherCodeList.add(voucherCodeDetail);

                            noVoucher = false;
                            
                            voucherCount++;
                        }
                        
                        if (voucherCount >= maxVoucherCount) {
                            break;
                        }
                    }

                    if (noVoucher == true) {

                        System.out.println("No voucher found");

                        transactionUseVoucherResponse.setTransactionID(transactionID);
                        transactionUseVoucherResponse.setInitialAmount(amount);
                        transactionUseVoucherResponse.setVoucherUsed(false);

                        transactionBean.setVoucherStatus(StatusHelper.VOUCHER_STATUS_NOT_FOUND);
                        em.merge(transactionBean);
                        userTransaction.commit();

                        return transactionUseVoucherResponse;

                    }
                    else {

                        //caricamento della ProductList dalla transazione; � un caricamento fittizio di una transazione refuel
                        String refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_PREPAY;
                        List<ProductDetail> productList = this.getProductList(transactionBean, amount);

                        ConsumeVoucherResult consumeVoucherResult = new ConsumeVoucherResult();

                        Double totalConsumed = 0.0;
                        PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
                        prePaidConsumeVoucherBean.setOperationID(operationID);
                        prePaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
                        prePaidConsumeVoucherBean.setOperationType("CONSUME");
                        prePaidConsumeVoucherBean.setTransactionBean(transactionBean);

                        try {

                            consumeVoucherResult = fidelityService.consumeVoucher(operationID, transactionID, voucherType, stationID, refuelMode, paymentMode, "it", partnerType,
                                    requestTimestamp, productList, voucherCodeList, FidelityConstants.CONSUME_TYPE_PARTIAL, "");

                            prePaidConsumeVoucherBean.setCsTransactionID(consumeVoucherResult.getCsTransactionID());
                            prePaidConsumeVoucherBean.setMarketingMsg(consumeVoucherResult.getMarketingMsg());
                            prePaidConsumeVoucherBean.setMessageCode(consumeVoucherResult.getMessageCode());
                            prePaidConsumeVoucherBean.setStatusCode(consumeVoucherResult.getStatusCode());
                            prePaidConsumeVoucherBean.setWarningMsg(consumeVoucherResult.getWarningMsg());
                            em.persist(prePaidConsumeVoucherBean);

                            for (VoucherDetail voucherDetail : consumeVoucherResult.getVoucherList()) {

                                if (voucherDetail.getConsumedValue() == 0.0) {

                                    System.out.println(voucherDetail.getVoucherCode() + ": Valore consumato 0.0 -> aggiornamento non necessario");
                                    continue;
                                }

                                PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean = new PrePaidConsumeVoucherDetailBean();

                                prePaidConsumeVoucherDetailBean.setConsumedValue(voucherDetail.getConsumedValue());
                                prePaidConsumeVoucherDetailBean.setExpirationDate(voucherDetail.getExpirationDate());
                                prePaidConsumeVoucherDetailBean.setInitialValue(voucherDetail.getInitialValue());
                                prePaidConsumeVoucherDetailBean.setPromoCode(voucherDetail.getPromoCode());
                                prePaidConsumeVoucherDetailBean.setPromoDescription(voucherDetail.getPromoDescription());
                                prePaidConsumeVoucherDetailBean.setPromoDoc(voucherDetail.getPromoDoc());
                                prePaidConsumeVoucherDetailBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                prePaidConsumeVoucherDetailBean.setVoucherCode(voucherDetail.getVoucherCode());
                                prePaidConsumeVoucherDetailBean.setVoucherStatus(voucherDetail.getVoucherStatus());
                                prePaidConsumeVoucherDetailBean.setVoucherType(voucherDetail.getVoucherType());
                                prePaidConsumeVoucherDetailBean.setVoucherValue(voucherDetail.getVoucherValue());
                                prePaidConsumeVoucherDetailBean.setPrePaidConsumeVoucherBean(prePaidConsumeVoucherBean);
                                em.persist(prePaidConsumeVoucherDetailBean);

                                prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailBean().add(prePaidConsumeVoucherDetailBean);

                                totalConsumed = totalConsumed + voucherDetail.getConsumedValue();

                                // Aggiorna le informazioni sul voucher associato all'utente

                                System.out.println("Aggiornamento voucher utente");

                                for (VoucherBean voucherBean : userBean.getVoucherList()) {

                                    if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                                        System.out.println("Aggiornamento voucher " + voucherDetail.getVoucherCode());

                                        voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                                        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                                        voucherBean.setInitialValue(voucherDetail.getInitialValue());
                                        voucherBean.setPromoCode(voucherDetail.getPromoCode());
                                        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                                        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                                        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                        voucherBean.setCode(voucherDetail.getVoucherCode());
                                        voucherBean.setStatus(voucherDetail.getVoucherStatus());
                                        voucherBean.setType(voucherDetail.getVoucherType());
                                        voucherBean.setValue(voucherDetail.getVoucherValue());
                                    }
                                }
                            }

                            // Aggiornamento dei dati utente
                            em.merge(userBean);

                            prePaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
                            transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

                            System.out.println("Totale pagato con voucher: " + totalConsumed);

                            if (totalConsumed > 0) {
                                System.out.println("Pagamento con voucher: importo " + totalConsumed);
                                transactionUseVoucherResponse.setTransactionID(transactionID);
                                transactionUseVoucherResponse.setInitialAmount(amount);
                                transactionUseVoucherResponse.setFinalAmount(amount - totalConsumed);
                                transactionUseVoucherResponse.setVoucherTotalAmount(totalConsumed);
                                transactionUseVoucherResponse.setVoucherUsed(true);
                                transactionBean.setVoucherStatus(StatusHelper.VOUCHER_STATUS_OK);

                            }
                            else {
                                System.out.println("Pagamento con voucher: importo nullo");
                                transactionUseVoucherResponse.setTransactionID(transactionID);
                                transactionUseVoucherResponse.setInitialAmount(amount);
                                transactionUseVoucherResponse.setVoucherUsed(false);
                                transactionBean.setVoucherStatus(StatusHelper.VOUCHER_STATUS_NOT_APPLICABLE);

                            }

                            em.persist(prePaidConsumeVoucherBean);
                            em.merge(transactionBean);
                            userTransaction.commit();
                            return transactionUseVoucherResponse;

                        }
                        catch (Exception e) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", "requestID", null, "Error consuming vouchers");

                            prePaidConsumeVoucherBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
                            prePaidConsumeVoucherBean.setMessageCode("STORNO DA VERIFICARE (" + e.getMessage() + ")");
                            prePaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
                            prePaidConsumeVoucherBean.setTransactionBean(transactionBean);
                            transactionBean.setVoucherReconciliation(true);
                            transactionBean.setVoucherStatus(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
                            transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

                            em.persist(prePaidConsumeVoucherBean);
                            em.merge(transactionBean);
                            userTransaction.commit();
                            transactionUseVoucherResponse.setTransactionID(transactionID);
                            transactionUseVoucherResponse.setInitialAmount(amount);
                            transactionUseVoucherResponse.setVoucherUsed(false);
                            return transactionUseVoucherResponse;

                        }
                    }
                }
                else {
                    userTransaction.commit();
                    System.out.println("Pagamento senza voucher");
                    transactionUseVoucherResponse.setTransactionID(transactionID);
                    transactionUseVoucherResponse.setInitialAmount(amount);
                    transactionUseVoucherResponse.setVoucherUsed(false);
                    return transactionUseVoucherResponse;
                }

            }
        }

        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED Transaction Voucher Payment with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }

    private List<ProductDetail> getProductList(TransactionBean transactionBean, Double amount) {
        List<ProductDetail> productList = new ArrayList<ProductDetail>(0);

        ProductDetail productDetail = new ProductDetail();
        productDetail.setAmount(amount);

        if (transactionBean.getProductID() == null) {
            productDetail.setProductCode(null);
        }
        else {
            if (transactionBean.getProductID().equals("SP")) {

                // sp
                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_SP);
            }
            else {

                if (transactionBean.getProductID().equals("GG")) {

                    // gasolio
                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GASOLIO);
                }
                else {

                    if (transactionBean.getProductID().equals("BS")) {

                        // blue_super
                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_SUPER);
                    }
                    else {

                        if (transactionBean.getProductID().equals("BD")) {

                            // blue_diesel
                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_DIESEL);
                        }
                        else {

                            if (transactionBean.getProductID().equals("MT")) {

                                // metano
                                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_METANO);
                            }
                            else {

                                if (transactionBean.getProductID().equals("GP")) {

                                    // gpl
                                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GPL);
                                }
                                else {

                                    if (transactionBean.getProductID().equals("AD")) {

                                        // ???
                                        productDetail.setProductCode(null);
                                    }
                                    else {

                                        // non_oil
                                        productDetail.setProductCode(null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        productDetail.setQuantity(transactionBean.getFuelQuantity());
        productList.add(productDetail);

        return productList;

    }

}
