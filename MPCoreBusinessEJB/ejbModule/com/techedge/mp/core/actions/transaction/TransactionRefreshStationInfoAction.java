package com.techedge.mp.core.actions.transaction;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionRefreshStationInfoAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public TransactionRefreshStationInfoAction() {
    }

    
    public String execute(
    		String ticketID,
			String stationID,
			String address,
			String city,
			String country,
			String province,
			Double latitude,
			Double longitude) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", ticketID, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.REFRESH_STATION_FAILED;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if ( userBean == null ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", ticketID, null, "User not found" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.REFRESH_STATION_FAILED;
    		}
    		
    		
    		StationBean stationBean = QueryRepository.findStationBeanById(em, stationID);

    		if ( stationBean == null ) {
    			
    			// Stazione non trovata
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", ticketID, null, "Station not found" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.REFRESH_STATION_NOT_FOUND;
    		}

    		if ( stationBean.getAddress() != null &&
    				stationBean.getCity() != null &&
    				stationBean.getCountry() != null &&
    				stationBean.getProvince() != null &&
    				stationBean.getLatitude() != null &&
    				stationBean.getLongitude() != null ) {
    			
	    		if ( stationBean.getAddress().equals(address) &&
	    				stationBean.getCity().equals(city) &&
	    				stationBean.getCountry().equals(country) &&
	    				stationBean.getProvince().equals(province) &&
	    				stationBean.getLatitude().equals(latitude) &&
	    				stationBean.getLongitude().equals(longitude) ) {
	
	    			// Stazione gi� aggiornata
	    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", ticketID, null, "Nothing to update" );
					
	    			userTransaction.commit();
	    			
	    			return ResponseHelper.REFRESH_STATION_NO_UPDATE;
	    		}
    		}

    		stationBean.setAddress(address);
    		stationBean.setCity(city);
    		stationBean.setCountry(country);
    		stationBean.setProvince(province);
    		stationBean.setLatitude(latitude);
    		stationBean.setLongitude(longitude);

    		em.merge(stationBean);

    		userTransaction.commit();
    		
    		return ResponseHelper.REFRESH_STATION_SUCCESS;
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED refreshing station with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", ticketID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}