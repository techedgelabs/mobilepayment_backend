package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionPersistStatusRequestKoStatusAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionPersistStatusRequestKoStatusAction() {}

    public String execute(String requestID, String transactionID, String subStatus, String subStatusDescription, EmailSenderRemote emailSender, String proxyHost, String proxyPort,
            String proxyNoHosts, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Ricerca la transazione associata al transactionID
            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);

            if (transactionBean == null) {

                // Transaction not found
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Transaction not found");

                userTransaction.commit();

                return StatusHelper.PERSIST_TRANSACTION_STATUS_REQUEST_KO_ERROR;
            }

            // Crea il nuovo stato
            Integer sequenceID = 0;
            Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
            for (TransactionStatusBean transactionStatusBean : transactionStatusData) {
                Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                if (sequenceID < transactionStatusBeanSequenceID) {
                    sequenceID = transactionStatusBeanSequenceID;
                }
            }
            sequenceID = sequenceID + 1;

            Date now = new Date();
            Timestamp timestamp = new java.sql.Timestamp(now.getTime());

            TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

            transactionStatusBean.setTransactionBean(transactionBean);
            transactionStatusBean.setSequenceID(sequenceID);
            transactionStatusBean.setTimestamp(timestamp);
            transactionStatusBean.setStatus(StatusHelper.STATUS_TRANSACTION_STATUS_REQUEST_KO);
            transactionStatusBean.setSubStatus(subStatus);
            // La stringa viene troncata se pi� lunga di 254 caratteri
            if (subStatusDescription.length() > 254)
                subStatusDescription = subStatusDescription.substring(0, 254);
            transactionStatusBean.setSubStatusDescription(subStatusDescription);
            transactionStatusBean.setRequestID(requestID);

            transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_ERROR);

            em.merge(transactionBean);

            em.persist(transactionStatusBean);

            Boolean isNewFlow = false;

            isNewFlow = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_PAYMENT_FLOW.getCode());
            
            if ( userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode())) {
                isNewFlow = Boolean.TRUE;
            }
            
            if (isNewFlow) {

                System.out.println("Invio mail ad utente nuovo flusso");
            }
            else {

                System.out.println("Invio mail ad utente vecchio flusso");
            }

            UserBean userBean = transactionBean.getUserBean();
            if (( userBean.getUserType() == User.USER_TYPE_CUSTOMER || userBean.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER ) && emailSender != null) {
                Email.sendPrePaidSummary(emailSender, transactionBean, transactionStatusBean, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
            }

            userTransaction.commit();

            return StatusHelper.PERSIST_TRANSACTION_STATUS_REQUEST_KO_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED persisting status request ko with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}