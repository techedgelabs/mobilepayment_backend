package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PendingTransactionRefuelResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionFinalizePendingRefuelAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionFinalizePendingRefuelAction() {}

    public PendingTransactionRefuelResponse execute(Integer transactionTimeAlive) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        PendingTransactionRefuelResponse pendingTransactionRefuelResponse = new PendingTransactionRefuelResponse();

        try {
            userTransaction.begin();

            // Ricerca le transazioni attive con stato finale nullo
            List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByFinalStatusNull(em);

            if (transactionBeanList.isEmpty()) {

                // Non esiste nessuna una transazione attiva
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Active transaction to finalize not found");
            }
            else {

                for (TransactionBean transactionBean : transactionBeanList) {

                    Calendar calendarDeadTime = Calendar.getInstance();
                    //calendarDeadTime.setTime(transactionBean.getCreationTimestamp());
                    //calendarDeadTime.add(Calendar.SECOND, transactionTimeAlive.intValue());
                    Date now = Calendar.getInstance().getTime();
                    int sequence = 1;
                    Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());
                    String requestID = null;
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    
                    TransactionStatusBean transactionStatusBean = transactionBean.getLastTransactionStatus();
                    sequence = transactionStatusBean.getSequenceID() + 1;
                    //for (TransactionStatusBean transactionStatusBean : transactionBean.getTransactionStatusBeanData()) {
                    //    sequence = transactionStatusBean.getSequenceID() + 1;
                    //}
                    
                    long transactionDeadTime = (transactionBean.getCreationTimestamp().getTime() + transactionTimeAlive.longValue());
                    calendarDeadTime.setTimeInMillis(transactionDeadTime);
                    System.out.println("TimeAlive: " + transactionTimeAlive.longValue());
                    System.out.println("Data creazione transazione: " + sdf.format(transactionBean.getCreationTimestamp()) + " (" + transactionBean.getCreationTimestamp().getTime() + ")");
                    System.out.println("Data presunta scadenza transazione: " + sdf.format(calendarDeadTime.getTime()) + " (" + transactionDeadTime + ")");
                    System.out.println("Data transazioni pendenti: " + sdf.format(now));

                    if (transactionDeadTime < now.getTime()) {
                        System.out.println("Active transaction " + transactionBean.getTransactionID() + " final_status setted to ABNORMAL END");
                        transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_ABEND);

                        
                        transactionStatusBean = new TransactionStatusBean();
                        transactionStatusBean.setTransactionBean(transactionBean);
                        transactionStatusBean.setSequenceID(sequence);
                        transactionStatusBean.setTimestamp(creationTimestamp);
                        transactionStatusBean.setStatus(StatusHelper.STATUS_TRANSACTION_ABEND);
                        transactionStatusBean.setSubStatus(null);
                        transactionStatusBean.setSubStatusDescription("Abnormal End");
                        transactionStatusBean.setRequestID(requestID);

                        em.merge(transactionBean);
                        em.persist(transactionStatusBean);
                        
                        pendingTransactionRefuelResponse.getTransactionList().add(transactionBean.toTransaction());
                    }
                }
            }
            
            userTransaction.commit();

            pendingTransactionRefuelResponse.setStatusCode(ResponseHelper.TRANSACTION_FINALIZE_PENDING_REFUEL_SUCCESS);
            return pendingTransactionRefuelResponse;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve pending refuel with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }
}