package com.techedge.mp.core.actions.transaction;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.FidelityConsumeVoucherData;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TransactionCancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.VoucherCommonOperations;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CancelPreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionCancelPreAuthorizationConsumeVoucherAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionCancelPreAuthorizationConsumeVoucherAction() {}

    public TransactionCancelPreAuthorizationConsumeVoucherResponse execute(String transactionID, FidelityServiceRemote fidelityService) throws EJBException {

        TransactionCancelPreAuthorizationConsumeVoucherResponse transactionCancelPreAuthorizationConsumeVoucherResponse = new TransactionCancelPreAuthorizationConsumeVoucherResponse();

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Ricerca la transazione associata al transactionID
            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);

            if (transactionBean == null) {

                // Transaction not found
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionID, null, "Transaction not found");

                userTransaction.commit();

                transactionCancelPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_NOT_FOUND);
                transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusCode(null);
                transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusMessage("Transaction not found");

                return transactionCancelPreAuthorizationConsumeVoucherResponse;

            }
            else {

                PrePaidConsumeVoucherBean preAuthorizationConsumeVoucherBean = null;
                
                for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {
                    
                    if (prePaidConsumeVoucherBean.getOperationType().equals("PRE-AUTHORIZATION")) {
                        
                        preAuthorizationConsumeVoucherBean = prePaidConsumeVoucherBean;
                    }
                }
                
                if ( preAuthorizationConsumeVoucherBean == null ) {
                    
                    // Errore preautorizzazione non trovata
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", "requestID", null, "Error pre authorization consume voucher not found");

                    userTransaction.commit();

                    transactionCancelPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE);
                    transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusCode(null);
                    transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusMessage("Error pre authorization consume voucher not found");

                    return transactionCancelPreAuthorizationConsumeVoucherResponse;
                    
                }

                System.out.println("Inizio Cancellazione preautorizzazione consumo voucher");

                // Chimamata al servizio cancelPreAuthorizationConsumeVoucher

                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                String preAuthOperationIDToCancel = preAuthorizationConsumeVoucherBean.getOperationID();

                PartnerType partnerType = PartnerType.MP;
                Long requestTimestamp = new Date().getTime();

                CancelPreAuthorizationConsumeVoucherResult cancelPreAuthorizationConsumeVoucherResult = new CancelPreAuthorizationConsumeVoucherResult();

                PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
                prePaidConsumeVoucherBean.setOperationID(operationID);
                prePaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
                prePaidConsumeVoucherBean.setOperationType("CANCEL-PRE-AUTHORIZATION");
                prePaidConsumeVoucherBean.setTransactionBean(transactionBean);

                try {

                    cancelPreAuthorizationConsumeVoucherResult = fidelityService.cancelPreAuthorizationConsumeVoucher(operationID, preAuthOperationIDToCancel, partnerType,
                            requestTimestamp);

                    prePaidConsumeVoucherBean.setCsTransactionID(cancelPreAuthorizationConsumeVoucherResult.getCsTransactionID());
                    prePaidConsumeVoucherBean.setMessageCode(cancelPreAuthorizationConsumeVoucherResult.getMessageCode());
                    prePaidConsumeVoucherBean.setStatusCode(cancelPreAuthorizationConsumeVoucherResult.getStatusCode());
                    prePaidConsumeVoucherBean.setOperationIDReversed(preAuthOperationIDToCancel);
                    em.persist(prePaidConsumeVoucherBean);
                    
                    transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);
                    em.merge(transactionBean);

                    transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusCode(cancelPreAuthorizationConsumeVoucherResult.getStatusCode());
                    transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusMessage(cancelPreAuthorizationConsumeVoucherResult.getMessageCode());
                    
                    transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityConsumeVoucherData(new FidelityConsumeVoucherData());
                    transactionCancelPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setCsTransactionID(
                            cancelPreAuthorizationConsumeVoucherResult.getCsTransactionID());
                    transactionCancelPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setMessageCode(
                            cancelPreAuthorizationConsumeVoucherResult.getMessageCode());
                    transactionCancelPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setStatusCode(
                            cancelPreAuthorizationConsumeVoucherResult.getStatusCode());

                    if (cancelPreAuthorizationConsumeVoucherResult.getStatusCode().equals("00")) {
                        transactionCancelPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS);
                    }
                    else {
                        transactionCancelPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE);
                    }
                    
                    VoucherCommonOperations.check(transactionBean.getUserBean(), fidelityService, em);
                    
                    userTransaction.commit();

                    return transactionCancelPreAuthorizationConsumeVoucherResponse;

                }
                catch (Exception e) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", "requestID", null, "Error in cancel pre authorization consume voucher");

                    userTransaction.commit();

                    transactionCancelPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_SYSTEM_ERRROR);
                    transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusCode(null);
                    transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusMessage(e.getMessage());

                    return transactionCancelPreAuthorizationConsumeVoucherResponse;

                }
            }
        }

        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED Transaction Cancel Pre Authorization Consume Voucher with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }

}
