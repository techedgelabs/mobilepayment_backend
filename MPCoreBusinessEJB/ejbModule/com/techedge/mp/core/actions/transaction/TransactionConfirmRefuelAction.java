package com.techedge.mp.core.actions.transaction;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionConfirmRefuelAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public TransactionConfirmRefuelAction() {
    }


    public String execute(
    		String ticketID,
			String requestID,
			String refuelID) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_UPDATE_INVALID_TICKET;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if ( userBean == null ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_UPDATE_INVALID_TICKET;
    		}
    		
    		
    		// Verifica lo stato dell'utente
    		Integer userStatus = userBean.getUserStatus();
    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
    			
    			// Un utente che si trova in questo stato non pu� invocare questo servizio
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to confirm refuel in status " + userStatus );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_UPDATE_UNAUTHORIZED;
    		}
    		
    		
    		// Ricerca la transazione attiva (Prepaid)
    		List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);
		    // Ricerca la transazione attiva (PostPaid)
    		 PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, refuelID);
    		
		    if ( transactionBeanList.isEmpty() && postPaidTransactionBean == null) {
    			
    			// Non esiste una transazione attiva
		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction not found" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.CONFIRM_REFUEL_FAILURE;
    		}
		    else if ( !transactionBeanList.isEmpty()) {

				TransactionBean pendingTransactionBean = null;

				for( TransactionBean transactionBean : transactionBeanList ) {

					// Si estrae la prima
					pendingTransactionBean = transactionBean;
				}

				if ( pendingTransactionBean == null ) {
					
					// Non esiste una transazione attiva
			    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Id wrong in confirm refuel" );
					
	    			userTransaction.commit();
	    			
	    			return ResponseHelper.CONFIRM_REFUEL_ID_WRONG;
				}
				else {

					// Si verifica il refuelID
					if ( !pendingTransactionBean.getTransactionID().equals(refuelID) ) {

						// Non esiste una transazione attiva
				    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Id wrong in confirm refuel" );
						
		    			userTransaction.commit();
		    			
		    			return ResponseHelper.CONFIRM_REFUEL_ID_WRONG;
					}
					else {

						// Si verificano gli stati della transazione

						Boolean statusFound = false;
						String lastStatus = "";

						for ( TransactionStatusBean transactionStatusBean : pendingTransactionBean.getTransactionStatusBeanData() ) {

							if ( transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_NOT_AUTHORIZED) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_NOT_DELETED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_TRANSACTION_STATUS_REQUEST_KO ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_CLOSED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_NOT_CLOSED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_NOT_DELETED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PUMP_NOT_ENABLED )
									) {

								statusFound = true;
								lastStatus = transactionStatusBean.getStatus();
							}
						}

						if ( statusFound == false ) {

							System.out.println( "last status: " + lastStatus );

							// Non esiste una transazione attiva
					    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to confirm refuel transaction in status " + lastStatus );
							
			    			userTransaction.commit();
			    			
			    			return ResponseHelper.CONFIRM_REFUEL_FAILURE;
						}


						// Imposta il flag confirmed a true
						pendingTransactionBean.setConfirmed(true);

						em.merge(pendingTransactionBean);
						
						userTransaction.commit();

						return ResponseHelper.CONFIRM_REFUEL_SUCCESS;
					}
				}
			}
		    else if(postPaidTransactionBean != null){
		    	
		    	if ( postPaidTransactionBean.getUserBean().getId()!=userBean.getId() ) {

					// Non esiste una transazione attiva
			    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User Id wrong in confirm refuel" );
					
	    			userTransaction.commit();
	    			
	    			return ResponseHelper.CONFIRM_REFUEL_ID_WRONG;
				}
				else {
					if (postPaidTransactionBean.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)){
						System.out.println( "last status: " + postPaidTransactionBean.getMpTransactionStatus() );
						// Non esiste una transazione attiva
				    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to confirm refuel transaction in status " + postPaidTransactionBean.getMpTransactionStatus() );
					
		    			userTransaction.commit();
		    			
		    			return ResponseHelper.CONFIRM_REFUEL_FAILURE;
						
					}
					
					postPaidTransactionBean.setNotificationUser(true);
					em.merge(postPaidTransactionBean);
					userTransaction.commit();

					return ResponseHelper.CONFIRM_REFUEL_SUCCESS;
					
				}
		    	
		    }
		    
		    return ResponseHelper.CONFIRM_REFUEL_ID_WRONG;
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED confirm refuel with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );
			    
	        throw new EJBException(ex2);
    	}
		
    }
}
