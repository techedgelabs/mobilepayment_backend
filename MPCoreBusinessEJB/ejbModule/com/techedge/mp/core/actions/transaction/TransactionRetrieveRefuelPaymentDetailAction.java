package com.techedge.mp.core.actions.transaction;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentRefuelDetailResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.TransactionStatusHistoryBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.PanHelper;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionRetrieveRefuelPaymentDetailAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public TransactionRetrieveRefuelPaymentDetailAction() {
    }

    @SuppressWarnings("unchecked")
    public PaymentRefuelDetailResponse execute(
    		String requestID,
			String ticketID,
			String refuelID) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		Query query = null;
    		
    		PaymentRefuelDetailResponse paymentRefuelDetailResponse = null;
    		
    		TicketBean ticketBean = null;
    		
    		query = em.createNamedQuery( TicketBean.FIND_BY_TICKETID );
	        query.setParameter( "ticketId", ticketID );
	        query.setMaxResults(1);
		    List<TicketBean> ticketBeanList = query.getResultList();
		    if (ticketBeanList != null && ticketBeanList.size() > 0) {
		    	ticketBean = (TicketBean) ticketBeanList.get(0);
		    }
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			
    			paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
    			paymentRefuelDetailResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
    			return paymentRefuelDetailResponse;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if ( userBean == null ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found" );
				
    			userTransaction.commit();
    			
    			paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
    			paymentRefuelDetailResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
    			return paymentRefuelDetailResponse;
    		}
    		
    		
    		// Verifica lo stato dell'utente
    		Integer userStatus = userBean.getUserStatus();
    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
    			
    			// Un utente che si trova in questo stato non pu� invocare questo servizio
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to retrieve refuel payment detail in status " + userStatus );
				
    			userTransaction.commit();
    			
    			paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
    			paymentRefuelDetailResponse.setStatusCode(ResponseHelper.USER_UPDATE_UNAUTHORIZED);
    			return paymentRefuelDetailResponse;
    		}
    		
    		
    		if ( refuelID == null ) {
    			
    			query = em.createNamedQuery( TransactionBean.FIND_ACTIVE_BY_USER );
    	        query.setParameter( "userBean", userBean );
    		    List<TransactionBean> transactionBeanList = query.getResultList();    		    
    			if ( transactionBeanList.isEmpty() ) {
    				
    				// Nessuna transazione attiva associata all'utente
    				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to retrieve refuel payment detail" );
    				
        			userTransaction.commit();
        			
        			paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
        			paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_FAILURE);
    				return paymentRefuelDetailResponse;
    			}
    			else {

    				TransactionBean pendingTransactionBean = null;

    				for( TransactionBean transactionBean : transactionBeanList ) {

    					// Si estrae la prima
    					pendingTransactionBean = transactionBean;
    				}

    				if ( pendingTransactionBean == null ) {

    					// Nessuna transazione attiva associata all'utente
        				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Id wrong in retrieving refuel payment detail" );
        				
            			userTransaction.commit();
            			
            			paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
            			paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_WRONG_ID);
    					return paymentRefuelDetailResponse;
    				}
    				else {

    					// Si estrae lo stato pi� recente associato alla transazione
    					Integer lastSequenceID = 0;
    					TransactionStatusBean lastTransactionStatusBean = null;

    					Set<TransactionStatusBean> transactionStatusData = pendingTransactionBean.getTransactionStatusBeanData();

    					for( TransactionStatusBean transactionStatusBean : transactionStatusData ) {

    						Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
    						if ( lastSequenceID < transactionStatusBeanSequenceID ) {
    							lastSequenceID = transactionStatusBeanSequenceID;
    							lastTransactionStatusBean = transactionStatusBean;
    						}
    					}
    					
    					paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
    					paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_SUCCESS);
    					paymentRefuelDetailResponse.setRefuelID(pendingTransactionBean.getTransactionID());
    					paymentRefuelDetailResponse.setStatus(lastTransactionStatusBean.getStatus());
    					paymentRefuelDetailResponse.setSubStatus(lastTransactionStatusBean.getSubStatus());
    					paymentRefuelDetailResponse.setUseVoucher(pendingTransactionBean.getUseVoucher());
    					paymentRefuelDetailResponse.setDate(lastTransactionStatusBean.getTimestamp());
    					paymentRefuelDetailResponse.setInitialAmount(pendingTransactionBean.getInitialAmount());
    					paymentRefuelDetailResponse.setFinalAmount(pendingTransactionBean.getFinalAmount());
    					paymentRefuelDetailResponse.setFuelAmount(pendingTransactionBean.getFuelAmount());
    					paymentRefuelDetailResponse.setFuelQuantity(pendingTransactionBean.getFuelQuantity());

    					paymentRefuelDetailResponse.setBankTransactionID(pendingTransactionBean.getBankTansactionID());
    					paymentRefuelDetailResponse.setShopLogin(pendingTransactionBean.getShopLogin());
    					paymentRefuelDetailResponse.setCurrency(pendingTransactionBean.getCurrency());
    					String maskedPan = PanHelper.maskPan(pendingTransactionBean.getToken());
    					paymentRefuelDetailResponse.setMaskedPan(maskedPan);
    					paymentRefuelDetailResponse.setAuthCode(pendingTransactionBean.getAuthorizationCode());

    					paymentRefuelDetailResponse.setSelectedStationID(pendingTransactionBean.getStationBean().getStationID());
    					// TODO - il nome della stazione non viene restituito
    					paymentRefuelDetailResponse.setSelectedStationName("");
    					paymentRefuelDetailResponse.setSelectedStationAddress(pendingTransactionBean.getStationBean().getAddress());
    					paymentRefuelDetailResponse.setSelectedStationCity(pendingTransactionBean.getStationBean().getCity());
    					paymentRefuelDetailResponse.setSelectedStationProvince(pendingTransactionBean.getStationBean().getProvince());
    					paymentRefuelDetailResponse.setSelectedStationCountry(pendingTransactionBean.getStationBean().getCountry());
    					paymentRefuelDetailResponse.setSelectedStationLatitude(pendingTransactionBean.getStationBean().getLatitude());
    					paymentRefuelDetailResponse.setSelectedStationLongitude(pendingTransactionBean.getStationBean().getLongitude());
    					paymentRefuelDetailResponse.setSelectedPumpID(pendingTransactionBean.getPumpID());
    					paymentRefuelDetailResponse.setSelectedPumpNumber(pendingTransactionBean.getPumpNumber());
    					paymentRefuelDetailResponse.setSelectedPumpFuelType(pendingTransactionBean.getProductDescription());
    					
    					userTransaction.commit();
    					
    					return paymentRefuelDetailResponse;
    				}
    			}
    		}
    		else {
    			
    			TransactionBean transactionBean = null;
    			query = em.createNamedQuery( TransactionBean.FIND_BY_ID );
    	        query.setParameter( "transactionID", refuelID );
    	        query.setMaxResults(1);
    		    List<TransactionBean> transactionBeanList = query.getResultList();
    		    if (transactionBeanList != null && transactionBeanList.size() > 0) {
    		    	transactionBean = (TransactionBean) transactionBeanList.get(0);
    		    }
    			
    			TransactionHistoryBean transactionHistoryBean = null;
    			query = em.createNamedQuery( TransactionHistoryBean.FIND_BY_ID );
    	        query.setParameter( "transactionID", refuelID );
    	        query.setMaxResults(1);
    		    List<TransactionHistoryBean> transactionHistoryBeanList = query.getResultList();
    		    if (transactionHistoryBeanList != null && transactionHistoryBeanList.size() > 0) {
    		    	transactionHistoryBean = (TransactionHistoryBean) transactionHistoryBeanList.get(0);
    		    }

    			if ( ( transactionBean == null || transactionBean.getUserBean().getId() != userBean.getId() ) &&
    				 ( transactionHistoryBean == null || transactionHistoryBean.getUserBean().getId() != userBean.getId() )
    				) {
    				
    				// Nessuna transazione trovata
    				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No refuel found with refuelID " + refuelID );
    				
        			userTransaction.commit();
        			
        			paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
    				paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_FAILURE);
    				return paymentRefuelDetailResponse;
    			}
    			else {
    				
    				if ( transactionBean != null ) {
    					
    					TransactionBean pendingTransactionBean = transactionBean;

    					// Si estrae lo stato pi� recente associato alla transazione
    					Integer lastSequenceID = 0;
    					TransactionStatusBean lastTransactionStatusBean = null;

    					Set<TransactionStatusBean> transactionStatusData = pendingTransactionBean.getTransactionStatusBeanData();

    					for( TransactionStatusBean transactionStatusBean : transactionStatusData ) {

    						Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
    						if ( lastSequenceID < transactionStatusBeanSequenceID ) {
    							lastSequenceID = transactionStatusBeanSequenceID;
    							lastTransactionStatusBean = transactionStatusBean;
    						}
    					}
    					
    					if ( lastTransactionStatusBean == null ) {
    						
    						// Nessuna transazione trovata
    	    				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Id wrong in retrieve refuel payment detail" );
    	    				
    	        			userTransaction.commit();
    	        			
    	        			paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
    	    				paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_FAILURE);
    	    				return paymentRefuelDetailResponse;
    					}
    					else {
    						
    						paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
	    					paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_SUCCESS);
	    					paymentRefuelDetailResponse.setRefuelID(pendingTransactionBean.getTransactionID());
	    					paymentRefuelDetailResponse.setStatus(lastTransactionStatusBean.getStatus());
	    					paymentRefuelDetailResponse.setSubStatus(lastTransactionStatusBean.getSubStatus());
	    					paymentRefuelDetailResponse.setUseVoucher(pendingTransactionBean.getUseVoucher());
	    					paymentRefuelDetailResponse.setDate(lastTransactionStatusBean.getTimestamp());
	    					paymentRefuelDetailResponse.setInitialAmount(pendingTransactionBean.getInitialAmount());
	    					paymentRefuelDetailResponse.setFinalAmount(pendingTransactionBean.getFinalAmount());
	    					paymentRefuelDetailResponse.setFuelAmount(pendingTransactionBean.getFuelAmount());
	    					paymentRefuelDetailResponse.setFuelQuantity(pendingTransactionBean.getFuelQuantity());
	
	    					paymentRefuelDetailResponse.setBankTransactionID(pendingTransactionBean.getBankTansactionID());
	    					paymentRefuelDetailResponse.setShopLogin(pendingTransactionBean.getShopLogin());
	    					paymentRefuelDetailResponse.setCurrency(pendingTransactionBean.getCurrency());
	    					String maskedPan = PanHelper.maskPan(pendingTransactionBean.getToken());
	    					paymentRefuelDetailResponse.setMaskedPan(maskedPan);
	    					paymentRefuelDetailResponse.setAuthCode(pendingTransactionBean.getAuthorizationCode());
	
	    					paymentRefuelDetailResponse.setSelectedStationID(pendingTransactionBean.getStationBean().getStationID());
	    					// TODO - il nome della stazione non viene restituito
	    					paymentRefuelDetailResponse.setSelectedStationName("");
	    					paymentRefuelDetailResponse.setSelectedStationAddress(pendingTransactionBean.getStationBean().getAddress());
	    					paymentRefuelDetailResponse.setSelectedStationLatitude(pendingTransactionBean.getStationBean().getLatitude());
	    					paymentRefuelDetailResponse.setSelectedStationLongitude(pendingTransactionBean.getStationBean().getLongitude());
	    					paymentRefuelDetailResponse.setSelectedPumpID(pendingTransactionBean.getPumpID());
	    					paymentRefuelDetailResponse.setSelectedPumpNumber(pendingTransactionBean.getPumpNumber());
	    					paymentRefuelDetailResponse.setSelectedPumpFuelType(pendingTransactionBean.getProductDescription());
	    					
	    					userTransaction.commit();
	    					
	    					return paymentRefuelDetailResponse;
    					}
    				}
    				else {
    					
    					// Si estrae lo stato pi� recente associato alla transazione
    					Integer lastSequenceID = 0;
    					TransactionStatusHistoryBean lastTransactionStatusBean = null;

    					Set<TransactionStatusHistoryBean> transactionStatusHistoryData = transactionHistoryBean.getTransactionStatusHistoryBeanData();

    					for( TransactionStatusHistoryBean transactionStatusHistoryBean : transactionStatusHistoryData ) {

    						Integer transactionStatusBeanSequenceID = transactionStatusHistoryBean.getSequenceID();
    						if ( lastSequenceID < transactionStatusBeanSequenceID ) {
    							lastSequenceID = transactionStatusBeanSequenceID;
    							lastTransactionStatusBean = transactionStatusHistoryBean;
    						}
    					}
    					
    					paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
    					paymentRefuelDetailResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_DETAIL_SUCCESS);
    					paymentRefuelDetailResponse.setRefuelID(transactionHistoryBean.getTransactionID());
    					paymentRefuelDetailResponse.setStatus(lastTransactionStatusBean.getStatus());
    					paymentRefuelDetailResponse.setSubStatus(lastTransactionStatusBean.getSubStatus());
    					paymentRefuelDetailResponse.setUseVoucher(transactionHistoryBean.getUseVoucher());
    					paymentRefuelDetailResponse.setDate(lastTransactionStatusBean.getTimestamp());
    					paymentRefuelDetailResponse.setInitialAmount(transactionHistoryBean.getInitialAmount());
    					paymentRefuelDetailResponse.setFinalAmount(transactionHistoryBean.getFinalAmount());
    					paymentRefuelDetailResponse.setFuelAmount(transactionHistoryBean.getFuelAmount());
    					paymentRefuelDetailResponse.setFuelQuantity(transactionHistoryBean.getFuelQuantity());

    					paymentRefuelDetailResponse.setBankTransactionID(transactionHistoryBean.getBankTansactionID());
    					paymentRefuelDetailResponse.setShopLogin(transactionHistoryBean.getShopLogin());
    					paymentRefuelDetailResponse.setCurrency(transactionHistoryBean.getCurrency());
    					String maskedPan = PanHelper.maskPan(transactionHistoryBean.getToken());
    					paymentRefuelDetailResponse.setMaskedPan(maskedPan);
    					paymentRefuelDetailResponse.setAuthCode(transactionHistoryBean.getAuthorizationCode());

    					paymentRefuelDetailResponse.setSelectedStationID(transactionHistoryBean.getStationBean().getStationID());
    					// TODO - il nome della stazione non viene restituito
    					paymentRefuelDetailResponse.setSelectedStationName("");
    					paymentRefuelDetailResponse.setSelectedStationAddress(transactionHistoryBean.getStationBean().getAddress());
    					paymentRefuelDetailResponse.setSelectedStationLatitude(transactionHistoryBean.getStationBean().getLatitude());
    					paymentRefuelDetailResponse.setSelectedStationLongitude(transactionHistoryBean.getStationBean().getLongitude());
    					paymentRefuelDetailResponse.setSelectedPumpID(transactionHistoryBean.getPumpID());
    					paymentRefuelDetailResponse.setSelectedPumpNumber(transactionHistoryBean.getPumpNumber());
    					paymentRefuelDetailResponse.setSelectedPumpFuelType(transactionHistoryBean.getProductDescription());

    					userTransaction.commit();
    					
    					return paymentRefuelDetailResponse;
    				}
    			}
    		}
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED retrieve refuel payment detail with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
