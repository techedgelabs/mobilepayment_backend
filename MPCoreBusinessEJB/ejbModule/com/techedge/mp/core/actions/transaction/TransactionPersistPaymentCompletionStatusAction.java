package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.RefuelingServiceRemote;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.CRMSfNotifyEvent;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.EsPromotionBean;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.PrePaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.RefuelDetailConverter;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.crm.EVENT_TYPE;
import com.techedge.mp.core.business.utilities.crm.PaymentModeUtil;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.LoadLoyaltyCreditsResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionPersistPaymentCompletionStatusAction {

    @Resource
    private EJBContext             context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager          em;

    @EJB
    private LoggerService          loggerService;

    @EJB
    private CRMService             crmService;

    @EJB
    private RefuelingServiceRemote refuelingService;
    
    @EJB
    private ParametersService parametersService;
    
    private final String 				PARAM_CRM_SF_ACTIVE="CRM_SF_ACTIVE";

    public TransactionPersistPaymentCompletionStatusAction() {}

    public String execute(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId, Double effectiveAmount,
            Boolean receiptEmailActive, EmailSenderRemote emailSender, RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, 
            UserCategoryService userCategoryService, String proxyHost, String proxyPort, String proxyNoHosts, StringSubstitution stringSubstitution, FidelityServiceRemote fidelityService) throws EJBException {
        Boolean isNewFlow = false;
        Boolean isNewAcquirerFlow = false;
        Boolean isGuestFlow = false;
        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Ricerca la transazione associata al transactionID
            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);

            if (transactionBean == null) {

                // Transaction not found
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionID, null, "Transaction not found");

                userTransaction.commit();

                return StatusHelper.PERSIST_PAYMENT_COMPLETION_STATUS_ERROR;
            }

            PrePaidLoadLoyaltyCreditsBean crmPrePaidLoadLoyaltyCredits = null;

            // Crea il nuovo stato
            Integer sequenceID = 0;
            Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
            for (TransactionStatusBean transactionStatusBean : transactionStatusData) {
                Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                if (sequenceID < transactionStatusBeanSequenceID) {
                    sequenceID = transactionStatusBeanSequenceID;
                }
            }
            sequenceID = sequenceID + 1;

            Date now = new Date();
            Timestamp timestamp = new java.sql.Timestamp(now.getTime());
            String requestID = String.valueOf(now.getTime());

            TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

            transactionStatusBean.setTransactionBean(transactionBean);
            transactionStatusBean.setSequenceID(sequenceID);
            transactionStatusBean.setTimestamp(timestamp);
            transactionStatusBean.setRequestID(requestID);

            Integer eventSequenceID = 0;
            Set<TransactionEventBean> transactionEventData = transactionBean.getTransactionEventBeanData();
            for (TransactionEventBean transactionEventBean : transactionEventData) {
                Integer transactionEventBeanSequenceID = transactionEventBean.getSequenceID();
                if (eventSequenceID < transactionEventBeanSequenceID) {
                    eventSequenceID = transactionEventBeanSequenceID;
                }
            }
            eventSequenceID = eventSequenceID + 1;

            TransactionEventBean transactionEventBean = new TransactionEventBean();
            transactionEventBean.setSequenceID(eventSequenceID);
            transactionEventBean.setTransactionBean(transactionBean);
            transactionEventBean.setTransactionResult(statusCode.toUpperCase());
            transactionEventBean.setErrorCode(errorCode);
            transactionEventBean.setErrorDescription(errorMessage);

            if (effectiveAmount == 0.0) {
                if (subStatusCode.equals(StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_CONSUME_VOUCHER_FAULT)) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionID, null,
                            "effectiveAmount = 0.0  -->  inserita riga MOV con risultato KO con amount " + transactionBean.getInitialAmount());

                    transactionEventBean.setEventType("MOV");
                    transactionEventBean.setEventAmount(transactionBean.getInitialAmount());
                }
                else {
                    transactionEventBean.setEventType("CAN");
                    transactionEventBean.setEventAmount(transactionBean.getInitialAmount());
                }
            }
            else {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionID, null,
                        "effectiveAmount > 0.0  -->  inserita riga MOV con amount " + effectiveAmount);

                transactionEventBean.setEventType("MOV");
                transactionEventBean.setEventAmount(effectiveAmount);
            }

            Double totalAmount = effectiveAmount;

            isNewFlow = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_PAYMENT_FLOW.getCode());
            isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            isGuestFlow = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.GUEST_FLOW.getCode());
            Boolean isBusiness = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.BUSINESS.getCode());
            Boolean isRefuelingOAuth2 = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.UTENTI_REFUELING_OAUTH2.getCode());
            
            if (isNewFlow) {

                System.out.println("Rilevato nuovo flusso di pagamento: totalAmount = effectiveAmount");
            }
            else {

                if ((isNewAcquirerFlow || isGuestFlow) && transactionBean.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {

                    System.out.println("Rilevato flusso di pagamento con nuovo acquirer: totalAmount = voucher consumati");

                    totalAmount = 0.0;
                }
                else {

                    System.out.println("Rilevato vecchio flusso di pagamento: totalAmount = effectiveAmount + voucher consumati");
                }

                if (transactionBean.getUseVoucher() && transactionBean.getPrePaidConsumeVoucherBeanList().size() > 0) {
                    for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {
                        if (prePaidConsumeVoucherBean.getOperationType().equals("CONSUME") && prePaidConsumeVoucherBean.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {
                            totalAmount = totalAmount + prePaidConsumeVoucherBean.getTotalConsumed();
                        }
                    }
                }
            }

            if (statusCode.toLowerCase().equals("ok")) {
                
                LoyaltyCardBean loyaltyCardBean = transactionBean.getUserBean().getVirtualLoyaltyCard();

                if (loyaltyCardBean != null && !isBusiness) {

                    // Caricamento punti in modalit� prepaid

                    System.out.println("Caricamento punti carta fedelt�");

                    String refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_PREPAY;

                    List<ProductDetail> productList = new ArrayList<ProductDetail>(0);

                    ProductDetail productDetail = new ProductDetail();
                    productDetail.setAmount(transactionBean.getFinalAmount());

                    String productID = transactionBean.getProductID();

                    System.out.println("trovato product id: " + productID);

                    String productCode = this.getProductCode(productID);
                    productDetail.setProductCode(productCode);

                    System.out.println("trovato product code: " + productCode);

                    productDetail.setQuantity(transactionBean.getFuelQuantity());

                    productList.add(productDetail);

                    System.out.println("Trovata carta fedelt�");

                    // Chimamata al servizio loadLoyaltyCredits

                    String operationID = new IdGenerator().generateId(16).substring(0, 33);
                    PartnerType partnerType = PartnerType.MP;
                    Long requestTimestamp = new Date().getTime();
                    String stationID = transactionBean.getStationBean().getStationID();
                    String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;

                    Long paymentMethodId = transactionBean.getPaymentMethodId();
                    String paymentMethodType = transactionBean.getPaymentMethodType();
                    String BIN = "";

                    if (paymentMethodId != null && paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {

                        PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(em, paymentMethodId, paymentMethodType);

                        if (paymentInfoBean != null) {

                            BIN = paymentInfoBean.getCardBin();

                            if (BIN != null && QueryRepository.findCardBinExists(em, BIN)) {
                                System.out.println("BIN valido (" + BIN + ")");
                                paymentMode = FidelityConstants.PAYMENT_METHOD_ENI_CARD;
                            }
                            else {
                                System.out.println("BIN Non Valido (" + BIN + ")");
                            }
                        }
                        else {
                            System.out.println("paymentInfoBean null");
                        }
                    }
                    else {
                        System.out.println("paymentMethodId null o pagato con voucher");
                    }

                    PrePaidLoadLoyaltyCreditsBean prePaidLoadLoyaltyCreditsBean = new PrePaidLoadLoyaltyCreditsBean();
                    prePaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                    prePaidLoadLoyaltyCreditsBean.setOperationType("LOAD");
                    prePaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                    prePaidLoadLoyaltyCreditsBean.setEanCode(loyaltyCardBean.getEanCode());

                    try {

                        System.out.println("Chiamata servizio di carico punti");

                        LoadLoyaltyCreditsResult loadLoyaltyCreditsResult = fidelityService.loadLoyaltyCredits(operationID, transactionID, stationID, loyaltyCardBean.getPanCode(),
                                BIN, refuelMode, paymentMode, "it", partnerType, requestTimestamp, "", productList);

                        System.out.println("Risposta servizio di carico punti: " + loadLoyaltyCreditsResult.getStatusCode() + " (" + loadLoyaltyCreditsResult.getMessageCode()
                                + ")");

                        prePaidLoadLoyaltyCreditsBean.setBalance(loadLoyaltyCreditsResult.getBalance());
                        prePaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                        prePaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                        prePaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                        prePaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                        prePaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                        prePaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                        prePaidLoadLoyaltyCreditsBean.setCsTransactionID(loadLoyaltyCreditsResult.getCsTransactionID());
                        prePaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                        prePaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                        prePaidLoadLoyaltyCreditsBean.setMessageCode(loadLoyaltyCreditsResult.getMessageCode());
                        prePaidLoadLoyaltyCreditsBean.setStatusCode(loadLoyaltyCreditsResult.getStatusCode());
                        prePaidLoadLoyaltyCreditsBean.setWarningMsg(loadLoyaltyCreditsResult.getWarningMsg());

                        prePaidLoadLoyaltyCreditsBean.setTransactionBean(transactionBean);

                        transactionBean.getPrePaidLoadLoyaltyCreditsBeanList().add(prePaidLoadLoyaltyCreditsBean);

                        crmPrePaidLoadLoyaltyCredits = prePaidLoadLoyaltyCreditsBean;
                        //em.persist(postPaidLoadLoyaltyCreditsBean);
                        //em.merge(poPTransactionBean);
                    }
                    catch (Exception e) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error loading loyalty credits");

                        prePaidLoadLoyaltyCreditsBean.setStatusCode(StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED);
                        prePaidLoadLoyaltyCreditsBean.setMessageCode("OPERAZIONE DI CARICAMENTO DA VERIFICARE (" + e.getMessage() + ")");
                        prePaidLoadLoyaltyCreditsBean.setTransactionBean(transactionBean);
                        transactionBean.getPrePaidLoadLoyaltyCreditsBeanList().add(prePaidLoadLoyaltyCreditsBean);
                        transactionBean.setLoyaltyReconcile(true);
                    }

                    /*
                     * TransactionStatusBean loyaltyTransactionStatusBean = new TransactionStatusBean();
                     * 
                     * transactionStatusBean.setTransactionBean(transactionBean);
                     * transactionStatusBean.setSequenceID(sequenceID);
                     * transactionStatusBean.setTimestamp(timestamp);
                     * transactionStatusBean.setRequestID(requestID);
                     * 
                     * oldStatus = newStatus;
                     * newStatus = StatusHelper.POST_PAID_STATUS_CREDITS_LOAD;
                     * sequenceID += 1;
                     * eventTimestamp = new Timestamp(new Date().getTime());
                     * transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY;
                     * 
                     * postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp,
                     * transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);
                     * 
                     * em.persist(postPaidTransactionEventBean);
                     */
                }
                
                /************************************************************/
                /* Impostazione flag toBeProcessed per promo Vodafone Black */
                
                // Se per l'utente � presente una riga nella tabella ES_PROMOTION
                // con intervallo di valifit� che comprende la data di creazione
                // della transazione bisogna impostare il campo toBeProcessed
                // a true
                
                EsPromotionBean esPromotionBean = QueryRepository.findEsPromotionToBeProcessedWithNullVoucherCodeByDate(em, transactionBean.getUserBean(), transactionBean.getCreationTimestamp());
                if (esPromotionBean != null) {
                    System.out.println("Trovata riga EsPromotionBean con flag da impostare");
                    esPromotionBean.setToBeProcessed(Boolean.TRUE);
                    em.merge(esPromotionBean);
                }
                
                /************************************************************/

                transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_TRANSACTION_CLOSED);
                transactionStatusBean.setSubStatus(subStatusCode);
                transactionStatusBean.setSubStatusDescription("Payment transaction succesful");

                //transactionBean.setFinalAmount(effectiveAmount);
                transactionBean.setFinalAmount(totalAmount);
                transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
                
                Boolean isRefueling = false;
                if (transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_REFUELING) ||
                    transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_REFUELING_NEW_ACQUIRER) ||
                    transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_REFUELING_OAUTH2)) {
                    isRefueling = true;
                }
                
                if (!transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_GUEST) && !isBusiness && !isRefueling) {
                	
                	String crmSfActive="0";
                	try {
                		crmSfActive = parametersService.getParamValue(PARAM_CRM_SF_ACTIVE);
            		} catch (ParameterNotFoundException e) {
            			e.printStackTrace();
            		}
                	System.out.println("crmSfActive: "+crmSfActive);

                    //chiamata al servizio di notifica per transazioni refueling

                    final String fiscalCode = transactionBean.getUserBean().getPersonalDataBean().getFiscalCode();
                    Date timestamp2 = transactionBean.getCreationTimestamp();
                    String surname = transactionBean.getUserBean().getPersonalDataBean().getLastName();
                    //String firstName = transactionBean.getUserBean().getPersonalDataBean().getFirstName();
                    String pv = transactionBean.getStationBean().getStationID();
                    String product = transactionBean.getProductDescription();
                    final Long sourceID = transactionBean.getId();
                    final CRMEventSourceType sourceType = CRMEventSourceType.PREPAID;
                    boolean flagPayment = true;
                    Integer loyaltyCredits = 0;
                    Double refuelQuantity = transactionBean.getFuelQuantity();
                    Double amount = transactionBean.getFinalAmount();
                    //recupero la modalita di pagamento prepaid
                    String paymentMode=PaymentModeUtil.getPaymentModePrepaid(transactionBean);
                    String refuelMode = transactionBean.getRefuelMode();
                    boolean flagNotification = false;
                    boolean flagCreditCard = false;
                    String cardBrand = null;
                    ClusterType cluster = ClusterType.ENIPAY;
                    boolean privacyFlag1 = false;
                    boolean privacyFlag2 = false;

                    if (crmPrePaidLoadLoyaltyCredits != null) {
                        loyaltyCredits = crmPrePaidLoadLoyaltyCredits.getCredits();
                    }

                    Set<TermsOfServiceBean> listTermOfService = transactionBean.getUserBean().getPersonalDataBean().getTermsOfServiceBeanData();
                    if (listTermOfService == null) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "flagPrivacy null");
                    }
                    else {
                        for (TermsOfServiceBean item : listTermOfService) {
                            if (item.getKeyval().equals("NOTIFICATION_NEW_1")) {
                                flagNotification = item.getAccepted();
                                break;
                            }
                        }
                        for (TermsOfServiceBean item : listTermOfService) {
                            if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                                privacyFlag1 = item.getAccepted();
                                break;
                            }
                        }
                        for (TermsOfServiceBean item : listTermOfService) {
                            if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                                privacyFlag2 = item.getAccepted();
                                break;
                            }
                        }
                    }

                    if (transactionBean.getUserBean().getPaymentMethodTypeCreditCard() != null) {
                        flagCreditCard = true;
                        cardBrand = transactionBean.getUserBean().getPaymentMethodTypeCreditCard().getBrand();
                    }

                    if (transactionBean.getUserBean().getVirtualizationCompleted()) {
                        cluster = ClusterType.YOUENI;
                    }
                    
                    //Nuova implementazione sf
                    //recupero parametro CRM_SF_ACTIVE per la gestione della nuova chiamata al CRM SF
                    //0 disattivato invio sf
                    //1 attivo solo invio sf
                    //2 attivi entrambi i crm ibm,sf
                   
                    if(crmSfActive.equalsIgnoreCase("0")||crmSfActive.equalsIgnoreCase("2")){

	                    final UserProfile userProfile = UserProfile.getUserProfileForEventRefueling(fiscalCode, timestamp2, surname, pv, product, flagPayment, loyaltyCredits,
	                            refuelQuantity, amount, refuelMode, flagNotification, flagCreditCard, cardBrand, cluster);
	
	                    new Thread(new Runnable() {
	
	                        @Override
	                        public void run() {
	                            String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.REFUELING, userProfile, sourceID, sourceType); 
	
	                            if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
	                                System.err.println("Error in send crm notification (" + crmResponse + ")");
	                            }
	                        }
	                    }, "executeBatchGetOffers (TransactionPersistPaymentCompletionStatusAction)").start();
                    }
                    
                    if(crmSfActive.equalsIgnoreCase("1")||crmSfActive.equalsIgnoreCase("2")){
                    	
                    	System.out.println("##################################################");
                    	System.out.println("fiscalCode:" + fiscalCode);
                    	System.out.println("timestamp:" + timestamp);
                    	System.out.println("pv:" +pv );
                    	System.out.println("product:" + product);
                    	System.out.println("flagPayment:" + flagPayment);
                    	System.out.println("loyaltyCredits:" +loyaltyCredits );
                    	System.out.println("refuelQuantity:" +refuelQuantity );
                    	System.out.println("refuelMode:" + refuelMode);
                    	System.out.println("surname:" +surname );
                    	System.out.println("getSecurityDataEmail():" +transactionBean.getUserBean().getPersonalDataBean().getSecurityDataEmail());
                    	System.out.println("getBirthDate():" + transactionBean.getUserBean().getPersonalDataBean().getBirthDate());
                    	System.out.println("flagNotification:" + flagNotification);
                    	System.out.println("flagCreditCard:" + flagCreditCard);
                    	System.out.println("cardBrand:" + cardBrand);
                    	System.out.println("cluster.getValue():" +cluster.getValue() );
                    	System.out.println("amount:" +amount );
                    	System.out.println("paymentMode:" +paymentMode );
                    	System.out.println("getActiveMobilePhone():" +transactionBean.getUserBean().getActiveMobilePhone() );
                    	
                    	String panCode="";
                    	if(transactionBean.getUserBean().getVirtualLoyaltyCard()!=null){
                    		System.out.println("getPanCode():" +transactionBean.getUserBean().getVirtualLoyaltyCard().getPanCode() );
                    		panCode=transactionBean.getUserBean().getVirtualLoyaltyCard().getPanCode();
                    	}else
                    		System.out.println("getPanCode():null");
                    	
                    	System.out.println("EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue():" + EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue());
                    	System.out.println("##################################################");
                    	
                    	
                    	String requestId = new IdGenerator().generateId(16).substring(0, 32);
                    	/*:TODO da verificare requestId */
                    	final CRMSfNotifyEvent crmSfNotifyEvent= new CRMSfNotifyEvent(requestId, fiscalCode, timestamp, pv, product, flagPayment, 
                    			loyaltyCredits, refuelQuantity, refuelMode, "", surname, transactionBean.getUserBean().getPersonalDataBean().getSecurityDataEmail(), 
                    			transactionBean.getUserBean().getPersonalDataBean().getBirthDate(), flagNotification, flagCreditCard, cardBrand==null?"":cardBrand, 
                    			cluster.getValue(), amount, privacyFlag1, privacyFlag2, transactionBean.getUserBean().getActiveMobilePhone(), panCode, 
                    			EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue(), "", "", paymentMode);
    	    
    	                new Thread(new Runnable() {
    	                    
    	                    @Override
    	                    public void run() {
    	                    	NotifyEventResponse crmResponse = crmService.sendNotifySfEventOffer(crmSfNotifyEvent.getRequestId(), crmSfNotifyEvent.getFiscalCode(), crmSfNotifyEvent.getDate(), 
    	                        		crmSfNotifyEvent.getStationId(), crmSfNotifyEvent.getProductId(), crmSfNotifyEvent.getPaymentFlag(), crmSfNotifyEvent.getCredits(), 
    	                        		crmSfNotifyEvent.getQuantity(), crmSfNotifyEvent.getRefuelMode(), crmSfNotifyEvent.getFirstName(), crmSfNotifyEvent.getLastName(), 
    	                        		crmSfNotifyEvent.getEmail(), crmSfNotifyEvent.getBirthDate(), crmSfNotifyEvent.getNotificationFlag(), crmSfNotifyEvent.getPaymentCardFlag(), 
    	                        		crmSfNotifyEvent.getBrand(), crmSfNotifyEvent.getCluster(),crmSfNotifyEvent.getAmount(), crmSfNotifyEvent.getPrivacyFlag1(), 
    	                        		crmSfNotifyEvent.getPrivacyFlag2(), crmSfNotifyEvent.getMobilePhone(), crmSfNotifyEvent.getLoyaltyCard(), crmSfNotifyEvent.getEventType(), 
    	                        		crmSfNotifyEvent.getParameter1(), crmSfNotifyEvent.getParameter2(), crmSfNotifyEvent.getPaymentMode());
    	    
    	                    	System.out.println("crmResponse succes:" + crmResponse.getSuccess());
    		                      System.out.println("crmResponse errorCode:" + crmResponse.getErrorCode());
    		                      System.out.println("crmResponse message:" + crmResponse.getMessage());
    		                      System.out.println("crmResponse requestId:" + crmResponse.getRequestId());
    	                    }
    	                }, "executeBatchGetOffersSF (TransactionPersistPaymentCompletionStatusAction)").start();
                    }

                    final String fiscalcode = transactionBean.getUserBean().getPersonalDataBean().getFiscalCode();
                    final String inputRequestId = (new Long(now.getTime())).toString();

                    if (transactionBean.getUserBean().getSource() != null && transactionBean.getUserBean().getSource().equals(User.USER_SOURCE_ENJOY)) {
                        if (transactionBean.getUserBean().getSourceNotified() == null || !transactionBean.getUserBean().getSourceNotified()) {
                            new Thread(new Runnable() {
    
                                @Override
                                public void run() {
                                    String notifySubscriptionResponse = refuelingService.notifySubscription(inputRequestId, fiscalcode);
                                    System.out.println("**** INFO from TransactionPersistPaymentCompletitionStatusAction ***** Calling notifySubscription for user enjoy notification ("
                                            + notifySubscriptionResponse + ")");
                                    if (!notifySubscriptionResponse.equals(StatusHelper.REFUELING_SUBSCRIPTION_SUCCESS)) {
                                        System.err.println("Error in sending subscription notification (" + notifySubscriptionResponse + ")");
                                    }
                                }
                            }, "executeBatchNotifySubscription (TransactionPersistPaymentCompletitionStatusAction)").start();

                        }
                        else {
                            System.out.println("Notifica gi� inviata per l'utente con codice fiscale: " + fiscalCode);
                        }
                    }
                    else {
                        System.out.println("Notifica Refueling non inviata. L'utente non � di tipo ENJOY");
                    }
                }
                
                if (!transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_GUEST) && !isBusiness) {
                    
                    String srcTransactionID = transactionBean.getSrcTransactionID();
                    if (srcTransactionID != null && !srcTransactionID.equals("")) {

                        System.out.println("Trovata transazione refueling -> invio notifica");

                        RefuelDetail fuelDeatail = RefuelDetailConverter.createRefuelDetail(transactionBean);

                        String status = transactionBean.getLastTransactionStatus().getStatus();

                        System.out.println("Status: " + status);

                        String mpTransactionStatus = "END_REFUELING";

                        System.out.println("Inizio chiamata servizio di notifica per Rifornimento completato con successo");

                        Runnable r = new TransactionRefuelSendNotification(requestID, srcTransactionID, transactionID, mpTransactionStatus, fuelDeatail,
                                refuelingNotificationService, refuelingNotificationServiceOAuth2, isRefuelingOAuth2, loggerService);

                        new Thread(r).start();
                    }
                }
                else {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Notifica non inviata per utente guest e business");
                }
            }
            else {

                // TODO mappare tutti gli stati della risposta GestPay
                transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_TRANSACTION_NOT_CLOSED);
                transactionStatusBean.setSubStatus(subStatusCode);
                transactionStatusBean.setSubStatusDescription("Fault (application error or system error)");

                transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT);

                //chiamata al servizio di notifica per transazioni refueling

                String srcTransactionID = transactionBean.getSrcTransactionID();
                if (srcTransactionID != null && !srcTransactionID.equals("")) {

                    System.out.println("Trovata transazione refueling -> invio notifica");

                    RefuelDetail fuelDeatail = RefuelDetailConverter.createRefuelDetail(transactionBean);

                    String status = transactionBean.getLastTransactionStatus().getStatus();

                    System.out.println("Status: " + status);

                    String mpTransactionStatus = "RECONCILIATION";

                    System.out.println("Inizio chiamata servizio di notifica per Errore movimentazione importo");

                    Runnable r = new TransactionRefuelSendNotification(requestID, srcTransactionID, transactionID, mpTransactionStatus, fuelDeatail, refuelingNotificationService,
                            refuelingNotificationServiceOAuth2, isRefuelingOAuth2, loggerService);

                    new Thread(r).start();
                }
            }

            UserBean userBean = transactionBean.getUserBean();

            em.merge(transactionBean);

            em.persist(transactionStatusBean);
            em.persist(transactionEventBean);

            if (receiptEmailActive == true
                    && (userBean.getUserType() == User.USER_TYPE_CUSTOMER || userBean.getUserType() == User.USER_TYPE_VOUCHER_TESTER || userBean.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER)
                    && !isBusiness) {

                // Invio della mail con lo scontrino del rifornimento
                if (emailSender != null) {
                    Email.sendPrePaidSummary(emailSender, transactionBean, transactionStatusBean, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
                }
            }

            userTransaction.commit();

            return StatusHelper.PERSIST_PAYMENT_COMPLETION_STATUS_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED persisting payment completion status with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", transactionID, null, message);

            throw new EJBException(ex2);
        }
    }

    /*
     * private RefuelDetail createRefuelDetail(TransactionBean transactionBean) {
     * 
     * //RefuelDetail di MPRefuelingAdapterEJBClient
     * RefuelDetail fuelDeatail = new RefuelDetail();
     * 
     * fuelDeatail.setAmount(transactionBean.getFinalAmount());
     * 
     * fuelDeatail.setTimestampStartRefuel(convertDateToString(transactionBean.getCreationTimestamp()));
     * fuelDeatail.setTimestampEndRefuel(convertDateToString(transactionBean.getEndRefuelTimestamp()));
     * 
     * fuelDeatail.setAuthorizationCode(transactionBean.getAuthorizationCode());
     * 
     * fuelDeatail.setFuelQuantity(transactionBean.getFuelQuantity());
     * fuelDeatail.setProductID(transactionBean.getProductID());
     * fuelDeatail.setProductDescription(transactionBean.getProductDescription());
     * 
     * RefuelingFuelTypeMapping rftm = new RefuelingFuelTypeMapping();
     * fuelDeatail.setFuelType(rftm.getFuelType(transactionBean.getProductID()));
     * return fuelDeatail;
     * }
     * 
     * private String convertDateToString(Date refuelDate) {
     * 
     * if (refuelDate == null) {
     * return "";
     * }
     * 
     * String dateString = null;
     * SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
     * 
     * try {
     * dateString = sdfr.format(refuelDate);
     * }
     * catch (Exception ex) {
     * ex.printStackTrace();
     * }
     * 
     * return dateString;
     * }
     */

    private String getProductCode(String productID) {

        if (productID == null || productID.isEmpty()) {
            return null;
        }

        if (productID.equals("SP")) {
            return FidelityConstants.PRODUCT_CODE_SP;
        }

        if (productID.equals("GG")) {
            return FidelityConstants.PRODUCT_CODE_GASOLIO;
        }

        if (productID.equals("BS")) {
            return FidelityConstants.PRODUCT_CODE_BLUE_SUPER;
        }

        if (productID.equals("BD")) {
            return FidelityConstants.PRODUCT_CODE_BLUE_DIESEL;
        }

        if (productID.equals("MT")) {
            return FidelityConstants.PRODUCT_CODE_METANO;
        }

        if (productID.equals("GP")) {
            return FidelityConstants.PRODUCT_CODE_GPL;
        }

        if (productID.equals("AD")) {
            return null;
        }

        return null;
    }

}
