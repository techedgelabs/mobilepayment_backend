package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionPersistUndoRefuelStatusAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public TransactionPersistUndoRefuelStatusAction() {
    }

    
    public String execute(
    		String ticketID,
			String requestID,
			String refuelID) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_UPDATE_INVALID_TICKET;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if ( userBean == null ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_UPDATE_INVALID_TICKET;
    		}
    		
    		
    		// Verifica lo stato dell'utente
    		Integer userStatus = userBean.getUserStatus();
    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
    			
    			// Un utente che si trova in questo stato non pu� invocare questo servizio
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to persist undo refuel status in status " + userStatus );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_UPDATE_UNAUTHORIZED;
    		}
    		
    		
    		// Ricerca la transazione attiva
    		List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);
		    
		    if ( transactionBeanList.isEmpty() ) {
    			
    			// Non esiste una transazione attiva
		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction not found" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.UNDO_REFUEL_FAILURE;
    		}
		    else {

				TransactionBean pendingTransactionBean = null;

				for( TransactionBean transactionBean : transactionBeanList ) {

					// Si estrae la prima
					pendingTransactionBean = transactionBean;
				}

				if ( pendingTransactionBean == null ) {
					
					// Non esiste una transazione attiva
			    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Id wrong in undo refuel" );
					
	    			userTransaction.commit();
	    			
	    			return ResponseHelper.UNDO_REFUEL_ID_WRONG;
				}
				else {

					// Si verificano gli stati della transazione

					for ( TransactionStatusBean transactionStatusBean : pendingTransactionBean.getTransactionStatusBeanData() ) {

						if ( transactionStatusBean.getStatus().equals(StatusHelper.STATUS_DELETE_REFUELING_REQUEST)) {

							// E' gi� presente una richiesta di annullamento rifornimento
							this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Found status DELETE_REFUELING_REQUEST" );
							
			    			userTransaction.commit();
			    			
			    			return ResponseHelper.UNDO_REFUEL_FAILURE;
						}

						if ( transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PUMP_ENABLED)) {

							// Una transazione che ha gi� raggiunto lo stato PUMP_ENABLE non pu� pi� essere cancellata
							this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to cancel transaction in status PUMP_ENABLED" );
							
			    			userTransaction.commit();
			    			
			    			return ResponseHelper.UNDO_REFUEL_FAILURE;
						}

						if ( transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PUMP_NOT_ENABLED) && !transactionStatusBean.getSubStatus().equals(StatusHelper.SUBSTATUS_PUMP_BUSY)) {

							// Una transazione che ha gi� raggiunto lo stato PUMP_NOT_ENABLED non pu� pi� essere cancellata
							this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to cancel transaction in status PUMP_NOT_ENABLED with subStatus " + transactionStatusBean.getSubStatus() );
							
			    			userTransaction.commit();
			    			
							return ResponseHelper.UNDO_REFUEL_FAILURE;
						}
					}

					// Crea il nuovo stato
					Integer sequenceID = 0;
					Set<TransactionStatusBean> transactionStatusData = pendingTransactionBean.getTransactionStatusBeanData();
					for( TransactionStatusBean transactionStatusBean : transactionStatusData ) {
						Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
						if ( sequenceID < transactionStatusBeanSequenceID ) {
							sequenceID = transactionStatusBeanSequenceID;
						}
					}
					sequenceID = sequenceID + 1;

					Date now = new Date();
					Timestamp timestamp = new java.sql.Timestamp(now.getTime());

					TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

					transactionStatusBean.setRequestID(requestID);
					transactionStatusBean.setSequenceID(sequenceID);
					transactionStatusBean.setStatus(StatusHelper.STATUS_DELETE_REFUELING_REQUEST);
					transactionStatusBean.setSubStatus("");
					transactionStatusBean.setSubStatusDescription("Undo refuel request");
					transactionStatusBean.setTimestamp(timestamp);
					transactionStatusBean.setTransactionBean(pendingTransactionBean);

					em.persist(transactionStatusBean);
					
					userTransaction.commit();

					return ResponseHelper.UNDO_REFUEL_SUCCESS;
				}
			}
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED persist undo refuel status with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}