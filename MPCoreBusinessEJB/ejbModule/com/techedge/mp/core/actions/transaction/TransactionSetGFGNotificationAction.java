package com.techedge.mp.core.actions.transaction;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionSetGFGNotificationAction {

	@Resource
	private EJBContext context;

	@PersistenceContext( unitName = "CrudPU" )
	private EntityManager em;

	@EJB
	private LoggerService loggerService;


	public TransactionSetGFGNotificationAction() {
	}


	public String execute(String transactionID, boolean flag_value, String electronicInvoiceID) throws EJBException {

		UserTransaction userTransaction = context.getUserTransaction();

		try {
			userTransaction.begin();

			// Ricerca la transazione associata al transactionID
			TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);

			if (transactionBean == null) {

				// Transaction not found
				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionID, null, "Transaction not found" );

				userTransaction.commit();

				return StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED;
			}

			// TODO verificare lo stato della transazione

			if (flag_value){
			    
			    System.out.println("setGFGNotification true");
			    
				transactionBean.setGFGNotification(Boolean.TRUE);
			}
			else{
				
			    System.out.println("setGFGNotification false");
                
                transactionBean.setGFGNotification(Boolean.FALSE);
			}
			
			if (electronicInvoiceID != null && !electronicInvoiceID.trim().isEmpty()) {
	            System.out.println("setGfgElectronicInvoiceID: " + electronicInvoiceID);
                
	            transactionBean.setGFGElectronicInvoiceID(electronicInvoiceID);
			}
			

			em.merge(transactionBean);

			userTransaction.commit();

			return StatusHelper.GFG_NOTIFICATION_SUCCESS;

		}
		catch (Exception ex2) {

			try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String message = "FAILED setting GFG notification with message (" + ex2.getMessage() + ")";
			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", transactionID, null, message );

			throw new EJBException(ex2);
		}
	}
}