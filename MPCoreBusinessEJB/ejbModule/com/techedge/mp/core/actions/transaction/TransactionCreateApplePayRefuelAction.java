package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.TransactionService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.CreateApplePayRefuelResponse;
import com.techedge.mp.core.business.interfaces.CreateRefuelResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.UnavailabilityPeriod;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.ApplePayPurchaseVoucher;
import com.techedge.mp.core.business.voucher.PurchaseVoucher;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionCreateApplePayRefuelAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionCreateApplePayRefuelAction() {}

    public CreateApplePayRefuelResponse execute(String ticketID, String requestID, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Double amountVoucher, Boolean useVoucher, String currency, String paymentType, String applePayPKPaymentToken, String outOfRange,
            String refuelMode, String defaultShopLogin, String defaultAcquirerID, Integer pinCheckMaxAttempts, Integer maxStatusAttempts, Integer reconciliationMaxAttempts, 
            String serverName, Double voucherTransactionMinAmount, List<String> userBlockExceptionList, EmailSenderRemote emailSender, UserCategoryService userCategoryService, TransactionService transactionService, GPServiceRemote gpService,
            FidelityServiceRemote fidelityService, UnavailabilityPeriodService unavailabilityPeriodService, String proxyHost, String proxyPort, String proxyNoHosts)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();
            
            CreateApplePayRefuelResponse createApplePayRefuelResponse = new CreateApplePayRefuelResponse();
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ServerName: " + serverName);

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                createApplePayRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return createApplePayRefuelResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                createApplePayRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return createApplePayRefuelResponse;
            }
            
            if ( userBlockExceptionList.contains(userBean.getPersonalDataBean().getSecurityDataEmail()) ) {
                
                System.out.println(userBean.getPersonalDataBean().getSecurityDataEmail() + " presente in lista utenti con eccezioni blocco");
            }
            else {
                // Controllo su disponibilit� del servizio di creazione transazioni prepaid
                ServiceAvailabilityData serviceAvailabilityData = unavailabilityPeriodService.retrieveServiceAvailability("CREATE_TRANSACTION_PREPAID",  new Date());
                if ( serviceAvailabilityData != null ) {
                    
                 // Servizio non disponibile
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "CreateRefuelService unavailable");
    
                    userTransaction.commit();
    
                    createApplePayRefuelResponse.setStatusCode(serviceAvailabilityData.getStatusCode());
                    return createApplePayRefuelResponse;
                }
            }
            

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus);

                userTransaction.commit();

                createApplePayRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_UNAUTHORIZED);
                return createApplePayRefuelResponse;
            }

            // Controlla se l'utente ha gi� una transazione attiva
            List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);

            if (!transactionBeanList.isEmpty()) {

                // Esiste una transazione associata all'utente non ancora completata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction found");

                userTransaction.commit();

                createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
                return createApplePayRefuelResponse;
            }
            
            createApplePayRefuelResponse = this.createApplePayRefuelTransaction(
                    requestID,
                    userTransaction,
                    userBean,
                    applePayPKPaymentToken,
                    amount,
                    amountVoucher,
                    stationID,
                    pumpID,
                    pinCheckMaxAttempts,
                    defaultShopLogin,
                    defaultAcquirerID,
                    useVoucher,
                    pumpNumber,
                    productID,
                    productDescription,
                    currency,
                    paymentType,
                    serverName,
                    maxStatusAttempts,
                    reconciliationMaxAttempts,
                    outOfRange,
                    refuelMode,
                    voucherTransactionMinAmount,
                    userCategoryService,
                    emailSender,
                    transactionService,
                    gpService,
                    fidelityService, 
                    proxyHost,
                    proxyPort,
                    proxyNoHosts);

            return createApplePayRefuelResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED apple pay transaction creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
    
    
    private CreateApplePayRefuelResponse createApplePayRefuelTransaction (
            String requestID,
            UserTransaction userTransaction,
            UserBean userBean,
            String applePayPKPaymentToken,
            Double amount,
            Double amountVoucher,
            String stationID,
            String pumpID,
            Integer pinCheckMaxAttempts,
            String defaultShopLogin,
            String defaultAcquirerID,
            Boolean useVoucher,
            Integer pumpNumber,
            String productID,
            String productDescription,
            String currency,
            String paymentType,
            String serverName,
            Integer maxStatusAttempts,
            Integer reconciliationMaxAttempts,
            String outOfRange,
            String refuelMode,
            Double voucherTransactionMinAmount,
            UserCategoryService userCategoryService,
            EmailSenderRemote emailSender,
            TransactionService transactionService,
            GPServiceRemote gpService,
            FidelityServiceRemote fidelityService, 
            String proxyHost,
            String proxyPort,
            String proxyNoHosts) throws Exception {
        
        System.out.println("Utilizzo nuovo flusso voucher");
        
        CreateApplePayRefuelResponse createApplePayRefuelResponse = new CreateApplePayRefuelResponse();
        
        // Verifica lo stationID
        StationBean stationBean = QueryRepository.findStationBeanById(em, stationID);

        if (stationBean == null) {

            // Lo stationID inserito non corrisponde a nessuna stazione valida
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID);

            userTransaction.commit();

            createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createApplePayRefuelResponse;
        }
        
        
        // Verifica che l'utente abbia gi� associato il pin
        PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();
        
        if ( paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("") ) {
            
            // Errore Pin non ancora inserito
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");
        
            userTransaction.commit();
            
            createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createApplePayRefuelResponse;
        }
        
        
        double capAvailable = 0.0;
        
        if (userBean.getCapAvailable() != null) {
            capAvailable = userBean.getCapAvailable().doubleValue();
        }
        
        // Se l'utente ha passato in input un Apple Pay Token Payment Package, bisogna effettuare l'acquisto del voucher
        if ( applePayPKPaymentToken != null ) {
            
            System.out.println("Esecuzione transazione di acquisto voucher con pagamento Apple Pay");
            
            if ( defaultShopLogin == null || defaultShopLogin.equals("") ) {
                
                // Si � verificato un errore nell'acquisto del voucher
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "defaultShopLogin null");

                userTransaction.commit();

                createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_CREATE_VOUCHER);
                return createApplePayRefuelResponse;
            }
            
            ApplePayPurchaseVoucher applePayPurchaseVoucher = new ApplePayPurchaseVoucher(em, gpService, fidelityService, emailSender, proxyHost, proxyPort, proxyNoHosts);
            VoucherTransactionBean voucherTransactionBean = applePayPurchaseVoucher.buy(userBean, amountVoucher, defaultShopLogin, defaultAcquirerID, currency, applePayPKPaymentToken, 
                    paymentType, reconciliationMaxAttempts);
            
            if (voucherTransactionBean.getFinalStatusType().startsWith("ERROR_")) {
                
                // Si � verificato un errore nell'acquisto del voucher
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error create voucher " + voucherTransactionBean.getFinalStatusType());

                userTransaction.commit();

                createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_CREATE_VOUCHER);
                return createApplePayRefuelResponse;
            }
            else {
                
                // Voucher acquistato con successo
                System.out.println("Voucher acquistato con successo");
            }
        }
        
        // Genera un transactionID da utilizzare nella chiamata al servizio di preautorizzazione consumo voucher
        String transactionID = new IdGenerator().generateId(16).substring(0, 32);
        
        // Preautorizza credito voucher
        System.out.println("Preautorizzazione credito voucher");
        
        TransactionPreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucher = transactionService.preAuthorizationConsumeVoucherNoTransaction(
            userBean.getId(),
            transactionID,
            stationID,
            amount);
        
        if (preAuthorizationConsumeVoucher == null) {
            
            // Errore nella preautorizzazione dell'importo tramite voucher
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error in preAuthorizationConsumeVoucherNoTransaction: preAuthorizationConsumeVoucher null");
        
            userTransaction.commit();
            
            createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createApplePayRefuelResponse;
        }
        
        if ( preAuthorizationConsumeVoucher.getStatusCode().equals(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_PV_NOT_ENABLED) ) {
            
            // Errore nella preautorizzazione dell'importo tramite voucher
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error in preAuthorizationConsumeVoucherNoTransaction: statusCode " + preAuthorizationConsumeVoucher.getStatusCode());
        
            userTransaction.commit();
            
            createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PV_NOT_ENABLED);
            return createApplePayRefuelResponse;
        }
                
        if ( !preAuthorizationConsumeVoucher.getStatusCode().equals(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS) ) {
            
            // Errore nella preautorizzazione dell'importo tramite voucher
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error in preAuthorizationConsumeVoucherNoTransaction: statusCode " + preAuthorizationConsumeVoucher.getStatusCode());
        
            userTransaction.commit();
            
            createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createApplePayRefuelResponse;
        }
        else {
                
            if ( preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getStatusCode().equals(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER ) ||
                 preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getStatusCode().equals(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER2 )) {
                
                System.out.println("Importo voucher non sufficiente");
                /*
                if ( paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER) ) {
                    
                    Double maxAmount       = 0.0;
                    Double thresholdAmount = 0.0;
                    
                    if ( preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getAmount() != null ) {
                        maxAmount = preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getAmount();
                    }

                    if ( maxAmount >= amount ) {
                        
                        System.out.println("L'utente ha credito voucher sufficiente");
                    }
                    else {
                        
                        System.out.println("L'utente non ha credito voucher sufficiente");
                        
                        Double diff = amount - maxAmount;
                        
                        System.out.println("amount - total: " + diff.toString());
                        
                        thresholdAmount = Math.ceil(amount - maxAmount);
                        
                        System.out.println("Threshold: " + thresholdAmount.toString());
                        
                        if ( thresholdAmount < voucherTransactionMinAmount ) {
                            thresholdAmount = voucherTransactionMinAmount;
                            
                            System.out.println("Threshold upd: " + thresholdAmount.toString());
                        }
                    }
                    
                    // L'utente non ha sufficiente credito voucher
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error in preAuthorizationConsumeVoucherNoTransaction: credito voucher insufficiente, maxAmount: " + maxAmount.toString() + ", thresholdAmount: " + thresholdAmount.toString());
                    
                    userTransaction.commit();
                    
                    createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_INSUFFICIENT_VOUCHER_AMOUNT);
                    createApplePayRefuelResponse.setMaxAmount(maxAmount);
                    createApplePayRefuelResponse.setThresholdAmount(thresholdAmount);
                    return createApplePayRefuelResponse;
                }
                else {
                    
                    // L'utente ha usato un metodo di pagamento di tipo credit_card e non ha sufficiente credito voucher
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error in preAuthorizationConsumeVoucherNoTransaction: preautorizzazione voucher in errore con metodo di pagamento di tipo credit_card");
                    
                    userTransaction.commit();
                    
                    createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
                    return createApplePayRefuelResponse;
                }
                */
            }
        }
        
        
        System.out.println("Preautorizzazione credito voucher con esito positivo");

        String shopLogin  = defaultShopLogin;
        String acquirerID = "QUENIT"; //defaultAcquirerID;
        
        shopLogin  = "ENIPAY" + stationBean.getStationID();

        Date now = new Date();
        Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

        // Crea la transazione con i dati ricevuti in input
        TransactionBean transactionBean = new TransactionBean();
        transactionBean.setTransactionID(transactionID);
        transactionBean.setUserBean(userBean);
        transactionBean.setStationBean(stationBean);
        transactionBean.setUseVoucher(useVoucher);
        transactionBean.setCreationTimestamp(creationTimestamp);
        transactionBean.setEndRefuelTimestamp(null);
        transactionBean.setInitialAmount(amount);
        transactionBean.setFinalAmount(null);
        transactionBean.setFuelAmount(null);
        transactionBean.setFuelQuantity(null);
        transactionBean.setPumpID(pumpID);
        transactionBean.setPumpNumber(pumpNumber);
        transactionBean.setProductID(productID);
        transactionBean.setProductDescription(productDescription);
        transactionBean.setCurrency(currency);
        transactionBean.setShopLogin(shopLogin);
        transactionBean.setAcquirerID(acquirerID);
        transactionBean.setBankTansactionID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getPreAuthOperationID());
        //transactionBean.setToken(paymentInfoBean.getToken());
        transactionBean.setProductID(null);
        transactionBean.setPaymentType(paymentType);
        transactionBean.setFinalStatusType(null);
        transactionBean.setGFGNotification(false);
        transactionBean.setConfirmed(false);
        //transactionBean.setPaymentMethodId(paymentInfoBean.getId());
        //transactionBean.setPaymentMethodType(paymentInfoBean.getType());
        transactionBean.setServerName(serverName);
        transactionBean.setStatusAttemptsLeft(maxStatusAttempts);
        transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
        transactionBean.setOutOfRange(outOfRange);
        transactionBean.setRefuelMode(refuelMode);
        transactionBean.setVoucherReconciliation(false);
        
        transactionBean.setAuthorizationCode(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getPreAuthOperationID());

        // Crea lo stato iniziale della transazione
        TransactionStatusBean transactionStatusBean = new TransactionStatusBean();
        transactionStatusBean.setTransactionBean(transactionBean);
        transactionStatusBean.setSequenceID(1);
        transactionStatusBean.setTimestamp(creationTimestamp);
        transactionStatusBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
        transactionStatusBean.setSubStatus(null);
        transactionStatusBean.setSubStatusDescription("Started");
        transactionStatusBean.setRequestID(requestID);
        
        // Crea la riga di log di preautorizzazione voucher
        PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
        prePaidConsumeVoucherBean.setOperationID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getOperationID());
        prePaidConsumeVoucherBean.setRequestTimestamp(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getRequestTimestamp());
        prePaidConsumeVoucherBean.setOperationType("PRE-AUTHORIZATION");
        prePaidConsumeVoucherBean.setAmount(amount);
        prePaidConsumeVoucherBean.setTransactionBean(transactionBean);
        prePaidConsumeVoucherBean.setCsTransactionID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getCsTransactionID());
        prePaidConsumeVoucherBean.setMessageCode(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getMessageCode());
        prePaidConsumeVoucherBean.setStatusCode(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getStatusCode());
        prePaidConsumeVoucherBean.setTotalConsumed(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getAmount());
        prePaidConsumeVoucherBean.setMarketingMsg(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getMarketingMsg());
        prePaidConsumeVoucherBean.setPreAuthOperationID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getPreAuthOperationID());
        
        transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

        em.persist(transactionBean);
        em.persist(transactionStatusBean);
        em.persist(prePaidConsumeVoucherBean);

        createApplePayRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_SUCCESS);
        //createApplePayRefuelResponse.setTokenValue(paymentInfoBean.getToken());
        createApplePayRefuelResponse.setTransactionID(transactionID);
        createApplePayRefuelResponse.setShopLogin(shopLogin);
        createApplePayRefuelResponse.setAcquirerID(acquirerID);
        createApplePayRefuelResponse.setCurrency(currency);
        createApplePayRefuelResponse.setPaymentType(paymentType);
        //createApplePayRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
        
        userTransaction.commit();
        
        
        return createApplePayRefuelResponse;
    }

}
