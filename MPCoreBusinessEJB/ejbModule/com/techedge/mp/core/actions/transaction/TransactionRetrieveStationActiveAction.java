package com.techedge.mp.core.actions.transaction;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionRetrieveStationActiveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionRetrieveStationActiveAction() {}

    public boolean execute(String stationID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Ricerca l'impianto associato al beaconCode
            StationBean stationBean = QueryRepository.findActiveStationBeanById(em, stationID);

            if (stationBean == null) {

                // Transaction not found
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", stationID, null, "Station not found");

                userTransaction.commit();

                return true;
            }
            else {

                boolean active = true;

                if (stationBean.getStationStatus() == Station.STATION_STATUS_NOT_ACTIVE) {
                    active = false;
                }

                userTransaction.commit();

                String log = "Station " + stationID + (active ? " not " : " ") + "active";
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", stationID, null, log);

                return active;
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve station id with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", stationID, null, message);

            throw new EJBException(ex2);
        }
    }
}
