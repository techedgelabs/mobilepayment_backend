package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionPersistEndRefuelReceivedStatusAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public TransactionPersistEndRefuelReceivedStatusAction() {
    }

    
    public String execute(
    		String requestID,
			String transactionID,
			Double amount,
			Double fuelQuantity,
			String fuelType,
			String productDescription,
			String productID,
			String timestampEndRefuel,
			Double unitPrice,
			String source) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		// Ricerca la transazione associata al transactionID
    		TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);
		    
    		if (transactionBean == null) {

    			// Transaction not found
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Transaction not found" );
				
    			userTransaction.commit();
    			
    			return StatusHelper.PERSIST_END_REFUEL_RECEIVED_ERROR;
    		}

    		// Lo stato deve essere inserito solo se non � gi� presente
    		for ( TransactionStatusBean transactionStatusBean : transactionBean.getTransactionStatusBeanData() ) {

    			if ( transactionStatusBean.getStatus().equals(StatusHelper.STATUS_END_REFUEL_RECEIVED) ) {

    				// Bisogna uscire senza inserire lo stato
    				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "EndRefuelReceived status found...exiting" );
    				
        			userTransaction.commit();
        			
        			return StatusHelper.PERSIST_END_REFUEL_RECEIVED_SUCCESS;
    			}
    		}

    		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "EndRefuelReceived status not found...creating" );
			
    		// Aggiorna i dati della transazione
    		Date endRefuelTimestamp = null;

    		if ( timestampEndRefuel != null ) {
    		
    			// Il campo endRefuelTimestamp viene valorizzato solo se l'end refuel ha restituito il timestamp
	    		try {
	    			endRefuelTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(timestampEndRefuel);
	    		}
	    		catch ( ParseException e) {
	
	    			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, "Error parsing timestampEndRefuel:" + timestampEndRefuel );
					
	    			endRefuelTimestamp = new Date();
	    		}
    		}

    		System.out.println("Initial amount: " + transactionBean.getInitialAmount() + ", final amount: " + amount);
            
            if ( amount > transactionBean.getInitialAmount() ) {
                
                System.out.println("Errore elettrovalvola: final amount maggiore dell'initial amount");
                
                amount = transactionBean.getInitialAmount();
                
                System.out.println("Initial amount: " + transactionBean.getInitialAmount() + ", final amount: " + amount);
            }
    		
    		transactionBean.setFinalAmount(amount);
    		transactionBean.setFuelQuantity(fuelQuantity);
    		transactionBean.setProductID(productID);
    		transactionBean.setProductDescription(productDescription);
    		transactionBean.setEndRefuelTimestamp(new Date());
    		transactionBean.setUnitPrice(unitPrice);
    		
    		System.out.println("Aggiornamento endRefuel sui dati del PV");
    		StationBean stationBean = transactionBean.getStationBean();
    		Date now = new Date();
    		long nowTimestamp = now.getTime();
    		long offset = nowTimestamp - endRefuelTimestamp.getTime();
    		
    		stationBean.setEndRefuelTimestamp(now);
    		stationBean.setEndRefuelOffset(offset);
            
            em.merge(transactionBean);
    		em.merge(stationBean);
    		
    		// Crea il nuovo stato
    		Integer sequenceID = 0;
    		Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
    		for( TransactionStatusBean transactionStatusBean : transactionStatusData ) {
    			Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
    			if ( sequenceID < transactionStatusBeanSequenceID ) {
    				sequenceID = transactionStatusBeanSequenceID;
    			}
    		}
    		sequenceID = sequenceID + 1;

    		Timestamp timestamp = new java.sql.Timestamp(now.getTime());

    		String subStatus = StatusHelper.SUBSTATUS_MESSAGE_RECEIVED;
    		if ( source.equals("getTransactionStatus")) {
    			subStatus = StatusHelper.SUBSTATUS_REFUEL_TERMINATED;
    		}

    		TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

    		transactionStatusBean.setTransactionBean(transactionBean);
    		transactionStatusBean.setSequenceID(sequenceID);
    		transactionStatusBean.setTimestamp(timestamp);
    		transactionStatusBean.setStatus(StatusHelper.STATUS_END_REFUEL_RECEIVED);
    		transactionStatusBean.setSubStatus(subStatus);
    		transactionStatusBean.setSubStatusDescription("Refueling terminated");
    		transactionStatusBean.setRequestID(requestID);

    		em.persist(transactionStatusBean);
    		
    		userTransaction.commit();

    		return StatusHelper.PERSIST_END_REFUEL_RECEIVED_SUCCESS;

    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED persisting end refuel received status with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
