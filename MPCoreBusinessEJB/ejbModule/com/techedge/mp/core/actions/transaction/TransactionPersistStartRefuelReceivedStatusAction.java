package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionPersistStartRefuelReceivedStatusAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public TransactionPersistStartRefuelReceivedStatusAction() {
    }

    
    public String execute(
    		String requestID,
			String transactionID,
			String source) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		// Ricerca la transazione associata al transactionID
    		TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);
		    
    		if (transactionBean == null) {

    			// Transaction not found
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Transaction not found" );
				
    			userTransaction.commit();
    			
    			return StatusHelper.PERSIST_START_REFUEL_RECEIVED_ERROR;
    		}

    		// Lo stato deve essere inserito solo se non � gi� presente
    		for ( TransactionStatusBean transactionStatusBean : transactionBean.getTransactionStatusBeanData() ) {

    			if ( transactionStatusBean.getStatus().equals(StatusHelper.STATUS_START_REFUEL_RECEIVED) ) {

    				// Bisogna uscire senza inserire lo stato
    				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "StartRefuelReceived status found...exiting" );
    				
        			userTransaction.commit();
        			
        			return StatusHelper.PERSIST_START_REFUEL_RECEIVED_SUCCESS;
    			}
    		}

    		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "StartRefuelReceived status not found...creating" );
			
    		// Crea il nuovo stato
    		Integer sequenceID = 0;
    		Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
    		for( TransactionStatusBean transactionStatusBean : transactionStatusData ) {
    			Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
    			if ( sequenceID < transactionStatusBeanSequenceID ) {
    				sequenceID = transactionStatusBeanSequenceID;
    			}
    		}
    		sequenceID = sequenceID + 1;

    		Date now = new Date();
    		Timestamp timestamp = new java.sql.Timestamp(now.getTime());

    		String subStatus = StatusHelper.SUBSTATUS_MESSAGE_RECEIVED;
    		if ( source.equals("getTransactionStatus")) {
    			subStatus = StatusHelper.SUBSTATUS_REFUEL_INPROGRESS;
    		}

    		TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

    		transactionStatusBean.setTransactionBean(transactionBean);
    		transactionStatusBean.setSequenceID(sequenceID);
    		transactionStatusBean.setTimestamp(timestamp);
    		transactionStatusBean.setStatus(StatusHelper.STATUS_START_REFUEL_RECEIVED);
    		transactionStatusBean.setSubStatus(subStatus);
    		transactionStatusBean.setSubStatusDescription("Refueling started");
    		transactionStatusBean.setRequestID(requestID);

    		em.persist(transactionStatusBean);

    		userTransaction.commit();
    		
    		return StatusHelper.PERSIST_START_REFUEL_RECEIVED_SUCCESS;

    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED persisting start refuel received status with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}