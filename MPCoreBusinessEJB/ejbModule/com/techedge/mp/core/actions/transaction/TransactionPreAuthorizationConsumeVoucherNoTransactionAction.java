package com.techedge.mp.core.actions.transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.FidelityConsumeVoucherData;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TransactionPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionPreAuthorizationConsumeVoucherNoTransactionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionPreAuthorizationConsumeVoucherNoTransactionAction() {}

    public TransactionPreAuthorizationConsumeVoucherResponse execute(Long userID, String mpTransactionID, String stationID, Double amount, FidelityServiceRemote fidelityService) throws EJBException {

        TransactionPreAuthorizationConsumeVoucherResponse transactionPreAuthorizationConsumeVoucherResponse = new TransactionPreAuthorizationConsumeVoucherResponse();

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Ricerca la transazione associata al transactionID
            UserBean userBean = QueryRepository.findUserById(em, userID);

            if (userBean == null) {

                // User not found
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", userID.toString(), null, "user not found");

                userTransaction.commit();

                transactionPreAuthorizationConsumeVoucherResponse.setFidelityConsumeVoucherData(null);
                transactionPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_NOT_FOUND);

                return transactionPreAuthorizationConsumeVoucherResponse;

            }
            else {

                System.out.println("Inizio Preautorizzazione consumo voucher");

                // Chimamata al servizio preAuthorizationConsumeVoucher

                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                VoucherConsumerType voucherType = VoucherConsumerType.ENI;

                PartnerType partnerType = PartnerType.MP;
                Long requestTimestamp = new Date().getTime();
                String language = FidelityConstants.LANGUAGE_ITALIAN;
                ProductType productType = ProductType.OIL;

                List<VoucherCodeDetail> voucherCodeList = this.getVoucherCodeList(userBean);
                
                // TODO Eliminare se la gestione della lista vuota sar� delegata a Quenit
                if ( voucherCodeList == null || voucherCodeList.isEmpty() ) {
                    
                    // L'utente non ha voucher associati
                    transactionPreAuthorizationConsumeVoucherResponse.setFidelityConsumeVoucherData(new FidelityConsumeVoucherData());
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setOperationID(operationID);
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setRequestTimestamp(requestTimestamp);
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setAmount(0.0);
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setCsTransactionID(null);
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setMarketingMsg(null);
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setMessageCode(null);
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setPreAuthOperationID(null);
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setStatusCode(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER);
                    transactionPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS);

                    userTransaction.commit();
                    
                    return transactionPreAuthorizationConsumeVoucherResponse;
                }

                PreAuthorizationConsumeVoucherResult preAuthorizationConsumeVoucherResult = new PreAuthorizationConsumeVoucherResult();

                try {

                    preAuthorizationConsumeVoucherResult = fidelityService.preAuthorizationConsumeVoucher(operationID, mpTransactionID, voucherType, stationID, amount,
                            voucherCodeList, partnerType, requestTimestamp, language, productType);

                    transactionPreAuthorizationConsumeVoucherResponse.setFidelityConsumeVoucherData(new FidelityConsumeVoucherData());
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setOperationID(operationID);
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setRequestTimestamp(requestTimestamp);
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setAmount(preAuthorizationConsumeVoucherResult.getAmount());
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setCsTransactionID(preAuthorizationConsumeVoucherResult.getCsTransactionID());
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setMarketingMsg(preAuthorizationConsumeVoucherResult.getMarketingMsg());
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setMessageCode(preAuthorizationConsumeVoucherResult.getMessageCode());
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setPreAuthOperationID(
                            preAuthorizationConsumeVoucherResult.getPreAuthOperationID());
                    transactionPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setStatusCode(preAuthorizationConsumeVoucherResult.getStatusCode());
                    
                    if (preAuthorizationConsumeVoucherResult.getStatusCode().equals(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_PV_NOT_ENABLED)) {
                        
                        transactionPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_PV_NOT_ENABLED);
                        userTransaction.commit();
                    
                        return transactionPreAuthorizationConsumeVoucherResponse;
                    }
                    
                    if ( preAuthorizationConsumeVoucherResult.getStatusCode().equals(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_OK) ||
                         preAuthorizationConsumeVoucherResult.getStatusCode().equals(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER) ||
                         preAuthorizationConsumeVoucherResult.getStatusCode().equals(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER2) ) {

                        transactionPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS);
                    }
                    else {
                            
                        transactionPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE);
                    }

                    userTransaction.commit();
                    
                    return transactionPreAuthorizationConsumeVoucherResponse;

                }
                catch (Exception e) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", "requestID", null, "Error in pre authorization consume voucher");

                    userTransaction.commit();

                    transactionPreAuthorizationConsumeVoucherResponse.setFidelityConsumeVoucherData(null);
                    transactionPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE);

                    return transactionPreAuthorizationConsumeVoucherResponse;

                }
            }
        }

        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED Transaction Pre Authorization Consume Voucher with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "requestID", null, message);

            throw new EJBException(ex2);
        }

    }

    private List<VoucherCodeDetail> getVoucherCodeList(UserBean userBean) {

        List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

        for (VoucherBean voucherBean : userBean.getVoucherList()) {

            if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {

                VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                voucherCodeDetail.setVoucherCode(voucherBean.getCode());
                voucherCodeList.add(voucherCodeDetail);
            }
        }

        return voucherCodeList;

    }

}
