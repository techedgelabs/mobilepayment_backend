package com.techedge.mp.core.actions.transaction;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PendingRefuelResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.PanHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionRetrievePendingRefuelAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public TransactionRetrievePendingRefuelAction() {
    }

    
    public PendingRefuelResponse execute(
    		String ticketID,
			String requestID) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		PendingRefuelResponse pendingRefuelResponse = null;
    		
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			
    			pendingRefuelResponse = new PendingRefuelResponse();
    			pendingRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
    			return pendingRefuelResponse;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if ( userBean == null ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found" );
				
    			userTransaction.commit();
    			
    			pendingRefuelResponse = new PendingRefuelResponse();
    			pendingRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
    			return pendingRefuelResponse;
    		}
    		
    		
    		// Verifica lo stato dell'utente
    		Integer userStatus = userBean.getUserStatus();
    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
    			
    			// Un utente che si trova in questo stato non pu� invocare questo servizio
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to retrieve pending refuel in status " + userStatus );
				
    			userTransaction.commit();
    			
    			pendingRefuelResponse = new PendingRefuelResponse();
    			pendingRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_UNAUTHORIZED);
    			return pendingRefuelResponse;
    		}
    		
    		
    		// Ricerca la transazione attiva
    		List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);
		    
		    if ( transactionBeanList.isEmpty() ) {
    			
    			// Non esiste una transazione attiva
		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction not found" );
				
    			userTransaction.commit();
    			
    			pendingRefuelResponse = new PendingRefuelResponse();
    			pendingRefuelResponse.setStatusCode(ResponseHelper.RETRIEVE_PENDING_REFUEL_FAILURE);
    			return pendingRefuelResponse;
    		}
		    else {

				TransactionBean pendingTransactionBean = null;

				for( TransactionBean transactionBean : transactionBeanList ) {

					// Si estrae la prima
					pendingTransactionBean = transactionBean;
				}

				if ( pendingTransactionBean == null ) {
					
					// Non esiste una transazione attiva
			    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Id wrong in confirm refuel" );
					
	    			userTransaction.commit();
	    			
	    			pendingRefuelResponse = new PendingRefuelResponse();
	    			pendingRefuelResponse.setStatusCode(ResponseHelper.RETRIEVE_PENDING_REFUEL_FAILURE);
					return pendingRefuelResponse;
				}
				else {

					// Si estrae lo stato pi� recente associato alla transazione
					Integer lastSequenceID = 0;
					TransactionStatusBean lastTransactionStatusBean = null;

					Set<TransactionStatusBean> transactionStatusData = pendingTransactionBean.getTransactionStatusBeanData();

					for( TransactionStatusBean transactionStatusBean : transactionStatusData ) {

						Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
						if ( lastSequenceID < transactionStatusBeanSequenceID ) {
							lastSequenceID = transactionStatusBeanSequenceID;
							lastTransactionStatusBean = transactionStatusBean;
						}
					}

					if ( lastTransactionStatusBean == null ) {
						
						// Errore nella lettura dello stato della transazione
				    	this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, "Id wrong in confirm refuel" );
						
		    			userTransaction.commit();
		    			
		    			pendingRefuelResponse = new PendingRefuelResponse();
		    			pendingRefuelResponse.setStatusCode(ResponseHelper.RETRIEVE_PENDING_REFUEL_FAILURE);
						return pendingRefuelResponse;
					}
					else {
						
						pendingRefuelResponse = new PendingRefuelResponse();
						pendingRefuelResponse.setStatusCode(ResponseHelper.RETRIEVE_PENDING_REFUEL_SUCCESS);
						pendingRefuelResponse.setRefuelID(pendingTransactionBean.getTransactionID());
						pendingRefuelResponse.setStatus(lastTransactionStatusBean.getStatus());
						pendingRefuelResponse.setSubStatus(lastTransactionStatusBean.getSubStatus());
						pendingRefuelResponse.setUseVoucher(pendingTransactionBean.getUseVoucher());
						pendingRefuelResponse.setDate(lastTransactionStatusBean.getTimestamp());
						pendingRefuelResponse.setInitialAmount(pendingTransactionBean.getInitialAmount());
						pendingRefuelResponse.setFinalAmount(pendingTransactionBean.getFinalAmount());
						pendingRefuelResponse.setFuelAmount(pendingTransactionBean.getFuelAmount());
						pendingRefuelResponse.setFuelQuantity(pendingTransactionBean.getFuelQuantity());
						
						pendingRefuelResponse.setBankTransactionID(pendingTransactionBean.getBankTansactionID());
						pendingRefuelResponse.setShopLogin(pendingTransactionBean.getShopLogin());
						pendingRefuelResponse.setCurrency(pendingTransactionBean.getCurrency());
						String maskedPan = PanHelper.maskPan(pendingTransactionBean.getToken());
						pendingRefuelResponse.setMaskedPan(maskedPan);
						pendingRefuelResponse.setAuthCode(pendingTransactionBean.getAuthorizationCode());
						
						pendingRefuelResponse.setSelectedStationID(pendingTransactionBean.getStationBean().getStationID());
						// TODO - il nome della stazione non viene restituito
						pendingRefuelResponse.setSelectedStationName("");
						pendingRefuelResponse.setSelectedStationAddress(pendingTransactionBean.getStationBean().getAddress());
						pendingRefuelResponse.setSelectedStationCity(pendingTransactionBean.getStationBean().getCity());
						pendingRefuelResponse.setSelectedStationProvince(pendingTransactionBean.getStationBean().getProvince());
						pendingRefuelResponse.setSelectedStationCountry(pendingTransactionBean.getStationBean().getCountry());
						pendingRefuelResponse.setSelectedStationLatitude(pendingTransactionBean.getStationBean().getLatitude());
						pendingRefuelResponse.setSelectedStationLongitude(pendingTransactionBean.getStationBean().getLongitude());
						pendingRefuelResponse.setSelectedPumpID(pendingTransactionBean.getPumpID());
						pendingRefuelResponse.setSelectedPumpNumber(pendingTransactionBean.getPumpNumber());
						pendingRefuelResponse.setSelectedPumpFuelType(pendingTransactionBean.getProductDescription());
						
						userTransaction.commit();
						
						return pendingRefuelResponse;
					}
				}
			}
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED retrieve pending refuel with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}