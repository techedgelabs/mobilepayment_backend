package com.techedge.mp.core.actions.transaction;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionGetTransactionDetailAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionGetTransactionDetailAction() {}

    public Transaction execute(String requestId, String mpTransactionID) throws EJBException {
        try {
            UserTransaction userTransaction = context.getUserTransaction();
            userTransaction.begin();
            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, mpTransactionID);

            Transaction transactionResponse = null;
            if (transactionBean != null) {
                transactionResponse = transactionBean.toTransaction();
            }
            
            String paymentMethodExpiration = "";
            
            PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(em, transactionBean.getPaymentMethodId(), transactionBean.getPaymentMethodType());
            if (paymentInfoBean != null) {
                
                System.out.println("paymentInfoBean trovato con id: " + transactionBean.getPaymentMethodId() + " e type: " + transactionBean.getPaymentMethodType());
                
                CardDepositTransactionBean cardDepositTransactionBean = QueryRepository.findCardDepositTransactionByShopTransactionID(em, paymentInfoBean.getCheckShopTransactionID());
                if (cardDepositTransactionBean != null) {
                    
                    System.out.println("cardDepositTransactionBean trovato con checkShopTransactionID: " + paymentInfoBean.getCheckShopTransactionID());
                    
                    String tokenExpiryMonth = cardDepositTransactionBean.getTokenExpiryMonth();
                    String tokenExpiryYear  = cardDepositTransactionBean.getTokenExpiryYear();
                    
                    paymentMethodExpiration = "20" + tokenExpiryYear + tokenExpiryMonth;
                }
                else {
                    
                    System.out.println("cardDepositTransactionBean non trovato con checkShopTransactionID: " + paymentInfoBean.getCheckShopTransactionID());
                }
            }
            else {
                
                System.out.println("paymentInfoBean non trovato con id: " + transactionBean.getPaymentMethodId() + " e type: " + transactionBean.getPaymentType());
            }
            
            transactionResponse.setPaymentMethodExpiration(paymentMethodExpiration);
            
            userTransaction.commit();
            return transactionResponse;

        }
        catch (Exception ex2) {

            String message = "FAILED Post Payed transaction get with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

}
