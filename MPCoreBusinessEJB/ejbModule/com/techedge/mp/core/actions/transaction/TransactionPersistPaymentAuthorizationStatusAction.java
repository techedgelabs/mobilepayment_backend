package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.RefuelingMappingErrorBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.RefuelDetailConverter;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionPersistPaymentAuthorizationStatusAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionPersistPaymentAuthorizationStatusAction() {}

    public String execute(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId, String authorizationCode,
            EmailSenderRemote emailSender, RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, 
            String proxyHost, String proxyPort, String proxyNoHosts, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        String mpTransactionStatus = "ERROR";
        String mpStatusCode = null;

        try {
            userTransaction.begin();

            // Ricerca la transazione associata al transactionID
            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionID);

            if (transactionBean == null) {

                // Transaction not found
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionID, null, "Transaction not found");

                userTransaction.commit();

                return StatusHelper.PERSIST_PAYMENT_AUTHORIZATION_STATUS_ERROR;
            }

            // Crea il nuovo stato
            Integer sequenceID = 0;
            Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
            for (TransactionStatusBean transactionStatusBean : transactionStatusData) {
                Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                if (sequenceID < transactionStatusBeanSequenceID) {
                    sequenceID = transactionStatusBeanSequenceID;
                }
            }
            sequenceID = sequenceID + 1;

            Date now = new Date();
            Timestamp timestamp = new java.sql.Timestamp(now.getTime());
            String requestID = String.valueOf(now.getTime());

            TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

            transactionStatusBean.setTransactionBean(transactionBean);
            transactionStatusBean.setSequenceID(sequenceID);
            transactionStatusBean.setTimestamp(timestamp);
            transactionStatusBean.setRequestID(requestID);

            Integer eventSequenceID = 0;
            Set<TransactionEventBean> transactionEventData = transactionBean.getTransactionEventBeanData();
            for (TransactionEventBean transactionEventBean : transactionEventData) {
                Integer transactionEventBeanSequenceID = transactionEventBean.getSequenceID();
                if (eventSequenceID < transactionEventBeanSequenceID) {
                    eventSequenceID = transactionEventBeanSequenceID;
                }
            }
            eventSequenceID = eventSequenceID + 1;

            TransactionEventBean transactionEventBean = new TransactionEventBean();
            transactionEventBean.setEventType("AUT");
            transactionEventBean.setSequenceID(eventSequenceID);
            transactionEventBean.setTransactionBean(transactionBean);
            transactionEventBean.setTransactionResult(statusCode.toUpperCase());
            transactionEventBean.setErrorCode(errorCode);
            transactionEventBean.setErrorDescription(errorMessage);
            transactionEventBean.setEventAmount(transactionBean.getInitialAmount());

            if (statusCode.toLowerCase().equals("ok")) {

                transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_AUTHORIZED);
                transactionStatusBean.setSubStatus(subStatusCode);
                transactionStatusBean.setSubStatusDescription("Payment authorized(TransactionResult=OK)");

                transactionBean.setBankTansactionID(bankTransactionId);
                transactionBean.setAuthorizationCode(authorizationCode);

                em.merge(transactionBean);
                em.persist(transactionStatusBean);
                em.persist(transactionEventBean);
            }
            else {
                // TODO mappare anche gli altri stati della risposta GestPay
                if (errorCode != null && !errorCode.isEmpty() && !errorCode.equals("0")) {
                    RefuelingMappingErrorBean refuelingMappingErrorBean = QueryRepository.findErrorRefuelingByErrorCode(em, errorCode);
                    if (refuelingMappingErrorBean != null) {
                        mpTransactionStatus = refuelingMappingErrorBean.getStatusCode();
                    }
                    else {
                        mpTransactionStatus = "AUTH_FAILED_500";
                    }
                }

                if (mpStatusCode != null && mpStatusCode.equals("AUTH_FAILED_451")) {

                    // L'errorCode 451 indica una mancanza di fondi sulla carta di pagamento; i forntend devono mostrare un messaggio specifico per questa casistica di errore
                    transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_INSUFFICIENT_CREDIT);
                }

                else {

                    transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_REFUSED);
                    
                }

                transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_FAILED);
                transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_NOT_AUTHORIZED);
                transactionStatusBean.setSubStatusDescription("Negative Response (TransactionResult=KO)");

                if (subStatusCode.equals(StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_FAULT)) {
                    transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_ERROR);
                    transactionStatusBean.setSubStatus(StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_FAULT);
                    transactionStatusBean.setSubStatusDescription("Negative Response (TransactionResult=ERROR)");
                }

                em.merge(transactionBean);
                em.persist(transactionStatusBean);
                em.persist(transactionEventBean);

                Boolean isNewFlow = false;

                isNewFlow = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_PAYMENT_FLOW.getCode());
                
                if ( userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode())) {
                    isNewFlow = Boolean.TRUE;
                }
                
                if (isNewFlow) {

                    System.out.println("Invio mail ad utente nuovo flusso");
                }
                else {

                    System.out.println("Invio mail ad utente vecchio flusso");
                }

                UserBean userBean = transactionBean.getUserBean();
                if (( userBean.getUserType() == User.USER_TYPE_CUSTOMER || userBean.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER ) && emailSender != null) {
                    Email.sendPrePaidSummary(emailSender, transactionBean, transactionStatusBean, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
                }

                //chiamata al servizio di notifica per transazioni refueling

                String srcTransactionID = transactionBean.getSrcTransactionID();
                if (srcTransactionID != null && !srcTransactionID.equals("")) {

                    System.out.println("Trovata transazione refueling -> invio notifica");

                    RefuelDetail fuelDeatail = RefuelDetailConverter.createRefuelDetail(transactionBean);

                    String status = transactionBean.getLastTransactionStatus().getStatus();

                    System.out.println("Status: " + status);

                    System.out.println("Inizio chiamata servizio di notifica per Errore autorizzazione pagamento");

                    Boolean isRefuelingOAuth2 = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.UTENTI_REFUELING_OAUTH2.getCode());
                    
                    Runnable r = new TransactionRefuelSendNotification(requestID, srcTransactionID, transactionID, mpTransactionStatus, fuelDeatail, refuelingNotificationService,
                            refuelingNotificationServiceOAuth2, isRefuelingOAuth2, loggerService);

                    new Thread(r).start();
                }
            }

            userTransaction.commit();

            return StatusHelper.PERSIST_PAYMENT_AUTHORIZATION_STATUS_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED persisting payment authorization status with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", transactionID, null, message);

            throw new EJBException(ex2);
        }
    }

    /*
     * private RefuelDetail createRefuelDetail(TransactionBean transactionBean) {
     * 
     * //RefuelDetail di MPRefuelingAdapterEJBClient
     * RefuelDetail fuelDeatail = new RefuelDetail();
     * 
     * fuelDeatail.setAmount(transactionBean.getFinalAmount());
     * 
     * fuelDeatail.setTimestampStartRefuel(convertDateToString(transactionBean.getCreationTimestamp()));
     * fuelDeatail.setTimestampEndRefuel(convertDateToString(transactionBean.getEndRefuelTimestamp()));
     * 
     * fuelDeatail.setFuelQuantity(transactionBean.getFuelQuantity());
     * fuelDeatail.setProductID(transactionBean.getProductID());
     * fuelDeatail.setProductDescription(transactionBean.getProductDescription());
     * 
     * RefuelingFuelTypeMapping rftm = new RefuelingFuelTypeMapping();
     * fuelDeatail.setFuelType(rftm.getFuelType(transactionBean.getProductID()));
     * return fuelDeatail;
     * }
     * 
     * private String convertDateToString(Date refuelDate) {
     * 
     * if (refuelDate == null) {
     * return "";
     * }
     * 
     * String dateString = null;
     * SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
     * 
     * try {
     * dateString = sdfr.format(refuelDate);
     * }
     * catch (Exception ex) {
     * ex.printStackTrace();
     * }
     * 
     * return dateString;
     * }
     */
}
