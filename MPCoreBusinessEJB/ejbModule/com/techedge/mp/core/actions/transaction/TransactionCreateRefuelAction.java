package com.techedge.mp.core.actions.transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.TransactionService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.CreateRefuelResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.PurchaseVoucher;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionCreateRefuelAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TransactionCreateRefuelAction() {}

    public CreateRefuelResponse execute(String ticketID, String requestID, String encodedPin, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Double amountVoucher, Boolean useVoucher, String currency, String paymentType, Long paymentMethodId,
            String paymentMethodType, String outOfRange, String refuelMode, String defaultShopLogin, String defaultAcquirerID, Integer pinCheckMaxAttempts,
            Integer maxStatusAttempts, Integer reconciliationMaxAttempts, String serverName, Double voucherTransactionMinAmount, List<String> userBlockExceptionList,
            EmailSenderRemote emailSender, UserCategoryService userCategoryService, TransactionService transactionService, GPServiceRemote gpService,
            FidelityServiceRemote fidelityService, UnavailabilityPeriodService unavailabilityPeriodService, String proxyHost, String proxyPort, String proxyNoHosts)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ServerName: " + serverName);

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return createRefuelResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return createRefuelResponse;
            }

            if (userBlockExceptionList.contains(userBean.getPersonalDataBean().getSecurityDataEmail())) {

                System.out.println(userBean.getPersonalDataBean().getSecurityDataEmail() + " presente in lista utenti con eccezioni blocco");
            }
            else {
                // Controllo su disponibilit� del servizio di creazione transazioni prepaid
                ServiceAvailabilityData serviceAvailabilityData = unavailabilityPeriodService.retrieveServiceAvailability("CREATE_TRANSACTION_PREPAID", new Date());
                if (serviceAvailabilityData != null) {

                    // Servizio non disponibile
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "CreateRefuelService unavailable");

                    userTransaction.commit();

                    createRefuelResponse.setStatusCode(serviceAvailabilityData.getStatusCode());
                    return createRefuelResponse;
                }
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus);

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_UNAUTHORIZED);
                return createRefuelResponse;
            }

            // Controlla se l'utente ha gi� una transazione attiva
            List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);

            if (!transactionBeanList.isEmpty()) {

                // Esiste una transazione associata all'utente non ancora completata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction found");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
                return createRefuelResponse;
            }

            Integer userType = userBean.getUserType();
            Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());

            if (useNewFlow) {

                createRefuelResponse = this.createRefuelTransactionNewFlow(requestID, userTransaction, userBean, paymentMethodId, paymentMethodType, amount, amountVoucher,
                        encodedPin, stationID, pumpID, pinCheckMaxAttempts, defaultShopLogin, defaultAcquirerID, useVoucher, pumpNumber, productID, productDescription, currency,
                        paymentType, serverName, maxStatusAttempts, reconciliationMaxAttempts, outOfRange, refuelMode, voucherTransactionMinAmount, userCategoryService,
                        emailSender, transactionService, gpService, fidelityService, proxyHost, proxyPort, proxyNoHosts);
            }
            else {

                createRefuelResponse = this.createRefuelTransactionOldFlow(requestID, userTransaction, userBean, paymentMethodId, paymentMethodType, amount, encodedPin, stationID,
                        pumpID, pinCheckMaxAttempts, defaultShopLogin, defaultAcquirerID, useVoucher, pumpNumber, productID, productDescription, currency, paymentType, serverName,
                        maxStatusAttempts, reconciliationMaxAttempts, outOfRange, refuelMode, userCategoryService, emailSender, transactionService);
            }

            /*
            // Se il medodo di pagamento non � specificato utilizza quello di default
            PaymentInfoBean paymentInfoBean = null;
            if (paymentMethodId == null && paymentMethodType == null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found");

                paymentInfoBean = userBean.findDefaultPaymentInfoBean();
            }
            else {

                paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
            }

            // Verifica che il metodo di pagamento selezionato sia in uno stato valido
            if (paymentInfoBean == null) {

                // Il metodo di pagamento selezionato non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
                return createRefuelResponse;
            }

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                    + paymentMethodType);

            if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_BLOCKED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_ERROR
                    || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING) {

                // Il metodo di pagamento selezionato non pu� essere utilizzato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
                return createRefuelResponse;
            }
            
            double capAvailable = 0.0;
            
            if (userBean.getCapAvailable() != null) {
                capAvailable = userBean.getCapAvailable().doubleValue();
            }

            if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {

                // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                        "Payment not verified and transaction amount exceeding the cap");

                Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean);
                
                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_CAP_FAILURE);
                return createRefuelResponse;
            }

            if (!encodedPin.equals(paymentInfoBean.getPin())) {

                // Il pin inserito non � valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");

                // Si sottrae uno al numero di tentativi residui
                Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
                if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                    pinCheckAttemptsLeft--;
                }
                else {
                    pinCheckAttemptsLeft = 0;
                }

                if (pinCheckAttemptsLeft == 0) {

                    // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                    paymentInfoBean.setDefaultMethod(false);
                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
                }

                paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

                em.merge(paymentInfoBean);

                userTransaction.commit();

                createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_WRONG);
                return createRefuelResponse;
            }

            // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
            paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);

            em.merge(paymentInfoBean);

            StationBean stationBean = QueryRepository.findStationBeanById(em, stationID);

            if (stationBean == null) {

                // Lo stationID inserito non corrisponde a nessuna stazione valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID);

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
                return createRefuelResponse;
            }

            String shopLogin = stationBean.getOilShopLogin();
            String acquirerID = stationBean.getOilAcquirerID();
            Boolean newPaymentFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.NEW_PAYMENT_FLOW.getCode());

            if (newPaymentFlow) {
                shopLogin = defaultShopLogin;
                acquirerID = defaultAcquirerID;
            }

            String transactionID = new IdGenerator().generateId(16).substring(0, 32);

            Date now = new Date();
            Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

            // Crea la transazione con i dati ricevuti in input
            TransactionBean transactionBean = new TransactionBean();
            transactionBean.setTransactionID(transactionID);
            transactionBean.setUserBean(userBean);
            transactionBean.setStationBean(stationBean);
            transactionBean.setUseVoucher(useVoucher);
            transactionBean.setCreationTimestamp(creationTimestamp);
            transactionBean.setEndRefuelTimestamp(null);
            transactionBean.setInitialAmount(amount);
            transactionBean.setFinalAmount(null);
            transactionBean.setFuelAmount(null);
            transactionBean.setFuelQuantity(null);
            transactionBean.setPumpID(pumpID);
            transactionBean.setPumpNumber(pumpNumber);
            transactionBean.setProductID(productID);
            transactionBean.setProductDescription(productDescription);
            transactionBean.setCurrency(currency);
            transactionBean.setShopLogin(shopLogin);
            transactionBean.setAcquirerID(acquirerID);
            transactionBean.setBankTansactionID(null);
            transactionBean.setToken(paymentInfoBean.getToken());
            transactionBean.setProductID(null);
            transactionBean.setPaymentType(paymentType);
            transactionBean.setFinalStatusType(null);
            transactionBean.setGFGNotification(false);
            transactionBean.setConfirmed(false);
            transactionBean.setPaymentMethodId(paymentInfoBean.getId());
            transactionBean.setPaymentMethodType(paymentInfoBean.getType());
            transactionBean.setServerName(serverName);
            transactionBean.setStatusAttemptsLeft(maxStatusAttempts);
            transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
            transactionBean.setOutOfRange(outOfRange);
            transactionBean.setRefuelMode(refuelMode);
            transactionBean.setVoucherReconciliation(false);

            // Crea lo stato iniziale della transazione
            TransactionStatusBean transactionStatusBean = new TransactionStatusBean();
            transactionStatusBean.setTransactionBean(transactionBean);
            transactionStatusBean.setSequenceID(1);
            transactionStatusBean.setTimestamp(creationTimestamp);
            transactionStatusBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
            transactionStatusBean.setSubStatus(null);
            transactionStatusBean.setSubStatusDescription("Started");
            transactionStatusBean.setRequestID(requestID);

            em.persist(transactionBean);
            em.persist(transactionStatusBean);

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_SUCCESS);
            createRefuelResponse.setTokenValue(paymentInfoBean.getToken());
            createRefuelResponse.setTransactionID(transactionID);
            createRefuelResponse.setShopLogin(shopLogin);
            createRefuelResponse.setAcquirerID(acquirerID);
            createRefuelResponse.setCurrency(currency);
            createRefuelResponse.setPaymentType(paymentType);
            createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            
            userTransaction.commit();
            */

            return createRefuelResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED transaction creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    private CreateRefuelResponse createRefuelTransactionOldFlow(String requestID, UserTransaction userTransaction, UserBean userBean, Long paymentMethodId,
            String paymentMethodType, Double amount, String encodedPin, String stationID, String pumpID, Integer pinCheckMaxAttempts, String defaultShopLogin,
            String defaultAcquirerID, Boolean useVoucher, Integer pumpNumber, String productID, String productDescription, String currency, String paymentType, String serverName,
            Integer maxStatusAttempts, Integer reconciliationMaxAttempts, String outOfRange, String refuelMode, UserCategoryService userCategoryService,
            EmailSenderRemote emailSender, TransactionService transactionService) throws Exception {

        System.out.println("Utilizzo vecchio flusso voucher");

        CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();

        // Se il medodo di pagamento non � specificato utilizza quello di default
        PaymentInfoBean paymentInfoBean = null;
        if (paymentMethodId == null && paymentMethodType == null) {

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found");

            paymentInfoBean = userBean.findDefaultPaymentInfoBean();
        }
        else {

            paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
        }

        // Verifica che il metodo di pagamento selezionato sia in uno stato valido
        if (paymentInfoBean == null || !paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {

            // Il metodo di pagamento selezionato non esiste
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
            return createRefuelResponse;
        }

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                + paymentMethodType);

        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_BLOCKED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_ERROR
                || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING) {

            // Il metodo di pagamento selezionato non pu� essere utilizzato
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
            return createRefuelResponse;
        }

        double capAvailable = 0.0;

        if (userBean.getCapAvailable() != null) {
            capAvailable = userBean.getCapAvailable().doubleValue();
        }

        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {

            // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Payment not verified and transaction amount exceeding the cap");

            Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean, null, null);

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_CAP_FAILURE);
            return createRefuelResponse;
        }

        if (!encodedPin.equals(paymentInfoBean.getPin())) {

            // Il pin inserito non � valido
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");

            // Si sottrae uno al numero di tentativi residui
            Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
            if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                pinCheckAttemptsLeft--;
            }
            else {
                pinCheckAttemptsLeft = 0;
            }

            if (pinCheckAttemptsLeft == 0) {

                // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                paymentInfoBean.setDefaultMethod(false);
                paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
            }

            paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

            em.merge(paymentInfoBean);

            userTransaction.commit();

            createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_WRONG);
            return createRefuelResponse;
        }

        // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
        paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);

        em.merge(paymentInfoBean);

        StationBean stationBean = QueryRepository.findStationBeanById(em, stationID);

        if (stationBean == null) {

            // Lo stationID inserito non corrisponde a nessuna stazione valida
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID);

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }

        String shopLogin = stationBean.getOilShopLogin();
        String acquirerID = stationBean.getOilAcquirerID();

        String transactionID = new IdGenerator().generateId(16).substring(0, 32);

        Date now = new Date();
        Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

        // Crea la transazione con i dati ricevuti in input
        TransactionBean transactionBean = new TransactionBean();
        transactionBean.setTransactionID(transactionID);
        transactionBean.setUserBean(userBean);
        transactionBean.setStationBean(stationBean);
        transactionBean.setUseVoucher(useVoucher);
        transactionBean.setCreationTimestamp(creationTimestamp);
        transactionBean.setEndRefuelTimestamp(null);
        transactionBean.setInitialAmount(amount);
        transactionBean.setFinalAmount(null);
        transactionBean.setFuelAmount(null);
        transactionBean.setFuelQuantity(null);
        transactionBean.setPumpID(pumpID);
        transactionBean.setPumpNumber(pumpNumber);
        transactionBean.setProductID(productID);
        transactionBean.setProductDescription(productDescription);
        transactionBean.setCurrency(currency);
        transactionBean.setShopLogin(shopLogin);
        transactionBean.setAcquirerID(acquirerID);
        transactionBean.setBankTansactionID(null);
        transactionBean.setToken(paymentInfoBean.getToken());
        transactionBean.setProductID(null);
        transactionBean.setPaymentType(paymentType);
        transactionBean.setFinalStatusType(null);
        transactionBean.setGFGNotification(false);
        transactionBean.setConfirmed(false);
        transactionBean.setPaymentMethodId(paymentInfoBean.getId());
        transactionBean.setPaymentMethodType(paymentInfoBean.getType());
        transactionBean.setServerName(serverName);
        transactionBean.setStatusAttemptsLeft(maxStatusAttempts);
        transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
        transactionBean.setOutOfRange(outOfRange);
        transactionBean.setRefuelMode(refuelMode);
        transactionBean.setVoucherReconciliation(false);

        // Crea lo stato iniziale della transazione
        TransactionStatusBean transactionStatusBean = new TransactionStatusBean();
        transactionStatusBean.setTransactionBean(transactionBean);
        transactionStatusBean.setSequenceID(1);
        transactionStatusBean.setTimestamp(creationTimestamp);
        transactionStatusBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
        transactionStatusBean.setSubStatus(null);
        transactionStatusBean.setSubStatusDescription("Started");
        transactionStatusBean.setRequestID(requestID);

        em.persist(transactionBean);
        em.persist(transactionStatusBean);

        createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_SUCCESS);
        createRefuelResponse.setTokenValue(paymentInfoBean.getToken());
        createRefuelResponse.setTransactionID(transactionID);
        createRefuelResponse.setShopLogin(shopLogin);
        createRefuelResponse.setAcquirerID(acquirerID);
        createRefuelResponse.setCurrency(currency);
        createRefuelResponse.setPaymentType(paymentType);
        createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

        userTransaction.commit();

        return createRefuelResponse;
    }

    private CreateRefuelResponse createRefuelTransactionNewFlow(String requestID, UserTransaction userTransaction, UserBean userBean, Long paymentMethodId,
            String paymentMethodType, Double amount, Double amountVoucher, String encodedPin, String stationID, String pumpID, Integer pinCheckMaxAttempts,
            String defaultShopLogin, String defaultAcquirerID, Boolean useVoucher, Integer pumpNumber, String productID, String productDescription, String currency,
            String paymentType, String serverName, Integer maxStatusAttempts, Integer reconciliationMaxAttempts, String outOfRange, String refuelMode,
            Double voucherTransactionMinAmount, UserCategoryService userCategoryService, EmailSenderRemote emailSender, TransactionService transactionService,
            GPServiceRemote gpService, FidelityServiceRemote fidelityService, String proxyHost, String proxyPort, String proxyNoHosts) throws Exception {

        System.out.println("Utilizzo nuovo flusso voucher");

        CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();

        // Verifica lo stationID
        StationBean stationBean = QueryRepository.findStationBeanById(em, stationID);

        if (stationBean == null) {

            // Lo stationID inserito non corrisponde a nessuna stazione valida
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID);

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }

        // Verifica che l'utente abbia gi� associato il pin
        PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();

        if (paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("")) {

            // Errore Pin non ancora inserito
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");

            userTransaction.commit();
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }

        // Verifica che il pin indicato per il rifornimento coincida con quello inserito in iscrizione
        if (!encodedPin.equals(paymentInfoVoucherBean.getPin())) {

            // Il pin inserito non � valido
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");

            // Si sottrae uno al numero di tentativi residui
            Integer pinCheckAttemptsLeft = paymentInfoVoucherBean.getPinCheckAttemptsLeft();
            if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                pinCheckAttemptsLeft--;
            }
            else {
                pinCheckAttemptsLeft = 0;
            }

            if (pinCheckAttemptsLeft == 0) {

                // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                paymentInfoVoucherBean.setDefaultMethod(false);
                paymentInfoVoucherBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                // e tutti i metodi di pagamento verificati e non verificati sono cancellati
                for (PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {

                    if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                            && (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                        paymentInfoBean.setDefaultMethod(false);
                        em.merge(paymentInfoBean);
                    }
                }
            }

            paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

            em.merge(paymentInfoVoucherBean);

            userTransaction.commit();

            createRefuelResponse.setPinCheckMaxAttempts(paymentInfoVoucherBean.getPinCheckAttemptsLeft());
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_WRONG);
            return createRefuelResponse;
        }

        // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
        paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);

        em.merge(paymentInfoVoucherBean);

        // Verifica il metodo utilizzato per il pagamento
        System.out.println("Verifica metodo di pagamento");

        PaymentInfoBean paymentInfoBean = null;

        // Se il medodo di pagamento non � specificato utilizza quello di default
        if (paymentMethodId == null && paymentMethodType == null) {

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found");

            paymentInfoBean = userBean.findDefaultPaymentInfoBean();
        }
        else {

            paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
        }

        // Verifica che il metodo di pagamento selezionato sia in uno stato valido
        if (paymentInfoBean == null
                || (!paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) && !paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER))) {

            // Il metodo di pagamento selezionato non esiste
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
            return createRefuelResponse;
        }

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                + paymentMethodType);

        if (paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

            // Il metodo di pagamento selezionato non pu� essere utilizzato
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
            return createRefuelResponse;
        }

        double capAvailable = 0.0;

        if (userBean.getCapAvailable() != null) {
            capAvailable = userBean.getCapAvailable().doubleValue();
        }

        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {

            // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Payment not verified and transaction amount exceeding the cap");

            Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean, null, null);

            userTransaction.commit();

            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_CAP_FAILURE);
            return createRefuelResponse;
        }

        // Se l'utente ha indicato in input come metodo di pagamento una carta di credito, bisogna effettuare l'acquisto del voucher
        if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {

            System.out.println("Esecuzione transazione di acquisto voucher");

            if (defaultShopLogin == null || defaultShopLogin.equals("")) {

                // Si � verificato un errore nell'acquisto del voucher
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "defaultShopLogin null");

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_CREATE_VOUCHER);
                return createRefuelResponse;
            }

            PurchaseVoucher purchaseVoucher = new PurchaseVoucher(em, gpService, fidelityService, emailSender, proxyHost, proxyPort, proxyNoHosts);
            VoucherTransactionBean voucherTransactionBean = purchaseVoucher.buy(userBean, amountVoucher, defaultShopLogin, defaultAcquirerID, currency, paymentInfoBean,
                    paymentMethodType, paymentType, reconciliationMaxAttempts);

            if (voucherTransactionBean.getFinalStatusType().startsWith("ERROR_")) {

                // Si � verificato un errore nell'acquisto del voucher
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                        "Error create voucher " + voucherTransactionBean.getFinalStatusType());

                userTransaction.commit();

                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_CREATE_VOUCHER);
                return createRefuelResponse;
            }
            else {

                // Voucher acquistato con successo
                System.out.println("Voucher acquistato con successo");
            }
        }

        // Genera un transactionID da utilizzare nella chiamata al servizio di preautorizzazione consumo voucher
        String transactionID = new IdGenerator().generateId(16).substring(0, 32);

        // Preautorizza credito voucher
        System.out.println("Preautorizzazione credito voucher");

        TransactionPreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucher = transactionService.preAuthorizationConsumeVoucherNoTransaction(userBean.getId(),
                transactionID, stationID, amount);

        if (preAuthorizationConsumeVoucher == null) {

            // Errore nella preautorizzazione dell'importo tramite voucher
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                    "Error in preAuthorizationConsumeVoucherNoTransaction: preAuthorizationConsumeVoucher null");

            userTransaction.commit();
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }

        if (preAuthorizationConsumeVoucher.getStatusCode().equals(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_PV_NOT_ENABLED)) {

            // Errore nella preautorizzazione dell'importo tramite voucher
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                    "Error in preAuthorizationConsumeVoucherNoTransaction: statusCode " + preAuthorizationConsumeVoucher.getStatusCode());

            userTransaction.commit();
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PV_NOT_ENABLED);
            return createRefuelResponse;
        }

        if (!preAuthorizationConsumeVoucher.getStatusCode().equals(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS)) {

            // Errore nella preautorizzazione dell'importo tramite voucher
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                    "Error in preAuthorizationConsumeVoucherNoTransaction: statusCode " + preAuthorizationConsumeVoucher.getStatusCode());

            userTransaction.commit();
            createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return createRefuelResponse;
        }
        else {

            if (preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getStatusCode().equals(
                    FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER)
                    || preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getStatusCode().equals(
                            FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER2)) {

                System.out.println("Importo voucher non sufficiente");

                if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {

                    Double maxAmount = 0.0;
                    Double thresholdAmount = 0.0;

                    if (preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getAmount() != null) {
                        maxAmount = preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getAmount();
                    }

                    if (maxAmount >= amount) {

                        System.out.println("L'utente ha credito voucher sufficiente");
                    }
                    else {

                        System.out.println("L'utente non ha credito voucher sufficiente");

                        Double diff = amount - maxAmount;

                        System.out.println("amount - total: " + diff.toString());

                        thresholdAmount = Math.ceil(amount - maxAmount);

                        System.out.println("Threshold: " + thresholdAmount.toString());

                        if (thresholdAmount < voucherTransactionMinAmount) {
                            thresholdAmount = voucherTransactionMinAmount;

                            System.out.println("Threshold upd: " + thresholdAmount.toString());
                        }
                    }

                    // L'utente non ha sufficiente credito voucher
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                            "Error in preAuthorizationConsumeVoucherNoTransaction: credito voucher insufficiente, maxAmount: " + maxAmount.toString() + ", thresholdAmount: "
                                    + thresholdAmount.toString());

                    userTransaction.commit();
                    createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_INSUFFICIENT_VOUCHER_AMOUNT);
                    createRefuelResponse.setMaxAmount(maxAmount);
                    createRefuelResponse.setThresholdAmount(thresholdAmount);
                    return createRefuelResponse;
                }
                else {

                    // L'utente ha usato un metodo di pagamento di tipo credit_card e non ha sufficiente credito voucher
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                            "Error in preAuthorizationConsumeVoucherNoTransaction: preautorizzazione voucher in errore con metodo di pagamento di tipo credit_card");

                    userTransaction.commit();
                    createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
                    return createRefuelResponse;
                }
            }
        }

        System.out.println("Preautorizzazione credito voucher con esito positivo");

        /*
        if (false) {
            
            System.out.println("Utilizzo voucher come metodo di pagamento");
            
            // Verifica che l'utente abbia gi� associato il pin
            paymentInfoBean = userBean.getVoucherPaymentMethod();
            
            if ( paymentInfoBean == null || paymentInfoBean.getPin() == null || paymentInfoBean.getPin().equals("") ) {
                
                // Errore Pin non ancora inserito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");
            
                userTransaction.commit();
                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
                return createRefuelResponse;
            }
            
            // Verifica che il pin indicato per il rifornimento coincida con quello inserito in iscrizione
            if (!encodedPin.equals(paymentInfoBean.getPin())) {

                // Il pin inserito non � valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");

                // Si sottrae uno al numero di tentativi residui
                Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
                if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                    pinCheckAttemptsLeft--;
                }
                else {
                    pinCheckAttemptsLeft = 0;
                }

                if (pinCheckAttemptsLeft == 0) {

                    // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                    paymentInfoBean.setDefaultMethod(false);
                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
                }

                paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

                em.merge(paymentInfoBean);

                userTransaction.commit();

                createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_WRONG);
                return createRefuelResponse;
            }
            
            // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
            paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);

            em.merge(paymentInfoBean);
            
        }
        else {
            
            System.out.println("Utilizzo carta di credito come metodo di pagamento");
            
            // Se il medodo di pagamento non � specificato utilizza quello di default
            
            if (paymentMethodId == null && paymentMethodType == null) {
        
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found");
        
                paymentInfoBean = userBean.findDefaultPaymentInfoBean();
            }
            else {
        
                paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
            }
        
            // Verifica che il metodo di pagamento selezionato sia in uno stato valido
            if (paymentInfoBean == null || !paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {
        
                // Il metodo di pagamento selezionato non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");
        
                userTransaction.commit();
        
                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
                return createRefuelResponse;
            }
        
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                    + paymentMethodType);
        
            if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_BLOCKED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_ERROR
                    || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING) {
        
                // Il metodo di pagamento selezionato non pu� essere utilizzato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());
        
                userTransaction.commit();
        
                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
                return createRefuelResponse;
            }
            
            double capAvailable = 0.0;
            
            if (userBean.getCapAvailable() != null) {
                capAvailable = userBean.getCapAvailable().doubleValue();
            }
        
            if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {
        
                // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                        "Payment not verified and transaction amount exceeding the cap");
        
                Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean);
                
                userTransaction.commit();
        
                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_CAP_FAILURE);
                return createRefuelResponse;
            }
        
            if (!encodedPin.equals(paymentInfoBean.getPin())) {
        
                // Il pin inserito non � valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");
        
                // Si sottrae uno al numero di tentativi residui
                Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
                if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                    pinCheckAttemptsLeft--;
                }
                else {
                    pinCheckAttemptsLeft = 0;
                }
        
                if (pinCheckAttemptsLeft == 0) {
        
                    // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                    paymentInfoBean.setDefaultMethod(false);
                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
                }
        
                paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);
        
                em.merge(paymentInfoBean);
        
                userTransaction.commit();
        
                createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_WRONG);
                return createRefuelResponse;
            }
        
            // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
            paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);
        
            em.merge(paymentInfoBean);
        }
        */

        String shopLogin = defaultShopLogin;
        String acquirerID = "QUENIT"; //defaultAcquirerID;

        shopLogin = "ENIPAY" + stationBean.getStationID();

        Date now = new Date();
        Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

        // Crea la transazione con i dati ricevuti in input
        TransactionBean transactionBean = new TransactionBean();
        transactionBean.setTransactionID(transactionID);
        transactionBean.setUserBean(userBean);
        transactionBean.setStationBean(stationBean);
        transactionBean.setUseVoucher(useVoucher);
        transactionBean.setCreationTimestamp(creationTimestamp);
        transactionBean.setEndRefuelTimestamp(null);
        transactionBean.setInitialAmount(amount);
        transactionBean.setFinalAmount(null);
        transactionBean.setFuelAmount(null);
        transactionBean.setFuelQuantity(null);
        transactionBean.setPumpID(pumpID);
        transactionBean.setPumpNumber(pumpNumber);
        transactionBean.setProductID(productID);
        transactionBean.setProductDescription(productDescription);
        transactionBean.setCurrency(currency);
        transactionBean.setShopLogin(shopLogin);
        transactionBean.setAcquirerID(acquirerID);
        transactionBean.setBankTansactionID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getPreAuthOperationID());
        transactionBean.setToken(paymentInfoBean.getToken());
        transactionBean.setProductID(null);
        transactionBean.setPaymentType(paymentType);
        transactionBean.setFinalStatusType(null);
        transactionBean.setGFGNotification(false);
        transactionBean.setConfirmed(false);
        transactionBean.setPaymentMethodId(paymentInfoBean.getId());
        transactionBean.setPaymentMethodType(paymentInfoBean.getType());
        transactionBean.setServerName(serverName);
        transactionBean.setStatusAttemptsLeft(maxStatusAttempts);
        transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
        transactionBean.setOutOfRange(outOfRange);
        transactionBean.setRefuelMode(refuelMode);
        transactionBean.setVoucherReconciliation(false);

        transactionBean.setAuthorizationCode(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getPreAuthOperationID());

        // Crea lo stato iniziale della transazione
        TransactionStatusBean transactionStatusBean = new TransactionStatusBean();
        transactionStatusBean.setTransactionBean(transactionBean);
        transactionStatusBean.setSequenceID(1);
        transactionStatusBean.setTimestamp(creationTimestamp);
        transactionStatusBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
        transactionStatusBean.setSubStatus(null);
        transactionStatusBean.setSubStatusDescription("Started");
        transactionStatusBean.setRequestID(requestID);

        // Crea la riga di log di preautorizzazione voucher
        PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
        prePaidConsumeVoucherBean.setOperationID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getOperationID());
        prePaidConsumeVoucherBean.setRequestTimestamp(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getRequestTimestamp());
        prePaidConsumeVoucherBean.setOperationType("PRE-AUTHORIZATION");
        prePaidConsumeVoucherBean.setAmount(amount);
        prePaidConsumeVoucherBean.setTransactionBean(transactionBean);
        prePaidConsumeVoucherBean.setCsTransactionID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getCsTransactionID());
        prePaidConsumeVoucherBean.setMessageCode(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getMessageCode());
        prePaidConsumeVoucherBean.setStatusCode(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getStatusCode());
        prePaidConsumeVoucherBean.setTotalConsumed(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getAmount());
        prePaidConsumeVoucherBean.setMarketingMsg(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getMarketingMsg());
        prePaidConsumeVoucherBean.setPreAuthOperationID(preAuthorizationConsumeVoucher.getFidelityConsumeVoucherData().getPreAuthOperationID());

        transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

        em.persist(transactionBean);
        em.persist(transactionStatusBean);
        em.persist(prePaidConsumeVoucherBean);

        createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_SUCCESS);
        createRefuelResponse.setTokenValue(paymentInfoBean.getToken());
        createRefuelResponse.setTransactionID(transactionID);
        createRefuelResponse.setShopLogin(shopLogin);
        createRefuelResponse.setAcquirerID(acquirerID);
        createRefuelResponse.setCurrency(currency);
        createRefuelResponse.setPaymentType(paymentType);
        createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

        userTransaction.commit();

        return createRefuelResponse;
    }

}
