package com.techedge.mp.core.actions.manager;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ExtendedPumpInfo;
import com.techedge.mp.core.business.interfaces.GetStationInfo;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.ManagerStationDetailsResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.model.ManagerTicketBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ManagerStationDetailsAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public ManagerStationDetailsResponse execute(String ticketId, String requestId, String stationId, ForecourtInfoServiceRemote forecourtInfoService) throws EJBException {
        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            ManagerTicketBean ticketBean = QueryRepository.findManagerTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isStandardTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                ManagerStationDetailsResponse managerResponse = new ManagerStationDetailsResponse();
                managerResponse.setStatusCode(ResponseHelper.MANAGER_REQU_INVALID_TICKET);
                return managerResponse;
            }

            ManagerBean managerBean = ticketBean.getManagerBean();
            if (managerBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                ManagerStationDetailsResponse managerResponse = new ManagerStationDetailsResponse();
                managerResponse.setStatusCode(ResponseHelper.MANAGER_STATION_USER_NOT_ACTIVE);
                return managerResponse;
            }
            else {
                // Verifica lo stato dell'utente
                Integer status = managerBean.getStatus();
                if (status != Manager.STATUS_ACTIVE && managerBean.getType() != Manager.TYPE_STANDARD) {

                    // Un utente che si trova in questo stato non pu� effettuare il login
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: Manager "
                            + managerBean.getUsername() + " in status " + status);

                    userTransaction.commit();

                    ManagerStationDetailsResponse managerResponse = new ManagerStationDetailsResponse();
                    managerResponse.setStatusCode(ResponseHelper.MANAGER_AUTH_UNAUTHORIZED);
                    return managerResponse;
                }

                boolean stationIdFound = false;
                boolean stationActive = true;

                for (StationBean stationBean : managerBean.getStations()) {
                    if (stationBean.getStationID().equals(stationId)) {
                        if (stationBean.getStationStatus() == Station.STATION_STATUS_NOT_ACTIVE) {
                            stationActive = false;
                            break;
                        }

                        stationIdFound = true;
                        break;
                    }
                }

                if (!stationActive) {
                    // StationID non trovata per l'utente manager
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "StationID: " + stationId + " not active for Manager "
                            + managerBean.getUsername());

                    userTransaction.commit();

                    ManagerStationDetailsResponse managerResponse = new ManagerStationDetailsResponse();
                    managerResponse.setStatusCode(ResponseHelper.MANAGER_STATION_NOT_FOUND);
                    return managerResponse;
                }

                if (!stationIdFound) {
                    // StationID non trovata per l'utente manager
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "StationID: " + stationId + " not found for Manager "
                            + managerBean.getUsername());

                    userTransaction.commit();

                    ManagerStationDetailsResponse managerResponse = new ManagerStationDetailsResponse();
                    managerResponse.setStatusCode(ResponseHelper.MANAGER_STATION_INVALID_STATION_ID);
                    return managerResponse;
                }
            }

            ManagerStationDetailsResponse managerResponse = new ManagerStationDetailsResponse();
            GetStationDetailsResponse getStationDetailsResponse = forecourtInfoService.getStationDetails(requestId, stationId, null, false);

            if (getStationDetailsResponse.getStatusCode().equals("STATION_PUMP_FOUND_200")) {
                managerResponse.setStatusCode(getStationDetailsResponse.getStatusCode());
                GetStationInfo stationInfo = new GetStationInfo();
                stationInfo.setStationId(getStationDetailsResponse.getStationDetail().getStationID());
                stationInfo.setAddress(getStationDetailsResponse.getStationDetail().getAddress());
                stationInfo.setCity(getStationDetailsResponse.getStationDetail().getCity());
                stationInfo.setCountry(getStationDetailsResponse.getStationDetail().getCountry());
                stationInfo.setProvince(getStationDetailsResponse.getStationDetail().getProvince());
                stationInfo.setLatitude(getStationDetailsResponse.getStationDetail().getLatitude());
                stationInfo.setLongitude(getStationDetailsResponse.getStationDetail().getLongitude());

                if (!getStationDetailsResponse.getStationDetail().getPumpDetails().isEmpty()) {
                    for (PumpDetail pumpDetail : getStationDetailsResponse.getStationDetail().getPumpDetails()) {
                        ExtendedPumpInfo pumpInfo = new ExtendedPumpInfo();
                        pumpInfo.setPumpId(pumpDetail.getPumpID());
                        pumpInfo.setNumber(pumpDetail.getPumpNumber());
                        pumpInfo.setStatus(pumpDetail.getPumpStatus());

                        stationInfo.getPumpList().add(pumpInfo);
                    }
                }

                managerResponse.setStationInfo(stationInfo);
                managerResponse.setStatusCode(ResponseHelper.MANAGER_STATION_SUCCESS);
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "StationID: " + stationId + " founded for Manager "
                        + managerBean.getUsername());
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "No pumps found for stationID: " + stationId + " for Manager "
                        + managerBean.getUsername());
                managerResponse.setStatusCode(ResponseHelper.MANAGER_STATION_FAILURE);
            }

            userTransaction.commit();
            
            return managerResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve station details with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }

    }

}
