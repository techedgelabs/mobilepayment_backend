package com.techedge.mp.core.actions.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.model.ManagerTicketBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ManagerRescuePasswordAction {

  @Resource
  private EJBContext context;

  @PersistenceContext(unitName = "CrudPU")
  private EntityManager em;

  @EJB
  private LoggerService loggerService;

  public ManagerRescuePasswordAction() {}

  public String execute(String ticketId, String requestId, String i_mail, EmailSenderRemote emailSender, Integer rescuePasswordExpirationTime, String passwordRecoveryLink)
      throws EJBException {

    UserTransaction userTransaction = context.getUserTransaction();

    try {
      userTransaction.begin();

      // Verifica il ticket
      ManagerTicketBean ticketBean = QueryRepository.findManagerTicketById(em, ticketId);

      if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isStandardTicket()) {

        // Ticket non valido
        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

        userTransaction.commit();

        return ResponseHelper.MANAGER_REQU_INVALID_TICKET;
      }

      ManagerBean managerBean = ticketBean.getManagerBean();
      if (managerBean == null) {

        // Ticket non valido
        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

        userTransaction.commit();

        return ResponseHelper.MANAGER_RESCUE_PASSWORD_NOT_EXISTS;
      }

     if ((managerBean.getStatus() != Manager.STATUS_TEMPORARY_PASSWORD) && (managerBean.getStatus() != Manager.STATUS_ACTIVE)) {

        // Lo stato dell'utente non consente il recupero della password
        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User status not valid for operation");

        userTransaction.commit();

        return ResponseHelper.MANAGER_RESCUE_USER_NOT_ACTIVE;
      }

      String temporaryRescuePassword = new IdGenerator().generateId(10).substring(0, 8);

      this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Temporary password: "
          + temporaryRescuePassword);

      String encodedPassword = EncoderHelper.encode(temporaryRescuePassword);

      this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Temporary encoded password: "
          + encodedPassword);

      //managerBean.setPassword(encodedPassword);

      managerBean.setRescuePassword(encodedPassword);
      
       Date now = new Date();
       Date expiryDate = DateHelper.addMinutesToDate(rescuePasswordExpirationTime, now);
       managerBean.setExpirationDateRescuePassword(expiryDate);

       // Salva i dati dell'utente su db
      em.merge(managerBean);

      // Invia l'email con il codice di attivazione
      if (emailSender != null) {

        EmailType emailType = EmailType.RESCUE_PASSWORD_EMAIL;
        String to = managerBean.getEmail();
        String name = managerBean.getUsername();
        if ((managerBean.getFirstName() != null && !managerBean.getFirstName().equals("")) && (managerBean.getLastName() != null && !managerBean.getLastName().equals(""))) {
            name = managerBean.getFirstName() + " " + managerBean.getLastName();
        }
        List<Parameter> parameters = new ArrayList<Parameter>(0);

        parameters.add(new Parameter("NAME", name));
        parameters.add(new Parameter("PASSWORD_RECOVERY_LINK", passwordRecoveryLink));
        parameters.add(new Parameter("VERIFICATION_CODE", temporaryRescuePassword));
        parameters.add(new Parameter("EMAIL", to));

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Sending email to " + to);

        String result = emailSender.sendEmail(emailType, to, parameters);

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "SendEmail result: " + result);

      }

      // Rinnova il ticket
      ticketBean.renew();
      em.merge(ticketBean);

      userTransaction.commit();

      return ResponseHelper.MANAGER_RESCUE_PASSWORD_SUCCESS;

    }
    catch (Exception ex2) {

      try {
        userTransaction.rollback();
      }
      catch (IllegalStateException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      catch (SecurityException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      catch (SystemException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
      this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

      throw new EJBException(ex2);
    }
  }
}
