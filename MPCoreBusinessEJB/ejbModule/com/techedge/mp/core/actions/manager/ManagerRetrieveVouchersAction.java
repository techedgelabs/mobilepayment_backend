package com.techedge.mp.core.actions.manager;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ManagerRetrieveVouchersResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.postpaid.VoucherHistory;
import com.techedge.mp.core.business.model.ManagerTicketBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ManagerRetrieveVouchersAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public ManagerRetrieveVouchersResponse execute(String ticketId, String requestId, String stationID, Date startDate, Date endDate, ForecourtInfoServiceRemote forecourtInfoService)
            throws EJBException {
        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            ManagerTicketBean ticketBean = QueryRepository.findManagerTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isStandardTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                ManagerRetrieveVouchersResponse managerRetrieveVouchersResponse = new ManagerRetrieveVouchersResponse();
                managerRetrieveVouchersResponse.setStatusCode(ResponseHelper.MANAGER_REQU_INVALID_TICKET);
                return managerRetrieveVouchersResponse;
            }

            if (stationID == null || stationID.equals("")) {
                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid stationID");

                userTransaction.commit();

                ManagerRetrieveVouchersResponse managerRetrieveVouchersResponse = new ManagerRetrieveVouchersResponse();
                managerRetrieveVouchersResponse.setStatusCode(ResponseHelper.MANAGER_RETRIEVE_VOUCHERS_INVALID_STATION_ID);
                return managerRetrieveVouchersResponse;
            }

            StationBean stationBean = null;

            for (StationBean tmpStationBean : ticketBean.getManagerBean().getStations()) {
                if (tmpStationBean.getStationID().equals(stationID)) {
                    stationBean = tmpStationBean;
                    break;
                }
            }

            if (stationBean == null) {
                // Stazione non trovata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Station not found");

                userTransaction.commit();

                ManagerRetrieveVouchersResponse managerRetrieveVouchersResponse = new ManagerRetrieveVouchersResponse();
                managerRetrieveVouchersResponse.setStatusCode(ResponseHelper.MANAGER_RETRIEVE_VOUCHERS_INVALID_STATION_ID);
                return managerRetrieveVouchersResponse;
            }

            ManagerRetrieveVouchersResponse managerRetrieveVouchersResponse = new ManagerRetrieveVouchersResponse();

            endDate = new Date(endDate.getTime() + 86399000);
            
            String startDateString = "";
            if (startDate != null) {
                startDateString = startDate.toString();
            }

            String endDateString = "";
            if (endDate != null) {
                endDateString = endDate.toString();
            }

            this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "execute", requestId, null, "StartDate:" + startDateString + "  EndDate:" + endDateString);

            List<PostPaidTransactionBean> postPaidTransactionList = QueryRepository.findPostPaidTransactionBeanByStationBeanAndDate(em, stationBean, startDate, endDate);

            for (PostPaidTransactionBean postPaidTransactionBean : postPaidTransactionList) {

                for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {

                    VoucherHistory voucherHistory = new VoucherHistory();

                    voucherHistory.setDate(postPaidTransactionBean.getCreationTimestamp());
                    voucherHistory.setPumpNumber(postPaidTransactionBean.getSourceNumber());
                    voucherHistory.setStationID(stationID);
                    voucherHistory.setTotalConsumed(postPaidConsumeVoucherBean.getTotalConsumed());

                    for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {

                        Voucher voucher = new Voucher();
                        voucher.setCode(postPaidConsumeVoucherDetailBean.getVoucherCode());
                        voucher.setConsumedValue(postPaidConsumeVoucherDetailBean.getConsumedValue());
                        voucher.setExpirationDate(postPaidConsumeVoucherDetailBean.getExpirationDate());
                        voucher.setInitialValue(postPaidConsumeVoucherDetailBean.getInitialValue());
                        voucher.setPromoCode(postPaidConsumeVoucherDetailBean.getPromoCode());
                        voucher.setPromoDescription(postPaidConsumeVoucherDetailBean.getPromoDescription());
                        voucher.setPromoDoc(postPaidConsumeVoucherDetailBean.getPromoDoc());
                        voucher.setStatus(postPaidConsumeVoucherDetailBean.getVoucherStatus());
                        voucher.setType(postPaidConsumeVoucherDetailBean.getVoucherType());
                        voucher.setValue(postPaidConsumeVoucherDetailBean.getVoucherValue());
                        voucher.setVoucherBalanceDue(postPaidConsumeVoucherDetailBean.getVoucherBalanceDue());

                        voucherHistory.getVoucherList().add(voucher);
                    }

                    managerRetrieveVouchersResponse.getVoucherHistory().add(voucherHistory);
                }
            }

            List<PostPaidTransactionHistoryBean> postPaidTransactionHistoryList = QueryRepository.findPostPaidTransactionHistoryBeanByStationBeanAndDate(em, stationBean, startDate, endDate);

            for (PostPaidTransactionHistoryBean postPaidTransactionHistoryBean : postPaidTransactionHistoryList) {

                for (PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean : postPaidTransactionHistoryBean.getPostPaidConsumeVoucherBeanList()) {

                    VoucherHistory voucherHistory = new VoucherHistory();

                    voucherHistory.setDate(postPaidTransactionHistoryBean.getCreationTimestamp());
                    voucherHistory.setPumpNumber(postPaidTransactionHistoryBean.getSourceNumber());
                    voucherHistory.setStationID(stationID);
                    voucherHistory.setTotalConsumed(postPaidConsumeVoucherHistoryBean.getTotalConsumed());

                    for (PostPaidConsumeVoucherDetailHistoryBean postPaidConsumeVoucherDetailHistoryBean : postPaidConsumeVoucherHistoryBean.getPostPaidConsumeVoucherDetailHistoryBean()) {

                        Voucher voucher = new Voucher();
                        voucher.setCode(postPaidConsumeVoucherDetailHistoryBean.getVoucherCode());
                        voucher.setConsumedValue(postPaidConsumeVoucherDetailHistoryBean.getConsumedValue());
                        voucher.setExpirationDate(postPaidConsumeVoucherDetailHistoryBean.getExpirationDate());
                        voucher.setInitialValue(postPaidConsumeVoucherDetailHistoryBean.getInitialValue());
                        voucher.setPromoCode(postPaidConsumeVoucherDetailHistoryBean.getPromoCode());
                        voucher.setPromoDescription(postPaidConsumeVoucherDetailHistoryBean.getPromoDescription());
                        voucher.setPromoDoc(postPaidConsumeVoucherDetailHistoryBean.getPromoDoc());
                        voucher.setStatus(postPaidConsumeVoucherDetailHistoryBean.getVoucherStatus());
                        voucher.setType(postPaidConsumeVoucherDetailHistoryBean.getVoucherType());
                        voucher.setValue(postPaidConsumeVoucherDetailHistoryBean.getVoucherValue());
                        voucher.setVoucherBalanceDue(postPaidConsumeVoucherDetailHistoryBean.getVoucherBalanceDue());

                        voucherHistory.getVoucherList().add(voucher);
                    }

                    managerRetrieveVouchersResponse.getVoucherHistory().add(voucherHistory);
                }
            }

            userTransaction.commit();

            managerRetrieveVouchersResponse.setStatusCode(ResponseHelper.MANAGER_RETRIEVE_VOUCHERS_SUCCESS);
            return managerRetrieveVouchersResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve vouchers with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }

    }

}
