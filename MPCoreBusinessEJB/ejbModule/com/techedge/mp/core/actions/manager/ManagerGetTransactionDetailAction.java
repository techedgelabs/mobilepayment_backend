package com.techedge.mp.core.actions.manager;

import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.model.ManagerTicketBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.utilities.PanHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ManagerGetTransactionDetailAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public ManagerGetTransactionDetailAction() {}

    public PostPaidGetTransactionDetailResponse execute(String requestID, String ticketID, String mpTransactionID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();
            PostPaidGetTransactionDetailResponse popGetTransactionDetailResponse = new PostPaidGetTransactionDetailResponse();

            // Verifica il ticket
            ManagerTicketBean ticketBean = QueryRepository.findManagerTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isStandardTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_INVALID_TICKET);
                return popGetTransactionDetailResponse;
            }

            ManagerBean managerBean = ticketBean.getManagerBean();
            if (managerBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_INVALID_TICKET);
                return popGetTransactionDetailResponse;
            }

            // Verifica lo stato del manager
            Integer managerStatus = managerBean.getStatus();
            if (managerStatus != Manager.STATUS_ACTIVE) {

                // Un manager che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status "
                        + managerStatus);

                userTransaction.commit();

                popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_UNAUTHORIZED);
                return popGetTransactionDetailResponse;
            }

            // Controlla se a mpTransactionID � associata una transazione post paid

            PostPaidTransactionBean poPTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, mpTransactionID);

            PostPaidTransactionHistoryBean poPTransactionHistoryBean = QueryRepository.findPostPaidTransactionHistoryBeanByMPId(em, mpTransactionID);

            if (poPTransactionHistoryBean == null) {

                if (poPTransactionBean != null) {

                    poPTransactionHistoryBean = new PostPaidTransactionHistoryBean(poPTransactionBean);
                }
            }

            if (poPTransactionHistoryBean == null) {

                // L'mpTransactionID inserito non corrisponde a nessuna transazione post paid valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No post paid transaction for mpTransactionId "
                        + mpTransactionID);

                userTransaction.commit();

                popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_NOT_RECOGNIZED);
                return popGetTransactionDetailResponse;
            }
            else {

                // Trovata una transazione post paid

                // Verifico che il sourceId sia quello di una delle stazioni per le quali il manager � abilitato
                
                StationBean stationBean = null;

                for (StationBean tmpStationBean : ticketBean.getManagerBean().getStations()) {
                    if (tmpStationBean.getStationID().equals(poPTransactionHistoryBean.getStationBean().getStationID())) {
                        stationBean = tmpStationBean;
                        break;
                    }
                }

                if (stationBean == null) {

                    // Stazione non trovata
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Station not found");

                    userTransaction.commit();

                    popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_UNAUTHORIZED);
                    return popGetTransactionDetailResponse;
                }
                
                popGetTransactionDetailResponse.setMpTransactionID(poPTransactionHistoryBean.getMpTransactionID());
                popGetTransactionDetailResponse.setMpTransactionStatus(poPTransactionHistoryBean.getMpTransactionStatus());
                popGetTransactionDetailResponse.setTransactionType(poPTransactionHistoryBean.getProductType());
                
                popGetTransactionDetailResponse.setStation(poPTransactionHistoryBean.getStationBean().toStation());
                popGetTransactionDetailResponse.setCreationTimestamp(poPTransactionHistoryBean.getCreationTimestamp());
                popGetTransactionDetailResponse.setAmount(poPTransactionHistoryBean.getAmount());
                popGetTransactionDetailResponse.setBankTansactionID(poPTransactionHistoryBean.getBankTansactionID());
                popGetTransactionDetailResponse.setAuthorizationCode(poPTransactionHistoryBean.getAuthorizationCode());
                popGetTransactionDetailResponse.setPanCode(PanHelper.maskPan(poPTransactionHistoryBean.getToken()));
                popGetTransactionDetailResponse.setPaymentMethodId(poPTransactionHistoryBean.getPaymentMethodId());
                popGetTransactionDetailResponse.setPaymentMethodType(poPTransactionHistoryBean.getPaymentMethodType());
                popGetTransactionDetailResponse.setPaymentType(poPTransactionHistoryBean.getPaymentType());

                popGetTransactionDetailResponse.setStatus(poPTransactionHistoryBean.getMpTransactionStatus());
                popGetTransactionDetailResponse.setSubStatus(null);
                popGetTransactionDetailResponse.setUseVoucher(poPTransactionHistoryBean.getUseVoucher());

                //IF (OK)

                if (poPTransactionHistoryBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {

                    popGetTransactionDetailResponse.setSourceType("FUEL");

                    Set<PostPaidRefuelHistoryBean> postPaidRefuelHistoryBeanSet = poPTransactionHistoryBean.getRefuelHistoryBean();

                    PostPaidRefuelHistoryBean postPaidRefuelBeanHistoryFound = null;

                    for (PostPaidRefuelHistoryBean postPaidRefuelHistoryBean : postPaidRefuelHistoryBeanSet) {
                        postPaidRefuelBeanHistoryFound = postPaidRefuelHistoryBean;
                        break;
                    }

                    if (postPaidRefuelBeanHistoryFound != null) {

                        System.out.println("postPaidRefuelBeanFound - pumpID: " + postPaidRefuelBeanHistoryFound.getPumpId());

                        popGetTransactionDetailResponse.setPumpID(postPaidRefuelBeanHistoryFound.getPumpId());
                        popGetTransactionDetailResponse.setPumpStatus(null);
                        popGetTransactionDetailResponse.setPumpNumber(postPaidRefuelBeanHistoryFound.getPumpNumber().toString());
                        popGetTransactionDetailResponse.setPumpFuelType(postPaidRefuelBeanHistoryFound.getProductDescription());
                    }
                    else {

                        System.out.println("postPaidRefuelBeanFound null");
                    }
                }
                else {

                    popGetTransactionDetailResponse.setSourceType("SHOP");

                    popGetTransactionDetailResponse.setCashID(poPTransactionHistoryBean.getSourceID());
                    
                    if (poPTransactionHistoryBean.getSourceNumber() != null) {
                        popGetTransactionDetailResponse.setCashNumber(poPTransactionHistoryBean.getSourceNumber().toString());
                    }
                    else {
                        popGetTransactionDetailResponse.setCashNumber(null);
                    }
                }

                popGetTransactionDetailResponse.setSourceStatus("POST-PAY");

                popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);


                if (poPTransactionHistoryBean.getCartHistoryBean() != null) {
                    for (PostPaidCartHistoryBean postPaidCartHistoryBean : poPTransactionHistoryBean.getCartHistoryBean()) {
                        PostPaidCart postPaidCart = new PostPaidCart();
                        postPaidCart.setAmount(postPaidCartHistoryBean.getAmount());
                        postPaidCart.setProductDescription(postPaidCartHistoryBean.getProductDescription());
                        postPaidCart.setProductId(postPaidCartHistoryBean.getProductId());
                        postPaidCart.setQuantity(postPaidCartHistoryBean.getQuantity());
                        popGetTransactionDetailResponse.getCartList().add(postPaidCart);
                    }
                }

                if (poPTransactionHistoryBean.getRefuelHistoryBean() != null) {
                    for (PostPaidRefuelHistoryBean postPaidRefuelHistoryBean : poPTransactionHistoryBean.getRefuelHistoryBean()) {
                        PostPaidRefuel postPaidRefuel = new PostPaidRefuel();
                        postPaidRefuel.setFuelAmount(postPaidRefuelHistoryBean.getFuelAmount());
                        postPaidRefuel.setFuelQuantity(postPaidRefuelHistoryBean.getFuelQuantity());
                        postPaidRefuel.setFuelType(postPaidRefuelHistoryBean.getFuelType());
                        postPaidRefuel.setProductDescription(postPaidRefuelHistoryBean.getProductDescription());
                        postPaidRefuel.setProductId(postPaidRefuelHistoryBean.getProductId());
                        postPaidRefuel.setPumpId(postPaidRefuelHistoryBean.getPumpId());
                        popGetTransactionDetailResponse.getRefuelList().add(postPaidRefuel);
                    }
                }
                
                if (!poPTransactionHistoryBean.getPostPaidConsumeVoucherBeanList().isEmpty()) {

                    System.out.println("Inizio lettura dati voucher");
                   
                    for (PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean : poPTransactionHistoryBean.getPostPaidConsumeVoucherBeanList()) {

                        PostPaidConsumeVoucher postPaidConsumeVoucher = new PostPaidConsumeVoucher();
                        postPaidConsumeVoucher.setCsTransactionID(postPaidConsumeVoucherHistoryBean.getCsTransactionID());
                        postPaidConsumeVoucher.setOperationID(postPaidConsumeVoucherHistoryBean.getOperationID());
                        postPaidConsumeVoucher.setOperationIDReversed(postPaidConsumeVoucherHistoryBean.getOperationIDReversed());
                        postPaidConsumeVoucher.setOperationType(postPaidConsumeVoucherHistoryBean.getOperationType());
                        postPaidConsumeVoucher.setRequestTimestamp(postPaidConsumeVoucherHistoryBean.getRequestTimestamp());
                        postPaidConsumeVoucher.setMarketingMsg(postPaidConsumeVoucherHistoryBean.getMarketingMsg());
                        postPaidConsumeVoucher.setWarningMsg(postPaidConsumeVoucherHistoryBean.getWarningMsg());
                        postPaidConsumeVoucher.setTotalConsumed(postPaidConsumeVoucherHistoryBean.getTotalConsumed());
                        postPaidConsumeVoucher.setReconciled(postPaidConsumeVoucherHistoryBean.getReconciled());

                        for (PostPaidConsumeVoucherDetailHistoryBean postPaidConsumeVoucherDetailHistoryBean : postPaidConsumeVoucherHistoryBean.getPostPaidConsumeVoucherDetailHistoryBean()) {

                            System.out.println("Inizio inserimento voucher");

                            PostPaidConsumeVoucherDetail postPaidConsumeVoucherDetail = new PostPaidConsumeVoucherDetail();
                            postPaidConsumeVoucherDetail.setConsumedValue(postPaidConsumeVoucherDetailHistoryBean.getConsumedValue());
                            postPaidConsumeVoucherDetail.setExpirationDate(postPaidConsumeVoucherDetailHistoryBean.getExpirationDate());
                            postPaidConsumeVoucherDetail.setInitialValue(postPaidConsumeVoucherDetailHistoryBean.getInitialValue());
                            postPaidConsumeVoucherDetail.setPromoCode(postPaidConsumeVoucherDetailHistoryBean.getPromoCode());
                            postPaidConsumeVoucherDetail.setPromoDescription(postPaidConsumeVoucherDetailHistoryBean.getPromoDescription());
                            postPaidConsumeVoucherDetail.setPromoDoc(postPaidConsumeVoucherDetailHistoryBean.getPromoDoc());
                            postPaidConsumeVoucherDetail.setVoucherBalanceDue(postPaidConsumeVoucherDetailHistoryBean.getVoucherBalanceDue());
                            postPaidConsumeVoucherDetail.setVoucherCode(postPaidConsumeVoucherDetailHistoryBean.getVoucherCode());
                            postPaidConsumeVoucherDetail.setVoucherStatus(postPaidConsumeVoucherDetailHistoryBean.getVoucherStatus());
                            postPaidConsumeVoucherDetail.setVoucherType(postPaidConsumeVoucherDetailHistoryBean.getVoucherType());
                            postPaidConsumeVoucherDetail.setVoucherValue(postPaidConsumeVoucherDetailHistoryBean.getVoucherValue());
                            
                            postPaidConsumeVoucher.getPostPaidConsumeVoucherDetail().add(postPaidConsumeVoucherDetail);
                            
                            System.out.println("Fine inserimento voucher");

                        }

                        popGetTransactionDetailResponse.getPostPaidConsumeVoucherList().add(postPaidConsumeVoucher);
                        
                    }
                    System.out.println("Fine lettura dati voucher");
                }
                
                if (!poPTransactionHistoryBean.getPostPaidLoadLoyaltyCreditsBeanList().isEmpty()) {

                    System.out.println("Inizio lettura dati loyalty");
                    
                    for (PostPaidLoadLoyaltyCreditsHistoryBean postPaidLoadLoyaltyCreditsHistoryBean : poPTransactionHistoryBean.getPostPaidLoadLoyaltyCreditsBeanList()) {

                        PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits = new PostPaidLoadLoyaltyCredits();
                        postPaidLoadLoyaltyCredits.setBalance(postPaidLoadLoyaltyCreditsHistoryBean.getBalance());
                        postPaidLoadLoyaltyCredits.setBalanceAmount(postPaidLoadLoyaltyCreditsHistoryBean.getBalanceAmount());
                        postPaidLoadLoyaltyCredits.setCardClassification(postPaidLoadLoyaltyCreditsHistoryBean.getCardClassification());
                        postPaidLoadLoyaltyCredits.setCardCodeIssuer(postPaidLoadLoyaltyCreditsHistoryBean.getCardCodeIssuer());
                        postPaidLoadLoyaltyCredits.setCardStatus(postPaidLoadLoyaltyCreditsHistoryBean.getCardStatus());
                        postPaidLoadLoyaltyCredits.setCardType(postPaidLoadLoyaltyCreditsHistoryBean.getCardType());
                        postPaidLoadLoyaltyCredits.setCredits(postPaidLoadLoyaltyCreditsHistoryBean.getCredits());
                        postPaidLoadLoyaltyCredits.setCsTransactionID(postPaidLoadLoyaltyCreditsHistoryBean.getCsTransactionID());
                        postPaidLoadLoyaltyCredits.setEanCode(postPaidLoadLoyaltyCreditsHistoryBean.getEanCode());
                        postPaidLoadLoyaltyCredits.setMarketingMsg(postPaidLoadLoyaltyCreditsHistoryBean.getMarketingMsg());
                        postPaidLoadLoyaltyCredits.setMessageCode(postPaidLoadLoyaltyCreditsHistoryBean.getMessageCode());
                        postPaidLoadLoyaltyCredits.setOperationID(postPaidLoadLoyaltyCreditsHistoryBean.getOperationID());
                        postPaidLoadLoyaltyCredits.setOperationType(postPaidLoadLoyaltyCreditsHistoryBean.getOperationType());
                        postPaidLoadLoyaltyCredits.setRequestTimestamp(postPaidLoadLoyaltyCreditsHistoryBean.getRequestTimestamp());
                        postPaidLoadLoyaltyCredits.setStatusCode(postPaidLoadLoyaltyCreditsHistoryBean.getStatusCode());

                        popGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList().add(postPaidLoadLoyaltyCredits);
                                                
                    }
                    
                    System.out.println("Fine lettura dati loyalty");
                }                

            }

            userTransaction.commit();

            return popGetTransactionDetailResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED Post Payed transaction get with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
    /*
    private ReceiptData addReceiptData(String key, String label, String unit, String value, int position, String parent, String link) {

        ReceiptData receiptData = new ReceiptData();

        receiptData.setLabel(label);
        receiptData.setKey(key);
        receiptData.setUnit(unit);
        receiptData.setValue(value);
        receiptData.setPosition(position);
        receiptData.setParent(parent);
        receiptData.setLink(link);

        return receiptData;
    }
    */
}
