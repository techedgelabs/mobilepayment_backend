package com.techedge.mp.core.actions.manager;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.model.ManagerTicketBean;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ManagerUpdatePasswordAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public ManagerUpdatePasswordAction() {}

    public String execute(String ticketId, String requestId, String oldPassword, String newPassword) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            ManagerTicketBean ticketBean = QueryRepository.findManagerTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isStandardTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.MANAGER_REQU_INVALID_TICKET;
            }

            ManagerBean managerBean = ticketBean.getManagerBean();
            if (managerBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.MANAGER_REQU_INVALID_TICKET;
            }

            // Verifica lo stato dell'utente
            Integer status = managerBean.getStatus();
            if (status != Manager.STATUS_ACTIVE && status != Manager.STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare
                // questo
                // servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user password in status " + status);

                userTransaction.commit();

                return ResponseHelper.MANAGER_PWD_USER_NOT_ACTIVE;
            }

            // Verifica la nuova password
            // TODO

            if (managerBean.getUsername().equals(newPassword)) {

                // La nuova password coincide con la username
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "new password = username");

                userTransaction.commit();

                return ResponseHelper.MANAGER_PWD_USERNAME_PASSWORD_EQUALS;
            }

            Date checkDate = new Date();
            if (!managerBean.getPassword().equals(oldPassword)
                    && (managerBean.getExpirationDateRescuePassword() == null || managerBean.getExpirationDateRescuePassword().before(checkDate) || !managerBean.getRescuePassword().equals(oldPassword))) {

                // La old password non coincide con quella dell'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Old password wrong");

                userTransaction.commit();

                return ResponseHelper.MANAGER_PWD_OLD_ERROR;

            }

            // Codifica la password
            String encodedPassword = EncoderHelper.encode(newPassword);
            if (managerBean.getPassword().equals(encodedPassword)) {

                // La nuova password coincide con quella vecchia
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "New password = old password");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_OLD_NEW_EQUALS;
            }

            // Aggiorna la password
            managerBean.setPassword(encodedPassword);
            managerBean.setRescuePassword(null);
            managerBean.setExpirationDateRescuePassword(null);

            if (managerBean.getStatus() == Manager.STATUS_TEMPORARY_PASSWORD) {
                managerBean.setStatus(Manager.STATUS_ACTIVE);
            }

            // Aggiorna i dati dell'utente
            em.merge(managerBean);

            // Rinnova il ticket
            ticketBean.renew();
            em.merge(ticketBean);

            userTransaction.commit();

            return ResponseHelper.MANAGER_PWD_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED password update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
