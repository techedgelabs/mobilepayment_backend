package com.techedge.mp.core.actions.manager;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.ManagerAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.model.ManagerTicketBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ManagerAuthenticationAction {

  @Resource
  private EJBContext context;

  @PersistenceContext(unitName = "CrudPU")
  private EntityManager em;

  @EJB
  private LoggerService loggerService;

  public ManagerAuthenticationAction() {}

  public ManagerAuthenticationResponse execute(String username, String password, String requestId, String deviceId, String deviceName, long maxPendingInterval, Integer ticketExpiryTime, Integer loginAttemptsLimit, Integer loginLockExpiryTime)
      throws EJBException {

    UserTransaction userTransaction = context.getUserTransaction();
    ManagerAuthenticationResponse authenticationResponse = null;
    ManagerBean managerBean = null;

    try {
      userTransaction.begin();

      managerBean = QueryRepository.findManagerByUsername(em, username);

      if (managerBean == null) {

        System.out.println("manager not found");

        // Se l'utente non esiste resitituisci il messaggio di errore
        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Manager " + username + " not found");
        authenticationResponse = new ManagerAuthenticationResponse();
        authenticationResponse.setStatusCode(ResponseHelper.MANAGER_AUTH_LOGIN_ERROR);

        userTransaction.commit();
        return authenticationResponse;
      }
      else {

        // Verifica lo stato dell'utente
        Integer status = managerBean.getStatus();
        if (status != Manager.STATUS_ACTIVE) {

          // Un utente che si trova in questo stato non pu� effettuare il login
          this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: Manager "
              + username + " in status " + status);
          authenticationResponse = new ManagerAuthenticationResponse();
          authenticationResponse.setStatusCode(ResponseHelper.MANAGER_AUTH_UNAUTHORIZED);

          userTransaction.commit();

          return authenticationResponse;
        }

        if (managerBean.getType() == Manager.TYPE_STANDARD) {

          if (managerBean.getPassword().equals(password)) {

            // 1) crea un nuovo ticket
            Manager manager = managerBean.toManager();

            Date now = new Date();
            Date lastUsed = now;
            Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

            String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

            ManagerTicketBean ticketBean = new ManagerTicketBean(newTicketId, managerBean, ManagerTicketBean.TYPE_STANDARD, now, lastUsed, expiryDate);

            em.persist(ticketBean);

            // Cancellazione delle informazioni per il recupero della password
            managerBean.setExpirationDateRescuePassword(null);
            managerBean.setRescuePassword(null);
            // TODO verificare se serve em.merge(personalDataBean);

            managerBean.setStatus(Manager.STATUS_ACTIVE);
            manager.setStatus(Manager.STATUS_ACTIVE);
            
            em.merge(managerBean);            

            // 2) restituisci il risultato
            authenticationResponse = new ManagerAuthenticationResponse();

            authenticationResponse.setStatusCode(ResponseHelper.MANAGER_AUTH_SUCCESS);
            authenticationResponse.setTicketId(ticketBean.getTicketId());
            authenticationResponse.setManager(manager);

            userTransaction.commit();

            return authenticationResponse;
          }
          else {

            Date checkDate = new Date();

            if (managerBean.getExpirationDateRescuePassword() != null && managerBean.getExpirationDateRescuePassword().after(checkDate)
                && managerBean.getRescuePassword().equals(password)) {

              // Tutti i ticket attivi associati all'utente devono essere
              // invalidati
              List<ManagerTicketBean> ticketBeanList = QueryRepository.findManagerTicketByManagerAndCheckDate(em, managerBean, checkDate);

              if (ticketBeanList != null && ticketBeanList.size() > 0) {

                for (ManagerTicketBean ticketBeanLoop : ticketBeanList) {

                  ticketBeanLoop.setExpirationTimestamp(checkDate);
                  em.merge(ticketBeanLoop);
                }
              }

              Manager manager = managerBean.toManager();

              Date now = new Date();
              Date lastUsed = now;
              Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

              String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

              ManagerTicketBean ticketBean = new ManagerTicketBean(newTicketId, managerBean, ManagerTicketBean.TYPE_STANDARD, now, lastUsed, expiryDate);

              em.persist(ticketBean);

              managerBean.setStatus(Manager.STATUS_TEMPORARY_PASSWORD);
              manager.setStatus(Manager.STATUS_TEMPORARY_PASSWORD);

              em.merge(managerBean);

              // 5) restituisci il risultato
              authenticationResponse = new ManagerAuthenticationResponse();

              authenticationResponse.setStatusCode(ResponseHelper.MANAGER_AUTH_SUCCESS);
              authenticationResponse.setTicketId(ticketBean.getTicketId());
              authenticationResponse.setManager(manager);

              userTransaction.commit();

              return authenticationResponse;
            }
            else {

              // La password inserita non � corretta
              this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong password for user "
                  + username);

              authenticationResponse = new ManagerAuthenticationResponse();
              authenticationResponse.setStatusCode(ResponseHelper.MANAGER_AUTH_LOGIN_ERROR);

              userTransaction.commit();

              return authenticationResponse;
            }
          }
        }
        else {

          // Un utente che si trova in questo stato non pu� effettuare il login
          this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: Manager "
              + username + " in status " + status);
 
          authenticationResponse = new ManagerAuthenticationResponse();
          authenticationResponse.setStatusCode(ResponseHelper.MANAGER_AUTH_UNAUTHORIZED);

          userTransaction.commit();

          return authenticationResponse;
        }
      }
    }
    catch (Exception ex2) {

      try {
        userTransaction.rollback();
      }
      catch (IllegalStateException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      catch (SecurityException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      catch (SystemException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
      this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

      throw new EJBException(ex2);
    }
  }
}
