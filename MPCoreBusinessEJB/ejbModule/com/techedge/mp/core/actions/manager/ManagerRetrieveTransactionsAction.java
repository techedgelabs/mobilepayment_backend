package com.techedge.mp.core.actions.manager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ConstantsHelper;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ManagerRetrieveTransactionsResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PaymentHistory;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCartData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionInfo;
import com.techedge.mp.core.business.model.ManagerTicketBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ManagerRetrieveTransactionsAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public ManagerRetrieveTransactionsResponse execute(String ticketId, String requestId, String stationID, Integer pumpMaxTransactions, Integer transactionsInterval, ForecourtInfoServiceRemote forecourtInfoService)
            throws EJBException {
        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            ManagerTicketBean ticketBean = QueryRepository.findManagerTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isStandardTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                ManagerRetrieveTransactionsResponse managerResponse = new ManagerRetrieveTransactionsResponse();
                managerResponse.setStatusCode(ResponseHelper.MANAGER_REQU_INVALID_TICKET);
                return managerResponse;
            }

            if (stationID == null || stationID.equals("")) {
                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid stationID");

                userTransaction.commit();

                ManagerRetrieveTransactionsResponse managerResponse = new ManagerRetrieveTransactionsResponse();
                managerResponse.setStatusCode(ResponseHelper.MANAGER_RETRIEVE_TRANSACTIONS_INVALID_STATION_ID);
                return managerResponse;
            }

            StationBean stationBean = null;

            for (StationBean tmpStationBean : ticketBean.getManagerBean().getStations()) {
                if (tmpStationBean.getStationID().equals(stationID)) {
                    stationBean = tmpStationBean;
                    break;
                }
            }

            if (stationBean == null) {
                // Stazione non trovata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Station not found");

                userTransaction.commit();

                ManagerRetrieveTransactionsResponse managerResponse = new ManagerRetrieveTransactionsResponse();
                managerResponse.setStatusCode(ResponseHelper.MANAGER_RETRIEVE_TRANSACTIONS_INVALID_STATION_ID);
                return managerResponse;
            }

            ManagerRetrieveTransactionsResponse managerResponse = new ManagerRetrieveTransactionsResponse();
            Calendar calendar = Calendar.getInstance();
            Date endDate = calendar.getTime();
            long endTimestamp = calendar.getTimeInMillis() - (transactionsInterval.longValue() * 1000);
            calendar.setTimeInMillis(endTimestamp);
            Date startDate = calendar.getTime();
            this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "execute", requestId, null, "StartDate:" + startDate + "  EndDate:" + endDate);

            List<PostPaidTransactionHistoryBean> transactionHistoryBeanList = new ArrayList<PostPaidTransactionHistoryBean>(0);

            List<String> pumpList = QueryRepository.findDistinctPostPaidTransactionSourceIdByStationId(em, stationBean);
            List<String> pumpList2 = QueryRepository.findDistinctPostPaidTransactionHistorySourceIdByStationId(em, stationBean);

            for (String pumpId : pumpList2) {
                if (!pumpList.contains(pumpId)) {
                    pumpList.add(pumpId);
                }
            }

            /*
             * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invocazione servizio getStationDetails per stationID: "
             * + stationID);
             * GetStationDetailsResponse getStationDetailsResponse = forecourtInfoService.getStationDetails(requestId, stationID, null, false);
             * GetStationDetailsResponse getStationDetailsResponse = new GetStationDetailsResponse();
             * StationDetail stationDetail = new StationDetail();
             * PumpDetail pump = new PumpDetail();
             * pump.setPumpID("P00000108");
             * stationDetail.getPumpDetails().add(pump);
             * getStationDetailsResponse.setStationDetail(stationDetail);
             * getStationDetailsResponse.setStatusCode("STATION_PUMP_FOUND_200");
             */

            //            if (getStationDetailsResponse.getStatusCode().equals("STATION_PUMP_FOUND_200")) {
            //                managerResponse.setStatusCode(getStationDetailsResponse.getStatusCode());
            //                if (!getStationDetailsResponse.getStationDetail().getPumpDetails().isEmpty()) {
            //                    for (PumpDetail pumpDetail : getStationDetailsResponse.getStationDetail().getPumpDetails()) {
            //                        String pumpID = pumpDetail.getPumpID();
            if (pumpList != null && !pumpList.isEmpty()) {
                for (String pumpID : pumpList) {
                    List<PostPaidTransactionBean> postPaidTransactionList = QueryRepository.findLastPostPaidTransactionBySourceId(em, pumpID, startDate, endDate, pumpMaxTransactions);
                    List<PostPaidTransactionHistoryBean> postPaidTransactionHistoryList = QueryRepository.findLastPostPaidTransactionHistoryBySourceId(em, pumpID, startDate, endDate, pumpMaxTransactions);

                    String debug = "Erogatore : " + pumpID + " trovato per la stationID: " + stationID + " - Trovate " + postPaidTransactionList.size() + " transazioni postpaid "
                            + " - Trovate " + postPaidTransactionHistoryList.size() + " transazioni history postpaid";
                    this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "execute", requestId, null, debug);

                    if (postPaidTransactionHistoryList != null && !postPaidTransactionHistoryList.isEmpty()) {
                        transactionHistoryBeanList.addAll(postPaidTransactionHistoryList);
                    }

                    if (postPaidTransactionList != null && !postPaidTransactionList.isEmpty()) {
                        for (PostPaidTransactionBean postPaidTransactionBean : postPaidTransactionList) {
                            PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = new PostPaidTransactionHistoryBean(postPaidTransactionBean);
                            transactionHistoryBeanList.add(postPaidTransactionHistoryBean);
                        }

                        Collections.sort(transactionHistoryBeanList, new Comparator<PostPaidTransactionHistoryBean>() {
                            public int compare(PostPaidTransactionHistoryBean synchronizedListOne, PostPaidTransactionHistoryBean synchronizedListTwo) {
                                // use instanceof to verify the references are indeed of the type in
                                // question
                                return synchronizedListOne.getCreationTimestamp().compareTo(synchronizedListTwo.getCreationTimestamp());
                            }
                        });
                        Collections.reverse(transactionHistoryBeanList);
                    }
                }
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "No pumps found for stationID: " + stationID + " for Manager "
                        + ticketBean.getManagerBean().getUsername());
                managerResponse.setStatusCode(ResponseHelper.MANAGER_STATION_FAILURE);
            }

            int index = 0;
            for (PostPaidTransactionHistoryBean postPaidTransactionHistoryBean : transactionHistoryBeanList) {
                PaymentHistory paymentHistory = new PaymentHistory();
                PostPaidTransactionInfo transactionInfo = new PostPaidTransactionInfo();
                transactionInfo.setMpTransactionID(transactionInfo.getMpTransactionID());
                paymentHistory.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_FUEL);
                paymentHistory.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_POST_PAY);
                transactionInfo.setTransactionID(postPaidTransactionHistoryBean.getMpTransactionID());
                transactionInfo.setStatus(postPaidTransactionHistoryBean.getMpTransactionStatus());
                transactionInfo.setStatusType(postPaidTransactionHistoryBean.getStatusType());
                transactionInfo.setAmount(postPaidTransactionHistoryBean.getAmount());
                
                System.out.println("transazione: " + transactionInfo.getMpTransactionID() + ", amount: " + transactionInfo.getAmount());

                if (postPaidTransactionHistoryBean.getLastPostPaidConsumeVoucherBean() != null) {

                    PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean = postPaidTransactionHistoryBean.getLastPostPaidConsumeVoucherBean();
                    transactionInfo.setVoucherAmount(postPaidConsumeVoucherHistoryBean.getTotalConsumed());
                }

                if (postPaidTransactionHistoryBean.getLastPostPaidLoadLoyaltyCreditsBean() != null) {

                    PostPaidLoadLoyaltyCreditsHistoryBean lastPostPaidLoadLoyaltyCreditsHistoryBean = postPaidTransactionHistoryBean.getLastPostPaidLoadLoyaltyCreditsBean();
                    transactionInfo.setLoyaltyBalance(lastPostPaidLoadLoyaltyCreditsHistoryBean.getBalance());
                }

                transactionInfo.setCreationTimestamp(postPaidTransactionHistoryBean.getCreationTimestamp());

                if (postPaidTransactionHistoryBean.getCartHistoryBean() != null && !postPaidTransactionHistoryBean.getCartHistoryBean().isEmpty()) {
                    for (PostPaidCartHistoryBean cartHistoryBean : postPaidTransactionHistoryBean.getCartHistoryBean()) {
                        PostPaidCartData cartData = new PostPaidCartData();
                        cartData.setProductId(cartHistoryBean.getProductId());
                        cartData.setProductDescription(cartHistoryBean.getProductDescription());
                        cartData.setAmount(cartHistoryBean.getAmount());
                        cartData.setQuantity(cartHistoryBean.getQuantity());
                        transactionInfo.getCart().add(cartData);
                    }
                }

                if (postPaidTransactionHistoryBean.getRefuelHistoryBean() != null && !postPaidTransactionHistoryBean.getRefuelHistoryBean().isEmpty()) {
                    for (PostPaidRefuelHistoryBean refuelHistoryBean : postPaidTransactionHistoryBean.getRefuelHistoryBean()) {
                        PostPaidRefuelData refuelData = new PostPaidRefuelData();
                        refuelData.setPumpId(refuelHistoryBean.getPumpId());
                        refuelData.setPumpNumber(refuelHistoryBean.getPumpNumber());
                        refuelData.setProductId(refuelHistoryBean.getProductId());
                        refuelData.setProductDescription(refuelHistoryBean.getProductDescription());
                        refuelData.setFuelType(refuelHistoryBean.getFuelType());
                        refuelData.setFuelAmount(refuelHistoryBean.getFuelAmount());
                        refuelData.setFuelQuantity(refuelHistoryBean.getFuelQuantity());
                        transactionInfo.getRefuel().add(refuelData);
                    }
                }

                paymentHistory.setTransaction(transactionInfo);
                managerResponse.getTransactionHistory().add(paymentHistory);

                if (index == pumpMaxTransactions.intValue())
                    break;

                index++;
            }

            userTransaction.commit();

            managerResponse.setStatusCode(ResponseHelper.MANAGER_RETRIEVE_TRANSACTIONS_SUCCESS);
            return managerResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve transactions with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }

    }

}
