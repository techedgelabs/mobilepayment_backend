package com.techedge.mp.core.business;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.techedge.mp.core.actions.dwh.ActivatePromotionAction;
import com.techedge.mp.core.actions.dwh.UpdatePasswordAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;

/**
 * Session Bean implementation class UserManager
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class DWHService implements DWHServiceRemote, DWHServiceLocal {

    private static final String  PARAM_PASSWORD_HISTORY_LENGTH     = "PASSWORD_HISTORY_LENGTH";
    private static final String  PARAM_RECONCILIATION_MAX_ATTEMPTS = "RECONCILIATION_MAX_ATTEMPTS";
    private static final String  PARAM_EMAIL_SENDER_ADDRESS        = "EMAIL_SENDER_ADDRESS";
    private static final String  PARAM_SMTP_HOST                   = "SMTP_HOST";
    private static final String  PARAM_SMTP_PORT                   = "SMTP_PORT";
    private static final String  PARAM_PROXY_HOST                  = "PROXY_HOST";
    private static final String  PARAM_PROXY_PORT                  = "PROXY_PORT";
    private static final String  PARAM_PROXY_NO_HOSTS              = "PROXY_NO_HOSTS";
    private static final String  PARAM_LANDING_WELCOME_VODAFONE    = "LANDING_WELCOME_VODAFONE";
    private static final String  PARAM_PROMOTION_ES_VODA_START     = "PROMOTION_ES_VODA_START";
    private static final String  PARAM_PROMOTION_ES_VODA_END       = "PROMOTION_ES_VODA_END";

    @EJB
    private UpdatePasswordAction updatePasswordAction;
    
    @EJB
    private ActivatePromotionAction activatePromotionAction;

    private ParametersService             parametersService                 = null;
    private LoggerService                 loggerService                     = null;
    private UserCategoryService           userCategoryService               = null;
    private EmailSenderRemote             emailSender                       = null;
    private PushNotificationServiceRemote pushNotificationService           = null;

    /**
     * Default constructor.
     */
    public DWHService() throws EJBException {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface Logger not found: " + e.getMessage());
            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface Parameters not found: " + e.getMessage());
            throw new EJBException(e);
        }
        
        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface UserCategory not found: " + e.getMessage());
            throw new EJBException(e);
        }
        
        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface EmailSender not found: " + e.getMessage());
            throw new EJBException(e);
        }
        
        try {
            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface PushNotificationService not found: " + e.getMessage());
            throw new EJBException(e);
        }
    }

    @Override
    public String updatePassword(String operationID, Long requestTimestamp, String fiscalCode, String email, String oldPassword, String newPassword) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        inputParameters.add(new Pair<String, String>("email", email));
        inputParameters.add(new Pair<String, String>("oldPassword", oldPassword));
        inputParameters.add(new Pair<String, String>("newPassword", newPassword));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePassword", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            Integer passwordHistoryLenght = Integer.valueOf(this.parametersService.getParamValue(DWHService.PARAM_PASSWORD_HISTORY_LENGTH));
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(DWHService.PARAM_RECONCILIATION_MAX_ATTEMPTS));

            response = updatePasswordAction.execute(operationID, requestTimestamp, fiscalCode, email, oldPassword, newPassword, passwordHistoryLenght, 
                    reconciliationMaxAttempts, this.userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePassword", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

	@Override
	public String activatePromotion(String promotionID, String fiscalCode) {
		Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("promotionID", promotionID));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "activatePromotion", promotionID, "opening", ActivityLog.createLogMessage(inputParameters));
        
        String response = null;
        try {
            String emailSenderAddress = this.parametersService.getParamValue(DWHService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(DWHService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(DWHService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(DWHService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(DWHService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(DWHService.PARAM_PROXY_NO_HOSTS);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(DWHService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            String landingWelcomeVodafone = this.parametersService.getParamValue(DWHService.PARAM_LANDING_WELCOME_VODAFONE);
            Long promoVodafoneStart = Long.valueOf(this.parametersService.getParamValue(DWHService.PARAM_PROMOTION_ES_VODA_START));
            Long promoVodafoneEnd = Long.valueOf(this.parametersService.getParamValue(DWHService.PARAM_PROMOTION_ES_VODA_END));
            
            emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
            
        	response = activatePromotionAction.execute(promotionID, fiscalCode, emailSender, pushNotificationService, maxRetryAttemps, landingWelcomeVodafone, promoVodafoneStart, promoVodafoneEnd);
        }
        catch (ParameterNotFoundException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }
        
        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "activatePromotion", promotionID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
	}

}
