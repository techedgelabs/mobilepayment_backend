package com.techedge.mp.core.business;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.actions.crm.GetMissionsAction;
import com.techedge.mp.core.actions.crm.InsertBulkContactKeyAction;
import com.techedge.mp.core.actions.crm.ProcessCrmOutboundInterfaceAction;
import com.techedge.mp.core.actions.crm.ProcessCrmSfOutboundInterfaceAction;
import com.techedge.mp.core.actions.crm.ProcessEventAction;
import com.techedge.mp.core.actions.crm.Promo4MeAction;
import com.techedge.mp.core.actions.crm.SendNotifyEventOfferAction;
import com.techedge.mp.core.actions.crm.UpdateContactKeyAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElement;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElementProcessingResult;
import com.techedge.mp.core.business.interfaces.crm.CrmSfDataElement;
import com.techedge.mp.core.business.interfaces.crm.GetMissionsResponse;
import com.techedge.mp.core.business.interfaces.crm.GetOffersResult;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.ProcessCrmOutboundInterfaceResponse;
import com.techedge.mp.core.business.interfaces.crm.ProcessEventResult;
import com.techedge.mp.core.business.interfaces.crm.Promo4MeResponse;
import com.techedge.mp.core.business.interfaces.crm.StatusCodeEnum;
import com.techedge.mp.core.business.interfaces.crm.UpdateContactKeyBulkResult;
import com.techedge.mp.core.business.interfaces.crm.UpdateContactKeyResult;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfEventBean;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.crm.adapter.interfaces.NotifyEventResult;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;

/**
 * Session Bean implementation class UserManager
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class CRMService implements CRMServiceRemote, CRMServiceLocal {
	
	private final static String UNIT_NAME = "CrudPU";
	
	@Resource
    private EJBContext                          context;
	
    @PersistenceContext(unitName = UNIT_NAME)
    private EntityManager em;

    @EJB
    private SendNotifyEventOfferAction        sendNotifyEventOfferAction;

    @EJB
    private ProcessCrmOutboundInterfaceAction processCrmOutboundInterfaceAction;
    
    @EJB
    private ProcessCrmSfOutboundInterfaceAction processCrmSfOutboundInterfaceAction;

    @EJB
    private Promo4MeAction                    promo4MeAction;
    
    @EJB
    private GetMissionsAction                 missionsAction;
    
    @EJB
    private ProcessEventAction                processEventAction;
    
    @EJB
    private UpdateContactKeyAction            updateContactKeyAction;
    
    @EJB
    private InsertBulkContactKeyAction        insertBulkContactKeyAction;
    

    private final static String               PARAM_RECONCILIATION_MAX_ATTEMPTS           = "RECONCILIATION_MAX_ATTEMPTS";
    private final static String               PARAM_PUSH_NOTIFICATION_EXPIRY_TIME         = "PUSH_NOTIFICATION_MAX_EXPIRY_TIME";
    private final static String               PARAM_CRM_USE_EXECUTE_BATCH                 = "CRM_USE_EXECUTE_BATCH";

    private final static String               PARAM_CRM_INTERACTION_POINT_REGISTRATION    = "CRM_INTERACTION_POINT_REGISTRATION";
    private final static String               PARAM_CRM_INTERACTION_POINT_LOYALTY_CREDITS = "CRM_INTERACTION_POINT_LOYALTY_CREDITS";
    private final static String               PARAM_CRM_INTERACTION_POINT_REFUELING       = "CRM_INTERACTION_POINT_REFUELING";
    private final static String               PARAM_CRM_INTERACTION_POINT_CREDIT_CARD     = "CRM_INTERACTION_POINT_CREDIT_CARD";
    private final static String               PARAM_CRM_INTERACTION_POINT_PROMO4ME_1      = "CRM_INTERACTION_POINT_PROMO4ME_1";
    private final static String               PARAM_CRM_INTERACTION_POINT_PROMO4ME_2      = "CRM_INTERACTION_POINT_PROMO4ME_2";
    private final static String               PARAM_CRM_INTERACTION_POINT_PROMO4ME_3      = "CRM_INTERACTION_POINT_PROMO4ME_3";
    private final static String               PARAM_CRM_INTERACTION_POINT_PROMO4ME_4      = "CRM_INTERACTION_POINT_PROMO4ME_4";
    private final static String               PARAM_CRM_INTERACTION_POINT_PROMO4ME_5      = "CRM_INTERACTION_POINT_PROMO4ME_5";
    private final static String               PARAM_CRM_INTERACTION_POINT_PROMO4ME_6      = "CRM_INTERACTION_POINT_PROMO4ME_6";
    private final static String               PARAM_CRM_INTERACTION_POINT_MISSION         = "CRM_INTERACTION_POINT_MISSION";
    private final static String               PARAM_CRM_PROCESS_CHECK_EMPTY_TABLE         = "CRM_PROCESS_CHECK_EMPTY_TABLE";
    private final static String               PARAM_CRM_PROCESS_MAX_DATA_PER_THREAD       = "CRM_PROCESS_MAX_DATA_PER_THREAD";
    private final static String               PARAM_CRM_PROCESS_MAX_THREADS               = "CRM_PROCESS_MAX_THREADS";
    
    

    private ParametersService                 parametersService                           = null;
    private LoggerService                     loggerService                               = null;
    private PushNotificationServiceRemote     pushNotificationService                     = null;
    private CRMAdapterServiceRemote           crmAdapterService                           = null;
    private FidelityServiceRemote             fidelityService                             = null;
    private UserCategoryService               userCategoryService                         = null;
    

    /**
     * Default constructor.
     */
    public CRMService() throws EJBException {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface Logger not found: " + e.getMessage());
            throw new EJBException(e);
        }

        try {
            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface PushNotification not found: " + e.getMessage());
            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface Parameters not found: " + e.getMessage());
            throw new EJBException(e);
        }

        try {
            this.crmAdapterService = EJBHomeCache.getInstance().getCRMAdapterService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface CRMAdapter not found: " + e.getMessage());
            throw new EJBException(e);
        }
        
        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface FidelityService not found: " + e.getMessage());
            throw new EJBException(e);
        }

        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface UserCategoryService not found: " + e.getMessage());
            throw new EJBException(e);
        }
        
        String interactionPointRegistration = null;
        String interactionPointRefueling = null;
        String interactionPointPromo4Me1 = null;
        String interactionPointPromo4Me2 = null;
        String interactionPointPromo4Me3 = null;
        String interactionPointPromo4Me4 = null;
        String interactionPointPromo4Me5 = null;
        String interactionPointPromo4Me6 = null;
        String interactionPointLoyaltyCredits = null;
        String interactionPointCreditCard = null;
        String interactionPointMission = null;
        try {

            interactionPointRegistration = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_REGISTRATION);
            interactionPointRefueling = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_REFUELING);
            interactionPointPromo4Me1 = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_PROMO4ME_1);
            interactionPointPromo4Me2 = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_PROMO4ME_2);
            interactionPointPromo4Me3 = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_PROMO4ME_3);
            interactionPointPromo4Me4 = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_PROMO4ME_4);
            interactionPointPromo4Me5 = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_PROMO4ME_5);
            interactionPointPromo4Me6 = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_PROMO4ME_6);
            interactionPointLoyaltyCredits = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_LOYALTY_CREDITS);
            interactionPointCreditCard = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_CREDIT_CARD);
            interactionPointMission = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_MISSION);

            InteractionPointType.LOYALTY_CREDITS.setPoint(interactionPointLoyaltyCredits);
            InteractionPointType.PROMO4ME_6.setPoint(interactionPointPromo4Me6);
            InteractionPointType.PROMO4ME_5.setPoint(interactionPointPromo4Me5);
            InteractionPointType.PROMO4ME_4.setPoint(interactionPointPromo4Me4);
            InteractionPointType.PROMO4ME_3.setPoint(interactionPointPromo4Me3);
            InteractionPointType.PROMO4ME_2.setPoint(interactionPointPromo4Me2);
            InteractionPointType.PROMO4ME_1.setPoint(interactionPointPromo4Me1);
            InteractionPointType.REFUELING.setPoint(interactionPointRefueling);
            InteractionPointType.REGISTRATION.setPoint(interactionPointRegistration);
            InteractionPointType.CREDIT_CARD.setPoint(interactionPointCreditCard);
            InteractionPointType.MISSION.setPoint(interactionPointMission);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "CRMService", null, null, "Errore nella lettura dei parametri: " + ex.getMessage());
            throw new EJBException(ex);
        }
    }

    @Override
    public String sendNotifyEventOffer(String fiscalCode, InteractionPointType interactionPoint, UserProfile userProfile, Long sourceID, CRMEventSourceType sourceType) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        inputParameters.add(new Pair<String, String>("interactionPoint", interactionPoint.getPoint()));
        inputParameters.add(new Pair<String, String>("sourceID", (sourceID != null ? sourceID.toString() : "")));
        inputParameters.add(new Pair<String, String>("sourceType", (sourceType != null ? sourceType.getValue() : "")));

        String parameterString = "{ ";
        boolean isFirst = true;
        HashMap<String, Object> parameters = userProfile.getParameters();

        for (String key : parameters.keySet()) {
            if (!isFirst) {
                parameterString += "; ";
            }

            parameterString += key + ": " + parameters.get(key);

            if (isFirst) {
                isFirst = false;
            }
        }

        parameterString += " }";

        inputParameters.add(new Pair<String, String>("userProfile", parameterString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendNotifyEventOffer", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        Integer maxRetryAttemps = 5;
        Integer expiryTime = 172800;
        boolean useExecuteBatch = false;

        try {
            maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
            expiryTime = Integer.valueOf(this.parametersService.getParamValue(PARAM_PUSH_NOTIFICATION_EXPIRY_TIME));
            useExecuteBatch = Boolean.parseBoolean(this.parametersService.getParamValue(PARAM_CRM_USE_EXECUTE_BATCH));
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendNotifyEventOffer", null, null, ex.getMessage() + ". Uso del valore di default");
        }

        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();

            response = sendNotifyEventOfferAction.execute(fiscalCode, interactionPoint, userProfile, sourceID, sourceType, expiryTime, maxRetryAttemps, useExecuteBatch, 
                    crmAdapterService, pushNotificationService, fidelityService, userCategoryService);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendNotifyEventOffer", null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendNotifyEventOffer", null, null, "Interface FidelityService not found: " + ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendNotifyEventOffer", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public ProcessCrmOutboundInterfaceResponse processCrmOutboundInterface(List<CrmDataElement> crmDataElementList) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        if (!crmDataElementList.isEmpty()) {
            //inputParameters.add(new Pair<String, String>("ticketID", ticketID));
            //inputParameters.add(new Pair<String, String>("requestID", requestID));
            //inputParameters.add(new Pair<String, String>("interactionPoint", interactionPoint.getValue()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "processCrmOurboundInterface", null, "opening", ActivityLog.createLogMessage(inputParameters));

        ProcessCrmOutboundInterfaceResponse processCrmOutboundInterfaceResponse = new ProcessCrmOutboundInterfaceResponse();

        try {
            boolean checkEmptyTable = Boolean.parseBoolean(this.parametersService.getParamValue(PARAM_CRM_PROCESS_CHECK_EMPTY_TABLE));
            int maxDataPerThread = Integer.parseInt(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_DATA_PER_THREAD));
            int maxThreads = Integer.parseInt(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_THREADS));

            List<CrmDataElementProcessingResult> crmDataElementProcessingResultList = processCrmOutboundInterfaceAction.execute(crmDataElementList, checkEmptyTable, maxDataPerThread, maxThreads);

            for (CrmDataElementProcessingResult crmDataElementProcessingResult : crmDataElementProcessingResultList) {
                processCrmOutboundInterfaceResponse.getCrmDataElementProcessingResultList().add(crmDataElementProcessingResult);
            }

            processCrmOutboundInterfaceResponse.setStatusCode(ResponseHelper.CRM_PROCESS_OUTBOUND_INTERFACE_SUCCESS);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "processCrmOurboundInterface", null, null, ex.getMessage());
            processCrmOutboundInterfaceResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "processCrmOurboundInterface", null, null, ex.getMessage());
            processCrmOutboundInterfaceResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", processCrmOutboundInterfaceResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "processCrmOurboundInterface", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return processCrmOutboundInterfaceResponse;
    }

    @Override
    public Promo4MeResponse getPromo4Me(String ticketID, String requestID) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPromo4Me", null, "opening", ActivityLog.createLogMessage(inputParameters));

        Promo4MeResponse response = null;
        boolean useExecuteBatch = false;
        Integer maxRetryAttemps = 5;

        try {
            useExecuteBatch = Boolean.parseBoolean(this.parametersService.getParamValue(PARAM_CRM_USE_EXECUTE_BATCH));
            maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getPromo4Me", null, null, ex.getMessage() + ". Uso del valore di default");
        }

        try {
            response = promo4MeAction.execute(ticketID, requestID, useExecuteBatch, maxRetryAttemps, crmAdapterService, userCategoryService);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getPromo4Me", requestID, null, ex.getMessage());
            response = new Promo4MeResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPromo4Me", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
    
    @Override
    public GetMissionsResponse getMissions(String ticketID, String requestID) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMissions", null, "opening", ActivityLog.createLogMessage(inputParameters));

        GetMissionsResponse response = null;
        Integer maxRetryAttemps = 5;

        try {
            maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getMissions", null, null, ex.getMessage() + ". Uso del valore di default");
        }

        try {
            response = missionsAction.execute(ticketID, requestID, maxRetryAttemps, crmAdapterService,InteractionPointType.MISSION.getPoint());
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getMissions", requestID, null, ex.getMessage());
            response = new GetMissionsResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMissions", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public ProcessEventResult processEvent(String operationId, String requestId, String fiscalCode, String promoCode, Double voucherAmount) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationId", operationId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        inputParameters.add(new Pair<String, String>("promoCode", promoCode));
        if (voucherAmount != null) {
            inputParameters.add(new Pair<String, String>("voucherAmount", voucherAmount.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("voucherAmount", "null"));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "processEvent", operationId, "opening", ActivityLog.createLogMessage(inputParameters));

        ProcessEventResult processEventResult = null;
        /*
        try {
            maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getMissions", null, null, ex.getMessage() + ". Uso del valore di default");
        }
        */

        try {
            processEventResult = processEventAction.execute(operationId, requestId, fiscalCode, promoCode, voucherAmount, this.fidelityService, this.userCategoryService);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "processEvent", operationId, null, ex.getMessage());
            processEventResult = new ProcessEventResult();
            processEventResult.setStatusCode(StatusCodeEnum.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", processEventResult.getStatusCode().name()));
        outputParameters.add(new Pair<String, String>("operationId", processEventResult.getOperationId()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "processEvent", operationId, "closing", ActivityLog.createLogMessage(outputParameters));

        return processEventResult;
    }
    
    @Override
    public NotifyEventResponse sendNotifySfEventOffer(String requestId, String fiscalCode, Date date, String stationId, String productId, Boolean paymentFlag, Integer credits, Double quantity, String refuelMode, String firstName, String lastName,
            String email, Date birthDate, Boolean notificationFlag, Boolean paymentCardFlag, String brand, String cluster, Double amount, Boolean privacyFlag1, Boolean privacyFlag2, String mobilePhone,
            String loyaltyCard, String eventType, String parameter1, String parameter2, String paymentMode) {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        
        String dateString = "";
        String birthDateString = "";
        
        if (date != null) {
            dateString = sdf.format(date);
        }
        if (birthDate != null) {
            birthDateString = sdf.format(birthDate);
        }
    	
    	Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestId",        requestId));
        inputParameters.add(new Pair<String, String>("fiscalCode",       fiscalCode));
        inputParameters.add(new Pair<String, String>("date",             dateString));
        inputParameters.add(new Pair<String, String>("stationId",        stationId));
        inputParameters.add(new Pair<String, String>("prodotto",         productId));
        inputParameters.add(new Pair<String, String>("paymentFlag",      paymentFlag.toString()));
        inputParameters.add(new Pair<String, String>("credits",          credits.toString()));
        inputParameters.add(new Pair<String, String>("quantity",         quantity.toString()));
        inputParameters.add(new Pair<String, String>("refuelMode",       refuelMode));
        inputParameters.add(new Pair<String, String>("firstName",        firstName));
        inputParameters.add(new Pair<String, String>("lastName",         lastName));
        inputParameters.add(new Pair<String, String>("email",            email));
        inputParameters.add(new Pair<String, String>("birthDate",        birthDateString));
        inputParameters.add(new Pair<String, String>("notificationFlag", notificationFlag.toString()));
        inputParameters.add(new Pair<String, String>("paymentCardFlag",  paymentCardFlag.toString()));
        inputParameters.add(new Pair<String, String>("brand",            brand));
        inputParameters.add(new Pair<String, String>("cluster",          cluster));
        inputParameters.add(new Pair<String, String>("amount",           amount.toString()));
        inputParameters.add(new Pair<String, String>("privacyFlag1",     privacyFlag1.toString()));
        inputParameters.add(new Pair<String, String>("privacyFlag2",     privacyFlag2.toString()));
        inputParameters.add(new Pair<String, String>("mobilePhone",      mobilePhone));
        inputParameters.add(new Pair<String, String>("loyaltyCard",      loyaltyCard));
        inputParameters.add(new Pair<String, String>("eventType",        eventType));
        inputParameters.add(new Pair<String, String>("parameter1",       parameter1));
        inputParameters.add(new Pair<String, String>("parameter2",       parameter2));
        inputParameters.add(new Pair<String, String>("paymentMode",      paymentMode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendNotifySfEventOffer", null, "opening", ActivityLog.createLogMessage(inputParameters));

        NotifyEventResponse notifyEventResponse = null;

        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();
            
            NotifyEventResult response = crmAdapterService.notifyEvent(requestId, fiscalCode, date, stationId, productId, paymentFlag, credits, quantity, refuelMode, 
            		firstName, lastName, email, birthDate, notificationFlag, paymentCardFlag, brand, cluster, amount, privacyFlag1, privacyFlag2, 
            		mobilePhone, loyaltyCard, eventType, parameter1, parameter2, paymentMode);
            
            notifyEventResponse = new NotifyEventResponse();
            notifyEventResponse.setSuccess(response.getSuccess());
            notifyEventResponse.setErrorCode(response.getErrorCode());
            notifyEventResponse.setMessage(response.getMessage());
            notifyEventResponse.setRequestId(requestId);
            
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendNotifySfEventOffer", null, null, ex.getMessage());
            notifyEventResponse = new NotifyEventResponse();
            notifyEventResponse.setSuccess(false);
            notifyEventResponse.setErrorCode("500");
            notifyEventResponse.setMessage("System Error");
            notifyEventResponse.setRequestId(requestId);
            
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendNotifySfEventOffer", null, null, "Interface FidelityService not found: " + ex.getMessage());
            //response = ResponseHelper.SYSTEM_ERROR;
            notifyEventResponse = new NotifyEventResponse();
            notifyEventResponse.setSuccess(false);
            notifyEventResponse.setErrorCode("500");
            notifyEventResponse.setMessage("System Error");
            notifyEventResponse.setRequestId(requestId);
        }catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendNotifySfEventOffer", null, null, "Interface FidelityService not found: " + ex.getMessage());
            //response = ResponseHelper.SYSTEM_ERROR;
            notifyEventResponse = new NotifyEventResponse();
            notifyEventResponse.setSuccess(false);
            notifyEventResponse.setErrorCode("500");
            notifyEventResponse.setMessage(ex.getMessage());
            notifyEventResponse.setRequestId(requestId);
        }
        
        try {
			userTransaction.begin();
			
			//aggiorno la tabella crm_sf_event
			UserBean user = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);
			
			if (user != null ) {
			    
    	        CRMSfEventBean crmSfEventBean =  CRMSfEventBean.crateCrmSfEventBean(notifyEventResponse, requestId, fiscalCode, date, stationId, productId, paymentFlag, credits, quantity, refuelMode, 
    	        		firstName, lastName, email, birthDate, notificationFlag, paymentCardFlag, brand, cluster, amount, privacyFlag1, privacyFlag2, 
    	        		mobilePhone, loyaltyCard, eventType, parameter1, parameter2, paymentMode, user);
    	        
    	        em.persist(crmSfEventBean);
			}
			else {
			    
			    // Non esiste nessun utente con il codice fiscale inserito
			    this.loggerService.log(ErrorLevel.WARN, this.getClass().getSimpleName(), "sendNotifySfEventOffer", null, null, "User with fiscalcode " + fiscalCode + " not found.");
			}
	        
	        userTransaction.commit();
	        
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RollbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeuristicMixedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeuristicRollbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
        
        
        
        

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();
        outParameters.add(new Pair<String, String>("success", notifyEventResponse.getSuccess().toString()));
        outParameters.add(new Pair<String, String>("errorCode", notifyEventResponse.getErrorCode()));
        outParameters.add(new Pair<String, String>("message", notifyEventResponse.getMessage()));
        outParameters.add(new Pair<String, String>("requestId", notifyEventResponse.getRequestId()));
        
        loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "notifyEventSF", requestId, "closing", ActivityLog.createLogMessage(outParameters));
        
        return notifyEventResponse;
    }
    
    @Override
    public GetOffersResult getMissionsSf(String requestId, String fiscalCode, Date date, Boolean notificationFlag, Boolean paymentCardFlag, String brand, String cluster) {
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        
        String dateString = "";
        
        if (date != null) {
            dateString = sdf.format(date);
        }
    	
    	Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestId",        requestId));
        inputParameters.add(new Pair<String, String>("fiscalCode",       fiscalCode));
        inputParameters.add(new Pair<String, String>("date",             dateString));
        inputParameters.add(new Pair<String, String>("notificationFlag", notificationFlag.toString()));
        inputParameters.add(new Pair<String, String>("paymentCardFlag",  paymentCardFlag.toString()));
        inputParameters.add(new Pair<String, String>("brand",            brand));
        inputParameters.add(new Pair<String, String>("cluster",          cluster));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendNotifyEventOffer", null, "opening", ActivityLog.createLogMessage(inputParameters));

        GetOffersResult response = null;

        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();
            
            response = crmAdapterService.getOffersSF(requestId, fiscalCode, date, notificationFlag, paymentCardFlag, brand, cluster);
        
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendNotifyEventOffer", null, null, ex.getMessage());
            response = new GetOffersResult();
            response.setSuccess(false);
            response.setErrorCode("500");
            response.setMessage("System Error");
            response.setRequestId(requestId);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendNotifyEventOffer", null, null, "Interface FidelityService not found: " + ex.getMessage());
            response = new GetOffersResult();
            response.setSuccess(false);
            response.setErrorCode("500");
            response.setMessage("System Error");
            response.setRequestId(requestId);
        }

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();
        outParameters.add(new Pair<String, String>("success", response.getSuccess().toString()));
        outParameters.add(new Pair<String, String>("errorCode", response.getErrorCode()));
        outParameters.add(new Pair<String, String>("message", response.getMessage()));
        outParameters.add(new Pair<String, String>("requestId", response.getRequestId()));
        
        loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "notifyEvent", requestId, "closing", ActivityLog.createLogMessage(outParameters));
        
        return response;
    	
    }
    
    @Override
    public ProcessCrmOutboundInterfaceResponse processCrmSfOutboundInterface(List<CrmSfDataElement> crmSfDataElementList) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "processCrmOurboundInterface", null, "opening", ActivityLog.createLogMessage(inputParameters));

        ProcessCrmOutboundInterfaceResponse processCrmOutboundInterfaceResponse = new ProcessCrmOutboundInterfaceResponse();

        try {
            boolean checkEmptyTable = Boolean.parseBoolean(this.parametersService.getParamValue(PARAM_CRM_PROCESS_CHECK_EMPTY_TABLE));
            int maxDataPerThread = Integer.parseInt(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_DATA_PER_THREAD));
            int maxThreads = Integer.parseInt(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_THREADS));

            List<CrmDataElementProcessingResult> crmDataElementProcessingResultList = processCrmSfOutboundInterfaceAction.execute(crmSfDataElementList, checkEmptyTable, maxDataPerThread, maxThreads);

            for (CrmDataElementProcessingResult crmDataElementProcessingResult : crmDataElementProcessingResultList) {
                processCrmOutboundInterfaceResponse.getCrmDataElementProcessingResultList().add(crmDataElementProcessingResult);
            }

            processCrmOutboundInterfaceResponse.setStatusCode(ResponseHelper.CRM_PROCESS_OUTBOUND_INTERFACE_SUCCESS);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "processCrmOurboundInterface", null, null, ex.getMessage());
            processCrmOutboundInterfaceResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "processCrmOurboundInterface", null, null, ex.getMessage());
            processCrmOutboundInterfaceResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", processCrmOutboundInterfaceResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "processCrmOurboundInterface", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return processCrmOutboundInterfaceResponse;
    }
    
    
    @Override
    public UpdateContactKeyResult updateContactKey(String operationId, String requestId, String fiscalCode, String contactKey) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationId", operationId));
        inputParameters.add(new Pair<String, String>("requestId",   requestId));
        inputParameters.add(new Pair<String, String>("fiscalCode",  fiscalCode));
        inputParameters.add(new Pair<String, String>("contactKey",  contactKey));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateContactKey", null, "opening", ActivityLog.createLogMessage(inputParameters));

        UpdateContactKeyResult updateContactKeyResult = null;
        
        try {
            updateContactKeyResult = updateContactKeyAction.execute(operationId, requestId, fiscalCode, contactKey, this.userCategoryService);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "processEvent", operationId, null, ex.getMessage());
            updateContactKeyResult = new UpdateContactKeyResult();
            updateContactKeyResult.setStatusCode(StatusCodeEnum.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", updateContactKeyResult.getStatusCode().name()));
        outputParameters.add(new Pair<String, String>("operationId", updateContactKeyResult.getOperationId()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "processEvent", operationId, "closing", ActivityLog.createLogMessage(outputParameters));

        return updateContactKeyResult;
        
    }
    
    @Override
    public UpdateContactKeyBulkResult insertBulkContactKey(String operationId, String requestId, Map<String,String> usersRefresh) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationId", operationId));
        inputParameters.add(new Pair<String, String>("requestId",   requestId));
        //inputParameters.add(new Pair<String, String>("usersRefresh",  usersRefresh));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateContactKey", null, "opening", ActivityLog.createLogMessage(inputParameters));

        UpdateContactKeyBulkResult updateContactKeyResult = null;
        
        try {
            updateContactKeyResult = insertBulkContactKeyAction.execute(operationId, requestId, usersRefresh, this.userCategoryService);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "processEvent", operationId, null, ex.getMessage());
            updateContactKeyResult = new UpdateContactKeyBulkResult();
            updateContactKeyResult.setStatusCode(StatusCodeEnum.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", updateContactKeyResult.getStatusCode().name()));
        outputParameters.add(new Pair<String, String>("operationId", updateContactKeyResult.getOperationId()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "processEvent", operationId, "closing", ActivityLog.createLogMessage(outputParameters));

        return updateContactKeyResult;
        
    }
}
