package com.techedge.mp.core.business;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.actions.reconciliation.ReconciliationDetailAction;
import com.techedge.mp.core.actions.reconciliation.ReconciliationParkingAction;
import com.techedge.mp.core.actions.reconciliation.ReconciliationReconcileAction;
import com.techedge.mp.core.actions.reconciliation.ReconciliationTransactionAction;
import com.techedge.mp.core.actions.reconciliation.ReconciliationUserAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationDetail;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfoData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationParkingData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationParkingSummary;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationTransactionSummary;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserSummary;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

/**
 * Session Bean implementation class ReconciliationService
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ReconciliationService implements ReconciliationServiceRemote, ReconciliationServiceLocal {

    @EJB
    private ReconciliationReconcileAction            reconciliationReconcileAction;
    @EJB
    private ReconciliationDetailAction               reconciliationDetailAction;
    @EJB
    private ReconciliationTransactionAction          reconciliationTransactionAction;
    @EJB
    private ReconciliationUserAction                 reconciliationUserAction;
    @EJB
    private ReconciliationParkingAction              reconciliationParkingAction;

    private LoggerService                            loggerService                                     = null;
    private ParametersService                        parametersService                                 = null;

    private ForecourtInfoServiceRemote               forecourtInfoService                              = null;
    private ForecourtPostPaidServiceRemote           forecourtPPService                                = null;
    private GPServiceRemote                          gpServiceRemote                                   = null;
    private TransactionService                       transactionService                                = null;
    private UserCategoryService                      userCategoryService                               = null;
    private RefuelingNotificationServiceRemote       refuelingNotificationService                      = null;
    private RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2                = null;

    private static final String                      PARAM_EMAIL_SENDER_ADDRESS                        = "EMAIL_SENDER_ADDRESS";
    private static final String                      PARAM_SMTP_HOST                                   = "SMTP_HOST";
    private static final String                      PARAM_SMTP_PORT                                   = "SMTP_PORT";
    private final static String                      PARAM_PROXY_HOST                                  = "PROXY_HOST";
    private final static String                      PARAM_PROXY_PORT                                  = "PROXY_PORT";
    private final static String                      PARAM_PROXY_NO_HOSTS                              = "PROXY_NO_HOSTS";
    private final static String                      PARAM_RECONCILIATION_TRANSACTION_REPORT_RECIPIENT = "RECONCILIATION_TRANSACTION_REPORT_RECIPIENT";
    private final static String                      PARAM_RECONCILIATION_MAX_ATTEMPTS                 = "RECONCILIATION_MAX_ATTEMPTS";
    private final static String                      PARAM_PUSH_NOTIFICATION_MAX_EXPIRY_TIME           = "PUSH_NOTIFICATION_MAX_EXPIRY_TIME";
    private final static String                      PARAM_STRING_SUBSTITUTION_PATTERN                 = "STRING_SUBSTITUTION_PATTERN";
    private final static String                      PARAM_CARTASI_VAULT_BLOCK                         = "CARTASI_VAULT_BLOCK";
    private final static String                      PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY               = "CARTASI_VAULT_BLOCK_ATTRIBUTE_KEY";
    private final static String                      PARAM_PARKING_ENCODED_SECRET_KEY                  = "PARKING_ENCODED_SECRET_KEY";
    private final static String                      PARAM_REFUELING_OAUTH2_ACTIVE                     = "REFUELING_OAUTH2_ACTIVE";

    private String                                   secretKey                                         = null;
    private String                                   parkingDecodedSecretKey                           = null;

    private StringSubstitution                       stringSubstitution                                = null;

    public ReconciliationService() {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.refuelingNotificationService = EJBHomeCache.getInstance().getRefuelingNotificationService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.refuelingNotificationServiceOAuth2 = EJBHomeCache.getInstance().getRefuelingNotificationServiceOAuth2();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        String vaultBlock = "";
        String vaultBlockAttributeCryptKey = "";
        String pattern = null;
        String parkingEncodedSecretKey = "";

        try {

            vaultBlock = parametersService.getParamValue(ReconciliationService.PARAM_CARTASI_VAULT_BLOCK);
            vaultBlockAttributeCryptKey = parametersService.getParamValue(ReconciliationService.PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY);
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
            parkingEncodedSecretKey = parametersService.getParamValue(PARAM_PARKING_ENCODED_SECRET_KEY);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        try {
            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(this.secretKey);
            parkingDecodedSecretKey = encryptionAES.decrypt(parkingEncodedSecretKey);
        }
        catch (Exception e) {
            System.err.println("Error decoding parking secret key: " + e.getMessage());
        }

        System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeCryptKey + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.secretKey + "'");

        stringSubstitution = new StringSubstitution(pattern);
    }

    @Override
    public ReconciliationInfoData reconciliationReconcileTransactions(String adminTicketId, String requestId, List<String> transactionsID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationReconcileTransactions", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        ReconciliationInfoData reconciliationInfoData = null;

        try {

            reconciliationInfoData = reconciliationReconcileAction.execute(requestId, transactionsID, this.forecourtInfoService, this.gpServiceRemote);
        }

        catch (EJBException ex) {
            reconciliationInfoData = new ReconciliationInfoData();
            reconciliationInfoData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", reconciliationInfoData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationReconcileTransactions", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return reconciliationInfoData;

    }

    @Override
    public ReconciliationDetail getReconciliationDetail(String adminTicketId, String requestId, String transactionsID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionsID", transactionsID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getReconciliationDetail", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        ReconciliationDetail reconciliationDetail = null;

        try {

            reconciliationDetail = reconciliationDetailAction.execute(requestId, transactionsID, this.forecourtInfoService);
        }

        catch (EJBException ex) {
            reconciliationDetail = new ReconciliationDetail();
            reconciliationDetail.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", reconciliationDetail.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationReconcileTransactions", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return reconciliationDetail;

    }

    @Override
    public ReconciliationTransactionSummary reconciliationTransaction(List<String> transactionsIDList) {

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationTransaction", null, "opening", null);

        ReconciliationTransactionSummary reconciliationTransactionSummary = new ReconciliationTransactionSummary();

        try {

            this.forecourtInfoService = EJBHomeCache.getInstance().getForecourtInfoService();
            this.forecourtPPService = EJBHomeCache.getInstance().getForecourtPostPaidService();
            this.gpServiceRemote = EJBHomeCache.getInstance().getGpService();
            this.transactionService = EJBHomeCache.getInstance().getTransactionService();
            FidelityServiceRemote fidelityService = EJBHomeCache.getInstance().getFidelityService();
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
            EmailSenderRemote emailSender = EJBHomeCache.getInstance().getEmailSender();
            CRMServiceRemote crmService = EJBHomeCache.getInstance().getCRMService();
            String reconciliationRecipient = null;
            String proxyHost = System.getProperty("http.proxyHost");
            String proxyPort = System.getProperty("http.proxyPort");
            String proxyNoHosts = System.getProperty("http.nonProxyHosts");

            try {
                reconciliationRecipient = this.parametersService.getParamValue(PARAM_RECONCILIATION_TRANSACTION_REPORT_RECIPIENT);
                String emailSenderAddress = this.parametersService.getParamValue(PARAM_EMAIL_SENDER_ADDRESS);
                String smtpHost = this.parametersService.getParamValue(PARAM_SMTP_HOST);
                String smtpPort = this.parametersService.getParamValue(PARAM_SMTP_PORT);
                proxyHost = this.parametersService.getParamValue(PARAM_PROXY_HOST);
                proxyPort = this.parametersService.getParamValue(PARAM_PROXY_PORT);
                proxyNoHosts = this.parametersService.getParamValue(PARAM_PROXY_NO_HOSTS);
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
            }
            catch (ParameterNotFoundException ex) {
                System.err.println("Errore nel caricamento dei parametri: " + ex.getMessage());
                emailSender = null;
            }

            reconciliationTransactionSummary = reconciliationTransactionAction.execute(gpServiceRemote, transactionService, forecourtInfoService, forecourtPPService,
                    fidelityService, crmService, emailSender, userCategoryService, proxyHost, proxyPort, proxyNoHosts, reconciliationRecipient, transactionsIDList, secretKey,
                    stringSubstitution, refuelingNotificationService, refuelingNotificationServiceOAuth2);

        }

        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "reconciliationTransaction", null, "closing", ex.getMessage());
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        //outputParameters.add(new Pair<String, String>("statusCode", reconciliationDetail.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationTransaction", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return reconciliationTransactionSummary;

    }

    @Override
    public ReconciliationUserSummary reconciliationUser(List<ReconciliationUserData> userReconciliationInterfaceIDList) {

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationUser", null, "opening", null);

        ReconciliationUserSummary reconciliationSummary = new ReconciliationUserSummary();

        try {
            DWHAdapterServiceRemote dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();
            CRMAdapterServiceRemote crmAdapterService = EJBHomeCache.getInstance().getCRMAdapterService();
            PushNotificationServiceRemote pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
            RefuelingNotificationServiceRemote refuelingNotificationService = EJBHomeCache.getInstance().getRefuelingNotificationService();
            FidelityServiceRemote fidelityService = EJBHomeCache.getInstance().getFidelityService();
            EmailSenderRemote emailSender = EJBHomeCache.getInstance().getEmailSender();
            String reconciliationRecipient = null;
            String proxyHost = System.getProperty("http.proxyHost");
            String proxyPort = System.getProperty("http.proxyPort");
            String proxyNoHosts = System.getProperty("http.nonProxyHosts");
            Integer pushNotificationExpiryTime = 172800;
            Integer reconciliationMaxRetryAttemps = 5;
            Boolean refuelingOauth2Active = null;

            try {
                reconciliationRecipient = this.parametersService.getParamValue(PARAM_RECONCILIATION_TRANSACTION_REPORT_RECIPIENT);
                pushNotificationExpiryTime = Integer.valueOf(this.parametersService.getParamValue(PARAM_PUSH_NOTIFICATION_MAX_EXPIRY_TIME));
                reconciliationMaxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
                String emailSenderAddress = this.parametersService.getParamValue(PARAM_EMAIL_SENDER_ADDRESS);
                String smtpHost = this.parametersService.getParamValue(PARAM_SMTP_HOST);
                String smtpPort = this.parametersService.getParamValue(PARAM_SMTP_PORT);
                proxyHost = this.parametersService.getParamValue(PARAM_PROXY_HOST);
                proxyPort = this.parametersService.getParamValue(PARAM_PROXY_PORT);
                proxyNoHosts = this.parametersService.getParamValue(PARAM_PROXY_NO_HOSTS);
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                refuelingOauth2Active = Boolean.valueOf(this.parametersService.getParamValue(PARAM_REFUELING_OAUTH2_ACTIVE));
            }
            catch (ParameterNotFoundException ex) {
                System.err.println("Errore nel caricamento dei parametri: " + ex.getMessage());
                emailSender = null;
            }

            reconciliationSummary = reconciliationUserAction.execute(dwhAdapterService, crmAdapterService, pushNotificationService, fidelityService, emailSender,
                    refuelingNotificationService, refuelingNotificationServiceOAuth2, parametersService, proxyHost, proxyPort, proxyNoHosts, pushNotificationExpiryTime, reconciliationMaxRetryAttemps,
                    reconciliationRecipient, userReconciliationInterfaceIDList, stringSubstitution, refuelingOauth2Active);

        }

        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "reconciliationUser", null, "closing", ex.getMessage());
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        //outputParameters.add(new Pair<String, String>("statusCode", reconciliationDetail.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationTransaction", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return reconciliationSummary;

    }

    @Override
    public ReconciliationParkingSummary reconciliationParking(List<ReconciliationParkingData> parkingReconciliationInterfaceIDList) {

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationParking", null, "opening", null);

        ReconciliationParkingSummary reconciliationSummary = new ReconciliationParkingSummary();

        try {
            EmailSenderRemote emailSender = EJBHomeCache.getInstance().getEmailSender();
            String reconciliationRecipient = null;
            String proxyHost = System.getProperty("http.proxyHost");
            String proxyPort = System.getProperty("http.proxyPort");
            String proxyNoHosts = System.getProperty("http.nonProxyHosts");
            Integer reconciliationMaxRetryAttemps = 5;

            try {
                reconciliationRecipient = this.parametersService.getParamValue(PARAM_RECONCILIATION_TRANSACTION_REPORT_RECIPIENT);
                reconciliationMaxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
                String emailSenderAddress = this.parametersService.getParamValue(PARAM_EMAIL_SENDER_ADDRESS);
                this.gpServiceRemote = EJBHomeCache.getInstance().getGpService();
                String smtpHost = this.parametersService.getParamValue(PARAM_SMTP_HOST);
                String smtpPort = this.parametersService.getParamValue(PARAM_SMTP_PORT);
                proxyHost = this.parametersService.getParamValue(PARAM_PROXY_HOST);
                proxyPort = this.parametersService.getParamValue(PARAM_PROXY_PORT);
                proxyNoHosts = this.parametersService.getParamValue(PARAM_PROXY_NO_HOSTS);
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
            }
            catch (ParameterNotFoundException ex) {
                System.err.println("Errore nel caricamento dei parametri: " + ex.getMessage());
                emailSender = null;
            }

            reconciliationSummary = reconciliationParkingAction.execute(emailSender, gpServiceRemote, parametersService, proxyHost, proxyPort, proxyNoHosts,
                    reconciliationMaxRetryAttemps, reconciliationRecipient, parkingReconciliationInterfaceIDList, stringSubstitution, parkingDecodedSecretKey);

        }

        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "reconciliationUser", null, "closing", ex.getMessage());
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        //outputParameters.add(new Pair<String, String>("statusCode", reconciliationDetail.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationTransaction", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return reconciliationSummary;

    }
}
