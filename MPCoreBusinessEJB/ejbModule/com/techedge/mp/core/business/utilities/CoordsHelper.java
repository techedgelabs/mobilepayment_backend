package com.techedge.mp.core.business.utilities;

public class CoordsHelper {

	public CoordsHelper() {}
	
	public static double calculateDistance(Double startLat, Double startLon, Double endLat, Double endLon) throws Exception {
	    
	    double dist = 100000000d;
	    
	    if ( startLat != null && startLon != null ){
		    
		    double theta = (double) (startLon - endLon);
		    dist = Math.sin(deg2rad(startLat)) * Math.sin(deg2rad(endLat)) + Math.cos(deg2rad(startLat)) * Math.cos(deg2rad(endLat)) * Math.cos(deg2rad(theta));
		    dist = Math.acos(dist);
		    dist = rad2deg(dist);
		    dist = dist * 60 * 1.1515;
		    dist = dist * 1609.344;
		}
	    
	    return (dist);
	}
	
	
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts decimal degrees to radians             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts radians to decimal degrees             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

}
