package com.techedge.mp.core.business.utilities.crm;

public enum PAYMENT_MODE_TYPE {
	
    CARTA("Carta"), VOUCHER("Voucher"), CARTA_VOUCHER("Carta+Voucher"), MULTICARD("Multicard");

    private final String name;

    private PAYMENT_MODE_TYPE(String s) {
        name = s;
    }
    
    public String getValue() {
        return this.name;
    }

}
