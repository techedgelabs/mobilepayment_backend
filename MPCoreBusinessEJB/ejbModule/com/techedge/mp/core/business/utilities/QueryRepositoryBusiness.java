package com.techedge.mp.core.business.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.StatisticalReportsBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;

public class QueryRepositoryBusiness {
    @SuppressWarnings("unchecked")
    public static UserBean findUserBusinessByEmail(EntityManager em, String email) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_EMAIL);

        query.setParameter("email", email);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();
        for (PersonalDataBean personalDataBean : personalDataBeanList) {

            //System.out.println("check user for personalData: " + personalDataBean.getId());

            userBean = QueryRepository.findUserByPersonalData(em, personalDataBean);

            if (userBean != null  && (userBean.getUserType() == User.USER_TYPE_BUSINESS || userBean.getUserType() == User.USER_TYPE_SERVICE)) {

                //System.out.println("userBean found");

                return userBean;
            }
            else {

                //System.out.println("userBean not found");
            }
        }

        return userBean;

    }
    
    @SuppressWarnings("unchecked")
    public static UserBean findNotCancelledUserBusinessByEmail(EntityManager em, String email) {

        UserBean userBean = null;
        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_EMAIL);
        query.setParameter("email", email);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();
        for (PersonalDataBean personalDataBean : personalDataBeanList) {

            //System.out.println("check user for personalData: " + personalDataBean.getId());

            userBean = QueryRepository.findUserByPersonalData(em, personalDataBean);

            if (userBean != null) {

                if (userBean.getUserStatus() != User.USER_STATUS_CANCELLED && (userBean.getUserType() == User.USER_TYPE_BUSINESS || userBean.getUserType() == User.USER_TYPE_SERVICE)) {
                    return userBean;
                }
                else {
                    System.out.println("userBean cancelled or not business");
                }
            }
        }

        return null;

    }
    
    @SuppressWarnings("unchecked")
    public static UserBean findNotCancelledUserBusinessByEmailAndSource(EntityManager em, String email, String source) {

        UserBean userBean = null;
        Query query = em.createNamedQuery(UserBean.FIND_BY_EMAIL_AND_SOURCE);
        query.setParameter("source", source);
        query.setParameter("email", email);
        List<UserBean> userBeanList = query.getResultList();

        if (userBeanList == null || userBeanList.isEmpty()) {
            return null;
        }
        
        userBean = userBeanList.get(0);
        
        if (userBean != null) {

            if (userBean.getUserStatus() != User.USER_STATUS_CANCELLED && (userBean.getUserType() == User.USER_TYPE_BUSINESS || userBean.getUserType() == User.USER_TYPE_SERVICE)) {
                return userBean;
            }
            else {
                System.out.println("userBean cancelled or not business");
            }
        }
        
        return null;

    }
    
    @SuppressWarnings("unchecked")
    public static UserBean findUserBusinessByFiscalCode(EntityManager em, String fiscalCode) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_FISCALCODE);

        query.setParameter("fiscalCode", fiscalCode);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();
        for (PersonalDataBean personalDataBean : personalDataBeanList) {

            System.out.println("check user for personalData: " + personalDataBean.getId());

            userBean = QueryRepository.findUserByPersonalData(em, personalDataBean);

            if (userBean != null && userBean.getUserType() == User.USER_TYPE_BUSINESS) {

                System.out.println("userBean business found");

                return userBean;
            }
            else {

                System.out.println("userBean business not found");
            }
        }

        return userBean;

    }

    @SuppressWarnings("unchecked")
    public static UserBean findNotCancelledUserBusinessByFiscalCode(EntityManager em, String fiscalCode) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_FISCALCODE);

        query.setParameter("fiscalCode", fiscalCode);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();
        for (PersonalDataBean personalDataBean : personalDataBeanList) {

            System.out.println("check user for personalData: " + personalDataBean.getId());

            userBean = QueryRepository.findUserByPersonalData(em, personalDataBean);

            if (userBean != null) {

                if (userBean.getUserStatus() != User.USER_STATUS_CANCELLED && userBean.getUserType() == User.USER_TYPE_BUSINESS) {

                    System.out.println("userBean business found");

                    return userBean;
                }
                else {

                    System.out.println("userBean cancelled");
                }
            }
            else {

                System.out.println("userBean not found");
            }
        }

        return null;

    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllPrePaidTransactionsBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsAreaBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_AREA_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllPrePaidTransactionsAreaBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_AREA_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsHistoryBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllPrePaidTransactionsHistoryBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsHistoryAreaBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_AREA_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllPrePaidTransactionsHistoryAreaBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_AREA_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_BUSINESS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_BUSINESS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllPostPaidTransactionsBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_BUSINESS);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_BUSINESS);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsAreaBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String source, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_AREA_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("source", source);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate, String source, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_AREA_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("source", source);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsHistoryBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_BUSINESS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_BUSINESS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllPostPaidTransactionsHistoryBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_BUSINESS);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_All_BUSINESS);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsHistoryAreaBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String source, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_AREA_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("source", source);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllPostPaidTransactionsHistoryAreaBusiness(EntityManager em,  Date dailyEndDate,
    		Date totalStartDate, String source, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_AREA_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("source", source);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisUsersCreditCardBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ACTIVE_CREDIT_CARD_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllUsersCreditCardBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE_CREDIT_CARD_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        
        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalAmountBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Double statisticsReportSynthesisAllPrePaidTotalAmountBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        Double result = (Double) query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalAmountHistoryBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Double statisticsReportSynthesisAllPrePaidTotalAmountHistoryBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result = (Double) query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalAmountBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Double statisticsReportSynthesisAllPostPaidTotalAmountBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result = (Double) query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalAmountHistoryBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Double statisticsReportSynthesisAllPostPaidTotalAmountHistoryBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result = (Double) query.getSingleResult();

        return result;
    }

    public static Object statisticsReportSynthesisStationsAppCountBusiness(EntityManager em) {

        Query query = em.createNamedQuery(StationBean.COUNT_ALL_APP_ACTIVE_BUSINESS);

        query.setMaxResults(1);

        Object resultList = query.getResultList();

        /*
         * if (resultList != null) {
         * return Arrays.asList((Object[]) resultList);
         * }
         */

        return resultList;
    }
    
    public static Long statisticsReportSynthesisAllStationsAppCountBusiness(EntityManager em) {

        Query query = em.createNamedQuery(StationBean.COUNT_ALL_APP_ACTIVE_BUSINESS);

        Long result = (Long) query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> statisticsReportDetailPrePaidTransactionsBusiness(EntityManager em, Date startDate, Date endDate) {

        Query query;

        if (startDate != null) {
            query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_DETAIL_BY_DATE_BUSINESS);
            query.setParameter("startDate", startDate);
            query.setParameter("endDate", endDate);
        }
        else {
            query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_DETAIL_BUSINESS);
            query.setParameter("endDate", endDate);
        }

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionHistoryBean> statisticsReportDetailPrePaidTransactionsHistoryBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate) {

        Query query;

        if (dailyStartDate != null) {
            query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_DETAIL_BY_DATE_BUSINESS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
        }
        else {
            query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_DETAIL_BUSINESS);
            query.setParameter("dailyEndDate", dailyEndDate);
        }

        List<TransactionHistoryBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> statisticsReportDetailPostPaidTransactionsBusiness(EntityManager em, Date startDate, Date endDate) {

        Query query;

        if (startDate != null) {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_DETAIL_BY_DATE_BUSINESS);
            query.setParameter("startDate", startDate);
            query.setParameter("endDate", endDate);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_DETAIL_BUSINESS);
            query.setParameter("endDate", endDate);
        }

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionHistoryBean> statisticsReportDetailPostPaidTransactionsHistoryBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate) {

        Query query;

        if (dailyStartDate != null) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_DETAIL_BY_DATE_BUSINESS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_DETAIL_BUSINESS);
            query.setParameter("dailyEndDate", dailyEndDate);
        }

        List<PostPaidTransactionHistoryBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisDistinctUsersTransactionBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        List<Long> integerList = null;

        Long almenoUnaTransazionePrimaDiIeri = 0l, almenoUnaTransazioneFinoAIeri = 0l, almenoUnaTransazioneIeri = 0l, almenoUnaTransazionePrimaDellaSettimana = 0l, almenoUnaTransazioneNellaSettimana = 0l;

        /*
         * Query query = em.createQuery("select count(distinct u.id) from UserBean u "
         * + "where u.id in (select distinct t.userBean.id from TransactionBean t where t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
         * + "and t.finalAmount > 0 and t.stationBean.stationStatus = 2 and t.creationTimestamp <= :dailyEndDate) "
         * + "or u.id in (select distinct th.userBean.id from TransactionHistoryBean th where th.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
         * + "and th.finalAmount > 0 and th.stationBean.stationStatus = 2 and th.creationTimestamp <= :dailyEndDate) "
         * + "or u.id in (select distinct pt.userBean.id from PostPaidTransactionBean pt where pt.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
         * + "and pt.amount > 0 and pt.stationBean.stationStatus = 2 and pt.creationTimestamp <= :dailyEndDate) "
         * + "or u.id in (select distinct pth.userBean.id from PostPaidTransactionHistoryBean pth where pth.mpTransactionStatus = '"
         * + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' " + "and pth.amount > 0 and pth.stationBean.stationStatus = 2 and pth.creationTimestamp <= :dailyEndDate)");
         */
        Query query = em.createQuery("select count(distinct u.id) from UserBean u "
                + "where u.id in (select distinct t.userBean.id from TransactionBean t where t.transactionCategory = 'BUSINESS' and t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t.finalAmount > 0 and (t.stationBean.stationStatus = 2 or t.stationBean.stationStatus = 1) and t.creationTimestamp <= :dailyEndDate "
                + "and t.creationTimestamp >= :totalStartDate) " + "or u.id in (select distinct th.userBean.id from TransactionHistoryBean th where th.transactionCategory = 'BUSINESS' "
                + "and th.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and th.finalAmount > 0 and (th.stationBean.stationStatus = 2 or th.stationBean.stationStatus = 1) and th.creationTimestamp <= :dailyEndDate "
                + "and th.creationTimestamp >= :totalStartDate) " + "or u.id in (select distinct pt.userBean.id from PostPaidTransactionBean pt where pt.transactionCategory = 'BUSINESS' "
                + "and pt.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and pt.amount > 0 and (pt.stationBean.stationStatus = 2 or pt.stationBean.stationStatus = 1) and pt.creationTimestamp <= :dailyEndDate "
                + "and pt.creationTimestamp >= :totalStartDate) "
                + "or u.id in (select distinct pth.userBean.id from PostPaidTransactionHistoryBean pth where pth.transactionCategory = 'BUSINESS' and pth.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' " + "and pth.amount > 0 and (pth.stationBean.stationStatus = 2 or pth.stationBean.stationStatus = 1) "
                + "and pth.creationTimestamp <= :dailyEndDate and pth.creationTimestamp >= :totalStartDate)");

        // Totale utenti che hanno effettuato almeno una transazione prima della settimana
        Date endDateWeekBefore = new Date(weeklyStartDate.getTime() - 1000);
        query.setParameter("dailyEndDate", endDateWeekBefore);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnaTransazionePrimaDellaSettimana = (Long) integerList.get(0);
        }

        // Totale utenti che hanno effettuato almeno una transazione prima di ieri
        Date endDateBefore = new Date(dailyEndDate.getTime() - (24 * 60 * 60 * 1000));
        query.setParameter("dailyEndDate", endDateBefore);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnaTransazionePrimaDiIeri = (Long) integerList.get(0);
        }

        // Totale utenti che hanno effettuato almeno una transazione prima di oggi
        Date endDateAfter = dailyEndDate;
        query.setParameter("dailyEndDate", endDateAfter);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnaTransazioneFinoAIeri = (Long) integerList.get(0);
        }

        almenoUnaTransazioneIeri = almenoUnaTransazioneFinoAIeri - almenoUnaTransazionePrimaDiIeri;
        almenoUnaTransazioneNellaSettimana = almenoUnaTransazioneFinoAIeri - almenoUnaTransazionePrimaDellaSettimana;

        // Creazione dell'oggetto risposta
        List<Object> result = new ArrayList<Object>(3);
        result.add(0, almenoUnaTransazioneIeri);
        result.add(1, almenoUnaTransazioneNellaSettimana);
        result.add(2, almenoUnaTransazioneFinoAIeri);

        return result;
    }

    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllDistinctUsersTransactionBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createQuery("select count(distinct u.id) from UserBean u "
                + "where u.id in (select distinct t.userBean.id from TransactionBean t where t.transactionCategory = 'BUSINESS' and t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t.finalAmount > 0 and (t.stationBean.stationStatus = 2 or t.stationBean.stationStatus = 1) and t.creationTimestamp <= :dailyEndDate "
                + "and t.creationTimestamp >= :totalStartDate) " + "or u.id in (select distinct th.userBean.id from TransactionHistoryBean th where th.transactionCategory = 'BUSINESS' "
                + "and th.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and th.finalAmount > 0 and (th.stationBean.stationStatus = 2 or th.stationBean.stationStatus = 1) and th.creationTimestamp <= :dailyEndDate "
                + "and th.creationTimestamp >= :totalStartDate) " + "or u.id in (select distinct pt.userBean.id from PostPaidTransactionBean pt where pt.transactionCategory = 'BUSINESS' "
                + "and pt.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and pt.amount > 0 and (pt.stationBean.stationStatus = 2 or pt.stationBean.stationStatus = 1) and pt.creationTimestamp <= :dailyEndDate "
                + "and pt.creationTimestamp >= :totalStartDate) "
                + "or u.id in (select distinct pth.userBean.id from PostPaidTransactionHistoryBean pth where pth.transactionCategory = 'BUSINESS' and pth.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' " + "and pth.amount > 0 and (pth.stationBean.stationStatus = 2 or pth.stationBean.stationStatus = 1) "
                + "and pth.creationTimestamp <= :dailyEndDate and pth.creationTimestamp >= :totalStartDate)");

        // Totale utenti che hanno effettuato almeno una transazione prima di oggi
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result= (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsFuelQuantityBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Double statisticsReportSynthesisAllPrePaidTransactionsFuelQuantityBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BUSINESS);

        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        List<Double> resultList = query.getResultList();
        
        return resultList.get(0);

    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsHistoryFuelQuantityBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Double statisticsReportSynthesisAllPrePaidTransactionsHistoryFuelQuantityBusiness(EntityManager em,Date dailyEndDate,Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Double> resultList = query.getResultList();

        return resultList.get(0);
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsFuelQuantityBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_TOTAL_FUEL_QUANTITY_BUSINESS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BUSINESS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static Double statisticsReportSynthesisAllPostPaidTransactionsFuelQuantityBusiness(EntityManager em, Date dailyEndDate, Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_TOTAL_FUEL_QUANTITY_BUSINESS);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BUSINESS);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        Double result = (Double)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsHistoryFuelQuantityBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_TOTAL_FUEL_QUANTITY_BUSINESS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BUSINESS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    @SuppressWarnings("unchecked")
    public static Double statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantityBusiness(EntityManager em, Date dailyEndDate,
            Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_TOTAL_FUEL_QUANTITY_BUSINESS);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BUSINESS);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        
        Double result = (Double) query.getSingleResult();

        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsFuelQuantityByProductBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTransactionsFuelQuantityByProductBusiness(EntityManager em, Date dailyEndDate, 
    		Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsHistoryFuelQuantityByProductBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTransactionsHistoryFuelQuantityByProductBusiness(EntityManager em, Date dailyEndDate,
            Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsFuelQuantityByProductBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = null;

        query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTransactionsFuelQuantityByProductBusiness(EntityManager em, Date dailyEndDate, 
    		Date totalStartDate, String productDescription) {

        Query query = null;

        query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsHistoryFuelQuantityByProductBusiness(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = null;

        query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantityByProductBusiness(EntityManager em, Date dailyEndDate,
            Date totalStartDate, String productDescription) {

        Query query = null;

        query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static Map<String,String> statisticsReportSynthesisFindForType(EntityManager em, String idRif, String typeExtraction) {

        Query query = null;
        
        query = em.createNamedQuery(StatisticalReportsBean.STATISTICS_REPORT_SYNTHESIS_FIND_REPORT_FOR_TYPE, StatisticalReportsBean.class);
        query.setParameter("typeExtraction", typeExtraction);
        query.setParameter("idRif", idRif);
        
        List<StatisticalReportsBean> result = query.getResultList();
        
        
        Map<String,String> mapStatReport = new HashMap<>();
        for (StatisticalReportsBean ele:result){
        	mapStatReport.put(ele.getChiave(), ele.getValore());
        }

        return mapStatReport;
    }
    
    @SuppressWarnings("unchecked")
    public static List<StatisticalReportsBean> statisticsReportFindForType(EntityManager em, String idRif, String typeExtraction) {

        Query query = null;
        
        query = em.createNamedQuery(StatisticalReportsBean.STATISTICS_REPORT_SYNTHESIS_FIND_REPORT_FOR_TYPE, StatisticalReportsBean.class);
        query.setParameter("typeExtraction", typeExtraction);
        query.setParameter("idRif", idRif);
        
        List<StatisticalReportsBean> result = query.getResultList();
        
        return result;
    }
}
