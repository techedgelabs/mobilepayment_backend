package com.techedge.mp.core.business.utilities;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;

public class VatNumberHelper {
    
    public static boolean validate(String vatCode, LoggerService loggerService)
    {
        int i, c, s;
        
        if(vatCode == null || vatCode.isEmpty()) {
            if (loggerService != null) {
                loggerService.log(ErrorLevel.INFO, "VatNumberHelper", "validate", null, null, "Vat number null or empty");
            }
            return false;
        }
        
        if( vatCode.length() != 11 ) {
            if (loggerService != null) {
                loggerService.log(ErrorLevel.INFO, "VatNumberHelper", "validate", null, null, "Invalid vat number length");
            }
            return false;
        }

        if (!vatCode.matches("^[0-9]{11}$")) {
            if (loggerService != null) {
                loggerService.log(ErrorLevel.INFO, "VatNumberHelper", "validate", null, null, "Invalid vat number format");
            }
            return false;
        }
        
        if (vatCode.equals("00000000000")) {
            if (loggerService != null) {
                loggerService.log(ErrorLevel.INFO, "VatNumberHelper", "validate", null, null, "Invalid vat number: 00000000000");
            }
            return false;
        }
        
        s = 0;
        
        for( i=0; i<=9; i+=2 ) {
            s += vatCode.charAt(i) - '0';
        }
        
        for( i=1; i<=9; i+=2 ) {
            c = 2*( vatCode.charAt(i) - '0' );
            
            if( c > 9 ) {
                c = c - 9;
            }
            
            s += c;
        }
        
        if( ( 10 - s%10 )%10 != vatCode.charAt(10) - '0' ) {
            if (loggerService != null) {
                loggerService.log(ErrorLevel.INFO, "VatNumberHelper", "validate", null, null, "Invalid code control for vat number");
            }
            return false;
        }
        
        return true;
    }
}
