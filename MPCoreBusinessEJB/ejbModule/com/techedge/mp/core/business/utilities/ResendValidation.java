package com.techedge.mp.core.business.utilities;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.DeviceSendingValidationType;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.MobilePhoneSendingValidationType;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SendValidationResult;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserDeviceBean;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;
import com.techedge.mp.sms.adapter.business.interfaces.SendMessageResult;
import com.techedge.mp.sms.adapter.business.interfaces.SmsType;

public class ResendValidation {

    public static SendValidationResult send(String sendingType, Integer maxRetryAttemps, UserBean userBean, MobilePhoneBean mobilePhoneBean, LoggerService loggerService,
            EmailSenderRemote emailSender, SmsServiceRemote smsService, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) {

        SendValidationResult result = new SendValidationResult();
        Integer userType = userBean.getUserType();
        Boolean useNewAcquirer = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
        Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());

        if (sendingType.equals(MobilePhoneSendingValidationType.EMAIL.getValue())) {
            String keyFrom = emailSender.getSender();
            
            if (emailSender != null) {
                EmailType emailType = EmailType.MOBILE_PHONE_VERIFICATION;
                
                if (useNewAcquirer) {
                    emailType = EmailType.MOBILE_PHONE_VERIFICATION_V2;
                    
                    if (stringSubstitution != null) {
                        keyFrom = stringSubstitution.getValue(keyFrom, 1);
                    }
                }

                String to = userBean.getPersonalDataBean().getSecurityDataEmail();
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
                parameters.add(new Parameter("VERIFICATION_CODE", mobilePhoneBean.getVerificationCode()));

                loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null, "Sending email to " + to);

                String emailResult = emailSender.sendEmail(emailType, keyFrom, to, parameters);

                loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null, "SendEmail result: " + result);

                if (emailResult.equals("SEND_MAIL_200")) {
                    result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_SUCCESS);
                }
                else {
                    result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE);
                }
            }
        }
        else {
            if (smsService != null) {

                SmsType smsType = SmsType.MOBILE_PHONE_VERIFICATION;
                
                if (useNewAcquirer) {
                    smsType = SmsType.MOBILE_PHONE_VERIFICATION_V2;
                }
                
                if (useBusiness) {
                    smsType = SmsType.MOBILE_PHONE_VERIFICATION_BUSINESS;
                }
                
                String destinationAddress = mobilePhoneBean.getPrefix() + mobilePhoneBean.getNumber();
                String senderAlias = smsService.getSender();
                
                if (useNewAcquirer) {
                    senderAlias = stringSubstitution.getValue(senderAlias, 1);
                }

                List<com.techedge.mp.sms.adapter.business.interfaces.Parameter> parameters = new ArrayList<com.techedge.mp.sms.adapter.business.interfaces.Parameter>(0);

                parameters.add(new com.techedge.mp.sms.adapter.business.interfaces.Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
                parameters.add(new com.techedge.mp.sms.adapter.business.interfaces.Parameter("VERIFICATION_CODE", mobilePhoneBean.getVerificationCode()));

                loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null, "Sending sms to " + destinationAddress);

                SendMessageResult sendMessageResult = smsService.sendShortMessage(smsType, senderAlias, destinationAddress, parameters, null);

                if (sendMessageResult != null) {
                    loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null,
                            "sendShortMessage result: " + sendMessageResult.getResponseMessage());
                }
                else {
                    loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null,
                            "sendShortMessage result: null");
                }

                if (sendMessageResult == null) {
                    result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE);
                    result.setMessageCode("Connection error");
                    result.setErrorCode(9999);
                }
                else {
                    if (sendMessageResult.getResponseCode() == 0) {
                        result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_SUCCESS);
                        result.setMessageCode(sendMessageResult.getResponseMessage());
                    }
                    else {
                        result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE);
                        result.setMessageCode(sendMessageResult.getResponseMessage());
                        result.setErrorCode(sendMessageResult.getResponseCode());
                    }
                }
            }
            else {
                loggerService.log(ErrorLevel.ERROR, ResendValidation.class.getSimpleName(), "execute", null, null, "Sms service is null!!!");
                result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE);
                result.setMessageCode("Sms service is null!!!");
            }
        }

        return result;
    }

    public static SendValidationResult sendForDevice(String sendingType, Integer maxRetryAttemps, UserBean userBean, UserDeviceBean userDeviceBean, LoggerService loggerService,
            EmailSenderRemote emailSender, SmsServiceRemote smsService, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) {

        SendValidationResult result = new SendValidationResult();
        Integer userType = userBean.getUserType();
        Boolean useNewAcquirer = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
        Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());

        if (sendingType.equals(DeviceSendingValidationType.EMAIL.getValue())) {
            String keyFrom = emailSender.getSender();

            if (emailSender != null) {
                EmailType emailType = EmailType.DEVICE_VERIFICATION_V2;
                
                if (stringSubstitution != null) {
                    keyFrom = stringSubstitution.getValue(keyFrom, 1);
                }
            
                String to = userBean.getPersonalDataBean().getSecurityDataEmail();
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
                parameters.add(new Parameter("VERIFICATION_CODE", userDeviceBean.getVerificationCode()));

                loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null, "Sending email to " + to);

                String emailResult = emailSender.sendEmail(emailType, keyFrom, to, parameters);

                loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null, "SendEmail result: " + result);

                if (emailResult.equals("SEND_MAIL_200")) {
                    result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_SUCCESS);
                }
                else {
                    result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE);
                }
            }
        }
        else {
            if (smsService != null) {

                SmsType smsType = SmsType.DEVICE_VERIFICATION_V2;
                
                if (useBusiness) {
                    smsType = SmsType.DEVICE_VERIFICATION_BUSINESS;
                }
                
                String destinationAddress = "";
                for (MobilePhoneBean mobilePhoneBean : userBean.getMobilePhoneList()) {
                    if (mobilePhoneBean.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE)) {
                        destinationAddress = mobilePhoneBean.getPrefix() + mobilePhoneBean.getNumber();
                        break;
                    }
                }

                String senderAlias = smsService.getSender();
                
                if (useNewAcquirer) {
                    senderAlias = stringSubstitution.getValue(senderAlias, 1);
                }
                
                List<com.techedge.mp.sms.adapter.business.interfaces.Parameter> parameters = new ArrayList<com.techedge.mp.sms.adapter.business.interfaces.Parameter>(0);

                parameters.add(new com.techedge.mp.sms.adapter.business.interfaces.Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
                parameters.add(new com.techedge.mp.sms.adapter.business.interfaces.Parameter("VERIFICATION_CODE", userDeviceBean.getVerificationCode()));

                loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null, "Sending sms to " + destinationAddress);

                SendMessageResult sendMessageResult = smsService.sendShortMessage(smsType, senderAlias, destinationAddress, parameters, null);

                if (sendMessageResult == null) {
                    loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null,
                            "sendShortMessage result: null");
                }
                else {
                    loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null,
                            "sendShortMessage result: " + sendMessageResult.getResponseMessage());
                }
                
                if (sendMessageResult == null) {
                    result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE);
                    result.setMessageCode("Connection error");
                    result.setErrorCode(9999);
                }
                else {
                    if (sendMessageResult.getResponseCode() == 0) {
                        result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_SUCCESS);
                        result.setMessageCode(sendMessageResult.getResponseMessage());
                    }
                    else {
                        result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE);
                        result.setMessageCode(sendMessageResult.getResponseMessage());
                        result.setErrorCode(sendMessageResult.getResponseCode());
                    }
                }
            }
            else {
                loggerService.log(ErrorLevel.ERROR, ResendValidation.class.getSimpleName(), "execute", null, null, "Sms service is null!!!");
                result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE);
                result.setMessageCode("Sms service is null!!!");
            }
        }

        return result;
    }
}
