package com.techedge.mp.core.business.utilities;

public class PinHelper {

    public static String checkPin(String i_pin, String[] checked_string) {
        String response = "OK";
        String c1, c2, c3, c4;
        Integer i1, i2, i3, i4;

        if (i_pin.length() < 4)
            return "KO";

        if (checked_string != null) {
            for (int i = 0; i < checked_string.length; i++) {
                if (checked_string[i].equals(i_pin)) {
                    System.out.println("checkPin: checked string error");
                    return "KO";
                }
            }
        }

        c1 = i_pin.substring(0, 1);
        c2 = i_pin.substring(1, 2);
        c3 = i_pin.substring(2, 3);
        c4 = i_pin.substring(3, 4);

        try {
            i1 = Integer.valueOf(c1);
            i2 = Integer.valueOf(c2);
            i3 = Integer.valueOf(c3);
            i4 = Integer.valueOf(c4);
        }
        catch (NumberFormatException ex) {
            System.out.println("checkPin: non numerico");
            return "KO";
        }

        System.out.println("i1: " + i1);
        System.out.println("i2: " + i2);
        System.out.println("i3: " + i3);
        System.out.println("i4: " + i4);

        if ((i1 == i2) && (i1 == i3) && (i1 == i4)) { //tutte uguali?
            System.out.println("checkPin: tutte uguali");
            return "KO";
        }

        /*if ((i1 == i2) && (i3 == i4)) { //cifre uguali a coppie
            System.out.println("checkPin: cifre uguali a coppie");
            return "KO";
        }*/

        if ((i1 < 7) && (i2 == i1 + 1) && (i3 == i2 + 1) && (i4 == i3 + 1)) { //cifre crescenti
            System.out.println("checkPin: cifre crescenti");
            return "KO";
        }

        if ((i1 > 2) && (i2 == i1 - 1) && (i3 == i2 - 1) && (i4 == i3 - 1)) { //cifre decrescenti
            System.out.println("checkPin: cifre decrescenti");
            return "KO";
        }

        return response;
    }

}
