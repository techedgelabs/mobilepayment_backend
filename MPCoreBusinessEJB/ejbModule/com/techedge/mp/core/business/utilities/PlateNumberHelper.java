package com.techedge.mp.core.business.utilities;

public class PlateNumberHelper {
	
	public PlateNumberHelper() {}
	
	public static Boolean isValid(String plateNumber) {
	    
	    if (plateNumber == null || plateNumber.isEmpty()) {
	        return Boolean.FALSE;
	    }
	    
	    if (plateNumber.length() > 10) {
	        return Boolean.FALSE;
	    }
	    
	    if (!plateNumber.matches("[A-Z0-9]+")) {
	        return Boolean.FALSE;
	    }
	        
	    return Boolean.TRUE;
	}
	
	public static String clear(String plateNumber) {
	    
	    plateNumber = plateNumber.replaceAll("\\s+", "");
	    plateNumber = plateNumber.toUpperCase();
	    
	    return plateNumber;
	}
}
