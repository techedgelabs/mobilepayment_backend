package com.techedge.mp.core.business.utilities;

import com.techedge.mp.core.business.interfaces.AdminRoleEnum;
import com.techedge.mp.core.business.model.AdminBean;
import com.techedge.mp.core.business.model.AdminRoleBean;

public class CheckRoleHelper {

    public CheckRoleHelper() {}

    public static Boolean isAuthorized(String className, AdminBean adminBean) {

        if (adminBean == null || (adminBean != null && (adminBean.getRoles() == null || adminBean.getRoles() != null && adminBean.getRoles().isEmpty()))) {
            return Boolean.FALSE;
        }

        for (AdminRoleBean item : adminBean.getRoles()) {
            if (item.getName().equals(AdminRoleEnum.ROLE_SUPERADMIN.getRole())) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
    
    public static Boolean isAuthorizedForRole(String className, AdminBean adminBean, AdminRoleEnum roleEnum) {

        if (adminBean == null || (adminBean != null && (adminBean.getRoles() == null || adminBean.getRoles() != null && adminBean.getRoles().isEmpty()))) {
            return Boolean.FALSE;
        }

        for (AdminRoleBean item : adminBean.getRoles()) {
            if (item.getName().equals(roleEnum.getRole())) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

}
