package com.techedge.mp.core.business.utilities;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;

public class RandomGenerator {

	public RandomGenerator() {}
	
	public static Double generateRandom(Double max) {
		
		// Ottieni un Double casuale compreso tra 0 e 1
		Double random = new Random().nextDouble();
		
		// Normalizza il risultato
		random = random * max;
		
		// Formatta il risultato a 2 cifre decimali
		DecimalFormat df = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);
	    df.applyPattern("#.##");
		df.setRoundingMode(RoundingMode.FLOOR);
	    
	    String resultString = df.format(random);
	    Double result = new Double(resultString);
	    
		return result;
	}
}
