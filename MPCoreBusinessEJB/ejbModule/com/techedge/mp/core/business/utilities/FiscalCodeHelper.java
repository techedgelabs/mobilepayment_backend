package com.techedge.mp.core.business.utilities;

public class FiscalCodeHelper {

    private String nome;
    private String cognome;
    private int    giorno;
    private int    mese;
    private int    anno;
    private String sesso;

    public FiscalCodeHelper(String nome, String cognome, int giorno, int mese, int anno, String sesso) {

        this.nome = nome;
        this.cognome = cognome;
        this.giorno = giorno;
        this.mese = mese;
        this.anno = anno;
        this.sesso = sesso;
    }
    
    public String toString() {
        
        String result = "{ ";
        
        result += "\"nome\": \"" + this.nome + "\", ";
        result += "\"cognome\": \"" + this.cognome + "\", ";
        result += "\"giorno\": " + this.giorno + ", ";
        result += "\"mese\": " + this.mese + ", ";
        result += "\"anno\": " + this.anno + ", ";
        result += "\"sesso\": \"" + this.sesso + "\", ";
        
        return result;
    }
    
    public String expected(String cityCode) {
        
        String result = "";
        
        result += this.nome + ";";
        result += this.cognome + ";;;";
        result += generaCodiceFiscale(cityCode) + ";";
        
        return result;
    }

    public Boolean verificaAnagrafica(String codiceFiscale) {

        if (codiceFiscale == null || codiceFiscale.isEmpty() || codiceFiscale.length() != 16) {
            
            return Boolean.FALSE;
        }
        
        codiceFiscale = codiceFiscale.toUpperCase();
        
        String carattereDiControllo = FiscalCodeHelper.calcolaCarattereDiControllo(codiceFiscale.substring(0, 15));
        
        if ( !carattereDiControllo.equals(codiceFiscale.substring(15))) {
            
            return Boolean.FALSE;
        }
        
        String codiceCognome = this.calcolaCodiceCognome(this.cognome);
        String codiceNome = this.calcolaCodiceNome(this.nome);
        String codiceDataNascitaESesso = this.calcolaCodiceDataNascitaESesso(this.anno, this.mese, this.giorno, this.sesso);

        String risultato = codiceCognome + codiceNome + codiceDataNascitaESesso;

        if (codiceFiscale.startsWith(risultato)) {
            return Boolean.TRUE;
        }
        else {
            return Boolean.FALSE;
        }
    }

    public Boolean verificaCodiceCatastale(String codiceFiscale, String codiceCatastale) {

        if (codiceFiscale == null || codiceFiscale.isEmpty() || codiceFiscale.length() != 16) {
            
            return Boolean.FALSE;
        }

        if (codiceCatastale == null || codiceCatastale.isEmpty() || codiceCatastale.length() != 4) {
            
            return Boolean.FALSE;
        }
        
        codiceFiscale = codiceFiscale.toUpperCase();
        codiceCatastale = codiceCatastale.toUpperCase();
        
        String codiceCastaleCodiceFiscale = codiceFiscale.substring(11, 15);
                
        if (!codiceCastaleCodiceFiscale.equals(codiceCatastale)) {
            
            return Boolean.FALSE;
        }
        
        return Boolean.TRUE;
    }
    
    private String generaCodiceFiscale(String cityCode) {
        
        String codiceCognome = this.calcolaCodiceCognome(this.cognome);
        String codiceNome = this.calcolaCodiceNome(this.nome);
        String codiceDataNascitaESesso = this.calcolaCodiceDataNascitaESesso(this.anno, this.mese, this.giorno, this.sesso);

        String risultato = codiceCognome + codiceNome + codiceDataNascitaESesso + cityCode;
        
        String carattereDiControllo = this.calcolaCarattereDiControllo(risultato.substring(0, 15));
        
        String codiceFiscale = risultato + carattereDiControllo;
        
        return codiceFiscale;
    }

    private String calcolaCodiceCognome(String cognome) {

        String codiceCognome;
        int numeroConsonanti;
        cognome = WordsHelper.eliminaSpaziBianchi(cognome).toUpperCase();

        if (cognome.length() >= 3) {

            numeroConsonanti = WordsHelper.getNumeroConsonanti(cognome);

            if (numeroConsonanti >= 3) {

                codiceCognome = WordsHelper.getPrimeConsonanti(cognome, 3);
            }
            else {

                codiceCognome = WordsHelper.getPrimeConsonanti(cognome, numeroConsonanti);
                codiceCognome += WordsHelper.getPrimeVocali(cognome, 3 - numeroConsonanti);
            }
        }
        else {

            int numeroCaratteri = cognome.length();
            codiceCognome = cognome + WordsHelper.nXChar(3 - numeroCaratteri);
        }

        return codiceCognome;
    }

    private String calcolaCodiceNome(String nome) {

        String codiceNome;
        int numeroConsonanti;
        nome = WordsHelper.eliminaSpaziBianchi(nome).toUpperCase();

        if (nome.length() >= 3) {

            numeroConsonanti = WordsHelper.getNumeroConsonanti(nome);

            if (numeroConsonanti >= 4) {

                codiceNome = WordsHelper.getConsonanteI(nome, 1) + WordsHelper.getConsonanteI(nome, 3) + WordsHelper.getConsonanteI(nome, 4);
            }
            else if (numeroConsonanti >= 3) {

                codiceNome = WordsHelper.getPrimeConsonanti(nome, 3);
            }
            else {

                codiceNome = WordsHelper.getPrimeConsonanti(nome, numeroConsonanti);
                codiceNome += WordsHelper.getPrimeVocali(nome, 3 - numeroConsonanti);
            }
        }
        else {

            int numeroCaratteri = nome.length();
            codiceNome = nome + WordsHelper.nXChar(3 - numeroCaratteri);
        }

        return codiceNome;
    }

    private String calcolaCodiceDataNascitaESesso(int anno, int mese, int giorno, String sesso) {

        String codiceDataNascitaESesso;
        String codiceAnno;
        String codiceMese;
        String codiceGiornoESesso;

        codiceAnno = calcolaCodiceAnno(anno);
        codiceMese = calcolaCodiceMese(mese);
        codiceGiornoESesso = calcolaCodiceGiornoESesso(giorno, sesso);

        codiceDataNascitaESesso = codiceAnno + codiceMese + codiceGiornoESesso;

        return codiceDataNascitaESesso;
    }

    private String calcolaCodiceAnno(int anno) {

        return Integer.toString(anno).substring(2);
    }

    private String calcolaCodiceMese(int mese) {

        String risultato;

        switch (mese) {
            case 1:
                risultato = "A";
                break;
            case 2:
                risultato = "B";
                break;
            case 3:
                risultato = "C";
                break;
            case 4:
                risultato = "D";
                break;
            case 5:
                risultato = "E";
                break;
            case 6:
                risultato = "H";
                break;
            case 7:
                risultato = "L";
                break;
            case 8:
                risultato = "M";
                break;
            case 9:
                risultato = "P";
                break;
            case 10:
                risultato = "R";
                break;
            case 11:
                risultato = "S";
                break;
            case 12:
                risultato = "T";
                break;
            default:
                risultato = "";
                break;
        }

        return risultato;
    }

    private String calcolaCodiceGiornoESesso(int giorno, String sesso) {

        String codiceGiorno = String.format("%02d", giorno);

        if (sesso.equals("F")) {

            int codiceGiornoIntero;
            codiceGiornoIntero = Integer.parseInt(codiceGiorno);
            codiceGiornoIntero += 40;
            codiceGiorno = Integer.toString(codiceGiornoIntero);
        }

        return codiceGiorno;
    }

    public static String calcolaCarattereDiControllo(String codice) {

        //Passaggio 1 (suddivisione dispari e pari)
        String pari = WordsHelper.getStringaPari(codice);
        String dispari = WordsHelper.getStringaDispari(codice);

        //Passaggio 2 (conversione valori)
        int sommaDispari = conversioneCaratteriDispari(dispari);
        int sommaPari = conversioneCaratteriPari(pari);

        //Passaggio 3 (somma, divisione e conversione finale)
        int somma = sommaDispari + sommaPari;
        int resto = (int) somma % 26;
        char restoConvertito = conversioneResto(resto);

        return Character.toString(restoConvertito);
    }

    private static int conversioneCaratteriDispari(String string) {
        int risultato = 0;
        for (int i = 0; i < string.length(); i++) {
            char carattere = string.charAt(i);
            switch (carattere) {
                case '0':
                case 'A':
                    risultato += 1;
                    break;
                case '1':
                case 'B':
                    risultato += 0;
                    break;
                case '2':
                case 'C':
                    risultato += 5;
                    break;
                case '3':
                case 'D':
                    risultato += 7;
                    break;
                case '4':
                case 'E':
                    risultato += 9;
                    break;
                case '5':
                case 'F':
                    risultato += 13;
                    break;
                case '6':
                case 'G':
                    risultato += 15;
                    break;
                case '7':
                case 'H':
                    risultato += 17;
                    break;
                case '8':
                case 'I':
                    risultato += 19;
                    break;
                case '9':
                case 'J':
                    risultato += 21;
                    break;
                case 'K':
                    risultato += 2;
                    break;
                case 'L':
                    risultato += 4;
                    break;
                case 'M':
                    risultato += 18;
                    break;
                case 'N':
                    risultato += 20;
                    break;
                case 'O':
                    risultato += 11;
                    break;
                case 'P':
                    risultato += 3;
                    break;
                case 'Q':
                    risultato += 6;
                    break;
                case 'R':
                    risultato += 8;
                    break;
                case 'S':
                    risultato += 12;
                    break;
                case 'T':
                    risultato += 14;
                    break;
                case 'U':
                    risultato += 16;
                    break;
                case 'V':
                    risultato += 10;
                    break;
                case 'W':
                    risultato += 22;
                    break;
                case 'X':
                    risultato += 25;
                    break;
                case 'Y':
                    risultato += 24;
                    break;
                case 'Z':
                    risultato += 23;
                    break;
            }
        }
        return risultato;
    }

    private static int conversioneCaratteriPari(String string) {

        int risultato = 0;

        for (int i = 0; i < string.length(); i++) {

            char carattere = string.charAt(i);
            int numero = Character.getNumericValue(carattere);

            if (Character.isLetter(carattere)) {
                //Se � una lettera
                numero = carattere - 65;
                risultato += numero;
            }
            else {
                //Se � un numero
                risultato += numero;
            }
        }
        return risultato;
    }

    private static char conversioneResto(int resto) {

        return (char) (resto + 65);
    }
}
