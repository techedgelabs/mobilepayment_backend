package com.techedge.mp.core.business.utilities;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_STATUS;
import com.techedge.mp.core.actions.scheduler.SchedulerJobStatisticsMyCiceroRefuelingReportAction.CustomReportDTO;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PromotionStatus;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.StationControlType;
import com.techedge.mp.core.business.interfaces.StatisticsObject;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.ActivityLogBean;
import com.techedge.mp.core.business.model.AdminBean;
import com.techedge.mp.core.business.model.AdminRoleBean;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.CardBinBean;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.CityInfoBean;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.EmailDomainBean;
import com.techedge.mp.core.business.model.EsPromotionBean;
import com.techedge.mp.core.business.model.EventCampaignBean;
import com.techedge.mp.core.business.model.LandingDataBean;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.LoyaltyCardMatcherBean;
import com.techedge.mp.core.business.model.MailingListBean;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.model.ManagerTicketBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.ParameterBean;
import com.techedge.mp.core.business.model.ParameterNotificationBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.PollingIntervalBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.PrefixNumberBean;
import com.techedge.mp.core.business.model.PromoCodeModeBean;
import com.techedge.mp.core.business.model.PromoPopupBean;
import com.techedge.mp.core.business.model.PromoUsersOnHoldBean;
import com.techedge.mp.core.business.model.PromoVoucherBean;
import com.techedge.mp.core.business.model.PromotionBean;
import com.techedge.mp.core.business.model.ProvinceInfoBean;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.RefuelingMappingErrorBean;
import com.techedge.mp.core.business.model.RefuelingSubscriptionsBean;
import com.techedge.mp.core.business.model.ResetPinCodeBean;
import com.techedge.mp.core.business.model.ShopTransactionDataBean;
import com.techedge.mp.core.business.model.SmsLogBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.StationControlBean;
import com.techedge.mp.core.business.model.StatisticalReportsBean;
import com.techedge.mp.core.business.model.SurveyBean;
import com.techedge.mp.core.business.model.SurveyQuestionBean;
import com.techedge.mp.core.business.model.SurveySubmissionBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.UnavailabilityPeriodBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserDeviceBean;
import com.techedge.mp.core.business.model.UserExternalDataBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.model.UserTypeBean;
import com.techedge.mp.core.business.model.VerificationCodeBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.crm.CRMEventBean;
import com.techedge.mp.core.business.model.crm.CRMOfferVoucherPromotionalBean;
import com.techedge.mp.core.business.model.crm.CRMOutboundBean;
import com.techedge.mp.core.business.model.crm.CRMPromotionBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfContactKeyBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfEventBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfOutboundBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfPromotionsBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfTokenBean;
import com.techedge.mp.core.business.model.dwh.DWHBrandDetailBean;
import com.techedge.mp.core.business.model.dwh.DWHCategoryBurnDetailBean;
import com.techedge.mp.core.business.model.dwh.DWHCategoryEarnDetailBean;
import com.techedge.mp.core.business.model.dwh.DWHEventBean;
import com.techedge.mp.core.business.model.dwh.DWHPartnerDetailBean;
import com.techedge.mp.core.business.model.loyalty.EventNotificationBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltySessionBean;
import com.techedge.mp.core.business.model.loyalty.RedemptionTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.model.station.StationStageBean;
import com.techedge.mp.core.business.model.voucher.VoucherEventCampaignBean;
import com.techedge.mp.core.business.model.voucher.VoucherPromotionalCrmSfBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionOperationBean;

public class QueryRepository {

    public QueryRepository() {}

    @SuppressWarnings("unchecked")
    public static UserBean findUserCustomerByEmail(EntityManager em, String email) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_EMAIL);

        query.setParameter("email", email);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();
        for (PersonalDataBean personalDataBean : personalDataBeanList) {

            //System.out.println("check user for personalData: " + personalDataBean.getId());

            userBean = QueryRepository.findUserByPersonalData(em, personalDataBean);

            if (userBean != null && userBean.getUserType() != User.USER_TYPE_BUSINESS) {

                //System.out.println("userBean found");

                return userBean;
            }
            else {

                //System.out.println("userBean not found");
            }
        }

        return userBean;

    }

    @SuppressWarnings("unchecked")
    public static UserBean findNotCancelledUserCustomerByEmail(EntityManager em, String email) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_EMAIL);

        query.setParameter("email", email);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();
        for (PersonalDataBean personalDataBean : personalDataBeanList) {

            //System.out.println("check user for personalData: " + personalDataBean.getId());

            userBean = QueryRepository.findUserByPersonalData(em, personalDataBean);

            if (userBean != null) {

                if (userBean.getUserStatus() != User.USER_STATUS_CANCELLED && userBean.getUserType() != User.USER_TYPE_BUSINESS) {

                    //System.out.println("userBean found");

                    return userBean;
                }
                else {

                    System.out.println("userBean cancelled or not customer");
                }
            }
            else {

                //System.out.println("userBean not found");
            }
        }

        return null;

    }

    @SuppressWarnings("unchecked")
    public static UserSocialDataBean findSocialUserByUUID(EntityManager em, String uuid, String provider) {

        Query query = em.createNamedQuery(UserSocialDataBean.FIND_SOCIAL_USER_BY_UUID_AND_STATUS);

        query.setParameter("uuid", uuid);
        query.setParameter("provider", provider);
        query.setParameter("userStatus", User.USER_STATUS_CANCELLED);

        List<UserSocialDataBean> result = query.getResultList();
        if (result.size() == 0) {
            System.out.println("userSocialDataBean not found");
            return null;
        }
        else if (result.size() > 1)
            throw new NonUniqueResultException();

        UserSocialDataBean userSocialDataBean = (UserSocialDataBean) result.get(0);
        System.out.println("userSocialDataBean found");
        return userSocialDataBean;

    }

    @SuppressWarnings("unchecked")
    public static UserBean findUserCustomerByFiscalCode(EntityManager em, String fiscalCode) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_FISCALCODE);

        query.setParameter("fiscalCode", fiscalCode);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();
        for (PersonalDataBean personalDataBean : personalDataBeanList) {

            System.out.println("check user for personalData: " + personalDataBean.getId());

            userBean = QueryRepository.findUserByPersonalData(em, personalDataBean);

            if (userBean != null && userBean.getUserType() != User.USER_TYPE_BUSINESS) {

                System.out.println("userBean customer found");

                return userBean;
            }
            else {

                System.out.println("userBean customer not found");
            }
        }

        return userBean;

    }

    @SuppressWarnings("unchecked")
    public static UserBean findNotCancelledUserCustomerByFiscalCode(EntityManager em, String fiscalCode) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_FISCALCODE);

        query.setParameter("fiscalCode", fiscalCode);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();
        for (PersonalDataBean personalDataBean : personalDataBeanList) {

            System.out.println("check user for personalData: " + personalDataBean.getId());

            userBean = QueryRepository.findUserByPersonalData(em, personalDataBean);

            if (userBean != null) {

                if (userBean.getUserStatus() != User.USER_STATUS_CANCELLED && userBean.getUserType() != User.USER_TYPE_BUSINESS) {

                    System.out.println("userBean customer found");

                    return userBean;
                }
                else {

                    System.out.println("userBean cancelled");
                }
            }
            else {

                System.out.println("userBean not found");
            }
        }

        return null;

    }
    
    @SuppressWarnings("unchecked")
    public static UserBean findNotCancelledAndRegCompleteUserCustomerByFiscalCode(EntityManager em, String fiscalCode) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_FISCALCODE);

        query.setParameter("fiscalCode", fiscalCode);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();
        for (PersonalDataBean personalDataBean : personalDataBeanList) {

            System.out.println("check user for personalData: " + personalDataBean.getId());

            userBean = QueryRepository.findUserByPersonalData(em, personalDataBean);

            if (userBean != null) {

                if (userBean.getUserStatus() != User.USER_STATUS_CANCELLED && userBean.getUserType() != User.USER_TYPE_BUSINESS && userBean.getUserStatus().intValue() != User.USER_STATUS_NEW.intValue()) {

                    System.out.println("userBean customer found");

                    return userBean;
                }
                else {

                    System.out.println("userBean cancelled or registration not completed");
                }
            }
            else {

                System.out.println("userBean not found");
            }
        }

        return null;

    }

    @SuppressWarnings("unchecked")
    public static List<PersonalDataBean> findPersonalDataBeanByEmail(EntityManager em, String email) {

        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_EMAIL);

        query.setParameter("email", email);
        query.setMaxResults(1);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();

        return personalDataBeanList;

    }

    /*
     * @SuppressWarnings("unchecked")
     * public static PersonalDataBean findPersonalDataBeanByEmail(EntityManager em, String email) {
     * 
     * PersonalDataBean personalDataBean = null;
     * 
     * Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_EMAIL);
     * 
     * query.setParameter("email", email);
     * query.setMaxResults(1);
     * 
     * List<PersonalDataBean> personalDataBeanList = query.getResultList();
     * 
     * if (personalDataBeanList != null && personalDataBeanList.size() > 0) {
     * personalDataBean = (PersonalDataBean) personalDataBeanList.get(0);
     * }
     * else {
     * personalDataBean = null;
     * }
     * 
     * return personalDataBean;
     * 
     * }
     * 
     * @SuppressWarnings("unchecked")
     * public static PersonalDataBean findPersonalDataBeanByFiscalCode(EntityManager em, String fiscalCode) {
     * 
     * PersonalDataBean personalDataBean = null;
     * 
     * Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_FISCALCODE);
     * 
     * query.setParameter("fiscalCode", fiscalCode.toUpperCase());
     * query.setMaxResults(1);
     * 
     * List<PersonalDataBean> personalDataBeanList = query.getResultList();
     * 
     * if (personalDataBeanList != null && personalDataBeanList.size() > 0) {
     * personalDataBean = (PersonalDataBean) personalDataBeanList.get(0);
     * }
     * else {
     * personalDataBean = null;
     * }
     * 
     * return personalDataBean;
     * 
     * }
     */
    @SuppressWarnings("unchecked")
    public static UserBean findUserById(EntityManager em, Long id) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(UserBean.FIND_BY_ID);

        query.setParameter("id", id);
        query.setMaxResults(1);

        List<UserBean> userBeanList = query.getResultList();

        if (userBeanList != null && userBeanList.size() > 0) {
            userBean = (UserBean) userBeanList.get(0);
        }
        else {
            userBean = null;
        }

        return userBean;

    }

    @SuppressWarnings("unchecked")
    public static UserBean findUserByPersonalData(EntityManager em, PersonalDataBean personalDataBean) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(UserBean.FIND_BY_PERSONALDATABEAN);

        query.setParameter("personalDataBean", personalDataBean);
        query.setMaxResults(1);

        List<UserBean> userBeanList = query.getResultList();

        if (userBeanList != null && userBeanList.size() > 0) {
            userBean = (UserBean) userBeanList.get(0);
        }
        else {
            userBean = null;
        }

        return userBean;

    }

    @SuppressWarnings("unchecked")
    public static UserBean findUserByPersonalDataAndNotStatus(EntityManager em, PersonalDataBean personalDataBean, Integer status) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(UserBean.FIND_BY_PERSONALDATABEANANDNOTSTATUS);

        query.setParameter("personalDataBean", personalDataBean);
        query.setParameter("userStatus", status);
        query.setMaxResults(1);

        List<UserBean> userBeanList = query.getResultList();

        if (userBeanList != null && userBeanList.size() > 0) {
            userBean = (UserBean) userBeanList.get(0);
        }
        else {
            userBean = null;
        }

        return userBean;

    }

    @SuppressWarnings("unchecked")
    public static ShopTransactionDataBean findShopTransactionBeanDataByPaymentInfo(EntityManager em, PaymentInfoBean paymentInfoBean) {

        ShopTransactionDataBean shopTransactionDataBean = null;

        Query query = em.createNamedQuery(ShopTransactionDataBean.FIND_BY_PAYMENTINFO);

        query.setParameter("paymentInfo", paymentInfoBean);
        query.setMaxResults(1);

        List<ShopTransactionDataBean> shopTransactionDataBeanList = query.getResultList();

        if (shopTransactionDataBeanList != null && shopTransactionDataBeanList.size() > 0) {
            shopTransactionDataBean = (ShopTransactionDataBean) shopTransactionDataBeanList.get(0);
        }
        else {
            shopTransactionDataBean = null;
        }

        return shopTransactionDataBean;

    }

    @SuppressWarnings("unchecked")
    public static ShopTransactionDataBean findShopTransactionBeanDataByTransactionIdAndStatus(EntityManager em, String transactionId, Integer status) {

        ShopTransactionDataBean shopTransactionDataBean = null;

        Query query = em.createNamedQuery(ShopTransactionDataBean.FIND_BY_TRANSACTIONIDANDSTATUS);

        query.setParameter("transactionId", transactionId);
        query.setParameter("status", status);
        query.setMaxResults(1);

        List<ShopTransactionDataBean> shopTransactionDataBeanList = query.getResultList();

        if (shopTransactionDataBeanList != null && shopTransactionDataBeanList.size() > 0) {
            shopTransactionDataBean = (ShopTransactionDataBean) shopTransactionDataBeanList.get(0);
        }
        else {
            shopTransactionDataBean = null;
        }

        return shopTransactionDataBean;

    }

    @SuppressWarnings("unchecked")
    public static TicketBean findTicketById(EntityManager em, String ticketId) {

        TicketBean ticketBean = null;

        Query query = em.createNamedQuery(TicketBean.FIND_BY_TICKETID);

        query.setParameter("ticketId", ticketId);
        query.setMaxResults(1);

        List<TicketBean> ticketBeanList = query.getResultList();

        if (ticketBeanList != null && ticketBeanList.size() > 0) {
            ticketBean = (TicketBean) ticketBeanList.get(0);
        }
        else {
            ticketBean = null;
        }

        return ticketBean;

    }

    @SuppressWarnings("unchecked")
    public static List<TicketBean> findTicketByUser(EntityManager em, UserBean userBean) {

        Query query = em.createNamedQuery(TicketBean.FIND_BY_USERID);
        query.setParameter("userid", userBean);
        Date date = new Date();
        date.setTime(0);
        query.setParameter("checkDate", date);
        List<TicketBean> ticketBeanList = query.getResultList();

        return ticketBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TicketBean> findTicketByUserAndCheckDate(EntityManager em, UserBean userBean, Date date) {

        Query query = em.createNamedQuery(TicketBean.FIND_BY_USERID);
        query.setParameter("userid", userBean);
        query.setParameter("checkDate", date);
        List<TicketBean> ticketBeanList = query.getResultList();

        return ticketBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TicketBean> findTicketsActive(EntityManager em) {

        // TicketBean ticketBean = null;

        Query query = em.createNamedQuery(TicketBean.FIND_TICKET_ACTIVE);
        query.setParameter("checkDate", new Date());

        List<TicketBean> ticketBeanList = query.getResultList();

        return ticketBeanList;

    }

    @SuppressWarnings("unchecked")
    public static Integer countActiveTickets(EntityManager em) {

        Query query = em.createNamedQuery(TicketBean.COUNT_TICKET_ACTIVE);
        query.setParameter("checkDate", new Date());

        Integer activeTickets;

        query.setMaxResults(1);

        List<Long> resultList = query.getResultList();
        if (resultList != null && resultList.size() > 0) {
            Long result = resultList.get(0);
            activeTickets = Integer.valueOf(result.intValue());
        }
        else {
            activeTickets = 0;
        }

        return activeTickets;

    }

    @SuppressWarnings("unchecked")
    public static PaymentInfoBean findPaymentInfoBeanByUserBean(EntityManager em, UserBean userBean) {

        PaymentInfoBean paymentInfoBean = null;

        Query query = em.createNamedQuery(PaymentInfoBean.FIND_BY_USERBEAN);

        query.setParameter("userBean", userBean);
        query.setMaxResults(1);

        List<PaymentInfoBean> paymentInfoBeanList = query.getResultList();

        if (paymentInfoBeanList != null && paymentInfoBeanList.size() > 0) {
            paymentInfoBean = (PaymentInfoBean) paymentInfoBeanList.get(0);
        }
        else {
            paymentInfoBean = null;
        }

        return paymentInfoBean;

    }

    @SuppressWarnings("unchecked")
    public static List<PaymentInfoBean> findOldPaymentInfoBeanByUserBean(EntityManager em, Date dayToCompare) {

        Query query = em.createNamedQuery(PaymentInfoBean.FIND_OLD_BY_ID);
        query.setParameter("dayToCompare", dayToCompare);

        List<PaymentInfoBean> paymentInfoBeanList = query.getResultList();

        return paymentInfoBeanList;

    }

    @SuppressWarnings("unchecked")
    public static VerificationCodeBean findVerificationCodeBeanById(EntityManager em, String verificationCodeId) {

        VerificationCodeBean verificationCodeBean = null;

        Query query = em.createNamedQuery(VerificationCodeBean.FIND_BY_VERIFICATIONCODEID);

        query.setParameter("verificationCodeId", verificationCodeId);
        query.setMaxResults(1);

        List<VerificationCodeBean> verificationCodeBeanList = query.getResultList();
        if (verificationCodeBeanList != null && verificationCodeBeanList.size() > 0) {
            verificationCodeBean = (VerificationCodeBean) verificationCodeBeanList.get(0);
        }
        else {
            verificationCodeBean = null;
        }

        return verificationCodeBean;

    }

    @SuppressWarnings("unchecked")
    public static List<VerificationCodeBean> findVerificationCodeBeanByUser(EntityManager em, UserBean user) {

        // VerificationCodeBean verificationCodeBean = null;

        Query query = em.createNamedQuery(VerificationCodeBean.FIND_BY_USER);

        query.setParameter("user", user);
        // query.setMaxResults(1);

        List<VerificationCodeBean> verificationCodeBeanList = query.getResultList();
        // if (verificationCodeBeanList != null && verificationCodeBeanList.size() >
        // 0) {
        // verificationCodeBean = (VerificationCodeBean)
        // verificationCodeBeanList.get(0);
        // }
        // else {
        // verificationCodeBean = null;
        // }

        return verificationCodeBeanList;

    }

    @SuppressWarnings("unchecked")
    public static VerificationCodeBean findVerificationCodeBeanByUserAndStatus(EntityManager em, UserBean user, Integer status) {

        VerificationCodeBean verificationCodeBean = null;

        Query query = em.createNamedQuery(VerificationCodeBean.FIND_BY_USER_AND_STATUS);

        query.setParameter("user", user);
        query.setParameter("status", status);
        query.setMaxResults(1);

        List<VerificationCodeBean> verificationCodeBeanList = query.getResultList();
        if (verificationCodeBeanList != null && verificationCodeBeanList.size() > 0) {
            verificationCodeBean = (VerificationCodeBean) verificationCodeBeanList.get(0);
        }
        else {
            verificationCodeBean = null;
        }

        return verificationCodeBean;

    }

    @SuppressWarnings("unchecked")
    public static TransactionBean findTransactionBeanById(EntityManager em, String transactionID) {

        TransactionBean transactionBean = null;

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_ID);

        query.setParameter("transactionID", transactionID);
        query.setMaxResults(1);

        List<TransactionBean> transactionBeanList = query.getResultList();
        if (transactionBeanList != null && transactionBeanList.size() > 0) {
            transactionBean = (TransactionBean) transactionBeanList.get(0);
        }
        else {
            transactionBean = null;
        }

        return transactionBean;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionOnHold(EntityManager em, String mpTransactionID, Date dayToCompare) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_ONHOLD);
        query.setParameter("mpTransaction", mpTransactionID);
        query.setParameter("time", dayToCompare);

        List<PostPaidTransactionBean> poPTransactionBeanList = query.getResultList();

        return poPTransactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionBeanByStationBeanAndDate(EntityManager em, StationBean stationBean, Date startDate, Date endDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_BY_STATION_AND_DATE);

        query.setParameter("stationBean", stationBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<PostPaidTransactionBean> poPTransactionBeanList = query.getResultList();

        return poPTransactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionHistoryBean> findPostPaidTransactionHistoryBeanByStationBeanAndDate(EntityManager em, StationBean stationBean, Date startDate,
            Date endDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.FIND_BY_STATION_AND_DATE);

        query.setParameter("stationBean", stationBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<PostPaidTransactionHistoryBean> poPTransactionHistoryBeanList = query.getResultList();

        return poPTransactionHistoryBeanList;
    }

    @SuppressWarnings("unchecked")
    public static PostPaidTransactionBean findPostPaidTransactionBeanByMPId(EntityManager em, String mpTransactionID) {

        PostPaidTransactionBean poPtransactionBean = null;

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_BY_MP_ID);

        query.setParameter("mpTransactionID", mpTransactionID);
        query.setMaxResults(1);

        List<PostPaidTransactionBean> transactionBeanList = query.getResultList();
        if (transactionBeanList != null && transactionBeanList.size() > 0) {
            poPtransactionBean = (PostPaidTransactionBean) transactionBeanList.get(0);
        }
        else {
            poPtransactionBean = null;
        }

        return poPtransactionBean;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionBeanBySRCId(EntityManager em, String srcTransactionID) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_BY_SRC_ID);
        query.setParameter("srcTransactionID", srcTransactionID);

        List<PostPaidTransactionBean> poPTransactionBeanList = query.getResultList();

        return poPTransactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionBeanBySRCIdAndSource(EntityManager em, String srcTransactionID, String sourceID) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_BY_SRC_ID_AND_SOURCE);
        query.setParameter("srcTransactionID", srcTransactionID);
        query.setParameter("sourceID", sourceID);

        List<PostPaidTransactionBean> poPTransactionBeanList = query.getResultList();

        return poPTransactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static PostPaidTransactionHistoryBean findPostPaidTransactionHistoryBeanByMPId(EntityManager em, String mpTransactionID) {

        PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = null;

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.FIND_BY_MP_ID);

        query.setParameter("mpTransactionID", mpTransactionID);
        query.setMaxResults(1);

        List<PostPaidTransactionHistoryBean> postPaidTransactionHistoryList = query.getResultList();
        if (postPaidTransactionHistoryList != null && postPaidTransactionHistoryList.size() > 0) {
            postPaidTransactionHistoryBean = (PostPaidTransactionHistoryBean) postPaidTransactionHistoryList.get(0);
        }
        else {
            postPaidTransactionHistoryBean = null;
        }

        return postPaidTransactionHistoryBean;

    }

    @SuppressWarnings("unchecked")
    public static PostPaidTransactionBean findPostPaidTransactionBeanByQRId(EntityManager em, String qrCode) {

        PostPaidTransactionBean poPtransactionBean = null;

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_BY_QR_CODE);

        query.setParameter("sourceID", qrCode);
        query.setParameter("mpTransactionStatus", StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD);

        query.setMaxResults(1);

        List<PostPaidTransactionBean> transactionBeanList = query.getResultList();
        if (transactionBeanList != null && transactionBeanList.size() > 0) {
            poPtransactionBean = (PostPaidTransactionBean) transactionBeanList.get(0);
        }
        else {
            poPtransactionBean = null;
        }

        return poPtransactionBean;

    }

    @SuppressWarnings("unchecked")
    public static PostPaidTransactionBean findPostPaidLastTransactionBeanByQRId(EntityManager em, String qrCode) {

        PostPaidTransactionBean postPaidTransactionBean = null;

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_LAST_BY_QR_CODE);

        query.setParameter("sourceID", qrCode);

        query.setMaxResults(1);

        List<PostPaidTransactionBean> popTransactionBeanList = query.getResultList();

        if (popTransactionBeanList != null && popTransactionBeanList.size() > 0) {
            postPaidTransactionBean = (PostPaidTransactionBean) popTransactionBeanList.get(0);
        }
        else {
            postPaidTransactionBean = null;
        }

        return postPaidTransactionBean;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionBeanByMPStatus(EntityManager em, String status) {

        // PostPaidTransactionBean poPtransactionBean = null;

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_BY_STATUS);
        query.setParameter("mpTransactionStatus", status);

        // query.setMaxResults(1);

        List<PostPaidTransactionBean> poPTransactionBeanList = query.getResultList();
        // if (transactionBeanList != null && transactionBeanList.size() > 0) {
        // poPtransactionBean = (PostPaidTransactionBean)
        // transactionBeanList.get(0);
        // }
        // else {
        // poPtransactionBean = null;
        // }

        return poPTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static TransactionHistoryBean findTransactionHistoryBeanById(EntityManager em, String transactionID) {

        TransactionHistoryBean transactionHistoryBean = null;

        Query query = em.createNamedQuery(TransactionHistoryBean.FIND_BY_ID);

        query.setParameter("transactionID", transactionID);
        query.setMaxResults(1);

        List<TransactionHistoryBean> transactionHistoryBeanList = query.getResultList();
        if (transactionHistoryBeanList != null && transactionHistoryBeanList.size() > 0) {
            transactionHistoryBean = (TransactionHistoryBean) transactionHistoryBeanList.get(0);
        }
        else {
            transactionHistoryBean = null;
        }

        return transactionHistoryBean;

    }

    @SuppressWarnings("unchecked")
    public static StationBean findStationBeanById(EntityManager em, String stationID) {

        StationBean stationBean = null;

        Query query = em.createNamedQuery(StationBean.FIND_BY_ID);

        query.setParameter("stationID", stationID);
        query.setMaxResults(1);

        List<StationBean> stationBeanList = query.getResultList();
        if (stationBeanList != null && stationBeanList.size() > 0) {
            stationBean = (StationBean) stationBeanList.get(0);
        }
        else {
            stationBean = null;
        }

        return stationBean;

    }

    @SuppressWarnings("unchecked")
    public static StationBean findActiveStationBeanById(EntityManager em, String stationID) {

        StationBean stationBean = null;

        Query query = em.createNamedQuery(StationBean.FIND_ACTIVE_BY_ID);

        query.setParameter("stationID", stationID);
        query.setMaxResults(1);

        List<StationBean> stationBeanList = query.getResultList();
        if (stationBeanList != null && stationBeanList.size() > 0) {
            stationBean = (StationBean) stationBeanList.get(0);
        }
        else {
            stationBean = null;
        }

        return stationBean;

    }

    @SuppressWarnings("unchecked")
    public static List<StationBean> getAllStationBeans(EntityManager em) {

        Query query = em.createNamedQuery(StationBean.FIND_ALL);

        List<StationBean> stationBeanList = query.getResultList();

        return stationBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<StationBean> getAllActiveStationBeans(EntityManager em) {

        Query query = em.createNamedQuery(StationBean.FIND_ALL_ACTIVE);

        List<StationBean> stationBeanList = query.getResultList();

        return stationBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<CityInfoBean> findCitiesByKey(EntityManager em, String key, Integer maxResults) {

        Query query = em.createNamedQuery(CityInfoBean.FIND_BY_KEY);

        query.setParameter("key", key + "%");
        query.setMaxResults(maxResults);

        List<CityInfoBean> cityInfoBeanList = query.getResultList();

        return cityInfoBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsByFinalStatusTypeAndNotification(EntityManager em, String finalStatusType, Boolean gfgNotification) {

        Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
        calendar.add(Calendar.MINUTE, -30);
        System.out.println("Data di recupero delle transazioni senza notifica: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(calendar.getTime()));

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_FINAL_STATUS_TYPE_AND_NOTIFICATION);

        query.setParameter("finalStatusType", finalStatusType);
        query.setParameter("gfgNotification", gfgNotification);
        query.setParameter("startTime", calendar.getTime());

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsByFinalStatusTypeAndNotificationDate(EntityManager em, String finalStatusType, Boolean gfgNotification, Date time) {

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_FINAL_STATUS_TYPE_AND_NOTIFICATION_DATE);

        query.setParameter("finalStatusType", finalStatusType);
        query.setParameter("gfgNotification", gfgNotification);
        query.setParameter("time", time);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsByFinalStatusTypeAndVoucherReconciliation(EntityManager em, String finalStatusType, Boolean voucherReconciliation) {

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_FINAL_STATUS_TYPE_AND_VOUCHER_RECONCILIATION);

        query.setParameter("finalStatusType", finalStatusType);
        query.setParameter("voucherReconciliation", voucherReconciliation);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsByFinalStatusTypeAndLoyaltyReconciliation(EntityManager em, String finalStatusType, Boolean loyaltyReconciliation) {

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_FINAL_STATUS_TYPE_AND_LOYALTY_RECONCILIATION);

        query.setParameter("finalStatusType", finalStatusType);
        query.setParameter("loyaltyReconcile", loyaltyReconciliation);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionsPaymentReconciliation(EntityManager em) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_PAYMENT_TO_RECONCILIATION);

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionsVoucherReconciliation(EntityManager em) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_VOUCHER_TO_RECONCILIATION);

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionsLoyaltyReconciliation(EntityManager em) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_LOYALTY_TO_RECONCILIATION);

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsByFinalStatusType(EntityManager em, String finalStatusType) {

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_FINAL_STATUS_TYPE);

        query.setParameter("finalStatusType", finalStatusType);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsByFinalStatusTypeDate(EntityManager em, String finalStatusType, Date time) {

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_FINAL_STATUS_TYPE_DATE);

        query.setParameter("finalStatusType", finalStatusType);
        query.setParameter("time", time);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsActiveByUserBean(EntityManager em, UserBean userBean) {

        Query query = em.createNamedQuery(TransactionBean.FIND_ACTIVE_BY_USER);

        query.setParameter("userBean", userBean);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsActiveByFinalStatusNull(EntityManager em) {

        Query query = em.createNamedQuery(TransactionBean.FIND_ACTIVE_BY_FINAL_STATUS_NULL);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsByDate(EntityManager em, Date startDate, Date endDate) {

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_DATE);

        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionsPaidByDate(EntityManager em, Date startDate, Date endDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_PAID_BY_DATE);

        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsByDateAndSrcTransaction(EntityManager em, Date startDate, Date endDate) {

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_DATE_SRC_TRANSACTION);

        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionToHistoryByStatusType(EntityManager em) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_TO_HISTORY_BY_STATUS_TYPE);

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionToHistory(EntityManager em, Date time) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_TO_HISTORY);
        query.setParameter("time", time);

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionsActiveByUserBean(EntityManager em, UserBean userBean) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_ACTIVE_BY_USER);

        query.setParameter("userBean", userBean);

        List<PostPaidTransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsActive(EntityManager em) {

        Query query = em.createNamedQuery(TransactionBean.FIND_ACTIVE_NEW);

        // query.setParameter( "userBean", userBean );

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static Integer countActiveTransactions(EntityManager em) {

        Query query = em.createNamedQuery(TransactionBean.COUNT_ACTIVE_NEW);

        Integer activeTransactions;

        query.setMaxResults(1);

        List<Long> resultList = query.getResultList();
        if (resultList != null && resultList.size() > 0) {
            Long result = resultList.get(0);
            activeTransactions = Integer.valueOf(result.intValue());
        }
        else {
            activeTransactions = 0;
        }

        return activeTransactions;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsByUserBeanAndDate(EntityManager em, UserBean userBean, Date startDate, Date endDate, int itemForPage, Boolean detail) {

        Query query = null;

        if (detail) {
            query = em.createNamedQuery(TransactionBean.FIND_SUCCESSFUL_BY_USER_AND_DATE);
        }
        else {
            query = em.createNamedQuery(TransactionBean.FIND_BY_USER_AND_DATE);
        }

        query.setParameter("userBean", userBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        if (detail && itemForPage >= 0) {
            query.setMaxResults(itemForPage);
        }

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionHistoryBean> findTransactionsHistoryByUserBeanAndDate(EntityManager em, UserBean userBean, Date startDate, Date endDate, int itemForPage,
            Boolean detail) {

        Query query = null;

        if (detail) {
            query = em.createNamedQuery(TransactionHistoryBean.FIND_SUCCESSFUL_BY_USER_AND_DATE);
        }
        else {
            query = em.createNamedQuery(TransactionHistoryBean.FIND_BY_USER_AND_DATE);
        }

        query.setParameter("userBean", userBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        if (detail && itemForPage >= 0) {
            query.setMaxResults(itemForPage);
        }

        List<TransactionHistoryBean> transactionHistoryBeanList = query.getResultList();

        return transactionHistoryBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionsByUserBeanAndDate(EntityManager em, UserBean userBean, Date startDate, Date endDate, int itemForPage,
            Boolean detail) {

        Query query = null;

        if (detail) {
            query = em.createNamedQuery(PostPaidTransactionBean.FIND_PAID_BY_USER_AND_DATE);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.FIND_BY_USER_AND_DATE);
        }

        query.setParameter("userBean", userBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        if (detail && itemForPage >= 0) {
            query.setMaxResults(itemForPage);
        }

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionsOnHoldByUserBeanAndStationBean(EntityManager em, UserBean userBean, StationBean stationBean) {

        Query query = null;

        query = em.createNamedQuery(PostPaidTransactionBean.FIND_ONHOLD_BY_USER_AND_STATION);
        query.setParameter("userBean", userBean);
        query.setParameter("stationBean", stationBean);

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionHistoryBean> findPostPaidTransactionsHistoryByUserBeanAndDate(EntityManager em, UserBean userBean, Date startDate, Date endDate,
            int itemForPage, Boolean detail) {

        Query query = null;

        if (detail) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.FIND_PAID_BY_USER_AND_DATE);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.FIND_BY_USER_AND_DATE);
        }

        query.setParameter("userBean", userBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        if (detail && itemForPage >= 0) {
            query.setMaxResults(itemForPage);
        }

        List<PostPaidTransactionHistoryBean> postPaidTransactionHistoryBeanList = query.getResultList();

        return postPaidTransactionHistoryBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<VoucherTransactionBean> findVoucherTransactionsByUserBeanAndDate(EntityManager em, UserBean userBean, Date startDate, Date endDate, int itemForPage,
            Boolean detail) {

        Query query = null;

        if (detail) {
            query = em.createNamedQuery(VoucherTransactionBean.FIND_SUCCESSFUL_BY_USER_AND_DATE);
        }
        else {
            query = em.createNamedQuery(VoucherTransactionBean.FIND_BY_USER_AND_DATE);
        }

        query.setParameter("userBean", userBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        if (detail && itemForPage >= 0) {
            query.setMaxResults(itemForPage);
        }

        List<VoucherTransactionBean> voucherTransactionBeanList = query.getResultList();

        return voucherTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<VoucherTransactionBean> findVoucherTransactionsByVoucherCode(EntityManager em, String voucherCode) {

        Query query = em.createNamedQuery(VoucherTransactionBean.FIND_BY_VOUCHER_CODE);

        query.setParameter("voucherCode", voucherCode);

        List<VoucherTransactionBean> voucherTransactionBeanList = query.getResultList();

        return voucherTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static PushNotificationBean findPushNotificationById(EntityManager em, Long id) {

        PushNotificationBean pushNotificationBean = null;

        Query query = em.createNamedQuery(PushNotificationBean.FIND_BY_ID);

        query.setParameter("id", id);
        query.setMaxResults(1);

        List<PushNotificationBean> pushNotificationBeanList = query.getResultList();

        if (pushNotificationBeanList != null && pushNotificationBeanList.size() > 0) {
            pushNotificationBean = (PushNotificationBean) pushNotificationBeanList.get(0);
        }
        else {
            pushNotificationBean = null;
        }

        return pushNotificationBean;

    }

    @SuppressWarnings("unchecked")
    public static EventNotificationBean findEventNotificationById(EntityManager em, Long id) {

        EventNotificationBean eventNotificationBean = null;

        Query query = em.createNamedQuery(EventNotificationBean.FIND_BY_ID);

        query.setParameter("id", id);
        query.setMaxResults(1);

        List<EventNotificationBean> eventNotificationBeanList = query.getResultList();

        if (eventNotificationBeanList != null && eventNotificationBeanList.size() > 0) {
            eventNotificationBean = (EventNotificationBean) eventNotificationBeanList.get(0);
        }
        else {
            eventNotificationBean = null;
        }

        return eventNotificationBean;

    }

    @SuppressWarnings("unchecked")
    public static EventNotificationBean findEventNotificationSuccessBySrcTransactionID(EntityManager em, String srcTransactionID, String eventType) {

        EventNotificationBean eventNotificationBean = null;

        Query query = em.createNamedQuery(EventNotificationBean.FIND_SUCCESS_BY_SRCTRANSACTIONID);

        query.setParameter("srcTransactionID", srcTransactionID);
        query.setParameter("eventType", eventType);
        query.setMaxResults(1);

        List<EventNotificationBean> eventNotificationBeanList = query.getResultList();

        if (eventNotificationBeanList != null && eventNotificationBeanList.size() > 0) {
            eventNotificationBean = (EventNotificationBean) eventNotificationBeanList.get(0);
        }
        else {
            eventNotificationBean = null;
        }

        return eventNotificationBean;

    }

    @SuppressWarnings("unchecked")
    public static AdminBean findAdminByEmail(EntityManager em, String email) {

        AdminBean adminBean = null;

        Query query = em.createNamedQuery(AdminBean.FIND_BY_EMAIL);

        query.setParameter("email", email);
        query.setMaxResults(1);

        List<AdminBean> adminBeanList = query.getResultList();

        if (adminBeanList != null && adminBeanList.size() > 0) {
            adminBean = (AdminBean) adminBeanList.get(0);
        }
        else {
            adminBean = null;
        }

        return adminBean;

    }

    @SuppressWarnings("unchecked")
    public static AdminBean findAdminByID(EntityManager em, Long id) {

        AdminBean adminBean = null;

        Query query = em.createNamedQuery(AdminBean.FIND_BY_ID);

        query.setParameter("id", id);
        query.setMaxResults(1);

        List<AdminBean> adminBeanList = query.getResultList();

        if (adminBeanList != null && adminBeanList.size() > 0) {
            adminBean = (AdminBean) adminBeanList.get(0);
        }
        else {
            adminBean = null;
        }

        return adminBean;

    }

    @SuppressWarnings("unchecked")
    public static List<AdminBean> findAdminByStatus(EntityManager em, Integer status) {

        Query query = em.createNamedQuery(AdminBean.FIND_BY_STATUS);

        query.setParameter("status", status);

        List<AdminBean> adminBeanList = query.getResultList();

        return adminBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<AdminBean> findAdmins(EntityManager em) {

        Query query = em.createNamedQuery(AdminBean.FIND_ALL);

        List<AdminBean> adminBeanList = query.getResultList();

        return adminBeanList;

    }

    @SuppressWarnings("unchecked")
    public static AdminTicketBean findAdminTicketById(EntityManager em, String ticketId) {

        AdminTicketBean adminTicketBean = null;

        Query query = em.createNamedQuery(AdminTicketBean.FIND_BY_TICKETID);

        query.setParameter("ticketId", ticketId);
        query.setMaxResults(1);

        List<AdminTicketBean> adminTicketBeanList = query.getResultList();

        if (adminTicketBeanList != null && adminTicketBeanList.size() > 0) {
            adminTicketBean = (AdminTicketBean) adminTicketBeanList.get(0);
        }
        else {
            adminTicketBean = null;
        }

        return adminTicketBean;

    }

    @SuppressWarnings("unchecked")
    public static List<ActivityLogBean> searchActivityLog(EntityManager em, Timestamp start, Timestamp end, ErrorLevel minLevel, ErrorLevel maxLevel, String source,
            String groupId, String phaseId, String messagePattern, Integer maxResults) {

        Query query = em.createNamedQuery(ActivityLogBean.SEARCH);

        query.setParameter("start", start);
        query.setParameter("end", end);
        query.setParameter("minLevel", minLevel);
        query.setParameter("maxLevel", maxLevel);
        query.setParameter("source", source);
        query.setParameter("groupId", groupId);
        query.setParameter("phaseId", phaseId);
        query.setParameter("messagePattern", messagePattern);

        // query.setFirstResult(100);
        query.setMaxResults(maxResults);

        List<ActivityLogBean> activityLogBeanList = query.getResultList();

        return activityLogBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<UserBean> searchUser(EntityManager em, String lastName, String firstName, String fiscalCode, String securityDataEmail, Integer userStatus,
            String externalUserId, Double capAvailableMin, Double capAvailableMax, Double capEffectiveMin, Double capEffectiveMax, Date creationDateMin, Date creationDateMax,
            Integer maxResults, Integer type) {

        Query query;
        if (userStatus != null) {
            if (externalUserId != null) {
                query = em.createNamedQuery(UserBean.FIND_BY_USERSEARCHPD_YES_USERSTATUS_YES_EXTERNALID);
                query.setParameter("userstatus", userStatus);
                query.setParameter("externaluserid", externalUserId);
            }
            else {
                query = em.createNamedQuery(UserBean.FIND_BY_USERSEARCHPD_YES_USERSTATUS_NO_EXTERNALID);
                query.setParameter("userstatus", userStatus);
            }
        }
        else {
            if (externalUserId != null) {
                query = em.createNamedQuery(UserBean.FIND_BY_USERSEARCHPD_NO_USERSTATUS_YES_EXTERNALID);
                query.setParameter("externaluserid", externalUserId);
            }
            else {
                query = em.createNamedQuery(UserBean.FIND_BY_USERSEARCHPD_NO_USERSTATUS_NO_EXTERNALID);
            }
        }

        query.setParameter("firstname", firstName);
        query.setParameter("lastname", lastName);
        query.setParameter("fiscalcode", fiscalCode);
        query.setParameter("email", securityDataEmail);
        query.setParameter("capavailablemin", capAvailableMin);
        query.setParameter("capavailablemax", capAvailableMax);
        query.setParameter("capeffectivemin", capEffectiveMin);
        query.setParameter("capeffectivemax", capEffectiveMax);
        query.setParameter("type", type);

        // query.setFirstResult(100);
        query.setMaxResults(maxResults);

        List<UserBean> userBeanList = query.getResultList();

        return userBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> searchTransactions(EntityManager em, Long userId, String stationId, String pumpId, String finalStatusType, Boolean GFGNotification,
            Boolean confirmed, Date creationTimestampStart, Date creationTimestampEnd, String productID, Integer maxResults) {

        UserBean userBean = null;
        StationBean stationBean = null;

        List<TransactionBean> transactionBeanList = new ArrayList<TransactionBean>(0);

        String queryString = "select t from TransactionBean t where 1=1";

        if (userId != null) {
            userBean = QueryRepository.findUserById(em, userId);
            if (userBean != null) {
                queryString += " and userBean = :userBean";
            }
            else {
                return transactionBeanList;
            }
        }

        if (stationId != null) {
            stationBean = QueryRepository.findStationBeanById(em, stationId);
            if (stationBean != null) {
                queryString += " and stationBean = :stationBean";
            }
            else {
                return transactionBeanList;
            }
        }

        if (GFGNotification != null) {
            queryString += " and GFGNotification = :GFGNotification";
        }

        if (confirmed != null) {
            queryString += " and confirmed = :confirmed";
        }

        queryString += " and pumpID like :pumpID";
        queryString += " and finalStatusType like :finalStatusType";
        queryString += " and creationTimestamp >= :creationTimestampStart";
        queryString += " and creationTimestamp <= :creationTimestampEnd";

        Query query = em.createQuery(queryString);

        if (userBean != null) {
            query.setParameter("userBean", userBean);
        }

        if (stationBean != null) {
            query.setParameter("stationBean", stationBean);
        }

        if (GFGNotification != null) {
            query.setParameter("GFGNotification", GFGNotification);
        }

        if (confirmed != null) {
            query.setParameter("confirmed", confirmed);
        }

        query.setParameter("pumpID", pumpId);
        query.setParameter("finalStatusType", finalStatusType);
        query.setParameter("creationTimestampStart", creationTimestampStart);
        query.setParameter("creationTimestampEnd", creationTimestampEnd);

        // query.setFirstResult(100);
        query.setMaxResults(maxResults);

        transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionHistoryBean> searchTransactionsHistory(EntityManager em, Long userId, String stationId, String pumpId, String finalStatusType,
            Boolean GFGNotification, Boolean confirmed, Date creationTimestampStart, Date creationTimestampEnd, String productID, Integer maxResults) {

        UserBean userBean = null;
        StationBean stationBean = null;

        List<TransactionHistoryBean> transactionHistoryBeanList = new ArrayList<TransactionHistoryBean>(0);

        String queryString = "select t from TransactionHistoryBean t where 1=1";

        if (userId != null) {
            userBean = QueryRepository.findUserById(em, userId);
            if (userBean != null) {
                queryString += " and userBean = :userBean";
            }
            else {
                return transactionHistoryBeanList;
            }
        }

        if (stationId != null) {
            stationBean = QueryRepository.findStationBeanById(em, stationId);
            if (stationBean != null) {
                queryString += " and stationBean = :stationBean";
            }
            else {
                return transactionHistoryBeanList;
            }
        }

        if (GFGNotification != null) {
            queryString += " and GFGNotification = :GFGNotification";
        }

        if (confirmed != null) {
            queryString += " and confirmed = :confirmed";
        }

        queryString += " and pumpID like :pumpID";
        queryString += " and finalStatusType like :finalStatusType";
        queryString += " and creationTimestamp >= :creationTimestampStart";
        queryString += " and creationTimestamp <= :creationTimestampEnd";

        Query query = em.createQuery(queryString);

        if (userBean != null) {
            query.setParameter("userBean", userBean);
        }

        if (stationBean != null) {
            query.setParameter("stationBean", stationBean);
        }

        if (GFGNotification != null) {
            query.setParameter("GFGNotification", GFGNotification);
        }

        if (confirmed != null) {
            query.setParameter("confirmed", confirmed);
        }

        query.setParameter("pumpID", pumpId);
        query.setParameter("finalStatusType", finalStatusType);
        query.setParameter("creationTimestampStart", creationTimestampStart);
        query.setParameter("creationTimestampEnd", creationTimestampEnd);

        // query.setFirstResult(100);
        query.setMaxResults(maxResults);

        transactionHistoryBeanList = query.getResultList();

        return transactionHistoryBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> searchPoPTransactions(EntityManager em, Long userId, String stationId, String sourceId, String mpTransactionStatus,
            Boolean notificationCreated, Boolean notificationPaid, Boolean notificationUser, Date creationTimestampStart, Date creationTimestampEnd, Integer maxResults) {

        UserBean userBean = null;
        StationBean stationBean = null;

        List<PostPaidTransactionBean> postPaidTransactionBeanList = new ArrayList<PostPaidTransactionBean>(0);

        String queryString = "select t from PostPaidTransactionBean t where 1=1";

        if (userId != null) {
            userBean = QueryRepository.findUserById(em, userId);
            if (userBean != null) {
                queryString += " and userBean = :userBean";
            }
            else {
                return postPaidTransactionBeanList;
            }
        }

        if (stationId != null) {
            stationBean = QueryRepository.findStationBeanById(em, stationId);
            if (stationBean != null) {
                queryString += " and stationBean = :stationBean";
            }
            else {
                return postPaidTransactionBeanList;
            }
        }

        if (notificationCreated != null) {
            queryString += " and notificationCreated = :notificationCreated";
        }

        if (notificationPaid != null) {
            queryString += " and notificationPaid = :notificationPaid";
        }

        if (notificationUser != null) {
            queryString += " and notificationUser = :notificationUser";
        }

        if (mpTransactionStatus != null) {
            queryString += " and mpTransactionStatus like :mpTransactionStatus";
        }

        queryString += " and sourceID like :sourceID";
        queryString += " and creationTimestamp >= :creationTimestampStart";
        queryString += " and creationTimestamp <= :creationTimestampEnd";

        Query query = em.createQuery(queryString);

        if (userBean != null) {
            query.setParameter("userBean", userBean);
        }

        if (stationBean != null) {
            query.setParameter("stationBean", stationBean);
        }

        if (notificationCreated != null) {
            query.setParameter("notificationCreated", notificationCreated);
        }

        if (notificationPaid != null) {
            query.setParameter("notificationPaid", notificationPaid);
        }

        if (notificationUser != null) {
            query.setParameter("notificationUser", notificationUser);
        }

        if (mpTransactionStatus != null) {
            query.setParameter("mpTransactionStatus", mpTransactionStatus);
        }

        query.setParameter("sourceID", sourceId);
        query.setParameter("creationTimestampStart", creationTimestampStart);
        query.setParameter("creationTimestampEnd", creationTimestampEnd);

        // query.setFirstResult(100);
        query.setMaxResults(maxResults);

        postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionHistoryBean> searchPoPTransactionsHistory(EntityManager em, Long userId, String stationId, String sourceId, String mpTransactionStatus,
            Boolean notificationCreated, Boolean notificationPaid, Boolean notificationUser, Date creationTimestampStart, Date creationTimestampEnd, Integer maxResults) {

        UserBean userBean = null;
        StationBean stationBean = null;

        List<PostPaidTransactionHistoryBean> postPaidTransactionHistoryBeanList = new ArrayList<PostPaidTransactionHistoryBean>(0);

        String queryString = "select t from PostPaidTransactionHistoryBean t where 1=1";

        if (userId != null) {
            userBean = QueryRepository.findUserById(em, userId);
            if (userBean != null) {
                queryString += " and userBean = :userBean";
            }
            else {
                return postPaidTransactionHistoryBeanList;
            }
        }

        if (stationId != null) {
            stationBean = QueryRepository.findStationBeanById(em, stationId);
            if (stationBean != null) {
                queryString += " and stationBean = :stationBean";
            }
            else {
                return postPaidTransactionHistoryBeanList;
            }
        }

        if (notificationCreated != null) {
            queryString += " and notificationCreated = :notificationCreated";
        }

        if (notificationPaid != null) {
            queryString += " and notificationPaid = :notificationPaid";
        }

        if (notificationUser != null) {
            queryString += " and notificationUser = :notificationUser";
        }

        queryString += " and sourceID like :sourceID";
        queryString += " and mpTransactionStatus like :mpTransactionStatus";
        queryString += " and creationTimestamp >= :creationTimestampStart";
        queryString += " and creationTimestamp <= :creationTimestampEnd";

        Query query = em.createQuery(queryString);

        if (userBean != null) {
            query.setParameter("userBean", userBean);
        }

        if (stationBean != null) {
            query.setParameter("stationBean", stationBean);
        }

        if (notificationCreated != null) {
            query.setParameter("notificationCreated", notificationCreated);
        }

        if (notificationPaid != null) {
            query.setParameter("notificationPaid", notificationPaid);
        }

        if (notificationUser != null) {
            query.setParameter("notificationUser", notificationUser);
        }

        query.setParameter("sourceID", sourceId);
        query.setParameter("mpTransactionStatus", mpTransactionStatus);
        query.setParameter("creationTimestampStart", creationTimestampStart);
        query.setParameter("creationTimestampEnd", creationTimestampEnd);

        // query.setFirstResult(100);
        query.setMaxResults(maxResults);

        postPaidTransactionHistoryBeanList = query.getResultList();

        return postPaidTransactionHistoryBeanList;

    }

    @SuppressWarnings("unchecked")
    public static PaymentInfoBean findPaymentMethodByIdAndType(EntityManager em, Long id, String type) {

        PaymentInfoBean paymentInfoBean = null;

        Query query = em.createNamedQuery(PaymentInfoBean.FIND_BY_ID_AND_TYPE);

        query.setParameter("id", id);
        query.setParameter("type", type);
        query.setMaxResults(1);

        List<PaymentInfoBean> paymentInfoBeanList = query.getResultList();

        if (paymentInfoBeanList != null && paymentInfoBeanList.size() > 0) {
            paymentInfoBean = (PaymentInfoBean) paymentInfoBeanList.get(0);
        }
        else {
            paymentInfoBean = null;
        }

        return paymentInfoBean;

    }

    @SuppressWarnings("unchecked")
    public static List<UserBean> findUserByCreationDate(EntityManager em, Date creationStart, Date creationEnd) {

        Query query = em.createNamedQuery(UserBean.FIND_BY_CREATION_DATE);

        query.setParameter("creationStart", creationStart);
        query.setParameter("creationEnd", creationEnd);

        List<UserBean> userBeanList = query.getResultList();

        return userBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<UserBean> findUserNotVerifiedByCreationDate(EntityManager em, Date creationStart, Date creationEnd) {

        Query query = em.createNamedQuery(UserBean.FIND_NEW_BY_CREATION_DATE);

        query.setParameter("creationStart", creationStart);
        query.setParameter("creationEnd", creationEnd);

        List<UserBean> userBeanList = query.getResultList();

        return userBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionByUser(EntityManager em, UserBean user) {

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_USER);
        query.setParameter("userBean", user);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<TransactionHistoryBean> findTransactionHistoryByUser(EntityManager em, UserBean user) {

        Query query = em.createNamedQuery(TransactionHistoryBean.FIND_BY_USER);
        query.setParameter("userBean", user);

        List<TransactionHistoryBean> transactionHistoryBeanList = query.getResultList();

        return transactionHistoryBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<StationBean> searchStationBean(EntityManager em, Station station) {

        List<StationBean> stationBeanList = new ArrayList<StationBean>(0);

        String queryString = "select t from StationBean t where 1=1";

        if (station.getStationID() != null) {
            queryString += " and stationID = :stationid";
        }

        if (station.getOilAcquirerID() != null) {
            queryString += " and oilAcquirerID = :oilAcquirerID";
        }

        if (station.getOilShopLogin() != null) {
            queryString += " and oilShopLogin = :oilShopLogin";
        }

        if (station.getNoOilAcquirerID() != null) {
            queryString += " and noOilAcquirerID = :noOilAcquirerID";
        }

        if (station.getNoOilShopLogin() != null) {
            queryString += " and noOilShopLogin = :noOilShopLogin";
        }

        if (station.getStationStatus() != null) {
            queryString += " and stationStatus = :stationStatus";
        }

        Query query = em.createQuery(queryString);

        if (station.getStationID() != null) {
            query.setParameter("stationid", station.getStationID());
        }

        if (station.getOilAcquirerID() != null) {
            query.setParameter("oilAcquirerID", station.getOilAcquirerID());
        }

        if (station.getOilShopLogin() != null) {
            query.setParameter("oilShopLogin", station.getOilShopLogin());
        }

        if (station.getNoOilAcquirerID() != null) {
            query.setParameter("noOilAcquirerID", station.getNoOilAcquirerID());
        }

        if (station.getNoOilShopLogin() != null) {
            query.setParameter("noOilShopLogin", station.getNoOilShopLogin());
        }

        if (station.getStationStatus() != null) {
            query.setParameter("stationStatus", station.getStationStatus());
        }

        stationBeanList = query.getResultList();

        return stationBeanList;

    }

    @SuppressWarnings("unchecked")
    public static ParameterBean findSingleParameterBeanByName(EntityManager em, String param) {

        String queryString = "select t from ParameterBean t where name = :name";

        Query query = em.createQuery(queryString);
        query.setParameter("name", param);

        List<ParameterBean> parameterBeanList = query.getResultList();
        if (parameterBeanList != null && parameterBeanList.size() > 0) {
            return (ParameterBean) parameterBeanList.get(0);
        }
        else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static List<ParameterBean> findParameterBeanByName(EntityManager em, String param) {

        List<ParameterBean> parameterBeanList = new ArrayList<ParameterBean>(0);

        String queryString = "select t from ParameterBean t where 1=1";

        if (param != null) {
            queryString += " and name like :name";
        }

        Query query = em.createQuery(queryString);

        if (param != null) {
            // query.setParameter( "name", "%"+param+"%");
            query.setParameter("name", param);
        }

        parameterBeanList = query.getResultList();

        return parameterBeanList;
    }

    @SuppressWarnings("unchecked")
    public static DocumentBean findDocumentByKey(EntityManager em, String docId) {

        DocumentBean documentBean;
        Query query = em.createNamedQuery(DocumentBean.FIND_BY_DOCUMENTKEY);
        query.setParameter("docukey", docId);
        query.setMaxResults(1);

        List<DocumentBean> documentBeanList = query.getResultList();

        if (documentBeanList != null && documentBeanList.size() > 0) {
            documentBean = (DocumentBean) documentBeanList.get(0);
        }
        else {
            documentBean = null;
        }

        return documentBean;

    }

    @SuppressWarnings("unchecked")
    public static List<DocumentBean> findDocumentAll(EntityManager em) {

        Query query = em.createNamedQuery(DocumentBean.FIND_BY_DOCUMENT_ALL);
        List<DocumentBean> documentBeanList = query.getResultList();

        return documentBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<DocumentBean> findDocumentByGroupCategory(EntityManager em, String groupCategory) {

        Query query = em.createNamedQuery(DocumentBean.FIND_BY_DOCUMENT_BY_GROUP);
        query.setParameter("groupCategory", groupCategory);
        List<DocumentBean> documentBeanList = query.getResultList();

        return documentBeanList;

    }

    @SuppressWarnings("unchecked")
    public static StationBean findStationBeanByBeaconCode(EntityManager em, String beaconCode) {

        StationBean stationBean;

        Query query = em.createNamedQuery(StationBean.FIND_BY_BEACONCODE);

        query.setParameter("beaconCode", beaconCode);
        query.setMaxResults(1);

        List<StationBean> stationBeanList = query.getResultList();

        if (stationBeanList != null && stationBeanList.size() > 0) {
            stationBean = (StationBean) stationBeanList.get(0);
        }
        else {
            stationBean = null;
        }

        return stationBean;

    }

    @SuppressWarnings("unchecked")
    public static StationBean findActiveStationBeanByBeaconCode(EntityManager em, String beaconCode) {

        StationBean stationBean;

        Query query = em.createNamedQuery(StationBean.FIND_ACTIVE_BY_BEACONCODE);

        query.setParameter("beaconCode", beaconCode);
        query.setMaxResults(1);

        List<StationBean> stationBeanList = query.getResultList();

        if (stationBeanList != null && stationBeanList.size() > 0) {
            stationBean = (StationBean) stationBeanList.get(0);
        }
        else {
            stationBean = null;
        }

        return stationBean;

    }

    @SuppressWarnings("unchecked")
    public static ManagerBean findManagerByUsername(EntityManager em, String username) {

        ManagerBean managerBean = null;

        Query query = em.createNamedQuery(ManagerBean.FIND_BY_USERNAME);

        query.setParameter("username", username);
        query.setMaxResults(1);

        List<ManagerBean> row = query.getResultList();

        if (row != null && row.size() > 0) {
            managerBean = row.get(0);
        }
        else {
            managerBean = null;
        }

        return managerBean;

    }

    @SuppressWarnings("unchecked")
    public static List<ManagerTicketBean> findManagerTicketByManagerAndCheckDate(EntityManager em, ManagerBean managerBean, Date date) {

        Query query = em.createNamedQuery(ManagerTicketBean.FIND_BY_MANAGERID);
        query.setParameter("managerid", managerBean);
        query.setParameter("checkDate", date);
        List<ManagerTicketBean> ticketBeanList = query.getResultList();

        return ticketBeanList;

    }

    @SuppressWarnings("unchecked")
    public static ManagerTicketBean findManagerTicketById(EntityManager em, String ticketId) {

        ManagerTicketBean ticketBean = null;

        Query query = em.createNamedQuery(ManagerTicketBean.FIND_BY_TICKETID);

        query.setParameter("ticketId", ticketId);
        query.setMaxResults(1);

        List<ManagerTicketBean> ticketBeanList = query.getResultList();

        if (ticketBeanList != null && ticketBeanList.size() > 0) {
            ticketBean = (ManagerTicketBean) ticketBeanList.get(0);
        }
        else {
            ticketBean = null;
        }

        return ticketBean;

    }

    @SuppressWarnings("unchecked")
    public static List<UserBean> findUserByStatus(EntityManager em, Integer userstatus) {

        Query query = em.createNamedQuery(UserBean.FIND_BY_STATUS);

        query.setParameter("userstatus", userstatus);

        List<UserBean> userBeanList = query.getResultList();

        return userBeanList;

    }

    @SuppressWarnings("unchecked")
    public static ManagerBean findManagerById(EntityManager em, Long managerId) {

        ManagerBean managerBean = null;

        Query query = em.createNamedQuery(ManagerBean.FIND_BY_ID);

        query.setParameter("managerId", managerId);
        query.setMaxResults(1);

        List<ManagerBean> row = query.getResultList();

        if (row != null && row.size() > 0) {
            managerBean = row.get(0);
        }
        else {
            managerBean = null;
        }

        return managerBean;

    }

    @SuppressWarnings("unchecked")
    public static ManagerBean findManagerByUsernameOrEmail(EntityManager em, String username, String email) {

        ManagerBean managerBean = null;

        Query query = em.createNamedQuery(ManagerBean.FIND_BY_USERNAME_OR_EMAIL);

        query.setParameter("username", username);
        query.setParameter("email", email);
        query.setMaxResults(1);

        List<ManagerBean> row = query.getResultList();

        if (row != null && row.size() > 0) {
            managerBean = row.get(0);
        }
        else {
            managerBean = null;
        }

        return managerBean;
    }

    @SuppressWarnings("unchecked")
    public static List<ManagerBean> searchManager(EntityManager em, Long id, String username, String email, Integer maxResults) {

        Query query;
        List<ManagerBean> managerBeanList;

        if (id == null && username == null && email == null) {
            query = em.createNamedQuery(ManagerBean.GET_ALL);
        }
        else {
            if (id != null && id.longValue() > 0) {
                query = em.createNamedQuery(ManagerBean.FIND_BY_ID);
                query.setParameter("managerId", id);
            }
            else {
                if (username != null && !username.equals("")) {
                    if (email != null && !email.equals("")) {
                        query = em.createNamedQuery(ManagerBean.FIND_BY_LIKE_USERNAME_OR_LIKE_EMAIL);
                        query.setParameter("username", "%" + username + "%");
                        query.setParameter("email", "%" + email + "%");
                    }
                    else {
                        query = em.createNamedQuery(ManagerBean.FIND_BY_LIKE_USERNAME);
                        query.setParameter("username", "%" + username + "%");
                    }
                }
                else {
                    if (email != null && !email.equals("")) {
                        query = em.createNamedQuery(ManagerBean.FIND_BY_LIKE_EMAIL);
                        query.setParameter("email", "%" + email + "%");
                    }
                    else {
                        managerBeanList = new ArrayList<ManagerBean>(0);
                        return managerBeanList;
                    }
                }
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        managerBeanList = query.getResultList();

        return managerBeanList;

    }

    @SuppressWarnings("unchecked")
    public static StationBean findStationBeanByPrimaryKey(EntityManager em, Long stationId) {

        StationBean stationBean = null;

        Query query = em.createNamedQuery(StationBean.FIND_BY_PRIMARY_KEY);

        query.setParameter("id", stationId);
        query.setMaxResults(1);

        List<StationBean> stationBeanList = query.getResultList();
        if (stationBeanList != null && stationBeanList.size() > 0) {
            stationBean = (StationBean) stationBeanList.get(0);
        }
        else {
            stationBean = null;
        }

        return stationBean;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findLastPostPaidTransactionBySourceId(EntityManager em, String sourceId, Date startDate, Date endDate, Integer maxResults) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_LAST_BY_SOURCE_ID);

        query.setParameter("sourceID", sourceId);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionHistoryBean> findLastPostPaidTransactionHistoryBySourceId(EntityManager em, String sourceId, Date startDate, Date endDate,
            Integer maxResults) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.FIND_LAST_BY_SOURCE_ID);

        query.setParameter("sourceID", sourceId);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        List<PostPaidTransactionHistoryBean> postPaidTransactionBeanHistoryList = query.getResultList();

        return postPaidTransactionBeanHistoryList;
    }

    @SuppressWarnings("unchecked")
    public static List<String> findDistinctPostPaidTransactionSourceIdByStationId(EntityManager em, StationBean stationBean) {
        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_DISTINCT_SOURCE_ID_BY_STATION_ID);

        query.setParameter("stationBean", stationBean);

        List<String> sourceIdList = query.getResultList();

        return sourceIdList;
    }

    @SuppressWarnings("unchecked")
    public static List<String> findDistinctPostPaidTransactionHistorySourceIdByStationId(EntityManager em, StationBean stationBean) {
        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.FIND_DISTINCT_SOURCE_ID_BY_STATION_ID);

        query.setParameter("stationBean", stationBean);

        List<String> sourceIdList = query.getResultList();

        return sourceIdList;
    }

    @SuppressWarnings("unchecked")
    public static VoucherBean findVoucherById(EntityManager em, Long id) {

        VoucherBean voucherBean = null;

        Query query = em.createNamedQuery(VoucherBean.FIND_BY_ID);

        query.setParameter("id", id);
        query.setMaxResults(1);

        List<VoucherBean> voucherBeanList = query.getResultList();
        if (voucherBeanList != null && voucherBeanList.size() > 0) {
            voucherBean = (VoucherBean) voucherBeanList.get(0);
        }
        else {
            voucherBean = null;
        }

        return voucherBean;

    }

    @SuppressWarnings("unchecked")
    public static VoucherBean findVoucherByCode(EntityManager em, String voucherCode) {

        VoucherBean voucherBean = null;

        Query query = em.createNamedQuery(VoucherBean.FIND_BY_CODE);

        query.setParameter("code", voucherCode);
        query.setMaxResults(1);

        List<VoucherBean> voucherBeanList = query.getResultList();
        if (voucherBeanList != null && voucherBeanList.size() > 0) {
            voucherBean = (VoucherBean) voucherBeanList.get(0);
        }
        else {
            voucherBean = null;
        }

        return voucherBean;

    }

    @SuppressWarnings("unchecked")
    public static List<VoucherBean> findActiveVoucherByUserBean(EntityManager em, UserBean userBean) {

        Query query = em.createNamedQuery(VoucherBean.FIND_ACTIVE_BY_USER);

        query.setParameter("userBean", userBean);

        List<VoucherBean> voucherBeanList = query.getResultList();

        return voucherBeanList;

    }

    @SuppressWarnings("unchecked")
    public static LoyaltyCardMatcherBean findLoyaltyCardMatcherById(EntityManager em, Long id) {

        LoyaltyCardMatcherBean loyaltyCardMatcherBean = null;

        Query query = em.createNamedQuery(LoyaltyCardMatcherBean.FIND_BY_ID);

        query.setParameter("id", id);
        query.setMaxResults(1);

        List<LoyaltyCardMatcherBean> loyaltyCardMatcherBeanList = query.getResultList();
        if (loyaltyCardMatcherBeanList != null && loyaltyCardMatcherBeanList.size() > 0) {
            loyaltyCardMatcherBean = (LoyaltyCardMatcherBean) loyaltyCardMatcherBeanList.get(0);
        }
        else {
            loyaltyCardMatcherBean = null;
        }

        return loyaltyCardMatcherBean;

    }

    @SuppressWarnings("unchecked")
    public static List<LoyaltyCardMatcherBean> findLoyaltyCardMatcherByOperationID(EntityManager em, String operationID) {

        Query query = em.createNamedQuery(LoyaltyCardMatcherBean.FIND_BY_OPERATION_ID);
        query.setParameter("operationID", operationID);

        List<LoyaltyCardMatcherBean> loyaltyCardMatcherBeanList = query.getResultList();

        return loyaltyCardMatcherBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidConsumeVoucherBean> findConsumedPostPaidConsumeVoucherBeanByStatusCodeAndTimestamp(EntityManager em, String statusCode, Long startTimestamp,
            Long endTimestamp) {

        Query query = em.createNamedQuery(PostPaidConsumeVoucherBean.FIND_CONSUMED_BY_STATUS_CODE_AND_TIMESTAMP);
        query.setParameter("statusCode", statusCode);
        query.setParameter("startTimestamp", startTimestamp);
        query.setParameter("endTimestamp", endTimestamp);

        List<PostPaidConsumeVoucherBean> postPaidConsumeVoucherBeanList = query.getResultList();

        return postPaidConsumeVoucherBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidConsumeVoucherBean> findPostPaidConsumeVoucherBeanReconciliation(EntityManager em) {

        Query query = em.createNamedQuery(PostPaidConsumeVoucherBean.FIND_BY_STATUS_CODE);
        query.setParameter("statusCode", StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);

        List<PostPaidConsumeVoucherBean> postPaidConsumeVoucherBeanList = query.getResultList();

        return postPaidConsumeVoucherBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidLoadLoyaltyCreditsBean> findPostPaidLoadLoyaltyCreditsBeanReconciliation(EntityManager em) {

        Query query = em.createNamedQuery(PostPaidLoadLoyaltyCreditsBean.FIND_BY_STATUS_CODE);
        query.setParameter("statusCode", StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED);

        List<PostPaidLoadLoyaltyCreditsBean> postPaidLoadLoyaltyCreditsBeanList = query.getResultList();

        return postPaidLoadLoyaltyCreditsBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionEventBean> findTransactionEventByTransaction(EntityManager em, TransactionBean transactionBean) {

        Query query = em.createNamedQuery(TransactionEventBean.FIND_BY_TRANSACTION);
        query.setParameter("transactionBean", transactionBean);

        List<TransactionEventBean> transactionEventBeanList = query.getResultList();

        return transactionEventBeanList;
    }

    @SuppressWarnings("unchecked")
    public static PaymentInfoBean findPaymentMethodTokenExists(EntityManager em, String token) {

        Query query = em.createNamedQuery(PaymentInfoBean.FIND_PAYMENT_TOKEN_EXISTS);
        query.setParameter("token", token);
        query.setMaxResults(1);

        List<PaymentInfoBean> paymentInfoBeanList = query.getResultList();

        if (paymentInfoBeanList != null && paymentInfoBeanList.size() > 0)
            return paymentInfoBeanList.get(0);

        return null;
    }

    @SuppressWarnings("unchecked")
    public static PaymentInfoBean findPaymentMethodHashExists(EntityManager em, String hash) {

        Query query = em.createNamedQuery(PaymentInfoBean.FIND_PAYMENT_HASH_EXISTS);
        query.setParameter("hash", hash);
        query.setMaxResults(1);

        List<PaymentInfoBean> paymentInfoBeanList = query.getResultList();

        if (paymentInfoBeanList != null && paymentInfoBeanList.size() > 0)
            return paymentInfoBeanList.get(0);

        return null;
    }

    @SuppressWarnings("unchecked")
    public static boolean findCardBinExists(EntityManager em, String number) {

        Query query = em.createNamedQuery(CardBinBean.FIND_BY_NUMBER);
        query.setParameter("number", number);

        List<CardBinBean> cardBinBeanList = query.getResultList();

        if (cardBinBeanList != null && cardBinBeanList.size() > 0)
            return true;

        return false;
    }

    @SuppressWarnings("unchecked")
    public static PromotionBean findPromotionByCode(EntityManager em, String promotionCode) {

        Query query = em.createNamedQuery(PromotionBean.FIND_PROMOTION_BY_CODE);
        query.setParameter("code", promotionCode);

        Date curDate = new Date();
        query.setParameter("date", curDate);
        query.setParameter("status", PromotionStatus.ACTIVE.getValue());

        query.setMaxResults(1);

        List<PromotionBean> promotionBeanList = query.getResultList();
        if (promotionBeanList != null && !promotionBeanList.isEmpty()) {

            return promotionBeanList.get(0);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static PromotionBean findSinglePromotionByCode(EntityManager em, String promotionCode) {

        Query query = em.createNamedQuery(PromotionBean.FIND_SINGLE_PROMOTION_BY_CODE);
        query.setParameter("code", promotionCode);

        query.setMaxResults(1);

        List<PromotionBean> promotionBeanList = query.getResultList();
        if (promotionBeanList != null && !promotionBeanList.isEmpty()) {

            return promotionBeanList.get(0);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<PromotionBean> findPromotions(EntityManager em) {

        Query query = em.createNamedQuery(PromotionBean.FIND_ALL);

        List<PromotionBean> promotionBeanList = query.getResultList();
        if (promotionBeanList != null && !promotionBeanList.isEmpty()) {

            return promotionBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static PromoVoucherBean findPromoVoucherByCode(EntityManager em, String voucherCode) {

        Query query = em.createNamedQuery(PromoVoucherBean.FIND_BY_VOUCHER_CODE);
        query.setParameter("voucherCode", voucherCode);

        query.setMaxResults(1);

        List<PromoVoucherBean> promoVoucherBeanList = query.getResultList();
        if (promoVoucherBeanList != null && !promoVoucherBeanList.isEmpty()) {

            return promoVoucherBeanList.get(0);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<PromoVoucherBean> findPromoVoucherByUser(EntityManager em, UserBean user) {

        Query query = em.createNamedQuery(PromoVoucherBean.FIND_BY_USER);
        query.setParameter("user", user);

        List<PromoVoucherBean> promoVoucherList = query.getResultList();
        if (promoVoucherList != null && !promoVoucherList.isEmpty()) {

            return promoVoucherList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<PromoVoucherBean> findPromoVoucherNewByPromotion(EntityManager em, PromotionBean promotionBean) {

        Query query = em.createNamedQuery(PromoVoucherBean.FIND_NEW_BY_PROMOTION);
        query.setParameter("promotionBean", promotionBean);

        List<PromoVoucherBean> promoVoucherList = query.getResultList();
        if (promoVoucherList != null && !promoVoucherList.isEmpty()) {

            return promoVoucherList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<PromoVoucherBean> findPromoVoucherByDateAndStatusConsumed(EntityManager em, Date insertedTimestamp) {

        Query query = em.createNamedQuery(PromoVoucherBean.FIND_BY_DATE_AND_STATUS_CONSUMED);
        query.setParameter("insertedTimestamp", insertedTimestamp);

        List<PromoVoucherBean> promoVoucherList = query.getResultList();
        if (promoVoucherList != null && !promoVoucherList.isEmpty()) {

            return promoVoucherList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<PromoVoucherBean> findPromoVoucherByStatusConsumed(EntityManager em) {

        Query query = em.createNamedQuery(PromoVoucherBean.FIND_BY_STATUS_CONSUMED);

        List<PromoVoucherBean> promoVoucherList = query.getResultList();
        if (promoVoucherList != null && !promoVoucherList.isEmpty()) {

            return promoVoucherList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static PromoVoucherBean findPromoVoucherByUserAndVerificationValue(EntityManager em, PromotionBean promotionBean, UserBean userBean, String verificationValue) {

        Query query = em.createNamedQuery(PromoVoucherBean.FIND_BY_USER_AND_VERIFICATION_VALUE);
        query.setParameter("promotionBean", promotionBean);
        query.setParameter("userBean", userBean);
        query.setParameter("verificationValue", verificationValue);

        query.setMaxResults(1);

        List<PromoVoucherBean> promoVoucherList = query.getResultList();
        if (promoVoucherList != null && !promoVoucherList.isEmpty()) {

            return promoVoucherList.get(0);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static PromoVoucherBean findPromoVoucherByUserAndPromotion(EntityManager em, PromotionBean promotionBean, UserBean userBean) {

        Query query = em.createNamedQuery(PromoVoucherBean.FIND_BY_USER_AND_PROMOTION);
        query.setParameter("promotionBean", promotionBean);
        query.setParameter("userBean", userBean);

        query.setMaxResults(1);

        List<PromoVoucherBean> promoVoucherList = query.getResultList();
        if (promoVoucherList != null && !promoVoucherList.isEmpty()) {

            return promoVoucherList.get(0);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<PromoVoucherBean> findPromoVoucherNotAssigned(EntityManager em) {

        Query query = em.createNamedQuery(PromoVoucherBean.FIND_VOUCHER_NOT_ASSIGNED);

        List<PromoVoucherBean> promoVoucherBeanList = query.getResultList();
        if (promoVoucherBeanList != null && !promoVoucherBeanList.isEmpty()) {

            return promoVoucherBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static PromotionBean findPromotionByVoucherCode(EntityManager em, String voucherCode) {

        Query query = em.createNamedQuery(PromoVoucherBean.FIND_PROMOTION_BY_VOUCHER_CODE);
        query.setParameter("code", voucherCode);

        List<PromotionBean> promotionBeanList = query.getResultList();
        if (promotionBeanList != null && !promotionBeanList.isEmpty()) {

            return promotionBeanList.get(0);
        }

        return null;

    }

    @SuppressWarnings("unchecked")
    public static List<PromoUsersOnHoldBean> findPromoUsersOnHold(EntityManager em) {

        Query query = em.createNamedQuery(PromoUsersOnHoldBean.FIND_USERS_ON_HOLD);

        List<PromoUsersOnHoldBean> promoUserOnHoldBeanList = query.getResultList();
        if (promoUserOnHoldBeanList != null && !promoUserOnHoldBeanList.isEmpty()) {

            return promoUserOnHoldBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<PromoUsersOnHoldBean> findPromoUsersByPromotion(EntityManager em, PromotionBean promotionBean) {

        Query query = em.createNamedQuery(PromoUsersOnHoldBean.FIND_BY_PROMOTION);
        query.setParameter("promotionBean", promotionBean);

        List<PromoUsersOnHoldBean> promoUserOnHoldBeanList = query.getResultList();
        if (promoUserOnHoldBeanList != null && !promoUserOnHoldBeanList.isEmpty()) {

            return promoUserOnHoldBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactions(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPrePaidTransactions(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_ALL_SYNTHESIS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsArea(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_AREA);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPrePaidTransactionsArea(EntityManager em, Date dailyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_AREA);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPrePaidTransactionsHistory(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsHistoryArea(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_AREA);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPrePaidTransactionsHistoryArea(EntityManager em, Date dailyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_AREA);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsProvince(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, String stationProvince) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_PROVINCE);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("stationProvince", stationProvince);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsHistoryProvince(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, String stationProvince) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_PROVINCE);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("stationProvince", stationProvince);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactions(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidTransactions(EntityManager em, Date dailyEndDate, Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsArea(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String source, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_AREA);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("source", source);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidTransactionsArea(EntityManager em, Date dailyEndDate, Date totalStartDate, String source, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_AREA);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("source", source);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidTransactionsHistory(EntityManager em, Date dailyEndDate, Date totalStartDate, 
    		String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsHistoryArea(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String source, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_AREA);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("source", source);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidTransactionsHistoryArea(EntityManager em, Date dailyEndDate, Date totalStartDate, String source, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_AREA);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("source", source);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsProvince(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, String source, String stationProvince) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_PROVINCE);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("source", source);
        query.setParameter("stationProvince", stationProvince);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsHistoryProvince(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, String source, String stationProvince) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_PROVINCE);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("source", source);
        query.setParameter("stationProvince", stationProvince);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisParkingTransactions(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(ParkingTransactionBean.STATISTICS_REPORT_SYNTHESIS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllParkingTransactions(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(ParkingTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    /*
     * public static List<Object> statisticsReportSynthesisUsers(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate) {
     * 
     * Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS);
     * query.setParameter("dailyStartDate", dailyStartDate);
     * query.setParameter("dailyEndDate", dailyEndDate);
     * query.setParameter("weeklyStartDate", weeklyStartDate);
     * query.setParameter("weeklyEndDate", weeklyEndDate);
     * 
     * query.setMaxResults(1);
     * 
     * Object resultList = query.getResultList();
     * 
     * if (resultList != null) {
     * return Arrays.asList((Object[]) resultList);
     * }
     * 
     * return null;
     * }
     */
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisUsersActive(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ACTIVE);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllUsersActive(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisUsersActiveBySource(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate, String source) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ACTIVE_BY_SOURCE);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("source", source);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllUsersActiveBySource(EntityManager em, Date dailyEndDate, Date totalStartDate, String source) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE_BY_SOURCE);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("source", source);

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisSocialUsersActive(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate, String socialProvider) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_SOCIAL_ACTIVE);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("socialProvider", socialProvider);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllSocialUsersActive(EntityManager em, Date dailyEndDate, Date totalStartDate, String socialProvider) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ALL_SOCIAL_ACTIVE);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("socialProvider", socialProvider);

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisGuestUsersActive(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ACTIVE_GUEST);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllGuestUsersActive(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE_GUEST);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisUsersLoyaltyCard(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ACTIVE_LOYALTYCARD);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllUsersLoyaltyCard(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE_LOYALTYCARD);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);


        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisUsersLoyaltyCardVirtual(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(LoyaltyCardBean.FIND_VIRTUAL);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllUsersLoyaltyCardVirtual(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(LoyaltyCardBean.FIND_ALL_VIRTUAL);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisUsersLoyaltyCardNoVirtual(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(LoyaltyCardBean.FIND_NO_VIRTUAL);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisUsersMastercard(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ACTIVE_MASTERCARD);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisUsersCreditCard(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ACTIVE_CREDIT_CARD);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllUsersCreditCard(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE_CREDIT_CARD);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisUsersNoCreditCard(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_ACTIVE_NO_CREDIT_CARD);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    /*
     * public static List<Object> statisticsReportSynthesisUsersVoucher(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate) {
     * 
     * Query query = em.createNamedQuery(UserBean.STATISTICS_REPORT_SYNTHESIS_VOCUHER);
     * query.setParameter("dailyStartDate", dailyStartDate);
     * query.setParameter("dailyEndDate", dailyEndDate);
     * query.setParameter("weeklyStartDate", weeklyStartDate);
     * query.setParameter("weeklyEndDate", weeklyEndDate);
     * 
     * query.setMaxResults(1);
     * 
     * Object resultList = query.getResultList();
     * 
     * if (resultList != null) {
     * return Arrays.asList((Object[]) resultList);
     * }
     * 
     * return null;
     * }
     */
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisUsersPromoVoucher(EntityManager em, PromotionBean promotionBean, Date dailyStartDate, Date dailyEndDate,
            Date weeklyStartDate, Date weeklyEndDate) {

        Query query = em.createNamedQuery(PromoVoucherBean.STATISTICS_REPORT_SYNTHESIS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("promotionBean", promotionBean);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidVoucherConsumed(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_VOUCHER_CONSUMED);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidVoucherHistoryConsumed(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_VOUCHER_CONSUMED);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidVoucherConsumed(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_VOUCHER_CONSUMED);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidPromoVoucherConsumed(EntityManager em, PromotionBean promotionBean, Date dailyStartDate, Date dailyEndDate,
            Date weeklyStartDate, Date weeklyEndDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_PROMO_VOUCHER_CONSUMED);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("promotionBean", promotionBean);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidPromoVoucherHistoryConsumed(EntityManager em, PromotionBean promotionBean, Date dailyStartDate, Date dailyEndDate,
            Date weeklyStartDate, Date weeklyEndDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_PROMO_VOUCHER_CONSUMED);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("promotionBean", promotionBean);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidPromoVoucherConsumed(EntityManager em, PromotionBean promotionBean, Date dailyStartDate, Date dailyEndDate,
            Date weeklyStartDate, Date weeklyEndDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_PROMO_VOUCHER_CONSUMED);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("promotionBean", promotionBean);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidPromoVoucherHistoryConsumed(EntityManager em, PromotionBean promotionBean, Date dailyStartDate, Date dailyEndDate,
            Date weeklyStartDate, Date weeklyEndDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_PROMO_VOUCHER_CONSUMED);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("promotionBean", promotionBean);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalAmount(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTotalAmount(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result = (Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalAmountHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTotalAmountHistory(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result = (Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalAmount(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTotalAmount(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result = (Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalAmountHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTotalAmountHistory(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result = (Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisParkingTotalAmount(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(ParkingTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllParkingTotalAmount(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(ParkingTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result = (Double) query.getSingleResult();
        
        return result;
    }

    public static Object statisticsReportSynthesisStationsAppCount(EntityManager em) {

        Query query = em.createNamedQuery(StationBean.COUNT_ALL_APP_ACTIVE);

        query.setMaxResults(1);

        Object resultList = query.getResultList();

        /*
         * if (resultList != null) {
         * return Arrays.asList((Object[]) resultList);
         * }
         */

        return resultList;
    }
    
    public static Long statisticsReportSynthesisAllStationsAppCount(EntityManager em) {

        Query query = em.createNamedQuery(StationBean.COUNT_ALL_APP_ACTIVE);

        Long result = (Long) query.getSingleResult();

        return result;
    }

    public static Object statisticsReportSynthesisStationsLoyaltyCount(EntityManager em) {

        Query query = em.createNamedQuery(StationBean.COUNT_ALL_LOYALTY_ACTIVE);

        query.setMaxResults(1);

        Object resultList = query.getResultList();

        /*
         * if (resultList != null) {
         * return Arrays.asList((Object[]) resultList);
         * }
         */

        return resultList;
    }
    
    public static Long statisticsReportSynthesisAllStationsLoyaltyCount(EntityManager em) {

        Query query = em.createNamedQuery(StationBean.COUNT_ALL_LOYALTY_ACTIVE);

        Long result = (Long) query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> statisticsReportDetailPrePaidTransactions(EntityManager em, Date startDate, Date endDate) {

        Query query;

        if (startDate != null) {
            query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_DETAIL_BY_DATE);
            query.setParameter("startDate", startDate);
            query.setParameter("endDate", endDate);
        }
        else {
            query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_DETAIL);
            query.setParameter("endDate", endDate);
        }

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionHistoryBean> statisticsReportDetailPrePaidTransactionsHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate) {

        Query query;

        if (dailyStartDate != null) {
            query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_DETAIL_BY_DATE);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
        }
        else {
            query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_DETAIL);
            query.setParameter("dailyEndDate", dailyEndDate);
        }

        List<TransactionHistoryBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> statisticsReportDetailPostPaidTransactions(EntityManager em, Date startDate, Date endDate) {

        Query query;

        if (startDate != null) {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_DETAIL_BY_DATE);
            query.setParameter("startDate", startDate);
            query.setParameter("endDate", endDate);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_DETAIL);
            query.setParameter("endDate", endDate);
        }

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    public static List<CustomReportDTO> statisticsReportMyCiceroRefuelingTransactions(EntityManager em, Date startDate, Date endDate) {

        TypedQuery<CustomReportDTO> query;

        query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_MYCICERO_POSTPAYED_TRANSACTIONS, CustomReportDTO.class);
        query.setParameter("transactionStatus", "PAID");
        query.setParameter("refuelMode", "Servito");
        query.setParameter("source", "MYCICERO");
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<CustomReportDTO> result = query.getResultList();

        return result;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionHistoryBean> statisticsReportDetailPostPaidTransactionsHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate) {

        Query query;

        if (dailyStartDate != null) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_DETAIL_BY_DATE);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_DETAIL);
            query.setParameter("dailyEndDate", dailyEndDate);
        }

        List<PostPaidTransactionHistoryBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisDistinctUsersTransaction(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        List<Long> integerList = null;

        Long almenoUnaTransazionePrimaDiIeri = 0l, almenoUnaTransazioneFinoAIeri = 0l, almenoUnaTransazioneIeri = 0l, almenoUnaTransazionePrimaDellaSettimana = 0l, almenoUnaTransazioneNellaSettimana = 0l;

        /*
         * Query query = em.createQuery("select count(distinct u.id) from UserBean u "
         * + "where u.id in (select distinct t.userBean.id from TransactionBean t where t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
         * + "and t.finalAmount > 0 and t.stationBean.stationStatus = 2 and t.creationTimestamp <= :dailyEndDate) "
         * + "or u.id in (select distinct th.userBean.id from TransactionHistoryBean th where th.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
         * + "and th.finalAmount > 0 and th.stationBean.stationStatus = 2 and th.creationTimestamp <= :dailyEndDate) "
         * + "or u.id in (select distinct pt.userBean.id from PostPaidTransactionBean pt where pt.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
         * + "and pt.amount > 0 and pt.stationBean.stationStatus = 2 and pt.creationTimestamp <= :dailyEndDate) "
         * + "or u.id in (select distinct pth.userBean.id from PostPaidTransactionHistoryBean pth where pth.mpTransactionStatus = '"
         * + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' " + "and pth.amount > 0 and pth.stationBean.stationStatus = 2 and pth.creationTimestamp <= :dailyEndDate)");
         */
        Query query = em.createQuery("select count(distinct u.id) from UserBean u "
                + "where u.id in (select distinct t.userBean.id from TransactionBean t where (t.transactionCategory is null or t.transactionCategory = 'CUSTOMER') and "
                + "t.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL
                + "' "
                + "and t.finalAmount > 0 and (t.stationBean.stationStatus = 2 or t.stationBean.stationStatus = 1) and t.creationTimestamp <= :dailyEndDate "
                + "and t.creationTimestamp >= :totalStartDate) "
                + "or u.id in (select distinct th.userBean.id from TransactionHistoryBean th where (th.transactionCategory is null or th.transactionCategory = 'CUSTOMER') "
                + "and th.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL
                + "' "
                + "and th.finalAmount > 0 and (th.stationBean.stationStatus = 2 or th.stationBean.stationStatus = 1) and th.creationTimestamp <= :dailyEndDate "
                + "and th.creationTimestamp >= :totalStartDate) "
                + "or u.id in (select distinct pt.userBean.id from PostPaidTransactionBean pt where (pt.transactionCategory is null or pt.transactionCategory = 'CUSTOMER') "
                + "and pt.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID
                + "' "
                + "and pt.amount > 0 and (pt.stationBean.stationStatus = 2 or pt.stationBean.stationStatus = 1) and pt.creationTimestamp <= :dailyEndDate "
                + "and pt.creationTimestamp >= :totalStartDate) "
                + "or u.id in (select distinct pth.userBean.id from PostPaidTransactionHistoryBean pth where (pth.transactionCategory is null or pth.transactionCategory = 'CUSTOMER') and pth.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' " + "and pth.amount > 0 and (pth.stationBean.stationStatus = 2 or pth.stationBean.stationStatus = 1) "
                + "and pth.creationTimestamp <= :dailyEndDate and pth.creationTimestamp >= :totalStartDate)");

        // Totale utenti che hanno effettuato almeno una transazione prima della settimana
        Date endDateWeekBefore = new Date(weeklyStartDate.getTime() - 1000);
        query.setParameter("dailyEndDate", endDateWeekBefore);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnaTransazionePrimaDellaSettimana = (Long) integerList.get(0);
        }

        // Totale utenti che hanno effettuato almeno una transazione prima di ieri
        Date endDateBefore = new Date(dailyEndDate.getTime() - (24 * 60 * 60 * 1000));
        query.setParameter("dailyEndDate", endDateBefore);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnaTransazionePrimaDiIeri = (Long) integerList.get(0);
        }

        // Totale utenti che hanno effettuato almeno una transazione prima di oggi
        Date endDateAfter = dailyEndDate;
        query.setParameter("dailyEndDate", endDateAfter);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnaTransazioneFinoAIeri = (Long) integerList.get(0);
        }

        almenoUnaTransazioneIeri = almenoUnaTransazioneFinoAIeri - almenoUnaTransazionePrimaDiIeri;
        almenoUnaTransazioneNellaSettimana = almenoUnaTransazioneFinoAIeri - almenoUnaTransazionePrimaDellaSettimana;

        // Creazione dell'oggetto risposta
        List<Object> result = new ArrayList<Object>(3);
        result.add(0, almenoUnaTransazioneIeri);
        result.add(1, almenoUnaTransazioneNellaSettimana);
        result.add(2, almenoUnaTransazioneFinoAIeri);

        return result;
    }
    
    public static Long statisticsReportSynthesisAllDistinctUsersTransaction(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createQuery("select count(distinct u.id) from UserBean u "
                + "where u.id in (select distinct t.userBean.id from TransactionBean t where (t.transactionCategory is null or t.transactionCategory = 'CUSTOMER') and "
                + "t.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL
                + "' "
                + "and t.finalAmount > 0 and (t.stationBean.stationStatus = 2 or t.stationBean.stationStatus = 1) and t.creationTimestamp <= :dailyEndDate "
                + "and t.creationTimestamp >= :totalStartDate) "
                + "or u.id in (select distinct th.userBean.id from TransactionHistoryBean th where (th.transactionCategory is null or th.transactionCategory = 'CUSTOMER') "
                + "and th.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL
                + "' "
                + "and th.finalAmount > 0 and (th.stationBean.stationStatus = 2 or th.stationBean.stationStatus = 1) and th.creationTimestamp <= :dailyEndDate "
                + "and th.creationTimestamp >= :totalStartDate) "
                + "or u.id in (select distinct pt.userBean.id from PostPaidTransactionBean pt where (pt.transactionCategory is null or pt.transactionCategory = 'CUSTOMER') "
                + "and pt.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID
                + "' "
                + "and pt.amount > 0 and (pt.stationBean.stationStatus = 2 or pt.stationBean.stationStatus = 1) and pt.creationTimestamp <= :dailyEndDate "
                + "and pt.creationTimestamp >= :totalStartDate) "
                + "or u.id in (select distinct pth.userBean.id from PostPaidTransactionHistoryBean pth where (pth.transactionCategory is null or pth.transactionCategory = 'CUSTOMER') and pth.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' " + "and pth.amount > 0 and (pth.stationBean.stationStatus = 2 or pth.stationBean.stationStatus = 1) "
                + "and pth.creationTimestamp <= :dailyEndDate and pth.creationTimestamp >= :totalStartDate)");

        // Totale utenti che hanno effettuato almeno una transazione prima di oggi
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisDistinctUsersMyCicero(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        List<Long> integerList = null;

        Long almenoUnaTransazionePrimaDiIeri = 0l, almenoUnaTransazioneFinoAIeri = 0l, almenoUnaTransazioneIeri = 0l, almenoUnaTransazionePrimaDellaSettimana = 0l, almenoUnaTransazioneNellaSettimana = 0l;

        Query query = em.createQuery("select count(distinct u.id) from UserBean u " + "where u.id in (select distinct p1.userBean.id " + "from ParkingTransactionBean p1 "
                + "where p1.status = '" + StatusHelper.PARKING_STATUS_ENDED + "' " + "and p1.finalPrice > 0 " + "and p1.timestamp >= :totalStartDate "
                + "and p1.timestamp < :dailyEndDate)");

        // Totale utenti che hanno effettuato almeno una transazione prima della settimana
        Date endDateWeekBefore = new Date(weeklyStartDate.getTime() - 1000);
        query.setParameter("dailyEndDate", endDateWeekBefore);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnaTransazionePrimaDellaSettimana = (Long) integerList.get(0);
        }

        // Totale utenti che hanno effettuato almeno una transazione prima di ieri
        Date endDateBefore = new Date(dailyEndDate.getTime() - (24 * 60 * 60 * 1000));
        query.setParameter("dailyEndDate", endDateBefore);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnaTransazionePrimaDiIeri = (Long) integerList.get(0);
        }

        // Totale utenti che hanno effettuato almeno una transazione prima di oggi
        Date endDateAfter = dailyEndDate;
        query.setParameter("dailyEndDate", endDateAfter);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnaTransazioneFinoAIeri = (Long) integerList.get(0);
        }

        almenoUnaTransazioneIeri = almenoUnaTransazioneFinoAIeri - almenoUnaTransazionePrimaDiIeri;
        almenoUnaTransazioneNellaSettimana = almenoUnaTransazioneFinoAIeri - almenoUnaTransazionePrimaDellaSettimana;

        // Creazione dell'oggetto risposta
        List<Object> result = new ArrayList<Object>(3);
        result.add(0, almenoUnaTransazioneIeri);
        result.add(1, almenoUnaTransazioneNellaSettimana);
        result.add(2, almenoUnaTransazioneFinoAIeri);

        return result;
    }
    
    public static Long statisticsReportSynthesisAllDistinctUsersMyCicero(EntityManager em, Date dailyEndDate, Date totalStartDate) {


        Query query = em.createQuery("select count(distinct u.id) from UserBean u " + "where u.id in (select distinct p1.userBean.id " + "from ParkingTransactionBean p1 "
                + "where p1.status = '" + StatusHelper.PARKING_STATUS_ENDED + "' " + "and p1.finalPrice > 0 " + "and p1.timestamp >= :totalStartDate "
                + "and p1.timestamp < :dailyEndDate)");

        // Totale utenti che hanno effettuato almeno una transazione prima di oggi
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisDistinctUsersLoyalty(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        List<Long> integerList = null;

        Long almenoUnCaricamentoPrimaDiIeri = 0l, almenoUnCaricamentoFinoAIeri = 0l, almenoUnCaricamentoIeri = 0l, almenoUnCaricamentoPrimaDellaSettimana = 0l, almenoUnCaricamentoNellaSettimana = 0l;

        Query query = em.createQuery("select count(distinct u.id) from UserBean u "
                + "where u.id in (select distinct e.userBean.id from EventNotificationBean e where e.eventType = 'LOYALTY' and e.transactionResult = '00' "
                + "and e.credits > 0 and e.requestTimestamp <= :dailyEndDate and e.requestTimestamp >= :totalStartDate) "

                + "or u.id in (select distinct pt.userBean.id from PostPaidTransactionBean pt join pt.postPaidLoadLoyaltyCreditsBeanList pl " + "where pt.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and pt.amount > 0 and (pt.stationBean.stationStatus = 2 or pt.stationBean.stationStatus = 1) and pt.creationTimestamp <= :dailyEndDate "
                + "and pt.creationTimestamp >= :totalStartDate and pl.statusCode = '00' and pl.operationType = 'LOAD' and pl.credits > 0) "

                + "or u.id in (select distinct pth.userBean.id from PostPaidTransactionHistoryBean pth join pth.postPaidLoadLoyaltyCreditsHistoryBeanList plh "
                + "where pth.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and pth.amount > 0 and (pth.stationBean.stationStatus = 2 or pth.stationBean.stationStatus = 1) and pth.creationTimestamp <= :dailyEndDate "
                + "and pth.creationTimestamp >= :totalStartDate and plh.statusCode = '00' and plh.operationType = 'LOAD' and plh.credits > 0)");

        // Totale utenti che hanno effettuato almeno una transazione prima della settimana
        Date endDateWeekBefore = new Date(weeklyStartDate.getTime() - 1000);
        query.setParameter("dailyEndDate", endDateWeekBefore);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnCaricamentoPrimaDellaSettimana = (Long) integerList.get(0);
        }

        // Totale utenti che hanno effettuato almeno una transazione prima di ieri
        Date endDateBefore = new Date(dailyEndDate.getTime() - (24 * 60 * 60 * 1000));
        query.setParameter("dailyEndDate", endDateBefore);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnCaricamentoPrimaDiIeri = (Long) integerList.get(0);
        }

        // Totale utenti che hanno effettuato almeno una transazione prima di oggi
        Date endDateAfter = dailyEndDate;
        query.setParameter("dailyEndDate", endDateAfter);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            almenoUnCaricamentoFinoAIeri = (Long) integerList.get(0);
        }

        almenoUnCaricamentoIeri = almenoUnCaricamentoFinoAIeri - almenoUnCaricamentoPrimaDiIeri;
        almenoUnCaricamentoNellaSettimana = almenoUnCaricamentoFinoAIeri - almenoUnCaricamentoPrimaDellaSettimana;

        // Creazione dell'oggetto risposta
        List<Object> result = new ArrayList<Object>(3);
        result.add(0, almenoUnCaricamentoIeri);
        result.add(1, almenoUnCaricamentoNellaSettimana);
        result.add(2, almenoUnCaricamentoFinoAIeri);

        return result;
    }
    
    public static Long statisticsReportSynthesisAllDistinctUsersLoyalty(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createQuery("select count(distinct u.id) from UserBean u "
                + "where u.id in (select distinct e.userBean.id from EventNotificationBean e where e.eventType = 'LOYALTY' and e.transactionResult = '00' "
                + "and e.credits > 0 and e.requestTimestamp <= :dailyEndDate and e.requestTimestamp >= :totalStartDate) "

                + "or u.id in (select distinct pt.userBean.id from PostPaidTransactionBean pt join pt.postPaidLoadLoyaltyCreditsBeanList pl " + "where pt.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and pt.amount > 0 and (pt.stationBean.stationStatus = 2 or pt.stationBean.stationStatus = 1) and pt.creationTimestamp <= :dailyEndDate "
                + "and pt.creationTimestamp >= :totalStartDate and pl.statusCode = '00' and pl.operationType = 'LOAD' and pl.credits > 0) "

                + "or u.id in (select distinct pth.userBean.id from PostPaidTransactionHistoryBean pth join pth.postPaidLoadLoyaltyCreditsHistoryBeanList plh "
                + "where pth.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and pth.amount > 0 and (pth.stationBean.stationStatus = 2 or pth.stationBean.stationStatus = 1) and pth.creationTimestamp <= :dailyEndDate "
                + "and pth.creationTimestamp >= :totalStartDate and plh.statusCode = '00' and plh.operationType = 'LOAD' and plh.credits > 0)");


        // Totale utenti che hanno effettuato almeno una transazione prima di oggi
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        
        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static SurveyBean findSurveyByCode(EntityManager em, String code) {
        SurveyBean surveyBean = null;

        Query query = em.createNamedQuery(SurveyBean.FIND_BY_CODE);

        query.setParameter("code", code);
        query.setMaxResults(1);

        List<SurveyBean> surveyBeanList = query.getResultList();

        if (surveyBeanList != null && surveyBeanList.size() > 0) {
            surveyBean = (SurveyBean) surveyBeanList.get(0);
        }
        else {
            surveyBean = null;
        }

        return surveyBean;

    }

    @SuppressWarnings("unchecked")
    public static List<SurveyBean> findAllSurvey(EntityManager em) {

        Query query = em.createNamedQuery(SurveyBean.FIND_ALL);

        List<SurveyBean> surveyBeanList = query.getResultList();

        return surveyBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<SurveyBean> findSurvey(EntityManager em, String code, Date startSearch, Date endSearch, Integer status) {

        String sqlQuery = "select s from SurveyBean s where 1 = 1";
        boolean codeParameter = false;
        boolean startSearchParameter = false;
        boolean endSearchParameter = false;
        boolean statusParameter = false;

        if (code != null) {
            sqlQuery += " and code = :code";
            codeParameter = true;
        }

        if (startSearch != null) {
            sqlQuery += " and end_date >= :startSearch";
            startSearchParameter = true;
        }

        if (endSearch != null) {
            sqlQuery += " and start_date <= :endSearch";
            endSearchParameter = true;
        }

        if (status != null) {
            sqlQuery += " and status = :status";
            statusParameter = true;
        }

        Query query = em.createQuery(sqlQuery);

        if (codeParameter) {
            query.setParameter("code", code);
        }

        if (startSearchParameter) {
            query.setParameter("startSearch", startSearch);
        }

        if (endSearchParameter) {
            query.setParameter("endSearch", endSearch);
        }

        if (statusParameter) {
            query.setParameter("status", status);
        }

        List<SurveyBean> surveyBeanList = query.getResultList();

        return surveyBeanList;

    }

    @SuppressWarnings("unchecked")
    public static SurveyQuestionBean findSurveyQuestionByCode(EntityManager em, String code) {
        SurveyQuestionBean surveyQuestionBean = null;

        Query query = em.createNamedQuery(SurveyQuestionBean.FIND_BY_CODE);

        query.setParameter("code", code);
        query.setMaxResults(1);

        List<SurveyQuestionBean> surveyQuestionBeanList = query.getResultList();

        if (surveyQuestionBeanList != null && surveyQuestionBeanList.size() > 0) {
            surveyQuestionBean = (SurveyQuestionBean) surveyQuestionBeanList.get(0);
        }
        else {
            surveyQuestionBean = null;
        }

        return surveyQuestionBean;

    }

    @SuppressWarnings("unchecked")
    public static SurveyBean findSurveyActive(EntityManager em, Date today) {
        SurveyBean surveyBean = null;

        Query query = em.createNamedQuery(SurveyBean.FIND_ACTIVE);

        query.setParameter("today", today);
        query.setMaxResults(1);

        List<SurveyBean> surveyBeanList = query.getResultList();

        if (surveyBeanList != null && surveyBeanList.size() > 0) {
            surveyBean = (SurveyBean) surveyBeanList.get(0);
        }
        else {
            surveyBean = null;
        }

        return surveyBean;

    }

    @SuppressWarnings("unchecked")
    public static SurveySubmissionBean findSurveySubmissionByKey(EntityManager em, String key) {
        SurveySubmissionBean surveySubmissionBean = null;

        Query query = em.createNamedQuery(SurveySubmissionBean.FIND_BY_KEY);

        query.setParameter("key", key);
        query.setMaxResults(1);

        List<SurveySubmissionBean> surveySubmissionBeanList = query.getResultList();

        if (surveySubmissionBeanList != null && surveySubmissionBeanList.size() > 0) {
            surveySubmissionBean = (SurveySubmissionBean) surveySubmissionBeanList.get(0);
        }
        else {
            surveySubmissionBean = null;
        }

        return surveySubmissionBean;

    }

    @SuppressWarnings("unchecked")
    public static List<SurveySubmissionBean> findSurveySubmissionsBySurveyCode(EntityManager em, String surveyCode) {

        Query query = em.createNamedQuery(SurveySubmissionBean.FIND_BY_SURVEY_CODE);

        query.setParameter("surveyCode", surveyCode);

        List<SurveySubmissionBean> surveySubmissionBeanList = query.getResultList();

        return surveySubmissionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TransactionBean> findTransactionsByPaymentMethodId(EntityManager em, Long paymentMethodId) {

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_PAYMENT_METHOD_ID);

        query.setParameter("paymentMethodId", paymentMethodId);

        List<TransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<PostPaidTransactionBean> findPostPaidTransactionsByPaymentMethodId(EntityManager em, Long paymentMethodId) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_BY_PAYMENT_METHOD_ID);

        query.setParameter("paymentMethodId", paymentMethodId);

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        return postPaidTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static Long countActiveToken(EntityManager em, String token) {

        Query query = em.createNamedQuery(PaymentInfoBean.COUNT_ACTIVE_TOKEN);
        query.setParameter("token", token);
        query.setMaxResults(1);

        Long activeTokenNumber = 0l;

        List<Long> integerList = query.getResultList();
        if (integerList != null && integerList.size() > 0) {
            activeTokenNumber = (Long) integerList.get(0);
        }

        return activeTokenNumber;
    }

    @SuppressWarnings("unchecked")
    public static TransactionBean findTransactionsBySrcTransactionID(EntityManager em, String srcTransactionID) {

        Query query = em.createNamedQuery(TransactionBean.FIND_BY_SRC_TRANSACTION_ID);

        query.setParameter("srcTransactionID", srcTransactionID);

        List<TransactionBean> transactionBeanList = query.getResultList();
        if (transactionBeanList != null && transactionBeanList.size() > 0) {

            return transactionBeanList.get(0);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<RefuelingMappingErrorBean> findAllErrorRefueling(EntityManager em) {

        Query query = em.createNamedQuery(RefuelingMappingErrorBean.FIND_ALL);
        List<RefuelingMappingErrorBean> errorRefuelingList = query.getResultList();

        return errorRefuelingList;

    }

    @SuppressWarnings("unchecked")
    public static RefuelingMappingErrorBean findErrorRefuelingByErrorCode(EntityManager em, String errorCode) {

        Query query = em.createNamedQuery(RefuelingMappingErrorBean.FIND_BY_ERRORCODE);
        query.setParameter("errorCode", errorCode);

        List<RefuelingMappingErrorBean> errorRefuelingList = query.getResultList();
        if (errorRefuelingList != null && errorRefuelingList.size() > 0) {

            return errorRefuelingList.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static RefuelingMappingErrorBean findErrorRefuelingByID(EntityManager em, String id) {

        Query query = em.createNamedQuery(RefuelingMappingErrorBean.FIND_BY_ID);
        Long idLong = Long.valueOf(id);
        query.setParameter("id", idLong);

        List<RefuelingMappingErrorBean> errorRefuelingList = query.getResultList();
        if (errorRefuelingList != null && errorRefuelingList.size() > 0) {

            return errorRefuelingList.get(0);
        }

        return null;

    }

    @SuppressWarnings("unchecked")
    public static UserCategoryBean findUserCategoryByName(EntityManager em, String name) {

        Query query = em.createNamedQuery(UserCategoryBean.FIND_BY_NAME);
        query.setParameter("name", name);
        query.setMaxResults(1);

        List<UserCategoryBean> userCategoryBeanList = query.getResultList();
        UserCategoryBean userCategoryBean = new UserCategoryBean();
        if (userCategoryBeanList != null && userCategoryBeanList.size() > 0) {
            userCategoryBean = (UserCategoryBean) userCategoryBeanList.get(0);
        }
        else {
            userCategoryBean = null;
        }

        return userCategoryBean;
    }

    @SuppressWarnings("unchecked")
    public static UserCategoryBean findUserCategoryById(EntityManager em, Long id) {

        Query query = em.createNamedQuery(UserCategoryBean.FIND_BY_ID);
        query.setParameter("id", id);
        query.setMaxResults(1);

        List<UserCategoryBean> userCategoryBeanList = query.getResultList();
        UserCategoryBean userCategoryBean = new UserCategoryBean();
        if (userCategoryBeanList != null && userCategoryBeanList.size() > 0) {
            userCategoryBean = (UserCategoryBean) userCategoryBeanList.get(0);
        }
        else {
            userCategoryBean = null;
        }

        return userCategoryBean;
    }

    @SuppressWarnings("unchecked")
    public static List<UserCategoryBean> findUserCategoryAllName(EntityManager em) {

        Query query = em.createNamedQuery(UserCategoryBean.FIND_LIST_ALL);

        List<UserCategoryBean> userCategoryBeanList = query.getResultList();

        return userCategoryBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<UserTypeBean> findUserTypeAllCode(EntityManager em) {

        Query query = em.createNamedQuery(UserTypeBean.FIND_ALL);

        List<UserTypeBean> userTypeBeanList = query.getResultList();

        return userTypeBeanList;
    }

    @SuppressWarnings("unchecked")
    public static UserTypeBean findUserTypeByCode(EntityManager em, Integer code) {

        Query query = em.createNamedQuery(UserTypeBean.FIND_BY_NAME);
        query.setParameter("code", code);
        query.setMaxResults(1);

        List<UserTypeBean> userTypeBeanList = query.getResultList();
        UserTypeBean userTypeBean = new UserTypeBean();
        if (userTypeBeanList != null && userTypeBeanList.size() > 0) {
            userTypeBean = (UserTypeBean) userTypeBeanList.get(0);
        }
        else {
            userTypeBean = null;
        }

        return userTypeBean;
    }

    @SuppressWarnings("unchecked")
    public static List<PrefixNumberBean> findAllPrefix(EntityManager em) {

        Query query = em.createNamedQuery(PrefixNumberBean.FIND_ALLPREFIX);

        List<PrefixNumberBean> prefixNumberBeanList = query.getResultList();

        return prefixNumberBeanList;
    }

    @SuppressWarnings("unchecked")
    public static PrefixNumberBean findPrefixByCode(EntityManager em, String code) {

        Query query = em.createNamedQuery(PrefixNumberBean.FIND_PREFIX_BYCODE);
        query.setParameter("code", code);
        query.setMaxResults(1);

        List<PrefixNumberBean> prefixNumberBeanList = query.getResultList();
        PrefixNumberBean prefixNumberBean = new PrefixNumberBean();
        if (prefixNumberBeanList != null && prefixNumberBeanList.size() > 0) {
            prefixNumberBean = (PrefixNumberBean) prefixNumberBeanList.get(0);
        }
        else {
            prefixNumberBean = null;
        }

        return prefixNumberBean;
    }

    @SuppressWarnings("unchecked")
    public static UserBean findUserByMobilePhone(EntityManager em, String mobilephone) {

        Query query = em.createNamedQuery(UserBean.FIND_BY_MOBILE_PHONE);
        query.setParameter("mobilephone", mobilephone);
        query.setMaxResults(1);

        List<UserBean> userBeanList = query.getResultList();
        UserBean userBean = new UserBean();
        if (userBeanList != null && userBeanList.size() > 0) {
            userBean = (UserBean) userBeanList.get(0);
        }
        else {
            userBean = null;
        }

        return userBean;
    }

    @SuppressWarnings("unchecked")
    public static MobilePhoneBean findMobilePhoneByNumber(EntityManager em, String number) {

        Query query = em.createNamedQuery(MobilePhoneBean.FIND_BY_NUMBER);
        query.setParameter("number", number);
        query.setMaxResults(1);

        List<MobilePhoneBean> mobilePhoneList = query.getResultList();
        MobilePhoneBean mobilePhoneBean = new MobilePhoneBean();
        if (mobilePhoneList != null && mobilePhoneList.size() > 0) {
            mobilePhoneBean = (MobilePhoneBean) mobilePhoneList.get(0);
        }
        else {
            mobilePhoneBean = null;
        }

        return mobilePhoneBean;
    }

    @SuppressWarnings("unchecked")
    public static MobilePhoneBean findMobilePhoneByNumberAndPrefix(EntityManager em, String number, String prefix) {

        Query query = em.createNamedQuery(MobilePhoneBean.FIND_BY_NUMBER_AND_PREFIX);
        query.setParameter("number", number);
        query.setParameter("prefix", prefix);
        query.setMaxResults(1);

        List<MobilePhoneBean> mobilePhoneList = query.getResultList();
        MobilePhoneBean mobilePhoneBean = new MobilePhoneBean();
        if (mobilePhoneList != null && mobilePhoneList.size() > 0) {
            mobilePhoneBean = (MobilePhoneBean) mobilePhoneList.get(0);
        }
        else {
            mobilePhoneBean = null;
        }

        return mobilePhoneBean;
    }

    @SuppressWarnings("unchecked")
    public static List<MobilePhoneBean> findAllActiveMobilePhoneByNumberAndPrefix(EntityManager em, String number, String prefix) {

        Query query = em.createNamedQuery(MobilePhoneBean.FIND_ACTIVE_BY_NUMBER_AND_PREFIX);
        query.setParameter("number", number);
        query.setParameter("prefix", prefix);

        List<MobilePhoneBean> mobilePhoneBeanList = query.getResultList();

        return mobilePhoneBeanList;
    }

    @SuppressWarnings("unchecked")
    public static MobilePhoneBean findActiveMobilePhoneByNumberAndPrefix(EntityManager em, String number, String prefix) {

        Query query = em.createNamedQuery(MobilePhoneBean.FIND_ACTIVE_BY_NUMBER_AND_PREFIX);
        query.setParameter("number", number);
        query.setParameter("prefix", prefix);
        query.setMaxResults(1);

        System.out.println("number: " + number);
        System.out.println("prefix: " + prefix);

        List<MobilePhoneBean> mobilePhoneList = query.getResultList();
        MobilePhoneBean mobilePhoneBean = new MobilePhoneBean();
        if (mobilePhoneList != null && mobilePhoneList.size() > 0) {
            mobilePhoneBean = (MobilePhoneBean) mobilePhoneList.get(0);
            System.out.println("FOUND");
        }
        else {
            mobilePhoneBean = null;
            System.out.println("NOT FOUND");
        }

        return mobilePhoneBean;
    }

    @SuppressWarnings("unchecked")
    public static MobilePhoneBean findMobilePhoneById(EntityManager em, Long id) {

        Query query = em.createNamedQuery(MobilePhoneBean.FIND_BY_ID);
        query.setParameter("id", id);
        query.setMaxResults(1);

        List<MobilePhoneBean> mobilePhoneList = query.getResultList();
        MobilePhoneBean mobilePhoneBean = new MobilePhoneBean();
        if (mobilePhoneList != null && mobilePhoneList.size() > 0) {
            mobilePhoneBean = (MobilePhoneBean) mobilePhoneList.get(0);
        }
        else {
            mobilePhoneBean = null;
        }

        return mobilePhoneBean;
    }

    @SuppressWarnings("unchecked")
    public static List<MobilePhoneBean> findMobilePhoneByUserBean(EntityManager em, UserBean userBean) {

        Query query = em.createNamedQuery(MobilePhoneBean.FIND_BY_USER);
        query.setParameter("userBean", userBean);

        List<MobilePhoneBean> mobilePhoneBeanList = query.getResultList();

        return mobilePhoneBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<PromoVoucherBean> findPromoVouchersNotConsumedByUser(EntityManager em, UserBean userBean) {

        Query query = em.createNamedQuery(PromoVoucherBean.FIND_BY_USER_AND_STATUS_NOT_CONSUMED);

        List<PromoVoucherBean> PromoVoucherBeanList = query.getResultList();

        return PromoVoucherBeanList;
    }

    @SuppressWarnings("unchecked")
    public static VoucherTransactionBean findVoucherTransactionBeanByID(EntityManager em, String voucherTransactionID) {

        Query query = em.createNamedQuery(VoucherTransactionBean.FIND_BY_ID);
        query.setParameter("voucherTransactionID", voucherTransactionID);
        query.setMaxResults(1);

        List<VoucherTransactionBean> voucherTransactionBeanList = query.getResultList();
        VoucherTransactionBean voucherTransactionBean = new VoucherTransactionBean();
        if (voucherTransactionBeanList != null && voucherTransactionBeanList.size() > 0) {
            voucherTransactionBean = (VoucherTransactionBean) voucherTransactionBeanList.get(0);
        }
        else {
            voucherTransactionBean = null;
        }

        return voucherTransactionBean;
    }

    @SuppressWarnings("unchecked")
    public static List<VoucherTransactionBean> searchVoucherTransactions(EntityManager em, Long userId, String voucherCode, Date creationTimestampStart, Date creationTimestampEnd,
            String finalStatusType, Integer maxResults) {

        UserBean userBean = null;

        List<VoucherTransactionBean> voucherTransactionBean = new ArrayList<VoucherTransactionBean>(0);

        String queryString = "select t from VoucherTransactionBean t where 1=1";

        if (userId != null) {
            userBean = QueryRepository.findUserById(em, userId);
            if (userBean != null) {
                queryString += " and userBean = :userBean";
            }
            else {
                return voucherTransactionBean;
            }
        }

        queryString += " and creationTimestamp >= :creationTimestampStart";
        queryString += " and creationTimestamp <= :creationTimestampEnd";

        if (finalStatusType != null) {
            queryString += " and finalStatusType like :finalStatusType";
        }
        Query query = em.createQuery(queryString);

        if (userBean != null) {
            query.setParameter("userBean", userBean);
        }

        query.setParameter("creationTimestampStart", creationTimestampStart);
        query.setParameter("creationTimestampEnd", creationTimestampEnd);
        if (finalStatusType != null) {

            query.setParameter("finalStatusType", finalStatusType);
        }
        // query.setFirstResult(100);
        query.setMaxResults(maxResults);

        voucherTransactionBean = query.getResultList();

        return voucherTransactionBean;

    }

    @SuppressWarnings("unchecked")
    public static VoucherTransactionOperationBean findVoucherTransactionOperationByTransaction(EntityManager em, VoucherTransactionBean voucherTransactionBean) {

        Query query = em.createNamedQuery(VoucherTransactionOperationBean.FIND_BY_TRANSACTION);
        query.setParameter("voucherTransactionBean", voucherTransactionBean);
        query.setMaxResults(1);

        List<VoucherTransactionOperationBean> voucherTransactionOperationBeanList = query.getResultList();
        VoucherTransactionOperationBean voucherTransactionOperationBean = null;

        if (voucherTransactionOperationBeanList != null && voucherTransactionOperationBeanList.size() > 0) {
            voucherTransactionOperationBean = (VoucherTransactionOperationBean) voucherTransactionOperationBeanList.get(0);
        }
        else {
            voucherTransactionOperationBean = null;
        }

        return voucherTransactionOperationBean;
    }

    @SuppressWarnings("unchecked")
    public static List<VoucherTransactionBean> findVoucherTransactionsByFinalStatusType(EntityManager em, String finalStatusType) {

        Query query = em.createNamedQuery(VoucherTransactionBean.FIND_BY_FINAL_STATUS_TYPE);

        query.setParameter("finalStatusType", finalStatusType);

        List<VoucherTransactionBean> transactionBeanList = query.getResultList();

        return transactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TermsOfServiceBean> findTermOfServiceByKeyValAndId(EntityManager em, String keyval, PersonalDataBean personalDataId) {

        Query query = em.createNamedQuery(TermsOfServiceBean.FIND_BY_ID_AND_KEYVAL);

        query.setParameter("keyval", keyval);

        query.setParameter("personalDataId", personalDataId);

        List<TermsOfServiceBean> termsOfServiceBeanList = query.getResultList();

        return termsOfServiceBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TermsOfServiceBean> findTermOfServiceByKeyVal(EntityManager em, String keyval) {

        Query query = em.createNamedQuery(TermsOfServiceBean.FIND_BY_KEYVAL);

        query.setParameter("keyval", keyval);

        List<TermsOfServiceBean> termsOfServiceBeanList = query.getResultList();

        return termsOfServiceBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TermsOfServiceBean> findTermOfServiceByPersonalDataId(EntityManager em, PersonalDataBean personalDataId) {

        Query query = em.createNamedQuery(TermsOfServiceBean.FIND_BY_PERSONALDATA_ID);

        query.setParameter("personalDataId", personalDataId);

        List<TermsOfServiceBean> termsOfServiceBeanList = query.getResultList();

        return termsOfServiceBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<TermsOfServiceBean> findTermOfServiceAll(EntityManager em) {

        Query query = em.createNamedQuery(TermsOfServiceBean.FIND_ALL);

        List<TermsOfServiceBean> termsOfServiceBeanList = query.getResultList();

        return termsOfServiceBeanList;

    }

    @SuppressWarnings("unchecked")
    public static PersonalDataBean findAllPersonalDataBean(EntityManager em, Long id) {

        Query query = em.createNamedQuery(PersonalDataBean.FIND_BY_ID);
        query.setParameter("id", id);
        query.setMaxResults(1);

        List<PersonalDataBean> personalDataBeanList = query.getResultList();
        PersonalDataBean personalDataBean = null;

        if (personalDataBeanList != null && personalDataBeanList.size() > 0) {
            personalDataBean = (PersonalDataBean) personalDataBeanList.get(0);
        }
        else {
            personalDataBean = null;
        }

        return personalDataBean;
    }

    @SuppressWarnings("unchecked")
    public static UnavailabilityPeriodBean findUnavailabilityPeriodByCode(EntityManager em, String code) {

        Query query = em.createNamedQuery(UnavailabilityPeriodBean.FIND_BY_CODE);
        query.setParameter("code", code);
        query.setMaxResults(1);

        List<UnavailabilityPeriodBean> unavailabilityPeriodBeanList = query.getResultList();
        UnavailabilityPeriodBean unavailabilityPeriodBean = null;

        if (unavailabilityPeriodBeanList != null && unavailabilityPeriodBeanList.size() > 0) {
            unavailabilityPeriodBean = (UnavailabilityPeriodBean) unavailabilityPeriodBeanList.get(0);
        }
        else {
            unavailabilityPeriodBean = null;
        }

        return unavailabilityPeriodBean;

    }

    @SuppressWarnings("unchecked")
    public static List<UnavailabilityPeriodBean> findAllUnavailabilityPeriods(EntityManager em) {

        Query query = em.createNamedQuery(UnavailabilityPeriodBean.FIND_ALL);

        List<UnavailabilityPeriodBean> unavailabilityPeriodBeanList = query.getResultList();

        return unavailabilityPeriodBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<UnavailabilityPeriodBean> findAllActiveUnavailabilityPeriods(EntityManager em) {

        Query query = em.createNamedQuery(UnavailabilityPeriodBean.FIND_ACTIVE);

        List<UnavailabilityPeriodBean> unavailabilityPeriodBeanList = query.getResultList();

        return unavailabilityPeriodBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<UnavailabilityPeriodBean> findUnavailabilityPeriodsByActive(EntityManager em, Boolean active) {

        Query query = em.createNamedQuery(UnavailabilityPeriodBean.FIND_BY_ACTIVE);
        query.setParameter("active", active);

        List<UnavailabilityPeriodBean> unavailabilityPeriodBeanList = query.getResultList();

        return unavailabilityPeriodBeanList;

    }

    @SuppressWarnings("unchecked")
    public static DocumentAttributeBean findDocumentAttributeByCheckKey(EntityManager em, String checkKey) {

        DocumentAttributeBean documentAttributeBean;
        Query query = em.createNamedQuery(DocumentAttributeBean.FIND_BY_CHECKKEY);
        query.setParameter("checkKey", checkKey);
        query.setMaxResults(1);

        List<DocumentAttributeBean> documentAttributeBeanList = query.getResultList();

        if (documentAttributeBeanList != null && documentAttributeBeanList.size() > 0) {
            documentAttributeBean = (DocumentAttributeBean) documentAttributeBeanList.get(0);
        }
        else {
            documentAttributeBean = null;
        }

        return documentAttributeBean;

    }

    @SuppressWarnings("unchecked")
    public static SmsLogBean findSmsLogByCorrelationID(EntityManager em, String correlationID) {

        SmsLogBean smsLogBean = null;
        Query query = em.createNamedQuery(SmsLogBean.FIND_BY_CORRELATIONID);
        query.setParameter("correlationID", correlationID);
        query.setMaxResults(1);

        List<SmsLogBean> smsLogBeanList = query.getResultList();

        if (smsLogBeanList != null && smsLogBeanList.size() > 0) {
            smsLogBean = (SmsLogBean) smsLogBeanList.get(0);
        }

        return smsLogBean;

    }

    @SuppressWarnings("unchecked")
    public static List<SmsLogBean> findSmsLogPending(EntityManager em) {

        Query query = em.createNamedQuery(SmsLogBean.FIND_BY_STATUS_PENDING);

        List<SmsLogBean> smsLogBeanList = query.getResultList();

        return smsLogBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<SmsLogBean> findSmsLogAll(EntityManager em) {

        Query query = em.createNamedQuery(SmsLogBean.FIND_ALL);

        List<SmsLogBean> smsLogBeanList = query.getResultList();

        return smsLogBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<UserBean> findCustomerUserWithRegistrationCompleted(EntityManager em) {

        Query query = em.createNamedQuery(UserBean.FIND_CUSTOMER_USER_WITH_REGISTRATION_COMPLETED);

        List<UserBean> userBeanList = query.getResultList();

        return userBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<UserBean> findCustomerUserNotMigrated(EntityManager em, Date createDateLimit) {

        Query query = em.createNamedQuery(UserBean.FIND_CUSTOMER_USER_NOT_MIGRATED);
        query.setParameter("createDateLimit", createDateLimit);

        List<UserBean> userBeanList = query.getResultList();

        return userBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<MailingListBean> findAllMailingList(EntityManager em) {

        Query query = em.createNamedQuery(MailingListBean.FIND_ALL);

        List<MailingListBean> mailingListBeanList = query.getResultList();

        return mailingListBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<MailingListBean> findMailingListByStatus(EntityManager em, Integer status, Integer packetSize) {

        Query query = em.createNamedQuery(MailingListBean.FIND_BY_STATUS);
        query.setParameter("status", status);
        query.setMaxResults(packetSize);

        List<MailingListBean> mailingListBeanList = query.getResultList();

        return mailingListBeanList;

    }

    @SuppressWarnings("unchecked")
    public static MailingListBean findMailingListById(EntityManager em, Long id) {

        Query query = em.createNamedQuery(MailingListBean.FIND_BY_ID);
        query.setParameter("id", id);
        query.setMaxResults(1);

        List<MailingListBean> mailingListBeanList = query.getResultList();
        MailingListBean mailingListBean = new MailingListBean();
        if (mailingListBeanList != null && mailingListBeanList.size() > 0) {
            mailingListBean = (MailingListBean) mailingListBeanList.get(0);
        }
        else {
            mailingListBean = null;
        }

        return mailingListBean;
    }

    @SuppressWarnings("unchecked")
    public static TransactionEventBean findTransactionEventById(EntityManager em, Long id) {

        Query query = em.createNamedQuery(TransactionEventBean.FIND_BY_ID);
        query.setParameter("id", id);
        query.setMaxResults(1);

        List<TransactionEventBean> transactionEventBeanList = query.getResultList();
        TransactionEventBean transactionEventBean = new TransactionEventBean();
        if (transactionEventBeanList != null && transactionEventBeanList.size() > 0) {
            transactionEventBean = (TransactionEventBean) transactionEventBeanList.get(0);
        }
        else {
            transactionEventBean = null;
        }

        return transactionEventBean;
    }

    @SuppressWarnings("unchecked")
    public static List<StatisticsObject> getStatisticsPrepaidByUser(EntityManager em, UserBean userBean, Date startDate, Date endDate) {

        Query query = em.createQuery("SELECT t.productID as productID, t.productDescription as productDescription, SUM(t.fuelQuantity) as quantity, SUM(t.finalAmount) as amount FROM TransactionBean t "
                + "WHERE t.finalStatusType = 'SUCCESSFUL' " + "AND t.userBean = :userBean " + "AND t.creationTimestamp >= :startDate " + "AND t.creationTimestamp <= :endDate "
                + "GROUP BY t.productID, t.productDescription");

        query.setParameter("userBean", userBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<Object> result = query.getResultList();

        List<StatisticsObject> statisticsObjectList = new ArrayList<StatisticsObject>(0);

        Iterator<Object> it = result.iterator();

        while (it.hasNext()) {

            Object[] object = (Object[]) it.next();

            StatisticsObject statisticsObject = new StatisticsObject();
            statisticsObject.productID = (String) object[0];
            statisticsObject.productDescription = (String) object[1];
            statisticsObject.quantity = (Double) object[2];
            statisticsObject.amount = (Double) object[3];
            statisticsObjectList.add(statisticsObject);
        }

        return statisticsObjectList;

    }

    @SuppressWarnings("unchecked")
    public static List<StatisticsObject> getStatisticsPrepaidHistoryByUser(EntityManager em, UserBean userBean, Date startDate, Date endDate) {

        Query query = em.createQuery("SELECT t.productID as productID, t.productDescription as productDescription, SUM(t.fuelQuantity) as quantity, SUM(t.finalAmount) as amount FROM TransactionHistoryBean t "
                + "WHERE t.finalStatusType = 'SUCCESSFUL' " + "AND t.userBean = :userBean " + "AND t.creationTimestamp >= :startDate " + "AND t.creationTimestamp <= :endDate "
                + "GROUP BY t.productID, t.productDescription");

        query.setParameter("userBean", userBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<Object> result = query.getResultList();

        List<StatisticsObject> statisticsObjectList = new ArrayList<StatisticsObject>(0);

        Iterator<Object> it = result.iterator();

        while (it.hasNext()) {

            Object[] object = (Object[]) it.next();

            StatisticsObject statisticsObject = new StatisticsObject();
            statisticsObject.productID = (String) object[0];
            statisticsObject.productDescription = (String) object[1];
            statisticsObject.quantity = (Double) object[2];
            statisticsObject.amount = (Double) object[3];
            statisticsObjectList.add(statisticsObject);
        }

        return statisticsObjectList;

    }

    @SuppressWarnings("unchecked")
    public static List<StatisticsObject> getStatisticsPostpaidByUser(EntityManager em, UserBean userBean, Date startDate, Date endDate) {

        Query query = em.createQuery("SELECT r.productId as productID, r.productDescription as productDescription, SUM(r.fuelQuantity) as quantity, SUM(r.fuelAmount) as amount FROM PostPaidTransactionBean t "
                + "JOIN t.refuelBean r " + "WHERE t.mpTransactionStatus = 'PAID' " + "AND t.userBean = :userBean " + "AND t.creationTimestamp >= :startDate "
                + "AND t.creationTimestamp <= :endDate " + "GROUP BY r.productId, r.productDescription");

        query.setParameter("userBean", userBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<Object> result = query.getResultList();

        List<StatisticsObject> statisticsObjectList = new ArrayList<StatisticsObject>(0);

        Iterator<Object> it = result.iterator();

        while (it.hasNext()) {

            Object[] object = (Object[]) it.next();

            StatisticsObject statisticsObject = new StatisticsObject();
            statisticsObject.productID = (String) object[0];
            statisticsObject.productDescription = (String) object[1];
            statisticsObject.quantity = (Double) object[2];
            statisticsObject.amount = (Double) object[3];
            statisticsObjectList.add(statisticsObject);
        }

        return statisticsObjectList;

    }

    @SuppressWarnings("unchecked")
    public static List<StatisticsObject> getStatisticsPostpaidHistoryByUser(EntityManager em, UserBean userBean, Date startDate, Date endDate) {

        Query query = em.createQuery("SELECT r.productId as productID, r.productDescription as productDescription, SUM(r.fuelQuantity) as quantity, SUM(r.fuelAmount) as amount FROM PostPaidTransactionHistoryBean t "
                + "JOIN t.refuelHistoryBean r "
                + "WHERE t.mpTransactionStatus = 'PAID' "
                + "AND t.userBean = :userBean "
                + "AND t.creationTimestamp >= :startDate "
                + "AND t.creationTimestamp <= :endDate " + "GROUP BY r.productId, r.productDescription");

        query.setParameter("userBean", userBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<Object> result = query.getResultList();

        List<StatisticsObject> statisticsObjectList = new ArrayList<StatisticsObject>(0);

        Iterator<Object> it = result.iterator();

        while (it.hasNext()) {

            Object[] object = (Object[]) it.next();

            StatisticsObject statisticsObject = new StatisticsObject();
            statisticsObject.productID = (String) object[0];
            statisticsObject.productDescription = (String) object[1];
            statisticsObject.quantity = (Double) object[2];
            statisticsObject.amount = (Double) object[3];
            statisticsObjectList.add(statisticsObject);
        }

        return statisticsObjectList;

    }

    @SuppressWarnings("unchecked")
    public static EmailDomainBean findEmailDomainByID(EntityManager em, String id) {

        EmailDomainBean emailDomainBean = null;
        Query query = em.createNamedQuery(EmailDomainBean.FIND_BY_ID);
        Long idLong = Long.valueOf(id);
        query.setParameter("id", idLong);
        query.setMaxResults(1);

        List<EmailDomainBean> emailDomainBeanList = query.getResultList();

        if (emailDomainBeanList != null && emailDomainBeanList.size() > 0) {
            emailDomainBean = (EmailDomainBean) emailDomainBeanList.get(0);
        }
        else {
            emailDomainBean = null;
        }

        return emailDomainBean;

    }

    @SuppressWarnings("unchecked")
    public static EmailDomainBean findEmailDomainByEmail(EntityManager em, String email) {

        EmailDomainBean emailDomainBean = null;
        Query query = em.createNamedQuery(EmailDomainBean.FIND_BY_EMAIL);
        query.setParameter("email", email);
        query.setMaxResults(1);

        List<EmailDomainBean> emailDomainBeanList = query.getResultList();

        if (emailDomainBeanList != null && emailDomainBeanList.size() > 0) {
            emailDomainBean = (EmailDomainBean) emailDomainBeanList.get(0);
        }
        else {
            emailDomainBean = null;
        }

        return emailDomainBean;

    }

    @SuppressWarnings("unchecked")
    public static List<EmailDomainBean> findAllEmailDomainBean(EntityManager em) {

        Query query = em.createNamedQuery(EmailDomainBean.FIND_ALL);

        List<EmailDomainBean> emailDomainBeanList = query.getResultList();

        return emailDomainBeanList;

    }

    @SuppressWarnings("unchecked")
    public static ProvinceInfoBean findProvinceByName(EntityManager em, String name) {

        ProvinceInfoBean provinceInfoBean = null;
        Query query = em.createNamedQuery(ProvinceInfoBean.FIND_BY_NAME);
        query.setParameter("name", name);
        query.setMaxResults(1);

        List<ProvinceInfoBean> provinceInfoBeanList = query.getResultList();

        if (provinceInfoBeanList != null && provinceInfoBeanList.size() > 0) {
            provinceInfoBean = (ProvinceInfoBean) provinceInfoBeanList.get(0);
        }
        else {
            provinceInfoBean = null;
        }

        return provinceInfoBean;

    }

    @SuppressWarnings("unchecked")
    public static LoyaltySessionBean findLoyaltySessionByFiscalCode(EntityManager em, String sessionId, String fiscalCode) {
        LoyaltySessionBean loyaltySessionBean = null;
        Query query = em.createNamedQuery(LoyaltySessionBean.FIND_BY_FISCALCODE_AND_SESSIONID);
        query.setParameter("fiscalCode", fiscalCode);
        query.setParameter("sessionId", sessionId);
        query.setMaxResults(1);

        List<LoyaltySessionBean> loyaltySessionBeanList = query.getResultList();

        if (loyaltySessionBeanList != null && loyaltySessionBeanList.size() > 0) {
            loyaltySessionBean = (LoyaltySessionBean) loyaltySessionBeanList.get(0);
        }
        else {
            loyaltySessionBean = null;
        }

        return loyaltySessionBean;
    }

    @SuppressWarnings("unchecked")
    public static PushNotificationBean getPushNotificationLogById(EntityManager em, Long id) {
        PushNotificationBean pushNotificationBean = null;
        Query query = em.createNamedQuery(PushNotificationBean.FIND_BY_ID);
        query.setParameter("id", id);
        query.setMaxResults(1);

        List<PushNotificationBean> pushNotificationLogBeanList = query.getResultList();

        if (pushNotificationLogBeanList != null && pushNotificationLogBeanList.size() > 0) {
            pushNotificationBean = (PushNotificationBean) pushNotificationLogBeanList.get(0);
        }
        else {
            pushNotificationBean = null;
        }

        return pushNotificationBean;
    }

    @SuppressWarnings("unchecked")
    public static List<UserBean> findUserByDeviceId(EntityManager em, String deviceID) {

        Query query = em.createNamedQuery(UserBean.FIND_USER_BY_DEVICEID);
        query.setParameter("deviceID", deviceID);

        List<UserBean> userBeanList = query.getResultList();

        return userBeanList;

    }

    @SuppressWarnings("unchecked")
    public static UserDeviceBean findDeviceByDeviceId(EntityManager em, String deviceID) {

        UserDeviceBean userDeviceBean = null;
        Query query = em.createNamedQuery(UserDeviceBean.FIND_DEVICE_BY_DEVICEID);
        query.setParameter("deviceID", deviceID);
        query.setMaxResults(1);

        List<UserDeviceBean> userDeviceBeanList = query.getResultList();

        if (userDeviceBeanList != null && userDeviceBeanList.size() > 0) {
            userDeviceBean = (UserDeviceBean) userDeviceBeanList.get(0);
        }
        else {
            userDeviceBean = null;
        }

        return userDeviceBean;

    }

    @SuppressWarnings("unchecked")
    public static UserDeviceBean findDeviceById(EntityManager em, Long id) {

        UserDeviceBean userDeviceBean = null;
        Query query = em.createNamedQuery(UserDeviceBean.FIND_DEVICE_BY_ID);
        query.setParameter("id", id);
        query.setMaxResults(1);

        List<UserDeviceBean> userDeviceBeanList = query.getResultList();

        if (userDeviceBeanList != null && userDeviceBeanList.size() > 0) {
            userDeviceBean = (UserDeviceBean) userDeviceBeanList.get(0);
        }
        else {
            userDeviceBean = null;
        }

        return userDeviceBean;

    }

    @SuppressWarnings("unchecked")
    public static ParameterNotificationBean findSingleParameterNotificationBeanByName(EntityManager em, String param) {

        String queryString = "select t from ParameterNotificationBean t where name = :name";

        Query query = em.createQuery(queryString);
        query.setParameter("name", param);

        List<ParameterNotificationBean> parameterNotificationBeanList = query.getResultList();
        if (parameterNotificationBeanList != null && parameterNotificationBeanList.size() > 0) {
            return (ParameterNotificationBean) parameterNotificationBeanList.get(0);
        }
        else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static List<ParameterNotificationBean> findParameterNotificationBeanByName(EntityManager em, String param) {

        List<ParameterNotificationBean> parameterNotificationBeanList = new ArrayList<ParameterNotificationBean>(0);

        String queryString = "select t from ParameterNotificationBean t where 1=1";

        if (param != null) {
            queryString += " and name like :name";
        }

        Query query = em.createQuery(queryString);

        if (param != null) {
            // query.setParameter( "name", "%"+param+"%");
            query.setParameter("name", param);
        }

        parameterNotificationBeanList = query.getResultList();

        return parameterNotificationBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<UserDeviceBean> findDeviceByUserID(EntityManager em, UserBean userID) {

        Query query = em.createNamedQuery(UserDeviceBean.FIND_DEVICE_BY_USERID);
        query.setParameter("userid", userID);
        List<UserDeviceBean> userDeviceBeanList = query.getResultList();

        return userDeviceBeanList;

    }

    @SuppressWarnings("unchecked")
    public static EventNotificationBean findEventNotificationByPushNotification(EntityManager em, PushNotificationBean push) {

        Query query = em.createNamedQuery(EventNotificationBean.FIND_BY_PUSHNOTIFICATION);
        query.setParameter("pushnotificationbean", push);
        query.setMaxResults(1);

        List<EventNotificationBean> eventNotificationBeanList = query.getResultList();

        if (eventNotificationBeanList != null && eventNotificationBeanList.size() > 0)
            return eventNotificationBeanList.get(0);

        return null;
    }

    @SuppressWarnings("unchecked")
    public static CRMEventBean findCRMEventByPushNotification(EntityManager em, PushNotificationBean push) {

        Query query = em.createNamedQuery(CRMEventBean.FIND_BY_PUSHNOTIFICATION);
        query.setParameter("pushnotificationbean", push);
        query.setMaxResults(1);

        List<CRMEventBean> crmEventBeanList = query.getResultList();

        if (crmEventBeanList != null && crmEventBeanList.size() > 0)
            return crmEventBeanList.get(0);

        return null;
    }

    @SuppressWarnings("unchecked")
    public static CRMOutboundBean findCRMOutboundByPushNotificationAndFiscalCode(EntityManager em, PushNotificationBean push, String fiscalCode) {

        Query query = em.createNamedQuery(CRMOutboundBean.FIND_BY_PUSHNOTIFICATION_AND_FISCALCODE);
        query.setParameter("pushnotificationbean", push);
        query.setParameter("fiscalCode", fiscalCode);
        query.setMaxResults(1);

        List<CRMOutboundBean> crmOutboundBeanList = query.getResultList();

        if (crmOutboundBeanList != null && crmOutboundBeanList.size() > 0)
            return crmOutboundBeanList.get(0);

        return null;
    }

    @SuppressWarnings("unchecked")
    public static LoyaltySessionBean findLoyaltySessionBySessionid(EntityManager em, String sessionId) {

        Query query = em.createNamedQuery(LoyaltySessionBean.FIND_BY_SESSIONID);
        query.setParameter("sessionId", sessionId);
        query.setMaxResults(1);

        List<LoyaltySessionBean> loyaltySessionBeanList = query.getResultList();

        if (loyaltySessionBeanList != null && loyaltySessionBeanList.size() > 0)
            return loyaltySessionBeanList.get(0);

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<UserBean> findUsersForNotification(EntityManager em) {

        Query query = em.createNamedQuery(UserBean.FIND_FOR_NOTIFICATION);

        List<UserBean> userBeanList = query.getResultList();

        if (userBeanList != null && userBeanList.size() > 0) {
            return userBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<CRMOutboundBean> findCRMOutboundNotProcessed(EntityManager em, Integer maxResult) {

        Query query = em.createNamedQuery(CRMOutboundBean.FIND_NOT_PROCESSED);

        if (maxResult != null) {
            query.setMaxResults(maxResult);
        }

        List<CRMOutboundBean> crmOutboundList = query.getResultList();

        return crmOutboundList;
    }

    @SuppressWarnings("unchecked")
    public static List<CRMOutboundBean> findCRMOutboundNotProcessed(EntityManager em, int start, int end) {

        Query query = em.createNamedQuery(CRMOutboundBean.FIND_NOT_PROCESSED);
        query.setFirstResult(start);
        query.setMaxResults(end);

        List<CRMOutboundBean> crmOutboundList = query.getResultList();

        return crmOutboundList;
    }

    @SuppressWarnings("unchecked")
    public static int findCRMOutboundNotProcessedCount(EntityManager em) {

        Query query = em.createNamedQuery(CRMOutboundBean.FIND_NOT_PROCESSED_COUNT);

        List<Object> resultSet = query.getResultList();
        int rowsCount = 0;

        if (resultSet != null && !resultSet.isEmpty()) {
            rowsCount = Integer.parseInt(resultSet.get(0).toString());
        }

        return rowsCount;
    }

    @SuppressWarnings("unchecked")
    public static int findCRMOutboundNotNotificationSentCount(EntityManager em) {

        Query query = em.createNamedQuery(CRMOutboundBean.FIND_NOT_NOTIFICATION_SENT_COUNT);

        List<Object> resultSet = query.getResultList();
        int rowsCount = 0;

        if (resultSet != null && !resultSet.isEmpty()) {
            rowsCount = Integer.parseInt(resultSet.get(0).toString());
        }

        return rowsCount;
    }

    @SuppressWarnings("unchecked")
    public static PrePaidConsumeVoucherBean findPrePaidConsumeVoucherBeanById(EntityManager em, Long id) {

        Query query = em.createNamedQuery(PrePaidConsumeVoucherBean.FIND_BY_ID);
        query.setParameter("id", id);
        query.setMaxResults(1);

        List<PrePaidConsumeVoucherBean> prePaidConsumeVoucherBeanList = query.getResultList();
        PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
        if (prePaidConsumeVoucherBeanList != null && prePaidConsumeVoucherBeanList.size() > 0) {
            prePaidConsumeVoucherBean = (PrePaidConsumeVoucherBean) prePaidConsumeVoucherBeanList.get(0);
        }
        else {
            prePaidConsumeVoucherBean = null;
        }

        return prePaidConsumeVoucherBean;
    }

    @SuppressWarnings("unchecked")
    public static PrePaidConsumeVoucherDetailBean findPrePaidConsumeVoucherDetailBeanById(EntityManager em, Long id) {

        Query query = em.createNamedQuery(PrePaidConsumeVoucherDetailBean.FIND_BY_ID);
        query.setParameter("id", id);
        query.setMaxResults(1);

        List<PrePaidConsumeVoucherDetailBean> prePaidConsumeVoucherDetailBeanList = query.getResultList();
        PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean = new PrePaidConsumeVoucherDetailBean();
        if (prePaidConsumeVoucherDetailBeanList != null && prePaidConsumeVoucherDetailBeanList.size() > 0) {
            prePaidConsumeVoucherDetailBean = (PrePaidConsumeVoucherDetailBean) prePaidConsumeVoucherDetailBeanList.get(0);
        }
        else {
            prePaidConsumeVoucherDetailBean = null;
        }

        return prePaidConsumeVoucherDetailBean;
    }

    public static int confirmTransaction(EntityManager em, Long id) {
        try {
            int count = em.createNamedQuery("confirmTransactionBean", TransactionBean.class).setParameter(1, id).executeUpdate();

            return count;
        }
        catch (Exception e) {
            return 0;
        }
    }

    @SuppressWarnings("unchecked")
    public static AdminRoleBean findAdminRoleByName(EntityManager em, String name) {

        AdminRoleBean adminRoleBean;
        Query query = em.createNamedQuery(AdminRoleBean.FIND_BY_NAME);
        query.setParameter("name", name);
        query.setMaxResults(1);

        List<AdminRoleBean> adminRoleBeanList = query.getResultList();

        if (adminRoleBeanList != null && adminRoleBeanList.size() > 0) {
            adminRoleBean = (AdminRoleBean) adminRoleBeanList.get(0);
        }
        else {
            adminRoleBean = null;
        }

        return adminRoleBean;

    }

    @SuppressWarnings("unchecked")
    public static List<AdminRoleBean> findAllAdminRolePeriods(EntityManager em) {

        Query query = em.createNamedQuery(AdminRoleBean.FIND_ALL);

        List<AdminRoleBean> adminRoleBeanList = query.getResultList();

        return adminRoleBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<DWHEventBean> findDWHEventToReconcilie(EntityManager em) {

        Query query = em.createNamedQuery(DWHEventBean.FIND_TO_RECONCILIE);

        List<DWHEventBean> userPropagationtransactionBeanList = query.getResultList();

        if (userPropagationtransactionBeanList != null && userPropagationtransactionBeanList.size() > 0) {
            return userPropagationtransactionBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static DWHEventBean findDWHEventByUserAndOperation(EntityManager em, UserBean userBean, String operation) {

        Query query = em.createNamedQuery(DWHEventBean.FIND_BY_USER_AND_OPERATION);
        query.setParameter("userBean", userBean);
        query.setParameter("operation", operation);
        query.setMaxResults(1);

        List<DWHEventBean> dwhEventBeanList = query.getResultList();

        if (dwhEventBeanList != null && dwhEventBeanList.size() > 0) {
            return dwhEventBeanList.get(0);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<DWHEventBean> findDWHEventByUser(EntityManager em, UserBean userBean) {

        Query query = em.createNamedQuery(DWHEventBean.FIND_BY_USER);
        query.setParameter("userBean", userBean);
        query.setMaxResults(1);

        List<DWHEventBean> dwhEventBeanList = query.getResultList();

        if (dwhEventBeanList != null && dwhEventBeanList.size() > 0) {
            return dwhEventBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<PushNotificationBean> findPushNotificationToReconcilie(EntityManager em) {

        Query query = em.createNamedQuery(PushNotificationBean.FIND_TO_RECONCILIE);

        List<PushNotificationBean> pushNotificationBeanList = query.getResultList();

        if (pushNotificationBeanList != null && pushNotificationBeanList.size() > 0) {
            return pushNotificationBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<CRMEventBean> findCRMEventToReconcilie(EntityManager em) {

        Query query = em.createNamedQuery(CRMEventBean.FIND_TO_RECONCILIE);

        List<CRMEventBean> crmEventBeanList = query.getResultList();

        if (crmEventBeanList != null && crmEventBeanList.size() > 0) {
            return crmEventBeanList;
        }

        return null;
    }
    
    @SuppressWarnings("unchecked")
    public static List<CRMSfEventBean> findCRMSfEventToReconcilie(EntityManager em) {

        Query query = em.createNamedQuery(CRMSfEventBean.FIND_TO_RECONCILIE);

        List<CRMSfEventBean> crmSfEventBeanList = query.getResultList();

        if (crmSfEventBeanList != null && crmSfEventBeanList.size() > 0) {
            return crmSfEventBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<CRMOfferVoucherPromotionalBean> findCRMOfferVoucherPromotionalToReconcilie(EntityManager em) {

        Query query = em.createNamedQuery(CRMOfferVoucherPromotionalBean.FIND_TO_RECONCILIE);

        List<CRMOfferVoucherPromotionalBean> crmOfferVoucherPromotionalBeanList = query.getResultList();

        if (crmOfferVoucherPromotionalBeanList != null && crmOfferVoucherPromotionalBeanList.size() > 0) {
            return crmOfferVoucherPromotionalBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<RedemptionTransactionBean> findRedemptionToReconcilie(EntityManager em) {

        Query query = em.createNamedQuery(RedemptionTransactionBean.FIND_TO_RECONCILIE);

        List<RedemptionTransactionBean> redemptionTransactionBeanList = query.getResultList();

        if (redemptionTransactionBeanList != null && redemptionTransactionBeanList.size() > 0) {
            return redemptionTransactionBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static LandingDataBean findLandingPageByName(EntityManager em, String sectionID) {

        LandingDataBean landingDataBean;
        Query query = em.createNamedQuery(LandingDataBean.FIND_BY_SECTIONID);
        query.setParameter("sectionID", sectionID);
        query.setMaxResults(1);

        List<LandingDataBean> landingDataBeanList = query.getResultList();

        if (landingDataBeanList != null && landingDataBeanList.size() > 0) {
            landingDataBean = (LandingDataBean) landingDataBeanList.get(0);
        }
        else {
            landingDataBean = null;
        }

        return landingDataBean;

    }

    @SuppressWarnings("unchecked")
    public static PromoPopupBean findPromoPopupBySection(EntityManager em, String sectionId, Date checkDate) {

        PromoPopupBean promoPopupBean;
        Query query = em.createNamedQuery(PromoPopupBean.FIND_BY_SECTIONID);
        query.setParameter("sectionId", sectionId);
        query.setParameter("checkDate", checkDate);
        query.setMaxResults(1);

        List<PromoPopupBean> promoPopupBeanList = query.getResultList();

        if (promoPopupBeanList != null && promoPopupBeanList.size() > 0) {
            promoPopupBean = (PromoPopupBean) promoPopupBeanList.get(0);
        }
        else {
            promoPopupBean = null;
        }

        return promoPopupBean;

    }

    @SuppressWarnings("unchecked")
    public static List<LandingDataBean> findAllLandingPages(EntityManager em) {

        Query query = em.createNamedQuery(LandingDataBean.FIND_ALL);

        List<LandingDataBean> landingDataBeanList = query.getResultList();

        if (landingDataBeanList != null && landingDataBeanList.size() > 0) {
            return landingDataBeanList;
        }

        return null;

    }

    @SuppressWarnings("unchecked")
    public static PollingIntervalBean findPollingIntervalByStatus(EntityManager em, String status) {

        PollingIntervalBean pollingIntervalBean;
        Query query = em.createNamedQuery(PollingIntervalBean.FIND_BY_STATUS);
        query.setParameter("status", status);
        query.setMaxResults(1);

        List<PollingIntervalBean> pollingIntervalBeanList = query.getResultList();

        if (pollingIntervalBeanList != null && pollingIntervalBeanList.size() > 0) {
            pollingIntervalBean = (PollingIntervalBean) pollingIntervalBeanList.get(0);
        }
        else {
            pollingIntervalBean = null;
        }

        return pollingIntervalBean;

    }

    @SuppressWarnings("unchecked")
    public static List<PollingIntervalBean> findAllPollingInterval(EntityManager em) {

        Query query = em.createNamedQuery(PollingIntervalBean.FIND_ALL);

        List<PollingIntervalBean> pollingIntervalBeanList = query.getResultList();

        return pollingIntervalBeanList;

    }

    @SuppressWarnings("unchecked")
    public static CardDepositTransactionBean findCardDepositTransactionByShopTransactionID(EntityManager em, String shopTransactionID) {

        CardDepositTransactionBean cardDepositTransactionBean = null;

        Query query = em.createNamedQuery(CardDepositTransactionBean.FIND_BY_SHOPTRANSACTIONID);

        query.setParameter("shopTransactionID", shopTransactionID);
        query.setMaxResults(1);

        List<CardDepositTransactionBean> cardDepositTransactionBeanList = query.getResultList();

        if (cardDepositTransactionBeanList != null && cardDepositTransactionBeanList.size() > 0) {
            cardDepositTransactionBean = (CardDepositTransactionBean) cardDepositTransactionBeanList.get(0);
        }
        else {
            cardDepositTransactionBean = null;
        }

        return cardDepositTransactionBean;

    }

    @SuppressWarnings("unchecked")
    public static PostPaidRefuelBean findPostPaidRefuelByPumpNumber(EntityManager em, Integer pumpNumber) {

        PostPaidRefuelBean postPaidRefuelBean = null;

        Query query = em.createNamedQuery(PostPaidRefuelBean.FIND_TRANSACTION_BY_PUMP);

        query.setParameter("pumpNumber", pumpNumber);
        query.setMaxResults(1);

        List<PostPaidRefuelBean> postPaidRefuelBeanList = query.getResultList();

        if (postPaidRefuelBeanList != null && postPaidRefuelBeanList.size() > 0) {
            postPaidRefuelBean = (PostPaidRefuelBean) postPaidRefuelBeanList.get(0);
        }
        else {
            postPaidRefuelBean = null;
        }

        return postPaidRefuelBean;

    }

    @SuppressWarnings("unchecked")
    public static PostPaidTransactionBean findPostPaidTransactinByPumpNumber(EntityManager em, StationBean stationBean, String sourceNumber) {

        PostPaidTransactionBean postPaidTransactionBean = null;

        Query query = em.createNamedQuery(PostPaidTransactionBean.FIND_BY_STATIONID_SOURCE_NUMBER);

        query.setParameter("stationBean", stationBean);
        query.setParameter("sourceNumber", sourceNumber);
        query.setMaxResults(1);

        List<PostPaidTransactionBean> postPaidTransactionBeanList = query.getResultList();

        if (postPaidTransactionBeanList != null && postPaidTransactionBeanList.size() > 0) {
            postPaidTransactionBean = (PostPaidTransactionBean) postPaidTransactionBeanList.get(0);
        }
        else {
            postPaidTransactionBean = null;
        }

        return postPaidTransactionBean;

    }

    @SuppressWarnings("unchecked")
    public static CityInfoBean findCityByName(EntityManager em, String name) {
        CityInfoBean cityInfoBean = null;

        Query query = em.createNamedQuery(CityInfoBean.FIND_BY_NAME);
        query.setParameter("name", name);
        query.setMaxResults(1);

        List<CityInfoBean> CityInfoBeanList = query.getResultList();

        if (CityInfoBeanList != null && CityInfoBeanList.size() > 0) {
            cityInfoBean = (CityInfoBean) CityInfoBeanList.get(0);
        }
        else {
            cityInfoBean = null;
        }

        return cityInfoBean;
    }

    @SuppressWarnings("unchecked")
    public static List<LoyaltyCardBean> findLoyaltyCardBeanByUserBean(EntityManager em, UserBean userBean) {

        Query query = em.createNamedQuery(LoyaltyCardBean.FIND_BY_USER);
        query.setParameter("userBean", userBean);

        List<LoyaltyCardBean> loyaltyCardBeanList = query.getResultList();

        return loyaltyCardBeanList;

    }

    @SuppressWarnings("unchecked")
    public static StationControlBean findControlGetLastRefuelByStation(EntityManager em, StationBean stationBean) {

        StationControlBean stationControlBean = null;
        Query query = em.createNamedQuery(StationControlBean.FIND_BY_STATION);
        query.setParameter("stationBean", stationBean);
        query.setParameter("control", StationControlType.GET_LAST_REFUEL.getCode());
        query.setMaxResults(1);

        List<StationControlBean> stationControlBeanList = query.getResultList();

        if (stationControlBeanList != null && stationControlBeanList.size() > 0) {
            stationControlBean = (StationControlBean) stationControlBeanList.get(0);
        }
        else {
            stationControlBeanList = null;
        }

        return stationControlBean;

    }

    @SuppressWarnings("unchecked")
    public static VoucherEventCampaignBean findVoucherEventCampaign(EntityManager em, UserBean userBean, EventCampaignBean eventCampaignBean) {
        VoucherEventCampaignBean voucherEventCampaignBean = null;

        Query query = em.createNamedQuery(VoucherEventCampaignBean.FIND_BY_USER);
        query.setParameter("userBean", userBean);
        query.setParameter("eventCampaignBean", eventCampaignBean);

        List<VoucherEventCampaignBean> voucherEventCampaignBeanList = query.getResultList();

        if (voucherEventCampaignBeanList != null && voucherEventCampaignBeanList.size() > 0) {
            voucherEventCampaignBean = (VoucherEventCampaignBean) voucherEventCampaignBeanList.get(0);
        }
        else {
            voucherEventCampaignBean = null;
        }

        return voucherEventCampaignBean;

    }

    @SuppressWarnings("unchecked")
    public static EventCampaignBean findEventCampaignByPromoCode(EntityManager em, String promoCode) {

        EventCampaignBean eventCampaignBean = null;

        Query query = em.createNamedQuery(EventCampaignBean.FIND_BY_PROMOCODE);
        query.setParameter("promoCode", promoCode);

        List<EventCampaignBean> eventCampaignBeanList = query.getResultList();

        if (eventCampaignBeanList != null && eventCampaignBeanList.size() > 0) {
            eventCampaignBean = (EventCampaignBean) eventCampaignBeanList.get(0);
        }
        else {
            eventCampaignBean = null;
        }

        return eventCampaignBean;

    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalLoyalty(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidTotalLoyalty(EntityManager em, Date dailyEndDate,Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidLoyaltyRefuelmode(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String refuelMode) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_LOYALTY_REFUELMODE);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("refuelMode", refuelMode);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidLoyaltyRefuelmode(EntityManager em, Date dailyEndDate, Date totalStartDate, String refuelMode) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_LOYALTY_REFUELMODE);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("refuelMode", refuelMode);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalLoyaltyArea(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_AREA);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidTotalLoyaltyArea(EntityManager em, Date dailyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_AREA);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalLoyaltyHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidTotalLoyaltyHistory(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidLoyaltyRefuelmodeHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String refuelMode) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_LOYALTY_REFUELMODE);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("refuelMode", refuelMode);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidLoyaltyRefuelmodeHistory(EntityManager em, Date dailyEndDate, Date totalStartDate, String refuelMode) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_LOYALTY_REFUELMODE);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("refuelMode", refuelMode);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalLoyaltyHistoryArea(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_AREA);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryArea(EntityManager em, Date dailyEndDate,  Date totalStartDate, String area) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_AREA);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalLoyalty(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPrePaidTotalLoyalty(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalLoyaltyHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPrePaidTotalLoyaltyHistory(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalLoyaltyArea(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate, Date weeklyEndDate,
            Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_AREA);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPrePaidTotalLoyaltyArea(EntityManager em, Date dailyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_AREA);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalLoyaltyHistoryArea(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_AREA);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryArea(EntityManager em, Date dailyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_AREA);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisEventNotificationTotalLoyalty(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllEventNotificationTotalLoyalty(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisEventNotificationLoyaltyRefuelmode(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String refuelMode) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_LOYALTY_REFUELMODE);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("refuelMode", refuelMode);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllEventNotificationLoyaltyRefuelmode(EntityManager em, Date dailyEndDate, Date totalStartDate, String refuelMode) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_ALL_LOYALTY_REFUELMODE);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("refuelMode", refuelMode);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisEventNotificationLoyaltyNonOil(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_LOYALTY_NONOIL);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllEventNotificationLoyaltyNonOil(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_ALL_LOYALTY_NONOIL);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisEventNotificationLoyaltyOilAndNonOil(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Integer dailyCount  = 0;
        Integer weeklyCount = 0;
        Integer fullCount   = 0;
        
        Query queryDaily = em.createQuery(
                "SELECT COUNT(e1.id) AS count FROM EventNotificationBean e1 "
                + "JOIN e1.loyaltyTransactionDetail ltd1 "
                + "JOIN ltd1.nonOilDetails nd1 "
                + "JOIN ltd1.refuelDetails rd1 "
                + "WHERE e1.eventType = 'LOYALTY'"
                + "AND e1.transactionResult = '00'" 
                + "AND e1.credits > 0" 
                + "AND e1.requestTimestamp >= :dailyStartDate "
                + "AND e1.requestTimestamp < :dailyEndDate");
        
        queryDaily.setParameter("dailyStartDate", dailyStartDate);
        queryDaily.setParameter("dailyEndDate", dailyEndDate);
        
        queryDaily.setMaxResults(1);

        List<Object> dailyResultList = queryDaily.getResultList();
        if (dailyResultList != null && !dailyResultList.isEmpty()) {
            dailyCount = Integer.parseInt(dailyResultList.get(0).toString());
        }
        
        
        
        Query queryWeekly = em.createQuery(
                "SELECT COUNT(e1.id) AS count FROM EventNotificationBean e1 "
                + "JOIN e1.loyaltyTransactionDetail ltd1 "
                + "JOIN ltd1.nonOilDetails nd1 "
                + "JOIN ltd1.refuelDetails rd1 "
                + "WHERE e1.eventType = 'LOYALTY'"
                + "AND e1.transactionResult = '00'" 
                + "AND e1.credits > 0" 
                + "AND e1.requestTimestamp >= :weeklyStartDate "
                + "AND e1.requestTimestamp < :weeklyEndDate");
        
        queryWeekly.setParameter("weeklyStartDate", weeklyStartDate);
        queryWeekly.setParameter("weeklyEndDate", weeklyEndDate);
        
        queryWeekly.setMaxResults(1);

        List<Object> weeklyResultList = queryWeekly.getResultList();
        if (weeklyResultList != null && !weeklyResultList.isEmpty()) {
            weeklyCount = Integer.parseInt(weeklyResultList.get(0).toString());
        }
        
        
        
        Query queryFull = em.createQuery(
                "SELECT COUNT(e1.id) AS count FROM EventNotificationBean e1 "
                + "JOIN e1.loyaltyTransactionDetail ltd1 "
                + "JOIN ltd1.nonOilDetails nd1 "
                + "JOIN ltd1.refuelDetails rd1 "
                + "WHERE e1.eventType = 'LOYALTY'"
                + "AND e1.transactionResult = '00'" 
                + "AND e1.credits > 0" 
                + "AND e1.requestTimestamp >= :totalStartDate "
                + "AND e1.requestTimestamp < :dailyEndDate");
        
        queryFull.setParameter("totalStartDate", totalStartDate);
        queryFull.setParameter("dailyEndDate", dailyEndDate);
        
        queryFull.setMaxResults(1);

        List<Object> fullResultList = queryFull.getResultList();
        if (fullResultList != null && !fullResultList.isEmpty()) {
            fullCount = Integer.parseInt(fullResultList.get(0).toString());
        }
        
        System.out.println("DailyCount:  " + dailyCount);
        System.out.println("WeeklyCount: " + weeklyCount);
        System.out.println("FullCount:   " + fullCount);

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add((Object)dailyCount);
        zeroList.add((Object)weeklyCount);
        zeroList.add((Object)fullCount);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllEventNotificationLoyaltyOilAndNonOil(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query queryFull = em.createQuery(
                "SELECT COUNT(e1.id) AS count FROM EventNotificationBean e1 "
                + "JOIN e1.loyaltyTransactionDetail ltd1 "
                + "JOIN ltd1.nonOilDetails nd1 "
                + "JOIN ltd1.refuelDetails rd1 "
                + "WHERE e1.eventType = 'LOYALTY'"
                + "AND e1.transactionResult = '00'" 
                + "AND e1.credits > 0" 
                + "AND e1.requestTimestamp >= :totalStartDate "
                + "AND e1.requestTimestamp < :dailyEndDate");
        
        queryFull.setParameter("totalStartDate", totalStartDate);
        queryFull.setParameter("dailyEndDate", dailyEndDate);
        
        Long result = (Long) queryFull.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisEventNotificationTotalLoyaltyArea(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_AREA);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllEventNotificationTotalLoyaltyArea(EntityManager em, Date dailyEndDate, Date totalStartDate, String area) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_AREA);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("area", area);

        Long result = (Long) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalLoyaltyCredits(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_CREDITS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPostPaidTotalLoyaltyCredits(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_CREDITS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Integer result =(Integer) query.getSingleResult();
        
        return result.longValue();
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalLoyaltyCreditsHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_CREDITS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    @SuppressWarnings("unchecked")
    public static Long statisticsReportSynthesisAllPostPaidTotalLoyaltyCreditsHistory(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_CREDITS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Integer result =(Integer) query.getSingleResult();
        
        return result.longValue();
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalLoyaltyCredits(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_CREDITS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPrePaidTotalLoyaltyCredits(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_CREDITS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Integer result =(Integer) query.getSingleResult();
        
        return result.longValue();
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalLoyaltyCreditsHistory(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_CREDITS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllPrePaidTotalLoyaltyCreditsHistory(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_CREDITS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Integer result =(Integer) query.getSingleResult();
        
        return result.longValue();
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisEventNotificationTotalLoyaltyCredits(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_CREDITS);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Long statisticsReportSynthesisAllEventNotificationTotalLoyaltyCredits(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_CREDITS);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Integer result = (Integer) query.getSingleResult();
        
        return result.longValue();
    }

    @SuppressWarnings("unchecked")
    public static List<MobilePhoneBean> getPendingMobilePhoneNumbers(EntityManager em, Date dayToCompare) {

        Query query = em.createNamedQuery(MobilePhoneBean.FIND_PENDING_NUMBER);
        query.setParameter("time", dayToCompare);

        List<MobilePhoneBean> mobilePhoneList = query.getResultList();

        return mobilePhoneList;

    }

    @SuppressWarnings("unchecked")
    public static int getCRMOutboundRowsCount(EntityManager em) {

        Query query = em.createNamedQuery(CRMOutboundBean.ROWS_COUNT);

        List<Object> resultSet = query.getResultList();
        int rowsCount = 0;

        if (resultSet != null && !resultSet.isEmpty()) {
            rowsCount = Integer.parseInt(resultSet.get(0).toString());
        }

        return rowsCount;
    }

    @SuppressWarnings("unchecked")
    public static List<CRMOutboundBean> getAllCRMOutbound(EntityManager em, boolean onlyOneRow) {

        Query query = em.createNamedQuery(CRMOutboundBean.SELECT_ALL);

        if (onlyOneRow) {
            query.setMaxResults(1);
        }

        List<CRMOutboundBean> resultSet = query.getResultList();

        return resultSet;
    }

    @SuppressWarnings("unchecked")
    public static List<CRMOutboundBean> getAllCRMOutbound(EntityManager em, int start, int end) {

        Query query = em.createNamedQuery(CRMOutboundBean.SELECT_ALL);

        query.setFirstResult(start);
        query.setMaxResults(end);

        List<CRMOutboundBean> resultSet = query.getResultList();

        return resultSet;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportStationsPrePaidTransactions(EntityManager em, Date startDate, Date endDate, TransactionCategoryType category) {
        SimpleDateFormat sdfDatabase = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");

        String categoryCondition = "";
        if (category.getValue().equals(TransactionCategoryType.TRANSACTION_BUSINESS.getValue())) {
            categoryCondition = "t.transactionCategory = '" + category.getValue() + "'";
        }
        else {
            categoryCondition = "(t.transactionCategory = '" + category.getValue() + "' OR t.transactionCategory is NULL)";
        }
        
        Query query = em.createQuery("SELECT t.stationBean.stationID, count(t.id), sum(t.fuelQuantity) FROM TransactionBean t " + "WHERE " + categoryCondition + " AND t.finalStatusType = 'SUCCESSFUL' AND t.finalAmount > 0 "
                + "AND t.stationBean.stationStatus = 2 " + "AND t.creationTimestamp >= '" + sdfDatabase.format(startDate) + "' AND t.creationTimestamp < '"
                + sdfDatabase.format(endDate) + "' " + "GROUP BY t.stationBean.stationID");

        List<Object> resultList = query.getResultList();

        return resultList;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportStationsPrePaidCreditsTransactions(EntityManager em, Date startDate, Date endDate) {
        SimpleDateFormat sdfDatabase = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");

        Query query = em.createQuery("SELECT t.stationBean.stationID, sum(l.credits) FROM TransactionBean t JOIN t.prePaidLoadLoyaltyCreditsBeanList l WHERE t.finalStatusType = 'SUCCESSFUL' AND t.finalAmount > 0 "
                + "AND l.statusCode = '00' AND l.operationType = 'LOAD' AND l.credits > 0 "
                + "AND t.stationBean.stationStatus = 2 " + "AND t.creationTimestamp >= '" + sdfDatabase.format(startDate) + "' AND t.creationTimestamp < '"
                + sdfDatabase.format(endDate) + "' " + "GROUP BY t.stationBean.stationID");

        List<Object> resultList = query.getResultList();

        return resultList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportStationsPostPaidTransactions(EntityManager em, Date startDate, Date endDate, TransactionCategoryType category) {
        SimpleDateFormat sdfDatabase = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");

        String categoryCondition = "";
        if (category.getValue().equals(TransactionCategoryType.TRANSACTION_BUSINESS.getValue())) {
            categoryCondition = "t.transactionCategory = '" + category.getValue() + "'";
        }
        else {
            categoryCondition = "(t.transactionCategory = '" + category.getValue() + "' OR t.transactionCategory is NULL)";
        }
        
        Query query = em.createQuery("SELECT t.stationBean.stationID, count(t.id), sum(r.fuelQuantity) FROM PostPaidTransactionBean t JOIN t.refuelBean r " + "WHERE " + categoryCondition + " AND t.mpTransactionStatus = 'PAID' AND t.amount > 0 "
                + "AND t.stationBean.stationStatus = 2 " + "AND t.creationTimestamp >= '" + sdfDatabase.format(startDate) + "' AND t.creationTimestamp < '"
                + sdfDatabase.format(endDate) + "' " + "GROUP BY t.stationBean.stationID");

        List<Object> resultList = query.getResultList();

        return resultList;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportStationsPostPaidCreditsTransactions(EntityManager em, Date startDate, Date endDate) {
        SimpleDateFormat sdfDatabase = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");

        Query query = em.createQuery("SELECT t.stationBean.stationID, sum(l.credits) FROM PostPaidTransactionBean t JOIN t.refuelBean r JOIN t.postPaidLoadLoyaltyCreditsBeanList l WHERE t.mpTransactionStatus = 'PAID' AND t.amount > 0 "
                + "AND l.statusCode = '00' AND l.operationType = 'LOAD' AND l.credits > 0 "
                + "AND t.stationBean.stationStatus = 2 " + "AND t.creationTimestamp >= '" + sdfDatabase.format(startDate) + "' AND t.creationTimestamp < '"
                + sdfDatabase.format(endDate) + "' " + "GROUP BY t.stationBean.stationID");

        List<Object> resultList = query.getResultList();

        return resultList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportStationsEventNotificationTransactions(EntityManager em, Date startDate, Date endDate) {
        SimpleDateFormat sdfDatabase = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");

        Query query = em.createQuery("SELECT e.stationID, count(e.id), sum(r.fuelQuantity), sum(e.credits) FROM EventNotificationBean e JOIN e.loyaltyTransactionDetail l JOIN l.refuelDetails r, StationBean s "
                + "WHERE e.stationID = s.stationID AND e.eventType = 'LOYALTY' AND e.transactionResult = '00' AND e.credits > 0 " + "AND s.stationStatus = 2 "
                + "AND e.requestTimestamp >= '" + sdfDatabase.format(startDate) + "' AND e.requestTimestamp < '" + sdfDatabase.format(endDate) + "' " + "GROUP BY e.stationID");

        List<Object> resultList = query.getResultList();

        return resultList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportStationsPostPaidLoyaltyTransactions(EntityManager em, Date startDate, Date endDate) {
        SimpleDateFormat sdfDatabase = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");

        Query query = em.createQuery("SELECT t.stationBean.stationID, COUNT(t.id), sum(r.fuelQuantity) from PostPaidTransactionBean t " + "JOIN t.postPaidLoadLoyaltyCreditsBeanList tl "
                + " JOIN t.refuelBean r WHERE tl.statusCode = '00' AND tl.operationType = 'LOAD' AND tl.credits > 0 AND t.mpTransactionStatus = 'PAID' AND t.amount > 0 "
                + "AND t.stationBean.stationStatus = 2 " + "AND t.creationTimestamp >= '" + sdfDatabase.format(startDate) + "' AND t.creationTimestamp < '"
                + sdfDatabase.format(endDate) + "' " + "GROUP BY t.stationBean.stationID");

        List<Object> resultList = query.getResultList();

        return resultList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportStationsList(EntityManager em) {

        Query query = em.createQuery("SELECT s.stationID, s.address, s.city, s.province, p.area, s.newAcquirerActive, s.loyaltyActive " + "FROM StationBean s, ProvinceInfoBean p "
                + "WHERE s.province = p.name AND s.stationStatus = 2");

        List<Object> resultList = query.getResultList();

        return resultList;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsBusinessReportStationsList(EntityManager em) {

        Query query = em.createQuery("SELECT s.stationID, s.address, s.city, s.province, p.area, s.businessActive " + "FROM StationBean s, ProvinceInfoBean p "
                + "WHERE s.province = p.name AND s.stationStatus = 2");

        List<Object> resultList = query.getResultList();

        return resultList;
    }

    @SuppressWarnings("unchecked")
    public static List<DWHPartnerDetailBean> findAllDwhPartnerDetail(EntityManager em) {

        Query query = em.createNamedQuery(DWHPartnerDetailBean.FIND_ALL);

        List<DWHPartnerDetailBean> dwhPartnerDetailList = query.getResultList();

        if (dwhPartnerDetailList != null && dwhPartnerDetailList.size() > 0) {
            return dwhPartnerDetailList;
        }

        return dwhPartnerDetailList;
    }

    @SuppressWarnings("unchecked")
    public static List<DWHCategoryEarnDetailBean> findAllDwhCategoryEarnDetailBean(EntityManager em) {

        Query query = em.createNamedQuery(DWHCategoryEarnDetailBean.FIND_ALL);

        List<DWHCategoryEarnDetailBean> dwhCategoryDetailList = query.getResultList();

        if (dwhCategoryDetailList != null && !dwhCategoryDetailList.isEmpty()) {
            return dwhCategoryDetailList;
        }

        return dwhCategoryDetailList;
    }

    @SuppressWarnings("unchecked")
    public static List<DWHCategoryBurnDetailBean> findAllDwhCategoryBurnDetailBean(EntityManager em) {

        Query query = em.createNamedQuery(DWHCategoryBurnDetailBean.FIND_ALL);

        List<DWHCategoryBurnDetailBean> dwhCategoryBurnDetailList = query.getResultList();

        if (dwhCategoryBurnDetailList != null && !dwhCategoryBurnDetailList.isEmpty()) {
            return dwhCategoryBurnDetailList;
        }

        return dwhCategoryBurnDetailList;
    }

    @SuppressWarnings("unchecked")
    public static List<DWHBrandDetailBean> findAllDwhBrandDetailBean(EntityManager em) {

        Query query = em.createNamedQuery(DWHBrandDetailBean.FIND_ALL);

        List<DWHBrandDetailBean> dwhBrandDetailList = query.getResultList();

        if (dwhBrandDetailList != null && !dwhBrandDetailList.isEmpty()) {
            return dwhBrandDetailList;
        }

        return dwhBrandDetailList;
    }

    @SuppressWarnings("unchecked")
    public static List<CRMPromotionBean> findAllCrmPromotions(EntityManager em) {

        Query query = em.createNamedQuery(CRMPromotionBean.SELECT_ALL);

        List<CRMPromotionBean> crmPromotionBeanList = query.getResultList();

        if (crmPromotionBeanList != null && crmPromotionBeanList.size() > 0) {
            return crmPromotionBeanList;
        }

        return crmPromotionBeanList;
    }
    
    @SuppressWarnings("unchecked")
    public static List<CRMSfPromotionsBean> findAllCrmSfPromotions(EntityManager em) {

        Query query = em.createNamedQuery(CRMSfPromotionsBean.SELECT_ALL);

        List<CRMSfPromotionsBean> crmSfPromotionBeanList = query.getResultList();

        return crmSfPromotionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<ParkingTransactionBean> findUserActiveParkingTransaction(EntityManager em, UserBean user) {

        Query query = em.createNamedQuery(ManagerTicketBean.FIND_BY_MANAGERID);
        //query.setParameter("managerid", managerBean);
        //query.setParameter("checkDate", date);
        List<ParkingTransactionBean> parkingTransactionBeanList = query.getResultList();

        return parkingTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static ParkingTransactionBean findParkingTransactionByParkingTransactionId(EntityManager em, UserBean user, String parkingTransactionId) {

        ParkingTransactionBean parkingTransactionBean = null;

        Query query = em.createNamedQuery(ParkingTransactionBean.FIND_BY_PARKING_TRANSACTION_ID);
        query.setParameter("parkingTransactionId", parkingTransactionId);
        query.setParameter("userBean", user);

        List<ParkingTransactionBean> parkingTransactionBeanList = query.getResultList();

        if (parkingTransactionBeanList != null && parkingTransactionBeanList.size() > 0) {
            parkingTransactionBean = (ParkingTransactionBean) parkingTransactionBeanList.get(0);
        }

        return parkingTransactionBean;

    }

    @SuppressWarnings("unchecked")
    public static List<ParkingTransactionItemBean> findParkingTransactionItemByTransactionId(EntityManager em, String parkingTransactionId, String status) {

        Query query = em.createNamedQuery(ParkingTransactionItemBean.FIND_UNSETTLED_TRANSACTION_ITEM_BY_PARKING_TRANSACTION);
        query.setParameter("parkingTransactionId", parkingTransactionId);
        query.setParameter("status", status);

        List<ParkingTransactionItemBean> parkingTransactionItemBeanList = query.getResultList();

        return parkingTransactionItemBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<ParkingTransactionItemBean> findActiveParkingTransactionItemByTransactionId(EntityManager em, String parkingTransactionId, String status) {

        Query query = em.createNamedQuery(ParkingTransactionItemBean.FIND_ACTIVE_TRANSACTION_ITEM_BY_PARKING_TRANSACTION);
        query.setParameter("parkingTransactionId", parkingTransactionId);
        query.setParameter("status", status);

        List<ParkingTransactionItemBean> parkingTransactionItemBeanList = query.getResultList();

        return parkingTransactionItemBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<ParkingTransactionItemBean> findActiveParkingTransactionItemByUser(EntityManager em, UserBean user, String parkingTransactionStatus1,
            String parkingTransactionStatus2, String parkingTransactionStatus3) {

        Query query = em.createNamedQuery(ParkingTransactionItemBean.FIND_ACTIVE_TRANSACTION_ITEM_BY_USER);
        query.setParameter("userBean", user);
        query.setParameter("status1", parkingTransactionStatus1);
        query.setParameter("status2", parkingTransactionStatus2);
        query.setParameter("status3", parkingTransactionStatus3);

        List<ParkingTransactionItemBean> parkingTransactionItemBeanList = query.getResultList();

        return parkingTransactionItemBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<ParkingTransactionBean> findActiveParkingTransaction(EntityManager em, UserBean userBean) {

        Query query = em.createNamedQuery(ParkingTransactionBean.FIND_ACTIVE_BY_USER);
        query.setParameter("userBean", userBean);
        query.setParameter("statusStarted", PARKING_TRANSACTION_STATUS.STARTED.getValue());
        query.setParameter("statusEnded", PARKING_TRANSACTION_STATUS.ENDED.getValue());
        //query.setParameter("statusFailed",  PARKING_TRANSACTION_STATUS.FAILED.getValue());

        List<ParkingTransactionBean> parkingTransactionBeanList = query.getResultList();

        return parkingTransactionBeanList;

    }

    @SuppressWarnings("unchecked")
    public static List<ParkingTransactionBean> findParkingTransactionToBeClosed(EntityManager em, Integer maxIntervalPendingParkingTransaction) {

        Date now = new Date();
        Date maxEstimatedParkingEndTime = new Date(now.getTime() - maxIntervalPendingParkingTransaction);

        System.out.println("maxEstimatedParkingEndTime: " + maxEstimatedParkingEndTime);

        Query query = em.createNamedQuery(ParkingTransactionBean.FIND_TRANSACTION_TO_BE_CLOSED);
        query.setParameter("status", PARKING_TRANSACTION_STATUS.STARTED.getValue());
        query.setParameter("maxEstimatedParkingEndTime", maxEstimatedParkingEndTime);

        List<ParkingTransactionBean> parkingTransactionBeanList = query.getResultList();

        return parkingTransactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<ParkingTransactionBean> findParkingTransactionToBeReconciled(EntityManager em, String status) {

        Query query = em.createNamedQuery(ParkingTransactionBean.FIND_TRANSACTION_TO_BE_RECONCILED);
        query.setParameter("status", status);

        List<ParkingTransactionBean> parkingTransactionBeanList = query.getResultList();

        return parkingTransactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<ParkingTransactionBean> findParkingTransactionsByUserBeanAndDate(EntityManager em, UserBean userBean, Date startDate, Date endDate, int itemForPage,
            Boolean detail) {

        Query query = null;

        query = em.createNamedQuery(ParkingTransactionBean.FIND_SUCCESSFUL_BY_USER_AND_DATE);
        query.setParameter("userBean", userBean);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        if (detail && itemForPage >= 0) {
            query.setMaxResults(itemForPage);
        }

        List<ParkingTransactionBean> parkingTransactionBeanList = query.getResultList();

        return parkingTransactionBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<StationStageBean> findStationStageByRequest(EntityManager em, String requestID) {

        Query query = em.createNamedQuery(StationStageBean.FIND_BY_REQUEST);
        query.setParameter("requestID", requestID);
        query.setMaxResults(1);

        List<StationStageBean> stationStageBeanList = query.getResultList();

        return stationStageBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<StationStageBean> findStationStageToProcessByRequest(EntityManager em, String requestID) {

        Query query = em.createNamedQuery(StationStageBean.FIND_BY_NOT_PROCESSED_AND_REQUEST);
        query.setParameter("requestID", requestID);

        List<StationStageBean> stationStageBeanList = query.getResultList();

        return stationStageBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<StationStageBean> findStationStageToProcess(EntityManager em) {

        Query query = em.createNamedQuery(StationStageBean.FIND_BY_NOT_PROCESSED);

        List<StationStageBean> stationStageBeanList = query.getResultList();

        return stationStageBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<StationBean> findStationAllAppActive(EntityManager em) {

        Query query = em.createNamedQuery(StationBean.FIND_ALL_APP_ACTIVE);

        List<StationBean> stationBeanList = query.getResultList();

        return stationBeanList;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsFuelQuantity(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTransactionsFuelQuantity(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result = (Double)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsHistoryFuelQuantity(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTransactionsHistoryFuelQuantity(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result = (Double)query.getSingleResult();

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsFuelQuantity(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_TOTAL_FUEL_QUANTITY);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTransactionsFuelQuantity(EntityManager em, Date dailyEndDate, Date totalStartDate, 
    		String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_TOTAL_FUEL_QUANTITY);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        Double result = (Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsHistoryFuelQuantity(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_TOTAL_FUEL_QUANTITY);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY);
            query.setParameter("dailyStartDate", dailyStartDate);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("weeklyStartDate", weeklyStartDate);
            query.setParameter("weeklyEndDate", weeklyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantity(EntityManager em, Date dailyEndDate, Date totalStartDate, 
    		String source, String refuelMode) {

        Query query = null;

        if (refuelMode != null) {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_TOTAL_FUEL_QUANTITY);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
            query.setParameter("refuelMode", refuelMode);
        }
        else {
            query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY);
            query.setParameter("dailyEndDate", dailyEndDate);
            query.setParameter("totalStartDate", totalStartDate);
            query.setParameter("source", source);
        }

        Double result = (Double) query.getSingleResult();
        
        return result;
    }


    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisEventNotificationTotalLoyaltyFuelQuantity(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllEventNotificationTotalLoyaltyFuelQuantity(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalLoyaltyFuelQuantity(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTotalLoyaltyFuelQuantity(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalLoyaltyHistoryFuelQuantity(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryFuelQuantity(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalLoyaltyFuelQuantity(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTotalLoyaltyFuelQuantity(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalLoyaltyHistoryFuelQuantity(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryFuelQuantity(EntityManager em, Date dailyEndDate, Date totalStartDate) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static ResetPinCodeBean findValidResetPinCode(EntityManager em, String code, UserBean userBean) {

        Date now = new Date();

        Query query = em.createNamedQuery(ResetPinCodeBean.FIND);
        query.setParameter("code", code);
        query.setParameter("userBean", userBean);
        query.setParameter("now", now);
        query.setParameter("status", ResetPinCodeBean.STATUS_NEW);

        query.setMaxResults(1);

        List<ResetPinCodeBean> resetPinCodeBeanList = query.getResultList();

        if (resetPinCodeBeanList != null && resetPinCodeBeanList.size() > 0) {
            return resetPinCodeBeanList.get(0);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static CityInfoBean findCityByNameAndProvince(EntityManager em, String name, String province) {
        CityInfoBean cityInfoBean = null;

        Query query = em.createNamedQuery(CityInfoBean.FIND_BY_NAME_AND_PROVINCE);
        query.setParameter("name", name);
        query.setParameter("province", province);
        query.setMaxResults(1);

        List<CityInfoBean> CityInfoBeanList = query.getResultList();

        if (CityInfoBeanList != null && CityInfoBeanList.size() > 0) {
            cityInfoBean = (CityInfoBean) CityInfoBeanList.get(0);
        }
        else {
            cityInfoBean = null;
        }

        return cityInfoBean;
    }

    @SuppressWarnings("unchecked")
    public static TicketBean findTicketBusinessById(EntityManager em, String ticketId) {

        TicketBean ticketBean = null;

        Query query = em.createNamedQuery(TicketBean.FIND_BUSINESS_BY_TICKETID);

        query.setParameter("ticketId", ticketId);
        query.setMaxResults(1);

        List<TicketBean> ticketBeanList = query.getResultList();

        if (ticketBeanList != null && ticketBeanList.size() > 0) {
            ticketBean = (TicketBean) ticketBeanList.get(0);
        }
        else {
            ticketBean = null;
        }

        return ticketBean;

    }

    @SuppressWarnings("unchecked")
    public static List<RefuelingSubscriptionsBean> findRefuelingSubscriptionToReconcilie(EntityManager em) {

        Query query = em.createNamedQuery(RefuelingSubscriptionsBean.FIND_TO_RECONCILIE);

        List<RefuelingSubscriptionsBean> refuelingSubscriptionsBeanList = query.getResultList();

        if (refuelingSubscriptionsBeanList != null && refuelingSubscriptionsBeanList.size() > 0) {
            return refuelingSubscriptionsBeanList;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static int statisticsBOPrepaidTransactions(EntityManager em, Date startDate, Date endDate) {

        int result = 0;

        Query query = em.createQuery("SELECT COUNT(t) from TransactionBean t WHERE t.finalStatusType = :finalStatusType AND t.finalAmount > 0 "
                + "AND t.creationTimestamp >= :startDate AND t.creationTimestamp <= :endDate");
        query.setParameter("finalStatusType", StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<Object> resultList = query.getResultList();

        if (resultList != null && resultList.size() > 0) {
            Long resultLong = (Long) resultList.get(0);
            result = resultLong.intValue();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public static int statisticsBORefuelingTransactions(EntityManager em, Date startDate, Date endDate) {

        int result = 0;

        Query query = em.createQuery("SELECT COUNT(t) from TransactionBean t WHERE t.srcTransactionID is not null AND t.finalStatusType = :finalStatusType AND t.finalAmount > 0 "
                + "AND t.creationTimestamp >= :startDate AND t.creationTimestamp <= :endDate");
        query.setParameter("finalStatusType", StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<Object> resultList = query.getResultList();

        if (resultList != null && resultList.size() > 0) {
            Long resultLong = (Long) resultList.get(0);
            result = resultLong.intValue();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public static int statisticsBOPostPaidTransactions(EntityManager em, Date startDate, Date endDate) {

        int result = 0;

        Query query = em.createQuery("SELECT COUNT(p) from PostPaidTransactionBean p WHERE p.mpTransactionStatus = :mpTransactionStatus AND p.amount > 0 "
                + "AND p.creationTimestamp >= :startDate AND p.creationTimestamp <= :endDate");
        query.setParameter("mpTransactionStatus", StatusHelper.POST_PAID_FINAL_STATUS_PAID);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<Object> resultList = query.getResultList();

        if (resultList != null && resultList.size() > 0) {
            Long resultLong = (Long) resultList.get(0);
            result = resultLong.intValue();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public static int statisticsBOCustomerUsers(EntityManager em, Date startDate, Date endDate) {

        int result = 0;

        Query query = em.createQuery("SELECT COUNT(u) from UserBean u WHERE (u.userType = 1 or u.userType = 8) AND u.userStatus = 2 and u.userStatusRegistrationCompleted = 1 "
                + "AND u.userStatusRegistrationTimestamp >= :startDate AND u.userStatusRegistrationTimestamp <= :endDate");
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<Object> resultList = query.getResultList();

        if (resultList != null && resultList.size() > 0) {
            Long resultLong = (Long) resultList.get(0);
            result = resultLong.intValue();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public static int statisticsBOBusinessUsers(EntityManager em, Date startDate, Date endDate) {

        int result = 0;

        Query query = em.createQuery("SELECT COUNT(u) from UserBean u WHERE u.userType = 9 AND u.userStatus = 2 and u.userStatusRegistrationCompleted = 1 "
                + "AND u.userStatusRegistrationTimestamp >= :startDate AND u.userStatusRegistrationTimestamp <= :endDate");
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        List<Object> resultList = query.getResultList();

        if (resultList != null && resultList.size() > 0) {
            Long resultLong = (Long) resultList.get(0);
            result = resultLong.intValue();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public static UserBean getUserService(EntityManager em) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(UserBean.FIND_BY_USER_TYPE);
        query.setParameter("userType", User.USER_TYPE_SERVICE);

        List<UserBean> userBeanList = query.getResultList();

        if (userBeanList != null && userBeanList.size() > 0) {
            userBean = userBeanList.get(0);
        }

        return userBean;
    }

    @SuppressWarnings("unchecked")
    public static int countVoucherByPromoCodeAndUserFiscalCode(EntityManager em, String fiscalCode, String promoCode) {

        int result = 0;
        
        Query query = em.createQuery("SELECT COUNT(v) from VoucherBean v WHERE v.promoCode = :promoCode AND v.userBean.personalDataBean.fiscalCode = :fiscalCode");
        query.setParameter("promoCode", promoCode);
        query.setParameter("fiscalCode", fiscalCode);

        List<Object> resultList = query.getResultList();
        
        if (resultList != null && resultList.size() > 0) {
            Long resultLong = (Long) resultList.get(0);
            result = resultLong.intValue();
        }

        return result;

    }

    @SuppressWarnings("unchecked")
    public static ShopTransactionDataBean findShopTransactionBeanDataByTransactionId(EntityManager em, String transactionId) {

        ShopTransactionDataBean shopTransactionDataBean = null;

        Query query = em.createNamedQuery(ShopTransactionDataBean.FIND_BY_TRANSACTIONID);

        query.setParameter("transactionId", transactionId);
        query.setMaxResults(1);

        List<ShopTransactionDataBean> shopTransactionDataBeanList = query.getResultList();

        if (shopTransactionDataBeanList != null && shopTransactionDataBeanList.size() > 0) {
            shopTransactionDataBean = (ShopTransactionDataBean) shopTransactionDataBeanList.get(0);
        }
        else {
            shopTransactionDataBean = null;
        }

        return shopTransactionDataBean;

    }

    @SuppressWarnings("unchecked")
    public static UserBean findUserByFiscalCode(EntityManager em, String fiscalCode) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(UserBean.FIND_BY_FISCALCODE);

        query.setParameter("fiscalCode", fiscalCode);
        query.setMaxResults(1);

        List<UserBean> userBeanList = query.getResultList();

        if (userBeanList != null && userBeanList.size() > 0) {
            userBean = (UserBean) userBeanList.get(0);
        }
        else {
            userBean = null;
        }

        return userBean;

    }

    @SuppressWarnings("unchecked")
    public static UserBean findUserByEmail(EntityManager em, String email) {

        UserBean userBean = null;

        Query query = em.createNamedQuery(UserBean.FIND_BY_EMAIL);

        query.setParameter("email", email);
        query.setMaxResults(1);

        List<UserBean> userBeanList = query.getResultList();

        if (userBeanList != null && userBeanList.size() > 0) {
            userBean = (UserBean) userBeanList.get(0);
        }
        else {
            userBean = null;
        }

        return userBean;

    }

    @SuppressWarnings("unchecked")
    public static PaymentInfoBean findMulticardPaymentInfoBeanByUserBean(EntityManager em, UserBean userBean) {

        PaymentInfoBean paymentInfoBean = null;

        Query query = em.createNamedQuery(PaymentInfoBean.FIND_PAYMENT_BY_USER);

        query.setParameter("userBean", userBean);
        query.setParameter("type", PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);

        query.setMaxResults(1);

        List<PaymentInfoBean> paymentInfoBeanList = query.getResultList();

        if (paymentInfoBeanList != null && paymentInfoBeanList.size() > 0) {
            paymentInfoBean = (PaymentInfoBean) paymentInfoBeanList.get(0);
        }
        else {
            paymentInfoBean = null;
        }

        return paymentInfoBean;

    }
    
    @SuppressWarnings("unchecked")
    public static List<PaymentInfoBean> findMulticardPaymentInfoListByUserBean(EntityManager em, UserBean userBean) {

        Query query = em.createNamedQuery(PaymentInfoBean.FIND_PAYMENT_BY_USER);

        query.setParameter("userBean", userBean);
        query.setParameter("type", PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);

        List<PaymentInfoBean> paymentInfoBeanList = query.getResultList();

        return paymentInfoBeanList;

    }

    @SuppressWarnings("unchecked")
    public static UserExternalDataBean findExternalUserByUUID(EntityManager em, String uuid, String provider) {

        Query query = em.createNamedQuery(UserExternalDataBean.FIND_EXTERNAL_USER_BY_UUID_AND_PROVIDER);

        query.setParameter("uuid", uuid);
        query.setParameter("provider", provider);

        List<UserExternalDataBean> result = query.getResultList();
        if (result.size() == 0) {
            System.out.println("UserExternalDataBean not found");
            return null;
        }
        else if (result.size() > 1)
            throw new NonUniqueResultException();

        UserExternalDataBean userExternalDataBean = (UserExternalDataBean) result.get(0);
        System.out.println("UserExternalDataBean found");
        return userExternalDataBean;

    }
    
    @SuppressWarnings("unchecked")
    public static PromoCodeModeBean findByModeAndPromoCode(EntityManager em, String mode, String promoCode) {

        PromoCodeModeBean promoCodeModeBean = null;

        Query query = em.createNamedQuery(PromoCodeModeBean.FIND_BY_MODE_AND_PROMOCODE);

        query.setParameter("mode", mode);
        query.setParameter("promoCode", promoCode);
        query.setMaxResults(1);

        List<PromoCodeModeBean> promoCodeModeBeanList = query.getResultList();

        if (promoCodeModeBeanList != null && promoCodeModeBeanList.size() > 0) {
            promoCodeModeBean = (PromoCodeModeBean) promoCodeModeBeanList.get(0);
        }
        else {
            promoCodeModeBean = null;
        }

        return promoCodeModeBean;

    }
    
    @SuppressWarnings("unchecked")
    public static Map<String,String> statisticsReportSynthesisFindForType(EntityManager em, String idRif, String typeExtraction) {

        Query query = null;
        
        query = em.createNamedQuery(StatisticalReportsBean.STATISTICS_REPORT_SYNTHESIS_FIND_REPORT_FOR_TYPE, StatisticalReportsBean.class);
        query.setParameter("typeExtraction", typeExtraction);
        query.setParameter("idRif", idRif);
        
        List<StatisticalReportsBean> result = query.getResultList();
        
        
        Map<String,String> mapStatReport = new HashMap<>();
        for (StatisticalReportsBean ele:result){
        	mapStatReport.put(ele.getChiave(), ele.getValore());
        }

        return mapStatReport;
    }
    
    @SuppressWarnings("unchecked")
    public static List<StatisticalReportsBean> statisticsReportFindForType(EntityManager em, String idRif, String typeExtraction) {

        Query query = null;
        
        query = em.createNamedQuery(StatisticalReportsBean.STATISTICS_REPORT_SYNTHESIS_FIND_REPORT_FOR_TYPE, StatisticalReportsBean.class);
        query.setParameter("typeExtraction", typeExtraction);
        query.setParameter("idRif", idRif);
        
        List<StatisticalReportsBean> result = query.getResultList();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsFuelQuantityByProduct(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTransactionsFuelQuantityByProduct(EntityManager em, Date dailyEndDate, 
    		Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();

        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTransactionsHistoryFuelQuantityByProduct(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTransactionsHistoryFuelQuantityByProduct(EntityManager em, Date dailyEndDate,
            Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsFuelQuantityByProduct(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = null;

        query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTransactionsFuelQuantityByProduct(EntityManager em, Date dailyEndDate, 
    		Date totalStartDate, String productDescription) {

        Query query = null;

        query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTransactionsHistoryFuelQuantityByProduct(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = null;

        query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantityByProduct(EntityManager em, Date dailyEndDate,
            Date totalStartDate, String productDescription) {

        Query query = null;

        query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisEventNotificationTotalLoyaltyFuelQuantityByProduct(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllEventNotificationTotalLoyaltyFuelQuantityByProduct(EntityManager em, Date dailyEndDate, 
    		Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(EventNotificationBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalLoyaltyFuelQuantityByProduct(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate,String productDescription) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTotalLoyaltyFuelQuantityByProduct(EntityManager em, Date dailyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPostPaidTotalLoyaltyHistoryFuelQuantityByProduct(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryFuelQuantityByProduct(EntityManager em, Date dailyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(PostPaidTransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalLoyaltyFuelQuantityByProduct(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTotalLoyaltyFuelQuantityByProduct(EntityManager em, Date dailyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<Object> statisticsReportSynthesisPrePaidTotalLoyaltyHistoryFuelQuantityByProduct(EntityManager em, Date dailyStartDate, Date dailyEndDate, Date weeklyStartDate,
            Date weeklyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyStartDate", dailyStartDate);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("weeklyStartDate", weeklyStartDate);
        query.setParameter("weeklyEndDate", weeklyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {

            return Arrays.asList((Object[]) resultList.get(0));
        }

        ArrayList<Object> zeroList = new ArrayList<Object>();
        zeroList.add(0);
        zeroList.add(0);
        zeroList.add(0);

        return zeroList;
    }
    
    public static Double statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryFuelQuantityByProduct(EntityManager em, Date dailyEndDate, Date totalStartDate, String productDescription) {

        Query query = em.createNamedQuery(TransactionHistoryBean.STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT);
        query.setParameter("dailyEndDate", dailyEndDate);
        query.setParameter("totalStartDate", totalStartDate);
        query.setParameter("productDescription", productDescription);

        Double result =(Double) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<EsPromotionBean> findEsPromotionByFiscalCode(EntityManager em, String fiscalCode, Date firstDayOfMonth){
    	Query query = em.createNamedQuery(EsPromotionBean.FIND_BY_FISCALCODE);
    	query.setParameter("fiscalCode", fiscalCode);
    	query.setParameter("firstDayOfMonth", firstDayOfMonth);
    	
    	List<EsPromotionBean> listPromotion = query.getResultList();
    	
    	return listPromotion;
    }
    
    @SuppressWarnings("unchecked")
    public static List<EsPromotionBean> findEsPromotionForCreateVoucher(EntityManager em, String voucherCode, Date firstDayOfMonth, Date lastDayOfMonth){
        
        Query query = null;
        if (voucherCode != null) {
            query = em.createNamedQuery(EsPromotionBean.FIND_BY_CURRENT_MONTH_VOUCHER_CODE);
            query.setParameter("voucherCode", voucherCode);
        }
        else {
            query = em.createNamedQuery(EsPromotionBean.FIND_BY_CURRENT_MONTH_WITH_NULL_VOUCHER_CODE);
        }
    	query.setParameter("firstDayOfMonth", firstDayOfMonth);
    	query.setParameter("lastDayOfMonth", lastDayOfMonth);
    	
    	List<EsPromotionBean> listPromotion = query.getResultList();
    	
    	return listPromotion;
    }
    
    @SuppressWarnings("unchecked")
    public static List<EsPromotionBean> findEsPromotionToBeProcessed(EntityManager em){
        
        Query query = null;
        query = em.createNamedQuery(EsPromotionBean.FIND_TO_BE_PROCESSED);
        
        List<EsPromotionBean> listPromotion = query.getResultList();
        
        return listPromotion;
    }
    
    @SuppressWarnings("unchecked")
    public static EsPromotionBean findEsPromotionToBeProcessedWithNullVoucherCodeByDate(EntityManager em, UserBean userBean, Date checkDate){
        
        EsPromotionBean esPromotionBean = null;
        
        Query query = null;
        query = em.createNamedQuery(EsPromotionBean.FIND_TO_BE_PROCESSED_WITH_NULL_VOUCHER_CODE_BY_DATE);
        query.setParameter("userBean", userBean);
        query.setParameter("checkDate", checkDate);
        
        query.setMaxResults(1);

        List<EsPromotionBean> esPromotionBeanList = query.getResultList();

        if (esPromotionBeanList != null && esPromotionBeanList.size() > 0) {
            esPromotionBean = (EsPromotionBean) esPromotionBeanList.get(0);
        }
        else {
            esPromotionBean = null;
        }

        return esPromotionBean;
    }
    
    public static Integer findAmountRefuelForPeriodPrepaid(EntityManager em, long userId, Date periodStartDate, Date periodEndDate) {

        Query query = em.createNamedQuery(TransactionBean.AMOUNT_REFUEL_FOR_PERIOD);
        query.setParameter("userId", userId);
        query.setParameter("periodStartDate", periodStartDate);
        query.setParameter("periodEndDate", periodEndDate);

        Integer result =(Integer) query.getSingleResult();
        
        return result;
    }
    
    public static Integer findAmountRefuelForPeriodPostpaid(EntityManager em, long userId, Date periodStartDate, Date periodEndDate) {

        Query query = em.createNamedQuery(PostPaidTransactionBean.AMOUNT_REFUEL_FOR_PERIOD);
        query.setParameter("userId", userId);
        query.setParameter("periodStartDate", periodStartDate);
        query.setParameter("periodEndDate", periodEndDate);

        Integer result =(Integer) query.getSingleResult();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<VoucherPromotionalCrmSfBean> findCRMSfVoucherPromotionalByRequestId(EntityManager em, String sourceRequestId){
    	Query query = em.createNamedQuery(VoucherPromotionalCrmSfBean.SELECT_BY_REQUEST_ID);
    	query.setParameter("sourceRequestId", sourceRequestId);
    	
    	List<VoucherPromotionalCrmSfBean> result = query.getResultList();
    	
    	return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<VoucherPromotionalCrmSfBean> findCRMSfVoucherPromotionalByOperationId(EntityManager em, String sourceOperationId){
    	Query query = em.createNamedQuery(VoucherPromotionalCrmSfBean.SELECT_BY_OPERATION_ID);
    	query.setParameter("sourceOperationId", sourceOperationId);
    	
    	List<VoucherPromotionalCrmSfBean> result = query.getResultList();
    	
    	return result;
    }
    
    public static CRMSfTokenBean findTokenCrmSf(EntityManager em){
    	Query query = em.createNamedQuery(CRMSfTokenBean.SELECT_TOKEN ,CRMSfTokenBean.class);
    	
    	List<CRMSfTokenBean> result = query.getResultList();
    	
    	if(result.size()>0)
    		return result.get(0);
    	else
    		return null;
    }
    
    @SuppressWarnings("unchecked")
    public static List<CRMSfOutboundBean> findCRMSfOutboundDeliveryStatus(EntityManager em, String deliveryStatus, Integer maxResult) {

        Query query = em.createNamedQuery(CRMSfOutboundBean.FIND_DELIVERY_STATUS);
        query.setParameter("deliveryStatus", deliveryStatus);

        if (maxResult != null) {
            query.setMaxResults(maxResult);
        }

        List<CRMSfOutboundBean> crmOutboundList = query.getResultList();

        return crmOutboundList;
    }
    
    @SuppressWarnings("unchecked")
    public static List<CRMSfOutboundBean> getAllCRMSfOutbound(EntityManager em, boolean onlyOneRow) {

        Query query = em.createNamedQuery(CRMSfOutboundBean.SELECT_ALL);

        if (onlyOneRow) {
            query.setMaxResults(1);
        }

        List<CRMSfOutboundBean> resultSet = query.getResultList();

        return resultSet;
    }
    
    @SuppressWarnings("unchecked")
    public static int getCRMSfOutboundRowsCount(EntityManager em) {

        Query query = em.createNamedQuery(CRMSfOutboundBean.ROWS_COUNT);

        List<Object> resultSet = query.getResultList();
        int rowsCount = 0;

        if (resultSet != null && !resultSet.isEmpty()) {
            rowsCount = Integer.parseInt(resultSet.get(0).toString());
        }

        return rowsCount;
    }
    
    public static CRMSfContactKeyBean findContactKeyBeanByUser(EntityManager em, long userId){
    	Query query = em.createNamedQuery(CRMSfContactKeyBean.SELECT_BY_USER ,CRMSfContactKeyBean.class);
    	query.setParameter("userId", userId);
    	
    	List<CRMSfContactKeyBean> result = query.getResultList();
    	
    	if(result.size()>0)
    		return result.get(0);
    	else
    		return null;
    }
    
      
    public static Integer countPendingTransactions(EntityManager em, String serverName){
        Query query = em.createNamedQuery(TransactionBean.COUNT_PENDING_TRANSACTION ,TransactionBean.class);
        query.setParameter("serverName", serverName);
        
        List<TransactionBean> transactionBeanList = query.getResultList();
         
        return transactionBeanList.size();
    }
}
