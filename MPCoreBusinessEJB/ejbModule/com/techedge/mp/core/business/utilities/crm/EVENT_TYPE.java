package com.techedge.mp.core.business.utilities.crm;

public enum EVENT_TYPE {
	
    ISCRIZIONE_NUOVO_CLIENTE("1"), ACCREDITO_PUNTI_LOYALTY_NFC_TOTP("2"), ESECUZIONE_RIFORNIMENTO_APP("3"), ASSOCIAZIONE_CARTA_PAGAMENTO("4"), ASSOCIAZIONE_CARTA_LOYALTY("5");

    private final String name;

    private EVENT_TYPE(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {

        return name.equals(otherName);
    }

    public String getValue() {
        return this.name;
    }

}
