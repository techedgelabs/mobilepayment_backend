package com.techedge.mp.core.business.utilities;

import java.io.IOException;
import java.io.Serializable;

import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.json.JSONException;
import org.json.JSONObject;

import com.techedge.mp.core.business.UserV2Service;
import com.techedge.mp.core.business.exceptions.SocialAuthenticationException;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.SocialUserAccountData;

/**
 * Verifica dell'integrit� dell'accessToken fornito dall'autenticazione da
 * client mobile per Social Provider Facebook, Google (OAuth 2.0 process chain)
 * 
 * 
 * 
 */
public class SocialAuthenticationHelper implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    public static enum PROVIDER {
        FACEBOOK("facebook"), GOOGLE("google");
        String val;

        private PROVIDER(String value) {
            val = value;
        }

        public static PROVIDER getEnumVal(String val) {
            return PROVIDER.valueOf(val);
        }

    };

    public static SocialUserAccountData executeSocialAuthentication(String socialProvider, String accessToken, String proxyHost, String proxyPort, String proxyNoHosts,
            String deviceType) throws SocialAuthenticationException, IOException {

        SocialUserAccountData socialUserAccountData = null;

        try {
            SocialAuthenticationHelper.PROVIDER provider = SocialAuthenticationHelper.PROVIDER.getEnumVal(socialProvider);

            String _profileUri = null;
            OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
            OAuthClientRequest bearerClientRequest = null;
            OAuthResourceResponse resourceResponse = null;
            SocialAuthenticationException socialAuthenticationException = null;
            JSONObject json = null;

            Proxy proxy = new Proxy(proxyHost, proxyPort, proxyNoHosts);

            switch (provider) {

                case FACEBOOK:

                    System.out.println("Autenticazione con Facebook");

                    _profileUri = UserV2Service.facebookProfileUri;
                    String facebookCheckTokenUri = UserV2Service.facebookCheckTokenUriPrefix.concat(accessToken).concat(
                            "&access_token=" + UserV2Service.fbAppId + "|" + UserV2Service.fbAppSecret);
                    bearerClientRequest = new OAuthBearerClientRequest(facebookCheckTokenUri).setAccessToken(accessToken).buildHeaderMessage();

                    System.out.println("Invio richiesta autenticazione");

                    proxy.setHttp();

                    resourceResponse = oAuthClient.resource(bearerClientRequest, OAuth.HttpMethod.GET, OAuthResourceResponse.class);

                    System.out.println("ResponseCode: " + resourceResponse.getResponseCode());

                    if (resourceResponse.getResponseCode() == 200) {

                        bearerClientRequest = new OAuthBearerClientRequest(_profileUri).setAccessToken(accessToken).buildHeaderMessage();

                        resourceResponse = oAuthClient.resource(bearerClientRequest, OAuth.HttpMethod.GET, OAuthResourceResponse.class);

                        json = new JSONObject(resourceResponse.getBody());

                        proxy.unsetHttp();

                    }
                    else {

                        proxy.unsetHttp();

                        throw new SocialAuthenticationException(ResponseHelper.USER_CREATE_INVALID_TOKEN,
                                "Error in authentication :: The App_id in the input_token did not match the Viewing App");
                    }
                    break;

                case GOOGLE:

                    //throw new OAuthSystemException();
                    System.out.println("Autenticazione con Google");

                    _profileUri = UserV2Service.googleProfileUri;
                    String googleCheckTokenUri = UserV2Service.googleCheckTokenUriPrefix.concat(accessToken);
                    //System.out.println("googleCheckTokenUri: " + googleCheckTokenUri);
                    bearerClientRequest = new OAuthBearerClientRequest(googleCheckTokenUri).setAccessToken(accessToken).buildBodyMessage(); //.setAccessToken(accessToken).buildHeaderMessage();

                    System.out.println("Invio richiesta autenticazione");

                    proxy.setHttp();

                    resourceResponse = oAuthClient.resource(bearerClientRequest, OAuth.HttpMethod.GET, OAuthResourceResponse.class);

                    System.out.println("ResponseCode: " + resourceResponse.getResponseCode());

                    if (resourceResponse.getResponseCode() == 200) {

                        json = new JSONObject(resourceResponse.getBody());
                        System.out.println("json data :: " + json.toString());
                        String jsonAppID = json.getString("aud");
                        String GOOGLE_APP_ID = null;
                        if (deviceType.startsWith("IPH"))
                            GOOGLE_APP_ID = UserV2Service.googleClientIdIOS;
                        else
                            GOOGLE_APP_ID = UserV2Service.googleClientIdAND;
                        System.out.println("Checking Token for device "+(deviceType.startsWith("AND") ? "android " : "ios ")+"==> GOOGLE CLIENT ID :: "+GOOGLE_APP_ID);
                        if (!jsonAppID.equals(GOOGLE_APP_ID))
                            throw new SocialAuthenticationException(ResponseHelper.USER_CREATE_INVALID_TOKEN,
                                    "Error in authentication :: The App_id in the input_token did not match the Viewing App");

                        proxy.unsetHttp();

                    }
                    else {

                        json = new JSONObject(resourceResponse.getBody());
                        System.out.println("json data :: " + json.toString());
                        
                        proxy.unsetHttp();

                        throw new SocialAuthenticationException(ResponseHelper.USER_CREATE_INVALID_TOKEN, "INVALID TOKEN");
                    }

                    break;

                default:

                    proxy.unsetHttp();

                    throw new SocialAuthenticationException();
            }

            System.out.println("json: " + json.toString());

            if (resourceResponse.getResponseCode() != 200) {

                socialAuthenticationException = new SocialAuthenticationException();
                if (json.has("error")) {
                    socialAuthenticationException.setErrorMessage(json.getJSONObject("error").getString("message"));
                    socialAuthenticationException.setErrCode(json.getJSONObject("error").getInt("code"));

                }
                else {
                    socialAuthenticationException.setErrCode(resourceResponse.getResponseCode());
                    socialAuthenticationException.setErrorMessage("Unknow Error!");

                }
                throw socialAuthenticationException;
            }

            String userEmail = null;
            try {
                userEmail = json.getString("email");
            }
            catch (Exception e1) {

            }
            String userId = json.getString(provider.equals(SocialAuthenticationHelper.PROVIDER.FACEBOOK) ? "id" : "sub");
            String name = null;
            try {
                name = json.getString("name");
            }
            catch (Exception e1) {

            }
            String firstName = null;
            try {
                firstName = json.getString(provider.equals(SocialAuthenticationHelper.PROVIDER.FACEBOOK) ? "first_name" : "given_name");
            }
            catch (Exception e1) {

            }
            String lastName = null;
            try {
                lastName = json.getString(provider.equals(SocialAuthenticationHelper.PROVIDER.FACEBOOK) ? "last_name" : "family_name");
            }
            catch (Exception e1) {

            }

            socialUserAccountData = new SocialUserAccountData();
            socialUserAccountData.setUserAccountId(userId);
            socialUserAccountData.setEmail(userEmail);
            socialUserAccountData.setName(name);
            socialUserAccountData.setFirstName(firstName);
            socialUserAccountData.setLastName(lastName);

            if (provider.equals(SocialAuthenticationHelper.PROVIDER.FACEBOOK)) {
                String gender = null;
                try {
                    gender = json.getString("gender");
                }
                catch (Exception e) {

                }
                
                String birthday = null;
                try {
                    birthday = json.getString("birthday");
                }
                catch (Exception e) {

                }
                String homeTown = null;
                try {
                    homeTown = json.getJSONObject("hometown").getString("name");
                }
                catch (Exception e) {

                }
                socialUserAccountData.setGender(gender);
                socialUserAccountData.setBirthday(birthday);
                socialUserAccountData.setHomeTown(homeTown);
            }

        }
        catch (OAuthProblemException | OAuthSystemException ex) {
            throw new SocialAuthenticationException(null, ex.getMessage());
        }
        catch (JSONException jsonException) {
            throw new SocialAuthenticationException(null, jsonException.getMessage());
        }
        return socialUserAccountData;
    }

}
