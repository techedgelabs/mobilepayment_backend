package com.techedge.mp.core.business.utilities;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.EventNotificationService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.ParkingTransactionV2Service;
import com.techedge.mp.core.business.ReconciliationService;
import com.techedge.mp.core.business.StationServiceRemote;
import com.techedge.mp.core.business.TransactionService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.document.encoder.business.DocumentServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.AuthorizationPlusServiceRemote;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.GSServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

public class EJBHomeCache {

    final String                                 emailSenderLocalJndi                   = "java:global/MPEmailSenderEAR/MPEmailSenderEJB/EmailSender!com.techedge.mp.email.sender.business.EmailSenderRemote";
    final String                                 loggerServiceJndi                      = "java:app/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerService";
    final String                                 parametersServiceJndi                  = "java:app/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersService";
    final String                                 gsServiceRemoteJndi                    = "java:global/MPPaymentAdapterEAR/MPPaymentAdapterEJB/GSService!com.techedge.mp.payment.adapter.business.GSServiceRemote";
    final String                                 gpServiceRemoteJndi                    = "java:global/MPPaymentAdapterEAR/MPPaymentAdapterEJB/GPService!com.techedge.mp.payment.adapter.business.GPServiceRemote";
    final String                                 documentServiceLocalJndi               = "java:global/MPDocumentServiceEAR/MPDocumentServiceEJB/DocumentService!com.techedge.mp.document.encoder.business.DocumentServiceRemote";
    final String                                 forecourtInfoServiceLocalJndi          = "java:global/MPForecourtAdapterEAR/MPForecourtAdapterEJB/ForecourtInfoService!com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote";
    final String                                 transactionServiceJndi                 = "java:app/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionService";
    final String                                 forecourtPostPaidServiceRemoteJndi     = "java:global/MPForecourtShopAdapterEAR/MPForecourtShopAdapterEJB/ForecourtPostPaidService!com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote";
    final String                                 fidelityServiceRemoteJndi              = "java:global/MPFidelityAdapterEAR/MPFidelityAdapterEJB/FidelityService!com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote";
    final String                                 reconciliationServiceJndi              = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ReconciliationService!com.techedge.mp.core.business.ReconciliationService";
    final String                                 refuelingNotificationServiceRemoteJndi = "java:global/MPRefuelingAdapterEAR/MPRefuelingAdapterEJB/RefuelingNotificationService!com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote";
    final String                                 refuelingNotificationServiceOAuth2RemoteJndi = "java:global/MPRefuelingAdapterEAR/MPRefuelingAdapterEJB/RefuelingNotificationServiceOAuth2!com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote";
    final String                                 userCategoryServiceJndi                = "java:app/MPCoreBusinessEJB/UserCategoryService!com.techedge.mp.core.business.UserCategoryService";
    final String                                 unavaliabilityServiceJndi              = "java:app/MPCoreBusinessEJB/UnavailabilityPeriodService!com.techedge.mp.core.business.UnavailabilityPeriodService";
    final String                                 smsServiceRemoteJndi                   = "java:global/MPSmsAdapterEAR/MPSmsAdapterEJB/SmsService!com.techedge.mp.sms.adapter.business.SmsServiceRemote";
    final String                                 dwhAdapterServiceRemoteJndi            = "java:global/MPDWHAdapterEAR/MPDWHAdapterEJB/DWHAdapterService!com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote";
    final String                                 pushNotificationServiceRemoteJndi      = "java:global/MPPushNotificationAdapterEAR/MPPushNotificationAdapterEJB/PushNotificationService!com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote";
    final String                                 crmAdapterServiceRemoteJndi            = "java:global/MPCRMAdapterEAR/MPCRMAdapterEJB/CRMAdapterService!com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote";
    final String                                 crmServiceRemoteJndi                   = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/CRMService!com.techedge.mp.core.business.CRMServiceRemote";
    final String                                 parkingServiceRemoteJndi               = "java:global/MPParkingServiceEAR/MPParkingServiceEJB/ParkingService!com.techedge.mp.parking.integration.business.ParkingServiceRemote";
    final String                                 parkingTransactionV2ServiceJndi        = "java:app/MPCoreBusinessEJB/ParkingTransactionV2Service!com.techedge.mp.core.business.ParkingTransactionV2Service";
    final String                                 stationServiceRemoteJndi               = "java:app/MPCoreBusinessEJB/StationService!com.techedge.mp.core.business.StationService";
    final String                                 paymentServiceRemoteJndi               = "java:global/MPFidelityAdapterEAR/MPFidelityAdapterEJB/PaymentService!com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote";
    final String                                 authorizationPlusServiceRemoteJndi     = "java:global/MPFidelityAdapterEAR/MPFidelityAdapterEJB/AuthorizationPlusService!com.techedge.mp.fidelity.adapter.business.AuthorizationPlusServiceRemote";
    final String                                 cardInfoServiceRemoteJndi              = "java:global/MPFidelityAdapterEAR/MPFidelityAdapterEJB/CardInfoService!com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote";
    final String                                 eventNotificationServiceJndi           = "java:app/MPCoreBusinessEJB/EventNotificationService!com.techedge.mp.core.business.EventNotificationService";
   
    private static EJBHomeCache                  instance;

    protected Context                            context                                = null;

    protected EmailSenderRemote                  emailSender                            = null;
    protected LoggerService                      loggerService                          = null;
    protected ParametersService                  parametersService                      = null;
    protected GSServiceRemote                    gsService                              = null;
    protected GPServiceRemote                    gpService                              = null;
    protected DocumentServiceRemote              documentService                        = null;
    protected ForecourtInfoServiceRemote         forecourtInfoServiceRemote             = null;
    protected TransactionService                 transactionService                     = null;
    protected ForecourtPostPaidServiceRemote     forecourtPostPaidService               = null;
    protected FidelityServiceRemote              fidelityService                        = null;
    protected ReconciliationService              reconciliationService                  = null;
    protected RefuelingNotificationServiceRemote refuelingNotificationService           = null;
    protected RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2 = null;
    protected UserCategoryService                userCategoryService                    = null;
    protected UnavailabilityPeriodService        unavailabilityPeriodService            = null;
    protected SmsServiceRemote                   smsService                             = null;
    protected DWHAdapterServiceRemote            dwhAdapterService                      = null;
    protected PushNotificationServiceRemote      pushNotificationService                = null;
    protected CRMAdapterServiceRemote            crmAdapterService                      = null;
    protected CRMServiceRemote                   crmService                             = null;
    protected ParkingServiceRemote               parkingService                         = null;
    protected ParkingTransactionV2Service        parkingTransactionV2Service            = null;
    protected StationServiceRemote               stationService                         = null;
    protected PaymentServiceRemote               paymentService                         = null;
    protected AuthorizationPlusServiceRemote     authorizationPlusService               = null;
    protected CardInfoServiceRemote              cardInfoService                        = null;
    protected EventNotificationService           eventNotificationService               = null;

    
    private EJBHomeCache() throws InterfaceNotFoundException {

        final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();

        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

        try {

            context = new InitialContext(jndiProperties);
            emailSender = (EmailSenderRemote) context.lookup(emailSenderLocalJndi);
            loggerService = (LoggerService) context.lookup(loggerServiceJndi);
            parametersService = (ParametersService) context.lookup(parametersServiceJndi);
            gsService = (GSServiceRemote) context.lookup(gsServiceRemoteJndi);
            gpService = (GPServiceRemote) context.lookup(gpServiceRemoteJndi);
            documentService = (DocumentServiceRemote) context.lookup(documentServiceLocalJndi);
            forecourtInfoServiceRemote = (ForecourtInfoServiceRemote) context.lookup(forecourtInfoServiceLocalJndi);
            forecourtPostPaidService = (ForecourtPostPaidServiceRemote) context.lookup(forecourtPostPaidServiceRemoteJndi);
            fidelityService = (FidelityServiceRemote) context.lookup(fidelityServiceRemoteJndi);
//            refuelingNotificationService = (RefuelingNotificationServiceRemote) context.lookup(refuelingNotificationServiceRemoteJndi);
            dwhAdapterService = (DWHAdapterServiceRemote) context.lookup(dwhAdapterServiceRemoteJndi);
            //reconciliationService = (ReconciliationService) context.lookup(reconciliationServiceJndi);
            parkingService = (ParkingServiceRemote) context.lookup(parkingServiceRemoteJndi);
            paymentService = (PaymentServiceRemote) context.lookup(paymentServiceRemoteJndi);
            authorizationPlusService = (AuthorizationPlusServiceRemote) context.lookup(authorizationPlusServiceRemoteJndi);
            cardInfoService = (CardInfoServiceRemote) context.lookup(cardInfoServiceRemoteJndi);
            //eventNotificationService = (EventNotificationService) context.lookup(eventNotificationServiceJndi);

        }
        catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();

            //throw new InterfaceNotFoundException(emailSenderLocalJndi);
        }

    }

    public static synchronized EJBHomeCache getInstance() throws InterfaceNotFoundException {
        if (instance == null)
            instance = new EJBHomeCache();

        return instance;
    }

    public EmailSenderRemote getEmailSender() {

        return emailSender;
    }

    public LoggerService getLoggerService() {

        return loggerService;
    }

    public ParametersService getParametersService() {

        return parametersService;
    }

    public GSServiceRemote getGsService() {

        return gsService;
    }

    public GPServiceRemote getGpService() {

        return gpService;
    }

    public DocumentServiceRemote getDocumentService() {

        return documentService;
    }

    public ForecourtInfoServiceRemote getForecourtInfoService() {

        return forecourtInfoServiceRemote;
    }

    public ForecourtPostPaidServiceRemote getForecourtPostPaidService() {

        return forecourtPostPaidService;
    }

    public TransactionService getTransactionService() {

        if (this.transactionService == null) {

            try {

                this.transactionService = (TransactionService) context.lookup(transactionServiceJndi);
            }
            catch (Exception ex) {

                return null;
            }
        }

        return transactionService;
    }

    public FidelityServiceRemote getFidelityService() {

        return fidelityService;
    }

    public ReconciliationService getReconciliationService() {

        if (this.reconciliationService == null) {

            try {

                this.reconciliationService = (ReconciliationService) context.lookup(reconciliationServiceJndi);
            }
            catch (Exception ex) {

                return null;
            }
        }

        return reconciliationService;
    }

    public RefuelingNotificationServiceRemote getRefuelingNotificationService() {

        if (this.refuelingNotificationService == null) {

            try {

                this.refuelingNotificationService = (RefuelingNotificationServiceRemote) context.lookup(refuelingNotificationServiceRemoteJndi);
            }
            catch (Exception ex) {

                return null;
            }
        }

        return refuelingNotificationService;
    }

    public RefuelingNotificationServiceOAuth2Remote getRefuelingNotificationServiceOAuth2() {

        if (this.refuelingNotificationServiceOAuth2 == null) {

            try {

                this.refuelingNotificationServiceOAuth2 = (RefuelingNotificationServiceOAuth2Remote) context.lookup(refuelingNotificationServiceOAuth2RemoteJndi);
            }
            catch (Exception ex) {

                return null;
            }
        }

        return refuelingNotificationServiceOAuth2;
    }

    public UserCategoryService getUserCategoryService() {

        if (this.userCategoryService == null) {

            try {

                this.userCategoryService = (UserCategoryService) context.lookup(userCategoryServiceJndi);
            }
            catch (Exception ex) {

                return null;
            }
        }

        return userCategoryService;
    }
    
    public UnavailabilityPeriodService getUnavailabilityPeriodService() {

        if (this.unavailabilityPeriodService == null) {

            try {

                this.unavailabilityPeriodService = (UnavailabilityPeriodService) context.lookup(unavaliabilityServiceJndi);
            }
            catch (Exception ex) {

                return null;
            }
        }

        return unavailabilityPeriodService;
    }
    
    public SmsServiceRemote getSmsService() throws InterfaceNotFoundException {

        if (this.smsService == null) {

            try {

                this.smsService = (SmsServiceRemote) context.lookup(smsServiceRemoteJndi);
            }
            catch (Exception ex) {
                
                throw new InterfaceNotFoundException(smsServiceRemoteJndi);
            }
        }

        return smsService;
    }

    public DWHAdapterServiceRemote getDWHAdapterService() {

        if (this.dwhAdapterService == null) {

            try {

                this.dwhAdapterService = (DWHAdapterServiceRemote) context.lookup(dwhAdapterServiceRemoteJndi);
            }
            catch (Exception ex) {

                return null;
            }
        }

        return dwhAdapterService;
    }
    
    
    public PushNotificationServiceRemote getPushNotificationService() throws InterfaceNotFoundException {

        if (this.pushNotificationService == null) {

            try {

                this.pushNotificationService = (PushNotificationServiceRemote) context.lookup(pushNotificationServiceRemoteJndi);
            }
            catch (Exception ex) {
                
                throw new InterfaceNotFoundException(pushNotificationServiceRemoteJndi);
            }
        }

        return pushNotificationService;
    }
    

    public CRMAdapterServiceRemote getCRMAdapterService() throws InterfaceNotFoundException {
        if (this.crmAdapterService == null) {
            try {
                crmAdapterService = (CRMAdapterServiceRemote) context.lookup(crmAdapterServiceRemoteJndi);
            }
            catch (Exception ex) {
                throw new InterfaceNotFoundException(crmAdapterServiceRemoteJndi);
            }
        }
        
        return crmAdapterService;
    }
    
    
    public CRMServiceRemote getCRMService() throws InterfaceNotFoundException {
        if (this.crmService == null) {
            try {
                crmService = (CRMServiceRemote) context.lookup(crmServiceRemoteJndi);
            }
            catch (Exception ex) {
                throw new InterfaceNotFoundException(crmServiceRemoteJndi);
            }
        }
        
        return crmService;
    }
    
    public ParkingServiceRemote getParkingService() throws InterfaceNotFoundException {
        if (this.parkingService == null) {
            try {
                parkingService = (ParkingServiceRemote) context.lookup(parkingServiceRemoteJndi);
            }
            catch (Exception ex) {
                throw new InterfaceNotFoundException(parkingServiceRemoteJndi);
            }
        }
        
        return parkingService;
    }

    public ParkingTransactionV2Service getParkingTransactionV2Service() throws InterfaceNotFoundException {
        if (this.parkingTransactionV2Service == null) {
            try {
                parkingTransactionV2Service = (ParkingTransactionV2Service) context.lookup(parkingTransactionV2ServiceJndi);
            }
            catch (Exception ex) {
                throw new InterfaceNotFoundException(parkingTransactionV2ServiceJndi);
            }
        }

        return parkingTransactionV2Service;
    }
    
    public StationServiceRemote getStationService() throws InterfaceNotFoundException {
        if (this.stationService == null) {
            try {
                stationService = (StationServiceRemote) context.lookup(stationServiceRemoteJndi);
            }
            catch (Exception ex) {
                throw new InterfaceNotFoundException(stationServiceRemoteJndi);
            }
        }

        return stationService;
    }
    
    public PaymentServiceRemote getPaymentService() {

        return paymentService;
    }

    public AuthorizationPlusServiceRemote getAuthorizationPlusService() throws InterfaceNotFoundException {
        if (this.authorizationPlusService == null) {
            try {
                authorizationPlusService = (AuthorizationPlusServiceRemote) context.lookup(authorizationPlusServiceRemoteJndi);
            }
            catch (Exception ex) {
                throw new InterfaceNotFoundException(authorizationPlusServiceRemoteJndi);
            }
        }

        return authorizationPlusService;    }

    public void setAuthorizationPlusService(
            AuthorizationPlusServiceRemote authorizationPlusService) {
        this.authorizationPlusService = authorizationPlusService;
    }
    
    public CardInfoServiceRemote getCardInfoService() {
        return cardInfoService;
    }

    public void setCardInfoService(CardInfoServiceRemote cardInfoService) {
        this.cardInfoService = cardInfoService;
    }
    
    public EventNotificationService getEventNotificationService() {

        if (this.eventNotificationService == null) {

            try {

                this.eventNotificationService = (EventNotificationService) context.lookup(eventNotificationServiceJndi);
            }
            catch (Exception ex) {

                return null;
            }
        }

        return eventNotificationService;
    }
}
