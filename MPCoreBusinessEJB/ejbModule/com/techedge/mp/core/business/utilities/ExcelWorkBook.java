package com.techedge.mp.core.business.utilities;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWorkBook {

    Workbook wb;

    public ExcelWorkBook() {
        wb = new XSSFWorkbook();
    }

    public ExcelCellStyle createCellStyle(boolean fontBold, int fontHeight, Color fontColor, Color backgroundColor, HorizontalAlignment align, int indent, int borderTop,
            int borderRight, int borderBottom, int borderLeft, Color borderColor) {
        return new ExcelCellStyle(fontBold, fontHeight, fontColor, backgroundColor, align, indent, borderTop, borderRight, borderBottom, borderLeft, borderColor);
    }

    public ExcelCellStyle createCellStyle(boolean fontBold, int fontHeight, Color fontColor, Color backgroundColor, HorizontalAlignment align, int indent) {
        return new ExcelCellStyle(fontBold, fontHeight, fontColor, backgroundColor, align, indent, -1, -1, -1, -1, null);
    }

    public ExcelCellStyle createCellStyle(boolean fontBold, int fontHeight, Color fontColor, Color backgroundColor, HorizontalAlignment align) {
        return new ExcelCellStyle(fontBold, fontHeight, fontColor, backgroundColor, align, -1, -1, -1, -1, -1, null);
    }

    public ExcelCellStyle createCellStyle(boolean fontBold, int fontHeight, Color fontColor, HorizontalAlignment align) {
        return createCellStyle(fontBold, fontHeight, fontColor, null, align, -1, -1, -1, -1, -1, null);
    }

    public ExcelCellStyle createCellStyle(boolean fontBold, int fontHeight, HorizontalAlignment align) {
        return createCellStyle(fontBold, fontHeight, null, null, align, -1, -1, -1, -1, -1, null);
    }

    public ExcelCellStyle createCellStyle(boolean fontBold, int fontHeight, Color fontColor, Color backgroundColor) {
        return createCellStyle(fontBold, fontHeight, fontColor, backgroundColor, null, -1, -1, -1, -1, -1, null);
    }
    
    public ExcelSheetData createSheetData() {
        return new ExcelSheetData();
    }

    public Sheet addImageJpg(Sheet sheet, int rowIndex, int colIndex, String imageUrl, boolean resize, String proxyHost, String proxyPort, String proxyNoHosts) /*throws IOException*/ {

        Proxy proxy = new Proxy(proxyHost, proxyPort, proxyNoHosts);
        proxy.setHttp();

        int pictureIdx = -1;
        
        try {
            URL image = new URL(imageUrl);
            //BufferedImage imageIO = ImageIO.read(image));
            //int height= imageIO.getHeight();
            //int width=imageIO.getWidth();

            byte[] bytes = IOUtils.toByteArray(image.openStream());
            pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
        }
        catch (Exception ex) {
            System.err.println("Errore nel caricamento del logo: " + ex.getMessage());
        }

        // Create the drawing patriarch.  This is the top level container for all shapes. 
        Drawing drawing = sheet.createDrawingPatriarch();

        CreationHelper helper = wb.getCreationHelper();

        //add a picture shape
        ClientAnchor anchor = helper.createClientAnchor();
        //set top-left corner of the picture,
        //subsequent call of Picture#resize() will operate relative to it
        anchor.setRow1(rowIndex);
        anchor.setCol1(colIndex);
        
        if (pictureIdx != -1) {
            
            Picture pict = drawing.createPicture(anchor, pictureIdx);
    
            //auto-size picture relative to its top-left corner
            pict.resize();
        }
        
        proxy.unsetHttp();

        return sheet;
    }

    public Sheet addText(Sheet sheet, ExcelCellStyle dataStyle, int sheetRowIndex, int sheetColIndex, String text) {
        Row row = sheet.getRow(sheetRowIndex);

        if (row == null) {
            row = sheet.createRow(sheetRowIndex);
        }

        Cell cell = row.createCell(sheetColIndex);
        cell.setCellValue(text);
        cell.setCellStyle(dataStyle.getCellStyle());
        sheet.autoSizeColumn(sheetColIndex);

        return sheet;
    }

    public Sheet addTable(Sheet sheet, String[] columnsHeading, ArrayList<ArrayList<String>> data, ExcelCellStyle columnsHeadingStyle, ExcelCellStyle dataStyle, int sheetRowIndex,
            int sheetColIndex) {
        int index, rowIndex, colIndex;
        Row row = sheet.getRow(sheetRowIndex);

        if (row == null) {
            row = sheet.createRow(sheetRowIndex);
        }

        for (index = 0, colIndex = sheetColIndex; index < columnsHeading.length; index++, colIndex++) {
            Cell cell = row.createCell(colIndex);
            cell.setCellValue(columnsHeading[index]);
            cell.setCellStyle(columnsHeadingStyle.getCellStyle());
        }

        for (sheetRowIndex += 1, rowIndex = 0; rowIndex < data.size(); rowIndex++, sheetRowIndex++) {
            row = sheet.getRow(sheetRowIndex);
            if (row == null) {
                row = sheet.createRow(sheetRowIndex);
            }
            int colsSize = data.get(rowIndex).size();

            for (index = 0, colIndex = sheetColIndex; index < colsSize; index++, colIndex++) {
                Cell cell = row.createCell(colIndex);
                cell.setCellValue((String) data.get(rowIndex).get(index));
                cell.setCellStyle(dataStyle.getCellStyle());
                sheet.autoSizeColumn(colIndex);
            }
        }

        return sheet;
    }

    public Sheet addTable(Sheet sheet, String[] columnsHeading, ArrayList<ArrayList<String>> data, ExcelCellStyle columnsHeadingStyle,
            ArrayList<ArrayList<ExcelCellStyle>> dataStyle, int sheetRowIndex, int sheetColIndex) {
        int index, rowIndex, colIndex;
        Row row = sheet.getRow(sheetRowIndex);

        if (row == null) {
            row = sheet.createRow(sheetRowIndex);
        }

        for (index = 0, colIndex = sheetColIndex; index < columnsHeading.length; index++, colIndex++) {
            Cell cell = row.createCell(colIndex);
            cell.setCellValue(columnsHeading[index]);
            cell.setCellStyle(columnsHeadingStyle.getCellStyle());
        }

        for (sheetRowIndex += 1, rowIndex = 0; rowIndex < data.size(); rowIndex++, sheetRowIndex++) {
            row = sheet.getRow(sheetRowIndex);
            if (row == null) {
                row = sheet.createRow(sheetRowIndex);
            }

            ArrayList<String> dataRow = data.get(rowIndex);
            ArrayList<ExcelCellStyle> dataStyleRow = dataStyle.get(rowIndex);
            int colsSize = dataRow.size();

            for (index = 0, colIndex = sheetColIndex; index < colsSize; index++, colIndex++) {
                Cell cell = row.createCell(colIndex);
                cell.setCellValue(dataRow.get(index));

                if (dataStyleRow.get(index) != null) {
                    cell.setCellStyle(dataStyleRow.get(index).getCellStyle());
                }

                sheet.autoSizeColumn(colIndex);
            }
        }

        return sheet;
    }

    public Sheet addSheetBlank(String sheetName, boolean printGridlines, boolean displayGridlines) {
        Sheet sheet = wb.createSheet(sheetName);
        sheet.setPrintGridlines(printGridlines);
        sheet.setDisplayGridlines(displayGridlines);

        return sheet;
    }

    public Sheet addMergedCells(Sheet sheet, int firstRow, int lastRow, int firstCol, int lastCol, ExcelCellStyle excelCellStyle) {
        org.apache.poi.ss.util.CellRangeAddress cellRangeAddress = new org.apache.poi.ss.util.CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
        sheet.addMergedRegion(cellRangeAddress);

        if (excelCellStyle != null) {
            CellStyle cellStyle = excelCellStyle.getCellStyle();
            if (cellStyle.getBorderTop() != -1) {
                RegionUtil.setBorderTop(cellStyle.getBorderTop(), cellRangeAddress, sheet, wb);
                RegionUtil.setTopBorderColor(cellStyle.getTopBorderColor(), cellRangeAddress, sheet, wb);
            }

            if (cellStyle.getBorderRight() != -1) {
                RegionUtil.setBorderRight(cellStyle.getBorderRight(), cellRangeAddress, sheet, wb);
                RegionUtil.setRightBorderColor(cellStyle.getRightBorderColor(), cellRangeAddress, sheet, wb);
            }

            if (cellStyle.getBorderBottom() != -1) {
                RegionUtil.setBorderBottom(cellStyle.getBorderBottom(), cellRangeAddress, sheet, wb);
                RegionUtil.setBottomBorderColor(cellStyle.getBottomBorderColor(), cellRangeAddress, sheet, wb);
            }

            if (cellStyle.getBorderLeft() != -1) {
                RegionUtil.setBorderLeft(cellStyle.getBorderLeft(), cellRangeAddress, sheet, wb);
                RegionUtil.setLeftBorderColor(cellStyle.getLeftBorderColor(), cellRangeAddress, sheet, wb);
            }
        }

        return sheet;
    }

    public Sheet addMergedCells(Sheet sheet, int firstRow, int lastRow, int firstCol, int lastCol) {
        return addMergedCells(sheet, firstRow, lastRow, firstCol, lastCol, null);
    }

    //public void addSheet(String sheetName, String[] columnsHeading, ArrayList<ArrayList<String>> data, ExcelCellStyle columnsHeadingStyle, ExcelCellStyle dataStyle) {
    public void addSheet(String sheetName, String[] columnsHeading, ExcelSheetData data, ExcelCellStyle columnsHeadingStyle, ExcelCellStyle dataStyle) {
        Sheet sheet = null;
        sheet = wb.createSheet(sheetName);

        // Row and column indexes
        int rowIndex = 0;
        int colIndex = 0;
        int sheetRow = 0;

        // Generate column headings
        Row row = sheet.createRow(sheetRow);

        for (colIndex = 0; colIndex < columnsHeading.length; colIndex++) {
            Cell cell = row.createCell(colIndex);
            cell.setCellValue(columnsHeading[colIndex]);
            cell.setCellStyle(columnsHeadingStyle.getCellStyle());
        }

        //System.out.println("RIGHE TOTALI: " + data.size());

        for (sheetRow = 1, rowIndex = 0; rowIndex < data.getSize(); rowIndex++, sheetRow++) {
            row = sheet.createRow(sheetRow);

            for (colIndex = 0; colIndex < columnsHeading.length; colIndex++) {
                Cell cell = row.createCell(colIndex);
                //cell.setCellValue((String) data.get(rowIndex).get(colIndex));
                cell.setCellValue(data.getRowData(rowIndex, colIndex));
                cell.setCellStyle(dataStyle.getCellStyle());
            }
        }

        for (colIndex = 0; colIndex < columnsHeading.length; colIndex++) {
            sheet.autoSizeColumn(colIndex);
        }
    }
    
    public byte[] getBytesToStream() throws IOException {
        byte[] bytes;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        wb.write(baos);
        bytes = baos.toByteArray();

        return bytes;
    }

    public class ExcelCellStyle {

        XSSFCellStyle cellStyle;

        private ExcelCellStyle(boolean fontBold, int fontHeight, Color fontColor, Color backgroundColor, HorizontalAlignment align, int indent, int borderTop, int borderRight, int borderBottom, int borderLeft, Color borderColor) {
            cellStyle = (XSSFCellStyle) wb.createCellStyle();
            XSSFFont cellFont = (XSSFFont) wb.createFont();

            if (align != null) {
                cellStyle.setAlignment(align);
            }

            if (indent != -1) {
                cellStyle.setIndention((short) indent);
            }

            if (borderTop != -1) {
                cellStyle.setBorderTop((short) borderTop);
                cellStyle.setTopBorderColor(new XSSFColor(borderColor));
            }

            if (borderRight != -1) {
                cellStyle.setBorderRight((short) borderRight);
                cellStyle.setRightBorderColor(new XSSFColor(borderColor));
            }

            if (borderBottom != -1) {
                cellStyle.setBorderBottom((short) borderBottom);
                cellStyle.setBottomBorderColor(new XSSFColor(borderColor));
            }

            if (borderLeft != -1) {
                cellStyle.setBorderLeft((short) borderLeft);
                cellStyle.setLeftBorderColor(new XSSFColor(borderColor));
            }

            if (fontColor != null) {
                cellFont.setColor(new XSSFColor(fontColor));
            }

            if (backgroundColor != null) {
                cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
                cellStyle.setFillForegroundColor(new XSSFColor(backgroundColor));
                cellStyle.setFillBackgroundColor(new XSSFColor(backgroundColor));
            }

            if (fontBold) {
                cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
            }
            else {
                cellFont.setBoldweight(Font.BOLDWEIGHT_NORMAL);
            }

            cellFont.setFontHeightInPoints((short) fontHeight);
            cellStyle.setFont(cellFont);
        }

        private CellStyle getCellStyle() {
            return cellStyle;
        }
    }

    public class ExcelSheetData {

        private List<ArrayList<String>> data = new ArrayList<ArrayList<String>>(0);

        public ExcelSheetData() {}
        
        public int createRow() {
            data.add(new ArrayList<String>(0));
            return (data.size() - 1);
        }

        public void addRowData(String value, int rowIndex) {
            data.get(rowIndex).add(value);
        }

        public String getRowData(int rowIndex, int columIndex) {
            return data.get(rowIndex).get(columIndex);
        }

        public int getSize() {
            return data.size();
        }

        @SuppressWarnings("rawtypes")
        public void sortData(final int columnIndex, final Class columnClass, final Object... classArgs) {
            Collections.sort(data, new Comparator<ArrayList<String>>() {

                @Override
                public int compare(ArrayList<String> row1, ArrayList<String> row2) {
                    int compare = 0;
                    
                    try {
                        if (columnClass.equals(Date.class)) {
                            Date value1 = new SimpleDateFormat((String) classArgs[0]).parse(row1.get(columnIndex));
                            Date value2 = new SimpleDateFormat((String) classArgs[0]).parse(row2.get(columnIndex));
                            
                            compare = value1.compareTo(value2);
                        }

                        if (columnClass.equals(Long.class)) {
                            Long value1 = new Long(row1.get(columnIndex));
                            Long value2 = new Long(row2.get(columnIndex));
                            
                            compare = value1.compareTo(value2);
                        }
                        
                        if (columnClass.equals(Integer.class)) {
                            Integer value1 = new Integer(row1.get(columnIndex));
                            Integer value2 = new Integer(row2.get(columnIndex));
                            
                            compare = value1.compareTo(value2);
                        }
                    }
                    catch (Exception ex) {
                        System.err.println("Si � verificato un errore nella comparazione dei dati: " + ex.getMessage());
                    }
                    
                    return compare;
                }
                
            });
        }

    }
}
