package com.techedge.mp.core.business.utilities;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.DWHOperationType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.model.dwh.DWHEventBean;
import com.techedge.mp.core.business.model.dwh.DWHEventOperationBean;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.DWHAdapterResult;
import com.techedge.mp.dwh.adapter.utilities.StatusCode;

public class AsyncDWHService implements Runnable {
    
    private UserTransaction userTransaction;
    private EntityManager entityManager;
    private UserBean userBean;
    private List<TermsOfServiceBean> listTermOfService;
    private List<MobilePhoneBean> listMobilePhone;
    private List<UserSocialDataBean> listUserSocial;
    private String requestId;
    private Integer reconciliationMaxAttempts;
    private String codCard;
    private Date date;
    private String password;
    
    public AsyncDWHService(UserTransaction userTransaction, EntityManager entityManager, UserBean userBean, List<MobilePhoneBean> listMobilePhone, 
            List<TermsOfServiceBean> listTermOfService, List<UserSocialDataBean> listUserSocial, String requestId, String codCard, Integer reconciliationMaxAttempts) {

        this.userTransaction = userTransaction;
        this.entityManager = entityManager;
        this.userBean = userBean;
        this.listTermOfService = listTermOfService;
        this.listMobilePhone = listMobilePhone;
        this.listUserSocial = listUserSocial;
        this.requestId = requestId;
        this.reconciliationMaxAttempts = reconciliationMaxAttempts;
        this.codCard = codCard;
        this.date = new Date();
    }
    
    public AsyncDWHService(UserTransaction userTransaction, EntityManager entityManager, UserBean userBean, String requestId, String password, Integer reconciliationMaxAttempts) {
        
        this.userTransaction = userTransaction;
        this.entityManager = entityManager;
        this.userBean = userBean;
        this.requestId = requestId;
        this.reconciliationMaxAttempts = reconciliationMaxAttempts;
        this.password = password;
        this.date = new Date();
    }
    
    
    @Override
    public void run() {
        
        try {
            userTransaction.begin();
            
            if (password == null) {
                setUsetDataPlus();
            }
            else {
                changeUserPasswordPlus();
            }
            
            userTransaction.commit();
        }
        catch (Exception ex) {
            System.err.println("Errore nel thread asincrono AsyncDWHService: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
    
    private void setUsetDataPlus() throws InterfaceNotFoundException {
        
        if (userBean.getUserType().equals(User.USER_TYPE_GUEST)) {
            System.out.println("Rilevato utente di tipo guest: propagazione a DWH non effettuata");
            return;
        }
        
        String eventStatus = StatusHelper.DWH_EVENT_STATUS_SUCCESS;
        boolean toReconcilie = false;
        String campaignType = "NOLOY";
        Boolean userBlock = false;
        String mobilePhone = null;
        HashMap<String, Boolean> flagPrivacy = new HashMap<>(0);

        for (TermsOfServiceBean item : listTermOfService) {
            if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                flagPrivacy.put("privacy_eni", item.getAccepted());
            }
            if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                flagPrivacy.put("flg_privacy_partner", item.getAccepted());
                flagPrivacy.put("flg_privacy_analisi", item.getAccepted());
            }
            //if (item.getKeyval().equals("GEOLOCALIZZAZIONE_NEW_1")) {
                flagPrivacy.put("flg_privacy_geo", Boolean.TRUE);
            //}
        }

        for (MobilePhoneBean item : listMobilePhone) {
            if (item.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE)) {
                mobilePhone = item.getPrefix() + item.getNumber();
                break;
            }
        }
        
        if (userBean.getUserStatus().equals(User.USER_STATUS_BLOCKED)) {
            userBlock = true;
        }

        if(userBean.getVirtualizationCompleted() != null && userBean.getVirtualizationCompleted() == Boolean.TRUE) {    
            campaignType = "YOUCA";
        }
        
        String securityDataPassword = userBean.getPersonalDataBean().getSecurityDataPassword();
        
        Boolean isSocial  = Boolean.FALSE;
        String socialType = "";
        String idSocial   = null;
        
        String socialProvider = null;
        if (!listUserSocial.isEmpty()) {
            for(UserSocialDataBean userSocialDataBean : listUserSocial) {
                socialProvider = userSocialDataBean.getProvider();
                idSocial       = userSocialDataBean.getUuid();
            }
        }
        
        if (socialProvider != null) {
            if (socialProvider.equalsIgnoreCase("GOOGLE")) {
                isSocial             = Boolean.TRUE;
                socialType           = "google";
                securityDataPassword = "";
            }
            if (socialProvider.equalsIgnoreCase("FACEBOOK")) {
                isSocial             = Boolean.TRUE;
                socialType           = "facebook";
                securityDataPassword = "";
            }
        }
        
        String source = userBean.getSource();
        if (source != null) {
            if (source.equalsIgnoreCase("ENJOY")) {
                isSocial             = Boolean.FALSE;
                socialType           = "enjoy";
            }
            if (source.equalsIgnoreCase("MYCICERO")) {
                isSocial             = Boolean.FALSE;
                socialType           = "mycicero";
            }
        }
        
        DWHAdapterServiceRemote dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();
        DWHAdapterResult result = dwhAdapterService.setUserDataPlus(userBean.getPersonalDataBean().getSecurityDataEmail(),
                securityDataPassword, userBean.getPersonalDataBean().getFirstName(), userBean.getPersonalDataBean().getLastName(),
                userBean.getPersonalDataBean().getSex(), userBean.getPersonalDataBean().getFiscalCode(), userBean.getPersonalDataBean().getBirthDate(),
                userBean.getPersonalDataBean().getBirthMunicipality(), userBean.getPersonalDataBean().getBirthProvince(), mobilePhone, codCard, flagPrivacy, requestId,
                Long.valueOf(date.getTime()), userBlock, campaignType, isSocial, socialType, idSocial);
        
        if (!result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS)) {
            eventStatus = StatusHelper.DWH_EVENT_STATUS_FAILED;
            // L'utente non esiste
            System.err.println("DWH error: " + result.getMessageCode() + " (" + result.getStatusCode() + ")");

            if (result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR)) {
                toReconcilie = true;
                eventStatus = StatusHelper.DWH_EVENT_STATUS_ERROR;
            }
        }

        DWHEventBean dWHEventBean = QueryRepository.findDWHEventByUserAndOperation(entityManager, userBean, DWHOperationType.SET_USER_DATA.name());
        
        if (dWHEventBean == null) {
            dWHEventBean = new DWHEventBean();
            dWHEventBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
            dWHEventBean.setToReconcilie(toReconcilie);
            dWHEventBean.setEventStatus(eventStatus);
            dWHEventBean.setOperation(DWHOperationType.SET_USER_DATA);
            dWHEventBean.setUserBean(userBean);

            entityManager.persist(dWHEventBean);
        }
        else {
            dWHEventBean.setEventStatus(eventStatus);
            dWHEventBean.setToReconcilie(toReconcilie);
            
            entityManager.merge(dWHEventBean);
        }

        DWHEventOperationBean dWHEventOperationBean = new DWHEventOperationBean();
        dWHEventOperationBean.setRequestTimestamp(date);
        dWHEventOperationBean.setDWHEventBean(dWHEventBean);
        dWHEventOperationBean.setRequestId(requestId);
        dWHEventOperationBean.setStatusCode(result.getStatusCode());
        dWHEventOperationBean.setMessageCode(result.getMessageCode());

        entityManager.persist(dWHEventOperationBean);
    }
    
    private void changeUserPasswordPlus() throws InterfaceNotFoundException {
        String eventStatus = StatusHelper.DWH_EVENT_STATUS_SUCCESS;
        boolean toReconcilie = false;
        
        DWHEventBean dwhEventBeanChangePassword = QueryRepository.findDWHEventByUserAndOperation(entityManager, userBean, DWHOperationType.CHANGE_PASSWORD.name());
        DWHAdapterServiceRemote dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();

        String email = userBean.getPersonalDataBean().getSecurityDataEmail();

        DWHAdapterResult result = dwhAdapterService.changeUserPasswordPlus(email, password, requestId, date.getTime());

        if (!result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS)) {
            
            // L'utente non esiste
            System.err.println("DWH error: " + result.getMessageCode() + " (" + result.getStatusCode() + ")");

            if (result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR)) {
                eventStatus = StatusHelper.DWH_EVENT_STATUS_ERROR;
                toReconcilie = true;
            }
            else {
                eventStatus = StatusHelper.DWH_EVENT_STATUS_FAILED;
            }
            /*
            DWHEventBean dwhEventBeanUserData = QueryRepository.findDWHEventByUserAndOperation(entityManager, userBean, DWHOperationType.SET_USER_DATA.name());
            
            dwhEventBeanUserData.setToReconcilie(true);
            dwhEventBeanUserData.setEventStatus(eventStatus);
            entityManager.merge(dwhEventBeanUserData);
            */
        }
        
        if (dwhEventBeanChangePassword == null) {
            dwhEventBeanChangePassword = new DWHEventBean();
            dwhEventBeanChangePassword.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
            dwhEventBeanChangePassword.setToReconcilie(toReconcilie);
            dwhEventBeanChangePassword.setEventStatus(eventStatus);
            dwhEventBeanChangePassword.setOperation(DWHOperationType.CHANGE_PASSWORD);
            dwhEventBeanChangePassword.setUserBean(userBean);
            
            entityManager.persist(dwhEventBeanChangePassword);
        }
        else {
            dwhEventBeanChangePassword.setEventStatus(eventStatus);
            dwhEventBeanChangePassword.setToReconcilie(toReconcilie);
            entityManager.merge(dwhEventBeanChangePassword);
        }

        DWHEventOperationBean dWHEventOperationBean = new DWHEventOperationBean();
        dWHEventOperationBean.setRequestTimestamp(date);
        dWHEventOperationBean.setDWHEventBean(dwhEventBeanChangePassword);
        dWHEventOperationBean.setRequestId(requestId);
        dWHEventOperationBean.setStatusCode(result.getStatusCode());
        dWHEventOperationBean.setMessageCode(result.getMessageCode());
        
        entityManager.persist(dWHEventOperationBean);
    }
}
