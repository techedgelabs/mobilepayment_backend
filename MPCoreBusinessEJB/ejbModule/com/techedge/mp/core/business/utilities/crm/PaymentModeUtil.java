package com.techedge.mp.core.business.utilities.crm;

import java.util.Set;

import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;

public class PaymentModeUtil {
	
	public static String getPaymentModePostpaid(PostPaidTransactionBean poPTransactionBean){
		String modalitaPagamento="";
        if(poPTransactionBean.getAcquirerID().equalsIgnoreCase("QENIT"))
        	modalitaPagamento=PAYMENT_MODE_TYPE.VOUCHER.getValue();
        else if(poPTransactionBean.getAcquirerID().equalsIgnoreCase("MULTICARD"))
        	modalitaPagamento=PAYMENT_MODE_TYPE.MULTICARD.getValue();
        else if(poPTransactionBean.getAcquirerID().equalsIgnoreCase("CARTASI")){
        	Set<PostPaidConsumeVoucherBean> postPaidConsumeVoucher = poPTransactionBean.getPostPaidConsumeVoucherBeanList();
        	if(postPaidConsumeVoucher!=null && postPaidConsumeVoucher.size()>0){
        		for( PostPaidConsumeVoucherBean ele : postPaidConsumeVoucher){
        			int voucherAmount=0;
        			
        			if(ele.getStatusCode().equalsIgnoreCase("00") 
        			   && ele.getOperationType().equalsIgnoreCase("CONSUME")
        			   && ele.getTotalConsumed()>0){
        				voucherAmount += ele.getTotalConsumed();
        			}
        			
        			if(voucherAmount==0)
        				modalitaPagamento=PAYMENT_MODE_TYPE.CARTA.getValue();
        			else if (voucherAmount<poPTransactionBean.getAmount())
        				modalitaPagamento=PAYMENT_MODE_TYPE.CARTA_VOUCHER.getValue();
        			else if (voucherAmount==poPTransactionBean.getAmount())
        				modalitaPagamento=PAYMENT_MODE_TYPE.VOUCHER.getValue();
        		}
        	}else{
        		modalitaPagamento=PAYMENT_MODE_TYPE.CARTA.getValue();
        	}
        }
        
        return modalitaPagamento;
	}
	
	public static String getPaymentModePrepaid(TransactionBean transactionBean){
		String modalitaPagamento="";
        if(transactionBean.getAcquirerID().equalsIgnoreCase("QENIT"))
        	modalitaPagamento=PAYMENT_MODE_TYPE.VOUCHER.getValue();
        else if(transactionBean.getAcquirerID().equalsIgnoreCase("MULTICARD"))
        	modalitaPagamento=PAYMENT_MODE_TYPE.MULTICARD.getValue();
        else if(transactionBean.getAcquirerID().equalsIgnoreCase("CARTASI")){
        	Set<PrePaidConsumeVoucherBean> postPaidConsumeVoucher = transactionBean.getPrePaidConsumeVoucherBeanList();
        	if(postPaidConsumeVoucher!=null && postPaidConsumeVoucher.size()>0){
        		for( PrePaidConsumeVoucherBean ele : postPaidConsumeVoucher){
        			int voucherAmount=0;
        			
        			if(ele.getStatusCode().equalsIgnoreCase("00") 
        			   && ele.getOperationType().equalsIgnoreCase("CONSUME")
        			   && ele.getTotalConsumed()>0){
        				voucherAmount += ele.getTotalConsumed();
        			}
        			
        			if(voucherAmount==0)
        				modalitaPagamento=PAYMENT_MODE_TYPE.CARTA.getValue();
        			else if (voucherAmount<transactionBean.getAmount())
        				modalitaPagamento=PAYMENT_MODE_TYPE.CARTA_VOUCHER.getValue();
        			else if (voucherAmount==transactionBean.getAmount())
        				modalitaPagamento=PAYMENT_MODE_TYPE.VOUCHER.getValue();
        		}
        	}else{
        		modalitaPagamento=PAYMENT_MODE_TYPE.CARTA.getValue();
        	}
        }
        
        return modalitaPagamento;
	}

}
