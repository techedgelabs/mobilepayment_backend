package com.techedge.mp.core.business.utilities;

import java.util.HashMap;
import java.util.Map;

import com.techedge.mp.core.actions.refueling.RefuelingEnumStatus;

public class RefuelingStatusMapping {
    
    private Map<String, RefuelingEnumStatus>        refuelingMap;
    
    public RefuelingStatusMapping() {
        
        refuelingMap = new HashMap<String, RefuelingEnumStatus>();

        refuelingMap.put("0007", RefuelingEnumStatus.START_REFUELING);
        refuelingMap.put("0009", RefuelingEnumStatus.REFUELING_IN_PROGRESS);
        refuelingMap.put("0010", RefuelingEnumStatus.END_REFUELING);
        refuelingMap.put("0013", RefuelingEnumStatus.END_REFUELING);
        refuelingMap.put("0015", RefuelingEnumStatus.END_REFUELING);
        refuelingMap.put("0003", RefuelingEnumStatus.ERROR);
        refuelingMap.put("0005", RefuelingEnumStatus.ERROR);
        //refuelingMap.put("0008", RefuelingEnumStatus.ERROR);
        
        
        refuelingMap.put("0017", RefuelingEnumStatus.RECONCILIATION);
        refuelingMap.put("0018", RefuelingEnumStatus.RECONCILIATION);
        refuelingMap.put("0006", RefuelingEnumStatus.RECONCILIATION);
        refuelingMap.put("0012", RefuelingEnumStatus.RECONCILIATION);
        refuelingMap.put("0014", RefuelingEnumStatus.RECONCILIATION);
        refuelingMap.put("0016", RefuelingEnumStatus.RECONCILIATION);
    }

    public RefuelingEnumStatus getEnumState(String status){
        
        if(status != null && !status.equals("")){
            return refuelingMap.get(status);
        }
        return null;
    }
    
    public String getStringState(String status){
       
        if(status != null && !status.equals("")){
            if ( refuelingMap.get(status) != null ) { 
                return refuelingMap.get(status).toString();
            }
            else {
                return "";
            }
        }
        return null;
    }
}
