package com.techedge.mp.core.business.utilities.test;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import com.techedge.mp.core.business.utilities.CaptchaHelper;
import com.techedge.mp.core.business.utilities.Proxy;

public class CaptchaHelperTest extends TestCase {

    @Before
    public void setUp() {

    }

    @Test
    public void test() {

        String captchaCode           = "03AJIzXZ7cilWrnM5itfbRqW_yB8fpHkScAMR0DX-Ehd5YmfIy8ivkMQmpORoDTpZ49ZI7XYTf-CNZZsfu8We2Yyki4Eluk0Ovwjau3JVpIRVCORgxIFopDcpcdvlKU866hIzpnriY_-JgYvBJ4YxACqumGfKDbV_hx5G4aDAnEGWb1YEDd-OBTE-1KSXeKJaO2zVrG6kkZeDUY2FQpQaDQNrA1koTJG8nQTg6wU3pCoZHur5uz3joFt7ppu2wgBt3yRTA6xmcBlGQud6wQrSqCvS1fxbXzdy5jKVXl-XmzggEn_s_WWZaMJGALhXoOUAQFAwhKkzVR2TEUr3p9XQmWygU7CYxDiXV-K7CnW32kfCnFgtyTA4dMkGV_wGPrzIAlF8eFPRpMBJpJLrurUHsR-lu2X7ccTgPVEjSiaJzyShu5Z85_QpdHCqKKfh-dXVe_y0ucdSXuqlyz8ee2Jtvfz2sFeTc0ZU0NA";
        String captchaSecret         = "6Lf5m0YUAAAAAL-kac5i1sg--zhoaEEwaZZP3Oih";
        String urlCapthaVerification = "https://www.google.com/recaptcha/api/siteverify";
        String proxyHost             = "sprite.techedge.corp";
        String proxyPort             = "3128";
        String proxyNoHosts          = "";

        Proxy proxy = new Proxy(proxyHost, proxyPort, proxyNoHosts);
        
        proxy.setHttp();
        
        Boolean check = Boolean.FALSE;
        try {
            check = CaptchaHelper.checkCaptcha(captchaCode, captchaSecret, urlCapthaVerification);
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            proxy.unsetHttp();
        }
        
        proxy.unsetHttp();

        assertEquals(Boolean.TRUE, check);
    }
}
