package com.techedge.mp.core.business.utilities;

import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;


public class StationHelper {

	//ForecourtInfoServiceRemote forecourtService      = null;	

	public StationHelper(){
		
		/*
		try	{
			forecourtService   = EJBHomeCache.getInstance().getForecourtInfoService();
		}
		catch (InterfaceNotFoundException e) {
		}
		*/
	}
	
	/*
	// StationInfoResponse
	public StationStatusMessageResponse getStationPump(String requestId, String stationId, String pumpId, boolean requestType){
		System.out.println("StationHelper.getStationDetail-pre");
		
		StationStatusMessageRequest stationInfoRequest = new StationStatusMessageRequest();
		//StationInfoRequest stationInfoRequest = new StationInfoRequest();

		stationInfoRequest.setRequestID(requestId);
		stationInfoRequest.setStationID(stationId);
		stationInfoRequest.setPumpID(pumpId);
		stationInfoRequest.setPumpDetailsReq(requestType);

		StationStatusMessageResponse stationInfoResponse = forecourtService.getStationDetails(stationInfoRequest);
		//String stationStatusCode = stationInfoResponse.getStatusCode();
		
		System.out.println("StationHelper.getStationDetail-after");
		
		
		return stationInfoResponse;

	}
	*/
	
	public static String checkType(GetStationDetailsResponse getStationDetailsResponse) {
		
	 // se non ho risposta dal forecourtInfoService avr� stationDeatil null
	 // Se all'oggetto non � associato un erogatore, allora viene considerato come una cassa
		String type = "ERRORE";
		
		if( (!getStationDetailsResponse.getStatusCode().equals("PUMP_NOT_FOUND_404") || getStationDetailsResponse.getStatusCode().equals("PUMP_STATUS_NOT_AVAILABLE_500")) && 
		        getStationDetailsResponse.getStationDetail() != null) {
			
		    type = checkPumpOrShop(getStationDetailsResponse);
			
		}
		
		System.out.println("StationHelper.check" + type);
		
		return type;
	}

    private static String checkPumpOrShop(GetStationDetailsResponse getStationDetailsResponse) {
        
      //Errore la stazione ha prolemi
        String type = "ERRORE";
        
        if( getStationDetailsResponse.getStationDetail().getPumpDetails() != null &&
                getStationDetailsResponse.getStationDetail().getPumpDetails().size() > 0 ) {
            
            com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail pumpInfo = getStationDetailsResponse.getStationDetail().getPumpDetails().get(0);
            
            if (pumpInfo.getRefuelMode().equals("Self")) {
                
                //PRE-PAY FUEL
                type = "FUEL_PRE_PAY";
            }
            else {
                
                //POST-PAY FUEL
                type = "FUEL_POST_PAY";
            }
        }
        else {
            
            if( getStationDetailsResponse.getStationDetail().getSourceDetails() != null &&
                    getStationDetailsResponse.getStationDetail().getSourceDetails().size()>0) {
                
                type = "SHOP";
            }
        }
        
        return type;
    }
}
