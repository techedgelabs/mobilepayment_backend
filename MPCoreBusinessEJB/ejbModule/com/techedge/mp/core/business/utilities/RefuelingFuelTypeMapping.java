package com.techedge.mp.core.business.utilities;

import java.util.HashMap;
import java.util.Map;

import com.techedge.mp.core.business.interfaces.Pair;


public class RefuelingFuelTypeMapping {

    private Map<String, Pair<String, String>>    refuelingFuelType;
    
    public RefuelingFuelTypeMapping(){
        
        refuelingFuelType = new HashMap<String, Pair<String, String>>();
        fillRefuelingFuelTypeMap("SP", "SenzaPb", "Benzina");
        fillRefuelingFuelTypeMap("BS", "BluSuper", "Benzina");
        fillRefuelingFuelTypeMap("GG", "Gasolio", "Gasolio");
        fillRefuelingFuelTypeMap("BD", "BluDiesel", "Gasolio");
        fillRefuelingFuelTypeMap("MT", "Metano", "N/A");
        fillRefuelingFuelTypeMap("GP", "GPL", "N/A");
        fillRefuelingFuelTypeMap("AD", "ADBLU", "N/A");
    }
    
    private void fillRefuelingFuelTypeMap(String key, String description, String fuelType){
        
        Pair<String, String> pair = new Pair<String, String>(description,fuelType);
        refuelingFuelType.put(key, pair);
    }

    public String getProductDescription(String productID){
        
        if(productID != null && !productID.equals("")){
            
            return refuelingFuelType.get(productID).getLeft();
        }
        return  null;
    }
    
    public String getFuelType(String productID){
        
        if(productID != null && !productID.equals("")){
            return refuelingFuelType.get(productID).getRight();
        }
        return null;
    }
}
