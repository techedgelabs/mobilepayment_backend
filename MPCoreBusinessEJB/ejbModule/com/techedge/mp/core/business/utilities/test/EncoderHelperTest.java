package com.techedge.mp.core.business.utilities.test;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import com.techedge.mp.core.business.utilities.EncoderHelper;

public class EncoderHelperTest extends TestCase {

    @Before
    public void setUp() {

    }

    @Test
    public void test() {

        String plainPassword           = "qumqlxO7";
        String expectedEncodedPassword = "aMRdoV2MVyTm98VnrSiXhw==";
        
        String encodedPassword = EncoderHelper.encodeMD5(plainPassword);
        
        assertEquals(expectedEncodedPassword, encodedPassword);
    }
}
