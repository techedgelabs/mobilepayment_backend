package com.techedge.mp.core.business.utilities;

public class PanHelper {
	
	public static final String BRAND_VISA       = "Visa";
	public static final String BRAND_MASTERCARD = "MasterCard";
	public static final String BRAND_MAESTRO    = "Maestro";
	public static final String BRAND_VPAY       = "VPay";

	public PanHelper() {}
	
	public static String maskPan(String pan) {
		
		if ( pan == null ) {
			return null;
		}
		
		if ( pan.length() != 16) {
			return null;
		}
		
		String maskedPan = pan.substring(0, 2) + "XXXXXXXXXX" + pan.substring(12, 16);
		
		return maskedPan;
	}
	
	public static String getBrand(String token) {
		
		if ( token == null ) {
			return "";
		}
		
		if ( token.startsWith("4") ) {
			return PanHelper.BRAND_VISA;
		}
		
		if ( token.startsWith("5") ) {
			return PanHelper.BRAND_MASTERCARD;
		}
		
		return "";
	}
	
	public static String generateBrand(String brand) {
	    
	    if ( brand == null || brand.isEmpty() ) {
            return "";
        }
	    
	    String outBrand = "";
	    
	    if ( brand.equalsIgnoreCase("VISA") ) {
	        outBrand = PanHelper.BRAND_VISA; 
	    }
	    
	    if ( brand.equalsIgnoreCase("MASTERCARD") ) {
            outBrand = PanHelper.BRAND_MASTERCARD; 
        }
	    
	    if ( brand.equalsIgnoreCase("MAESTRO") ) {
            outBrand = PanHelper.BRAND_MAESTRO; 
        }
	    
	    if ( brand.equalsIgnoreCase("VPAY") ) {
            outBrand = PanHelper.BRAND_VPAY; 
        }
	    
	    return outBrand;
	}
	
	public static String maskLoyaltyCode(String loyaltyCode) {
        
        if ( loyaltyCode == null ) {
            return null;
        }
        
        int length = loyaltyCode.length();
        
        if ( length <= 6 ) {
            return loyaltyCode;
        }
        
        int baseLength = length - 6;
        
        String baseMaskedLoyaltyCode = "";
        for(int i=0; i<baseLength; i++) {
            baseMaskedLoyaltyCode += "X";
        }
        
        String maskedLoyaltyCode = loyaltyCode.substring(0, 2) + baseMaskedLoyaltyCode + loyaltyCode.substring(length - 4, length);
        
        return maskedLoyaltyCode;
    }
}
