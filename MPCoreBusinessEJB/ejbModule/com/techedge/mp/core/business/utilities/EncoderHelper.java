package com.techedge.mp.core.business.utilities;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.params.KeyParameter;

public class EncoderHelper {
	
	private static final int HASH_BYTE_SIZE = 128;
	private static final int PBKDF2_ITERATIONS = 1000;
	private static final String SALT = "mobilepayment";
	private static final String SALT_REFUELING = "enjoy";
	
	public EncoderHelper() {}
	
	
	public static String encode(String plainText) {
		
		if ( plainText == null || plainText.equals("") )
			return null;
		
		PKCS5S2ParametersGenerator generator = new PKCS5S2ParametersGenerator(new SHA256Digest());
	    generator.init(PBEParametersGenerator.PKCS5PasswordToUTF8Bytes(plainText.toCharArray()), SALT.getBytes(), PBKDF2_ITERATIONS);
	    KeyParameter key = (KeyParameter)generator.generateDerivedMacParameters(HASH_BYTE_SIZE);
	    byte[] hash = key.getKey();
		
		String encodedText = DatatypeConverter.printBase64Binary(hash);
		
		return encodedText;
	}
	
	public static String encodeRefueling(String plainText) {
        
        if ( plainText == null || plainText.equals("") )
            return null;
        
        PKCS5S2ParametersGenerator generator = new PKCS5S2ParametersGenerator(new SHA256Digest());
        generator.init(PBEParametersGenerator.PKCS5PasswordToUTF8Bytes(plainText.toCharArray()), SALT_REFUELING.getBytes(), PBKDF2_ITERATIONS);
        KeyParameter key = (KeyParameter)generator.generateDerivedMacParameters(HASH_BYTE_SIZE);
        byte[] hash = key.getKey();
        
        String encodedText = DatatypeConverter.printBase64Binary(hash);
        
        return encodedText;
    }
	
	public static String encodeMD5(String plainText) {
	    
	    MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
	    md5.update(StandardCharsets.UTF_8.encode(plainText));
	    byte[] rawData = md5.digest();
        return DatatypeConverter.printBase64Binary(rawData);
	}
}
