package com.techedge.mp.core.business.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

	public DateHelper() {}
	
	public static Date addMinutesToDate(int minutes, Date beforeTime){
	    
		final long ONE_MINUTE_IN_MILLIS=60000;

	    long curTimeInMs = beforeTime.getTime();
	    Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
	    return afterAddingMins;
	}
	
	public static Date convertToDateOfBirth(String birthDay) throws ParseException
	{
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return formatter.parse(birthDay);
	}
}
