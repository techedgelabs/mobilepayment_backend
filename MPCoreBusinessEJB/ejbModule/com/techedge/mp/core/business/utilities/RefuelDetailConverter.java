package com.techedge.mp.core.business.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;

public class RefuelDetailConverter {

    public RefuelDetailConverter() {

    }

    public static RefuelDetail createRefuelDetail(TransactionBean transactionBean) {

        //RefuelDetail di MPRefuelingAdapterEJBClient
        RefuelDetail fuelDeatail = new RefuelDetail();

        Date timestampStartRefuel = null;
        Date timestampEndRefuel = null;

        for (TransactionStatusBean transactionStatusBean : transactionBean.getTransactionStatusBeanData()) {

            if (transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PUMP_ENABLED)) {
                timestampStartRefuel = new Date(transactionStatusBean.getTimestamp().getTime());
            }
            if (transactionStatusBean.getStatus().equals(StatusHelper.STATUS_END_REFUEL_RECEIVED)) {
                timestampEndRefuel = new Date(transactionStatusBean.getTimestamp().getTime());
            }
        }

        fuelDeatail.setTimestampStartRefuel(convertDateToString(timestampStartRefuel));
        fuelDeatail.setTimestampEndRefuel(convertDateToString(timestampEndRefuel));

        fuelDeatail.setAuthorizationCode(transactionBean.getAuthorizationCode());
        fuelDeatail.setAmount(transactionBean.getFinalAmount());

        fuelDeatail.setFuelQuantity(transactionBean.getFuelQuantity());
        fuelDeatail.setProductID(transactionBean.getProductID());
        fuelDeatail.setProductDescription(transactionBean.getProductDescription());

        RefuelingFuelTypeMapping rftm = new RefuelingFuelTypeMapping();
        fuelDeatail.setFuelType(rftm.getFuelType(transactionBean.getProductID()));
        return fuelDeatail;
    }

    private static String convertDateToString(Date refuelDate) {

        if (refuelDate == null) {
            return "";
        }

        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        try {
            dateString = sdfr.format(refuelDate);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return dateString;
    }
}
