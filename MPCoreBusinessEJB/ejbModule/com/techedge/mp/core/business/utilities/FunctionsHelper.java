package com.techedge.mp.core.business.utilities;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;

public class FunctionsHelper {

    public FunctionsHelper() {}

    public static boolean set(Object object, String fieldName, Object fieldValue) {
        Class<?> clazz = object.getClass();
        while (clazz != null) {
            try {
                Field field = clazz.getDeclaredField(fieldName);
                field.setAccessible(true);
                field.set(object, fieldValue);
                return true;
            }
            catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
            catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return false;
    }

    public static Object toObject(Class clazz, String value, EntityManager em) {
        if (value == null) {
            return null;
        }
        try {
            if (Boolean.class == clazz) {
                Boolean result = Boolean.parseBoolean(value);
                return result;
            }
            if (Byte.class == clazz) {
                Boolean result = Boolean.parseBoolean(value);
                return result;
            }
            if (Short.class == clazz) {
                Short result = Short.parseShort(value);
                return result;
            }
            if (Integer.class == clazz) {
                Integer result = Integer.parseInt(value);
                return result;
            }
            if (Long.class == clazz || long.class == clazz) {
                Long result = Long.parseLong(value);
                return result;
            }
            if (Float.class == clazz) {
                Float result = Float.parseFloat(value);
                return result;
            }
            if (Double.class == clazz) {
                Double result = Double.parseDouble(value);
                return result;
            }
            if (BigDecimal.class == clazz) {
                BigDecimal result = new BigDecimal(value);
                return result;
            }
            if (Date.class == clazz) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                return sdf.parse(value);
            }
            if (Timestamp.class == clazz) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date parsedDate = sdf.parse(value);
                Timestamp timestamp = new Timestamp(parsedDate.getTime());
                return timestamp;
            }
            if (String.class == clazz)
                return value;
            else {
                Long id = Long.parseLong(value);
                if (id != null) {
                    return em.find(clazz, id);
                }
                else {
                    return null;
                }
            }
        }
        catch (NullPointerException npe) {
            return null;
        }
        catch (NumberFormatException nfe) {
            return null;
        }
        catch (ParseException pe) {
            System.err.println("Errore nella parsing della data: " + pe.getMessage());
            return null;
        }
    }

    public static Object getObject(Object object, String fieldName) {

        Class<?> clazz = object.getClass();
        while (clazz != null) {
            try {
                Field field = clazz.getDeclaredField(fieldName);
                field.setAccessible(true);
                Object result = field.get(object);
                return result;
            }
            catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
            catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return null;

    }
}
