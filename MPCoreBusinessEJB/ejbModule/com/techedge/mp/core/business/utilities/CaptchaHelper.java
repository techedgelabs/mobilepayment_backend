package com.techedge.mp.core.business.utilities;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

class CaptchaCheckResult {

    private boolean success = false;

    public CaptchaCheckResult() {}

    public CaptchaCheckResult(String jsonString) {
        if (jsonString != null && !jsonString.isEmpty() && jsonString.contains("\"success\": true")) {
            this.success = true;
        }
    }

    public boolean isSuccess() {
        return this.success;
    }
}

public class CaptchaHelper {

    public CaptchaHelper() {}

    public static boolean checkCaptcha(String captchaCode, String captchaSecret, String urlCapthaVerification) throws IOException {

        String url = urlCapthaVerification;
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");

        String urlParameters = "secret=" + captchaSecret + "&response=" + captchaCode;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        String result = response.toString();

        CaptchaCheckResult captchaCheckResult = new CaptchaCheckResult(result);

        return captchaCheckResult.isSuccess();
    }

}
