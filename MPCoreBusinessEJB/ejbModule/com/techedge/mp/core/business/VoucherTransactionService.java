package com.techedge.mp.core.business;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.techedge.mp.core.actions.voucher.CreateApplePayVoucherTransactionAction;
import com.techedge.mp.core.actions.voucher.CreateVoucherTransactionAction;
import com.techedge.mp.core.actions.voucher.RetrieveVoucherTransactionAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.voucher.CreateApplePayVoucherTransactionResult;
import com.techedge.mp.core.business.interfaces.voucher.CreateVoucherTransactionResult;
import com.techedge.mp.core.business.interfaces.voucher.RetrieveVoucherTransactionDataResponse;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@LocalBean
public class VoucherTransactionService implements VoucherTransactionServiceRemote, VoucherTransactionServiceLocal {

    private static final String                                PARAM_UIC                         = "UIC";
    private static final String                                PARAM_PAYMENT_TYPE                = "PAYMENT_TYPE";
    private static final String                                PARAM_PIN_CHECK_MAX_ATTEMPTS      = "PIN_CHECK_MAX_ATTEMPTS";
    private static final String                                PARAM_RECONCILIATION_MAX_ATTEMPTS = "RECONCILIATION_MAX_ATTEMPTS";
    private static final String                                PARAM_EMAIL_SENDER_ADDRESS        = "EMAIL_SENDER_ADDRESS";
    private static final String                                PARAM_SMTP_HOST                   = "SMTP_HOST";
    private static final String                                PARAM_SMTP_PORT                   = "SMTP_PORT";
    private final static String                                PARAM_PROXY_HOST                  = "PROXY_HOST";
    private final static String                                PARAM_PROXY_PORT                  = "PROXY_PORT";
    private final static String                                PARAM_PROXY_NO_HOSTS              = "PROXY_NO_HOSTS";
    private final static String                                PARAM_ACQUIRER_ID                 = "ACQUIRER_ID";
    private final static String                                PARAM_SHOPLOGIN_NEW_FLOW          = "SHOPLOGIN_NEW_FLOW";
    private final static String                                PARAM_USER_BLOCK_EXCEPTION        = "USER_BLOCK_EXCEPTION";

    @EJB
    private CreateVoucherTransactionAction                      createVoucherTransactionAction;

    @EJB
    private CreateApplePayVoucherTransactionAction              createApplePayVoucherTransactionAction;
    
    @EJB
    private RetrieveVoucherTransactionAction                    retrieveVoucherTransactionAction;


    private LoggerService                                      loggerService                     = null;
    private ParametersService                                  parametersService                 = null;
    private EmailSenderRemote                                  emailSender                       = null;
    private FidelityServiceRemote                              fidelityService                   = null;
    private UserCategoryService                                userCategoryService               = null;
    private UnavailabilityPeriodService                        unavailabilityPeriodService       = null;

    /**
     * Default constructor.
     */
    public VoucherTransactionService() {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get LoggerService object");
            
            throw new EJBException(e);
        }

        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get EmailSender object");

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ParametersService object");
            
            throw new EJBException(e);
        }

        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get FidelityService object");
            
            throw new EJBException(e);
        }

        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get FidelityService object");
            
            throw new EJBException(e);
        }
        
        try {
            this.unavailabilityPeriodService = EJBHomeCache.getInstance().getUnavailabilityPeriodService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
    }
    
    @Override
    public CreateVoucherTransactionResult createVoucherTransaction(String requestID, String ticketID, String pin, Double amount, Long paymentMethodID, String paymentMethodType) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("pin", pin));
        inputParameters.add(new Pair<String, String>("paymentMethodID", paymentMethodID.toString()));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createVoucherTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateVoucherTransactionResult createVoucherTransactionResult = new CreateVoucherTransactionResult();
        
        try {

            String emailSenderAddress = this.parametersService.getParamValue(VoucherTransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(VoucherTransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(VoucherTransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(VoucherTransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(VoucherTransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(VoucherTransactionService.PARAM_PROXY_NO_HOSTS);
            String acquirerID = this.parametersService.getParamValue(VoucherTransactionService.PARAM_ACQUIRER_ID);
            String shopLoginNewFlow = this.parametersService.getParamValue(VoucherTransactionService.PARAM_SHOPLOGIN_NEW_FLOW);
            String paymentType = this.parametersService.getParamValue(VoucherTransactionService.PARAM_PAYMENT_TYPE);
            String currency = this.parametersService.getParamValue(VoucherTransactionService.PARAM_UIC);
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(VoucherTransactionService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(VoucherTransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            
            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();
            
            String userBlockException = this.parametersService.getParamValue(VoucherTransactionService.PARAM_USER_BLOCK_EXCEPTION);
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
            
            createVoucherTransactionResult = createVoucherTransactionAction.execute(ticketID, requestID, pin, amount, shopLoginNewFlow, acquirerID, currency, paymentMethodID, 
                    paymentMethodType, paymentType, reconciliationMaxAttempts, pinCheckMaxAttempts, userBlockExceptionList, userCategoryService, emailSender, gpService, fidelityService, unavailabilityPeriodService, proxyHost, proxyPort, proxyNoHosts);
            
            /*
            createVoucherResponse = createVoucherTransaction.(amount, authorizationCode, shopTransactionID, bankTransactionID)transactionCreateRefuelAction.execute(ticketID, requestID, encodedPin, stationID, pumpID, pumpNumber, productID, productDescription, amount, amountVoucher,
                    useVoucher, currency, paymentType, paymentMethodId, paymentMethodType, outOfRange, refuelMode, shopLogin, acquirerID, pinCheckMaxAttempts, statusMaxAttempts,
                    reconciliationMaxAttempts, serverName, emailSender, userCategoryService, this);
            */
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createVoucherTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get GPService object");
            createVoucherTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createVoucherTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", createVoucherTransactionResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("voucherTransactionID", createVoucherTransactionResult.getVoucherTransactionID()));
        outputParameters.add(new Pair<String, String>("voucherCode", createVoucherTransactionResult.getVoucherCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createVoucherTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createVoucherTransactionResult;        
    }
    
    @Override
    public CreateApplePayVoucherTransactionResult createApplePayVoucherTransaction(String requestID, String ticketID, Double amount, String applePayPKPaymentToken) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        inputParameters.add(new Pair<String, String>("applePayPKPaymentToken", applePayPKPaymentToken));
        
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createApplePayVoucherTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateApplePayVoucherTransactionResult createApplePayVoucherTransactionResult = new CreateApplePayVoucherTransactionResult();
        
        try {

            String emailSenderAddress = this.parametersService.getParamValue(VoucherTransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(VoucherTransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(VoucherTransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(VoucherTransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(VoucherTransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(VoucherTransactionService.PARAM_PROXY_NO_HOSTS);
            String acquirerID = this.parametersService.getParamValue(VoucherTransactionService.PARAM_ACQUIRER_ID);
            String shopLoginNewFlow = this.parametersService.getParamValue(VoucherTransactionService.PARAM_SHOPLOGIN_NEW_FLOW);
            String paymentType = this.parametersService.getParamValue(VoucherTransactionService.PARAM_PAYMENT_TYPE);
            String currency = this.parametersService.getParamValue(VoucherTransactionService.PARAM_UIC);
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(VoucherTransactionService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(VoucherTransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            
            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();
            
            String userBlockException = this.parametersService.getParamValue(VoucherTransactionService.PARAM_USER_BLOCK_EXCEPTION);
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
            
            createApplePayVoucherTransactionResult = createApplePayVoucherTransactionAction.execute(ticketID, requestID, amount, shopLoginNewFlow, acquirerID, currency, applePayPKPaymentToken,
                    paymentType, reconciliationMaxAttempts, pinCheckMaxAttempts, userBlockExceptionList, userCategoryService, emailSender, gpService, fidelityService, unavailabilityPeriodService, proxyHost, proxyPort, proxyNoHosts);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get GPService object");
            createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createApplePayVoucherTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", createApplePayVoucherTransactionResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("voucherTransactionID", createApplePayVoucherTransactionResult.getVoucherTransactionID()));
        outputParameters.add(new Pair<String, String>("voucherCode", createApplePayVoucherTransactionResult.getVoucherCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createApplePayVoucherTransactionResult", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createApplePayVoucherTransactionResult;
    }
    
    @Override
    public RetrieveVoucherTransactionDataResponse retrieveVoucherTransactionDetail(String requestID, String ticketID, String voucherTransactionID) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("voucherTransactionID", voucherTransactionID));
        
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveVoucherTransactionDetail", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveVoucherTransactionDataResponse retrieveVoucherTransactionDetailResponse = new RetrieveVoucherTransactionDataResponse();
        
        try {

            retrieveVoucherTransactionDetailResponse = retrieveVoucherTransactionAction.execute(requestID, ticketID, voucherTransactionID);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrieveVoucherTransactionDetailResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveVoucherTransactionDetailResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("messageCode", retrieveVoucherTransactionDetailResponse.getMessageCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveVoucherTransactionDetail", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveVoucherTransactionDetailResponse;       
    }

}
