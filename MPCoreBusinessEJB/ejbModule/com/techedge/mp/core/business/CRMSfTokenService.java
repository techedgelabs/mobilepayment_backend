package com.techedge.mp.core.business;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.techedge.mp.core.actions.crm.TokenSFAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.utilities.EJBHomeCache;

/**
 * Session Bean implementation class UserManager
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class CRMSfTokenService implements CRMSfTokenServiceRemote, CRMSfTokenServiceLocal {

    @EJB
    private TokenSFAction        tokenSFAction;

    private ParametersService                 parametersService                           = null;
    private LoggerService                     loggerService                               = null;
    

    /**
     * Default constructor.
     */
    public CRMSfTokenService() throws EJBException {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface Logger not found: " + e.getMessage());
            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface Parameters not found: " + e.getMessage());
            throw new EJBException(e);
        }
    }

    /**
     * Se newToken==null restituisce il token salvato sulla CRM_SF_TOKEN 
     * Se newToken!=null aggiorna la tabella CRM_SF_TOKEN con il nuovo valore fornito in input
     * @param String newToken
     */
    @Override
    public String manageToken(String newToken) {
    	
    	String result=null;
        
    	try{
    		result=this.tokenSFAction.execute(newToken);
    	}catch(Exception e){
    		this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "CRMSfTokenService", null, null, "Errore : " + e.getMessage());
    	}
    	
        return result;
    }

}
