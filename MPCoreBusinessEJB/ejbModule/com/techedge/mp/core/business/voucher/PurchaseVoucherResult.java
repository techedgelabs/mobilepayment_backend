package com.techedge.mp.core.business.voucher;

import java.io.Serializable;

import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;

public class PurchaseVoucherResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4362098320896870476L;

    private VoucherTransactionBean voucherTransactionBean;
    private String statusCode;
    
    
    public VoucherTransactionBean getVoucherTransactionBean() {
        return voucherTransactionBean;
    }
    
    public void setVoucherTransactionBean(VoucherTransactionBean voucherTransactionBean) {
        this.voucherTransactionBean = voucherTransactionBean;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
    
    
    
    
    
    
    
}
