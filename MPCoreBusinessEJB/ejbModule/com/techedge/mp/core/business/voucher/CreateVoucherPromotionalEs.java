package com.techedge.mp.core.business.voucher;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.voucher.VoucherPromotionalBean;
import com.techedge.mp.core.business.model.voucher.VoucherPromotionalDetailBean;
import com.techedge.mp.core.business.model.voucher.VoucherPromotionalEsBean;
import com.techedge.mp.core.business.model.voucher.VoucherPromotionalEsDetailBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CreateVoucherPromotionalResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;

public class CreateVoucherPromotionalEs {
    private EntityManager em;
    private FidelityServiceRemote fidelityService;
    private UserBean userBean;
    private VoucherPromotionalEsBean voucherPromotionalEsBean;
    private CreateVoucherPromotionalResult createVoucherResult;
    
    public CreateVoucherPromotionalEs(EntityManager em, FidelityServiceRemote fidelityService, UserBean userBean) {
        this.em = em;  
        this.fidelityService = fidelityService;
        this.userBean = userBean;
        voucherPromotionalEsBean = new VoucherPromotionalEsBean();
    }

    public String create(String promoCode, Double amount) {
        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        PartnerType partnerType = PartnerType.MP;
        VoucherConsumerType voucherType = VoucherConsumerType.ENI;
        long requestTimestamp = new Date().getTime();
        String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
        BigDecimal totalAmount = BigDecimal.valueOf(amount);
        createVoucherResult = new CreateVoucherPromotionalResult();
        String response;
        
        voucherPromotionalEsBean.setPartnerType(partnerType);
        voucherPromotionalEsBean.setPromoCode(promoCode);
        voucherPromotionalEsBean.setVoucherType(voucherType.getValue());
        voucherPromotionalEsBean.setTotalAmount(totalAmount.doubleValue());
        voucherPromotionalEsBean.setFiscalCode(fiscalCode);
        
        em.persist(voucherPromotionalEsBean);
        
        VoucherPromotionalEsDetailBean voucherPromotionalDetailBean = new VoucherPromotionalEsDetailBean();
        voucherPromotionalDetailBean.setOperationID(operationID);
        voucherPromotionalDetailBean.setRequestTimestamp(new Date(requestTimestamp));
        voucherPromotionalDetailBean.setVoucherPromotionalEsBean(voucherPromotionalEsBean);
        
        try {
            createVoucherResult = fidelityService.createVoucherPromotional(operationID, voucherType, partnerType, requestTimestamp, fiscalCode, promoCode, totalAmount);
            
            if (createVoucherResult.getStatusCode().equals(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_OK)) {
                response = ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_SUCCESS;
                System.out.println("Creazione del voucher promozionale (" + promoCode + ") per l'utente (" + userBean.getId() + ")");
            }
            else {
                response = ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_CREATE_ERROR;
                System.out.println("Errore nella creazione del voucher promozionale (" + promoCode + ") per l'utente (" + userBean.getId() + "): "
                + createVoucherResult.getMessageCode());
            }
        }
        catch (FidelityServiceException ex) {
            System.err.println("Errore nella creazione del voucher promozionale (" + promoCode + "): " + ex.getMessage());
            createVoucherResult.setStatusCode(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_GENERIC_ERROR);
            createVoucherResult.setMessageCode(ex.getMessage());
            createVoucherResult.setCsTransactionID(null);
            response = ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_CREATE_ERROR;
        }
        
        voucherPromotionalDetailBean.setCsTransactionID(createVoucherResult.getCsTransactionID());
        voucherPromotionalDetailBean.setMessageCode(createVoucherResult.getMessageCode());
        voucherPromotionalDetailBean.setStatusCode(createVoucherResult.getStatusCode());
        
        em.persist(voucherPromotionalDetailBean);
        
        voucherPromotionalEsBean.getVoucherPromotionalEsDetailBeanList().add(voucherPromotionalDetailBean);
        
        return response;
    }
    
    public String associateToUser(CreateVoucherPromotionalResult createVoucherResult) {
        String response;
        VoucherDetail voucherDetail = createVoucherResult.getVoucher();
        try {
            VoucherBean voucherBean = new VoucherBean();
            voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
            voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
            voucherBean.setInitialValue(voucherDetail.getInitialValue());
            voucherBean.setPromoCode(voucherDetail.getPromoCode());
            voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
            voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
            voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
            voucherBean.setCode(voucherDetail.getVoucherCode());
            voucherBean.setStatus(voucherDetail.getVoucherStatus());
            voucherBean.setType(voucherDetail.getVoucherType());
            voucherBean.setValue(voucherDetail.getVoucherValue());
            voucherBean.setIsCombinable(voucherDetail.getIsCombinable());
            voucherBean.setMinAmount(voucherDetail.getMinAmount());
            voucherBean.setMinQuantity(voucherDetail.getMinQuantity());
            voucherBean.setPromoPartner(voucherDetail.getPromoPartner());
            voucherBean.setUserBean(userBean);
    
            em.persist(voucherBean);
    
            voucherPromotionalEsBean.setVoucherBean(voucherBean);
            em.merge(voucherPromotionalEsBean);
            
            System.out.println("Associato voucher promozionale (" + voucherDetail.getPromoCode() + ") a l'utente (" + userBean.getId() + ")");
            response = ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_SUCCESS;
        }
        catch (Exception ex) {
            System.err.println("Errore nell'associazione voucher promozionale (" + voucherDetail.getPromoCode() + ") a l'utente (" + userBean.getId() + ")");
            response = ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_ASSIGN_ERROR;
        }
        
        return response;
    }

    public VoucherPromotionalEsBean getVoucherPromotionalEsBean() {
        return voucherPromotionalEsBean;
    }

    public CreateVoucherPromotionalResult getCreateVoucherResult() {
        return createVoucherResult;
    }

}
