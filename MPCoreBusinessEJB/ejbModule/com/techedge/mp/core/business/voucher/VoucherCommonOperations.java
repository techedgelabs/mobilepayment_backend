package com.techedge.mp.core.business.voucher;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.interfaces.FidelityConsumeVoucherData;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.TransactionConsumeVoucherPreAuthResponse;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CancelPreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;

public class VoucherCommonOperations {

    public static String check(UserBean userBean, FidelityServiceRemote fidelityService, EntityManager entityManager) {

        if (userBean.getVoucherList().isEmpty()) {
    
            // L'utente non ha voucher associati
            System.out.println("No voucher found for user: " + userBean.getPersonalDataBean().getSecurityDataEmail());
                    return ResponseHelper.VOUCHER_UPDATER_CHECK_VOUCHER_NOT_FOUND;
        }
     
        // Prima di restituire i voucher bisogna verificarne lo stato
    
        Date now = new Date();
    
        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        PartnerType partnerType = PartnerType.MP;
        VoucherConsumerType voucherConsumerType = VoucherConsumerType.ENI;
        Long requestTimestamp = now.getTime();
    
        List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);
    
        for (VoucherBean voucherBean : userBean.getVoucherList()) {
    
            if ( voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO) || voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_DA_CONFERMARE)) {
                
                System.out.println("Voucher Bean Code " + voucherBean.getCode());
                VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                voucherCodeDetail.setVoucherCode(voucherBean.getCode());
                voucherCodeList.add(voucherCodeDetail);
            }
        }
    
        if (voucherCodeList.isEmpty()) {
    
            System.out.println("No voucher found for user: " + userBean.getPersonalDataBean().getSecurityDataEmail());
            return ResponseHelper.VOUCHER_UPDATER_CHECK_VOUCHER_NOT_FOUND;
        }
    
        CheckVoucherResult checkVoucherResult = new CheckVoucherResult();
    
        try {
    
            checkVoucherResult = fidelityService.checkVoucher(operationID, voucherConsumerType, partnerType, requestTimestamp, voucherCodeList);
        }
        catch (Exception e) {
            System.out.println("Error in check voucher for user " + userBean.getPersonalDataBean().getSecurityDataEmail() + ": " + e.getMessage());
            return ResponseHelper.VOUCHER_UPDATER_CHECK_SYSTEM_ERROR;
        }
    
        // Verifica l'esito del check
        String checkVoucherStatusCode = checkVoucherResult.getStatusCode();
    
        if (checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_OK)) {

            // Aggiorna lo stato dei voucher dell'utente
            for (VoucherBean voucherBean : userBean.getVoucherList()) {
    
                //System.out.println("Trovato voucher: " + voucherBean.getCode() + " in stato: " + voucherBean.getStatus());
    
                if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO) || 
                        voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_ESAURITO) ||
                        voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_DA_CONFERMARE)) {
    
                    // Ricerca il voucher nella risposta del servizio di verifica
                    for (VoucherDetail voucherDetail : checkVoucherResult.getVoucherList()) {
    
                        if (voucherDetail.getVoucherCode().equals(voucherBean.getCode())) {
    
                            System.out.println("Voucher trovato nella risposta del servizio di check");
    
                            // Aggiorna il voucher con i dati restituiti dal servizio
                            updateVoucher(voucherBean, voucherDetail, entityManager);
    
                            System.out.println("Voucher aggiornato");
                        }
                    }
                }
            }
            entityManager.merge(userBean);
        }
        return ResponseHelper.VOUCHER_UPDATER_CHECK_SUCCESS;
    }

    public static TransactionConsumeVoucherPreAuthResponse consumePreAuthorization(TransactionBean transactionBean, Double amount, FidelityServiceRemote fidelityService, EntityManager entityManager) {

        PrePaidConsumeVoucherBean preAuthorizationConsumeVoucherBean = null;
        TransactionConsumeVoucherPreAuthResponse transactionConsumeVoucherPreAuthResponse = new TransactionConsumeVoucherPreAuthResponse();
        
        for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {
            
            if (prePaidConsumeVoucherBean.getOperationType().equals("PRE-AUTHORIZATION")) {
                
                preAuthorizationConsumeVoucherBean = prePaidConsumeVoucherBean;
                
                System.out.println("Trovata operazione di preautorizzazione voucher con operation id " + preAuthorizationConsumeVoucherBean.getPreAuthOperationID());
            }
        }
        
        if ( preAuthorizationConsumeVoucherBean == null ) {
            
            // Errore preautorizzazione non trovata
            
            System.err.println("Error pre authorization consume voucher not found");

            transactionConsumeVoucherPreAuthResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CONSUME_PRE_AUTH_ID_NOT_FOUND);
            transactionConsumeVoucherPreAuthResponse.setFidelityStatusCode(null);
            transactionConsumeVoucherPreAuthResponse.setFidelityStatusMessage("Error pre authorization consume voucher not found");

            return transactionConsumeVoucherPreAuthResponse;
            
        }
        
        UserBean userBean = transactionBean.getUserBean();
        
        System.out.println("Consumo voucher preautorizzati");

        // Chimamata al servizio consumeVoucher

        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        PartnerType partnerType = PartnerType.MP;
        VoucherConsumerType voucherType = VoucherConsumerType.ENI;
        Long requestTimestamp = new Date().getTime();
        String stationID = transactionBean.getStationBean().getStationID();
        String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;
        String language = FidelityConstants.LANGUAGE_ITALIAN;

        
        //caricamento della ProductList dalla transazione; � un caricamento fittizio di una transazione refuel
        String refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_PREPAY;
        List<ProductDetail> productList = getProductList(transactionBean, amount);

        ConsumeVoucherResult consumeVoucherResult = new ConsumeVoucherResult();

        Double totalConsumed = 0.0;
        PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
        prePaidConsumeVoucherBean.setOperationID(operationID);
        prePaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
        prePaidConsumeVoucherBean.setOperationType("CONSUME");
        prePaidConsumeVoucherBean.setTransactionBean(transactionBean);

        try {

            consumeVoucherResult = fidelityService.consumeVoucher(operationID, transactionBean.getTransactionID(), voucherType, stationID, refuelMode, paymentMode, 
                    language, partnerType, requestTimestamp, productList, null, FidelityConstants.CONSUME_TYPE_PARTIAL, preAuthorizationConsumeVoucherBean.getPreAuthOperationID());

            prePaidConsumeVoucherBean.setCsTransactionID(consumeVoucherResult.getCsTransactionID());
            prePaidConsumeVoucherBean.setMarketingMsg(consumeVoucherResult.getMarketingMsg());
            prePaidConsumeVoucherBean.setMessageCode(consumeVoucherResult.getMessageCode());
            prePaidConsumeVoucherBean.setStatusCode(consumeVoucherResult.getStatusCode());
            prePaidConsumeVoucherBean.setWarningMsg(consumeVoucherResult.getWarningMsg());
            entityManager.persist(prePaidConsumeVoucherBean);

            for (VoucherDetail voucherDetail : consumeVoucherResult.getVoucherList()) {

                if (voucherDetail.getConsumedValue() == 0.0) {

                    System.out.println(voucherDetail.getVoucherCode() + ": Valore consumato 0.0 -> aggiornamento non necessario");
                    continue;
                }

                PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean = new PrePaidConsumeVoucherDetailBean();

                prePaidConsumeVoucherDetailBean.setConsumedValue(voucherDetail.getConsumedValue());
                prePaidConsumeVoucherDetailBean.setExpirationDate(voucherDetail.getExpirationDate());
                prePaidConsumeVoucherDetailBean.setInitialValue(voucherDetail.getInitialValue());
                prePaidConsumeVoucherDetailBean.setPromoCode(voucherDetail.getPromoCode());
                prePaidConsumeVoucherDetailBean.setPromoDescription(voucherDetail.getPromoDescription());
                prePaidConsumeVoucherDetailBean.setPromoDoc(voucherDetail.getPromoDoc());
                prePaidConsumeVoucherDetailBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                prePaidConsumeVoucherDetailBean.setVoucherCode(voucherDetail.getVoucherCode());
                prePaidConsumeVoucherDetailBean.setVoucherStatus(voucherDetail.getVoucherStatus());
                prePaidConsumeVoucherDetailBean.setVoucherType(voucherDetail.getVoucherType());
                prePaidConsumeVoucherDetailBean.setVoucherValue(voucherDetail.getVoucherValue());
                prePaidConsumeVoucherDetailBean.setPrePaidConsumeVoucherBean(prePaidConsumeVoucherBean);
                
                entityManager.persist(prePaidConsumeVoucherDetailBean);

                prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailBean().add(prePaidConsumeVoucherDetailBean);

                totalConsumed = totalConsumed + voucherDetail.getConsumedValue();

                // Aggiorna le informazioni sul voucher associato all'utente

                System.out.println("Aggiornamento voucher utente");

                for (VoucherBean voucherBean : userBean.getVoucherList()) {

                    if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                        System.out.println("Aggiornamento voucher " + voucherDetail.getVoucherCode());

                        voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                        voucherBean.setInitialValue(voucherDetail.getInitialValue());
                        voucherBean.setPromoCode(voucherDetail.getPromoCode());
                        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                        voucherBean.setCode(voucherDetail.getVoucherCode());
                        voucherBean.setStatus(voucherDetail.getVoucherStatus());
                        voucherBean.setType(voucherDetail.getVoucherType());
                        voucherBean.setValue(voucherDetail.getVoucherValue());
                    }
                }
            }

            // Aggiornamento dei dati utente
            entityManager.merge(userBean);

            prePaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
            transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

            System.out.println("Totale pagato con voucher: " + totalConsumed);

            entityManager.persist(prePaidConsumeVoucherBean);
            entityManager.merge(transactionBean);

            transactionConsumeVoucherPreAuthResponse.setFidelityStatusCode(consumeVoucherResult.getStatusCode());
            transactionConsumeVoucherPreAuthResponse.setFidelityStatusMessage(consumeVoucherResult.getMessageCode());                    
            
            transactionConsumeVoucherPreAuthResponse.setFidelityConsumeVoucherData(new FidelityConsumeVoucherData());
            transactionConsumeVoucherPreAuthResponse.getFidelityConsumeVoucherData().setCsTransactionID(
                    consumeVoucherResult.getCsTransactionID());
            transactionConsumeVoucherPreAuthResponse.getFidelityConsumeVoucherData().setMessageCode(
                    consumeVoucherResult.getMessageCode());
            transactionConsumeVoucherPreAuthResponse.getFidelityConsumeVoucherData().setStatusCode(
                    consumeVoucherResult.getStatusCode());

            if (consumeVoucherResult.getStatusCode().equals("00")) {
                transactionConsumeVoucherPreAuthResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CONSUME_SUCCESS);
            }
            else {
                transactionConsumeVoucherPreAuthResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CONSUME_FAILURE);
            }
            
            return transactionConsumeVoucherPreAuthResponse;

        }
        catch (Exception e) {

            System.err.println("Error consuming vouchers: " + e.getMessage());

            prePaidConsumeVoucherBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
            prePaidConsumeVoucherBean.setMessageCode("STATO DA VERIFICARE (" + e.getMessage() + ")");
            prePaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
            prePaidConsumeVoucherBean.setTransactionBean(transactionBean);
            
            transactionBean.setVoucherReconciliation(true);
            transactionBean.setVoucherStatus(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
            transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

            entityManager.persist(prePaidConsumeVoucherBean);
            entityManager.merge(transactionBean);
            
            transactionConsumeVoucherPreAuthResponse.setFidelityStatusCode(null);
            transactionConsumeVoucherPreAuthResponse.setFidelityStatusMessage(e.getMessage());
            transactionConsumeVoucherPreAuthResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CONSUME_SYSTEM_ERROR);

            return transactionConsumeVoucherPreAuthResponse;

        }
        
    }
    
    public static TransactionConsumeVoucherPreAuthResponse consume(TransactionBean transactionBean, Double amount, FidelityServiceRemote fidelityService, EntityManager entityManager) {

        TransactionConsumeVoucherPreAuthResponse transactionConsumeVoucherPreAuthResponse = new TransactionConsumeVoucherPreAuthResponse();
        UserBean userBean = transactionBean.getUserBean();
        
        List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

        for (VoucherBean voucherBean : userBean.getVoucherList()) {

            System.out.println("Trovato voucher " + voucherBean.getCode() + " con stato " + voucherBean.getStatus());

            if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {

                System.out.println("Voucher inserito in richiesta");

                VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                voucherCodeDetail.setVoucherCode(voucherBean.getCode());
                voucherCodeList.add(voucherCodeDetail);
            }
        }
        
        System.out.println("Consumo voucher promozionali");

        // Chimamata al servizio consumeVoucher

        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        PartnerType partnerType = PartnerType.MP;
        VoucherConsumerType voucherType = VoucherConsumerType.ENI;
        Long requestTimestamp = new Date().getTime();
        String stationID = transactionBean.getStationBean().getStationID();
        String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;
        String language = FidelityConstants.LANGUAGE_ITALIAN;

        
        //caricamento della ProductList dalla transazione; � un caricamento fittizio di una transazione refuel
        String refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_PREPAY;
        List<ProductDetail> productList = getProductList(transactionBean, amount);

        ConsumeVoucherResult consumeVoucherResult = new ConsumeVoucherResult();

        Double totalConsumed = 0.0;
        PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
        prePaidConsumeVoucherBean.setOperationID(operationID);
        prePaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
        prePaidConsumeVoucherBean.setOperationType("CONSUME");
        prePaidConsumeVoucherBean.setTransactionBean(transactionBean);

        try {

            consumeVoucherResult = fidelityService.consumeVoucher(operationID, transactionBean.getTransactionID(), voucherType, stationID, refuelMode, paymentMode, 
                    language, partnerType, requestTimestamp, productList, voucherCodeList, FidelityConstants.CONSUME_TYPE_PARTIAL, null);

            prePaidConsumeVoucherBean.setCsTransactionID(consumeVoucherResult.getCsTransactionID());
            prePaidConsumeVoucherBean.setMarketingMsg(consumeVoucherResult.getMarketingMsg());
            prePaidConsumeVoucherBean.setMessageCode(consumeVoucherResult.getMessageCode());
            prePaidConsumeVoucherBean.setStatusCode(consumeVoucherResult.getStatusCode());
            prePaidConsumeVoucherBean.setWarningMsg(consumeVoucherResult.getWarningMsg());
            
            entityManager.persist(prePaidConsumeVoucherBean);

            for (VoucherDetail voucherDetail : consumeVoucherResult.getVoucherList()) {

                if (voucherDetail.getConsumedValue() == 0.0) {

                    System.out.println(voucherDetail.getVoucherCode() + ": Valore consumato 0.0 -> aggiornamento non necessario");
                    continue;
                }

                PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean = new PrePaidConsumeVoucherDetailBean();

                prePaidConsumeVoucherDetailBean.setConsumedValue(voucherDetail.getConsumedValue());
                prePaidConsumeVoucherDetailBean.setExpirationDate(voucherDetail.getExpirationDate());
                prePaidConsumeVoucherDetailBean.setInitialValue(voucherDetail.getInitialValue());
                prePaidConsumeVoucherDetailBean.setPromoCode(voucherDetail.getPromoCode());
                prePaidConsumeVoucherDetailBean.setPromoDescription(voucherDetail.getPromoDescription());
                prePaidConsumeVoucherDetailBean.setPromoDoc(voucherDetail.getPromoDoc());
                prePaidConsumeVoucherDetailBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                prePaidConsumeVoucherDetailBean.setVoucherCode(voucherDetail.getVoucherCode());
                prePaidConsumeVoucherDetailBean.setVoucherStatus(voucherDetail.getVoucherStatus());
                prePaidConsumeVoucherDetailBean.setVoucherType(voucherDetail.getVoucherType());
                prePaidConsumeVoucherDetailBean.setVoucherValue(voucherDetail.getVoucherValue());
                prePaidConsumeVoucherDetailBean.setPrePaidConsumeVoucherBean(prePaidConsumeVoucherBean);
                
                entityManager.persist(prePaidConsumeVoucherDetailBean);

                prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailBean().add(prePaidConsumeVoucherDetailBean);

                totalConsumed = totalConsumed + voucherDetail.getConsumedValue();

                // Aggiorna le informazioni sul voucher associato all'utente

                System.out.println("Aggiornamento voucher utente");

                for (VoucherBean voucherBean : userBean.getVoucherList()) {

                    if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                        System.out.println("Aggiornamento voucher " + voucherDetail.getVoucherCode());

                        voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                        voucherBean.setInitialValue(voucherDetail.getInitialValue());
                        voucherBean.setPromoCode(voucherDetail.getPromoCode());
                        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                        voucherBean.setCode(voucherDetail.getVoucherCode());
                        voucherBean.setStatus(voucherDetail.getVoucherStatus());
                        voucherBean.setType(voucherDetail.getVoucherType());
                        voucherBean.setValue(voucherDetail.getVoucherValue());
                    }
                }
            }

            // Aggiornamento dei dati utente
            entityManager.merge(userBean);

            prePaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
            transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

            System.out.println("Totale pagato con voucher: " + totalConsumed);

            entityManager.persist(prePaidConsumeVoucherBean);
            entityManager.merge(transactionBean);

            transactionConsumeVoucherPreAuthResponse.setFidelityStatusCode(consumeVoucherResult.getStatusCode());
            transactionConsumeVoucherPreAuthResponse.setFidelityStatusMessage(consumeVoucherResult.getMessageCode());                    
            
            transactionConsumeVoucherPreAuthResponse.setFidelityConsumeVoucherData(new FidelityConsumeVoucherData());
            transactionConsumeVoucherPreAuthResponse.getFidelityConsumeVoucherData().setCsTransactionID(
                    consumeVoucherResult.getCsTransactionID());
            transactionConsumeVoucherPreAuthResponse.getFidelityConsumeVoucherData().setMessageCode(
                    consumeVoucherResult.getMessageCode());
            transactionConsumeVoucherPreAuthResponse.getFidelityConsumeVoucherData().setStatusCode(
                    consumeVoucherResult.getStatusCode());

            if (consumeVoucherResult.getStatusCode().equals("00")) {
                transactionConsumeVoucherPreAuthResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CONSUME_SUCCESS);
            }
            else {
                transactionConsumeVoucherPreAuthResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CONSUME_FAILURE);
            }
            
            return transactionConsumeVoucherPreAuthResponse;

        }
        catch (Exception e) {

            System.err.println("Error consuming vouchers: " + e.getMessage());

            prePaidConsumeVoucherBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
            prePaidConsumeVoucherBean.setMessageCode("STATO DA VERIFICARE (" + e.getMessage() + ")");
            prePaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
            prePaidConsumeVoucherBean.setTransactionBean(transactionBean);
            
            transactionBean.setVoucherReconciliation(true);
            transactionBean.setVoucherStatus(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
            transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

            entityManager.persist(prePaidConsumeVoucherBean);
            entityManager.merge(transactionBean);
            
            transactionConsumeVoucherPreAuthResponse.setFidelityStatusCode(null);
            transactionConsumeVoucherPreAuthResponse.setFidelityStatusMessage(e.getMessage());
            transactionConsumeVoucherPreAuthResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CONSUME_SYSTEM_ERROR);

            return transactionConsumeVoucherPreAuthResponse;

        }
        
    }
    
    public static TransactionCancelPreAuthorizationConsumeVoucherResponse cancelPreAuthorizationConsume(TransactionBean transactionBean, FidelityServiceRemote fidelityService, EntityManager entityManager) {
        
        TransactionCancelPreAuthorizationConsumeVoucherResponse transactionCancelPreAuthorizationConsumeVoucherResponse = new TransactionCancelPreAuthorizationConsumeVoucherResponse();
        PrePaidConsumeVoucherBean preAuthorizationConsumeVoucherBean = null;
        
        for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {
            
            if (prePaidConsumeVoucherBean.getOperationType().equals("PRE-AUTHORIZATION")) {
                
                preAuthorizationConsumeVoucherBean = prePaidConsumeVoucherBean;
            }
        }
        
        if ( preAuthorizationConsumeVoucherBean == null ) {
            
            // Errore preautorizzazione non trovata
            
            System.err.println("Error pre authorization consume voucher not found");

            transactionCancelPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CANCEL_PRE_AUTH_PRE_AUTH_ID_NOT_FOUND);
            transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusCode(null);
            transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusMessage("Error pre authorization consume voucher not found");

            return transactionCancelPreAuthorizationConsumeVoucherResponse;
            
        }

        System.out.println("Inizio Cancellazione preautorizzazione consumo voucher");

        // Chimamata al servizio cancelPreAuthorizationConsumeVoucher

        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        String preAuthOperationIDToCancel = preAuthorizationConsumeVoucherBean.getOperationID();

        PartnerType partnerType = PartnerType.MP;
        Long requestTimestamp = new Date().getTime();

        CancelPreAuthorizationConsumeVoucherResult cancelPreAuthorizationConsumeVoucherResult = new CancelPreAuthorizationConsumeVoucherResult();

        PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
        prePaidConsumeVoucherBean.setOperationID(operationID);
        prePaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
        prePaidConsumeVoucherBean.setOperationType("CANCEL-PRE-AUTHORIZATION");
        prePaidConsumeVoucherBean.setTransactionBean(transactionBean);

        try {

            cancelPreAuthorizationConsumeVoucherResult = fidelityService.cancelPreAuthorizationConsumeVoucher(operationID, preAuthOperationIDToCancel, partnerType,
                    requestTimestamp);

            prePaidConsumeVoucherBean.setCsTransactionID(cancelPreAuthorizationConsumeVoucherResult.getCsTransactionID());
            prePaidConsumeVoucherBean.setMessageCode(cancelPreAuthorizationConsumeVoucherResult.getMessageCode());
            prePaidConsumeVoucherBean.setStatusCode(cancelPreAuthorizationConsumeVoucherResult.getStatusCode());
            prePaidConsumeVoucherBean.setOperationIDReversed(preAuthOperationIDToCancel);
            
            entityManager.persist(prePaidConsumeVoucherBean);
            
            transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);
            
            entityManager.merge(transactionBean);

            transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusCode(cancelPreAuthorizationConsumeVoucherResult.getStatusCode());
            transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusMessage(cancelPreAuthorizationConsumeVoucherResult.getMessageCode());
            
            transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityConsumeVoucherData(new FidelityConsumeVoucherData());
            transactionCancelPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setCsTransactionID(
                    cancelPreAuthorizationConsumeVoucherResult.getCsTransactionID());
            transactionCancelPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setMessageCode(
                    cancelPreAuthorizationConsumeVoucherResult.getMessageCode());
            transactionCancelPreAuthorizationConsumeVoucherResponse.getFidelityConsumeVoucherData().setStatusCode(
                    cancelPreAuthorizationConsumeVoucherResult.getStatusCode());

            if (cancelPreAuthorizationConsumeVoucherResult.getStatusCode().equals("00")) {
                transactionCancelPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CANCEL_PRE_AUTH_SUCCESS);
            }
            else {
                transactionCancelPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CANCEL_PRE_AUTH_FAILURE);
            }
            
            check(transactionBean.getUserBean(), fidelityService, entityManager);
            
            System.out.println("Result StatusCode: " + cancelPreAuthorizationConsumeVoucherResult.getStatusCode());
            System.out.println("Result StatusMessage: " + cancelPreAuthorizationConsumeVoucherResult.getMessageCode());
            
            System.out.println("Response FidelityStatusCode: " + transactionCancelPreAuthorizationConsumeVoucherResponse.getFidelityStatusCode());
            System.out.println("Response FidelityStatusMessage: " + transactionCancelPreAuthorizationConsumeVoucherResponse.getFidelityStatusMessage());
            System.out.println("Response StatusCode: " + transactionCancelPreAuthorizationConsumeVoucherResponse.getStatusCode());
            
            return transactionCancelPreAuthorizationConsumeVoucherResponse;

        }
        catch (Exception e) {

            System.err.println("Error in cancel pre authorization consume voucher: " + e.getMessage());

            transactionCancelPreAuthorizationConsumeVoucherResponse.setStatusCode(ResponseHelper.VOUCHER_UPDATER_CANCEL_PRE_AUTH_SYSTEM_ERROR);
            transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusCode(null);
            transactionCancelPreAuthorizationConsumeVoucherResponse.setFidelityStatusMessage(e.getMessage());

            return transactionCancelPreAuthorizationConsumeVoucherResponse;

        }        
    }

    private static void updateVoucher(VoucherBean voucherBean, VoucherDetail voucherDetail, EntityManager em) {

        double consumedValue = voucherDetail.getConsumedValue().doubleValue();

        if (consumedValue == 0.0) {
            consumedValue = voucherDetail.getVoucherValue().doubleValue() - voucherDetail.getVoucherBalanceDue().doubleValue();
        }

        voucherBean.setConsumedValue(consumedValue);
        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
        voucherBean.setInitialValue(voucherDetail.getInitialValue());
        voucherBean.setPromoCode(voucherDetail.getPromoCode());
        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
        voucherBean.setCode(voucherDetail.getVoucherCode());
        voucherBean.setStatus(voucherDetail.getVoucherStatus());
        voucherBean.setType(voucherDetail.getVoucherType());
        voucherBean.setValue(voucherDetail.getVoucherValue());
        voucherBean.setMinAmount(voucherDetail.getMinAmount());
        voucherBean.setMinQuantity(voucherDetail.getMinQuantity());
        voucherBean.setIsCombinable(voucherDetail.getIsCombinable());
        voucherBean.setValidPV(voucherDetail.getValidPV());
        /*
         * for (ProductDetail productDetail : voucherDetail.getValidProduct()) {
         * voucherBean.getValidProducts().add(productDetail.getProductCode());
         * }
         * 
         * for (RefuelMode refuelMode : voucherDetail.getValidRefuelMode()) {
         * voucherBean.getValidRefuelMode().add(refuelMode.getValue());
         * }
         */
    }

    private static List<ProductDetail> getProductList(TransactionBean transactionBean, Double amount) {
        List<ProductDetail> productList = new ArrayList<ProductDetail>(0);

        ProductDetail productDetail = new ProductDetail();
        productDetail.setAmount(amount);

        if (transactionBean.getProductID() == null) {
            productDetail.setProductCode(null);
        }
        else {
            if (transactionBean.getProductID().equals("SP")) {

                // sp
                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_SP);
            }
            else {

                if (transactionBean.getProductID().equals("GG")) {

                    // gasolio
                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GASOLIO);
                }
                else {

                    if (transactionBean.getProductID().equals("BS")) {

                        // blue_super
                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_SUPER);
                    }
                    else {

                        if (transactionBean.getProductID().equals("BD")) {

                            // blue_diesel
                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_DIESEL);
                        }
                        else {

                            if (transactionBean.getProductID().equals("MT")) {

                                // metano
                                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_METANO);
                            }
                            else {

                                if (transactionBean.getProductID().equals("GP")) {

                                    // gpl
                                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GPL);
                                }
                                else {

                                    if (transactionBean.getProductID().equals("AD")) {

                                        // ???
                                        productDetail.setProductCode(null);
                                    }
                                    else {

                                        // non_oil
                                        productDetail.setProductCode(null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        productDetail.setQuantity(transactionBean.getFuelQuantity());
        productList.add(productDetail);

        return productList;

    }
    
    
}
