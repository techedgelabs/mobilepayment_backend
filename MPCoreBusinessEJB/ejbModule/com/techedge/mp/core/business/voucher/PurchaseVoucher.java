package com.techedge.mp.core.business.voucher;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionEventBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionOperationBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionStatusBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CreateVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public class PurchaseVoucher {
    
    private EntityManager em;
    private VoucherTransactionBean voucherTransactionBean;
    private GPServiceRemote gpService;
    private FidelityServiceRemote fidelityService;
    private EmailSenderRemote emailSender;
    //private String createVoucherOperationID;
    //private Double createVoucherAmount;
    private String proxyHost;
    private String proxyPort;
    private String proxyNoHosts;
    
    public PurchaseVoucher(EntityManager em, GPServiceRemote gpService, FidelityServiceRemote fidelityService, 
            EmailSenderRemote emailSender, String proxyHost, String proxyPort, String proxyNoHosts) {
        
        this.em = em;  
        this.gpService = gpService;
        this.fidelityService = fidelityService;
        this.emailSender = emailSender;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.proxyNoHosts = proxyNoHosts;
    }

    private void initializeTransaction(UserBean userBean, Double amount, String shopLogin, String acquirerID, String currency, PaymentInfoBean paymentInfoBean, 
            String paymentMethodType, String paymentType, Integer sequenceID, Integer reconciliationMaxAttemps) {
        
        String transactionID = new IdGenerator().generateId(16).substring(0, 32);
        
        voucherTransactionBean = new VoucherTransactionBean();
        voucherTransactionBean.setVoucherTransactionID(transactionID);
        voucherTransactionBean.setAmount(amount);
        voucherTransactionBean.setShopLogin(shopLogin);
        voucherTransactionBean.setAcquirerID(acquirerID);
        voucherTransactionBean.setCreationTimestamp(new Date());
        voucherTransactionBean.setCurrency(currency);
        voucherTransactionBean.setUserBean(userBean);
        voucherTransactionBean.setPaymentMethodId(paymentInfoBean.getId());
        voucherTransactionBean.setToken(paymentInfoBean.getToken());
        voucherTransactionBean.setPaymentMethodType(paymentMethodType);
        voucherTransactionBean.setPaymentType(paymentType);
        voucherTransactionBean.setConfirmed(false);
        voucherTransactionBean.setVoucherCode(null);
        voucherTransactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttemps);
               
        em.persist(voucherTransactionBean);

        //VoucherTransactionStatusBean voucherTransactionStatusBean = generateVoucherTransactionStatusBean(1, StatusHelper.VOUCHER_STATUS_PURCHASE_START, null, null);
        //em.persist(voucherTransactionStatusBean);
        
        //voucherTransactionBean.getTransactionStatusBeanList().add(voucherTransactionStatusBean);
        
        VoucherTransactionStatusBean voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(1, StatusHelper.VOUCHER_STATUS_PURCHASE_START, "START PURCHASE", null);
        em.persist(voucherTransactionStatusBean);
        em.merge(voucherTransactionBean);

        em.flush();
    }
    

    public /*PurchaseVoucherResult*/ VoucherTransactionBean buy(UserBean userBean, Double amount, String shopLogin, String acquirerID, String currency, 
            PaymentInfoBean paymentInfoBean, String paymentMethodType, String paymentType, Integer reconciliationMaxAttemps) throws Exception {

        //PurchaseVoucherResult result = new PurchaseVoucherResult();
        
        Extension[] extension_array = new Extension[1];
        Extension i_extension = new Extension();
        i_extension.setKey("CST_DISTRID");
        i_extension.setValue("");
        extension_array[0] = i_extension;
        
        Integer statusSequenceID = 1;
        Integer eventSequenceID = 1;
        
        initializeTransaction(userBean, amount, shopLogin, acquirerID, currency, paymentInfoBean, paymentMethodType, paymentType, statusSequenceID, reconciliationMaxAttemps);
        
        statusSequenceID += 1;
        
        GestPayData gestPayDataAUTHResponse = doPaymentAuth(statusSequenceID, eventSequenceID, extension_array);
        
        if (gestPayDataAUTHResponse.getTransactionResult().equals("ERROR")) {
            voucherTransactionBean.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_AUTH);
            voucherTransactionBean.setEndTimestamp(new Date());
            em.merge(voucherTransactionBean);
            
            Email.sendVoucherSummary(emailSender, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
            return voucherTransactionBean;
            //result.setVoucherTransactionBean(voucherTransactionBean);
            //return result;
        }

        if (gestPayDataAUTHResponse.getTransactionResult().equals("KO")) {
            voucherTransactionBean.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_PAY_AUTH);
            voucherTransactionBean.setEndTimestamp(new Date());
            em.merge(voucherTransactionBean);
            
            Email.sendVoucherSummary(emailSender, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
            return voucherTransactionBean;
            //result.setVoucherTransactionBean(voucherTransactionBean);
            //return result;
        }
        
        statusSequenceID += 1;
        eventSequenceID += 1;

        CreateVoucherResult createVoucherResult = doCreate(statusSequenceID, eventSequenceID);
        
        if (createVoucherResult.getStatusCode().equals(FidelityResponse.CREATE_VOUCHER_SYSTEM_ERROR)) {
            voucherTransactionBean.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            voucherTransactionBean.setEndTimestamp(new Date());
            em.merge(voucherTransactionBean);
            
            Email.sendVoucherSummary(emailSender, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
            return voucherTransactionBean;
            //result.setVoucherTransactionBean(voucherTransactionBean);
            //return result;
        }

        if (!createVoucherResult.getStatusCode().equals(FidelityResponse.CREATE_VOUCHER_OK)) {
            
            statusSequenceID += 1;
            eventSequenceID += 1;

            GestPayData gestPayDataDELETEResponse = doPaymentAuthDelete(statusSequenceID, eventSequenceID);
            
            if (!gestPayDataDELETEResponse.getTransactionResult().equals("OK")) {
                voucherTransactionBean.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_CAN);
                voucherTransactionBean.setEndTimestamp(new Date());
                em.merge(voucherTransactionBean);
                
                Email.sendVoucherSummary(emailSender, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
                return voucherTransactionBean;
                //result.setVoucherTransactionBean(voucherTransactionBean);
                //return result;
            }

            voucherTransactionBean.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_CREATE);
            voucherTransactionBean.setEndTimestamp(new Date());
            em.merge(voucherTransactionBean);
            
            Email.sendVoucherSummary(emailSender, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
            return voucherTransactionBean;
            //result.setVoucherTransactionBean(voucherTransactionBean);
            //return result;
        }
        
        statusSequenceID += 1;
        eventSequenceID += 1;

        GestPayData gestPayDataSETTLEResponse = doPaymentSettle(statusSequenceID, eventSequenceID, extension_array);
        
        if (gestPayDataSETTLEResponse.getTransactionResult().equals("ERROR")) {
            voucherTransactionBean.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_MOV);
            voucherTransactionBean.setEndTimestamp(new Date());
            em.merge(voucherTransactionBean);
            
            Email.sendVoucherSummary(emailSender, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
            return voucherTransactionBean;
            //result.setVoucherTransactionBean(voucherTransactionBean);
            //return result;
        }

        if (gestPayDataSETTLEResponse.getTransactionResult().equals("KO")) {
            
            statusSequenceID += 1;
            eventSequenceID += 1;
            
            DeleteVoucherResult deleteVoucherResult = doDelete(statusSequenceID, eventSequenceID);
            
            if (!deleteVoucherResult.getStatusCode().equals(FidelityResponse.DELETE_VOUCHER_OK)) {
                voucherTransactionBean.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_DELETE);
                voucherTransactionBean.setEndTimestamp(new Date());
                em.merge(voucherTransactionBean);
                
                Email.sendVoucherSummary(emailSender, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
                return voucherTransactionBean;
            }
            
            statusSequenceID += 1;
            eventSequenceID += 1;

            GestPayData gestPayDataDELETEResponse = doPaymentAuthDelete(statusSequenceID, eventSequenceID);
            
            if (gestPayDataDELETEResponse.getTransactionResult().equals("ERROR")) {
                voucherTransactionBean.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_CAN);
                voucherTransactionBean.setEndTimestamp(new Date());
                em.merge(voucherTransactionBean);
                
                Email.sendVoucherSummary(emailSender, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
                return voucherTransactionBean;
                //result.setVoucherTransactionBean(voucherTransactionBean);
                //return result;
            }

            voucherTransactionBean.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_PAY_MOV);
            voucherTransactionBean.setEndTimestamp(new Date());
            em.merge(voucherTransactionBean);
            
            Email.sendVoucherSummary(emailSender, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
            return voucherTransactionBean;
            //result.setVoucherTransactionBean(voucherTransactionBean);
            //return result;
        }

        VoucherDetail voucherDetail = createVoucherResult.getVoucher();
        VoucherBean voucherBean = new VoucherBean();
        voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
        voucherBean.setInitialValue(voucherDetail.getInitialValue());
        voucherBean.setPromoCode(voucherDetail.getPromoCode());
        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
        voucherBean.setCode(voucherDetail.getVoucherCode());
        voucherBean.setStatus(voucherDetail.getVoucherStatus());
        voucherBean.setType(voucherDetail.getVoucherType());
        voucherBean.setValue(voucherDetail.getVoucherValue());
        voucherBean.setIsCombinable(voucherDetail.getIsCombinable());
        voucherBean.setMinAmount(voucherDetail.getMinAmount());
        voucherBean.setMinQuantity(voucherDetail.getMinQuantity());
        voucherBean.setPromoPartner(voucherDetail.getPromoPartner());
        voucherBean.setUserBean(voucherTransactionBean.getUserBean());
        em.persist(voucherBean);
        
        voucherTransactionBean.setVoucherCode(voucherDetail.getVoucherCode());
        voucherTransactionBean.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_SUCCESSFUL);
        voucherTransactionBean.setConfirmed(true);
        voucherTransactionBean.setEndTimestamp(new Date());
        em.merge(voucherTransactionBean);
        
        Email.sendVoucherSummary(emailSender, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
        return voucherTransactionBean;
    }
    
    
    
    private GestPayData doPaymentAuth(Integer statusSequenceID, Integer eventSequenceID, Extension[] extension_array) {
        
        Double amount = voucherTransactionBean.getAmount();
        String token = voucherTransactionBean.getToken();
        String currency = voucherTransactionBean.getCurrency();
        String transactionID = voucherTransactionBean.getVoucherTransactionID();
        String shopLogin = voucherTransactionBean.getShopLogin();
        String acquirerId = voucherTransactionBean.getAcquirerID();
        String groupAcquirer = voucherTransactionBean.getGroupAcquirer();
        String encodedSecretKey = voucherTransactionBean.getEncodedSecretKey();
        String status = StatusHelper.VOUCHER_STATUS_AUTH_OK;
        //Timestamp statusTimestamp = new Timestamp(new Date().getTime());
        //String requestID = String.valueOf(new Date().getTime());

        GestPayData gestPayData = gpService.callPagam(amount, transactionID, shopLogin, currency, token, null, acquirerId, groupAcquirer, encodedSecretKey, "", extension_array, "CUSTOMER");

        if (gestPayData == null) {
            gestPayData = new GestPayData();
            gestPayData.setTransactionResult("ERROR");
            gestPayData.setErrorCode("9999");
            gestPayData.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            gestPayData.setAmount(amount.toString());
            status = StatusHelper.VOUCHER_STATUS_AUTH_ERROR;
        }
        else {
            if (gestPayData.getTransactionResult().equalsIgnoreCase("KO")) {
                status = StatusHelper.VOUCHER_STATUS_AUTH_KO;
            }
        }

        System.out.println("Operazione GestPay AUTH: " + gestPayData.getTransactionResult());
                
        String errorCode = gestPayData.getErrorCode();
        String errorDescription = gestPayData.getErrorDescription();
        String eventResult = gestPayData.getTransactionResult();
        Double eventAmount = Double.valueOf(gestPayData.getAmount());

        //VoucherTransactionStatusBean voucherTransactionStatusBean = generateVoucherTransactionStatusBean(statusSequenceID, status, null, null);
        //em.persist(voucherTransactionStatusBean);

        //VoucherTransactionEventBean voucherTransactionEventBean = generateVoucherTransactionEventBean("AUT", eventAmount, eventSequenceID, errorCode, errorDescription, eventResult);        
        //em.persist(voucherTransactionEventBean);
        
        VoucherTransactionStatusBean voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(statusSequenceID, status, null, null);
        em.persist(voucherTransactionStatusBean);
        
        VoucherTransactionEventBean voucherTransactionEventBean = voucherTransactionBean.addVoucherTransactionEventBean(EventType.AUT, eventAmount, eventSequenceID, errorCode, errorDescription, eventResult);
        em.persist(voucherTransactionEventBean);

        voucherTransactionBean.setBankTansactionID(gestPayData.getBankTransactionID());
        voucherTransactionBean.setAuthorizationCode(gestPayData.getAuthorizationCode());
        em.merge(voucherTransactionBean);
        
        return gestPayData;
    }

    private GestPayData doPaymentAuthDelete(Integer statusSequenceID, Integer eventSequenceID) {
        
        Double amount = voucherTransactionBean.getAmount();
        String currency = voucherTransactionBean.getCurrency();
        String transactionID = voucherTransactionBean.getVoucherTransactionID();
        String bankTransactionID = voucherTransactionBean.getBankTansactionID();
        String shopLogin = voucherTransactionBean.getShopLogin();
        String acquirerId = voucherTransactionBean.getAcquirerID();
        String groupAcquirer = voucherTransactionBean.getGroupAcquirer();
        String encodedSecretKey = voucherTransactionBean.getEncodedSecretKey();
        String status = StatusHelper.VOUCHER_STATUS_CAN_OK;

        GestPayData gestPayData = gpService.deletePagam(amount, transactionID, shopLogin, currency, bankTransactionID, null, acquirerId, groupAcquirer, encodedSecretKey);

        if (gestPayData == null) {
            gestPayData = new GestPayData();
            gestPayData.setTransactionResult("ERROR");
            gestPayData.setErrorCode("9999");
            gestPayData.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            gestPayData.setAmount(amount.toString());
            status = StatusHelper.VOUCHER_STATUS_CAN_ERROR;
        }

        System.out.println("Operazione GestPay AUTH DEL: " + gestPayData.getTransactionResult());
        
        
        String errorCode = gestPayData.getErrorCode();
        String errorDescription = gestPayData.getErrorDescription();
        String eventResult = gestPayData.getTransactionResult();
        Double eventAmount = Double.valueOf(gestPayData.getAmount());

        //VoucherTransactionStatusBean voucherTransactionStatusBean = generateVoucherTransactionStatusBean(statusSequenceID, status, null, null);
        //em.persist(voucherTransactionStatusBean);

        //VoucherTransactionEventBean voucherTransactionEventBean = generateVoucherTransactionEventBean("CAN", eventAmount, eventSequenceID, errorCode, errorDescription, eventResult);        
        //em.persist(voucherTransactionEventBean);

        VoucherTransactionStatusBean voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(statusSequenceID, status, null, null);
        em.persist(voucherTransactionStatusBean);
        
        VoucherTransactionEventBean voucherTransactionEventBean = voucherTransactionBean.addVoucherTransactionEventBean(EventType.CAN, eventAmount, eventSequenceID, errorCode, errorDescription, eventResult);
        em.persist(voucherTransactionEventBean);
        
        em.merge(voucherTransactionBean);
        
        return gestPayData;
    }

    private GestPayData doPaymentSettle(Integer statusSequenceID, Integer eventSequenceID, Extension[] extension_array) {

        Double amount = voucherTransactionBean.getAmount();
        String currency = voucherTransactionBean.getCurrency();
        String transactionID = voucherTransactionBean.getVoucherTransactionID();
        String shopLogin = voucherTransactionBean.getShopLogin();
        String acquirerId = voucherTransactionBean.getAcquirerID();
        String groupAcquirer = voucherTransactionBean.getGroupAcquirer();
        String encodedSecretKey = voucherTransactionBean.getEncodedSecretKey();
        String status = StatusHelper.VOUCHER_STATUS_MOV_OK;

        GestPayData gestPayData = gpService.callSettle(amount, transactionID, shopLogin, currency, acquirerId, groupAcquirer, encodedSecretKey,
                voucherTransactionBean.getToken(), voucherTransactionBean.getAuthorizationCode(), voucherTransactionBean.getBankTansactionID(), voucherTransactionBean.getRefuelMode(),
                voucherTransactionBean.getProductID(), 0.0, 0.0);

        if (gestPayData == null) {
            gestPayData = new GestPayData();
            gestPayData.setTransactionResult("ERROR");
            gestPayData.setErrorCode("9999");
            gestPayData.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            gestPayData.setAmount(amount.toString());
            status = StatusHelper.VOUCHER_STATUS_MOV_ERROR;
        }
        else {
            if (gestPayData.getTransactionResult().equalsIgnoreCase("KO")) {
                status = StatusHelper.VOUCHER_STATUS_MOV_KO;
            }
        }

        System.out.println("Operazione GestPay SETTLE: " + gestPayData.getTransactionResult());


        String errorCode = gestPayData.getErrorCode();
        String errorDescription = gestPayData.getErrorDescription();
        String eventResult = gestPayData.getTransactionResult();
        
        System.out.println("amount: " + gestPayData.getAmount());
        
        Double eventAmount = Double.valueOf(gestPayData.getAmount());

        //VoucherTransactionStatusBean voucherTransactionStatusBean = generateVoucherTransactionStatusBean(statusSequenceID, status, null, null);
        //em.persist(voucherTransactionStatusBean);

        //VoucherTransactionEventBean voucherTransactionEventBean = generateVoucherTransactionEventBean("MOV", eventAmount, eventSequenceID, errorCode, errorDescription, eventResult);        
        //em.persist(voucherTransactionEventBean);
        
        VoucherTransactionStatusBean voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(statusSequenceID, status, null, null);
        em.persist(voucherTransactionStatusBean);
        
        VoucherTransactionEventBean voucherTransactionEventBean = voucherTransactionBean.addVoucherTransactionEventBean(EventType.MOV, eventAmount, eventSequenceID, errorCode, errorDescription, eventResult);
        em.persist(voucherTransactionEventBean);
        
        em.merge(voucherTransactionBean);
        

        return gestPayData;
    }

    private CreateVoucherResult doCreate(Integer statusSequenceID, Integer eventSequenceID) {
        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        PartnerType partnerType = PartnerType.MP;
        VoucherConsumerType voucherType = VoucherConsumerType.ENI;
        long requestTimestamp = new Date().getTime();
        
        CreateVoucherResult createVoucherResult = new CreateVoucherResult();
        
        BigDecimal amount = BigDecimal.valueOf(voucherTransactionBean.getAmount());
        String bankTransactionID = voucherTransactionBean.getBankTansactionID();
        String shopTransactionID = voucherTransactionBean.getVoucherTransactionID();
        String authorizationCode = voucherTransactionBean.getAuthorizationCode();
        
        VoucherTransactionOperationBean voucherTransactionOperationBean = new VoucherTransactionOperationBean();
        voucherTransactionOperationBean.setOperationID(operationID);
        voucherTransactionOperationBean.setRequestTimestamp(new Date());
        voucherTransactionOperationBean.setOperationType("CREATE");
        voucherTransactionOperationBean.setAmount(voucherTransactionBean.getAmount());
        voucherTransactionOperationBean.setVoucherTransactionBean(voucherTransactionBean);
        
        String errorCode = null;
        String errorDescription = null;
        String eventResult = null;
        Double eventAmount = voucherTransactionBean.getAmount();
        String status = StatusHelper.VOUCHER_STATUS_CREATE_OK;

        try {

            createVoucherResult = fidelityService.createVoucher(voucherType, amount, bankTransactionID, shopTransactionID, authorizationCode, operationID, 
                    partnerType, requestTimestamp);

            voucherTransactionOperationBean.setCsTransactionID(createVoucherResult.getCsTransactionID());
            voucherTransactionOperationBean.setMessageCode(createVoucherResult.getMessageCode());
            voucherTransactionOperationBean.setStatusCode(createVoucherResult.getStatusCode());

            errorCode = createVoucherResult.getStatusCode();
            errorDescription = createVoucherResult.getMessageCode();

            if (createVoucherResult.getStatusCode().equals(FidelityResponse.CREATE_VOUCHER_OK)) {
                eventResult = "OK";
            }
            else {
                eventResult = "KO";
                status = StatusHelper.VOUCHER_STATUS_CREATE_KO;
            }
        }
        catch (Exception e) {

            System.err.println("Error creating voucher: " + e.getMessage());

            errorCode = FidelityResponse.CREATE_VOUCHER_SYSTEM_ERROR;
            errorDescription = e.getMessage();
            eventResult = "ERROR";
            status = StatusHelper.VOUCHER_STATUS_CREATE_ERROR;
            
            voucherTransactionOperationBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
            voucherTransactionOperationBean.setMessageCode("STATO DA VERIFICARE (" + e.getMessage() + ")");
            createVoucherResult.setStatusCode(errorCode);
            createVoucherResult.setMessageCode(errorDescription);
        }
        
        voucherTransactionBean.getTransactionOperationBeanList().add(voucherTransactionOperationBean);
        em.persist(voucherTransactionOperationBean);
        
        //VoucherTransactionStatusBean voucherTransactionStatusBean = generateVoucherTransactionStatusBean(statusSequenceID, status, null, null);
        //em.persist(voucherTransactionStatusBean);

        //VoucherTransactionEventBean voucherTransactionEventBean = generateVoucherTransactionEventBean("CRE", eventAmount, eventSequenceID, errorCode, errorDescription, eventResult);        
        //em.persist(voucherTransactionEventBean);
        
        VoucherTransactionStatusBean voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(statusSequenceID, status, null, null);
        em.persist(voucherTransactionStatusBean);
        
        VoucherTransactionEventBean voucherTransactionEventBean = voucherTransactionBean.addVoucherTransactionEventBean(EventType.CRE, eventAmount, eventSequenceID, errorCode, errorDescription, eventResult);
        em.persist(voucherTransactionEventBean);
        
        em.merge(voucherTransactionBean);


        return createVoucherResult;
    }
        
    private DeleteVoucherResult doDelete(Integer statusSequenceID, Integer eventSequenceID) {
        
        VoucherTransactionOperationBean createVoucherTransactionOperationBean = null;
        DeleteVoucherResult deleteVoucherResult = new DeleteVoucherResult();
        
        for (VoucherTransactionOperationBean tmpVoucherTransactionOperationBean : voucherTransactionBean.getTransactionOperationBeanList()) {
            if (tmpVoucherTransactionOperationBean.getOperationType().equals("CREATE") && 
                    tmpVoucherTransactionOperationBean.getStatusCode().equals(FidelityResponse.CREATE_VOUCHER_OK)) {
                
                createVoucherTransactionOperationBean = tmpVoucherTransactionOperationBean;
            }
        }
        
        if (createVoucherTransactionOperationBean == null) {
            deleteVoucherResult.setStatusCode("1111");
            deleteVoucherResult.setMessageCode("Create operation not found");
            return deleteVoucherResult;
        }
        
        
        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        PartnerType partnerType = PartnerType.MP;
        long requestTimestamp = new Date().getTime();
        
        
        VoucherTransactionOperationBean voucherTransactionOperationBean = new VoucherTransactionOperationBean();
        voucherTransactionOperationBean.setOperationID(operationID);
        voucherTransactionOperationBean.setOperationIDReversed(createVoucherTransactionOperationBean.getOperationID());
        voucherTransactionOperationBean.setRequestTimestamp(new Date());
        voucherTransactionOperationBean.setOperationType("DELETE");
        voucherTransactionOperationBean.setVoucherTransactionBean(voucherTransactionBean);
        
        String errorCode = deleteVoucherResult.getStatusCode();
        String errorDescription = deleteVoucherResult.getMessageCode();
        String eventResult = null;
        Double eventAmount = createVoucherTransactionOperationBean.getAmount();
        String status = StatusHelper.VOUCHER_STATUS_DELETE_OK;

        try {

            deleteVoucherResult = fidelityService.deleteVoucher(createVoucherTransactionOperationBean.getOperationID(), operationID, partnerType, requestTimestamp);

            voucherTransactionOperationBean.setCsTransactionID(deleteVoucherResult.getCsTransactionID());
            voucherTransactionOperationBean.setMessageCode(deleteVoucherResult.getMessageCode());
            voucherTransactionOperationBean.setStatusCode(deleteVoucherResult.getStatusCode());

            errorCode = deleteVoucherResult.getStatusCode();
            errorDescription = deleteVoucherResult.getMessageCode();

            if (deleteVoucherResult.getStatusCode().equals(FidelityResponse.DELETE_VOUCHER_OK)) {
                eventResult = "OK";
            }
            else {
                eventResult = "ERROR";
                status = StatusHelper.VOUCHER_STATUS_DELETE_ERROR;
            }
        }
        catch (Exception e) {

            System.err.println("Error deleting voucher: " + e.getMessage());

            errorCode = FidelityResponse.DELETE_VOUCHER_SYSTEM_ERROR;
            errorDescription = e.getMessage();
            eventResult = "ERROR";
            status = StatusHelper.VOUCHER_STATUS_DELETE_ERROR;
            
            voucherTransactionOperationBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
            voucherTransactionOperationBean.setMessageCode("STATO DA VERIFICARE (" + e.getMessage() + ")");
            deleteVoucherResult.setStatusCode(errorCode);
            deleteVoucherResult.setMessageCode(errorDescription);
            
        }

        voucherTransactionBean.getTransactionOperationBeanList().add(voucherTransactionOperationBean);
        em.persist(voucherTransactionOperationBean);

        //VoucherTransactionStatusBean voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(statusSequenceID, status, null, null);
        //em.persist(voucherTransactionStatusBean);

        //VoucherTransactionEventBean voucherTransactionEventBean = voucherTransactionBean.addVoucherTransactionEventBean("DEL", eventAmount, eventSequenceID, errorCode, errorDescription, eventResult);        
        //em.persist(voucherTransactionEventBean);
        
        VoucherTransactionStatusBean voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(statusSequenceID, status, null, null);
        em.persist(voucherTransactionStatusBean);
        
        VoucherTransactionEventBean voucherTransactionEventBean = voucherTransactionBean.addVoucherTransactionEventBean(EventType.DEL, eventAmount, eventSequenceID, 
                errorCode, errorDescription, eventResult);
        em.persist(voucherTransactionEventBean);
        
        em.merge(voucherTransactionBean);
        
        return deleteVoucherResult;
    }



}

