package com.techedge.mp.core.business.voucher;


public enum EventType {
    AUT,
    MOV,
    CAN,
    CRE,
    DEL,
    STO;

    public String value() {
        return name();
    }

    public static EventType fromValue(String v) {
        return valueOf(v);
    }
    
}
