package com.techedge.mp.core.business;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.actions.admin.*;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminArchiveInvertResponse;
import com.techedge.mp.core.business.interfaces.AdminArchiveResponse;
import com.techedge.mp.core.business.interfaces.AdminAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.AdminDeleteTicketResponse;
import com.techedge.mp.core.business.interfaces.AdminErrorResponse;
import com.techedge.mp.core.business.interfaces.AdminPoPArchiveResponse;
import com.techedge.mp.core.business.interfaces.AdminPropagationUserDataResult;
import com.techedge.mp.core.business.interfaces.AdminRetrieveDataResponse;
import com.techedge.mp.core.business.interfaces.AdminRolesResponse;
import com.techedge.mp.core.business.interfaces.AdminUpdateParamsResponse;
import com.techedge.mp.core.business.interfaces.AdminUserNotVerifiedDeleteResult;
import com.techedge.mp.core.business.interfaces.CityInfo;
import com.techedge.mp.core.business.interfaces.CountPendingTransactionsResult;
import com.techedge.mp.core.business.interfaces.DeleteTestersData;
import com.techedge.mp.core.business.interfaces.DocumentAttribute;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GeneratePvResponse;
import com.techedge.mp.core.business.interfaces.GenerateTestersData;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.MappingErrorData;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ParamInfo;
import com.techedge.mp.core.business.interfaces.PaymentDataResponse;
import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;
import com.techedge.mp.core.business.interfaces.PaymentMethodResponse;
import com.techedge.mp.core.business.interfaces.PollingIntervalData;
import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.core.business.interfaces.ProvinceData;
import com.techedge.mp.core.business.interfaces.RefuelingMappingError;
import com.techedge.mp.core.business.interfaces.RemoteSystem;
import com.techedge.mp.core.business.interfaces.AdminRemoteSystemCheckResult;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveActivityLogData;
import com.techedge.mp.core.business.interfaces.RetrieveDocumentResponse;
import com.techedge.mp.core.business.interfaces.RetrieveLandingResponse;
import com.techedge.mp.core.business.interfaces.RetrieveManagerListData;
import com.techedge.mp.core.business.interfaces.RetrieveParamsData;
import com.techedge.mp.core.business.interfaces.RetrievePoPTransactionListData;
import com.techedge.mp.core.business.interfaces.RetrievePromotionsData;
import com.techedge.mp.core.business.interfaces.RetrieveStatisticsData;
import com.techedge.mp.core.business.interfaces.RetrieveTransactionListData;
import com.techedge.mp.core.business.interfaces.RetrieveVoucherTransactionListData;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.StationActivationData;
import com.techedge.mp.core.business.interfaces.StationData;
import com.techedge.mp.core.business.interfaces.StationsAdminData;
import com.techedge.mp.core.business.interfaces.SurveyList;
import com.techedge.mp.core.business.interfaces.SurveyQuestion;
import com.techedge.mp.core.business.interfaces.TypeData;
import com.techedge.mp.core.business.interfaces.UnavailabilityPeriodResponse;
import com.techedge.mp.core.business.interfaces.UpdatePvActivationFlagResult;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationDetail;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfoData;
import com.techedge.mp.core.business.interfaces.user.RetrieveUserListData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryData;
import com.techedge.mp.core.business.interfaces.user.UserTypeData;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

/**
 * Session Bean implementation class AdminService
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminService implements AdminServiceRemote, AdminServiceLocal {

    /*
     * private static final Integer ADMIN_TICKET_EXPIRY_TIME = 30; // minutes
     * private static final Integer VERIFICATION_CODE_EXPIRY_TIME = 172800; //
     * minutes private static final String ACTIVATION_LINK =
     * "http://trustmyphone-01.mashfrog.com/mobilePaymentValidateMail/redirect.php?type=email_verification&"
     * ; private static final Integer ACTIVITY_LOG_MAX_RESULTS = 1000; private
     * static final Double MAX_CAP_VALUE = 100.00; private static final Double
     * TESTER_INITIAL_CAP = 0.10;
     */

    private static final String                             PARAM_ADMIN_TICKET_EXPIRY_TIME      = "ADMIN_TICKET_EXPIRY_TIME";
    private static final String                             PARAM_VERIFICATION_CODE_EXPIRY_TIME = "VERIFICATION_CODE_EXPIRY_TIME";
    private static final String                             PARAM_ACTIVATION_LINK               = "ACTIVATION_LINK";
    private static final String                             PARAM_ACTIVITY_LOG_MAX_RESULTS      = "ACTIVITY_LOG_MAX_RESULTS";
    private static final String                             PARAM_MAX_CAP_VALUE                 = "MAX_CAP_VALUE";
    private static final String                             PARAM_TESTER_INITIAL_CAP            = "TESTER_INITIAL_CAP";
    private static final String                             PARAM_EMAIL_SENDER_ADDRESS          = "EMAIL_SENDER_ADDRESS";
    private static final String                             PARAM_SMTP_HOST                     = "SMTP_HOST";
    private static final String                             PARAM_SMTP_PORT                     = "SMTP_PORT";
    private final static String                             PARAM_PROXY_HOST                    = "PROXY_HOST";
    private final static String                             PARAM_PROXY_PORT                    = "PROXY_PORT";
    private final static String                             PARAM_TRANSACTION_ARCHIVE_TIME      = "TRANSACTION_ARCHIVE_TIME";
    private final static String                             PARAM_PROXY_NO_HOSTS                = "PROXY_NO_HOSTS";
    private final static String                             PARAM_RECONCILIATION_MAX_ATTEMPTS   = "RECONCILIATION_MAX_ATTEMPTS";
    private final static String                             PARAM_CARTASI_VAULT_BLOCK           = "CARTASI_VAULT_BLOCK";
    private final static String                             PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY = "CARTASI_VAULT_BLOCK_ATTRIBUTE_KEY";

    private static final String                             PARAM_APIKEY_CARTASI                = "APIKEY_CARTASI";
    private static final String                             PARAM_UICCODE_DEPOSIT_CARTASI       = "UICCODE_DEPOSIT_CARTASI";
    private static final String                             PARAM_GROUP_ACQUIRER_CARTASI        = "GROUP_ACQUIRER_CARTASI";
    private static final String                             PARAM_ENCODED_SECRET_KEY_CARTASI    = "ENCODED_SECRET_KEY_CARTASI";
    private static final String                             PARAM_PIN_CHECK_MAX_ATTEMPTS        = "PIN_CHECK_MAX_ATTEMPTS";
    private static final String                             PARAM_CHECK_AMOUNT_VALUE            = "CHECK_AMOUNT_VALUE";
    private static final String                             PARAM_MAX_CHECK_AMOUNT              = "MAX_CHECK_AMOUNT";
    private final static String                             PARAM_STRING_SUBSTITUTION_PATTERN   = "STRING_SUBSTITUTION_PATTERN";
    
    private final static String                             PARAM_DEPOSIT_CARD_MULTICARD_BO_KEY           = "DEPOSIT_CARD_MULTICARD_BO_KEY";
    private final static String                             PARAM_DEPOSIT_CARD_MULTICARD_BO_NOTIFY_OK_URL = "DEPOSIT_CARD_MULTICARD_BO_NOTIFY_OK_URL";
    private final static String                             PARAM_DEPOSIT_CARD_MULTICARD_BO_NOTIFY_KO_URL = "DEPOSIT_CARD_MULTICARD_BO_NOTIFY_KO_URL";
    private final static String                             PARAM_DEPOSIT_CARD_MULTICARD_BO_URL           = "DEPOSIT_CARD_MULTICARD_BO_URL";
    private final static String                             PARAM_LOYALTY_SESSION_EXPIRY_TIME             = "LOYALTY_SESSION_EXPIRY_TIME";             //minutes

    @EJB
    private AdminAuthenticationAction                       adminAuthenticationAction;

    @EJB
    private AdminLogoutAction                               adminLogoutAction;

    @EJB
    private AdminCreateAction                               adminCreateAction;

    @EJB
    private AdminActivityLogRetrieveAction                  adminActivityLogRetrieveAction;

    @EJB
    private AdminUserListRetrieveAction                     adminUserListRetrieveAction;

    @EJB
    private AdminTransactionRetrieveAction                  adminTransactionRetrieveAction;

    @EJB
    private AdminPoPTransactionRetrieveAllAction            adminPoPTransactionRetrieveAllAction;

    @EJB
    private AdminPaymentMethodRetrieveAction                adminPaymentMethodRetrieveAction;

    @EJB
    private AdminUserUpdateAction                           adminUserUpdateAction;

    @EJB
    private AdminTransactionUpdateAction                    adminTransactionUpdateAction;

    @EJB
    private AdminPaymentMethodUpdateAction                  adminPaymentMethodUpdateAction;

    @EJB
    private AdminCustomerUserCreateAction                   adminCustomerUserCreateAction;

    @EJB
    private AdminUserDeleteAction                           adminUserDeleteAction;

    @EJB
    private AdminGenerateTestersAction                      adminGenerateTestersAction;

    @EJB
    private AdminTestersDeleteAction                        adminTestersDeleteAction;

    @EJB
    private AdminStatisticsRetrieveAction                   adminStatisticsRetrieveAction;

    @EJB
    private AdminStationCreateAction                        adminAddStationAction;

    @EJB
    private AdminStationUpdateAction                        adminUpdateStationAction;

    @EJB
    private AdminStationDeleteAction                        adminDeleteStationAction;

    @EJB
    private AdminStationsCreateAction                       adminAddStationsAction;

    @EJB
    private AdminStationsDeleteAction                       adminDeleteStationsAction;

    @EJB
    private AdminStationRetrieveAction                      adminSearchStationsAction;

    @EJB
    private AdminParamsRetrieveAction                       adminParamsRetrieveAction;

    @EJB
    private AdminParamUpdateAction                          adminParamUpdateAction;

    @EJB
    private AdminTransactionArchiveAction                   adminTransactionArchiveAction;

    @EJB
    private AdminTransactionArchiveInvertAction             adminTransactionArchiveInvertAction;

    @EJB
    private AdminPoPTransactionArchiveAction                adminPoPTransactionArchiveAction;

    @EJB
    private AdminReconcileAction                            adminReconcileAction;

    @EJB
    private AdminReconcileDetailAction                      adminReconcileDetailAction;

    @EJB
    private AdminUserEmailConfirmResendAction               adminUserEmailConfirmResendAction;

    @EJB
    private AdminManagerCreateAction                        adminManagerCreateAction;

    @EJB
    private AdminManagerDeleteAction                        adminManagerDeleteAction;

    @EJB
    private AdminManagerUpdateAction                        adminManagerUpdateAction;

    @EJB
    private AdminManagerRetrieveAction                      adminManagerRetrieveAction;

    @EJB
    private AdminManagerAddStationAction                    adminManagerAddStationAction;

    @EJB
    private AdminAuthorizationCheckAction                   adminAuthorizationCheckAction;

    @EJB
    private AdminPoPTransactionRetrieveReconciliationAction adminPoPTransactionRetrieveReconciliationAction;

    @EJB
    private AdminAddVoucherToPromotionAction                adminAddVoucherToPromotionAction;

    @EJB
    private AdminSurveyCreateAction                         adminSurveyCreateAction;

    @EJB
    private AdminSurveyUpdateAction                         adminSurveyUpdateAction;

    @EJB
    private AdminSurveyQuestionAddAction                    adminSurveyQuestionAddAction;

    @EJB
    private AdminSurveyQuestionUpdateAction                 adminSurveyQuestionUpdateAction;

    @EJB
    private AdminSurveyQuestionDeleteAction                 adminSurveyQuestionDeleteAction;

    @EJB
    private AdminSurveyRetrieveAction                       adminSurveyRetrieveAction;

    @EJB
    private AdminSurveyResetAction                          adminSurveyResetAction;

    @EJB
    private AdminCardBinCreateAction                        adminCardBinCreateAction;

    @EJB
    private AdminPromotionsRetrieveAction                   adminPromotionsRetrieveAction;

    @EJB
    private AdminPromotionUpdateAction                      adminPromotionUpdateAction;

    @EJB
    private AdminErrorRetriveAction                         adminErrorRetriveAction;

    @EJB
    private AdminErrorCreateAction                          adminErrorCreateAction;

    @EJB
    private AdminErrorUpdateAction                          adminErrorUpdateAction;

    @EJB
    private AdminErrorDeleteAction                          adminErrorDeleteAction;

    @EJB
    private AdminUserCategoryAddTypeAction                  adminUserCategoryAddTypeAction;

    @EJB
    private AdminUserCategoryCreateAction                   adminUserCategoryCreateAction;

    @EJB
    private AdminUserTypeCreateAction                       adminUserTypeCreateAction;

    @EJB
    private AdminUserCategoryDeleteTypeAction               adminUserCategoryDeleteTypeAction;

    @EJB
    private AdminUserCategoryRetrieveAction                 adminUserCategoryRetrieveAction;

    @EJB
    private AdminUserTypeRetrieveAction                     adminUserTypeRetrieveAction;

    @EJB
    private AdminPrefixNumberCreateAction                   adminPrefixNumberCreateAction;

    @EJB
    private AdminPrefixNumberDeleteAction                   adminPrefixNumberDeleteAction;

    @EJB
    private AdminPrefixNumberAllRetrieveAction              adminRetriveAllPrefixNumberAction;

    @EJB
    private AdminVoucherTransactionsRetrieveAction          adminVoucherTransactionsRetrieveAction;

    @EJB
    private AdminTermsOfServiceForceUpdateAction            adminTermsOfServiceForceUpdateAction;

    @EJB
    private AdminBlockPeriodCreateAction                    adminBlockPeriodCreateAction;

    @EJB
    private AdminBlockPeriodUpdateAction                    adminBlockPeriodUpdateAction;

    @EJB
    private AdminBlockPeriodDeleteAction                    adminBlockPeriodDeleteAction;

    @EJB
    private AdminBlockPeriodRetrieveAction                  adminBlockPeriodRetrieveAction;

    @EJB
    private AdminDocumentCreateAction                       adminDocumentCreateAction;

    @EJB
    private AdminDocumentUpdateAction                       adminDocumentUpdateAction;

    @EJB
    private AdminDocumentDeleteAction                       adminDocumentDeleteAction;

    @EJB
    private AdminDocumentRetrieveAction                     adminDocumentRetrieveAction;

    @EJB
    private AdminDocumentAttributeCreateAction              adminDocumentAttributeCreateAction;

    @EJB
    private AdminDocumentAttributeDeleteAction              adminDocumentAttributeDeleteAction;

    @EJB
    private AdminGeneratePvAction                           adminGeneratePvAction;

    @EJB
    private AdminPasswordUpdateAction                       adminPasswordUpdateAction;

    @EJB
    private AdminForceSmsNotificationStatusAction           adminForceSmsNotificationStatusAction;

    @EJB
    private AdminGenerateMailingListAction                  adminGenerateMailingListAction;

    @EJB
    private AdminMailingListUpdateAction                    adminMailingListUpdateAction;

    @EJB
    private AdminVoucherAssignAction                        adminVoucherAssignAction;

    @EJB
    private AdminTransactionEventDeleteAction               adminTransactionEventDeleteAction;

    @EJB
    private AdminUserInsertInPromoOnHoldAction              adminUserInsertInPromoOnHoldAction;

    @EJB
    private UserCategoryService                             userCategory;

    @EJB
    private AdminEmailDomainCreateAction                    adminEmailDomainCreateAction;

    @EJB
    private AdminEmailDomainUpdateInBlacklistAction         adminEditEmailDomainAction;

    @EJB
    private AdminEmailDomainRetrieveInBlacklistAction       adminGetEmailDomainAction;

    @EJB
    private AdminEmailDomainDeleteInBlacklistAction         adminRemoveEmailDomainAction;

    @EJB
    private AdminPoPTransactionUpdateAction                 adminPoPTransactionUpdateAction;

    @EJB
    private AdminVoucherTransactionUpdateAction             adminVoucherTransactionUpdateAction;

    @EJB
    private AdminVoucherDataUpdateAction                    adminVoucherDataUpdateAction;

    @EJB
    private AdminVoucherDeleteAction                        adminVoucherDeleteAction;

    @EJB
    private AdminProvinceCreateAction                       adminProvinceCreateAction;

    @EJB
    private AdminMobilePhoneDeleteAction                    adminMobilePhoneDeleteAction;

    @EJB
    private AdminUserDataPropagationAction                  adminUserDataPropagationAction;

    @EJB
    private AdminGeneralCreateAction                        adminGeneralCreateAction;

    @EJB
    private AdminGeneralUpdateAction                        adminGeneralUpdateAction;

    @EJB
    private AdminPrepaidConsumeVoucherUpdateAction          adminPrepaidConsumeVoucherUpdateAction;

    @EJB
    private AdminPrepaidConsumeVoucherDetailCreateAction    adminPrepaidConsumeVoucherDetailCreateAction;

    @EJB
    private AdminGeneralDeleteAction                        adminGeneralDeleteAction;

    @EJB
    private AdminRoleCreateAction                           adminRoleCreateAction;

    @EJB
    private AdminRoleDeleteAction                           adminRoleDeleteAction;

    @EJB
    private AdminRoleAddToAdminAction                       adminRoleAddToAdminAction;

    @EJB
    private AdminRoleRetrieveAction                         adminRoleRetrieveAction;

    @EJB
    private AdminRoleRemoveToAdminAction                    adminRoleRemoveToAdminAction;

    @EJB
    private AdminPushNotificationSendAction                 adminPushNotificationSendAction;

    @EJB
    private AdminUpdateAction                               adminUpdateAction;

    @EJB
    private AdminRetrieveAction                             adminRetrieveAction;

    @EJB
    private AdminDeleteAction                               adminDeleteAction;

    @EJB
    private AdminRefuelingUserCreateAction                  adminCreateRefuelingUserAction;

    @EJB
    private AdminRefuelingUserUpdatePasswordAction          adminUpdatePasswordRefuelingUserAction;

    @EJB
    private AdminRefuelingUserInsertPaymentMethodAction     adminInsertPaymentMethodRefuelingUserAction;

    @EJB
    private AdminRefuelingUserRemovePaymentMethodAction     adminRemovePaymentMethodRefuelingUserAction;

    @EJB
    private AdminRefuelingUserRetrevePaymentDataAction      adminRetrievePaymentDataRefuelingUserAction;

    @EJB
    private AdminRefuelingUserInsertMulticardAction         adminInsertMulticardRefuelingUserAction;
    
    @EJB
    private AdminGeneralMassiveDeleteAction                 adminGeneralMassiveDeleteAction;

    @EJB
    private AdminRetrieveLandingAction                      adminRetrieveLandingAction;

    @EJB
    private AdminPollingIntervalRetrieveAction              adminPollingIntervalRetrieveAction;

    @EJB
    private AdminUpdateCitiesAction                         adminUpdateCitiesAction;
    
    @EJB
    private AdminUserNotVerifiedDeleteAction                adminUserNotVerifiedDeleteAction;
    
    @EJB
    private AdminUpdatePvActivationFlagAction               adminUpdatePvActivationFlagAction;
    
    @EJB
    private AdminTicketDeleteAction                         adminTicketDeleteAction;
    
    @EJB
    private AdminRemoteSystemCheckAction                    adminRemoteSystemCheckAction;

    @EJB
    private AdminCountPendingTransactionsAction             adminCountPendingTransactionsAction;
    
    @EJB
    private AdminPaymentMethodStatusUpdateAction            adminPaymentMethodStatusUpdateAction;
    
    private LoggerService                                   loggerService                       = null;
    private ParametersService                               parametersService                   = null;
    private EmailSenderRemote                               emailSender                         = null;
    private ForecourtInfoServiceRemote                      forecourtInfoService                = null;
    private FidelityServiceRemote                           fidelityService                     = null;
    private CardInfoServiceRemote                           cardInfoService                     = null;
    
    private UserCategoryService                             userCategoryService                 = null;
    private String                                          secretKey;
    private StringSubstitution                              stringSubstitution                  = null;

    public AdminService() {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get EmailSender object");

            throw new EJBException(e);
        }

        try {
            parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            forecourtInfoService = EJBHomeCache.getInstance().getForecourtInfoService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            fidelityService = EJBHomeCache.getInstance().getFidelityService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        
        try {
            cardInfoService = EJBHomeCache.getInstance().getCardInfoService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        String vaultBlock = "";
        String vaultBlockAttributeCryptKey = "";
        String pattern = null;

        try {

            vaultBlock = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK);
            vaultBlockAttributeCryptKey = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY);
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeCryptKey + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.secretKey + "'");
        
        stringSubstitution = new StringSubstitution(pattern);
    }

    @Override
    public AdminAuthenticationResponse adminAuthentication(String email, String password, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("email", email));
        inputParameters.add(new Pair<String, String>("password", password));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAuthentication", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AdminAuthenticationResponse adminAuthenticationResponse = null;
        try {
            Integer adminTicketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_ADMIN_TICKET_EXPIRY_TIME));

            adminAuthenticationResponse = adminAuthenticationAction.execute(email, password, requestId, adminTicketExpiryTime);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            adminAuthenticationResponse = new AdminAuthenticationResponse();
            adminAuthenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            adminAuthenticationResponse.setTicketId(null);
            adminAuthenticationResponse.setAdmin(null);
        }
        catch (EJBException ex) {
            adminAuthenticationResponse = new AdminAuthenticationResponse();
            adminAuthenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            adminAuthenticationResponse.setTicketId(null);
            adminAuthenticationResponse.setAdmin(null);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", adminAuthenticationResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAuthentication", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return adminAuthenticationResponse;
    }

    @Override
    public String adminLogout(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminLogout", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = adminLogoutAction.execute(ticketId, requestId);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminLogout", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminCreate(String adminTicketId, String requestId, Admin admin) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("admin", admin.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreate", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            Integer verificationCodeExpiryTime = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_VERIFICATION_CODE_EXPIRY_TIME));
            String activationLink = this.parametersService.getParamValue(AdminService.PARAM_ACTIVATION_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(AdminService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(AdminService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(AdminService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(AdminService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(AdminService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(AdminService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = adminCreateAction.execute(adminTicketId, requestId, admin, verificationCodeExpiryTime, activationLink, this.emailSender);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreate", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public RetrieveActivityLogData adminActivityLogRetrieve(String adminTicketId, String requestId, Timestamp start, Timestamp end, ErrorLevel minLevel, ErrorLevel maxLevel,
            String source, String groupId, String phaseId, String messagePattern) {

        String startString = "";
        String endString = "";

        if (start != null) {
            startString = start.toString();
        }

        if (end != null) {
            endString = end.toString();
        }

        String minLevelString = "";
        if (minLevel != null) {
            minLevelString = minLevel.toString();
        }

        String maxLevelString = "";
        if (maxLevel != null) {
            maxLevelString = maxLevel.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("start", startString));
        inputParameters.add(new Pair<String, String>("end", endString));
        inputParameters.add(new Pair<String, String>("minLevel", minLevelString));
        inputParameters.add(new Pair<String, String>("maxLevel", maxLevelString));
        inputParameters.add(new Pair<String, String>("source", source));
        inputParameters.add(new Pair<String, String>("groupId", groupId));
        inputParameters.add(new Pair<String, String>("phaseId", phaseId));
        inputParameters.add(new Pair<String, String>("messagePattern", messagePattern));

        // Disattivazione log per servizio retrieveActivityLog
        // this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveActivityLog", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveActivityLogData retrieveActivityLogData = null;

        try {
            Integer activityLogMaxResults = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_ACTIVITY_LOG_MAX_RESULTS));

            retrieveActivityLogData = adminActivityLogRetrieveAction.execute(adminTicketId, requestId, start, end, minLevel, maxLevel, source, groupId, phaseId, messagePattern,
                    activityLogMaxResults);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrieveActivityLogData = new RetrieveActivityLogData();
            retrieveActivityLogData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            retrieveActivityLogData = new RetrieveActivityLogData();
            retrieveActivityLogData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveActivityLogData.getStatusCode()));

        // Disattivazione log per servizio retrieveActivityLog
        // this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveActivityLog", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveActivityLogData;
    }

    public RetrieveUserListData adminUserRetrieve(String adminTicketId, String requestId, Long id, String firstName, 					// LIKE
            // |OPT
            String lastName,  					// LIKE |OPT
            String fiscalCode,					// LIKE |OPT
            String securityDataEmail,			// LIKE |OPT
            Integer userStatus,					// EQUALS |OPT
            String externalUserId,				// LIKE |OPT
            Double capAvailableMin, Double capAvailableMax, Double capEffectiveMin, Double capEffectiveMax, Timestamp creationDateMin, Timestamp creationDateMax, Integer userType) {

        String idString = "";
        if (id != null) {
            idString = id.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", idString));
        inputParameters.add(new Pair<String, String>("firstName", firstName));
        inputParameters.add(new Pair<String, String>("lastName", lastName));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        inputParameters.add(new Pair<String, String>("securityDataEmail", securityDataEmail));
        inputParameters.add(new Pair<String, String>("userStatus", this.integerToString(userStatus)));
        inputParameters.add(new Pair<String, String>("capAvailableMin", this.doubleToString(capAvailableMin)));
        inputParameters.add(new Pair<String, String>("capAvailableMax", this.doubleToString(capAvailableMax)));
        inputParameters.add(new Pair<String, String>("capEffectiveMin", this.doubleToString(capEffectiveMin)));
        inputParameters.add(new Pair<String, String>("capEffectiveMax", this.doubleToString(capEffectiveMax)));
        inputParameters.add(new Pair<String, String>("creationDateMin", this.timestampToString(creationDateMin)));
        inputParameters.add(new Pair<String, String>("creationDateMax", this.timestampToString(creationDateMax)));
        inputParameters.add(new Pair<String, String>("userType", this.integerToString(userType)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveUserList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveUserListData retrieveUserListData = null;

        try {
            Integer activityLogMaxResults = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_ACTIVITY_LOG_MAX_RESULTS));
            Double maxCapValue = Double.valueOf(this.parametersService.getParamValue(AdminService.PARAM_MAX_CAP_VALUE));

            retrieveUserListData = adminUserListRetrieveAction.execute(adminTicketId, requestId, id, firstName, 					// LIKE
                    // |OPT
                    lastName,  					// LIKE |OPT
                    fiscalCode,					// LIKE |OPT
                    securityDataEmail,			// LIKE |OPT
                    userStatus,					// EQUALS |OPT
                    externalUserId,				// LIKE |OPT
                    capAvailableMin, capAvailableMax, capEffectiveMin, capEffectiveMax, creationDateMin, creationDateMax, activityLogMaxResults, maxCapValue, userType);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrieveUserListData = new RetrieveUserListData();
            retrieveUserListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            retrieveUserListData = new RetrieveUserListData();
            retrieveUserListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveUserListData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveUserList", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveUserListData;

    }

    public RetrieveTransactionListData adminTransactionRetrieve(String adminTicketId, String requestId, String transactionId, 		//
            String userId,  				//
            String stationId,//
            String pumpId,//
            String finalStatusType, //
            Boolean GFGNotification,//
            Boolean confirmed,//
            Date creationTimestampStart,//
            Date creationTimestampEnd,	//
            String productID, Boolean transactionHistoryFlag) {

        Long userIdLong = null;

        if (userId == null || userId.equals("")) {

            userIdLong = null;
        }
        else {

            try {
                userIdLong = Long.valueOf(userId);
            }
            catch (NumberFormatException ex) {

                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminRetrieveTransaction", requestId, null, "Error parsing userId: " + userId);

                RetrieveTransactionListData retrieveTransactionListData = new RetrieveTransactionListData();
                retrieveTransactionListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);

                Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
                outputParameters.add(new Pair<String, String>("statusCode", retrieveTransactionListData.getStatusCode()));

                this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveTransaction", requestId, "closing",
                        ActivityLog.createLogMessage(outputParameters));

                return retrieveTransactionListData;
            }
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionId", transactionId));
        inputParameters.add(new Pair<String, String>("userId", userId));
        inputParameters.add(new Pair<String, String>("stationId", stationId));
        inputParameters.add(new Pair<String, String>("pumpId", pumpId));
        inputParameters.add(new Pair<String, String>("finalStatusType", finalStatusType));
        inputParameters.add(new Pair<String, String>("GFGNotification", this.toString(GFGNotification)));
        inputParameters.add(new Pair<String, String>("confirmed", this.toString(confirmed)));
        inputParameters.add(new Pair<String, String>("creationTimestampStart", this.toString(creationTimestampStart)));
        inputParameters.add(new Pair<String, String>("creationTimestampEnd", this.toString(creationTimestampEnd)));
        inputParameters.add(new Pair<String, String>("productID", this.toString(productID)));
        inputParameters.add(new Pair<String, String>("transactionHistoryFlag", this.toString(transactionHistoryFlag)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveTransaction", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveTransactionListData retrieveTransactionListData = null;

        try {
            Integer activityLogMaxResults = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_ACTIVITY_LOG_MAX_RESULTS));

            retrieveTransactionListData = adminTransactionRetrieveAction.execute(adminTicketId, requestId, transactionId, userIdLong, stationId, pumpId, finalStatusType,
                    GFGNotification, confirmed, creationTimestampStart, creationTimestampEnd, productID, activityLogMaxResults, transactionHistoryFlag);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrieveTransactionListData = new RetrieveTransactionListData();
            retrieveTransactionListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            retrieveTransactionListData = new RetrieveTransactionListData();
            retrieveTransactionListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveTransactionListData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveTransaction", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveTransactionListData;

    }

    public RetrievePoPTransactionListData adminPoPTransactionRetrieve(String adminTicketId, String requestId, String mpTransactionId,       //
            String userId,                  //
            String stationId,//
            String sourceId,//
            String mpTransactionStatus, //
            Boolean notificationCreated,//
            Boolean notificationPaid,//
            Boolean notificationUser, Date creationTimestampStart,//
            Date creationTimestampEnd,  //
            Boolean transactionHistoryFlag) {

        Long userIdLong = null;

        if (userId == null || userId.equals("")) {

            userIdLong = null;
        }
        else {

            try {
                userIdLong = Long.valueOf(userId);
            }
            catch (NumberFormatException ex) {

                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminRetrievePoPTransaction", requestId, null, "Error parsing userId: " + userId);

                RetrievePoPTransactionListData retrievePoPTransactionListData = new RetrievePoPTransactionListData();
                retrievePoPTransactionListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);

                Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
                outputParameters.add(new Pair<String, String>("statusCode", retrievePoPTransactionListData.getStatusCode()));

                this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePoPTransaction", requestId, "closing",
                        ActivityLog.createLogMessage(outputParameters));

                return retrievePoPTransactionListData;
            }
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("mpTransactionId", mpTransactionId));
        inputParameters.add(new Pair<String, String>("userId", userId));
        inputParameters.add(new Pair<String, String>("stationId", stationId));
        inputParameters.add(new Pair<String, String>("sourceId", sourceId));
        inputParameters.add(new Pair<String, String>("mpTransactionStatus", mpTransactionStatus));
        inputParameters.add(new Pair<String, String>("notificationCreated", this.toString(notificationCreated)));
        inputParameters.add(new Pair<String, String>("notificationPaid", this.toString(notificationPaid)));
        inputParameters.add(new Pair<String, String>("notificationUser", this.toString(notificationUser)));
        inputParameters.add(new Pair<String, String>("creationTimestampStart", this.toString(creationTimestampStart)));
        inputParameters.add(new Pair<String, String>("creationTimestampEnd", this.toString(creationTimestampEnd)));
        inputParameters.add(new Pair<String, String>("transactionHistoryFlag", this.toString(transactionHistoryFlag)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePoPTransaction", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        RetrievePoPTransactionListData retrievePoPTransactionListData = null;

        try {
            Integer activityLogMaxResults = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_ACTIVITY_LOG_MAX_RESULTS));

            retrievePoPTransactionListData = adminPoPTransactionRetrieveAllAction.execute(adminTicketId, requestId, mpTransactionId, userIdLong, stationId, sourceId,
                    mpTransactionStatus, notificationCreated, notificationPaid, notificationUser, creationTimestampStart, creationTimestampEnd, activityLogMaxResults,
                    transactionHistoryFlag);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrievePoPTransactionListData = new RetrievePoPTransactionListData();
            retrievePoPTransactionListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            retrievePoPTransactionListData = new RetrievePoPTransactionListData();
            retrievePoPTransactionListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrievePoPTransactionListData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePoPTransaction", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return retrievePoPTransactionListData;

    }

    @Override
    public PaymentInfoResponse adminPaymentMethodRetrieve(String adminTicketId, String requestId, Long id, String type) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", id.toString()));
        inputParameters.add(new Pair<String, String>("type", type));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminFindPaymentMethod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        PaymentInfoResponse paymentInfoResponse = null;

        try {
            paymentInfoResponse = adminPaymentMethodRetrieveAction.execute(adminTicketId, requestId, id, type);
        }
        catch (EJBException ex) {
            paymentInfoResponse = new PaymentInfoResponse();
            paymentInfoResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", paymentInfoResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminFindPaymentMethod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return paymentInfoResponse;
    }

    @Override
    public String adminUserUpdate(String adminTicketId, String requestId, Long id, String firstName, String lastName, String email, Date birthDate, String birthMunicipality,
            String birthProvince, String sex, String fiscalCode, String language, Integer status, Boolean registrationCompleted, Double capAvailable, Double capEffective,
            String externalUserId, Integer userType, Boolean virtualizationCompleted, Integer virtualizationAttemptsLeft, Boolean eniStationUserType, Boolean depositCardStepCompleted) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", id.toString()));
        inputParameters.add(new Pair<String, String>("firstName", firstName));
        inputParameters.add(new Pair<String, String>("lastName", lastName));
        inputParameters.add(new Pair<String, String>("birthDate", birthDate.toString()));
        inputParameters.add(new Pair<String, String>("birthMunicipality", birthMunicipality));
        inputParameters.add(new Pair<String, String>("birthProvince", birthProvince));
        inputParameters.add(new Pair<String, String>("sex", sex));
        inputParameters.add(new Pair<String, String>("language", language));
        inputParameters.add(new Pair<String, String>("securitydata_email", email));
        inputParameters.add(new Pair<String, String>("status", status.toString()));
        inputParameters.add(new Pair<String, String>("registrationCompleted", registrationCompleted.toString()));
        inputParameters.add(new Pair<String, String>("capAvailable", capAvailable.toString()));
        inputParameters.add(new Pair<String, String>("capEffective", capEffective.toString()));
        inputParameters.add(new Pair<String, String>("externalUserId", externalUserId));
        inputParameters.add(new Pair<String, String>("userType", userType.toString()));
        
        if (virtualizationCompleted != null) {
            inputParameters.add(new Pair<String, String>("virtualizationCompleted", virtualizationCompleted.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("virtualizationCompleted", "null"));
        }
        
        if (virtualizationAttemptsLeft != null) {
            inputParameters.add(new Pair<String, String>("virtualizationAttemptsLeft", virtualizationAttemptsLeft.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("virtualizationAttemptsLeft", "null"));
        }
        
        if (eniStationUserType != null) {
            inputParameters.add(new Pair<String, String>("eniStationUserType", eniStationUserType.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("eniStationUserType", "null"));
        }
        
        if (depositCardStepCompleted != null) {
            inputParameters.add(new Pair<String, String>("depositCardStepCompleted", depositCardStepCompleted.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("depositCardStepCompleted", "null"));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateUser", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminUserUpdateAction.execute(adminTicketId, requestId, id, firstName, lastName, email, birthDate, birthMunicipality, birthProvince, sex, fiscalCode,
                    language, status, registrationCompleted, capAvailable, capEffective, externalUserId, userType, virtualizationCompleted, virtualizationAttemptsLeft,
                    eniStationUserType, depositCardStepCompleted);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateUser", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminPaymentMethodUpdate(String adminTicketId, String requestId, Long id, String type, Integer status, String defaultMethod, Integer attemptsLeft,
            Double checkAmount) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("methodId", id.toString()));
        inputParameters.add(new Pair<String, String>("status", status.toString()));
        inputParameters.add(new Pair<String, String>("defaultMethod", defaultMethod));
        inputParameters.add(new Pair<String, String>("attemptsLeft", attemptsLeft.toString()));
        inputParameters.add(new Pair<String, String>("checkAmount", checkAmount.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateUser", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminPaymentMethodUpdateAction.execute(adminTicketId, requestId, id, type, status, defaultMethod, attemptsLeft, checkAmount);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateUser", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminTransactionUpdate(String adminTicketId, String requestId, String transactionId, String finalStatusType, Boolean GFGNotification, Boolean confirmed,
            Integer reconciliationAttemptsLeft) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionId", transactionId));
        inputParameters.add(new Pair<String, String>("finalStatusType", finalStatusType));
        inputParameters.add(new Pair<String, String>("GFGNotification", GFGNotification.toString()));
        inputParameters.add(new Pair<String, String>("confirmed", confirmed.toString()));
        inputParameters.add(new Pair<String, String>("reconciliationAttemptsLeft", reconciliationAttemptsLeft.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateTransaction", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminTransactionUpdateAction.execute(adminTicketId, requestId, transactionId, finalStatusType, GFGNotification, confirmed, reconciliationAttemptsLeft);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateTransaction", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminCustomerUserCreate(String adminTicketId, String requestId, User user) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("admin", user.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreate", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            Integer verificationCodeExpiryTime = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_VERIFICATION_CODE_EXPIRY_TIME));
            String activationLink = this.parametersService.getParamValue(AdminService.PARAM_ACTIVATION_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(AdminService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(AdminService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(AdminService.PARAM_SMTP_PORT);
            Double testerInitialCap = Double.valueOf(this.parametersService.getParamValue(AdminService.PARAM_TESTER_INITIAL_CAP));
            String proxyHost = this.parametersService.getParamValue(AdminService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(AdminService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(AdminService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = adminCustomerUserCreateAction.execute(adminTicketId, requestId, user, verificationCodeExpiryTime, activationLink, testerInitialCap, this.emailSender);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreate", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminUserDelete(String adminTicketId, String requestId, Long userId, Date creationStart, Date creationEnd) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("admin", this.toString(userId)));
        inputParameters.add(new Pair<String, String>("creationStart", this.toString(creationStart)));
        inputParameters.add(new Pair<String, String>("creationEnd", this.toString(creationEnd)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteUser", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = adminUserDeleteAction.execute(adminTicketId, requestId, userId, creationStart, creationEnd);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteUser", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public GenerateTestersData adminTestersGenerate(String adminTicketId, String requestId, String emailPrefix, String emailSuffix, Integer startNumber, Integer userCount,
            String token, Integer userType) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("emailPrefix", emailPrefix));
        inputParameters.add(new Pair<String, String>("emailSuffix", emailSuffix));
        inputParameters.add(new Pair<String, String>("startNumber", this.toString(startNumber)));
        inputParameters.add(new Pair<String, String>("userCount", this.toString(userCount)));
        inputParameters.add(new Pair<String, String>("token", token));
        inputParameters.add(new Pair<String, String>("userType", userType.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGenerateTesters", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        GenerateTestersData generateTestersData = null;
        try {
            generateTestersData = adminGenerateTestersAction.execute(adminTicketId, requestId, emailPrefix, emailSuffix, startNumber, userCount, token, userType, this);
        }
        catch (EJBException ex) {
            generateTestersData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", generateTestersData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGenerateTesters", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return generateTestersData;
    }

    @Override
    public DeleteTestersData adminTestersDelete(String adminTicketId, String requestId, String emailPrefix, String emailSuffix, Integer startNumber, Integer userCount) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("emailPrefix", emailPrefix));
        inputParameters.add(new Pair<String, String>("emailSuffix", emailSuffix));
        inputParameters.add(new Pair<String, String>("startNumber", this.toString(startNumber)));
        inputParameters.add(new Pair<String, String>("userCount", this.toString(userCount)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteTesters", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        DeleteTestersData deleteTestersData = null;
        try {
            deleteTestersData = adminTestersDeleteAction.execute(adminTicketId, requestId, emailPrefix, emailSuffix, startNumber, userCount, this);
        }
        catch (EJBException ex) {
            deleteTestersData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", deleteTestersData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteTesters", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return deleteTestersData;
    }

    @Override
    public RetrieveStatisticsData adminRetrieveStatistics(String adminTicketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveStatistics", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveStatisticsData retrieveStatisticsData = null;
        try {
            retrieveStatisticsData = adminStatisticsRetrieveAction.execute(adminTicketId, requestId);
        }
        catch (EJBException ex) {
            retrieveStatisticsData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveStatisticsData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveStatistics", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveStatisticsData;
    }

    @Override
    public String adminAddStation(String adminTicketId, String requestId, Station station) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("stationId", station.getStationID()));
        inputParameters.add(new Pair<String, String>("OilAcquirerID", station.getOilAcquirerID()));
        inputParameters.add(new Pair<String, String>("OilShopLogin", station.getOilShopLogin()));
        inputParameters.add(new Pair<String, String>("NoOilAcquirerID", station.getNoOilAcquirerID()));
        inputParameters.add(new Pair<String, String>("NoOilShopLogin", station.getNoOilShopLogin()));
        String status = (station.getStationStatus() != null) ? station.getStationStatus().toString() : "";
        inputParameters.add(new Pair<String, String>("Status", status));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddStation", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminAddStationAction.execute(adminTicketId, requestId, station, secretKey);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddStation", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminUpdateStation(String adminTicketId, String requestId, Station station) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("stationId", station.getStationID()));
        inputParameters.add(new Pair<String, String>("OilAcquirerID", station.getOilAcquirerID()));
        inputParameters.add(new Pair<String, String>("OilShopLogin", station.getOilShopLogin()));
        inputParameters.add(new Pair<String, String>("NoOilAcquirerID", station.getNoOilAcquirerID()));
        inputParameters.add(new Pair<String, String>("NoOilShopLogin", station.getNoOilShopLogin()));
        String status = (station.getStationStatus() != null) ? station.getStationStatus().toString() : "";
        inputParameters.add(new Pair<String, String>("Status", status));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateStation", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = (String) adminUpdateStationAction.commonExeceute(adminTicketId, requestId, secretKey, station);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateStation", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminDeleteStation(String adminTicketId, String requestId, Station station) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("stationId", station.getStationID()));
        inputParameters.add(new Pair<String, String>("OilAcquirerID", station.getOilAcquirerID()));
        inputParameters.add(new Pair<String, String>("OilShopLogin", station.getOilShopLogin()));
        inputParameters.add(new Pair<String, String>("NoOilAcquirerID", station.getNoOilAcquirerID()));
        inputParameters.add(new Pair<String, String>("NoOilShopLogin", station.getNoOilShopLogin()));
        String status = (station.getStationStatus() != null) ? station.getStationStatus().toString() : "";
        inputParameters.add(new Pair<String, String>("Status", status));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteStation", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminDeleteStationAction.execute(adminTicketId, requestId, station);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteStation", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public StationsAdminData adminAddStations(String adminTicketId, String requestId, StationsAdminData stationsAdminData) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddStations", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        // StationsAdminData stationsAdminData = null;

        try {
            stationsAdminData = adminAddStationsAction.execute(adminTicketId, requestId, stationsAdminData, this);
        }
        catch (EJBException ex) {
            stationsAdminData = new StationsAdminData();
            stationsAdminData.setStatusCode(ResponseHelper.SYSTEM_ERROR);

        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", stationsAdminData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddStations", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return stationsAdminData;
    }

    @Override
    public StationsAdminData adminSearchStations(String adminTicketId, String requestId, Station station) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("stationId", station.getStationID()));
        inputParameters.add(new Pair<String, String>("OilAcquirerID", station.getOilAcquirerID()));
        inputParameters.add(new Pair<String, String>("OilShopLogin", station.getOilShopLogin()));
        inputParameters.add(new Pair<String, String>("NoOilAcquirerID", station.getNoOilAcquirerID()));
        inputParameters.add(new Pair<String, String>("NoOilShopLogin", station.getNoOilShopLogin()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSearchStation", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        StationsAdminData stationsAdminData = null;

        try {
            stationsAdminData = adminSearchStationsAction.execute(adminTicketId, requestId, station, secretKey);
        }
        catch (EJBException ex) {
            stationsAdminData = new StationsAdminData();
            stationsAdminData.setStatusCode(ResponseHelper.SYSTEM_ERROR);

        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", stationsAdminData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSearchStation", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return stationsAdminData;
    }

    @Override
    public StationsAdminData adminDeleteStations(String adminTicketId, String requestId, StationsAdminData stationsAdminData) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteStations", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        // StationsAdminData stationsAdminData = null;

        try {
            stationsAdminData = adminDeleteStationsAction.execute(adminTicketId, requestId, stationsAdminData, this);
        }
        catch (EJBException ex) {
            stationsAdminData = new StationsAdminData();
            stationsAdminData.setStatusCode(ResponseHelper.SYSTEM_ERROR);

        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", stationsAdminData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteStations", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return stationsAdminData;
    }

    @Override
    public RetrieveParamsData adminRetrieveParams(String adminTicketId, String requestId, String param) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("param", param));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveParams", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveParamsData retrieveParamsData = null;
        try {
            retrieveParamsData = adminParamsRetrieveAction.execute(adminTicketId, requestId, param);
        }
        catch (EJBException ex) {
            retrieveParamsData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveParamsData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveParams", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveParamsData;
    }

    @Override
    public String adminUpdateParam(String adminTicketId, String requestId, String param, String value, String operation) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("param", param));
        inputParameters.add(new Pair<String, String>("value", value));
        inputParameters.add(new Pair<String, String>("operation", operation));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateParam", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            // response = ResponseHelper.ADMIN_UPDATE_PARAM_SUCCESS;
            response = adminParamUpdateAction.execute(adminTicketId, requestId, param, value, operation);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateParam", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public AdminUpdateParamsResponse adminUpdateParams(String adminTicketId, String requestId, List<ParamInfo> params) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("params", String.valueOf(params.size())));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateParams", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AdminUpdateParamsResponse adminUpdateParamsResponse = null;
        try {
            /*
             * adminUpdateParamsResponse = adminUpdateParamsAction.execute(
             * adminTicketId, requestId, params );
             */
            adminUpdateParamsResponse = new AdminUpdateParamsResponse();
            adminUpdateParamsResponse.setStatusCode(ResponseHelper.ADMIN_UPDATE_PARAMS_SUCCESS);
        }
        catch (EJBException ex) {
            adminUpdateParamsResponse = new AdminUpdateParamsResponse();
            adminUpdateParamsResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", adminUpdateParamsResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateParams", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return adminUpdateParamsResponse;
    }

    @Override
    public AdminArchiveResponse adminArchive(String adminTicketId, String requestId, String transactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateParams", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AdminArchiveResponse adminArchiveResponse = null;
        try {
            String time = this.parametersService.getParamValue(AdminService.PARAM_TRANSACTION_ARCHIVE_TIME);
            adminArchiveResponse = adminTransactionArchiveAction.execute(adminTicketId, requestId, Long.valueOf(time), transactionID);
        }
        catch (EJBException ex) {
            adminArchiveResponse = new AdminArchiveResponse();
            adminArchiveResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (ParameterNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", adminArchiveResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateParams", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return adminArchiveResponse;
    }

    @Override
    public AdminArchiveInvertResponse adminArchiveInvert(String adminTicketId, String requestId, String transactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateParams", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AdminArchiveInvertResponse adminArchiveResponse = null;
        try {
            adminArchiveResponse = adminTransactionArchiveInvertAction.execute(adminTicketId, requestId, transactionID);
        }
        catch (EJBException ex) {
            adminArchiveResponse = new AdminArchiveInvertResponse();
            adminArchiveResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", adminArchiveResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateParams", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return adminArchiveResponse;
    }

    @Override
    public AdminPoPArchiveResponse adminPoPArchive(String adminTicketId, String requestId, Date startDate, Date endDate) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminPoPArchive", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AdminPoPArchiveResponse adminPoPArchiveResponse = null;
        try {
            adminPoPArchiveResponse = adminPoPTransactionArchiveAction.execute(adminTicketId, requestId, startDate, endDate);
        }
        catch (EJBException ex) {
            adminPoPArchiveResponse = new AdminPoPArchiveResponse();
            adminPoPArchiveResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", adminPoPArchiveResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminPoPArchive", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return adminPoPArchiveResponse;
    }

    @Override
    public String adminResendConfirmUserEmail(String adminTicketId, String requestId, String userEmail, String category) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("userEmail", userEmail));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "resendConfirmUserEmail", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();

            String activationLink = this.parametersService.getParamValue(AdminService.PARAM_ACTIVATION_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(AdminService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(AdminService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(AdminService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(AdminService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(AdminService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(AdminService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = adminUserEmailConfirmResendAction.execute(adminTicketId, requestId, userEmail, activationLink, category, emailSender, userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (InterfaceNotFoundException e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get UserCategoryService object");
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "resendConfirmUserEmail", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public ReconciliationInfoData adminReconcileTransactions(String adminTicketId, String requestId, List<String> transactionsID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationReconcileTransactions", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        ReconciliationInfoData reconciliationInfoData = null;

        try {

            reconciliationInfoData = adminReconcileAction.execute(adminTicketId, requestId, transactionsID);
        }

        catch (EJBException ex) {
            reconciliationInfoData = new ReconciliationInfoData();
            reconciliationInfoData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", reconciliationInfoData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationReconcileTransactions", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return reconciliationInfoData;

    }

    @Override
    public ReconciliationDetail adminReconcileDetail(String adminTicketId, String requestId, String transactionsID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionsID", transactionsID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationReconcileDetail", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        ReconciliationDetail reconciliationDetail = null;

        try {

            reconciliationDetail = adminReconcileDetailAction.execute(adminTicketId, requestId, transactionsID);
        }

        catch (EJBException ex) {
            reconciliationDetail = new ReconciliationDetail();
            reconciliationDetail.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", reconciliationDetail.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reconciliationReconcileDetail", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return reconciliationDetail;

    }

    @Override
    public String adminCreateManager(String adminTicketId, String requestId, Manager manager) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("manager.username", manager.getUsername()));
        inputParameters.add(new Pair<String, String>("manager.email", manager.getEmail()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateManager", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminManagerCreateAction.execute(adminTicketId, requestId, manager);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateManager", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminDeleteManager(String adminTicketId, String requestId, Manager manager) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("manager.Id", String.valueOf(manager.getId())));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteManager", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminManagerDeleteAction.execute(adminTicketId, requestId, manager);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteManager", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public String adminUpdateManager(String adminTicketId, String requestId, Manager manager) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("manager.username", manager.getUsername()));
        inputParameters.add(new Pair<String, String>("manager.email", manager.getEmail()));
        inputParameters.add(new Pair<String, String>("manager.status", manager.getStatus().toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateManager", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminManagerUpdateAction.execute(adminTicketId, requestId, manager);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateManager", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public RetrieveManagerListData adminSearchManager(String adminTicketId, String requestId, Long id, String username, String email, Integer maxResults) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", (id == null ? "" : id.toString())));
        inputParameters.add(new Pair<String, String>("username", username));
        inputParameters.add(new Pair<String, String>("email", email));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSearchManager", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveManagerListData retrieveManagerListData = new RetrieveManagerListData();

        try {
            retrieveManagerListData = adminManagerRetrieveAction.execute(adminTicketId, requestId, id, username, email, maxResults);
        }
        catch (EJBException ex) {
            retrieveManagerListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveManagerListData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSearchManager", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveManagerListData;
    }

    @Override
    public String adminAddStationManager(String adminTicketId, String requestId, Long managerId, Long stationId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("managerId", managerId.toString()));
        inputParameters.add(new Pair<String, String>("stationId", stationId.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddStationManager", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminManagerAddStationAction.execute(adminTicketId, requestId, managerId, stationId);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateManager", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminCheckAdminAuthorization(String adminTicketId, String operation) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("operation", operation));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "AdminCheckAdminAuthorization", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminAuthorizationCheckAction.execute(adminTicketId, operation);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "AdminCheckAdminAuthorization", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public RetrievePoPTransactionListData adminRetrievePoPTransactionReconciliation(String adminTicketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePoPTransactionReconciliation", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        RetrievePoPTransactionListData retrievePoPTransactionListData = null;

        try {
            Integer activityLogMaxResults = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_ACTIVITY_LOG_MAX_RESULTS));

            retrievePoPTransactionListData = adminPoPTransactionRetrieveReconciliationAction.execute(adminTicketId, requestId, activityLogMaxResults);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrievePoPTransactionListData = new RetrievePoPTransactionListData();
            retrievePoPTransactionListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            retrievePoPTransactionListData = new RetrievePoPTransactionListData();
            retrievePoPTransactionListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrievePoPTransactionListData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePoPTransactionReconciliation", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return retrievePoPTransactionListData;
    }

    @Override
    public String adminAddVoucherToPromotion(String adminTicketId, String requestId, PromoVoucherInput listPromoVoucher, String promotionCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddVoucherToPromotion", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;

        try {

            response = (String) adminAddVoucherToPromotionAction.commonExeceute(adminTicketId, requestId, null, listPromoVoucher, promotionCode);
        }
        catch (EJBException ex) {

            response = ResponseHelper.ADMIN_ADD_VOUCHER_TO_PROMOTION_VOUCHER_ALREADY_PRESENT;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddVoucherToPromotion", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminSurveyCreate(String adminTicketId, String requestId, String code, String description, String note, Integer status, Date startDate, Date endDate,
            ArrayList<SurveyQuestion> questions) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("code", code));
        inputParameters.add(new Pair<String, String>("description", description));
        inputParameters.add(new Pair<String, String>("note", note));
        inputParameters.add(new Pair<String, String>("status", status.toString()));
        inputParameters.add(new Pair<String, String>("startDate", sdf.format(startDate)));
        inputParameters.add(new Pair<String, String>("endDate", sdf.format(endDate)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyCreate", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;

        try {

            response = adminSurveyCreateAction.execute(adminTicketId, requestId, code, description, note, status, startDate, endDate, questions);
        }
        catch (EJBException ex) {

            response = ResponseHelper.ADMIN_SURVEY_CREATE_FAILURE;
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminSurveyCreate", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyCreate", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public String adminSurveyUpdate(String adminTicketId, String requestId, String code, String description, String note, Integer status, Date startDate, Date endDate) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("code", code));
        inputParameters.add(new Pair<String, String>("description", description));
        inputParameters.add(new Pair<String, String>("note", note));
        inputParameters.add(new Pair<String, String>("status", status.toString()));
        inputParameters.add(new Pair<String, String>("startDate", sdf.format(startDate)));
        inputParameters.add(new Pair<String, String>("endDate", sdf.format(endDate)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyUpdate", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;

        try {

            response = adminSurveyUpdateAction.execute(adminTicketId, requestId, code, description, note, status, startDate, endDate);
        }
        catch (EJBException ex) {

            response = ResponseHelper.ADMIN_SURVEY_UPDATE_FAILURE;
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminSurveyUpdate", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyUpdate", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public String adminSurveyQuestionAdd(String adminTicketId, String requestId, String surveyCode, String questionCode, String text, String type, Integer sequence) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("surveyCode", surveyCode));
        inputParameters.add(new Pair<String, String>("questionCode", questionCode));
        inputParameters.add(new Pair<String, String>("text", text));
        inputParameters.add(new Pair<String, String>("type", type));
        inputParameters.add(new Pair<String, String>("sequence", sequence.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyQuestionAdd", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;

        try {

            response = adminSurveyQuestionAddAction.execute(adminTicketId, requestId, surveyCode, questionCode, text, type, sequence);
        }
        catch (EJBException ex) {

            response = ResponseHelper.ADMIN_SURVEY_QUESTION_ADD_FAILURE;
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminSurveyQuestionAdd", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyQuestionAdd", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public String adminSurveyQuestionUpdate(String adminTicketId, String requestId, String questionCode, String text, String type, Integer sequence) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("questionCode", questionCode));
        inputParameters.add(new Pair<String, String>("text", text));
        inputParameters.add(new Pair<String, String>("type", type));
        inputParameters.add(new Pair<String, String>("sequence", sequence.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyQuestionUpdate", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;

        try {

            response = adminSurveyQuestionUpdateAction.execute(adminTicketId, requestId, questionCode, text, type, sequence);
        }
        catch (EJBException ex) {

            response = ResponseHelper.ADMIN_SURVEY_QUESTION_UPDATE_FAILURE;
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminSurveyQuestionUpdate", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyQuestionUpdate", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public String adminSurveyQuestionDelete(String adminTicketId, String requestId, String questionCode) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("questionCode", questionCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyQuestionDelete", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;

        try {

            response = adminSurveyQuestionDeleteAction.execute(adminTicketId, requestId, questionCode);
        }
        catch (EJBException ex) {

            response = ResponseHelper.ADMIN_SURVEY_QUESTION_DELETE_FAILURE;
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminSurveyQuestionDelete", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyQuestionDelete", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public SurveyList adminSurveyGet(String adminTicketId, String requestId, String surveyCode, Date startDateSearch, Date endDateSearch, Integer status) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("surveyCode", surveyCode));
        inputParameters.add(new Pair<String, String>("startDateSearch", (startDateSearch != null) ? sdf.format(startDateSearch) : "null"));
        inputParameters.add(new Pair<String, String>("endDateSearch", (endDateSearch != null) ? sdf.format(endDateSearch) : "null"));
        inputParameters.add(new Pair<String, String>("status", (status != null) ? status.toString() : "null"));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyGet", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        SurveyList surveyList = null;

        try {

            surveyList = adminSurveyRetrieveAction.execute(adminTicketId, requestId, surveyCode, startDateSearch, endDateSearch, status);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminSurveyGet", requestId, null, ex.getMessage());
            surveyList.setStatusCode(ResponseHelper.ADMIN_SURVEY_GET_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", surveyList.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyGet", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return surveyList;

    }

    @Override
    public String adminSurveyReset(String adminTicketId, String requestId, String surveyCode) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("surveyCode", surveyCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyReset", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {

            response = adminSurveyResetAction.execute(adminTicketId, requestId, surveyCode);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminSurveyReset", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_SURVEY_RESET_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminSurveyReset", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminCardBinAdd(String adminTicketId, String requestId, String bin) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("bin", bin));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCardBinAdd", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {

            response = adminCardBinCreateAction.execute(adminTicketId, requestId, bin);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminCardBinAdd", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_CARD_BIN_ADD_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCardBinAdd", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public RetrievePromotionsData adminRetrievePromotions(String adminTicketId, String requestId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePromotions", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrievePromotionsData response = null;

        try {

            response = adminPromotionsRetrieveAction.execute(adminTicketId, requestId);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminRetrievePromotions", requestId, null, ex.getMessage());
            response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_PROMOTIONS_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePromotions", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminUpdatePromotion(String adminTicketId, String requestId, String code, String description, Integer status, Date startData, Date endData) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("code", code));
        inputParameters.add(new Pair<String, String>("description", description));
        inputParameters.add(new Pair<String, String>("status", status.toString()));
        inputParameters.add(new Pair<String, String>("startData", startData.toString()));
        inputParameters.add(new Pair<String, String>("endData", endData.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePromotion", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {

            response = adminPromotionUpdateAction.execute(adminTicketId, requestId, code, description, status, startData, endData);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminUpdatePromotion", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_RETRIEVE_PROMOTIONS_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePromotion", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public List<AdminErrorResponse> adminGetMappingError(String adminTicket, String requestID, String errorCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("errorCode", errorCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refuelingGetError", "", "opening", ActivityLog.createLogMessage(inputParameters));
        List<AdminErrorResponse> adminErrorResponseList = new ArrayList<AdminErrorResponse>();

        try {
            MappingErrorData refuelingMappingErrorBean = new MappingErrorData();
            refuelingMappingErrorBean = adminErrorRetriveAction.execute(adminTicket, requestID, errorCode);
            if (refuelingMappingErrorBean != null && refuelingMappingErrorBean.getRefuelingMappingError().size() > 0) {
                for (RefuelingMappingError item : refuelingMappingErrorBean.getRefuelingMappingError()) {
                    AdminErrorResponse adminErrorResponse = new AdminErrorResponse();
                    adminErrorResponse.setGpErrorCode(item.getErrorCode());
                    adminErrorResponse.setMpStatusCode(item.getStatusCode());
                    adminErrorResponseList.add(adminErrorResponse);
                }
            }
        }
        catch (EJBException ex) {

        }

        return adminErrorResponseList;
    }

    @Override
    public String adminCreateMappingError(String adminTicket, String requestID, String gpErrorCode, String mpStatusCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("errorCode", gpErrorCode));
        inputParameters.add(new Pair<String, String>("statusCode", mpStatusCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refuelingCreateError", "", "opening", ActivityLog.createLogMessage(inputParameters));

        String errorRefueling = "";
        try {
            errorRefueling = adminErrorCreateAction.execute(adminTicket, requestID, gpErrorCode, mpStatusCode);
        }
        catch (EJBException ex) {

        }

        return errorRefueling;
    }

    @Override
    public String adminEditMappingError(String adminTicket, String requestID, String gpErrorCode, String mpStatusCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("errorCode", gpErrorCode));
        inputParameters.add(new Pair<String, String>("statusCode", mpStatusCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refuelingEditError", "", "opening", ActivityLog.createLogMessage(inputParameters));

        String errorRefueling = "";
        try {
            errorRefueling = adminErrorUpdateAction.execute(adminTicket, requestID, gpErrorCode, mpStatusCode);
        }
        catch (EJBException ex) {

        }

        return errorRefueling;
    }

    @Override
    public String adminRemoveMappingError(String adminTicket, String requestID, String errorCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("errorCode", errorCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refuelingRemoveError", "", "opening", ActivityLog.createLogMessage(inputParameters));

        String errorRefueling = "";
        try {
            errorRefueling = adminErrorDeleteAction.execute(adminTicket, requestID, errorCode);
        }
        catch (EJBException ex) {

        }

        return errorRefueling;
    }

    @Override
    public String adminCreateUserCategory(String adminTicketId, String requestId, String name) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("name", name));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateUserCategory", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {

            response = adminUserCategoryCreateAction.execute(adminTicketId, requestId, name);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminCreateUserCategory", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_USER_CATEGORY_CREATE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateUserCategory", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminCreateUserType(String adminTicketId, String requestId, Integer code) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("code", code.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateUserType", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {

            response = adminUserTypeCreateAction.execute(adminTicketId, requestId, code);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminCreateUserType", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_USERTYPE_CATEGORY_UPDATE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateUserType", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminAddTypeToUserCategory(String adminTicketId, String requestId, String name, Integer code) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("name", name));
        inputParameters.add(new Pair<String, String>("code", code.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddTypeToUserCategory", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {

            response = adminUserCategoryAddTypeAction.execute(adminTicketId, requestId, name, code);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminAddTypeToUserCategory", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_USERTYPE_CATEGORY_UPDATE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddTypeToUserCategory", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminRemoveTypeFromUserCategory(String adminTicketId, String requestId, String name, Integer code) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("name", name));
        inputParameters.add(new Pair<String, String>("code", code.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemoveTypeFromUserCategory", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {

            response = adminUserCategoryDeleteTypeAction.execute(adminTicketId, requestId, name, code);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminRemoveTypeFromUserCategory", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_USERTYPE_CATEGORY_UPDATE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemoveTypeFromUserCategory", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public UserCategoryData adminRetrieveUserCategory(String adminTicketId, String requestId, String name) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("name", name));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveUserCategory", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        UserCategoryData response = new UserCategoryData();

        try {

            response = adminUserCategoryRetrieveAction.execute(adminTicketId, requestId, name);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminRetrieveUserCategory", requestId, null, ex.getMessage());
            response.setStatusCode(ResponseHelper.ADMIN_USER_CATEGORY_CREATE_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveUserCategory", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public UserTypeData adminRetrieveUserType(String adminTicketId, String requestId, Integer code) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        //        inputParameters.add(new Pair<String, String>("code", code.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateUserType", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        UserTypeData response = new UserTypeData();

        try {

            response = adminUserTypeRetrieveAction.execute(adminTicketId, requestId, code);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminCreateUserType", requestId, null, ex.getMessage());
            response.setStatusCode(ResponseHelper.ADMIN_USERTYPE_CATEGORY_UPDATE_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateUserType", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminCreatePrefixNumber(String adminTicketId, String requestId, String code) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("code", code));
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreatePrefixNumber", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";
        try {
            response = adminPrefixNumberCreateAction.execute(adminTicketId, requestId, code);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminCreatePrefixNumber", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_USER_CATEGORY_CREATE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreatePrefixNumber", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public String adminRemovePrefixNumber(String adminTicketId, String requestId, String code) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("code", code));
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemovePrefixNumber", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";
        try {
            response = adminPrefixNumberDeleteAction.execute(adminTicketId, requestId, code);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminRemovePrefixNumber", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_USER_CATEGORY_CREATE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemovePrefixNumber", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public PrefixNumberResult adminRetrieveAllPrefixNumber(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveAllPrefixNumber", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        PrefixNumberResult prefixNumberResult = new PrefixNumberResult();

        try {
            prefixNumberResult = adminRetriveAllPrefixNumberAction.execute(ticketId, requestId);
        }
        catch (Exception ex) {
            prefixNumberResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", prefixNumberResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveAllPrefixNumber", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return prefixNumberResult;

    }

    @Override
    public RetrieveVoucherTransactionListData adminRetrieveVoucherTransactions(String adminTicketId, String requestId, String voucherTransactionId,        //
            Long userId,                  //
            String voucherCode,//
            Date creationTimestampStart,//
            Date creationTimestampEnd,  //
            String finalStatusType) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("voucherTransactionId", voucherTransactionId));
        if (userId != null) {
            inputParameters.add(new Pair<String, String>("userId", userId.toString()));
        }
        inputParameters.add(new Pair<String, String>("voucherCode", voucherCode));
        inputParameters.add(new Pair<String, String>("creationTimestampStart", this.toString(creationTimestampStart)));
        inputParameters.add(new Pair<String, String>("creationTimestampEnd", this.toString(creationTimestampEnd)));
        inputParameters.add(new Pair<String, String>("finalStatusType", finalStatusType));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveVoucherTransaction", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        RetrieveVoucherTransactionListData retrieveVoucherTransactionListData = null;

        try {
            Integer activityLogMaxResults = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_ACTIVITY_LOG_MAX_RESULTS));

            retrieveVoucherTransactionListData = adminVoucherTransactionsRetrieveAction.execute(adminTicketId, requestId, voucherTransactionId, userId, voucherCode,
                    creationTimestampStart, creationTimestampEnd, finalStatusType, activityLogMaxResults);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrieveVoucherTransactionListData = new RetrieveVoucherTransactionListData();
            retrieveVoucherTransactionListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            retrieveVoucherTransactionListData = new RetrieveVoucherTransactionListData();
            retrieveVoucherTransactionListData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveVoucherTransactionListData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveVoucherTransaction", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return retrieveVoucherTransactionListData;

    }

    @Override
    public String adminForceUpdateTermsOfService(String ticketId, String requestId, String keyval, Long personalDataId, Boolean valid) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (keyval != null) {
            inputParameters.add(new Pair<String, String>("termsOfServiceId", keyval));
        }
        if (personalDataId != null) {
            inputParameters.add(new Pair<String, String>("personalDataId", personalDataId.toString()));
        }
        if (valid != null) {
            inputParameters.add(new Pair<String, String>("valid", valid.toString()));
        }
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "forceUpdateTermsOfService", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String forceUpdateTermsOfServiceResult = "";

        try {
            forceUpdateTermsOfServiceResult = adminTermsOfServiceForceUpdateAction.execute(ticketId, requestId, keyval, personalDataId, valid);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", forceUpdateTermsOfServiceResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "forceUpdateTermsOfService", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return forceUpdateTermsOfServiceResult;

    }

    @Override
    public String adminCreateBlockPeriod(String ticketId, String requestId, String code, String startDate, String endDate, String startTime, String endTime, String operation,
            String statusCode, String statusMessage, Boolean active) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (startDate != null) {
            inputParameters.add(new Pair<String, String>("startDate", startDate));
        }
        if (endDate != null) {
            inputParameters.add(new Pair<String, String>("endDate", endDate));
        }
        if (startTime != null) {
            inputParameters.add(new Pair<String, String>("startTime", startTime));
        }
        if (endTime != null) {
            inputParameters.add(new Pair<String, String>("endTime", endTime));
        }
        inputParameters.add(new Pair<String, String>("code", code));

        inputParameters.add(new Pair<String, String>("operation", operation));
        inputParameters.add(new Pair<String, String>("statusCode", statusCode));
        inputParameters.add(new Pair<String, String>("statusMessage", statusMessage));
        if (active != null) {
            inputParameters.add(new Pair<String, String>("active", active.toString()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateBlockPeriod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String createBlockPeriodResult = "";

        try {
            createBlockPeriodResult = adminBlockPeriodCreateAction.execute(ticketId, requestId, code, startDate, endDate, startTime, endTime, operation, statusCode, statusMessage,
                    active);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", createBlockPeriodResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateBlockPeriod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return createBlockPeriodResult;

    }

    @Override
    public String adminUpdateBlockPeriod(String ticketId, String requestId, String code, String startDate, String endDate, String startTime, String endTime, String operation,
            String statusCode, String statusMessage, Boolean active) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (startDate != null) {
            inputParameters.add(new Pair<String, String>("startDate", startDate));
        }
        if (endDate != null) {
            inputParameters.add(new Pair<String, String>("endDate", endDate));
        }
        if (startTime != null) {
            inputParameters.add(new Pair<String, String>("startTime", startTime));
        }
        if (endTime != null) {
            inputParameters.add(new Pair<String, String>("endTime", endTime));
        }
        if (code != null) {
            inputParameters.add(new Pair<String, String>("code", code));
        }
        if (operation != null) {
            inputParameters.add(new Pair<String, String>("operation", operation));
        }
        if (statusCode != null) {
            inputParameters.add(new Pair<String, String>("statusCode", statusCode));
        }
        if (statusMessage != null) {
            inputParameters.add(new Pair<String, String>("statusMessage", statusMessage));
        }
        if (active != null) {
            inputParameters.add(new Pair<String, String>("active", active.toString()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateBlockPeriod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String updateBlockPeriodResult = "";

        try {
            updateBlockPeriodResult = adminBlockPeriodUpdateAction.execute(ticketId, requestId, code, startDate, endDate, startTime, endTime, operation, statusCode, statusMessage,
                    active);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", updateBlockPeriodResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateBlockPeriod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return updateBlockPeriodResult;

    }

    @Override
    public UnavailabilityPeriodResponse adminRetrieveBlockPeriod(String ticketId, String requestId, String code, Boolean active) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (code != null) {
            inputParameters.add(new Pair<String, String>("code", code));
        }
        if (active != null) {
            inputParameters.add(new Pair<String, String>("active", active.toString()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteBlockPeriod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        UnavailabilityPeriodResponse unavailabilityPeriodResponse = new UnavailabilityPeriodResponse();

        try {
            unavailabilityPeriodResponse = adminBlockPeriodRetrieveAction.execute(ticketId, requestId, code, active);
        }
        catch (Exception ex) {
            unavailabilityPeriodResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return unavailabilityPeriodResponse;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", unavailabilityPeriodResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteBlockPeriod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return unavailabilityPeriodResponse;

    }

    @Override
    public String adminDeleteBlockPeriod(String ticketId, String requestId, String code) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (code != null) {
            inputParameters.add(new Pair<String, String>("code", code));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteBlockPeriod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String deleteBlockPeriodResult = "";

        try {
            deleteBlockPeriodResult = adminBlockPeriodDeleteAction.execute(ticketId, requestId, code);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", deleteBlockPeriodResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteBlockPeriod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return deleteBlockPeriodResult;

    }

    @Override
    public String adminUpdateDocument(String adminTicketId, String requestID, String documentKey, Integer position, String templateFile, String title, String subtitle,
            String userCategory, String groupCategory) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("documentKey", documentKey));

        if (position != null) {
            inputParameters.add(new Pair<String, String>("position", position.toString()));
        }
        if (templateFile != null) {
            inputParameters.add(new Pair<String, String>("templateFile", templateFile));
        }
        if (title != null) {
            inputParameters.add(new Pair<String, String>("title", title));
        }
        if (userCategory != null) {
            inputParameters.add(new Pair<String, String>("userCategory", userCategory));
        }
        if (groupCategory != null) {
            inputParameters.add(new Pair<String, String>("groupCategory", groupCategory));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateDocument", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String updateDocumentResult = "";

        try {
            updateDocumentResult = adminDocumentUpdateAction.execute(adminTicketId, requestID, documentKey, position, templateFile, title, subtitle, userCategory, groupCategory);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", updateDocumentResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateDocument", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return updateDocumentResult;

    }

    @Override
    public String adminCreateDocument(String adminTicketId, String requestID, String documentKey, Integer position, String templateFile, String title, String subtitle,
            List<DocumentAttribute> attributes, String userCategory, String groupCategory) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        if (documentKey != null) {
            inputParameters.add(new Pair<String, String>("documentKey", documentKey));
        }
        if (position != null) {
            inputParameters.add(new Pair<String, String>("position", position.toString()));
        }
        if (templateFile != null) {
            inputParameters.add(new Pair<String, String>("templateFile", templateFile));
        }
        if (title != null) {
            inputParameters.add(new Pair<String, String>("title", title));
        }
        if (userCategory != null) {
            inputParameters.add(new Pair<String, String>("userCategory", userCategory));
        }
        if (groupCategory != null) {
            inputParameters.add(new Pair<String, String>("groupCategory", groupCategory));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateDocument", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String createDocumentResult = "";

        try {
            createDocumentResult = adminDocumentCreateAction.execute(adminTicketId, requestID, documentKey, position, templateFile, title, subtitle, attributes, userCategory,
                    groupCategory);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", createDocumentResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateDocument", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createDocumentResult;

    }

    @Override
    public String adminDeleteDocument(String adminTicketId, String requestID, String documentKey) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("documentKey", documentKey));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteDocument", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String deleteDocumentResult = "";

        try {
            deleteDocumentResult = adminDocumentDeleteAction.execute(adminTicketId, requestID, documentKey);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", deleteDocumentResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteDocument", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return deleteDocumentResult;

    }

    @Override
    public RetrieveDocumentResponse adminRetrieveDocument(String adminTicketId, String requestID, String documentKey) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("documentKey", documentKey));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveDocument", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveDocumentResponse retrieveDocuResult = null;

        try {
            retrieveDocuResult = adminDocumentRetrieveAction.execute(adminTicketId, requestID, documentKey);
        }
        catch (Exception ex) {
            retrieveDocuResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return retrieveDocuResult;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", retrieveDocuResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveDocument", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveDocuResult;

    }

    @Override
    public String adminCreateDocumentAttribute(String adminTicketId, String requestID, String checkKey, Boolean mandatory, Integer position, String conditionText,
            String extendedConditionText, String documentKey, String subtitle) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("checkKey", checkKey));
        inputParameters.add(new Pair<String, String>("mandatory", mandatory.toString()));
        inputParameters.add(new Pair<String, String>("position", position.toString()));

        if (conditionText != null) {
            inputParameters.add(new Pair<String, String>("conditionText", conditionText));
        }
        if (extendedConditionText != null) {
            inputParameters.add(new Pair<String, String>("extendedConditionText", extendedConditionText));
        }

        inputParameters.add(new Pair<String, String>("documentKey", documentKey));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateDocument", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String createDocumentAttributeResult = "";

        try {
            createDocumentAttributeResult = adminDocumentAttributeCreateAction.execute(adminTicketId, requestID, checkKey, mandatory, position, conditionText,
                    extendedConditionText, documentKey, subtitle);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", createDocumentAttributeResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateDocument", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createDocumentAttributeResult;

    }

    @Override
    public String adminDeleteDocumentAttribute(String adminTicketId, String requestID, String checkKey) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("checkKey", checkKey));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteDocumentAttribute", requestID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String deleteDocumentAttributeResult = "";

        try {
            deleteDocumentAttributeResult = adminDocumentAttributeDeleteAction.execute(adminTicketId, requestID, checkKey);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", deleteDocumentAttributeResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteDocumentAttribute", requestID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return deleteDocumentAttributeResult;

    }

    @Override
    public GeneratePvResponse adminGeneratePv(String ticketId, String requestID, Integer finalStatus, Boolean skipCheck, String noOilAcquirerId, String noOilShopLogin,
            String oilAcquirerId, String oilShopLogin, List<StationData> stationList) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("finalStatus", finalStatus.toString()));
        inputParameters.add(new Pair<String, String>("noOilAcquirerId", noOilAcquirerId));
        inputParameters.add(new Pair<String, String>("noOilShopLogin", noOilShopLogin));
        inputParameters.add(new Pair<String, String>("oilAcquirerId", oilAcquirerId));
        inputParameters.add(new Pair<String, String>("oilShopLogin", oilShopLogin));

        int count = 0;
        String stationListString = "{";
        for (StationData station : stationList) {
            if (count != 0) {
                stationListString += ",";
            }
            stationListString += stationListString + " \"" + station.getStationID() + "\"";
            count++;
        }
        stationListString += "}";

        inputParameters.add(new Pair<String, String>("stationListString", stationListString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGeneratePv", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GeneratePvResponse generatePvResponse = new GeneratePvResponse();

        try {
            generatePvResponse = adminGeneratePvAction.execute(ticketId, requestID, finalStatus, skipCheck, noOilAcquirerId, noOilShopLogin, oilAcquirerId, oilShopLogin,
                    stationList, this.forecourtInfoService, this.secretKey);
        }
        catch (Exception ex) {
            generatePvResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return generatePvResponse;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", generatePvResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGeneratePv", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return generatePvResponse;
    }

    @Override
    public String adminUpdatePassword(String adminTicketId, String requestID, String oldPassword, String newPassword) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("oldPassword", oldPassword));
        inputParameters.add(new Pair<String, String>("newPassword", newPassword));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePassword", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String result = null;

        try {
            result = adminPasswordUpdateAction.execute(adminTicketId, requestID, oldPassword, newPassword);
        }
        catch (Exception ex) {
            result = ResponseHelper.SYSTEM_ERROR;
            return result;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", result));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePassword", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return result;

    }

    @Override
    public String adminForceSmsNotificationStatus(String ticketId, String requestId, String correlationId, Integer status) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (correlationId != null) {
            inputParameters.add(new Pair<String, String>("correlationId", correlationId));
        }
        inputParameters.add(new Pair<String, String>("status", status.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "forceSmsNotificationStatus", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String adminForceSmsNotificationStatusResult = "";

        try {
            adminForceSmsNotificationStatusResult = adminForceSmsNotificationStatusAction.execute(ticketId, requestId, correlationId, status);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", adminForceSmsNotificationStatusResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "forceSmsNotificationStatus", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return adminForceSmsNotificationStatusResult;

    }

    @Override
    public String adminGenerateMailingList(String ticketId, String requestId, String selectionType) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("selectionType", selectionType));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGenerateMailingList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String adminGenerateMailingListResult = "";

        try {
            adminGenerateMailingListResult = adminGenerateMailingListAction.execute(ticketId, requestId, selectionType);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", adminGenerateMailingListResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGenerateMailingList", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return adminGenerateMailingListResult;
    }

    @Override
    public String adminUpdateMailingList(String ticketId, String requestId, Long id, String email, String name, Integer status, String template) {

        String stringId = "";
        if (id != null) {
            stringId = id.toString();
        }

        String stringStatus = "";
        if (status != null) {
            stringStatus = status.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", stringId));
        inputParameters.add(new Pair<String, String>("email", email));
        inputParameters.add(new Pair<String, String>("name", name));
        inputParameters.add(new Pair<String, String>("ststus", stringStatus));
        inputParameters.add(new Pair<String, String>("template", template));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateMailingList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String adminUpdateMailingListResult = "";

        try {
            adminUpdateMailingListResult = adminMailingListUpdateAction.execute(ticketId, requestId, id, email, name, status, template);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", adminUpdateMailingListResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateMailingList", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return adminUpdateMailingListResult;
    }

    @Override
    public String adminAssignVoucher(String ticketId, String requestId, Long userId, String voucherCode) {

        String stringUserId = "";
        if (userId != null) {
            stringUserId = userId.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("userId", stringUserId));
        inputParameters.add(new Pair<String, String>("voucherCode", voucherCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAssignVoucher", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String adminAssignVoucherResult = "";

        try {
            adminAssignVoucherResult = adminVoucherAssignAction.execute(ticketId, requestId, userId, voucherCode, this.fidelityService);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", adminAssignVoucherResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAssignVoucher", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return adminAssignVoucherResult;
    }

    @Override
    public String adminRemoveTransactionEvent(String ticketId, String requestId, Long transactionEventId) {

        String stringTransactionEventId = "";
        if (transactionEventId != null) {
            stringTransactionEventId = transactionEventId.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionEventId", stringTransactionEventId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemoveTransactionEvent", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String adminRemoveTransactionEventResult = "";

        try {
            adminRemoveTransactionEventResult = adminTransactionEventDeleteAction.execute(ticketId, requestId, transactionEventId);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", adminRemoveTransactionEventResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemoveTransactionEvent", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return adminRemoveTransactionEventResult;
    }

    @Override
    public String adminInsertUserInPromoOnHold(String ticketId, String requestId, String promotionCode, Long userId) {

        String stringUserId = "";
        if (userId != null) {
            stringUserId = userId.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("promotionCode", promotionCode));
        inputParameters.add(new Pair<String, String>("userId", stringUserId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminInsertUserInPromoOnHold", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String adminInsertUserInPromoOnHoldResult = "";

        try {
            adminInsertUserInPromoOnHoldResult = adminUserInsertInPromoOnHoldAction.execute(ticketId, requestId, promotionCode, userId);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", adminInsertUserInPromoOnHoldResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminInsertUserInPromoOnHold", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return adminInsertUserInPromoOnHoldResult;
    }

    @Override
    public String adminCreateEmailDomainInBlackList(String adminTicketId, String requestId, List<String> emails) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        String emailsString = "";
        for (String email : emails) {
            emailsString = emailsString + email + ", ";
        }
        inputParameters.add(new Pair<String, String>("emails", emailsString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateEmailDomainInBlackList", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminEmailDomainCreateAction.execute(adminTicketId, requestId, emails);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminCreateEmailDomainInBlackList", requestId, null, ex.getMessage());
            response = ResponseHelper.MAIL_CREATE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateEmailDomainInBlackList", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminEditEmailDomainInBlackList(String adminTicketId, String requestId, String id, String email) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", id));
        inputParameters.add(new Pair<String, String>("email", email));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminEditEmailDomainInBlackList", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminEditEmailDomainAction.execute(adminTicketId, requestId, id, email);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminEditEmailDomainInBlackList", requestId, null, ex.getMessage());
            response = ResponseHelper.MAIL_EDIT_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminEditEmailDomainInBlackList", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminRemoveEmailDomainInBlackList(String adminTicketId, String requestId, String email) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("email", email));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemoveEmailDomainInBlackList", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminRemoveEmailDomainAction.execute(adminTicketId, requestId, email);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminRemoveEmailDomainInBlackList", requestId, null, ex.getMessage());
            response = ResponseHelper.MAIL_EDIT_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemoveEmailDomainInBlackList", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public List<String> adminGetEmailDomainInBlackList(String adminTicketId, String requestId, String email) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("email", email));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGetEmailDomainInBlackList", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        List<String> response = new ArrayList<String>();

        try {
            response = adminGetEmailDomainAction.execute(adminTicketId, requestId, email);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminGetEmailDomainInBlackList", requestId, null, ex.getMessage());
        }

        Set<Pair<String, List<String>>> outputParameters = new HashSet<Pair<String, List<String>>>();
        outputParameters.add(new Pair<String, List<String>>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGetEmailDomainInBlackList", requestId, "closing", "");

        return response;
    }

    @Override
    public String adminUpdatePopTransaction(String ticketId, String requestId, String transactionId, String mpTransactionStatus, Boolean toReconcile) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionId", transactionId));
        inputParameters.add(new Pair<String, String>("mpTransactionStatus", mpTransactionStatus));
        if (toReconcile != null) {
            inputParameters.add(new Pair<String, String>("toReconcile", toReconcile.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("toReconcile", "null"));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePopTransaction", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminPoPTransactionUpdateAction.execute(ticketId, requestId, transactionId, mpTransactionStatus, toReconcile);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminUpdatePopTransaction", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePopTransaction", requestId, "closing", "");

        return response;
    }

    @Override
    public String adminUpdateVoucherTransaction(String ticketId, String requestId, String voucherTransactionId, String finalStatusType) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("voucherTransactionId", voucherTransactionId));
        inputParameters.add(new Pair<String, String>("finalStatusType", finalStatusType));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateVoucherTransaction", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminVoucherTransactionUpdateAction.execute(ticketId, requestId, voucherTransactionId, finalStatusType);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminUpdateVoucherTransaction", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateVoucherTransaction", requestId, "closing", "");

        return response;
    }

    @Override
    public String adminUpdateVoucherData(String ticketId, String requestId, Long id, String status) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (id == null) {
            inputParameters.add(new Pair<String, String>("id", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("id", id.toString()));
        }
        inputParameters.add(new Pair<String, String>("status", status));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateVoucherData", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminVoucherDataUpdateAction.execute(ticketId, requestId, id, status);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminUpdateVoucherData", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateVoucherData", requestId, "closing", "");

        return response;
    }

    @Override
    public String adminDeleteVoucher(String ticketId, String requestId, Long id) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", id.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteVoucher", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminVoucherDeleteAction.execute(ticketId, requestId, id);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminDeleteVoucher", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteVoucher", requestId, "closing", "");

        return response;
    }

    @Override
    public String adminCreateProvince(String ticketId, String requestId, List<ProvinceData> province) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateProvince", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminProvinceCreateAction.execute(ticketId, requestId, province);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminCreateProvince", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateProvince", requestId, "closing", "");

        return response;
    }

    @Override
    public String adminRemoveMobilePhone(String ticketId, String requestId, Long mobilePhoneId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (mobilePhoneId == null) {
            inputParameters.add(new Pair<String, String>("mobilePhoneId", "null"));
        }
        else {
            inputParameters.add(new Pair<String, String>("mobilePhoneId", mobilePhoneId.toString()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemoveMobilePhone", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminMobilePhoneDeleteAction.execute(ticketId, requestId, mobilePhoneId);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminRemoveMobilePhone", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemoveMobilePhone", requestId, "closing", "");

        return response;
    }

    @Override
    public List<AdminPropagationUserDataResult> adminPropagationUserData(String adminTicketId, String requestId, Long userID, List<String> fiscalCode) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("userID", (userID == null) ? "" : userID.toString()));
        
        String fiscalCodeToString = "{ ";
        
        if (fiscalCode != null) {
            for (int i = 0; i < fiscalCode.size(); i++) {
                fiscalCodeToString += fiscalCode.get(i);
                
                if (i <= (fiscalCode.size() - 2)) {
                    fiscalCodeToString += ", ";    
                }
            }
        }
        
        fiscalCodeToString += " }";
        
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCodeToString));
        
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminPropagationUserData", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        List<AdminPropagationUserDataResult> resultList = new ArrayList<AdminPropagationUserDataResult>();

        try {
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            resultList = adminUserDataPropagationAction.execute(adminTicketId, requestId, userID, reconciliationMaxAttempts, fiscalCode);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            AdminPropagationUserDataResult adminPropagationUserDataResponse = new AdminPropagationUserDataResult();
            adminPropagationUserDataResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            adminPropagationUserDataResponse.setStatusMessage(ex.getMessage());
            resultList.add(adminPropagationUserDataResponse);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminPropagationUserData", requestId, null, ex.getMessage());
            AdminPropagationUserDataResult adminPropagationUserDataResponse = new AdminPropagationUserDataResult();
            adminPropagationUserDataResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            adminPropagationUserDataResponse.setStatusMessage(ex.getMessage());
            resultList.add(adminPropagationUserDataResponse);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        
        String resultToString = "";
        
        for (AdminPropagationUserDataResult result : resultList) {
            resultToString = "{ " +
                        "statuCode: " + this.toString(result.getStatusCode()) +
                        ", statusMessage: " + this.toString(result.getStatusMessage()) +
                        " }";
            
            outputParameters.add(new Pair<String, String>(this.toString(result.getFiscalCode()), resultToString));
        }
        
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminPropagationUserData", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return resultList;
    }

    @Override
    public String adminGeneralCreate(String adminTicketId, String requestId, String type, List<TypeData> fields) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("type", type));
        for (TypeData item : fields) {
            inputParameters.add(new Pair<String, String>(item.getField(), item.getValue()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGeneralCreate", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminGeneralCreateAction.execute(adminTicketId, requestId, type, fields);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminGeneralCreate", requestId, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGeneralCreate", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminGeneralUpdate(String adminTicketId, String requestId, String type, Long id, List<TypeData> fields) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("type", type));
        inputParameters.add(new Pair<String, String>("id", id.toString()));
        for (TypeData item : fields) {
            inputParameters.add(new Pair<String, String>(item.getField(), item.getValue()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGeneralUpdate", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminGeneralUpdateAction.execute(adminTicketId, requestId, type, id, fields);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminGeneralUpdate", requestId, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGeneralUpdate", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminUpdatePrepaidConsumeVoucher(String ticketId, String requestId, Long id, String csTransactionID, String marketingMsg, String messageCode, String operationID,
            String operationIDReversed, String operationType, Boolean reconciled, String statusCode, Double totalConsumed, String warningMsg, Double amount,
            String preAuthOperationID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (id == null) {
            inputParameters.add(new Pair<String, String>("id", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("id", id.toString()));
        }
        inputParameters.add(new Pair<String, String>("csTransactionID", csTransactionID));
        inputParameters.add(new Pair<String, String>("marketingMsg", marketingMsg));
        inputParameters.add(new Pair<String, String>("messageCode", messageCode));
        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("operationIDReversed", operationIDReversed));
        inputParameters.add(new Pair<String, String>("operationType", operationType));
        if (reconciled == null) {
            inputParameters.add(new Pair<String, String>("reconciled", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("reconciled", reconciled.toString()));
        }
        inputParameters.add(new Pair<String, String>("statusCode", statusCode));
        if (totalConsumed == null) {
            inputParameters.add(new Pair<String, String>("totalConsumed", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("totalConsumed", totalConsumed.toString()));
        }
        inputParameters.add(new Pair<String, String>("requestId", warningMsg));
        if (amount == null) {
            inputParameters.add(new Pair<String, String>("amount", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        }
        inputParameters.add(new Pair<String, String>("preAuthOperationID", preAuthOperationID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePrepaidConsumeVoucher", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminPrepaidConsumeVoucherUpdateAction.execute(ticketId, requestId, id, csTransactionID, marketingMsg, messageCode, operationID, operationIDReversed,
                    operationType, reconciled, statusCode, totalConsumed, warningMsg, amount, preAuthOperationID);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminUpdatePrepaidConsumeVoucher", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePrepaidConsumeVoucher", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminAddPrepaidConsumeVoucherDetail(String ticketId, String requestId, Double consumedValue, Date expirationDate, Double initialValue, String promoCode,
            String promoDescription, String promoDoc, Double voucherBalanceDue, String voucherCode, String voucherStatus, String voucherType, Double voucherValue,
            Long prePaidConsumeVoucherId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (consumedValue == null) {
            inputParameters.add(new Pair<String, String>("consumedValue", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("consumedValue", consumedValue.toString()));
        }
        if (expirationDate == null) {
            inputParameters.add(new Pair<String, String>("expirationDate", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("expirationDate", expirationDate.toString()));
        }
        if (initialValue == null) {
            inputParameters.add(new Pair<String, String>("initialValue", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("initialValue", initialValue.toString()));
        }
        inputParameters.add(new Pair<String, String>("promoCode", promoCode));
        inputParameters.add(new Pair<String, String>("promoDescription", promoDescription));
        inputParameters.add(new Pair<String, String>("promoDoc", promoDoc));
        if (voucherBalanceDue == null) {
            inputParameters.add(new Pair<String, String>("voucherBalanceDue", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("voucherBalanceDue", voucherBalanceDue.toString()));
        }
        inputParameters.add(new Pair<String, String>("voucherCode", voucherCode));

        inputParameters.add(new Pair<String, String>("voucherStatus", voucherStatus));
        inputParameters.add(new Pair<String, String>("voucherType", voucherType));
        if (voucherValue == null) {
            inputParameters.add(new Pair<String, String>("voucherValue", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("voucherValue", voucherValue.toString()));
        }
        if (prePaidConsumeVoucherId == null) {
            inputParameters.add(new Pair<String, String>("prePaidConsumeVoucherId", ""));
        }
        else {
            inputParameters.add(new Pair<String, String>("prePaidConsumeVoucherId", prePaidConsumeVoucherId.toString()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddPrepaidConsumeVoucherDetail", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminPrepaidConsumeVoucherDetailCreateAction.execute(ticketId, requestId, consumedValue, expirationDate, initialValue, promoCode, promoDescription,
                    promoDoc, voucherBalanceDue, voucherCode, voucherStatus, voucherType, voucherValue, prePaidConsumeVoucherId);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminAddPrepaidConsumeVoucherDetail", requestId, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddPrepaidConsumeVoucherDetail", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminGeneralDelete(String ticketId, String requestId, String type, String id) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGeneralDelete", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminGeneralDeleteAction.execute(ticketId, requestId, type, id);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminGeneralDelete", requestId, null, ex.getMessage());
        }
        catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGeneralDelete", requestId, "closing", "");

        return response;
    }

    @Override
    public String adminCreateAdminRole(String adminTicketId, String requestID, String name) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("name", name));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateDocument", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String createAdminRoleResult = "";

        try {
            createAdminRoleResult = adminRoleCreateAction.execute(adminTicketId, requestID, name);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", createAdminRoleResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateDocument", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createAdminRoleResult;

    }

    @Override
    public String adminDeleteAdminRole(String adminTicketId, String requestID, String name) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("name", name));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteAdminRole", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String deleteAdminRoleResult = "";

        try {
            deleteAdminRoleResult = adminRoleDeleteAction.execute(adminTicketId, requestID, name);
        }
        catch (Exception ex) {
            return ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", deleteAdminRoleResult));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteAdminRole", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return deleteAdminRoleResult;

    }

    @Override
    public String adminAddRoleToAdmin(String adminTicketId, String requestId, String role, String admin) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("role", role));
        inputParameters.add(new Pair<String, String>("admin", admin));

        //        inputParameters.add(new Pair<String, String>("code", code.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddRoleToAdmin", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {

            response = adminRoleAddToAdminAction.execute(adminTicketId, requestId, role, admin);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminAddRoleToAdmin", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_ADD_ROLE_TO_ADMIN_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddRoleToAdmin", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public AdminRolesResponse retrieveAdminRole(String adminTicketId, String requestId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveAdminRole", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AdminRolesResponse response = new AdminRolesResponse();

        try {

            response = adminRoleRetrieveAction.execute(adminTicketId, requestId);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "retrieveAdminRole", requestId, null, ex.getMessage());
            response.setStatusCode(ResponseHelper.ADMIN_ADMIN_ROLE_RETRIEVE_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveAdminRole", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminRemoveRoleToAdmin(String adminTicketId, String requestId, String role, String admin) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("role", role));
        inputParameters.add(new Pair<String, String>("admin", admin));

        //        inputParameters.add(new Pair<String, String>("code", code.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddRoleToAdmin", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {

            response = adminRoleRemoveToAdminAction.execute(adminTicketId, requestId, role, admin);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminAddRoleToAdmin", requestId, null, ex.getMessage());
            response = ResponseHelper.ADMIN_ADMIN_ROLE_REMOVE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminAddRoleToAdmin", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminPushNotificationSend(String adminTicketId, String requestID, Long userID, Long idMessage, String titleNotification, String messageNotification) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("userID", userID.toString()));
        inputParameters.add(new Pair<String, String>("idMessage", idMessage.toString()));
        inputParameters.add(new Pair<String, String>("titleNotification", titleNotification));
        inputParameters.add(new Pair<String, String>("messageNotification", messageNotification));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminPushNotificationSend", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {

            response = adminPushNotificationSendAction.execute(adminTicketId, requestID, userID, idMessage, titleNotification, messageNotification);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminPushNotificationSend", requestID, null, ex.getMessage());
            response = ResponseHelper.ADMIN_ADMIN_ROLE_REMOVE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminPushNotificationSend", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminUpdate(String adminTicketId, String requestID, Admin admin) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("adminID", admin.getId().toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdate", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {

            response = adminUpdateAction.execute(adminTicketId, requestID, admin);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminUpdate", requestID, null, ex.getMessage());
            response = ResponseHelper.ADMIN_UPDATE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdate", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public AdminRetrieveDataResponse adminRetrieve(String adminTicketId, String requestID, Long id, String email, Integer status) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        if (email != null) {
            inputParameters.add(new Pair<String, String>("email", email));
        }
        if (status != null) {
            inputParameters.add(new Pair<String, String>("status", status.toString()));
        }
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieve", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        AdminRetrieveDataResponse response = new AdminRetrieveDataResponse();

        try {

            response = adminRetrieveAction.execute(adminTicketId, requestID, id, email, status);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminRetrieve", requestID, null, ex.getMessage());
            response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieve", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminDelete(String adminTicketId, String requestID, Long adminID) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));
        inputParameters.add(new Pair<String, String>("adminID", adminID.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDelete", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {

            response = adminDeleteAction.execute(adminTicketId, requestID, adminID);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminDelete", requestID, null, ex.getMessage());
            response = ResponseHelper.ADMIN_DELETE_FAILURE;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDelete", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminCreateRefuelingUser(String ticketId, String requestId, String username, String password) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("username", username));
        inputParameters.add(new Pair<String, String>("password", password));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateRefuelingUser", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {

            response = adminCreateRefuelingUserAction.execute(ticketId, requestId, username, password);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCreateRefuelingUser", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String adminUpdatePasswordRefuelingUser(String ticketId, String requestId, String username, String newPassword) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("username", username));
        inputParameters.add(new Pair<String, String>("newPassword", newPassword));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePasswordRefuelingUser", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {

            response = adminUpdatePasswordRefuelingUserAction.execute(ticketId, requestId, username, newPassword);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePasswordRefuelingUser", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public PaymentMethodResponse adminInsertPaymentMethodRefuelingUser(String ticketId, String requestId, String username) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("username", username));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "insertPaymentMethod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        PaymentMethodResponse paymentMethodResponse = null;
        try {
            String apiKey = this.parametersService.getParamValue(AdminService.PARAM_APIKEY_CARTASI);
            String uicCode = this.parametersService.getParamValue(AdminService.PARAM_UICCODE_DEPOSIT_CARTASI);
            String groupAcquirer = this.parametersService.getParamValue(AdminService.PARAM_GROUP_ACQUIRER_CARTASI);
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            String checkAmountValue = this.parametersService.getParamValue(AdminService.PARAM_CHECK_AMOUNT_VALUE);
            Double maxCheckAmount = Double.valueOf(this.parametersService.getParamValue(AdminService.PARAM_MAX_CHECK_AMOUNT));

            String encodedSecretKey = this.parametersService.getParamValue(AdminService.PARAM_ENCODED_SECRET_KEY_CARTASI);
            String decodedSecretKey = "";

            if (encodedSecretKey != null && !encodedSecretKey.equals("")) {

                EncryptionAES encryptionAES = new EncryptionAES();
                encryptionAES.loadSecretKey(this.secretKey);
                decodedSecretKey = encryptionAES.decrypt(encodedSecretKey);
            }

            paymentMethodResponse = adminInsertPaymentMethodRefuelingUserAction.execute(ticketId, requestId, username, pinCheckMaxAttempts, apiKey, checkAmountValue,
                    maxCheckAmount, uicCode, groupAcquirer, decodedSecretKey);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            paymentMethodResponse = new PaymentMethodResponse();
            paymentMethodResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            paymentMethodResponse = new PaymentMethodResponse();
            paymentMethodResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception e) {
            e.printStackTrace();

            paymentMethodResponse = new PaymentMethodResponse();
            paymentMethodResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", paymentMethodResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "insertPaymentMethod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return paymentMethodResponse;
    }

    @Override
    public String adminRemovePaymentMethodRefuelingUser(String ticketId, String requestId, String username, Long id) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("username", username));
        inputParameters.add(new Pair<String, String>("id", id.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemovePaymentMethodRefuelingUser", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {

            response = adminRemovePaymentMethodRefuelingUserAction.execute(ticketId, requestId, username, id, cardInfoService);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRemovePaymentMethodRefuelingUser", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public PaymentDataResponse adminRetrievePaymentDataMethodRefuelingUser(String ticketId, String requestId, String username, Long id) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("username", username));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePaymentDataMethodRefuelingUser", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        PaymentDataResponse response = null;
        try {

            response = adminRetrievePaymentDataRefuelingUserAction.execute(ticketId, requestId, username, id);
        }

        catch (EJBException ex) {
            response = new PaymentDataResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePaymentDataMethodRefuelingUser", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }
    
    @Override
    public PaymentMethodResponse adminInsertMulticardRefuelingUser(String ticketId, String requestId, String username) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("username", username));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminInsertMulticardRefuelingUser", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        PaymentMethodResponse paymentMethodResponse = null;
        try {
            
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            String plainKey = this.parametersService.getParamValue(AdminService.PARAM_DEPOSIT_CARD_MULTICARD_BO_KEY);
            String notifyOkUrl = this.parametersService.getParamValue(AdminService.PARAM_DEPOSIT_CARD_MULTICARD_BO_NOTIFY_OK_URL);
            String notifyKoUrl = this.parametersService.getParamValue(AdminService.PARAM_DEPOSIT_CARD_MULTICARD_BO_NOTIFY_KO_URL);
            String depositCardMulticardBoUrl = this.parametersService.getParamValue(AdminService.PARAM_DEPOSIT_CARD_MULTICARD_BO_URL);
            Integer loyaltySessionExpiryTime = Integer.valueOf(this.parametersService.getParamValue(AdminService.PARAM_LOYALTY_SESSION_EXPIRY_TIME));
            
            paymentMethodResponse = adminInsertMulticardRefuelingUserAction.execute(ticketId, requestId, username, pinCheckMaxAttempts, loyaltySessionExpiryTime, plainKey, notifyOkUrl, notifyKoUrl, depositCardMulticardBoUrl);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            paymentMethodResponse = new PaymentMethodResponse();
            paymentMethodResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            paymentMethodResponse = new PaymentMethodResponse();
            paymentMethodResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception e) {
            e.printStackTrace();

            paymentMethodResponse = new PaymentMethodResponse();
            paymentMethodResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", paymentMethodResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminInsertMulticardRefuelingUser", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return paymentMethodResponse;
    }

    @Override
    public String adminGeneralMassiveDelete(String ticketId, String requestId, String type, String idFrom, String idTo) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGeneralMassiveDelete", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = "";

        try {
            response = adminGeneralMassiveDeleteAction.execute(ticketId, requestId, type, idFrom, idTo, this);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminGeneralMassiveDelete", requestId, null, ex.getMessage());
        }
        catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminGeneralMassiveDelete", requestId, "closing", "");

        return response;
    }
    
    @Override
    public AdminDeleteTicketResponse adminTicketDelete(String ticketId, String requestId, String ticketType, String idFrom, String idTo) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("ticketType", ticketType));
        inputParameters.add(new Pair<String, String>("idFrom", idFrom));
        inputParameters.add(new Pair<String, String>("idTo", idTo));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminTicketDelete", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AdminDeleteTicketResponse adminDeleteTicketResponse = null;

        try {
            adminDeleteTicketResponse = adminTicketDeleteAction.execute(ticketId, requestId, ticketType, idFrom, idTo, this);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminTicketDelete", requestId, null, ex.getMessage());
            
            adminDeleteTicketResponse = new AdminDeleteTicketResponse();
            adminDeleteTicketResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", adminDeleteTicketResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminTicketDelete", requestId, "closing", "");

        return adminDeleteTicketResponse;
    }


    @Override
    public RetrieveLandingResponse adminRetrieveLanding(String ticketId, String requestId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveLanding", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveLandingResponse retrieveLandingResponse = null;

        try {
            retrieveLandingResponse = adminRetrieveLandingAction.execute(ticketId, requestId);
        }
        catch (EJBException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminRetrieveLanding", requestId, null, ex.getMessage());

            retrieveLandingResponse = new RetrieveLandingResponse();
            retrieveLandingResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveLandingResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrieveLanding", requestId, "closing", "");

        return retrieveLandingResponse;
    }

    @Override
    public PollingIntervalData adminRetrievePollingInterval(String adminTicketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePollingInterval", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        PollingIntervalData pollingIntervalData = null;

        try {
            pollingIntervalData = adminPollingIntervalRetrieveAction.execute(adminTicketId, requestId);
        }
        catch (EJBException ex) {
            pollingIntervalData = new PollingIntervalData();
            pollingIntervalData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", pollingIntervalData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminRetrievePollingInterval", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return pollingIntervalData;
    }

    @Override
    public String adminUpdateCities(String adminTicketId, String requestId, List<CityInfo> citiesList, boolean deleteAllRows) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deleteAllRows", String.valueOf(deleteAllRows)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateCities", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminUpdateCitiesAction.execute(adminTicketId, requestId, citiesList, deleteAllRows);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "adminUpdateCities", requestId, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdateCities", requestId, "closing", "");

        return response;
    }
    
    @Override
    public AdminUserNotVerifiedDeleteResult adminUserNotVerifiedDelete(String adminTicketId, String requestId, Long userId, Date creationStart, Date creationEnd) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("userId", this.toString(userId)));
        inputParameters.add(new Pair<String, String>("creationStart", this.toString(creationStart)));
        inputParameters.add(new Pair<String, String>("creationEnd", this.toString(creationEnd)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteUser", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AdminUserNotVerifiedDeleteResult adminUserNotVerifiedDeleteResult = null;
        try {
            adminUserNotVerifiedDeleteResult = adminUserNotVerifiedDeleteAction.execute(adminTicketId, requestId, userId, creationStart, creationEnd);
        }
        catch (EJBException ex) {
            adminUserNotVerifiedDeleteResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", adminUserNotVerifiedDeleteResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminDeleteUser", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return adminUserNotVerifiedDeleteResult;
    }
    
    @Override
    public UpdatePvActivationFlagResult adminUpdatePvActivationFlag(String ticketId, String requestID, List<StationActivationData> stationActivationList) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestID));

        int count = 0;
        String stationActivationListString = "{";
        for (StationActivationData stationActivation : stationActivationList) {
            if (count != 0) {
                stationActivationListString += ",";
            }
            stationActivationListString += " \"stationID\": \"" + stationActivation.getStationID() + "\"," +
                                           " \"status\": \"" + stationActivation.getStatus() + "\"," +
                                           " \"newAcquirerActive\": \"" + stationActivation.getNewAcquirerActive() + "\"," +
                                           " \"refuelingActive\": \"" + stationActivation.getRefuelingActive() + "\"," +
                                           " \"loyaltyActive\": \"" + stationActivation.getLoyaltyActive() + "\",";
            count++;
        }
        stationActivationListString += "}";

        inputParameters.add(new Pair<String, String>("stationListString", stationActivationListString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePvActivationFlag", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        UpdatePvActivationFlagResult updatePvActivationFlagResult = new UpdatePvActivationFlagResult();

        try {
            updatePvActivationFlagResult = adminUpdatePvActivationFlagAction.execute(ticketId, requestID, stationActivationList, this.forecourtInfoService);
        }
        catch (Exception ex) {
            updatePvActivationFlagResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return updatePvActivationFlagResult;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", updatePvActivationFlagResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminUpdatePvActivationFlag", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return updatePvActivationFlagResult;
    }
    
    @Override
    public List<AdminRemoteSystemCheckResult> systemCheck(String adminTicketId, String requestId, List<RemoteSystem> remoteSystemList) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        
        String remoteSystems = "{ ";
        int index = 1;
        
        if (remoteSystemList != null) {
            for (RemoteSystem remoteSystem : remoteSystemList) {
                if (remoteSystem != null && remoteSystem.getSystemId() != null) {
                    remoteSystems += "systemId: " + remoteSystem.getSystemId();
                }
                index++;
                
                if (index < remoteSystemList.size()) {
                    remoteSystems += "; ";
                }
            }
        }
        
        remoteSystems += " }";
        
        inputParameters.add(new Pair<String, String>("remoteSystem", remoteSystems));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "systemCheck", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        List<AdminRemoteSystemCheckResult> resultList = new ArrayList<>();

        try {
            CRMAdapterServiceRemote crmAdapterService = EJBHomeCache.getInstance().getCRMAdapterService();
            DWHAdapterServiceRemote dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();
            ParkingServiceRemote parkingService = EJBHomeCache.getInstance().getParkingService();
            RefuelingNotificationServiceRemote refuelingNotificationService = EJBHomeCache.getInstance().getRefuelingNotificationService();
            CRMServiceRemote crmService = EJBHomeCache.getInstance().getCRMService();
            resultList = adminRemoteSystemCheckAction.execute(adminTicketId, requestId, remoteSystemList, forecourtInfoService, crmAdapterService, fidelityService, dwhAdapterService, 
                    refuelingNotificationService, parkingService, crmService);
        }
        catch (InterfaceNotFoundException ex) {
            System.err.println("Errore nella creazione dell'istanza del servizio di verifica impianto: " + ex.getMessage());
            AdminRemoteSystemCheckResult result = new AdminRemoteSystemCheckResult();
            result.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            result.setStatusMessage("Errore nella creazione dell'istanza del servizio di verifica impianto: " + ex.getMessage());
            resultList.add(result);
        }
        catch (EJBException ex) {
            System.err.println("Errore nell'esecuzione del servizio: " + ex.getMessage());
            AdminRemoteSystemCheckResult result = new AdminRemoteSystemCheckResult();
            result.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            result.setStatusMessage("Errore nell'esecuzione del servizio: " + ex.getMessage());
            resultList.add(result);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        
        
        String results = "{ ";
        index = 1;
        
        for (AdminRemoteSystemCheckResult result : resultList) {
            results += "{"
                    + "systemId: " + toString(result.getSystemId())
                    + "; statusCode: " + result.getStatusCode()
                    + "; statusMessage: " + toString(result.getStatusMessage())
                    + "}";
            
            if (index < resultList.size()) {
                results += " , ";
            }
            
            index++;
        }
        
        outputParameters.add(new Pair<String, String>("resultList", results));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "systemCheck", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        
        return resultList;
    }

    @Override
    public CountPendingTransactionsResult adminCountPendingTransactions(String adminTicketId, String requestId, String serverName ) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("serverName", serverName));
        
       
        
       this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCountPendingTransactions", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        CountPendingTransactionsResult countPendingTransactionsResult = new CountPendingTransactionsResult();

        try {
            countPendingTransactionsResult = adminCountPendingTransactionsAction.execute(adminTicketId, requestId, serverName);
        }
       
        catch (EJBException ex) {
            System.err.println("Errore nell'esecuzione del servizio: " + ex.getMessage());
            countPendingTransactionsResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
             return countPendingTransactionsResult;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        
        
       
        
        outputParameters.add(new Pair<String, String>("statusCode",countPendingTransactionsResult.getStatusCode()));
        if(countPendingTransactionsResult.getCount()!=null)  {
            outputParameters.add(new Pair<String, String>("count",countPendingTransactionsResult.getCount().toString())); 
        }
        else {
            outputParameters.add(new Pair<String, String>("count","null")); 
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminCountPendingTransactions", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        
        return countPendingTransactionsResult;
    }

    
    private String doubleToString(Double input) {
        if (input != null) {
            return input.toString();
        }
        else {
            return "";
        }
    }

    private String integerToString(Integer input) {
        if (input != null) {
            return input.toString();
        }
        else {
            return "";
        }
    }

    private String timestampToString(Timestamp input) {
        if (input != null) {
            return input.toString();
        }
        else {
            return "";
        }
    }

    private String toString(Object input) {
        if (input != null) {
            return input.toString();
        }
        else {
            return "";
        }
    }

    @Override
    public String adminPaymentMethodStatusUpdate(String adminTicketId, String requestId, Long id, String type, Integer status, String message) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("adminTicketId", adminTicketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", id.toString()));
        inputParameters.add(new Pair<String, String>("type", type));
        inputParameters.add(new Pair<String, String>("status", status.toString()));
        inputParameters.add(new Pair<String, String>("message", message));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminPaymentMethodStatusUpdate", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = adminPaymentMethodStatusUpdateAction.execute(adminTicketId, requestId, id, type, status, message);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "adminPaymentMethodStatusUpdate", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

}
