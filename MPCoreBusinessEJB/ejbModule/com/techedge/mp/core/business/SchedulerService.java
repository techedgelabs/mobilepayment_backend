package com.techedge.mp.core.business;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timer;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.actions.scheduler.SchedulerJobClosePendingParkingTransactionAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobCreateVoucherEsPromotionAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobDailyConsumeVoucherReportAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobDailyStatisticsReportAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobDailyStatisticsReportBusinessAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobFinalizePendingTransactionAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobPrepareDailyStatisticsReportAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobPrepareDailyStatisticsReportBusinessAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobRemoveMobilePhoneNotVerifiedAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobRemoveOldPaymentAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobRemoveOnHoldTransactionAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobSendingDailyStatisticsReportAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobSendingDailyStatisticsReportBusinessAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobSessionCleanerAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobSmsPendingRetryAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobStatisticsMyCiceroRefuelingReportAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobUpdateCrmPromotionsDataAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobUpdateCrmSfPromotionsDataAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobUpdateDWHBurnDataAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobUpdateDWHEarnDataAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobUserNewClearAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobUserNewResendEmailAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobVoucherSendReportAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobWeeklyStatisticsReportAction;
import com.techedge.mp.core.actions.scheduler.SchedulerJobWeeklyStatisticsReportBusinessAction;
import com.techedge.mp.core.actions.scheduler.SchedulerProcessCrmOutboundInterfaceAction;
import com.techedge.mp.core.actions.scheduler.SchedulerProcessCrmSfOutboundInterfaceAction;
import com.techedge.mp.core.actions.scheduler.SchedulerProcessMailingListAction;
import com.techedge.mp.core.actions.scheduler.SchedulerProcessResultClusterCrmOutboundInterfaceAction;
import com.techedge.mp.core.actions.scheduler.SchedulerProcessResultCrmOutboundInterfaceAction;
import com.techedge.mp.core.actions.scheduler.SchedulerProcessResultCrmSfOutboundInterfaceAction;
import com.techedge.mp.core.actions.scheduler.SchedulerProcessSubscribeCrmOutboundInterfaceAction;
import com.techedge.mp.core.actions.scheduler.SchedulerPromotionAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.CrmOutboundProcessResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.GetOffersResult;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.Response;
import com.techedge.mp.core.business.interfaces.crm.StatusCode;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.SFTPService;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.DWHAdapterResult;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

/**
 * Session Bean implementation class SchedulerService
 */
@Startup
@Singleton
@LocalBean
//@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class SchedulerService implements SchedulerServiceLocal, SchedulerServiceRemote {

    private static final String                              PARAM_USER_CREATION_TIME_LIMIT                   = "USER_CREATION_TIME_LIMIT";
    private static final String                              PARAM_USER_RESEND_EMAIL_TIME_LIMIT               = "USER_RESEND_EMAIL_TIME_LIMIT";
    private static final String                              PARAM_EMAIL_SENDER_ADDRESS                       = "EMAIL_SENDER_ADDRESS";
    private static final String                              PARAM_SMTP_HOST                                  = "SMTP_HOST";
    private static final String                              PARAM_SMTP_PORT                                  = "SMTP_PORT";
    private static final String                              PARAM_ACTIVATION_LINK                            = "ACTIVATION_LINK";
    private static final String                              PARAM_JOB_SCHEDULER_INSTANCE                     = "JOB_SCHEDULER_INSTANCE";
    private static final String                              PARAM_SERVER_NAME                                = "SERVER_NAME";
    private final static String                              PARAM_PROXY_HOST                                 = "PROXY_HOST";
    private final static String                              PARAM_PROXY_PORT                                 = "PROXY_PORT";
    private final static String                              PARAM_PROXY_NO_HOSTS                             = "PROXY_NO_HOSTS";
    private final static String                              PARAM_PENDING_TRANSACTION_REPORT_RECIPIENT       = "PENDING_TRANSACTION_REPORT_RECIPIENT";
    private final static String                              PARAM_DAILY_STATISTICS_REPORT_RECIPIENT          = "DAILY_STATISTICS_REPORT_RECIPIENT";
    private final static String                              PARAM_DAILY_STATISTICS_REPORT_NEW_RECIPIENT      = "DAILY_STATISTICS_REPORT_NEW_RECIPIENT";
    private final static String                              PARAM_DAILY_STATISTICS_REPORT_RECIPIENT_BCC      = "DAILY_STATISTICS_REPORT_RECIPIENT_BCC";
    private final static String                              PARAM_SERVICE_RECIPIENT      			      	  = "SERVICE_RECIPIENT";
    private final static String                              PARAM_DAILY_STATISTICS_REPORT_LOGO_URL           = "DAILY_STATISTICS_REPORT_LOGO_URL";
    private final static String                              PARAM_DAILY_CONSUME_VOUCHER_REPORT_RECIPIENT     = "DAILY_CONSUME_VOUCHER_REPORT_RECIPIENT";
    private final static String                              PARAM_DAILY_CONSUME_VOUCHER_REPORT_RECIPIENT_BCC = "DAILY_CONSUME_VOUCHER_REPORT_RECIPIENT_BCC";
    private final static String                              PARAM_PROMO_EMAIL_INTERVAL                       = "PROMO_EMAIL_INTERVAL";
    private final static String                              PARAM_PROMO_LANDING_URL                          = "PROMO_LANDING_URL";
    private final static String                              PARAM_TIMEOUT_ONHOLD_TRANSACTION                 = "TIMEOUT_ONHOLD_TRANSACTION";
    private final static String                              PARAM_TIMEOUT_PAYMENT_METHOD_VERIFICATION        = "TIMEOUT_PAYMENT_METHOD_VERIFICATION";
    private final static String                              PARAM_SHOPLOGIN_CHECK                            = "SHOPLOGIN_CHECK";
    private final static String                              PARAM_APIKEY_CARTASI                             = "APIKEY_CARTASI";
    private final static String                              PARAM_ENCODED_SECRET_KEY_CARTASI                 = "ENCODED_SECRET_KEY_CARTASI";
    private final static String                              PARAM_CARTASI_VAULT_BLOCK                        = "CARTASI_VAULT_BLOCK";
    private final static String                              PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY              = "CARTASI_VAULT_BLOCK_ATTRIBUTE_KEY";
    private final static String                              PARAM_SMS_PENDING_TIMEOUT                        = "SMS_PENDING_TIMEOUT";
    private final static String                              PARAM_REFUELING_CHECK_CONNECTION_RECIPIENT       = "REFUELING_CHECKING_RECIPIENT";
    private final static String                              PARAM_FIDELITY_CHECK_CONNECTION_RECIPIENT        = "FIDELITY_CHECKING_RECIPIENT";
    private final static String                              PARAM_FORECOURT_CHECK_CONNECTION_RECIPIENT       = "FORECOURT_CHECKING_RECIPIENT";
    private final static String                              PARAM_MAILING_LIST_SUBJECT                       = "MAILING_LIST_SUBJECT";
    private final static String                              PARAM_MAILING_LIST_PACKET_SIZE                   = "MAILING_LIST_PACKET_SIZE";
    private final static String                              PARAM_CRM_FTPS_HOST                              = "CRM_FTPS_HOST";
    private final static String                              PARAM_CRM_FTPS_PORT                              = "CRM_FTPS_PORT";
    private final static String                              PARAM_CRM_FTPS_USERNAME                          = "CRM_FTPS_USERNAME";
    private final static String                              PARAM_CRM_FTPS_PASSWORD                          = "CRM_FTPS_PASSWORD";
    private final static String                              PARAM_CRM_FTPS_READ_PATH                         = "CRM_FTPS_READ_PATH";
    private final static String                              PARAM_CRM_FTPS_ARCHIVE_PATH                      = "CRM_FTPS_ARCHIVE_PATH";
    private final static String                              PARAM_CRM_FTPS_RESULT_PATH                       = "CRM_FTPS_RESULT_PATH";
    private final static String                              PARAM_CRM_FTPS_PROXY_HOST                        = "CRM_FTPS_PROXY_HOST";
    private final static String                              PARAM_CRM_FTPS_PROXY_PORT                        = "CRM_FTPS_PROXY_PORT";
    private final static String                              PARAM_DWH_CHECK_CONNECTION_RECIPIENT             = "DWH_CHECKING_RECIPIENT";
    private final static String                              PARAM_CRM_CHECK_CONNECTION_RECIPIENT             = "CRM_CHECKING_RECIPIENT";
    private final static String                              PARAM_STRING_SUBSTITUTION_PATTERN                = "STRING_SUBSTITUTION_PATTERN";
    private final static String                              PARAM_TIMEOUT_PENDING_MOBILE_PHONE               = "TIMEOUT_PENDING_MOBILE_PHONE";
    private final static String                              PARAM_CRM_MAX_USERS_PROCESSED                    = "CRM_MAX_USERS_PROCESSED";
    private final static String                              PARAM_PUSH_NOTIFICATION_EXPIRY_TIME              = "PUSH_NOTIFICATION_MAX_EXPIRY_TIME";
    private final static String                              PARAM_RECONCILIATION_MAX_ATTEMPTS                = "RECONCILIATION_MAX_ATTEMPTS";
    private final static String                              PARAM_CRM_SUMMARY_RECIPIENT                      = "CRM_SUMMARY_RECIPIENT";
    private final static String                              PARAM_CRM_PROCESS_RESULT_DELAY                   = "CRM_PROCESS_RESULT_DELAY";
    private final static String                              PARAM_WEEKLY_STATISTICS_REPORT_RECIPIENT         = "WEEKLY_STATISTICS_REPORT_RECIPIENT";
    private final static String                              PARAM_WEEKLY_STATISTICS_REPORT_BUSINESS_RECIPIENT = "WEEKLY_STATISTICS_REPORT_BUSINESS_RECIPIENT";
    private final static String                              PARAM_CRM_INTERACTION_POINT_HPC                  = "CRM_INTERACTION_POINT_HPC";
    private final static String                              PARAM_MAX_INTERVAL_PENDING_PARKING_TRANSACTION   = "MAX_INTERVAL_PENDING_PARKING_TRANSACTION";
    private static final String                              PARAM_STATISTICS_REPORT_MYCICERO_REFUELING_RECIPIENT       = "STATISTICS_REPORT_MYCICERO_REFUELING_RECIPIENT";
    private final static String                              PARAM_DAILY_STATISTICS_REPORT_BUSINESS_LOGO_URL  = "DAILY_STATISTICS_REPORT_BUSINESS_LOGO_URL";
    private final static String                              PARAM_DAILY_STATISTICS_REPORT_BUSINESS_RECIPIENT = "DAILY_STATISTICS_REPORT_BUSINESS_RECIPIENT";
    private final static String                              PARAM_DAILY_STATISTICS_REPORT_BUSINESS_NEW_RECIPIENT = "DAILY_STATISTICS_REPORT_BUSINESS_NEW_RECIPIENT";
    private final static String                              PARAM_CRM_PROCESS_CHECK_EMPTY_TABLE              = "CRM_PROCESS_CHECK_EMPTY_TABLE";
    private final static String                              PARAM_CRM_PROCESS_MAX_DATA_PER_THREAD            = "CRM_PROCESS_MAX_DATA_PER_THREAD";
    private final static String                              PARAM_CRM_PROCESS_MAX_THREADS                    = "CRM_PROCESS_MAX_THREADS";
    private final static String                              PARAM_CRM_AWS_ARN_TOPIC                          = "CRM_AWS_ARN_TOPIC";
    private final static String                              PARAM_UPDATE_STATION_LIST_SERVICE_ACTIVE         = "UPDATE_STATION_LIST_SERVICE_ACTIVE";
    private final static String                              PARAM_PROMOTION_ES_PROMO_CODE_VODA               = "PROMOTION_ES_PROMO_CODE_VODA";
    private final static String                              PARAM_PROMOTION_ES_VODA_AMOUNT         		  = "PROMOTION_ES_VODA_AMOUNT";
    private final static String                              PARAM_PROMOTION_ES_VODA_SILL         		  	  = "PROMOTION_ES_VODA_SILL";
    private final static String                              PARAM_LANDING_WELCOME_VODAFONE                   = "LANDING_WELCOME_VODAFONE";
    private final static String                              PARAM_LANDING_WINNER_VODAFONE                    = "LANDING_WINNER_VODAFONE";

    @Resource
    private EJBContext                                       context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager                                    em;

    @Resource
    private SessionContext                                   sessionContext;

    @EJB
    private SchedulerJobUserNewClearAction                   schedulerJobUserNewClearAction;

    @EJB
    private SchedulerJobUserNewResendEmailAction             schedulerJobUserNewResendEmailAction;

    @EJB
    private SchedulerJobVoucherSendReportAction              schedulerJobVoucherSendReportAction;

    @EJB
    private SchedulerJobFinalizePendingTransactionAction     schedulerJobFinalizePendingTransactionAction;

    @EJB
    private SchedulerJobDailyStatisticsReportAction          schedulerJobDailyStatisticsReportAction;
    
    @EJB
    private SchedulerJobPrepareDailyStatisticsReportAction   schedulerJobPrepareDailyStatisticsReportAction;

    @EJB
    private SchedulerPromotionAction                         schedulerPromotionAction;

    @EJB
    private SchedulerJobRemoveOnHoldTransactionAction        schedulerJobRemoveOnHoldTransactionAction;

    @EJB
    private SchedulerJobRemoveOldPaymentAction               schedulerJobRemoveOldPaymentAction;

    @EJB
    private SchedulerJobDailyConsumeVoucherReportAction      schedulerJobDailyConsumeVoucherReportAction;

    @EJB
    private SchedulerJobSmsPendingRetryAction                schedulerJobSmsPendingRetryAction;

    @EJB
    private SchedulerProcessMailingListAction                schedulerProcessMailingListAction;

    @EJB
    private SchedulerProcessCrmOutboundInterfaceAction       schedulerProcessCrmOutboundInterfaceAction;
    
    @EJB
    private SchedulerProcessCrmSfOutboundInterfaceAction       schedulerProcessCrmSfOutboundInterfaceAction;

    @EJB
    private SchedulerProcessResultCrmOutboundInterfaceAction schedulerProcessResultCrmOutboundInterfaceAction;
    
    @EJB
    private SchedulerProcessResultCrmSfOutboundInterfaceAction schedulerProcessResultCrmSfOutboundInterfaceAction;

    @EJB
    private SchedulerProcessResultClusterCrmOutboundInterfaceAction schedulerProcessResultClusterCrmOutboundInterfaceAction;

    @EJB
    private SchedulerProcessSubscribeCrmOutboundInterfaceAction schedulerProcessSubscribeCrmOutboundInterfaceAction;
    
    @EJB
    private SchedulerJobRemoveMobilePhoneNotVerifiedAction   schedulerProcessRemovePhoneNumberNotVerifiedAction;

    @EJB
    private SchedulerJobWeeklyStatisticsReportAction         schedulerJobWeeklyStatisticsReportAction;
    
    @EJB
    private SchedulerJobWeeklyStatisticsReportBusinessAction schedulerJobWeeklyStatisticsReportBusinessAction;

	@EJB
    private SchedulerJobSessionCleanerAction                 schedulerJobSessionCleanerAction;
    
    @EJB
    private SchedulerJobUpdateDWHEarnDataAction               schedulerJobUpdateDWHEarnDataAction;

    @EJB
    private SchedulerJobUpdateDWHBurnDataAction              schedulerJobUpdateDWHBurnDataAction;
    
    @EJB
    private SchedulerJobUpdateCrmPromotionsDataAction        schedulerJobUpdateCrmPromotionsDataAction;
    
    @EJB
    private SchedulerJobUpdateCrmSfPromotionsDataAction        schedulerJobUpdateCrmSfPromotionsDataAction;
    
    @EJB
    private SchedulerJobClosePendingParkingTransactionAction schedulerJobClosePendingParkingTransactionAction;
    
    @EJB
    private SchedulerJobStatisticsMyCiceroRefuelingReportAction schedulerJobStatisticsMyCiceroRefuelingReportAction;
    
    @EJB
    private SchedulerJobDailyStatisticsReportBusinessAction     schedulerJobDailyStatisticsReportBusinessAction;
    
    @EJB
    private SchedulerJobPrepareDailyStatisticsReportBusinessAction     schedulerJobPrepareDailyStatisticsReportBusinessAction;
    
    @EJB
    private SchedulerJobSendingDailyStatisticsReportBusinessAction     schedulerJobSendingDailyStatisticsReportBusinessAction;
    
    @EJB
    private SchedulerJobSendingDailyStatisticsReportAction     schedulerJobSendingDailyStatisticsReportAction;
    
    @EJB
    private SchedulerJobCreateVoucherEsPromotionAction   schedulerJobCreateVoucherEsPromotionAction;

    
    //private LoggerService                        loggerService                      = null;
    private ParametersService                                parametersService                                = null;
    private EmailSenderRemote                                emailSender                                      = null;
    private ReconciliationService                            reconciliationService                            = null;
    private TransactionService                               transactionService                               = null;
    private GPServiceRemote                                  gpService                                        = null;
    private UserCategoryService                              userCategoryService                              = null;
    private CRMServiceRemote                                 crmService                                       = null;
    private CRMAdapterServiceRemote                          crmAdapterService                                = null;
    private DWHAdapterServiceRemote                          dwhAdapterService                                = null;
    private ParkingTransactionV2Service                      parkingTransactionV2Service                      = null;
    private StationServiceRemote                             stationService                                   = null;

    private String                                           secretKey                                        = null;
    private StringSubstitution                               stringSubstitution                               = null;

    public SchedulerService() {
        /*
         * try {
         * this.loggerService = EJBHomeCache.getInstance().getLoggerService();
         * }
         * catch (InterfaceNotFoundException e) {
         * 
         * throw new EJBException(e);
         * }
         */
        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            reconciliationService = EJBHomeCache.getInstance().getReconciliationService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            transactionService = EJBHomeCache.getInstance().getTransactionService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            gpService = EJBHomeCache.getInstance().getGpService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            crmService = EJBHomeCache.getInstance().getCRMService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            crmAdapterService = EJBHomeCache.getInstance().getCRMAdapterService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        
        /*
        try {
            parkingTransactionV2Service = EJBHomeCache.getInstance().getParkingTransactionV2Service();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        */

        String vaultBlock = "";
        String vaultBlockAttributeCryptKey = "";
        String pattern = null;

        try {

            vaultBlock = parametersService.getParamValue(SchedulerService.PARAM_CARTASI_VAULT_BLOCK);
            vaultBlockAttributeCryptKey = parametersService.getParamValue(SchedulerService.PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY);
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        stringSubstitution = new StringSubstitution(pattern);
        
        
        try {

            String interactionPointHpc = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_INTERACTION_POINT_HPC);
            
            InteractionPointType.HPC.setPoint(interactionPointHpc);
        }
        catch (ParameterNotFoundException ex) {
            System.err.println("Errore nella lettura dei parametri: " + ex.getMessage());
            //throw new EJBException(ex);
        }
    }

    @PostConstruct
    public void initialize() {}

    @Schedule(hour = "*", minute = "34", persistent = false)
    // ogni ora al trentaduesimo minuto
    public void jobUserNewClear() {

        System.out.println("start jobUserNewClear");

        String response = null;

        try {
            Integer userCreationTimeLimit = Integer.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_USER_CREATION_TIME_LIMIT));
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {

                response = schedulerJobUserNewClearAction.execute(userCreationTimeLimit);
            }
        }
        catch (ParameterNotFoundException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }

        System.out.println("response: " + response);
    }

    // @Schedule(hour = "*", minute = "*", second = "20/60", persistent = false)
    // example: Every hour 
    public void jobUserNewResendEmail() {

        System.out.println("start jobUserNewResendEmail");

        String response = null;

        try {
            Integer userResendEmailTimeLimit = Integer.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_USER_RESEND_EMAIL_TIME_LIMIT));
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            String activationLink = this.parametersService.getParamValue(SchedulerService.PARAM_ACTIVATION_LINK);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = schedulerJobUserNewResendEmailAction.execute(userResendEmailTimeLimit, activationLink, this.emailSender, userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }

        System.out.println("response: " + response);
    }

    @Schedule(hour = "*", minute = "8", persistent = false)
    // ogni ora all'ottavo minuto
    public void jobReconciliateTransaction() {

        System.out.println("start jobReconciliateTransaction");

        try {
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {

                reconciliationService.reconciliationTransaction(null);
            }
        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobReconciliateTransaction");
    }

    @Schedule(hour = "*", minute = "9", persistent = false)
    // ogni ora al nono minuto
    public void jobReconciliateUser() {

        System.out.println("start jobReconciliateUser");

        try {
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {

                reconciliationService.reconciliationUser(null);
            }
        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobReconciliateUser");
    }
    
    //@Schedule(hour = "*", minute = "9", persistent = false)
    // ogni ora al x minuto
    public void jobReconciliateParking() {

        System.out.println("start jobReconciliateParking");

        try {
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {

                reconciliationService.reconciliationParking(null);
            }
        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobReconciliateParking");
    }

    @Schedule(hour = "*", minute = "*/10", persistent = false)
    // Ogni 10 minuti
    public void jobFinalizePendingTransaction() {

        System.out.println("start jobFinalizePendingTransaction");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_PENDING_TRANSACTION_REPORT_RECIPIENT);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance) && reportRecipient != null) {

                this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                schedulerJobFinalizePendingTransactionAction.execute(transactionService, emailSender, reportRecipient);
            }
        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobFinalizePendingTransaction");
    }

    //@Schedule(hour = "6", minute = "0", persistent = false)
    // Ogni giorno alle ore 06.00
    public void jobDailyStatisticsReport() {

        System.out.println("start jobDailyStatisticsReport");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_RECIPIENT);
            final String reportRecipientBCC = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_RECIPIENT_BCC);
            final String reportLogoUrl = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_LOGO_URL);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerJobDailyStatisticsReportAction.execute(emailSender, reportRecipient, reportRecipientBCC, reportLogoUrl, proxyHost, proxyPort, proxyNoHosts);
                    }
                }, "jobDailyStatisticsReport").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobDailyStatisticsReport");
    }
    
    @Schedule(hour = "5", minute = "20", persistent = false)
    //@Schedule(hour = "*", minute = "*/1", persistent = false)
    // Ogni giorno alle ore 01.00
    public void jobPrepareDailyStatisticsReport() {

        System.out.println("start jobPrepareDailyStatisticsReport");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_NEW_RECIPIENT);
            final String reportRecipientService = this.parametersService.getParamValue(SchedulerService.PARAM_SERVICE_RECIPIENT);
            final String reportLogoUrl = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_LOGO_URL);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerJobPrepareDailyStatisticsReportAction.execute(emailSender, reportRecipient, reportRecipientService, reportLogoUrl, proxyHost, proxyPort, proxyNoHosts);
                    }
                }, "jobPrepareDailyStatisticsReport").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobPrepareDailyStatisticsReport");
    }
    
    @Schedule(hour = "6", minute = "0", persistent = false)
    //@Schedule(hour = "*", minute = "*/1", persistent = false)
    // Ogni giorno alle ore 06.00
    public void jobSendingDailyStatisticsReport() {

        System.out.println("start jobSendingDailyStatisticsReport");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_NEW_RECIPIENT);
            final String reportRecipientService = this.parametersService.getParamValue(SchedulerService.PARAM_SERVICE_RECIPIENT);
            final String reportLogoUrl = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_LOGO_URL);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerJobSendingDailyStatisticsReportAction.execute(emailSender, reportRecipient, reportRecipientService, reportLogoUrl, proxyHost, proxyPort, proxyNoHosts);
                    }
                }, "jobSendingDailyStatisticsReport").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobSendingDailyStatisticsReport");
    }


    //@Schedule(hour = "6", minute = "15", persistent = false)
    // Ogni giorno alle ore 06.15
    public void jobDailyStatisticsReportBusiness() {

        System.out.println("start jobDailyStatisticsReportBusiness");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_BUSINESS_RECIPIENT);
            final String reportLogoUrl = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_BUSINESS_LOGO_URL);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerJobDailyStatisticsReportBusinessAction.execute(emailSender, reportRecipient, reportLogoUrl, proxyHost, proxyPort, proxyNoHosts);
                    }
                }, "jobDailyStatisticsReportBusiness").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobDailyStatisticsReport");
    }
    
    @Schedule(hour = "5", minute = "40", persistent = false)
    //@Schedule(hour = "*", minute = "*/1", persistent = false)
    //Ogni giorno alle ore 01:30
    public void jobPrepareDailyStatisticsReportBusiness() {

        System.out.println("start jobPrepareDailyStatisticsReportBusiness");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_BUSINESS_NEW_RECIPIENT);
            final String reportRecipientService = this.parametersService.getParamValue(SchedulerService.PARAM_SERVICE_RECIPIENT);
            final String reportLogoUrl = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_BUSINESS_LOGO_URL);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerJobPrepareDailyStatisticsReportBusinessAction.execute(emailSender, reportRecipient,reportRecipientService, reportLogoUrl, proxyHost, proxyPort, proxyNoHosts);
                    }
                }, "jobPrepareDailyStatisticsReportBusiness").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobPrepareDailyStatisticsReportBusiness");
    }
    
    @Schedule(hour = "6", minute = "15", persistent = false)
    //@Schedule(hour = "*", minute = "*/1", persistent = false)
    //Ogni giorno alle ore 06:15
    public void jobSendingDailyStatisticsReportBusiness() {

        System.out.println("start jobSendingDailyStatisticsReportBusiness");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_BUSINESS_NEW_RECIPIENT);
            final String reportRecipientService = this.parametersService.getParamValue(SchedulerService.PARAM_SERVICE_RECIPIENT);
            final String reportLogoUrl = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_STATISTICS_REPORT_BUSINESS_LOGO_URL);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                    	schedulerJobSendingDailyStatisticsReportBusinessAction.execute(emailSender, reportRecipient,reportRecipientService, reportLogoUrl, proxyHost, proxyPort, proxyNoHosts);
                    }
                }, "jobSendingDailyStatisticsReportBusiness").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobSendingDailyStatisticsReportBusiness");
    }

    @Schedule(hour = "9", minute = "1", persistent = false)
    // Ogni giorno alle ore 09.00
    public void jobDailyConsumeVoucherReport() {

        System.out.println("start jobDailyConsumeVoucherReport");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_CONSUME_VOUCHER_REPORT_RECIPIENT);
            final String reportRecipientBCC = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_CONSUME_VOUCHER_REPORT_RECIPIENT_BCC);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerJobDailyConsumeVoucherReportAction.execute(emailSender, reportRecipient, reportRecipientBCC, proxyHost, proxyPort, proxyNoHosts);
                    }
                }, "jobDailyConsumeVoucherReport").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobDailyConsumeVoucherReport");
    }

    public void listSchedulers() {
        Collection<Timer> allTimers = context.getTimerService().getTimers();

        for (Timer timer : allTimers) {
            System.out.println("SYSTEM TIMER : " + timer.toString());
            System.out.println("SYSTEM TIMER SCHEDULE: " + timer.getSchedule().toString());
            System.out.println("SYSTEM TIMER INFO: " + timer.getInfo());
        }
    }

    public void cancelSchedulers() {
        Collection<Timer> allTimers = sessionContext.getTimerService().getTimers();

        for (Timer timer : allTimers) {
            System.out.println("SYSTEM TIMER : " + timer.toString());

            try {
                timer.cancel();
                System.out.println("SYSTEM TIMER CANCELLED");
            }
            catch (Exception ex) {
                System.out.println("SYSTEM TIMER NOT AVAILABLE");
            }

            System.out.println("SYSTEM TIMER SCHEDULE: " + timer.getSchedule().toString());
        }

    }

    //@Schedule(hour = "*", minute = "*/5", persistent = false)
    // every 5 minutes
    public void jobPromotion() {

        System.out.println("start jobPromotion");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);
            Integer promoEmailInterval = new Integer(parametersService.getParamValue(SchedulerService.PARAM_PROMO_EMAIL_INTERVAL));
            String promoLandingUrl = parametersService.getParamValue(SchedulerService.PARAM_PROMO_LANDING_URL);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

                // Calcolo del periodo di ricerca

                // La fine del periodo di ricerca � pari alla mezzanotte del giorno attuale
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.SECOND, (promoEmailInterval.intValue() * -1));

                schedulerPromotionAction.execute(emailSender, calendar.getTime(), promoLandingUrl);
            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobPromotion");

    }

    @Schedule(hour = "*", minute = "3", persistent = false)
    // ogni ora al terzo minuto
    public void jobRemoveOnHoldTransaction() {

        System.out.println("start jobRemoveOnHoldTransaction");

        try {

            Long timeoutOnHoldTransaction = Long.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_TIMEOUT_ONHOLD_TRANSACTION));

            Date now = new Date();
            Date dateCheck = new Date(now.getTime() - timeoutOnHoldTransaction);

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                schedulerJobRemoveOnHoldTransactionAction.execute(dateCheck);
            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobRemoveOnHoldTransaction");
    }

    @Schedule(hour = "11", minute = "1", persistent = false)
    // Ogni giorno ore 11
    public void jobRemoveOldPayment() {

        System.out.println("start jobRemoveOldPayment");

        try {

            String shopLogin = this.parametersService.getParamValue(SchedulerService.PARAM_SHOPLOGIN_CHECK);
            String shopLoginNewAcquirer = this.parametersService.getParamValue(SchedulerService.PARAM_APIKEY_CARTASI);
            Long timeoutPaymentMethodVerification = Long.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_TIMEOUT_PAYMENT_METHOD_VERIFICATION));

            // Decodifica stringa acquirer
            String encodedSecretKey = this.parametersService.getParamValue(SchedulerService.PARAM_ENCODED_SECRET_KEY_CARTASI);
            String decodedSecretKey = "";

            if (encodedSecretKey != null && !encodedSecretKey.equals("")) {

                EncryptionAES encryptionAES = new EncryptionAES();
                encryptionAES.loadSecretKey(this.secretKey);
                decodedSecretKey = encryptionAES.decrypt(encodedSecretKey);
            }

            Date now = new Date();
            Date dayToCompare = new Date(now.getTime() - timeoutPaymentMethodVerification);

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                schedulerJobRemoveOldPaymentAction.execute(shopLogin, shopLoginNewAcquirer, dayToCompare, decodedSecretKey, gpService, userCategoryService);
            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }
        catch (Exception ex) {
            ex.printStackTrace();

            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobRemoveOnHoldTransaction");
    }

    //@Schedule(hour = "*", minute = "*/1", persistent = false)
    //
    public void jobArchiveTransaction() {

        System.out.println("start jobArchiveTransaction");
        /*
        try {

            String shopLogin = this.parametersService.getParamValue(SchedulerService.PARAM_SHOPLOGIN_CHECK);
            Long timeoutPaymentMethodVerification = Long.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_TIMEOUT_PAYMENT_METHOD_VERIFICATION));

            Date now = new Date();
            Date dayToCompare = new Date(now.getTime() - timeoutPaymentMethodVerification);

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                schedulerJobRemoveOldPaymentAction.execute(shopLogin, dayToCompare, gpService);
            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }
         */
        System.out.println("stop jobArchiveTransaction");
    }

    @Schedule(hour = "*", minute = "6", persistent = false)
    // ogni ora al sesto minuto
    public void jobSmsPendingRetry() {

        System.out.println("start jobSmsPendingRetry");
        SmsServiceRemote smsService = null;

        try {
            smsService = EJBHomeCache.getInstance().getSmsService();
        }
        catch (InterfaceNotFoundException ex) {
            System.err.println("Errore nella creazione dell'istanza del servizio di sms: " + ex.getMessage());
            System.out.println("stop jobSmsPendingRetry");
            return;
        }

        try {

            Long pendingTimeout = Long.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_SMS_PENDING_TIMEOUT));

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                schedulerJobSmsPendingRetryAction.execute(pendingTimeout, smsService, userCategoryService, stringSubstitution);
            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobSmsPendingRetry");
    }

    @Schedule(hour = "*", minute = "4", persistent = false)
    // ogni ora al quarto minuto
    public void jobRefuelingCheckConnection() {
        System.out.println("start jobRefuelingCheckConnection");

        Boolean start = false;
        //RefuelingNotificationServiceRemote refuelingNotificationService = null;
        RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2 = null;
        String response;
        String emailRecipient = null;

        try {

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            emailRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_REFUELING_CHECK_CONNECTION_RECIPIENT);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                start = true;
            }

        }
        catch (ParameterNotFoundException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
            System.err.println("Errore nella lettura dei parametri: " + ex.getMessage());
        }

        if (start) {

            String requestID = "ENJ-000000000000";
            String srcTransactionID = "COMMUNICATION_SYSTEM";
            String mpTransactionID = "0000000000000000000000000000000";
            String mpTransactionStatus = "ERROR";
            String startRefuel = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            RefuelDetail refuelDetail = new RefuelDetail();
            refuelDetail.setAmount(0.0);
            refuelDetail.setTimestampStartRefuel(startRefuel);
            refuelDetail.setTimestampEndRefuel(startRefuel);
            refuelDetail.setAuthorizationCode("00000");
            refuelDetail.setFuelQuantity(0.0);
            refuelDetail.setFuelType("Gasolio");
            refuelDetail.setProductID("GG");
            refuelDetail.setProductDescription("Gasolio");

            try {
                //refuelingNotificationService = EJBHomeCache.getInstance().getRefuelingNotificationService();
                refuelingNotificationServiceOAuth2 = EJBHomeCache.getInstance().getRefuelingNotificationServiceOAuth2();
                
                response = refuelingNotificationServiceOAuth2.sendMPTransactionNotification(requestID, srcTransactionID, mpTransactionID, mpTransactionStatus, refuelDetail);
                //response = refuelingNotificationService.sendMPTransactionNotification(requestID, srcTransactionID, mpTransactionID, mpTransactionStatus, refuelDetail);
            }
            catch (InterfaceNotFoundException ex) {
                response = ResponseHelper.SYSTEM_ERROR;
                System.err.println("Errore nella creazione dell'istanza del servizio di refueling notification: " + ex.getMessage());
            }

            if (!response.equals("MESSAGE_RECEIVED_200") && (emailRecipient != null && !emailRecipient.isEmpty())) {
                System.out.println("Invio email");

                String keyFrom = emailSender.getSender();

                if (stringSubstitution != null) {
                    keyFrom = stringSubstitution.getValue(keyFrom, 1);
                }

                System.out.println("keyFrom: " + keyFrom);
                EmailType emailType = EmailType.BACKEND_CHECK_CONNECTION_V2;
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("TIME", new SimpleDateFormat("HH:mm:ss").format(new Date())));
                parameters.add(new Parameter("BACKEND", "Refueling"));

                //emailRecipient = "giovanni.dorazio@techedgegroup.com";

                // Sending email
                System.out.println("Sending email to: " + emailRecipient);
                String subject = "Eni Station + Controllo connessione servizi Refueling";
                String sendEmailResult = emailSender.sendEmail(emailType, keyFrom, emailRecipient, null, null, subject, parameters);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }

            System.out.println("sendMPTransactionNotification: " + response);
        }

        System.out.println("stop jobRefuelingCheckConnection");
    }

    @Schedule(hour = "*", minute = "7", persistent = false)
    // ogni ora al settimo minuto
    public void jobFidelityCheckConnection() {
        System.out.println("start jobFidelityCheckConnection");

        Boolean start = false;
        FidelityServiceRemote fidelityService = null;
        String response;
        String emailRecipient = null;

        try {

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            emailRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_FIDELITY_CHECK_CONNECTION_RECIPIENT);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                start = true;
            }

        }
        catch (ParameterNotFoundException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
            System.err.println("Errore nella lettura dei parametri: " + ex.getMessage());
        }

        if (start) {

            Date now = new Date();

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            PartnerType partnerType = PartnerType.MP;
            VoucherConsumerType voucherConsumerType = VoucherConsumerType.ENI;
            Long requestTimestamp = now.getTime();

            List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

            VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
            voucherCodeDetail.setVoucherCode("0000000000");
            voucherCodeList.add(voucherCodeDetail);

            CheckVoucherResult checkVoucherResult = new CheckVoucherResult();

            try {

                fidelityService = EJBHomeCache.getInstance().getFidelityService();

                checkVoucherResult = fidelityService.checkVoucher(operationID, voucherConsumerType, partnerType, requestTimestamp, voucherCodeList);
                response = checkVoucherResult.getStatusCode();
            }
            catch (InterfaceNotFoundException ex) {
                response = ResponseHelper.SYSTEM_ERROR;
                System.err.println("Errore nella creazione dell'istanza del servizio di verifica voucher: " + ex.getMessage());
            }
            catch (FidelityServiceException ex2) {
                response = ResponseHelper.SYSTEM_ERROR;
                System.err.println("Impossibile raggiungere il sistema di backend di Quenit: " + ex2.getMessage());
            }

            if (response.equals(ResponseHelper.SYSTEM_ERROR) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                System.out.println("Invio email");

                String keyFrom = emailSender.getSender();

                if (stringSubstitution != null) {
                    keyFrom = stringSubstitution.getValue(keyFrom, 1);
                }

                System.out.println("keyFrom: " + keyFrom);
                EmailType emailType = EmailType.BACKEND_CHECK_CONNECTION_V2;
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("TIME", new SimpleDateFormat("HH:mm:ss", Locale.ITALY).format(new Date())));
                parameters.add(new Parameter("BACKEND", "Quenit"));

                // Sending email
                System.out.println("Sending email to: " + emailRecipient);
                String subject = "Eni Station + Controllo connessione servizi Quenit";
                String sendEmailResult = emailSender.sendEmail(emailType, keyFrom, emailRecipient, null, null, subject, parameters);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }

            System.out.println("checkVoucher: " + response);
        }

        System.out.println("stop jobFidelityCheckConnection");
    }

    @Schedule(hour = "*", minute = "11", persistent = false)
    // ogni ora all'undicesimo minuto
    public void jobForecourtCheckConnection() {
        System.out.println("start jobForecourtCheckConnection");

        Boolean start = false;
        ForecourtInfoServiceRemote forecourtInfoServiceRemote = null;
        String response = "";
        String emailRecipient = null;

        try {

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            emailRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_FORECOURT_CHECK_CONNECTION_RECIPIENT);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                start = true;
            }

        }
        catch (ParameterNotFoundException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
            System.err.println("Errore nella lettura dei parametri: " + ex.getMessage());
        }

        if (start) {

            Date now = new Date();

            Long requestTimestamp = now.getTime();
            String requestID = requestTimestamp.toString();

            GetStationDetailsResponse getStationDetailsResponse = null;

            try {

                forecourtInfoServiceRemote = EJBHomeCache.getInstance().getForecourtInfoService();

                getStationDetailsResponse = forecourtInfoServiceRemote.getStationDetails(requestID, "00000", null, false);
                response = getStationDetailsResponse.getStatusCode();
            }
            catch (InterfaceNotFoundException ex) {
                response = "GENERIC_FAULT_500";
                System.err.println("Errore nella creazione dell'istanza del servizio di verifica impianto: " + ex.getMessage());
            }

            if (response.equals("GENERIC_FAULT_500") && (emailRecipient != null && !emailRecipient.isEmpty())) {
                System.out.println("Invio email");

                String keyFrom = emailSender.getSender();

                if (stringSubstitution != null) {
                    keyFrom = stringSubstitution.getValue(keyFrom, 1);
                }

                System.out.println("keyFrom: " + keyFrom);
                EmailType emailType = EmailType.BACKEND_CHECK_CONNECTION_V2;
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("TIME", new SimpleDateFormat("HH:mm:ss").format(new Date())));
                parameters.add(new Parameter("BACKEND", "Forecourt"));

                // Sending email
                System.out.println("Sending email to: " + emailRecipient);
                String subject = "Eni Station + Controllo connessione servizi Forecourt";
                String sendEmailResult = emailSender.sendEmail(emailType, keyFrom, emailRecipient, null, null, subject, parameters);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }

            System.out.println("getStationDetails: " + response);
        }

        System.out.println("stop jobForecourtCheckConnection");

    }

    //@Schedule(hour = "*", minute = "*/5", second = "10", persistent = false)
    // ogni 5 minuti
    public void jobProcessMailingList() {

        System.out.println("start jobProcessMailingList");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);
            String mailingListSubject = this.parametersService.getParamValue(SchedulerService.PARAM_MAILING_LIST_SUBJECT);
            Integer mailingListPacketSize = new Integer(parametersService.getParamValue(SchedulerService.PARAM_MAILING_LIST_PACKET_SIZE));

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

                schedulerProcessMailingListAction.execute(emailSender, mailingListSubject, mailingListPacketSize);
            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobProcessMailingList");
    }

    //@Schedule(hour = "*", minute = "*/5", second = "10", persistent = false)
    // ogni 5 minuti
    public CrmOutboundProcessResponse jobProcessCrmOutboundInterface() {

        System.out.println("start jobProcessCrmOutboundInterface");
        CrmOutboundProcessResponse crmOutboundProcessResponse = new CrmOutboundProcessResponse();
        
        try {

            final String ftpsHost = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_HOST);
            final String ftpsPort = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PORT);
            final String ftpsUsername = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_USERNAME);
            final String ftpsPassword = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PASSWORD);
            final String ftpsReadPath = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_READ_PATH);
            final String ftpsArchivePath = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_ARCHIVE_PATH);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PROXY_PORT);
            final int maxDataPerThread = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_DATA_PER_THREAD));
            final int maxThreads = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_THREADS));

            final SFTPService ftpService = new SFTPService(ftpsHost, ftpsPort, ftpsUsername, ftpsPassword, proxyHost, proxyPort);

            final String fileExtensionData = "csv";
            final String fileExtensionOk = "xml.ok";
            final boolean checkEmptyTable = Boolean.parseBoolean(this.parametersService.getParamValue(SchedulerService.PARAM_CRM_PROCESS_CHECK_EMPTY_TABLE));
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                /*new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerProcessCrmOutboundInterfaceAction.execute(ftpsReadPath, ftpsArchivePath, fileExtensionData, fileExtensionOk, crmService, ftpService, 
                                checkEmptyTable, maxDataPerThread, maxThreads);
                    }
                }, "jobProcessCrmOutboundInterface").start();*/
                crmOutboundProcessResponse = schedulerProcessCrmOutboundInterfaceAction.execute(ftpsReadPath, ftpsArchivePath, fileExtensionData, fileExtensionOk, crmService, ftpService, 
                        checkEmptyTable, maxDataPerThread, maxThreads);
            }
            else {
                System.err.println("Server " + serverName + " non autorizzato al processamento del job");
                crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
                crmOutboundProcessResponse.setStatusMessage("Server " + serverName + " non autorizzato al processamento del job");
            }
        }
        catch (ParameterNotFoundException ex) {
            System.err.println("Errore nell'esecuzione del job di lettura file ftp e inserimento dati: " + ex.getMessage());
            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
            crmOutboundProcessResponse.setStatusMessage("Errore nell'esecuzione del job di lettura file ftp e inserimento dati: " + ex.getMessage());
        }

        System.out.println("stop jobProcessCrmOutboundInterface");
        return crmOutboundProcessResponse;
    }
    
    //@Schedule(hour = "*", minute = "*/1", persistent = false)
    // ogni 1 minuti
    public CrmOutboundProcessResponse jobProcessCrmSfOutboundInterface() {

        System.out.println("start jobProcessCrmOutboundInterface");
        CrmOutboundProcessResponse crmOutboundProcessResponse = new CrmOutboundProcessResponse();
        
        try {

            final String ftpsHost = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_HOST);
            final String ftpsPort = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PORT);
            final String ftpsUsername = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_USERNAME);
            final String ftpsPassword = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PASSWORD);
            final String ftpsReadPath = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_READ_PATH);
            final String ftpsArchivePath = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_ARCHIVE_PATH);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PROXY_PORT);
            final int maxDataPerThread = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_DATA_PER_THREAD));
            final int maxThreads = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_THREADS));

            final SFTPService ftpService = new SFTPService(ftpsHost, ftpsPort, ftpsUsername, ftpsPassword, proxyHost, proxyPort);

            final String fileExtensionData = "csv";
            final String fileExtensionOk = "xml.ok";
            final boolean checkEmptyTable = Boolean.parseBoolean(this.parametersService.getParamValue(SchedulerService.PARAM_CRM_PROCESS_CHECK_EMPTY_TABLE));
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                /*new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerProcessCrmOutboundInterfaceAction.execute(ftpsReadPath, ftpsArchivePath, fileExtensionData, fileExtensionOk, crmService, ftpService, 
                                checkEmptyTable, maxDataPerThread, maxThreads);
                    }
                }, "jobProcessCrmOutboundInterface").start();*/
                crmOutboundProcessResponse = schedulerProcessCrmSfOutboundInterfaceAction.execute(ftpsReadPath, ftpsArchivePath, fileExtensionData, fileExtensionOk, crmService, ftpService, 
                        checkEmptyTable, maxDataPerThread, maxThreads);
            }
            else {
                System.err.println("Server " + serverName + " non autorizzato al processamento del job");
                crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
                crmOutboundProcessResponse.setStatusMessage("Server " + serverName + " non autorizzato al processamento del job");
            }
        }
        catch (ParameterNotFoundException ex) {
            System.err.println("Errore nell'esecuzione del job di lettura file ftp e inserimento dati: " + ex.getMessage());
            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
            crmOutboundProcessResponse.setStatusMessage("Errore nell'esecuzione del job di lettura file ftp e inserimento dati: " + ex.getMessage());
        }

        System.out.println("stop jobProcessCrmOutboundInterface");
        return crmOutboundProcessResponse;
    }

    //@Schedule(hour = "*", minute = "*/10", second = "00", persistent = false)
    public void jobProcessResultCrmOutboundInterface() {

        System.out.println("start jobProcessResultCrmOutboundInterface");

        try {

            final String ftpsHost = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_HOST);
            final String ftpsPort = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PORT);
            final String ftpsUsername = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_USERNAME);
            final String ftpsPassword = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PASSWORD);
            final String ftpsResultPath = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_RESULT_PATH);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PROXY_PORT);
            final Integer maxUsersProcessed = Integer.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_CRM_MAX_USERS_PROCESSED));
            final Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
            final Integer expiryTime = Integer.valueOf(this.parametersService.getParamValue(PARAM_PUSH_NOTIFICATION_EXPIRY_TIME));
            final String summaryRecipient = this.parametersService.getParamValue(PARAM_CRM_SUMMARY_RECIPIENT);
            final Integer processResultDelay = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_RESULT_DELAY));
            final SFTPService ftpService = new SFTPService(ftpsHost, ftpsPort, ftpsUsername, ftpsPassword, proxyHost, proxyPort);
            final PushNotificationServiceRemote pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
            final FidelityServiceRemote fidelityService = EJBHomeCache.getInstance().getFidelityService();
            final String fileExtensionData = "csv";
            final String fileExtensionOk = "xml.ok";
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            schedulerProcessResultCrmOutboundInterfaceAction.execute(ftpsResultPath, fileExtensionData, fileExtensionOk, crmService, ftpService,
                                    pushNotificationService, fidelityService, emailSender, summaryRecipient, expiryTime, maxRetryAttemps, maxUsersProcessed, processResultDelay);
                        }
                        catch (Exception ex) {
                            System.out.println(ResponseHelper.SYSTEM_ERROR);
                        }
                    }
                }, "jobProcessResultCrmOutboundInterface").start();

            }
        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface PushNotification not found: " + e.getMessage());
        }

        System.out.println("stop jobProcessResultCrmOutboundInterface");
    }
    
    //@Schedule(hour = "*", minute = "*/1", persistent = false)
    // ogni 1 minuti
    public void jobProcessResultCrmSfOutboundInterface() {

        System.out.println("start jobProcessResultCrmSfOutboundInterface");

        try {

            final String ftpsHost = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_HOST);
            final String ftpsPort = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PORT);
            final String ftpsUsername = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_USERNAME);
            final String ftpsPassword = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PASSWORD);
            final String ftpsResultPath = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_RESULT_PATH);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PROXY_PORT);
            final Integer maxUsersProcessed = Integer.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_CRM_MAX_USERS_PROCESSED));
            final Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
            final Integer expiryTime = Integer.valueOf(this.parametersService.getParamValue(PARAM_PUSH_NOTIFICATION_EXPIRY_TIME));
            final String summaryRecipient = this.parametersService.getParamValue(PARAM_CRM_SUMMARY_RECIPIENT);
            final Integer processResultDelay = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_RESULT_DELAY));
            final SFTPService ftpService = new SFTPService(ftpsHost, ftpsPort, ftpsUsername, ftpsPassword, proxyHost, proxyPort);
            final PushNotificationServiceRemote pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
            final FidelityServiceRemote fidelityService = EJBHomeCache.getInstance().getFidelityService();
            final String fileExtensionData = "csv";
            final String fileExtensionOk = "xml.ok";
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            schedulerProcessResultCrmSfOutboundInterfaceAction.execute(ftpsResultPath, fileExtensionData, fileExtensionOk, crmService, ftpService,
                                    pushNotificationService, fidelityService, emailSender, summaryRecipient, expiryTime, maxRetryAttemps, maxUsersProcessed, processResultDelay);
                        }
                        catch (Exception ex) {
                            System.out.println(ResponseHelper.SYSTEM_ERROR);
                        }
                    }
                }, "jobProcessResultCrmOutboundInterface").start();

            }
        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface PushNotification not found: " + e.getMessage());
        }

        System.out.println("stop jobProcessResultCrmOutboundInterface");
    }

    
    //@Schedule(hour = "*", minute = "*/10", second = "00", persistent = false)
    public CrmOutboundProcessResponse jobProcessResultClusterCrmOutboundInterface() {

        System.out.println("start jobProcessResultClusterCrmOutboundInterface");
        CrmOutboundProcessResponse crmOutboundProcessResponse = new CrmOutboundProcessResponse();

        try {

            final String ftpsHost = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_HOST);
            final String ftpsPort = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PORT);
            final String ftpsUsername = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_USERNAME);
            final String ftpsPassword = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PASSWORD);
            final String ftpsResultPath = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_RESULT_PATH);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_FTPS_PROXY_PORT);
            final String summaryRecipient = this.parametersService.getParamValue(PARAM_CRM_SUMMARY_RECIPIENT);
            final Integer processResultDelay = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_RESULT_DELAY));
            final String fileExtensionData = "csv";
            final String fileExtensionOk = "xml.ok";
            final String arnTopic = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_AWS_ARN_TOPIC);
            final Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
            final Integer expiryTime = Integer.valueOf(this.parametersService.getParamValue(PARAM_PUSH_NOTIFICATION_EXPIRY_TIME));
            final int maxDataPerThread = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_DATA_PER_THREAD));
            final int maxThreads = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_THREADS));
            
            final PushNotificationServiceRemote pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
            final SFTPService ftpService = new SFTPService(ftpsHost, ftpsPort, ftpsUsername, ftpsPassword, proxyHost, proxyPort);

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                /*new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            schedulerProcessResultClusterCrmOutboundInterfaceAction.execute(ftpsResultPath, fileExtensionData, fileExtensionOk, ftpService,
                                    pushNotificationService, emailSender, summaryRecipient, processResultDelay, expiryTime, maxRetryAttemps, maxDataPerThread, maxThreads, arnTopic);
                        }
                        catch (Exception ex) {
                            System.out.println(ResponseHelper.SYSTEM_ERROR);
                        }
                    }
                }, "jobProcessResultClusterCrmOutboundInterface").start();*/
                crmOutboundProcessResponse = schedulerProcessResultClusterCrmOutboundInterfaceAction.execute(ftpsResultPath, fileExtensionData, fileExtensionOk, ftpService,
                        pushNotificationService, emailSender, summaryRecipient, processResultDelay, expiryTime, maxRetryAttemps, maxDataPerThread, maxThreads, arnTopic);
            }
            else {
                System.err.println("Server " + serverName + " non autorizzato al processamento del job");
                crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
                crmOutboundProcessResponse.setStatusMessage("Server " + serverName + " non autorizzato al processamento del job");
            }
        }
        catch (ParameterNotFoundException ex) {
            System.err.println("Parameter not found: " + ex.getMessage());
            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
            crmOutboundProcessResponse.setStatusMessage("Errore nell'esecuzione del job di invio notifica push e creazione risultati: " + ex.getMessage());
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface PushNotification not found: " + e.getMessage());
            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
            crmOutboundProcessResponse.setStatusMessage("Errore nell'esecuzione del job di invio notifica push e creazione risultati: " + e.getMessage());
        }

        System.out.println("stop jobProcessResultClusterCrmOutboundInterface");
        return crmOutboundProcessResponse;
    }
    
    //@Schedule(hour = "*", minute = "*/10", second = "00", persistent = false)
    public CrmOutboundProcessResponse jobProcessSubscribeCrmOutboundInterface() {

        System.out.println("start jobProcessSubscribeCrmOutboundInterface");
        CrmOutboundProcessResponse crmOutboundProcessResponse = new CrmOutboundProcessResponse();

        try {

            final int maxDataPerThread = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_DATA_PER_THREAD));
            final int maxThreads = Integer.valueOf(this.parametersService.getParamValue(PARAM_CRM_PROCESS_MAX_THREADS));
            final String arnTopic = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_AWS_ARN_TOPIC);
            
            final PushNotificationServiceRemote pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                /*new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            schedulerProcessSubscribeCrmOutboundInterfaceAction.execute(pushNotificationService, maxDataPerThread, maxThreads, arnTopic);
                        }
                        catch (Exception ex) {
                            System.out.println(ResponseHelper.SYSTEM_ERROR);
                        }
                    }
                }, "jobProcessSubscribeCrmOutboundInterface").start();*/
                crmOutboundProcessResponse = schedulerProcessSubscribeCrmOutboundInterfaceAction.execute(pushNotificationService, maxDataPerThread, maxThreads, arnTopic);

            }
        }
        catch (ParameterNotFoundException ex) {
            System.err.println("Parameter not found: " + ex.getMessage());
            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
            crmOutboundProcessResponse.setStatusMessage("Errore nell'esecuzione del job di sottoscrizione al cluster: " + ex.getMessage());
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Interface PushNotification not found: " + e.getMessage());
            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
            crmOutboundProcessResponse.setStatusMessage("Errore nell'esecuzione del job di sottoscrizione al cluster: " + e.getMessage());
        }

        System.out.println("stop jobProcessResultClusterCrmOutboundInterface");
        return crmOutboundProcessResponse;
    }
     
    //@Schedule(hour = "*", minute = "13", persistent = false)
    // ogni ora al diciasettesimo minuto
    public void jobDWHCheckConnection() {
        System.out.println("start jobDWHCheckConnection");

        Boolean start = false;
        String response = "";
        String emailRecipient = null;

        try {

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            emailRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_DWH_CHECK_CONNECTION_RECIPIENT);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                start = true;
            }

        }
        catch (ParameterNotFoundException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
            System.err.println("Errore nella lettura dei parametri: " + ex.getMessage());
        }

        if (start) {
            try {
                String email = "test@email.com";
                String password = "test";
                String requestId = String.valueOf(new Date().getTime());

                DWHAdapterResult result = dwhAdapterService.checkLegacyPasswordPlus(email, password, requestId);
                System.out.println("response DWH: " + result.getMessageCode() + " (" + result.getStatusCode() + ")");
                response = result.getStatusCode();
            }
            catch (Exception ex) {
                response = com.techedge.mp.dwh.adapter.utilities.StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR;
                System.err.println("Errore nell'esecuzione del servizio di verifica connessione backend di Serijakala: " + ex.getMessage());
            }

            if (response.equals(com.techedge.mp.dwh.adapter.utilities.StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                System.out.println("Invio email");

                String keyFrom = emailSender.getSender();

                if (stringSubstitution != null) {
                    keyFrom = stringSubstitution.getValue(keyFrom, 1);
                }

                System.out.println("keyFrom: " + keyFrom);
                EmailType emailType = EmailType.BACKEND_CHECK_CONNECTION_V2;
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("TIME", new SimpleDateFormat("HH:mm:ss").format(new Date())));
                parameters.add(new Parameter("BACKEND", "Serijakala"));

                // Sending email
                System.out.println("Sending email to: " + emailRecipient);
                String subject = "Eni Station + Controllo connessione servizi Serijakala";
                String sendEmailResult = emailSender.sendEmail(emailType, keyFrom, emailRecipient, null, null, subject, parameters);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }
        }

        System.out.println("stop jobDWHCheckConnection");

    }

    //@Schedule(hour = "*", minute = "14", persistent = false)
    // ogni ora al ventitresimo minuto
    public void jobCRMCheckConnection() {
        System.out.println("start jobCRMCheckConnection");

        Boolean start = false;
        String response = "";
        String emailRecipient = null;

        try {

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            emailRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_CHECK_CONNECTION_RECIPIENT);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                start = true;
            }

        }
        catch (ParameterNotFoundException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
            System.err.println("Errore nella lettura dei parametri: " + ex.getMessage());
        }

        if (start) {
            try {
                String sessionID = new IdGenerator().generateId(16).substring(0, 32);
                String audienceID = "999";
                String fiscalCode = "9999999999999999";
                Date timestamp = new Date();
                String surname = "";
                String pv = "17131";
                String product = "GG";
                boolean flagPayment = true;
                Integer credits = 0;
                Double refuelQuantity = 0.0;
                Double amount = 0.0;
                String refuelMode = "Servito";
                boolean flagNotification = false;
                boolean flagCreditCard = false;
                String cardBrand = null;
                ClusterType cluster = ClusterType.ENIPAY;

                UserProfile userProfile = UserProfile.getUserProfileForEventRefueling(fiscalCode, timestamp, surname, pv, product, flagPayment, credits, refuelQuantity, amount, refuelMode,
                        flagNotification, flagCreditCard, cardBrand, cluster);

                List<Response> responseBatch = crmAdapterService.executeBatchGetOffers(sessionID, fiscalCode, audienceID, InteractionPointType.REFUELING.getPoint(),
                        userProfile.getParameters());

                Response responseOffers = responseBatch.get(1);
                String message = "";

                if (!responseOffers.getStatusCode().equals(StatusCode.SUCCESS)) {
                    if (responseOffers.getAdvisoryMessages().size() > 0) {
                        message = responseOffers.getAdvisoryMessages().get(0).getMessage() + " (" + responseOffers.getAdvisoryMessages().get(0).getDetailMessage() + ")";
                    }
                }

                System.out.println("response batch: " + message);

            }
            catch (Exception ex) {
                response = ResponseHelper.SYSTEM_ERROR;
                System.err.println("Errore nell'esecuzione del servizio di verifica connessione backend CRM: " + ex.getMessage());
            }

            if (response.equals(ResponseHelper.SYSTEM_ERROR) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                System.out.println("Invio email");

                String keyFrom = emailSender.getSender();

                if (stringSubstitution != null) {
                    keyFrom = stringSubstitution.getValue(keyFrom, 1);
                }

                System.out.println("keyFrom: " + keyFrom);
                EmailType emailType = EmailType.BACKEND_CHECK_CONNECTION_V2;
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("TIME", new SimpleDateFormat("HH:mm:ss").format(new Date())));
                parameters.add(new Parameter("BACKEND", "CRM"));

                // Sending email
                System.out.println("Sending email to: " + emailRecipient);
                String subject = "Eni Station + Controllo connessione servizi CRM";
                String sendEmailResult = emailSender.sendEmail(emailType, keyFrom, emailRecipient, null, null, subject, parameters);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }
        }

        System.out.println("stop jobCRMCheckConnection");

    }
    
    @Schedule(hour = "*", minute = "13", persistent = false)
    // ogni ora al ventitresimo minuto
    public void jobCRMSfNotifyEventCheckConnection() {
        System.out.println("start jobCRMSfNotifyEventCheckConnection");
        
        Boolean start = false;
        String response = "";
        String emailRecipient = null;

        try {

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            emailRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_CHECK_CONNECTION_RECIPIENT);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                start = true;
            }

        }
        catch (ParameterNotFoundException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
            System.err.println("Errore nella lettura dei parametri: " + ex.getMessage());
        }

        if (start) {
            try {
                
                NotifyEventResponse notifyEventResult = new NotifyEventResponse();
                String crmRequestId = new IdGenerator().generateId(16).substring(0, 32); 
                DateFormat formatBirthDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
                Date date = new Date();
                Date birthDate = null;
                try {
                    birthDate = formatBirthDate.parse("1980-01-01");
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                notifyEventResult = crmService.sendNotifySfEventOffer(
                    crmRequestId,
                    "9999999999999999", 
                    date,
                    "1",
                    "Gasolio",
                    Boolean.TRUE, 
                    new Integer(10),
                    new Double(10.0),
                    "Self",
                    "Test", 
                    "Test",
                    "test@test.test",
                    birthDate,
                    Boolean.TRUE, 
                    Boolean.TRUE,
                    "",
                    "1",
                    new Double(1.0), 
                    Boolean.TRUE,
                    Boolean.TRUE,
                    "3330000000", 
                    "0000000000000000000",
                    "4",
                    "", 
                    "",
                    "");
                
                if (notifyEventResult.getSuccess()) {
                    response = ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS;
                }
                else {
                    response = ResponseHelper.ADMIN_SYSTEM_CHECK_FAILURE;
                }
                
                System.out.println("response: " + response);
                if (notifyEventResult.getMessage() != null) {
                    System.out.println("message: " + notifyEventResult.getMessage());
                }
                
            }
            catch (Exception ex) {
                response = ResponseHelper.SYSTEM_ERROR;
                System.err.println("Errore nell'esecuzione del servizio di verifica connessione backend CRM (servizio NotifyEvent): " + ex.getMessage());
            }

            if (response.equals(ResponseHelper.SYSTEM_ERROR) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                System.out.println("Invio email");

                String keyFrom = emailSender.getSender();

                if (stringSubstitution != null) {
                    keyFrom = stringSubstitution.getValue(keyFrom, 1);
                }

                System.out.println("keyFrom: " + keyFrom);
                EmailType emailType = EmailType.BACKEND_CHECK_CONNECTION_V2;
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("TIME", new SimpleDateFormat("HH:mm:ss").format(new Date())));
                parameters.add(new Parameter("BACKEND", "CRM"));

                // Sending email
                System.out.println("Sending email to: " + emailRecipient);
                String subject = "Eni Station + Controllo connessione servizi CRM (servizio NotifyEvent)";
                String sendEmailResult = emailSender.sendEmail(emailType, keyFrom, emailRecipient, null, null, subject, parameters);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }
        }

        System.out.println("stop jobCRMSfNotifyEventCheckConnection");

    }
    
    @Schedule(hour = "*", minute = "14", persistent = false)
    // ogni ora al ventitresimo minuto
    public void jobCRMSfGetOffersCheckConnection() {
        System.out.println("start jobCRMSfGetOffersCheckConnection");
        
        Boolean start = false;
        String response = "";
        String emailRecipient = null;

        try {

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            emailRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_CRM_CHECK_CONNECTION_RECIPIENT);
            String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                start = true;
            }

        }
        catch (ParameterNotFoundException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
            System.err.println("Errore nella lettura dei parametri: " + ex.getMessage());
        }

        if (start) {
            try {
                
                GetOffersResult getOffersResult = new GetOffersResult();
                String crmRequestId = new IdGenerator().generateId(16).substring(0, 32); 
                Date date = new Date();
                getOffersResult = crmService.getMissionsSf(
                        crmRequestId,
                        "9999999999999999",
                        date, 
                        Boolean.TRUE,
                        Boolean.TRUE,
                        "carta", 
                        "cluster");
                
                if (getOffersResult.getSuccess()) {
                    response = ResponseHelper.ADMIN_SYSTEM_CHECK_SUCCESS;
                }
                else {
                    response = ResponseHelper.ADMIN_SYSTEM_CHECK_FAILURE;
                }
                
                System.out.println("response: " + response);
                if (getOffersResult.getMessage() != null) {
                    System.out.println("message: " + getOffersResult.getMessage());
                }

            }
            catch (Exception ex) {
                response = ResponseHelper.SYSTEM_ERROR;
                System.err.println("Errore nell'esecuzione del servizio di verifica connessione backend CRM (servizio GetOffers): " + ex.getMessage());
            }

            if (response.equals(ResponseHelper.SYSTEM_ERROR) && (emailRecipient != null && !emailRecipient.isEmpty())) {
                System.out.println("Invio email");

                String keyFrom = emailSender.getSender();

                if (stringSubstitution != null) {
                    keyFrom = stringSubstitution.getValue(keyFrom, 1);
                }

                System.out.println("keyFrom: " + keyFrom);
                EmailType emailType = EmailType.BACKEND_CHECK_CONNECTION_V2;
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("TIME", new SimpleDateFormat("HH:mm:ss").format(new Date())));
                parameters.add(new Parameter("BACKEND", "CRM"));

                // Sending email
                System.out.println("Sending email to: " + emailRecipient);
                String subject = "Eni Station + Controllo connessione servizi CRM (servizio GetOffers)";
                String sendEmailResult = emailSender.sendEmail(emailType, keyFrom, emailRecipient, null, null, subject, parameters);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }
        }

        System.out.println("stop jobCRMSfGetOffersCheckConnection");

    }

    @Schedule(hour = "*", minute = "35", persistent = false)
    // ogni ora al trentacinquesimo minuto
    public void jobRemovePendingMobilePhone() {

        System.out.println("start jobRemovePendingMobilePhone");

        try {

            Long timeoutPendingMobilePhone = Long.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_TIMEOUT_PENDING_MOBILE_PHONE));

            Date now = new Date();
            Date dateCheck = new Date(now.getTime() - timeoutPendingMobilePhone);

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                schedulerProcessRemovePhoneNumberNotVerifiedAction.execute(dateCheck);
            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobRemovePendingMobilePhone");
    }

    @Schedule(dayOfWeek = "Mon", hour = "9", minute = "32", persistent = false)
    // ogni luned� alle 09:32
    public void jobWeeklyStatisticsReport() {

        System.out.println("start jobWeeklyStatisticsReport");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_WEEKLY_STATISTICS_REPORT_RECIPIENT);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerJobWeeklyStatisticsReportAction.execute(emailSender, reportRecipient, proxyHost, proxyPort, proxyNoHosts);
                    }
                }, "jobWeeklyStatisticsReport").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobWeeklyStatisticsReport");
    }
    
    @Schedule(dayOfWeek = "Mon", hour = "9", minute = "38", persistent = false)
    // ogni luned� alle 09:38
    public void jobWeeklyStatisticsReportBusiness() {

        System.out.println("start jobWeeklyStatisticsReportBusiness");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_WEEKLY_STATISTICS_REPORT_BUSINESS_RECIPIENT);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerJobWeeklyStatisticsReportBusinessAction.execute(emailSender, reportRecipient, proxyHost, proxyPort, proxyNoHosts);
                    }
                }, "jobWeeklyStatisticsReportBusiness").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobWeeklyStatisticsReportBusiness");
    }
    
    @Schedule(hour = "05", minute = "16", persistent = false)
    // ogni giorno alle 05:16
    public void jobSessionCleaner() {

        System.out.println("start jobSessionCleaner");

        try {

            Long timeoutSessionInterval        = 14400000l;
            //Long timeoutCustomerTicketInterval = 604800000l;
            Long timeoutCustomerTicketInterval = 7200000l;

            Date now = new Date();
            Date dateCheck = new Date(now.getTime() - timeoutSessionInterval);
            Date customerDateCheck = new Date(now.getTime() - timeoutCustomerTicketInterval);
            
            System.out.println("dateCheck: " + dateCheck);
            System.out.println("customerDateCheck: " + customerDateCheck);

            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                schedulerJobSessionCleanerAction.execute(dateCheck, customerDateCheck);
            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobSessionCleaner");
    }

    //@Schedule(hour = "*", minute = "32", persistent = false)
    // ogni ora al trentaduesimo minuto
    public void jobUpdateDWHEarnData() {

        System.out.println("start jobUpdateDWHEarnData");

        String response = null;

        try {
//            Integer userCreationTimeLimit = Integer.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_USER_CREATION_TIME_LIMIT));
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);
            
            if (serverName.equals(jobSchedulerInstance)) {

                response = schedulerJobUpdateDWHEarnDataAction.execute();
            }
            else {
                response = "Server name instance error...";
            }
        }
        catch (ParameterNotFoundException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }

        System.out.println("response: " + response);
        System.out.println("stop jobUpdateDWHEarnData");
    }
    
    //@Schedule(hour = "*", minute = "32", persistent = false)
    // ogni ora al trentaduesimo minuto
    public void jobUpdateDWHBurnData() {

        System.out.println("start jobUpdateDWHBurnData");

        String response = null;

        try {
//            Integer userCreationTimeLimit = Integer.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_USER_CREATION_TIME_LIMIT));
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);
            
            String site="AP";
            String card="";
            
            Date now = new Date();

            Long requestTimestamp = now.getTime();
            String requestID = requestTimestamp.toString();
            
            if (serverName.equals(jobSchedulerInstance)) {

                response = schedulerJobUpdateDWHBurnDataAction.execute(site, requestID, card, requestTimestamp);
            }
            else {
                response = "Server name instance error...";
            }
        }
        catch (ParameterNotFoundException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }

        System.out.println("response: " + response);
        System.out.println("stop jobUpdateDWHBurnData");
    }
    
    
    //@Schedule(hour = "*", minute = "32", persistent = false)
    // ogni ora al trentaduesimo minuto
    public void jobUpdateCrmPromotionsData() {

        System.out.println("start jobUpdateCrmPromotionsData");

        String response = null;

        try {
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);
            
            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);
            
            Date now = new Date();

            Long requestTimestamp = now.getTime();
            String requestID = requestTimestamp.toString();
            
            if (serverName.equals(jobSchedulerInstance)) {

                response = schedulerJobUpdateCrmPromotionsDataAction.execute(requestID, requestTimestamp, InteractionPointType.HPC.getPoint(), this.crmAdapterService);
            }
            else {
                response = "Server name instance error...";
            }
        }
        catch (ParameterNotFoundException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }

        System.out.println("response: " + response);
        System.out.println("stop jobUpdateCrmPromotionsData");
    }
    
    //@Schedule(hour = "*", minute = "32", persistent = false)
    // ogni ora al trentaduesimo minuto
    public void jobUpdateCrmSfPromotionsData() {

        System.out.println("start jobUpdateCrmSfPromotionsData");

        String response = null;

        try {
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);
            
            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);
            
            Date now = new Date();

            Long requestTimestamp = now.getTime();
            String requestID = requestTimestamp.toString();
            
            if (serverName.equals(jobSchedulerInstance)) {

                response = schedulerJobUpdateCrmSfPromotionsDataAction.execute(requestID, this.crmAdapterService);
            }
            else {
                response = "Server name instance error...";
            }
        }
        catch (ParameterNotFoundException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }

        System.out.println("response: " + response);
        System.out.println("stop jobUpdateCrmSfPromotionsData");
    }
    
    
    @Schedule(hour = "*", minute = "2/5", second = "10", persistent = false)
    // ogni ogni 5 minuti a partire dal secondo
    public void jobClosePendingParkingTransaction() {

        System.out.println("start jobClosePendingParkingTransaction");

        String response = null;

        try {
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);
            Integer maxIntervalPendingParkingTransaction = Integer.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_MAX_INTERVAL_PENDING_PARKING_TRANSACTION));
            
            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);
            
            if (serverName.equals(jobSchedulerInstance)) {

                if (this.parkingTransactionV2Service == null) {
                    this.parkingTransactionV2Service = EJBHomeCache.getInstance().getParkingTransactionV2Service();
                }
                
                response = schedulerJobClosePendingParkingTransactionAction.execute(maxIntervalPendingParkingTransaction, this.parkingTransactionV2Service );
            }
            else {
                response = "Server name instance error...";
            }
        }
        catch (ParameterNotFoundException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (InterfaceNotFoundException e) {
            throw new EJBException(e);
        }

        System.out.println("response: " + response);
        System.out.println("stop jobClosePendingParkingTransaction");
    }
    
    
    //@Schedule(dayOfMonth = "1", hour = "6", minute = "30", persistent = false)
    // Ogni mese alle ore 09.00
    public void jobMyCiceroUsersRefuelingReport() {

        System.out.println("start jobMyCiceroUsersRefuelingReport");

        try {
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String reportRecipient = this.parametersService.getParamValue(SchedulerService.PARAM_STATISTICS_REPORT_MYCICERO_REFUELING_RECIPIENT);
            final String reportRecipientBCC = this.parametersService.getParamValue(SchedulerService.PARAM_DAILY_CONSUME_VOUCHER_REPORT_RECIPIENT_BCC);
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        schedulerJobStatisticsMyCiceroRefuelingReportAction.execute(emailSender, reportRecipient, reportRecipientBCC, proxyHost, proxyPort, proxyNoHosts);
                    }
                }, "jobMyCiceroUsersRefuelingReport").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("stop jobMyCiceroUsersRefuelingReport");
    }
    
    @Schedule(dayOfWeek = "Mon-Fri", hour = "10", minute = "01", persistent = false)
    // Tutti i marted� e gioved� alle 10:01
    public void jobUpdateStations() {
        System.out.println("start jobUpdateStations");

        String response = null;

        try {
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);
            boolean serviceActive = Boolean.parseBoolean(this.parametersService.getParamValueNoCache(SchedulerService.PARAM_UPDATE_STATION_LIST_SERVICE_ACTIVE));
            
            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);
            
            if (serviceActive) {
                if (serverName.equals(jobSchedulerInstance)) {

                    try {
                        stationService = EJBHomeCache.getInstance().getStationService();
                    }
                    catch (InterfaceNotFoundException e) {

                        throw new EJBException(e);
                    }
                    
                    response = stationService.updateMaster(null);
                }
                else {
                    response = "Server name instance error...";
                }
            }
            else {
                response = "Service is not active...";
            }
        }
        catch (ParameterNotFoundException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {

            response = ResponseHelper.SYSTEM_ERROR;
        }

        System.out.println("response: " + response);
        System.out.println("stop jobUpdateStations");
    }
    
    @Schedule(hour = "*", minute = "1/5", persistent = false)
    // Eseguito ogni 5 minuti (1, 6, 11, 16, ...)
    public void jobCreateVoucherEsPromotion() {
    	System.out.println("start jobCreateVoucherEsPromotion");

        try {
            final PushNotificationServiceRemote pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
            final FidelityServiceRemote fidelityService = EJBHomeCache.getInstance().getFidelityService();
            final EventNotificationService eventNotificationService = EJBHomeCache.getInstance().getEventNotificationService();
            
            String jobSchedulerInstance = this.parametersService.getParamValue(SchedulerService.PARAM_JOB_SCHEDULER_INSTANCE);
            
            String emailSenderAddress = this.parametersService.getParamValue(SchedulerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(SchedulerService.PARAM_SMTP_PORT);
            final String proxyHost = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_HOST);
            final String proxyPort = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_PORT);
            final String proxyNoHosts = this.parametersService.getParamValue(SchedulerService.PARAM_PROXY_NO_HOSTS);
            final String serverName = this.parametersService.getParamValue(SchedulerService.PARAM_SERVER_NAME);
            final String promoCode = this.parametersService.getParamValue(SchedulerService.PARAM_PROMOTION_ES_PROMO_CODE_VODA);
            final Double amount = Double.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_PROMOTION_ES_VODA_AMOUNT));
            final Integer sogliaPromo = Integer.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_PROMOTION_ES_VODA_SILL));
            final Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(SchedulerService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            final String landingWinnerVodafone = this.parametersService.getParamValue(SchedulerService.PARAM_LANDING_WINNER_VODAFONE);

            System.out.println("serverName: " + serverName);
            System.out.println("jobSchedulerInstance: " + jobSchedulerInstance);

            if (serverName.equals(jobSchedulerInstance)) {
                emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
                new Thread(new Runnable() {
                    @Override 
                    public void run() {
                    	schedulerJobCreateVoucherEsPromotionAction.execute(eventNotificationService, pushNotificationService, fidelityService, promoCode, amount, sogliaPromo, maxRetryAttemps, emailSender, landingWinnerVodafone);
                    }
                }, "jobCreateVoucherEsPromotion").start();

            }

        }
        catch (ParameterNotFoundException ex) {
            System.err.println(ex.getMessage());
        } catch (InterfaceNotFoundException e) {
			e.printStackTrace();
		}

        System.out.println("stop jobCreateVoucherEsPromotion");
    }
}
