    package com.techedge.mp.core.business;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.model.ActivityLogBean;
import com.techedge.mp.core.business.utilities.EJBHomeCache;

/**
 * Session Bean implementation class LoggerService
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class LoggerService implements LoggerServiceRemote, LoggerServiceLocal {

	private static final String PARAM_LOG_LEVEL = "LOG_LEVEL";
	
	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "LogPU" )
    private EntityManager em;
    
    
    private ParametersService parametersService = null;
	
	private final static ErrorLevel DEFAULT_LEVEL = ErrorLevel.DEBUG;
	
	private ErrorLevel logLevel;
    
	
    public LoggerService() {

    	this.logLevel = DEFAULT_LEVEL;
    }
    

	@Override
	public void setLogLevel(ErrorLevel level) {
		
		this.logLevel = level;
	}
	

	@Override
	public void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

		try {
			if ( this.parametersService == null ) {
				this.parametersService = EJBHomeCache.getInstance().getParametersService();
			}
			
			Integer logLevel = Integer.valueOf(this.parametersService.getParamValue(LoggerService.PARAM_LOG_LEVEL));
			this.logLevel = ErrorLevel.buildErrorLevel(logLevel);
		}
		catch (ParameterNotFoundException ex) {
			System.out.println("Parameter LOG_LEVEL not found");
			this.logLevel = DEFAULT_LEVEL;
		}
		catch (InterfaceNotFoundException e) {
			System.out.println("InterfaceNotFoundException");
			this.logLevel = DEFAULT_LEVEL;
		}
		
		if ( this.logLevel.isWorseThan(level) ) {
			
			Date timestamp = new Date();
			
			String source = className + "." + methodName;
			
			String formattedMessage = formatMessage(message);
			
			ActivityLogBean activityLogBean = new ActivityLogBean(
				timestamp,
				level,
				source,
				groupId,
				phaseId,
				formattedMessage);
			
			// Disattivazione scrittura log su db
			
			//UserTransaction userTransaction = context.getUserTransaction();
			/*
			try {
				userTransaction.begin();*/
				System.out.println(activityLogBean);
	            /*em.persist( activityLogBean );
	            userTransaction.commit();
			}
			catch ( Exception ex ) {
				try {
					userTransaction.rollback();
				}
				catch (Exception ex2) {
					System.out.println( "Exception executing rollback for ActivityLogBean object in LoggerService: " + ex2.getMessage() );
					ex2.printStackTrace();
				}
				finally {						
					System.out.println( "Exception persisting ActivityLogBean object in LoggerService: " + ex.getMessage() );
					ex.printStackTrace();
				}
			}*/
		}
	}
	
	
	private String formatMessage(String message) {
		
		String formattedMessage = "";
		if ( message != null ) {
			formattedMessage = message.replaceAll("[\\n\\t]", "");
		}
		return formattedMessage;
	}
}
