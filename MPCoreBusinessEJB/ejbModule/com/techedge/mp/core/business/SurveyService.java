package com.techedge.mp.core.business;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.techedge.mp.core.actions.survey.SurveyGetAction;
import com.techedge.mp.core.actions.survey.SurveySubmitAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Survey;
import com.techedge.mp.core.business.interfaces.SurveyAnswers;
import com.techedge.mp.core.business.utilities.EJBHomeCache;

/**
 * Session Bean implementation class SurveyService
 */
@Startup
@Singleton
@LocalBean
public class SurveyService implements SurveyServiceLocal, SurveyServiceRemote {

    @Resource
    private EJBContext          context;

    @Resource
    private SessionContext      sessionContext;
    
    @EJB
    private SurveyGetAction surveyGetAction;

    @EJB
    private SurveySubmitAction surveySubmitAction;

    private LoggerService       loggerService                               = null;
    private ParametersService   parametersService                           = null;

    public SurveyService() {
        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get LoggerService object");
            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ParametersService object");

            throw new EJBException(e);
        }

    }

    @PostConstruct
    public void initialize() {}

    @Override
    public Survey surveyGet(String ticketId, String requestId, String code) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("code", code));
  
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "surveyGet", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        Survey survey = null;
        try {
            survey = surveyGetAction.execute(ticketId, requestId, code);
        }
        catch (EJBException ex) {
            survey.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", survey.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "surveyGet", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        
                
        return survey;
    }
    
    @Override
    public String surveySubmit(String ticketId, String requestId, String code, String key, SurveyAnswers answers) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("code", code));
        inputParameters.add(new Pair<String, String>("key", key));
        inputParameters.add(new Pair<String, String>("answers", answers.toString()));
  
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "surveySubmit", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = surveySubmitAction.execute(ticketId, requestId, code, key, answers);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "surveySubmit", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        
                
        return response;
    }
    

}
