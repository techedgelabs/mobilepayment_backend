package com.techedge.mp.core.business;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.techedge.mp.core.actions.refueling.RefuelingCreateMPTransactionAction;
import com.techedge.mp.core.actions.refueling.RefuelingGetMPTokenAction;
import com.techedge.mp.core.actions.refueling.RefuelingGetMPTransactionReportAction;
import com.techedge.mp.core.actions.refueling.RefuelingGetMPTransactionStatusAction;
import com.techedge.mp.core.actions.refueling.RefuelingSubscriptionAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingCreateMPTransactionResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTokenResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTransactionReportResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTransactionStatusResponse;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.NotifySubscriptionResult;

/**
 * Session Bean implementation class ManagerService
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class RefuelingService implements RefuelingServiceRemote, RefuelingServiceLocal {

    @EJB
    private RefuelingGetMPTokenAction                refuelingGetMPTokenAction;

    @EJB
    private RefuelingCreateMPTransactionAction       refuelingCreateMPTransactionAction;

    @EJB
    private RefuelingGetMPTransactionStatusAction    refuelingGetMPTransactionStatusAction;

    @EJB
    private RefuelingSubscriptionAction              refuelingSubscriptionAction;

    @EJB
    private RefuelingGetMPTransactionReportAction    refuelingGetMPTransactionReportAction;

    private LoggerService                            loggerService                      = null;

    private RefuelingNotificationServiceRemote       refuelingNotificationService       = null;

    private RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2 = null;

    private UserCategoryService                      userCategoryService                = null;
    
    private PaymentServiceRemote                     paymentService                     = null;

    @EJB
    private ParametersService                        parametersService;

    private final static String                      PARAM_REFUELING_OAUTH2_ACTIVE      = "REFUELING_OAUTH2_ACTIVE";
    private final static String                      PARAM_RECONCILIATION_MAX_ATTEMPTS  = "RECONCILIATION_MAX_ATTEMPTS";
    private final static String                      PARAM_MULTICARD_CURRENCY           = "MULTICARD_CURRENCY";

    private String multicardCurrency = "EUR";
    
    /**
     * Default constructor.
     */
    public RefuelingService() throws EJBException {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.refuelingNotificationService = EJBHomeCache.getInstance().getRefuelingNotificationService();
            this.refuelingNotificationServiceOAuth2 = EJBHomeCache.getInstance().getRefuelingNotificationServiceOAuth2();
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
            this.paymentService = EJBHomeCache.getInstance().getPaymentService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        
        
        try {
            this.multicardCurrency = this.parametersService.getParamValue(RefuelingService.PARAM_MULTICARD_CURRENCY);
        }
        catch (ParameterNotFoundException e) {

            throw new EJBException(e);
        }
        
    }

    @Override
    public RefuelingGetMPTokenResponse getMPToken(String requestID, String username, String password) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("username", username));
        inputParameters.add(new Pair<String, String>("password", password));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "RefuelingService", "getMPToken", "opening", ActivityLog.createLogMessage(inputParameters));

        RefuelingGetMPTokenResponse refuelingGetMPTokenResponse = null;

        try {

            refuelingGetMPTokenResponse = refuelingGetMPTokenAction.execute(requestID, username, password);
        }
        catch (EJBException ex) {
            refuelingGetMPTokenResponse = new RefuelingGetMPTokenResponse();
            refuelingGetMPTokenResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", refuelingGetMPTokenResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMPToken", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return refuelingGetMPTokenResponse;
    }

    @Override
    public RefuelingCreateMPTransactionResponse createMPTransaction(String requestID, String mpToken, String srcTransactionID, String stationID, String pumpID, Double amount,
            String currency, Integer pumpNumber, String refuelMode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpToken", mpToken));
        inputParameters.add(new Pair<String, String>("srcTransactionID", srcTransactionID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("amount", String.valueOf(amount)));
        inputParameters.add(new Pair<String, String>("currency", currency));
        inputParameters.add(new Pair<String, String>("pumpNumber", String.valueOf(pumpNumber)));
        inputParameters.add(new Pair<String, String>("refuelMode", refuelMode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "RefuelingService", "createMPTransaction", "opening",
                ActivityLog.createLogMessage(inputParameters));

        RefuelingCreateMPTransactionResponse refuelingCreateResponse;
        try {

            refuelingCreateResponse = refuelingCreateMPTransactionAction.commonExecute(RefuelingCreateMPTransactionResponse.class, this.userCategoryService, requestID, mpToken,
                    multicardCurrency, refuelingNotificationService, refuelingNotificationServiceOAuth2, paymentService, srcTransactionID, stationID, pumpID, amount, currency, pumpNumber, refuelMode);
        }
        catch (EJBException ex) {

            refuelingCreateResponse = new RefuelingCreateMPTransactionResponse();
            refuelingCreateResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", refuelingCreateResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMPTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return refuelingCreateResponse;
    }

    @Override
    public RefuelingGetMPTransactionStatusResponse getMPTransactionStatus(String requestID, String mpToken, String srcTransactionID, String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpToken", mpToken));
        inputParameters.add(new Pair<String, String>("srcTransactionID", srcTransactionID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "RefuelingService", "getMPTransactionStatus", "opening",
                ActivityLog.createLogMessage(inputParameters));

        RefuelingGetMPTransactionStatusResponse refuelingGetMPTransactionStatusResponse;

        try {

            refuelingGetMPTransactionStatusResponse = refuelingGetMPTransactionStatusAction.commonExecute(RefuelingGetMPTransactionStatusResponse.class, this.userCategoryService,
                    requestID, mpToken, multicardCurrency, refuelingNotificationService, refuelingNotificationServiceOAuth2, paymentService, srcTransactionID, mpTransactionID);
        }
        catch (EJBException ex) {

            refuelingGetMPTransactionStatusResponse = new RefuelingGetMPTransactionStatusResponse();
            refuelingGetMPTransactionStatusResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", refuelingGetMPTransactionStatusResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMPTransactionStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return refuelingGetMPTransactionStatusResponse;

    }

    @Override
    public RefuelingGetMPTransactionReportResponse getMPTransactionReport(String requestID, String mpToken, String startDate, String endDate, String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpToken", mpToken));
        inputParameters.add(new Pair<String, String>("startDate", startDate));
        inputParameters.add(new Pair<String, String>("endDate", endDate));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "RefuelingService", "getMPTransactionReport", "opening",
                ActivityLog.createLogMessage(inputParameters));

        RefuelingGetMPTransactionReportResponse refuelingGetMPTransactionReportResponse;

        try {

            refuelingGetMPTransactionReportResponse = refuelingGetMPTransactionReportAction.commonExecute(RefuelingGetMPTransactionReportResponse.class, this.userCategoryService,
                    requestID, mpToken, multicardCurrency, refuelingNotificationService, refuelingNotificationServiceOAuth2, paymentService, startDate, endDate, mpTransactionID, loggerService);
        }
        catch (EJBException ex) {

            refuelingGetMPTransactionReportResponse = new RefuelingGetMPTransactionReportResponse();
            refuelingGetMPTransactionReportResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", refuelingGetMPTransactionReportResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMPTransactionReport", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return refuelingGetMPTransactionReportResponse;
    }

    @Override
    public String notifySubscription(String requestId, String fiscalCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestId));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "RefuelingService", "getNotifySubscription", "opening",
                ActivityLog.createLogMessage(inputParameters));

        NotifySubscriptionResult notifySubscriptionResult;

        String result;

        try {
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
            Boolean refuelingOauth2Active = Boolean.valueOf(this.parametersService.getParamValue(PARAM_REFUELING_OAUTH2_ACTIVE));
            notifySubscriptionResult = refuelingSubscriptionAction.execute(requestId, fiscalCode, refuelingNotificationService, refuelingNotificationServiceOAuth2, maxRetryAttemps, refuelingOauth2Active);
            result = notifySubscriptionResult.getStatusCode();
        }
        catch (EJBException ex) {

            notifySubscriptionResult = new NotifySubscriptionResult();
            notifySubscriptionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            result = ResponseHelper.SYSTEM_ERROR;
        }
        catch (ParameterNotFoundException e) {
            throw new EJBException(e.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", notifySubscriptionResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getNotifySubscription", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return result;

    }
}
