package com.techedge.mp.core.business;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.actions.transaction.TransactionCancelPreAuthorizationConsumeVoucherAction;
import com.techedge.mp.core.actions.transaction.TransactionCheckUndoOperationAction;
import com.techedge.mp.core.actions.transaction.TransactionConfirmRefuelAction;
import com.techedge.mp.core.actions.transaction.TransactionConsumeVoucherPreAuthAction;
import com.techedge.mp.core.actions.transaction.TransactionCreateApplePayRefuelAction;
import com.techedge.mp.core.actions.transaction.TransactionCreateRefuelAction;
import com.techedge.mp.core.actions.transaction.TransactionExistsAction;
import com.techedge.mp.core.actions.transaction.TransactionFinalizePendingRefuelAction;
import com.techedge.mp.core.actions.transaction.TransactionGetTransactionDetailAction;
import com.techedge.mp.core.actions.transaction.TransactionPersistEndRefuelReceivedStatusAction;
import com.techedge.mp.core.actions.transaction.TransactionPersistPaymentAuthorizationStatusAction;
import com.techedge.mp.core.actions.transaction.TransactionPersistPaymentCompletionStatusAction;
import com.techedge.mp.core.actions.transaction.TransactionPersistPaymentDeletionStatusAction;
import com.techedge.mp.core.actions.transaction.TransactionPersistPumpEnableStatusAction;
import com.techedge.mp.core.actions.transaction.TransactionPersistPumpGenericFaultStatusAction;
import com.techedge.mp.core.actions.transaction.TransactionPersistStartRefuelReceivedStatusAction;
import com.techedge.mp.core.actions.transaction.TransactionPersistStatusRequestKoStatusAction;
import com.techedge.mp.core.actions.transaction.TransactionPersistTransactionOperationAction;
import com.techedge.mp.core.actions.transaction.TransactionPersistUndoRefuelStatusAction;
import com.techedge.mp.core.actions.transaction.TransactionPreAuthorizationConsumeVoucherAction;
import com.techedge.mp.core.actions.transaction.TransactionPreAuthorizationConsumeVoucherNoTransactionAction;
import com.techedge.mp.core.actions.transaction.TransactionRefreshStationInfoAction;
import com.techedge.mp.core.actions.transaction.TransactionRetrieveEventsDataAction;
import com.techedge.mp.core.actions.transaction.TransactionRetrievePendingRefuelAction;
import com.techedge.mp.core.actions.transaction.TransactionRetrieveRefuelPaymentDetailAction;
import com.techedge.mp.core.actions.transaction.TransactionRetrieveRefuelPaymentHistoryAction;
import com.techedge.mp.core.actions.transaction.TransactionRetrieveStationActiveAction;
import com.techedge.mp.core.actions.transaction.TransactionRetrieveStationIdAction;
import com.techedge.mp.core.actions.transaction.TransactionRetrieveStationIdByCoordsAction;
import com.techedge.mp.core.actions.transaction.TransactionSetGFGElectronicInvoiceIDAction;
import com.techedge.mp.core.actions.transaction.TransactionSetGFGNotificationAction;
import com.techedge.mp.core.actions.transaction.TransactionUpdateFinalAmountAction;
import com.techedge.mp.core.actions.transaction.TransactionUpdateFinalStatusAction;
import com.techedge.mp.core.actions.transaction.TransactionUpdateReconciliationAttemptsAction;
import com.techedge.mp.core.actions.transaction.TransactionUseVoucherAction;
import com.techedge.mp.core.actions.transaction.v2.TransactionV2CreateMulticardRefuelAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.CreateApplePayRefuelResponse;
import com.techedge.mp.core.business.interfaces.CreateMulticardRefuelResponse;
import com.techedge.mp.core.business.interfaces.CreateRefuelResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.PaymentRefuelDetailResponse;
import com.techedge.mp.core.business.interfaces.PaymentRefuelHistoryResponse;
import com.techedge.mp.core.business.interfaces.PendingRefuelResponse;
import com.techedge.mp.core.business.interfaces.PendingTransactionRefuelResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveTransactionEventsData;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionCancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.TransactionConsumeVoucherPreAuthResponse;
import com.techedge.mp.core.business.interfaces.TransactionExistsResponse;
import com.techedge.mp.core.business.interfaces.TransactionPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.TransactionUseVoucherResponse;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

@Stateless
@LocalBean
public class TransactionService implements TransactionServiceRemote, TransactionServiceLocal {

    /*
     * private static final String CURRENCY = "242";
     * private static final String PAYMENT_TYPE = "MobilePayment";
     * private static final Integer PIN_CHECK_MAX_ATTEMPTS = 5;
     * private static final Integer STATUS_MAX_ATTEMPTS = 3;
     */

    private static final String                                          PARAM_UIC                            = "UIC";
    private static final String                                          PARAM_SERVER_NAME                    = "SERVER_NAME";
    private static final String                                          PARAM_PAYMENT_TYPE                   = "PAYMENT_TYPE";
    private static final String                                          PARAM_PIN_CHECK_MAX_ATTEMPTS         = "PIN_CHECK_MAX_ATTEMPTS";
    private static final String                                          PARAM_STATUS_MAX_ATTEMPTS            = "STATUS_MAX_ATTEMPTS";
    private static final String                                          PARAM_RECONCILIATION_MAX_ATTEMPTS    = "RECONCILIATION_MAX_ATTEMPTS";
    private static final String                                          PARAM_EMAIL_SENDER_ADDRESS           = "EMAIL_SENDER_ADDRESS";
    private static final String                                          PARAM_SMTP_HOST                      = "SMTP_HOST";
    private static final String                                          PARAM_SMTP_PORT                      = "SMTP_PORT";
    private static final String                                          PARAM_RECEIPT_EMAIL_ACTIVE           = "RECEIPT_EMAIL_ACTIVE";
    private static final String                                          PARAM_RANGE_THRESHOLD                = "RANGE_THRESHOLD";
    private final static String                                          PARAM_PROXY_HOST                     = "PROXY_HOST";
    private final static String                                          PARAM_PROXY_PORT                     = "PROXY_PORT";
    private final static String                                          PARAM_PROXY_NO_HOSTS                 = "PROXY_NO_HOSTS";
    private final static String                                          PARAM_TRANSACTION_TIME_ALIVE         = "TRANSACTION_TIME_ALIVE";
    private final static String                                          PARAM_ACQUIRER_ID                    = "ACQUIRER_ID";
    private final static String                                          PARAM_SHOPLOGIN_NEW_FLOW             = "SHOPLOGIN_NEW_FLOW";
    private final static String                                          PARAM_VOUCHER_TRANSACTION_MIN_AMOUNT = "VOUCHER_TRANSACTION_MIN_AMOUNT";
    private final static String                                          PARAM_USER_BLOCK_EXCEPTION           = "USER_BLOCK_EXCEPTION";
    private final static String                                          PARAM_CARTASI_VAULT_BLOCK            = "CARTASI_VAULT_BLOCK";
    private final static String                                          PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY  = "CARTASI_VAULT_BLOCK_ATTRIBUTE_KEY";
    private final static String                                          PARAM_STRING_SUBSTITUTION_PATTERN    = "STRING_SUBSTITUTION_PATTERN";

    @EJB
    private TransactionCreateRefuelAction                                transactionCreateRefuelAction;

    @EJB
    private TransactionCreateApplePayRefuelAction                        transactionCreateApplePayRefuelAction;

    @EJB
    private TransactionPersistUndoRefuelStatusAction                     transactionPersistUndoRefuelStatusAction;

    @EJB
    private TransactionConfirmRefuelAction                               transactionConfirmRefuelAction;

    @EJB
    private TransactionRetrievePendingRefuelAction                       transactionRetrievePendingRefuelAction;

    @EJB
    private TransactionFinalizePendingRefuelAction                       transactionFinalizePendingRefuelAction;

    @EJB
    private TransactionRetrieveRefuelPaymentDetailAction                 transactionRetrieveRefuelPaymentDetailAction;

    @EJB
    private TransactionPersistStartRefuelReceivedStatusAction            transactionPersistStartRefuelReceivedStatusAction;

    @EJB
    private TransactionPersistEndRefuelReceivedStatusAction              transactionPersistEndRefuelReceivedStatusAction;

    @EJB
    private TransactionPersistStatusRequestKoStatusAction                transactionPersistStatusRequestKoStatusAction;

    @EJB
    private TransactionPersistPumpEnableStatusAction                     transactionPersistPumpEnableStatusAction;

    @EJB
    private TransactionPersistPumpGenericFaultStatusAction               transactionPersistPumpGenericFaultStatusAction;

    @EJB
    private TransactionSetGFGNotificationAction                          transactionSetGFGNotificationAction;

    @EJB
    private TransactionPersistPaymentAuthorizationStatusAction           transactionPersistPaymentAuthorizationStatusAction;

    @EJB
    private TransactionPersistPaymentDeletionStatusAction                transactionPersistPaymentDeletionStatusAction;

    @EJB
    private TransactionPersistPaymentCompletionStatusAction              transactionPersistPaymentCompletionStatusAction;

    @EJB
    private TransactionCheckUndoOperationAction                          transactionCheckUndoOperationAction;

    @EJB
    private TransactionExistsAction                                      transactionExistsAction;

    @EJB
    private TransactionRefreshStationInfoAction                          transactionRefreshStationInfoAction;

    @EJB
    private TransactionRetrieveRefuelPaymentHistoryAction                transactionRetrieveRefuelPaymentHistoryAction;

    @EJB
    private TransactionRetrieveStationIdByCoordsAction                   transactionRetrieveStationIdByCoordsAction;

    @EJB
    private TransactionRetrieveStationActiveAction                       transactionRetrieveStationActiveAction;

    @EJB
    private TransactionRetrieveEventsDataAction                          transactionRetrieveEventsDataAction;

    @EJB
    private TransactionRetrieveStationIdAction                           transactionRetrieveStationIdAction;

    @EJB
    private TransactionUpdateFinalStatusAction                           transactionUpdateFinalStatusAction;

    @EJB
    private TransactionUpdateFinalAmountAction                           transactionUpdateFinalAmountAction;

    @EJB
    private TransactionUpdateReconciliationAttemptsAction                transactionUpdateReconciliationAttemptsAction;

    @EJB
    private TransactionGetTransactionDetailAction                        transactionGetTransactionDetailAction;

    @EJB
    private TransactionUseVoucherAction                                  transactionUseVoucherAction;

    @EJB
    private TransactionPreAuthorizationConsumeVoucherAction              transactionPreAuthorizationConsumeVoucherAction;

    @EJB
    private TransactionCancelPreAuthorizationConsumeVoucherAction        transactionCancelPreAuthorizationConsumeVoucherAction;

    @EJB
    private TransactionConsumeVoucherPreAuthAction                       transactionConsumeVoucherPreAuthAction;

    @EJB
    private TransactionPreAuthorizationConsumeVoucherNoTransactionAction transactionPreAuthorizationConsumeVoucherNoTransactionAction;

    @EJB
    private TransactionSetGFGElectronicInvoiceIDAction                   transactionSetGFGElectronicInvoiceIDAction;
    
    @EJB
    private TransactionPersistTransactionOperationAction                 transactionPersistTransactionOperationAction;

    
    private LoggerService                                                loggerService                        = null;
    private ParametersService                                            parametersService                    = null;
    private EmailSenderRemote                                            emailSender                          = null;
    private FidelityServiceRemote                                        fidelityService                      = null;
    private RefuelingNotificationServiceRemote                           refuelingNotificationService         = null;
    private RefuelingNotificationServiceOAuth2Remote                     refuelingNotificationServiceOAuth2   = null;
    private UserCategoryService                                          userCategoryService                  = null;
    private UnavailabilityPeriodService                                  unavailabilityPeriodService          = null;

    private String                                                       secretKey                            = null;
    private StringSubstitution                                           stringSubstitution                   = null;

    /*
     * private String emailSenderAddress = null;
     * private String smtpHost = null;
     * private String smtpPort = null;
     * private Boolean receiptEmailActive = false;
     */

    /**
     * Default constructor.
     */
    public TransactionService() {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get EmailSender object");

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.refuelingNotificationService = EJBHomeCache.getInstance().getRefuelingNotificationService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.refuelingNotificationServiceOAuth2 = EJBHomeCache.getInstance().getRefuelingNotificationServiceOAuth2();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.unavailabilityPeriodService = EJBHomeCache.getInstance().getUnavailabilityPeriodService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        String vaultBlock = "";
        String vaultBlockAttributeCryptKey = "";
        String pattern = null;

        try {
            vaultBlock = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK);
            vaultBlockAttributeCryptKey = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY);
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeCryptKey + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.secretKey + "'");

        stringSubstitution = new StringSubstitution(pattern);
    }

    @Override
    public CreateRefuelResponse createRefuel(String ticketID, String requestID, String encodedPin, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Double amountVoucher, Boolean useVoucher, Long paymentMethodId, String paymentMethodType, String outOfRange, String refuelMode) {

        String paymentMethodIdString = "";

        if (paymentMethodId != null) {
            paymentMethodIdString = paymentMethodId.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("encodedPin", encodedPin));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("pumpNumber", pumpNumber.toString()));
        inputParameters.add(new Pair<String, String>("productID", productID));
        inputParameters.add(new Pair<String, String>("productDescription", productDescription));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        if (amountVoucher != null) {
            inputParameters.add(new Pair<String, String>("amountVoucher", amountVoucher.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("amountVoucher", "null"));
        }
        if (useVoucher != null) {
            inputParameters.add(new Pair<String, String>("useVoucher", useVoucher.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("useVoucher", "null"));
        }
        inputParameters.add(new Pair<String, String>("paymentMethodId", paymentMethodIdString));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("outOfRange", outOfRange));
        inputParameters.add(new Pair<String, String>("refuelMode", refuelMode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createRefuel", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateRefuelResponse createRefuelResponse = null;
        try {

            String serverName = this.parametersService.getParamValue(TransactionService.PARAM_SERVER_NAME);
            String currency = this.parametersService.getParamValue(TransactionService.PARAM_UIC);
            String paymentType = this.parametersService.getParamValue(TransactionService.PARAM_PAYMENT_TYPE);
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            Integer statusMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionService.PARAM_STATUS_MAX_ATTEMPTS));
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            String emailSenderAddress = this.parametersService.getParamValue(TransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_NO_HOSTS);
            String shopLoginNewFlow = this.parametersService.getParamValue(TransactionService.PARAM_SHOPLOGIN_NEW_FLOW);
            String acquirerID = this.parametersService.getParamValue(TransactionService.PARAM_ACQUIRER_ID);
            Double voucherTransactionMinAmount = Double.valueOf(this.parametersService.getParamValue(TransactionService.PARAM_VOUCHER_TRANSACTION_MIN_AMOUNT));
            String userBlockException = this.parametersService.getParamValue(TransactionService.PARAM_USER_BLOCK_EXCEPTION);
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            createRefuelResponse = transactionCreateRefuelAction.execute(ticketID, requestID, encodedPin, stationID, pumpID, pumpNumber, productID, productDescription, amount,
                    amountVoucher, useVoucher, currency, paymentType, paymentMethodId, paymentMethodType, outOfRange, refuelMode, shopLoginNewFlow, acquirerID,
                    pinCheckMaxAttempts, statusMaxAttempts, reconciliationMaxAttempts, serverName, voucherTransactionMinAmount, userBlockExceptionList, emailSender,
                    userCategoryService, this, gpService, fidelityService, unavailabilityPeriodService, proxyHost, proxyPort, proxyNoHosts);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createRefuelResponse = new CreateRefuelResponse();
            createRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get GPService object");
            createRefuelResponse = new CreateRefuelResponse();
            createRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            createRefuelResponse = new CreateRefuelResponse();
            createRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", createRefuelResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createRefuel", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createRefuelResponse;
    }

    @Override
    public String persistUndoRefuelStatus(String ticketID, String requestID, String refuelID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("refuelID", refuelID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistUndoRefuelStatus", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = transactionPersistUndoRefuelStatusAction.execute(ticketID, requestID, refuelID);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistUndoRefuelStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String confirmRefuel(String ticketID, String requestID, String refuelID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("refuelID", refuelID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "confirmRefuel", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = transactionConfirmRefuelAction.execute(ticketID, requestID, refuelID);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "confirmRefuel", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public PendingRefuelResponse retrievePendingRefuel(String requestID, String ticketID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrievePendingRefuel", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PendingRefuelResponse pendingRefuelResponse = null;
        try {
            pendingRefuelResponse = transactionRetrievePendingRefuelAction.execute(ticketID, requestID);
        }
        catch (EJBException ex) {
            pendingRefuelResponse = new PendingRefuelResponse();
            pendingRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", pendingRefuelResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrievePendingRefuel", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return pendingRefuelResponse;
    }

    @Override
    public PaymentRefuelDetailResponse retrieveRefuelPaymentDetail(String requestID, String ticketID, String refuelID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("refuelID", refuelID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveRefuelPaymentDetail", requestID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        PaymentRefuelDetailResponse paymentRefuelDetailResponse = null;
        try {
            paymentRefuelDetailResponse = transactionRetrieveRefuelPaymentDetailAction.execute(requestID, ticketID, refuelID);
        }
        catch (EJBException ex) {
            paymentRefuelDetailResponse = new PaymentRefuelDetailResponse();
            paymentRefuelDetailResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", paymentRefuelDetailResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveRefuelPaymentDetail", requestID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return paymentRefuelDetailResponse;
    }

    @Override
    public String persistStartRefuelReceivedStatus(String requestID, String transactionID, String source) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("source", source));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistStartRefuelReceivedStatus", requestID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = transactionPersistStartRefuelReceivedStatusAction.execute(requestID, transactionID, source);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistStartRefuelReceivedStatus", requestID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String persistEndRefuelReceivedStatus(String requestID, String transactionID, Double amount, Double fuelQuantity, String fuelType, String productDescription,
            String productID, String timestampEndRefuel, Double unitPrice, String source) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        inputParameters.add(new Pair<String, String>("fuelQuantity", fuelQuantity.toString()));
        inputParameters.add(new Pair<String, String>("fuelType", fuelType));
        inputParameters.add(new Pair<String, String>("productDescription", productDescription));
        inputParameters.add(new Pair<String, String>("productID", productID));
        inputParameters.add(new Pair<String, String>("timestampEndRefuel", timestampEndRefuel));
        if (unitPrice != null) {
            inputParameters.add(new Pair<String, String>("unitPrice", unitPrice.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("unitPrice", "null"));
        }
        inputParameters.add(new Pair<String, String>("source", source));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistEndRefuelReceivedStatus", requestID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = transactionPersistEndRefuelReceivedStatusAction.execute(requestID, transactionID, amount, fuelQuantity, fuelType, productDescription, productID,
                    timestampEndRefuel, unitPrice, source);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistEndRefuelReceivedStatus", requestID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String persistTransactionStatusRequestKoStatus(String requestID, String transactionID, String subStatus, String subStatusDescription) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("subStatus", subStatus));
        inputParameters.add(new Pair<String, String>("subStatusDescription", subStatusDescription));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistTransactionStatusRequestKoStatus", requestID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            String emailSenderAddress = this.parametersService.getParamValue(TransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = transactionPersistStatusRequestKoStatusAction.execute(requestID, transactionID, subStatus, subStatusDescription, emailSender, proxyHost, proxyPort,
                    proxyNoHosts, userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistTransactionStatusRequestKoStatus", requestID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String persistNewStatus(String statusCode, String messageCode, String transactionID) {

        return StatusHelper.PERSIST_NEW_STATUS_SUCCESS;
    }

    @Override
    public String persistPumpAvailabilityStatus(String statusCode, String messageCode, String transactionID, String requestID) {

        return StatusHelper.PERSIST_PUMP_AVAILABILITY_STATUS_SUCCESS;
    }

    @Override
    public String persistPumpEnableStatus(String statusCode, String messageCode, String transactionID, String requestID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("statusCode", statusCode));
        inputParameters.add(new Pair<String, String>("messageCode", messageCode));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistPumpEnableStatus", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = transactionPersistPumpEnableStatusAction.execute(statusCode, messageCode, transactionID, requestID, refuelingNotificationService, 
                    refuelingNotificationServiceOAuth2, userCategoryService);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistPumpEnableStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String persistPumpGenericFaultStatus(String transactionID, String requestID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistPumpGenericFaultStatus", requestID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            String emailSenderAddress = this.parametersService.getParamValue(TransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = transactionPersistPumpGenericFaultStatusAction.execute(transactionID, requestID, emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, 
                    proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistPumpGenericFaultStatus", requestID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String setGFGNotification(String transactionID, boolean flag_value, String electronicInvoiceID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("electronicInvoiceID", electronicInvoiceID != null ? electronicInvoiceID : ""));
        

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setGFGNotification", transactionID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = transactionSetGFGNotificationAction.execute(transactionID, flag_value, electronicInvoiceID);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setGFGNotification", transactionID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String persistPaymentAuthorizationStatus(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId,
            String authorizationCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("statusCode", statusCode));
        inputParameters.add(new Pair<String, String>("subStatusCode", subStatusCode));
        inputParameters.add(new Pair<String, String>("errorCode", errorCode));
        inputParameters.add(new Pair<String, String>("errorMessage", errorMessage));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("bankTransactionId", bankTransactionId));
        inputParameters.add(new Pair<String, String>("authorizationCode", authorizationCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistPaymentAuthorizationStatus", transactionID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            String emailSenderAddress = this.parametersService.getParamValue(TransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = transactionPersistPaymentAuthorizationStatusAction.execute(statusCode, subStatusCode, errorCode, errorMessage, transactionID, bankTransactionId,
                    authorizationCode, emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistPaymentAuthorizationStatus", transactionID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String persistPaymentDeletionStatus(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("statusCode", statusCode));
        inputParameters.add(new Pair<String, String>("subStatusCode", subStatusCode));
        inputParameters.add(new Pair<String, String>("errorCode", errorCode));
        inputParameters.add(new Pair<String, String>("errorMessage", errorMessage));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("bankTransactionId", bankTransactionId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistPaymentDeletionStatus", transactionID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            String emailSenderAddress = this.parametersService.getParamValue(TransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = transactionPersistPaymentDeletionStatusAction.execute(statusCode, subStatusCode, errorCode, errorMessage, transactionID, bankTransactionId, emailSender,
                    refuelingNotificationService, refuelingNotificationServiceOAuth2, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistPaymentDeletionStatus", transactionID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String persistPaymentCompletionStatus(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId,
            Double effectiveAmount) {

        String effectiveAmountString = "";
        if (effectiveAmount != null) {
            effectiveAmountString = effectiveAmount.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("statusCode", statusCode));
        inputParameters.add(new Pair<String, String>("subStatusCode", subStatusCode));
        inputParameters.add(new Pair<String, String>("errorCode", errorCode));
        inputParameters.add(new Pair<String, String>("errorMessage", errorMessage));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("bankTransactionId", bankTransactionId));
        inputParameters.add(new Pair<String, String>("effectiveAmount", effectiveAmountString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistPaymentCompletionStatus", transactionID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String emailSenderAddress = this.parametersService.getParamValue(TransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_PORT);
            String receiptEmailActiveString = this.parametersService.getParamValue(TransactionService.PARAM_RECEIPT_EMAIL_ACTIVE);
            String proxyHost = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_NO_HOSTS);

            Boolean receiptEmailActive = false;
            if (receiptEmailActiveString.equals("true")) {
                receiptEmailActive = true;
            }

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = transactionPersistPaymentCompletionStatusAction.execute(statusCode, subStatusCode, errorCode, errorMessage, transactionID, bankTransactionId,
                    effectiveAmount, receiptEmailActive, this.emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, this.userCategoryService, 
                    proxyHost, proxyPort, proxyNoHosts, stringSubstitution, this.fidelityService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistPaymentCompletionStatus", transactionID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public Boolean checkUndoOperation(String transactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkUndoOperation", transactionID, "opening", ActivityLog.createLogMessage(inputParameters));

        Boolean response = null;
        try {
            response = transactionCheckUndoOperationAction.execute(transactionID);
        }
        catch (EJBException ex) {
            response = StatusHelper.CHECK_UNDO_OPERATION_NOT_FOUND;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkUndoOperation", transactionID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public TransactionExistsResponse transactionExists(String transactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "transactionExists", transactionID, "opening", ActivityLog.createLogMessage(inputParameters));

        TransactionExistsResponse transactionExistsResponse = null;
        try {
            transactionExistsResponse = transactionExistsAction.execute(transactionID);
        }
        catch (EJBException ex) {
            transactionExistsResponse = null;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        if (transactionExistsResponse != null) {
            outputParameters.add(new Pair<String, String>("statusCode", transactionExistsResponse.getStatus()));
        }
        else {
            outputParameters.add(new Pair<String, String>("statusCode", null));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "transactionExists", transactionID, "closing", ActivityLog.createLogMessage(outputParameters));

        return transactionExistsResponse;
    }

    @Override
    public String refreshStationInfo(String ticketID, String stationID, String address, String city, String country, String province, Double latitude, Double longitude) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("address", address));
        inputParameters.add(new Pair<String, String>("city", city));
        inputParameters.add(new Pair<String, String>("country", country));
        inputParameters.add(new Pair<String, String>("province", province));
        inputParameters.add(new Pair<String, String>("latitude", latitude.toString()));
        inputParameters.add(new Pair<String, String>("longitude", longitude.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refreshStationInfo", ticketID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = transactionRefreshStationInfoAction.execute(ticketID, stationID, address, city, country, province, latitude, longitude);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refreshStationInfo", ticketID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    public PaymentRefuelHistoryResponse retrieveRefuelPaymentHistory(String requestID, String ticketID, Integer pageNumber, Integer itemForPage, Date startDate, Date endDate,
            Boolean detail) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("pageNumber", pageNumber.toString()));
        inputParameters.add(new Pair<String, String>("itemForPage", itemForPage.toString()));
        inputParameters.add(new Pair<String, String>("startDate", startDate.toString()));
        inputParameters.add(new Pair<String, String>("endDate", endDate.toString()));
        inputParameters.add(new Pair<String, String>("detail", detail.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveRefuelPaymentHistory", requestID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        PaymentRefuelHistoryResponse paymentRefuelHistoryResponse = null;
        try {
            paymentRefuelHistoryResponse = transactionRetrieveRefuelPaymentHistoryAction.execute(requestID, ticketID, pageNumber, itemForPage, startDate, endDate, detail);
        }
        catch (EJBException ex) {
            paymentRefuelHistoryResponse = new PaymentRefuelHistoryResponse();
            paymentRefuelHistoryResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", paymentRefuelHistoryResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveRefuelPaymentHistory", requestID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return paymentRefuelHistoryResponse;
    }

    @Override
    public String retrieveStationId(String beaconCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("beaconCode", beaconCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveStationId", beaconCode, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = transactionRetrieveStationIdAction.execute(beaconCode);
        }
        catch (EJBException ex) {
            response = null;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveStationId", beaconCode, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String retrieveStationIdByCoords(Double latitude, Double longitude) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("latitude", latitude.toString()));
        inputParameters.add(new Pair<String, String>("longitude", longitude.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveStationIdByCoords", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            Double outRangeThreshold = Double.valueOf(this.parametersService.getParamValue(TransactionService.PARAM_RANGE_THRESHOLD));

            response = transactionRetrieveStationIdByCoordsAction.execute(latitude, longitude, outRangeThreshold);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "retrieveStationIdByCoords", null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = null;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveStationIdByCoords", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    public RetrieveTransactionEventsData retrieveEventList(String requestId, String transactionId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestId));
        inputParameters.add(new Pair<String, String>("transactionId", transactionId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveRefuelPaymentHistory", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        RetrieveTransactionEventsData retrieveTransactionEventsData = null;
        try {
            retrieveTransactionEventsData = transactionRetrieveEventsDataAction.execute(requestId, transactionId);
        }
        catch (EJBException ex) {
            retrieveTransactionEventsData = new RetrieveTransactionEventsData();
            retrieveTransactionEventsData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveTransactionEventsData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveRefuelPaymentHistory", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return retrieveTransactionEventsData;
    }

    public String updateFinalStatus(String transactionID, String finalStatus) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("finalStatus", finalStatus));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateFinalStatus", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;
        try {
            response = transactionUpdateFinalStatusAction.execute(transactionID, finalStatus);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateFinalStatus", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    public String updateFinalAmount(String transactionID, Double finalAmount) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("finalAmount", finalAmount.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateFinalAmount", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;
        try {
            response = transactionUpdateFinalAmountAction.execute(transactionID, finalAmount);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateFinalAmount", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    public int updateReconciliationAttempts(String transactionID, Integer attemptsLeft) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("attemptsLeft", attemptsLeft.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateReconciliationAttempts", null, "opening", ActivityLog.createLogMessage(inputParameters));

        int attempts;

        try {
            attempts = transactionUpdateReconciliationAttemptsAction.execute(transactionID, attemptsLeft);
        }
        catch (EJBException ex) {
            attempts = -1;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("attempts", String.valueOf(attempts)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateReconciliationAttempts", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return attempts;

    }

    public PendingTransactionRefuelResponse finalizePendingRefuel() {
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "finalizePendingRefuel", null, "opening", null);

        PendingTransactionRefuelResponse response = new PendingTransactionRefuelResponse();

        try {
            Integer transactionTimeAlive = new Integer(0);

            try {
                if (this.parametersService.getParamValue(TransactionService.PARAM_TRANSACTION_TIME_ALIVE) != null) {
                    transactionTimeAlive = Integer.valueOf(this.parametersService.getParamValue(TransactionService.PARAM_TRANSACTION_TIME_ALIVE));
                }
                else {
                    System.out.println("Param 'TRANSACTION_TIME_ALIVE' is null. Used default value(0)");
                }
            }
            catch (ParameterNotFoundException pnfe) {
                System.out.println("Param 'TRANSACTION_TIME_ALIVE' is not found. Used default value(0)");
            }

            response = transactionFinalizePendingRefuelAction.execute(transactionTimeAlive);
        }
        catch (EJBException ex) {
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateReconciliationAttempts", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public TransactionUseVoucherResponse transactionUseVoucher(String mpTransactionID, Double amount) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionDetail", "", "opening", ActivityLog.createLogMessage(inputParameters));

        TransactionUseVoucherResponse transactionUseVoucherResponse = null;

        try {
            transactionUseVoucherResponse = transactionUseVoucherAction.execute(mpTransactionID, amount, fidelityService);
            // transactionResponse = transactionGetTransactionDetailAction.execute(requestId, mpTransactionID);
        }
        catch (EJBException ex) {
            transactionUseVoucherResponse = new TransactionUseVoucherResponse();
            transactionUseVoucherResponse.setTransactionID(mpTransactionID);
            transactionUseVoucherResponse.setVoucherUsed(false);
        }

        //        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        //        outputParameters.add(new Pair<String, String>("statusCode", popPaidGetTransactionDetailResponse.getStatusCode()));

        //        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionDetail", "", "closing", ActivityLog.createLogMessage(outputParameters));

        return transactionUseVoucherResponse;
    }

    @Override
    public Transaction getTransactionDetail(String requestId, String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionDetail", "", "opening", ActivityLog.createLogMessage(inputParameters));

        Transaction transactionResponse = null;

        try {
            transactionResponse = transactionGetTransactionDetailAction.execute(requestId, mpTransactionID);
            Integer userType = transactionResponse.getUser().getUserType();
            String userCategoryType = UserCategoryType.NEW_PAYMENT_FLOW.getCode();
            Boolean newPaymentFlow = userCategoryService.isUserTypeInUserCategory(userType, userCategoryType);
            transactionResponse.setNewPaymentFlow(newPaymentFlow);
            Boolean isBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());
            transactionResponse.setBusiness(isBusiness);
            System.out.println("UserCatgory: userType(" + userType + ") - userCategoryType(" + userCategoryType + ") "
                    + "- isBusiness(" + isBusiness + ") - newPaymentFlow(" + newPaymentFlow + ")");
            // Decodifica stringa acquirer
            String encodedSecretKey = transactionResponse.getEncodedSecretKey();

            if (encodedSecretKey == null || encodedSecretKey.equals("")) {

                transactionResponse.setEncodedSecretKey(null);
            }
            else {

                EncryptionAES encryptionAES = new EncryptionAES();
                encryptionAES.loadSecretKey(this.secretKey);
                String decodedSecretKey = encryptionAES.decrypt(encodedSecretKey);
                transactionResponse.setEncodedSecretKey(decodedSecretKey);
            }
        }
        catch (EJBException ex1) {
            //            transactionResponse = new PostPaidGetTransactionDetailResponse();
            //            popPaidGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE);
            ex1.printStackTrace();
        }
        catch (Exception ex2) {
            // TODO Auto-generated catch block
            ex2.printStackTrace();
        }

        //        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        //        outputParameters.add(new Pair<String, String>("statusCode", popPaidGetTransactionDetailResponse.getStatusCode()));

        //        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionDetail", "", "closing", ActivityLog.createLogMessage(outputParameters));

        return transactionResponse;
    }

    @Override
    public TransactionPreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucher(String transactionID, Double amount) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "preAuthorizationConsumeVoucher", "", "opening", ActivityLog.createLogMessage(inputParameters));

        TransactionPreAuthorizationConsumeVoucherResponse response = new TransactionPreAuthorizationConsumeVoucherResponse();

        try {
            response = transactionPreAuthorizationConsumeVoucherAction.execute(transactionID, amount, fidelityService);
        }
        catch (EJBException ex) {
            response.setStatusCode(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        outputParameters.add(new Pair<String, String>("statusCode", response.getStatusCode()));

        if (response.getFidelityConsumeVoucherData() != null) {

            if (response.getFidelityConsumeVoucherData().getAmount() != null) {
                outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.amount", response.getFidelityConsumeVoucherData().getAmount().toString()));
            }
            else {
                outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.amount", "null"));
            }

            outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.transactionID", response.getFidelityConsumeVoucherData().getCsTransactionID()));
            outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.marketingMsg", response.getFidelityConsumeVoucherData().getMarketingMsg()));
            outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.messageCode", response.getFidelityConsumeVoucherData().getMessageCode()));
            outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.preAuthOperationID", response.getFidelityConsumeVoucherData().getPreAuthOperationID()));
            outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.statusCode", response.getFidelityConsumeVoucherData().getStatusCode()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "preAuthorizationConsumeVoucher", "", "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public TransactionCancelPreAuthorizationConsumeVoucherResponse cancelPreAuthorizationConsumeVoucher(String transactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("transactionID", transactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "cancelPreAuthorizationConsumeVoucher", "", "opening",
                ActivityLog.createLogMessage(inputParameters));

        TransactionCancelPreAuthorizationConsumeVoucherResponse response = new TransactionCancelPreAuthorizationConsumeVoucherResponse();

        try {
            response = transactionCancelPreAuthorizationConsumeVoucherAction.execute(transactionID, fidelityService);
        }
        catch (EJBException ex) {
            response.setStatusCode(ResponseHelper.TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        outputParameters.add(new Pair<String, String>("statusCode", response.getStatusCode()));

        if (response.getFidelityConsumeVoucherData() != null) {

            outputParameters.add(new Pair<String, String>("cancelPreAuthorizationConsumeVoucherData.transactionID", response.getFidelityConsumeVoucherData().getCsTransactionID()));
            outputParameters.add(new Pair<String, String>("cancelPreAuthorizationConsumeVoucherData.messageCode", response.getFidelityConsumeVoucherData().getMessageCode()));
            outputParameters.add(new Pair<String, String>("cancelPreAuthorizationConsumeVoucherData.statusCode", response.getFidelityConsumeVoucherData().getStatusCode()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "cancelPreAuthorizationConsumeVoucher", "", "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public TransactionConsumeVoucherPreAuthResponse consumeVoucherPreAuth(String transactionID, Double amount) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("transactionID", transactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "consumeVoucherPreAuth", "", "opening", ActivityLog.createLogMessage(inputParameters));

        TransactionConsumeVoucherPreAuthResponse response = new TransactionConsumeVoucherPreAuthResponse();

        try {
            response = transactionConsumeVoucherPreAuthAction.execute(transactionID, amount, fidelityService);
        }
        catch (EJBException ex) {
            response.setStatusCode(ResponseHelper.TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        outputParameters.add(new Pair<String, String>("statusCode", response.getStatusCode()));

        if (response.getFidelityConsumeVoucherData() != null) {

            outputParameters.add(new Pair<String, String>("transactionConsumeVoucherPreAuthAction.transactionID", response.getFidelityConsumeVoucherData().getCsTransactionID()));
            outputParameters.add(new Pair<String, String>("transactionConsumeVoucherPreAuthAction.messageCode", response.getFidelityConsumeVoucherData().getMessageCode()));
            outputParameters.add(new Pair<String, String>("transactionConsumeVoucherPreAuthAction.statusCode", response.getFidelityConsumeVoucherData().getStatusCode()));
            outputParameters.add(new Pair<String, String>("transactionConsumeVoucherPreAuthAction.marketingMsg", response.getFidelityConsumeVoucherData().getMarketingMsg()));
            outputParameters.add(new Pair<String, String>("transactionConsumeVoucherPreAuthAction.warningMsg", response.getFidelityConsumeVoucherData().getWarningMsg()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "consumeVoucherPreAuth", "", "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public TransactionPreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucherNoTransaction(Long userID, String mpTransactionID, String stationID, Double amount) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("userID", userID.toString()));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "preAuthorizationConsumeVoucher", "", "opening", ActivityLog.createLogMessage(inputParameters));

        TransactionPreAuthorizationConsumeVoucherResponse response = new TransactionPreAuthorizationConsumeVoucherResponse();

        try {
            response = transactionPreAuthorizationConsumeVoucherNoTransactionAction.execute(userID, mpTransactionID, stationID, amount, fidelityService);
        }
        catch (EJBException ex) {
            response.setStatusCode(ResponseHelper.TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        outputParameters.add(new Pair<String, String>("statusCode", response.getStatusCode()));

        if (response.getFidelityConsumeVoucherData() != null) {

            if (response.getFidelityConsumeVoucherData().getAmount() != null) {
                outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.amount", response.getFidelityConsumeVoucherData().getAmount().toString()));
            }
            else {
                outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.amount", "null"));
            }

            outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.transactionID", response.getFidelityConsumeVoucherData().getCsTransactionID()));
            outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.marketingMsg", response.getFidelityConsumeVoucherData().getMarketingMsg()));
            outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.messageCode", response.getFidelityConsumeVoucherData().getMessageCode()));
            outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.preAuthOperationID", response.getFidelityConsumeVoucherData().getPreAuthOperationID()));
            outputParameters.add(new Pair<String, String>("preAuthorizationConsumeVoucherData.statusCode", response.getFidelityConsumeVoucherData().getStatusCode()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "preAuthorizationConsumeVoucher", "", "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public CreateApplePayRefuelResponse createApplePayRefuel(String ticketID, String requestID, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Double amountVoucher, Boolean useVoucher, String applePayPKPaymentToken, String outOfRange, String refuelMode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("pumpNumber", pumpNumber.toString()));
        inputParameters.add(new Pair<String, String>("productID", productID));
        inputParameters.add(new Pair<String, String>("productDescription", productDescription));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        if (amountVoucher != null) {
            inputParameters.add(new Pair<String, String>("amountVoucher", amountVoucher.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("amountVoucher", "null"));
        }
        if (useVoucher != null) {
            inputParameters.add(new Pair<String, String>("useVoucher", useVoucher.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("useVoucher", "null"));
        }
        inputParameters.add(new Pair<String, String>("applePayPKPaymentToken", applePayPKPaymentToken));
        inputParameters.add(new Pair<String, String>("outOfRange", outOfRange));
        inputParameters.add(new Pair<String, String>("refuelMode", refuelMode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createRefuel", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateApplePayRefuelResponse createApplePayRefuelResponse = null;
        try {

            String serverName = this.parametersService.getParamValue(TransactionService.PARAM_SERVER_NAME);
            String currency = this.parametersService.getParamValue(TransactionService.PARAM_UIC);
            String paymentType = this.parametersService.getParamValue(TransactionService.PARAM_PAYMENT_TYPE);
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            Integer statusMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionService.PARAM_STATUS_MAX_ATTEMPTS));
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            String emailSenderAddress = this.parametersService.getParamValue(TransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(TransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(TransactionService.PARAM_PROXY_NO_HOSTS);
            String shopLoginNewFlow = this.parametersService.getParamValue(TransactionService.PARAM_SHOPLOGIN_NEW_FLOW);
            String acquirerID = this.parametersService.getParamValue(TransactionService.PARAM_ACQUIRER_ID);
            Double voucherTransactionMinAmount = Double.valueOf(this.parametersService.getParamValue(TransactionService.PARAM_VOUCHER_TRANSACTION_MIN_AMOUNT));
            String userBlockException = this.parametersService.getParamValue(TransactionService.PARAM_USER_BLOCK_EXCEPTION);
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            createApplePayRefuelResponse = transactionCreateApplePayRefuelAction.execute(ticketID, requestID, stationID, pumpID, pumpNumber, productID, productDescription, amount,
                    amountVoucher, useVoucher, currency, paymentType, applePayPKPaymentToken, outOfRange, refuelMode, shopLoginNewFlow, acquirerID, pinCheckMaxAttempts,
                    statusMaxAttempts, reconciliationMaxAttempts, serverName, voucherTransactionMinAmount, userBlockExceptionList, emailSender, userCategoryService, this,
                    gpService, fidelityService, unavailabilityPeriodService, proxyHost, proxyPort, proxyNoHosts);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createApplePayRefuelResponse = new CreateApplePayRefuelResponse();
            createApplePayRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get GPService object");
            createApplePayRefuelResponse = new CreateApplePayRefuelResponse();
            createApplePayRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            createApplePayRefuelResponse = new CreateApplePayRefuelResponse();
            createApplePayRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", createApplePayRefuelResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createRefuel", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createApplePayRefuelResponse;
    }
    /*
    @Override
    public String setGFGElectronicInvoiceID(String transactionID, String value) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("value", value));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setGFGElectronicInvoiceID", transactionID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = transactionSetGFGElectronicInvoiceIDAction.execute(transactionID, value);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setGFGElectronicInvoiceID", transactionID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
        
    }
    */

    @Override
    public String persistTransactionOperation(String requestID, String transactionID, String code, String message, String status, String remoteTransactionId, String operationType,
            String operationId, Long requestTimestamp, Integer amount) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("code", code));
        inputParameters.add(new Pair<String, String>("message", message));
        inputParameters.add(new Pair<String, String>("status", status));
        inputParameters.add(new Pair<String, String>("remoteTransactionId", remoteTransactionId));
        inputParameters.add(new Pair<String, String>("operationType", operationType));
        inputParameters.add(new Pair<String, String>("operationId", operationId));
        if (requestTimestamp != null) {
            inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("requestTimestamp", "null"));
        }
        if (amount != null) {
            inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("amount", "null"));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistTransactionOperation", requestID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = transactionPersistTransactionOperationAction.execute(requestID, transactionID, code, message, status, remoteTransactionId, operationType,
                    operationId, amount, requestTimestamp);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "persistTransactionOperation", requestID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }
}
