package com.techedge.mp.core.business;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.actions.station.UpdateMasterAction;
import com.techedge.mp.core.actions.station.UpdateStagingAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.station.StationUpdateDetail;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;

/**
 * Session Bean implementation class UserManager
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class StationService implements StationServiceRemote, StationServiceLocal {

    private final static String PARAM_CARTASI_VAULT_BLOCK                           = "CARTASI_VAULT_BLOCK";
    private final static String PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY                 = "CARTASI_VAULT_BLOCK_ATTRIBUTE_KEY";
    private static final String PARAM_EMAIL_SENDER_ADDRESS                          = "EMAIL_SENDER_ADDRESS";
    private static final String PARAM_SMTP_HOST                                     = "SMTP_HOST";
    private static final String PARAM_SMTP_PORT                                     = "SMTP_PORT";
    private final static String PARAM_PROXY_HOST                                    = "PROXY_HOST";
    private final static String PARAM_PROXY_PORT                                    = "PROXY_PORT";
    private final static String PARAM_PROXY_NO_HOSTS                                = "PROXY_NO_HOSTS";
    private final static String PARAM_UPDATE_STATION_LIST_REPORT_INTERNAL_RECIPIENT = "UPDATE_STATION_LIST_REPORT_INTERNAL_RECIPIENT";
    private final static String PARAM_UPDATE_STATION_LIST_REPORT_EXTERNAL_RECIPIENT = "UPDATE_STATION_LIST_REPORT_EXTERNAL_RECIPIENT";
    private final static String PARAM_GROUP_ACQUIRER_CARTASI                        = "GROUP_ACQUIRER_CARTASI";

    @EJB
    UpdateStagingAction         updateStagingAction;

    @EJB
    UpdateMasterAction          updateMasterAction;

    private LoggerService       loggerService                               = null;
    private ParametersService   parametersService                           = null;
    private EmailSenderRemote   emailSender                                 = null;
    private ForecourtInfoServiceRemote forecourtInfoService = null;

    private String              secretKey                                   = null;

    public StationService() throws EJBException {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        String vaultBlock = "";
        String vaultBlockAttributeCryptKey = "";

        try {

            vaultBlock = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK);
            vaultBlockAttributeCryptKey = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }
    }

    @Override
    public String updateMaster(String requestID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateMaster", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
            this.forecourtInfoService = EJBHomeCache.getInstance().getForecourtInfoService();
            String emailSenderAddress = this.parametersService.getParamValue(StationService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(StationService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(StationService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(StationService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(StationService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(StationService.PARAM_PROXY_NO_HOSTS);
            String reportInternalRecipient = this.parametersService.getParamValue(StationService.PARAM_UPDATE_STATION_LIST_REPORT_INTERNAL_RECIPIENT);
            String reportExternalRecipient = this.parametersService.getParamValue(StationService.PARAM_UPDATE_STATION_LIST_REPORT_EXTERNAL_RECIPIENT);
            String groupAcquirer = this.parametersService.getParamValue(StationService.PARAM_GROUP_ACQUIRER_CARTASI);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
            response = updateMasterAction.execute(requestID, secretKey, reportInternalRecipient, reportExternalRecipient, groupAcquirer, emailSender, forecourtInfoService);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateMaster", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String updateStaging(String requestID, ArrayList<StationUpdateDetail> stationList) {

        String stationListString = "";
        int size = 1;

        if (stationList != null) {
            for (StationUpdateDetail station : stationList) {
                String str = "{ ";
                str += "StationID: " + station.getStationID();
                str += ", loyalty: " + station.isLoyalty();
                str += ", business: " + station.isBusiness();
                str += ", voucher payment: " + station.isVoucherPayment();
                str += ", acquirer: " + (station.getAlias() != null && !station.getAlias().isEmpty());
                str += " }";

                if (size > stationList.size()) {
                    str += "; ";
                }

                size++;
                stationListString += str;
            }
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("stationList", stationListString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateStaging", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = updateStagingAction.execute(requestID, stationList);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateStaging", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

}
