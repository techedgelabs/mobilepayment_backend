package com.techedge.mp.core.business;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.actions.poptransaction.v2.ApproveMulticardPopTransactionAction;
import com.techedge.mp.core.actions.poptransaction.v2.ApprovePopTransactionAction;
import com.techedge.mp.core.actions.poptransaction.v2.ApprovePopTransactionBusinessAction;
import com.techedge.mp.core.actions.poptransaction.v2.CreatePopTransactionAction;
import com.techedge.mp.core.actions.poptransaction.v2.GetPopTransactionDetailAction;
import com.techedge.mp.core.actions.poptransaction.v2.GetSourceDetailAction;
import com.techedge.mp.core.actions.poptransaction.v2.GetStationAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GetStationResponse;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.PostPaidTransaction;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.GetSourceDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveMulticardShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@LocalBean
public class PostPaidV2TransactionService implements PostPaidV2TransactionServiceRemote, PostPaidV2TransactionServiceLocal {

    private static final String            PARAM_UIC                                = "UIC";
    private static final String            PARAM_PIN_CHECK_MAX_ATTEMPTS             = "PIN_CHECK_MAX_ATTEMPTS";
    private static final String            PARAM_RECONCILIATION_MAX_ATTEMPTS        = "RECONCILIATION_MAX_ATTEMPTS";
    private static final String            PARAM_RANGE_THRESHOLD                    = "RANGE_THRESHOLD";
    private static final String            PARAM_OUTER_RANGE_THRESHOLD              = "OUTER_RANGE_THRESHOLD";
    private static final String            PARAM_RANGE_THRESHOLD_BLOCKING           = "RANGE_THRESHOLD_BLOCKING";
    private static final String            PARAM_EMAIL_SENDER_ADDRESS               = "EMAIL_SENDER_ADDRESS";
    private static final String            PARAM_SMTP_HOST                          = "SMTP_HOST";
    private static final String            PARAM_SMTP_PORT                          = "SMTP_PORT";
    private final static String            PARAM_PROXY_HOST                         = "PROXY_HOST";
    private final static String            PARAM_PROXY_PORT                         = "PROXY_PORT";
    private final static String            PARAM_PROXY_NO_HOSTS                     = "PROXY_NO_HOSTS";
    private final static String            PARAM_POST_PAID_TRANSACTION_TIMEOUT      = "POST_PAID_TRANSACTION_TIMEOUT";
    private final static String            PARAM_USER_BLOCK_EXCEPTION               = "USER_BLOCK_EXCEPTION";
    private final static String            PARAM_CARTASI_VAULT_BLOCK                = "CARTASI_VAULT_BLOCK";
    private final static String            PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY      = "CARTASI_VAULT_BLOCK_ATTRIBUTE_KEY";
    private final static String            PARAM_STRING_SUBSTITUTION_PATTERN        = "STRING_SUBSTITUTION_PATTERN";
    private final static String            PARAM_RETRY_TIME                         = "RETRY_TIME";
    private final static String            PARAM_GET_LAST_REFUEL_INTERVAL           = "GET_LAST_REFUEL_INTERVAL";
    private final static String            PARAM_STATION_END_REFUEL_OFFSET_VALIDITY = "STATION_END_REFUEL_OFFSET_VALIDITY";
    private final static String            PARAM_STATIONS_CONTROL_GET_LAST_REFUEL   = "STATIONS_CONTROL_GET_LAST_REFUEL";
    private final static String            PARAM_MULTICARD_CURRENCY                 = "MULTICARD_CURRENCY";

    @EJB
    private CreatePopTransactionAction     createPopTransactionAction;

    @EJB
    private ApprovePopTransactionAction    approvePopTransactionAction;

    @EJB
    private ApprovePopTransactionBusinessAction    approvePopTransactionBusinessAction;

    @EJB
    private GetSourceDetailAction          getSourceDetailAction;

    @EJB
    private GetStationAction               getStationsPoPtransaction;
    
    @EJB
    private ApproveMulticardPopTransactionAction approveMulticardPopTransactionAction;
    
    @EJB
    private GetPopTransactionDetailAction getPopTransactionDetailAction;

    private LoggerService                  loggerService                            = null;
    private ParametersService              parametersService                        = null;
    private ForecourtPostPaidServiceRemote forecourtPostPaidServiceRemote           = null;
    private ForecourtInfoServiceRemote     forecourtInfoServiceRemote               = null;
    private FidelityServiceRemote          fidelityService                          = null;
    private EmailSenderRemote              emailSender                              = null;
    private UserCategoryService            userCategoryService                      = null;
    private TransactionService             transactionService                       = null;

    private String                         secretKey                                = null;
    private StringSubstitution             stringSubstitution                       = null;

    /**
     * Default constructor.
     */
    public PostPaidV2TransactionService() {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.forecourtPostPaidServiceRemote = EJBHomeCache.getInstance().getForecourtPostPaidService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.forecourtInfoServiceRemote = EJBHomeCache.getInstance().getForecourtInfoService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        
        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.transactionService = EJBHomeCache.getInstance().getTransactionService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        String vaultBlock = "";
        String vaultBlockAttributeCryptKey = "";
        String pattern = null;

        try {

            vaultBlock = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK);
            vaultBlockAttributeCryptKey = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY);
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeCryptKey + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.secretKey + "'");

        stringSubstitution = new StringSubstitution(pattern);
    }

    @Override
    public PostPaidApproveShopTransactionResponse approvePopTransaction(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentMethodType,
            String encodedPin) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("paymentMethodId", paymentMethodId.toString()));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("encodedPin", encodedPin));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approvePopTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidApproveShopTransactionResponse postPaidApproveShopTransactionResponse = new PostPaidApproveShopTransactionResponse();

        try {
            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(this.secretKey);

            UnavailabilityPeriodService unavailabilityPeriodService = EJBHomeCache.getInstance().getUnavailabilityPeriodService();
            FidelityServiceRemote fidelityService = EJBHomeCache.getInstance().getFidelityService();
            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();

            String emailSenderAddress = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            String userBlockException = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_USER_BLOCK_EXCEPTION);
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            postPaidApproveShopTransactionResponse = approvePopTransactionAction.execute(requestID, ticketID, mpTransactionID, paymentMethodId, paymentMethodType, encodedPin,
                    pinCheckMaxAttempts, userBlockExceptionList, gpService, forecourtPostPaidServiceRemote, fidelityService, userCategoryService, emailSender,
                    unavailabilityPeriodService, proxyHost, proxyPort, proxyNoHosts, encryptionAES, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", postPaidApproveShopTransactionResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approvePopTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return postPaidApproveShopTransactionResponse;
    }

    @Override
    public PostPaidApproveShopTransactionResponse approvePopTransactionBusiness(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentMethodType,
            String encodedPin) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("paymentMethodId", paymentMethodId.toString()));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("encodedPin", encodedPin));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approvePopTransactionBusiness", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidApproveShopTransactionResponse postPaidApproveShopTransactionResponse = new PostPaidApproveShopTransactionResponse();

        try {
            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(this.secretKey);

            UnavailabilityPeriodService unavailabilityPeriodService = EJBHomeCache.getInstance().getUnavailabilityPeriodService();
            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();

            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            String userBlockException = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_USER_BLOCK_EXCEPTION);
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            postPaidApproveShopTransactionResponse = approvePopTransactionBusinessAction.execute(requestID, ticketID, mpTransactionID, paymentMethodId, paymentMethodType, encodedPin,
                    pinCheckMaxAttempts, userBlockExceptionList, gpService, forecourtPostPaidServiceRemote, unavailabilityPeriodService, encryptionAES, fidelityService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", postPaidApproveShopTransactionResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approvePopTransactionBusiness", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return postPaidApproveShopTransactionResponse;
    }
   
    @Override
    public GetSourceDetailResponse createPopTransaction(String requestID, String ticketID, String sourceID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("sourceID", sourceID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createPopTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetSourceDetailResponse getSourceDetailResponse = new GetSourceDetailResponse();

        try {
            UnavailabilityPeriodService unavailabilityPeriodService = EJBHomeCache.getInstance().getUnavailabilityPeriodService();

            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            String userBlockException = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_USER_BLOCK_EXCEPTION);
            Long retryAttempt = Long.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RETRY_TIME));

            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            getSourceDetailResponse = createPopTransactionAction.execute(requestID, ticketID, sourceID, reconciliationMaxAttempts, forecourtPostPaidServiceRemote,
                    unavailabilityPeriodService, userBlockExceptionList, retryAttempt);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getSourceDetailResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getSourceDetailResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            getSourceDetailResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getSourceDetailResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createPopTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getSourceDetailResponse;
    }

    @Override
    public GetSourceDetailResponse getSourceDetail(String requestID, String ticketID, String codeType, String sourceID, Double userPositionLatitude, Double userPositionLongitude) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        String userPositionLatitudeString = null;
        if (userPositionLatitude != null) {
            userPositionLatitudeString = userPositionLatitude.toString();
        }

        String userPositionLongitudeString = null;
        if (userPositionLongitude != null) {
            userPositionLongitudeString = userPositionLongitude.toString();
        }

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("codeType", codeType));
        inputParameters.add(new Pair<String, String>("sourceID", sourceID));
        inputParameters.add(new Pair<String, String>("userPositionLatitudeString", userPositionLatitudeString));
        inputParameters.add(new Pair<String, String>("userPositionLongitudeString", userPositionLongitudeString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getSourceDetail", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetSourceDetailResponse getSourceDetailResponse = new GetSourceDetailResponse();

        try {

            String currency = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_MULTICARD_CURRENCY);
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            Double rangeThreshold = new Double(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RANGE_THRESHOLD));
            Boolean rangeThresholdBlocking = new Boolean(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RANGE_THRESHOLD_BLOCKING));
            Integer transactionTimeout = Integer.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_POST_PAID_TRANSACTION_TIMEOUT));
            Long retryAttempt = Long.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RETRY_TIME));
            Long getLastRefuelInterval = Long.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_GET_LAST_REFUEL_INTERVAL));
            Long endRefuelOffsetValidity = Long.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_STATION_END_REFUEL_OFFSET_VALIDITY));
            Boolean controlGetLastRefuel = new Boolean(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_STATIONS_CONTROL_GET_LAST_REFUEL));

            System.out.println("param range threshold: '" + this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RANGE_THRESHOLD)
                    + "' - param range threshold blocking: '" + this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RANGE_THRESHOLD_BLOCKING) + "'");

            getSourceDetailResponse = getSourceDetailAction.execute(requestID, ticketID, codeType, sourceID, userPositionLatitude, userPositionLongitude, currency,
                    reconciliationMaxAttempts, rangeThreshold, rangeThresholdBlocking, transactionTimeout, retryAttempt, getLastRefuelInterval, endRefuelOffsetValidity, 
                    controlGetLastRefuel, forecourtInfoServiceRemote, forecourtPostPaidServiceRemote, userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getSourceDetailResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getSourceDetail", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getSourceDetailResponse;
    }

    @Override
    public GetStationResponse getStation(String requestID, String ticketID, Double userPositionLatitude, Double userPositionLongitude, String stationID, Boolean clearTransaction, Boolean refresh) {

        String userPositionLatitudeString = "";
        if (userPositionLatitude != null) {
            userPositionLatitudeString = userPositionLatitude.toString();
        }

        String userPositionLongitudeString = "";
        if (userPositionLatitude != null) {
            userPositionLongitudeString = userPositionLongitude.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("userPositionLatitude", userPositionLatitudeString));
        inputParameters.add(new Pair<String, String>("userPositionLongitude", userPositionLongitudeString));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        if (clearTransaction == null) {
            inputParameters.add(new Pair<String, String>("clearTransaction", "null"));
        }
        else {
            inputParameters.add(new Pair<String, String>("clearTransaction", clearTransaction.toString()));
        }
        if (refresh == null) {
            inputParameters.add(new Pair<String, String>("refresh", "null"));
        }
        else {
            inputParameters.add(new Pair<String, String>("refresh", refresh.toString()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "GetStation", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetStationResponse getStationsResponse = null;

        try {

            Double innerRangeThreshold = Double.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RANGE_THRESHOLD));
            Double outerRangeThreshold = Double.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_OUTER_RANGE_THRESHOLD));
            Boolean rangeThresholdBlocking = Boolean.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RANGE_THRESHOLD_BLOCKING));

            getStationsResponse = getStationsPoPtransaction.execute(requestID, ticketID, userPositionLatitude, userPositionLongitude, stationID, clearTransaction, refresh,
                    innerRangeThreshold, outerRangeThreshold, rangeThresholdBlocking, this.transactionService, this.forecourtInfoServiceRemote, this.forecourtPostPaidServiceRemote, this.userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getStationsResponse = new GetStationResponse();
            getStationsResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            getStationsResponse = new GetStationResponse();
            getStationsResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getStationsResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "GetStation", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getStationsResponse;
    }

    @Override
    public GetStationResponse retrieveStation(String requestID, String ticketID, String codeType, String beaconCode, Double userPositionLatitude, Double userPositionLongitude) {

        String userPositionLatitudeString = "";
        if (userPositionLatitude != null) {
            userPositionLatitudeString = userPositionLatitude.toString();
        }

        String userPositionLongitudeString = "";
        if (userPositionLatitude != null) {
            userPositionLongitudeString = userPositionLongitude.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("codeType", codeType));
        inputParameters.add(new Pair<String, String>("beaconCode", beaconCode));
        inputParameters.add(new Pair<String, String>("userPositionLatitude", userPositionLatitudeString));
        inputParameters.add(new Pair<String, String>("userPositionLongitude", userPositionLongitudeString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "GetStation", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetStationResponse getStationsResponse = null;

        try {

            Double innerRangeThreshold = Double.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RANGE_THRESHOLD));
            Double outerRangeThreshold = Double.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_OUTER_RANGE_THRESHOLD));
            Boolean rangeThresholdBlocking = Boolean.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_RANGE_THRESHOLD_BLOCKING));

            getStationsResponse = getStationsPoPtransaction.execute(requestID, ticketID, userPositionLatitude, userPositionLongitude, null, Boolean.FALSE, Boolean.TRUE, innerRangeThreshold,
                    outerRangeThreshold, rangeThresholdBlocking, this.transactionService, this.forecourtInfoServiceRemote, this.forecourtPostPaidServiceRemote, this.userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getStationsResponse = new GetStationResponse();
            getStationsResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            getStationsResponse = new GetStationResponse();
            getStationsResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getStationsResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "GetStation", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getStationsResponse;
    }

    @Override
    public PostPaidApproveMulticardShopTransactionResponse approveMulticardPopTransaction(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId,
            String paymentCryptogram) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("paymentMethodId", paymentMethodId.toString()));
        inputParameters.add(new Pair<String, String>("paymentCryptogram", paymentCryptogram));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approveMulticardPopTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidApproveMulticardShopTransactionResponse postPaidApproveMulticardShopTransactionResponse = new PostPaidApproveMulticardShopTransactionResponse();

        try {
            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(this.secretKey);

            UnavailabilityPeriodService unavailabilityPeriodService = EJBHomeCache.getInstance().getUnavailabilityPeriodService();
            FidelityServiceRemote fidelityService = EJBHomeCache.getInstance().getFidelityService();
            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();

            String emailSenderAddress = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            String userBlockException = this.parametersService.getParamValue(PostPaidV2TransactionService.PARAM_USER_BLOCK_EXCEPTION);
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            postPaidApproveMulticardShopTransactionResponse = approveMulticardPopTransactionAction.execute(requestID, ticketID, mpTransactionID, paymentMethodId, paymentCryptogram,
                    pinCheckMaxAttempts, userBlockExceptionList, gpService, forecourtPostPaidServiceRemote, fidelityService, userCategoryService, emailSender,
                    unavailabilityPeriodService, proxyHost, proxyPort, proxyNoHosts, encryptionAES, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", postPaidApproveMulticardShopTransactionResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approveMulticardPopTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return postPaidApproveMulticardShopTransactionResponse;
    }

    @Override
    public PostPaidTransaction getPopTransactionDetail(String requestID, String mpTransactionID) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPopTransactionDetail", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidTransaction postPaidTransaction = null;

        try {

            postPaidTransaction = getPopTransactionDetailAction.execute(requestID, mpTransactionID);
        }
        catch (EJBException ex) {
            ex.printStackTrace();
            return null;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        if (postPaidTransaction != null) {
            outputParameters.add(new Pair<String, String>("PostPaidTransactionStatus", postPaidTransaction.getMpTransactionStatus()));
        }
        else {
            outputParameters.add(new Pair<String, String>("PostPaidTransactionStatus", "null"));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPopTransactionDetail", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return postPaidTransaction;
    }

}
