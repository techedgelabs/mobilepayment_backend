package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionOperation;

@Entity
@Table(name = "TRANSACTION_OPERATION")
/*
 * @NamedQueries({
 * 
 * @NamedQuery(name = "TransactionEventBean.findTransactionEventByTransaction", query = "select t from TransactionEventBean t where t.transactionBean = :transactionBean"),
 * 
 * @NamedQuery(name = "TransactionEventBean.findTransactionEventById", query = "select t from TransactionEventBean t where t.id = :id")
 * })
 */
public class TransactionOperationBean {

    //public static final String FIND_BY_TRANSACTION = "TransactionEventBean.findTransactionEventByTransaction";
    //public static final String FIND_BY_ID = "TransactionEventBean.findTransactionEventById";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                 id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    private TransactionBean transactionBean;
    
    @Column(name = "operation_type", nullable = false)
    private String               operationType;

    @Column(name = "sequence_id", nullable = false)
    private Integer              sequenceID;

    @Column(name = "operation_id", nullable = false)
    private String               operationId;

    @Column(name = "request_timestamp", nullable = false)
    private Long                 requestTimestamp;

    @Column(name = "remote_transaction_id", nullable = false)
    private String               remoteTransactionId;

    @Column(name = "status", nullable = false)
    private String               status;

    @Column(name = "code", nullable = false)
    private String               code;

    @Column(name = "message", nullable = false)
    private String               message;
    
    @Column(name = "amount", nullable = true)
    private Integer              amount;

    public TransactionOperationBean() {}
    
    public TransactionOperation toTransactionOperation() {
        
        TransactionOperation transactionOperation = new TransactionOperation();
        transactionOperation.setId(this.id);
        transactionOperation.setOperationType(this.operationType);
        transactionOperation.setSequenceID(this.sequenceID);
        transactionOperation.setOperationId(this.operationId);
        transactionOperation.setRequestTimestamp(this.requestTimestamp);
        transactionOperation.setRemoteTransactionId(this.remoteTransactionId);
        transactionOperation.setStatus(this.status);
        transactionOperation.setCode(this.code);
        transactionOperation.setMessage(this.message);
        transactionOperation.setAmount(this.amount);
        return transactionOperation;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TransactionBean getTransactionBean() {
        return transactionBean;
    }

    public void setTransactionBean(TransactionBean transactionBean) {
        this.transactionBean = transactionBean;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Integer getSequenceID() {
        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {
        this.sequenceID = sequenceID;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getRemoteTransactionId() {
        return remoteTransactionId;
    }

    public void setRemoteTransactionId(String remoteTransactionId) {
        this.remoteTransactionId = remoteTransactionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

}
