package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "PROMO_CODE_MODE")
@NamedQueries({ @NamedQuery(name = "PromoCodeModeBean.findByModeAndPromoCode", query = "select pcm from PromoCodeModeBean pcm where pcm.mode = :mode and pcm.promoCode = :promoCode") })
public class PromoCodeModeBean {

    public static final String FIND_BY_MODE_AND_PROMOCODE = "PromoCodeModeBean.findByModeAndPromoCode";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "promo_code", nullable = true, length = 10)
    private String             promoCode;

    @Column(name = "mode", nullable = false)
    private String             mode;

    public PromoCodeModeBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

}
