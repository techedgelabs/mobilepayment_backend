package com.techedge.mp.core.business.model.station;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "STATIONS_STAGE")
@NamedQueries({ 
    @NamedQuery(name = "StationStageBean.findByRequest", query = "select s from StationStageBean s where s.requestID = :requestID"),
    @NamedQuery(name = "StationStageBean.findByNotProcessed", query = "select s from StationStageBean s where s.processed = 0 order by s.requestID"),
    @NamedQuery(name = "StationStageBean.findByNotProcessedAndRequest", query = "select s from StationStageBean s where s.processed = 0 and s.requestID = :requestID")
})
public class StationStageBean {

    public static final String FIND_BY_REQUEST                   = "StationStageBean.findByRequest";
    public static final String FIND_BY_NOT_PROCESSED_AND_REQUEST = "StationStageBean.findByNotProcessedAndRequest";
    public static final String FIND_BY_NOT_PROCESSED             = "StationStageBean.findByNotProcessed";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "station_id")
    private String             stationID;

    @Column(name = "address", nullable = true)
    private String             address;

    @Column(name = "city", nullable = true)
    private String             city;

    @Column(name = "country", nullable = true)
    private String             country;

    @Column(name = "province", nullable = true)
    private String             province;

    @Column(name = "latitude", nullable = true)
    private Double             latitude;

    @Column(name = "longitude", nullable = true)
    private Double             longitude;

    @Column(name = "loyalty")
    private Boolean            loyalty;

    @Column(name = "business")
    private Boolean            business;

    @Column(name = "voucher_payment")
    private Boolean            voucherPayment;

    @Column(name = "alias", nullable = true)
    private String             alias;

    @Column(name = "mac_key", nullable = true)
    private String             macKey;

    @Column(name = "temporarily_closed")
    private Boolean            temporarilyClosed;

    @Column(name = "validity_date_details")
    private Date               validityDateDetails;

    @Column(name = "processed")
    private Boolean            processed;

    @Column(name = "request_id")
    private String             requestID;

    public StationStageBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Boolean getLoyalty() {
        return loyalty;
    }

    public void setLoyalty(Boolean loyalty) {
        this.loyalty = loyalty;
    }

    public Boolean getBusiness() {
        return business;
    }

    public void setBusiness(Boolean business) {
        this.business = business;
    }

    public Boolean getVoucherPayment() {
        return voucherPayment;
    }

    public void setVoucherPayment(Boolean voucherPayment) {
        this.voucherPayment = voucherPayment;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getMacKey() {
        return macKey;
    }

    public void setMacKey(String macKey) {
        this.macKey = macKey;
    }

    public Boolean getTemporarilyClosed() {
        return temporarilyClosed;
    }

    public void setTemporarilyClosed(Boolean temporarilyClosed) {
        this.temporarilyClosed = temporarilyClosed;
    }

    public Date getValidityDateDetails() {
        return validityDateDetails;
    }

    public void setValidityDateDetails(Date validityDateDetails) {
        this.validityDateDetails = validityDateDetails;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }
}
