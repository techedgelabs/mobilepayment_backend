package com.techedge.mp.core.business.model.postpaid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidTransactionPaymentEvent;

@Entity
@Table(name = "POSTPAIDTRANSACTIONS_PAYMENT_EVENT_HISTORY")
//@NamedQueries({
//	@NamedQuery(name="TransactionBean.findTransactionByID", query="select t from TransactionBean t where t.transactionID = :transactionID"),
//	@NamedQuery(name="TransactionBean.findTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean"),
//	@NamedQuery(name="TransactionBean.findTransactionByUserAndDate", query="select t from TransactionBean t where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),
//	@NamedQuery(name="TransactionBean.findTransactionSuccessfulByUserAndDate", query="select t from TransactionBean t where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) and t.finalStatusType = 'SUCCESSFUL' order by t.creationTimestamp desc"),
//	//@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and (( t.finalStatusType is null ) or ( t.finalStatusType <> 'FAILED' and t.finalStatusType <> 'SUCCESSFUL' and t.finalStatusType <> 'MISSING_NOTIFICATION' ))")
//	@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and t.confirmed = false"),
//	@NamedQuery(name="TransactionBean.findActiveTransaction", query="select t from TransactionBean t where t.confirmed = false"),
//	@NamedQuery(name="TransactionBean.findActiveTransactionNew", query="select t from TransactionBean t where t.finalStatusType = null"),
//	@NamedQuery(name="TransactionBean.findTransactionByFinalStatusType", query="select t from TransactionBean t where t.finalStatusType = :finalStatusType"),
//	@NamedQuery(name="TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus", query="select t from TransactionBean t where t.finalStatusType = :finalStatusType and t.GFGNotification = :gfgNotification"),
//})
public class PostPaidTransactionPaymentEventHistoryBean {

    //	public static final String FIND_BY_ID = "TransactionBean.findTransactionByID";
    //	public static final String FIND_BY_USER = "TransactionBean.findTransactionByUser";
    //	public static final String FIND_ACTIVE_BY_USER = "TransactionBean.findActiveTransactionByUser";
    //	public static final String FIND_ACTIVE = "TransactionBean.findActiveTransaction";
    //	public static final String FIND_ACTIVE_NEW = "TransactionBean.findActiveTransactionNew";
    //	public static final String FIND_BY_FINAL_STATUS_TYPE = "TransactionBean.findTransactionByFinalStatusType";
    //	public static final String FIND_BY_FINAL_STATUS_TYPE_AND_NOTIFICATION = "TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus";
    //	public static final String FIND_BY_USER_AND_DATE = "TransactionBean.findTransactionByUserAndDate";
    //	public static final String FIND_SUCCESSFUL_BY_USER_AND_DATE = "TransactionBean.findTransactionSuccessfulByUserAndDate";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                           id;

    @Column(name = "sequence", nullable = true)
    private Integer                        sequence;

    @Column(name = "eventType", nullable = true)
    private String                         eventType;

    @Column(name = "transactionResult", nullable = true)
    private String                         transactionResult;

    @Column(name = "authorizationCode", nullable = true)
    private String                         authorizationCode;

    @Column(name = "errorCode", nullable = true)
    private String                         errorCode;

    @Column(name = "errorDescription", nullable = true)
    private String                         errorDescription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactionID", nullable = false)
    private PostPaidTransactionHistoryBean postPaidTransactionHistoryBean;

    public PostPaidTransactionPaymentEventHistoryBean() {}

    public PostPaidTransactionPaymentEventHistoryBean(PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean) {
        this.sequence = postPaidTransactionPaymentEventBean.getSequence();
        this.eventType = postPaidTransactionPaymentEventBean.getEventType();
        this.transactionResult = postPaidTransactionPaymentEventBean.getTransactionResult();
        this.authorizationCode = postPaidTransactionPaymentEventBean.getAuthorizationCode();
        this.errorCode = postPaidTransactionPaymentEventBean.getErrorCode();
        this.errorDescription = postPaidTransactionPaymentEventBean.getErrorDescription();

    }

    public PostPaidTransactionPaymentEvent toPostPaidTransactionPaymentEvent() {

        PostPaidTransactionPaymentEvent postPaidTransactionPaymentEvent = new PostPaidTransactionPaymentEvent();
        postPaidTransactionPaymentEvent.setSequence(this.sequence);
        postPaidTransactionPaymentEvent.setEventType(this.eventType);
        postPaidTransactionPaymentEvent.setTransactionResult(this.transactionResult);
        postPaidTransactionPaymentEvent.setAuthorizationCode(this.authorizationCode);
        postPaidTransactionPaymentEvent.setErrorCode(this.errorCode);
        postPaidTransactionPaymentEvent.setErrorDescription(this.errorDescription);
        return postPaidTransactionPaymentEvent;

    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {
        this.transactionResult = transactionResult;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public PostPaidTransactionHistoryBean getPostPaidTransactionHistoryBean() {
        return postPaidTransactionHistoryBean;
    }

    public void setPostPaidTransactionHistoryBean(PostPaidTransactionHistoryBean postPaidTransactionHistoryBean) {
        this.postPaidTransactionHistoryBean = postPaidTransactionHistoryBean;
    }

}
