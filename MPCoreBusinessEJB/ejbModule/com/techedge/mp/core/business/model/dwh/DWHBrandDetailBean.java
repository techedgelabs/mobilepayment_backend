package com.techedge.mp.core.business.model.dwh;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.BrandDataDetail;
import com.techedge.mp.dwh.adapter.interfaces.BrandDetail;

@Entity
@Table(name = "DWH_BRAND")
@NamedQueries({ @NamedQuery(name = "DWHBrandDetailBean.findById", query = "select bd from DWHBrandDetailBean bd where bd.id = :id"),
        @NamedQuery(name = "DWHBrandDetailBean.findAll", query = "select cd from DWHBrandDetailBean cd") })
public class DWHBrandDetailBean {

    public static final String FIND_By_ID = "DWHBrandDetailBean.findById";
    public static final String FIND_ALL   = "DWHBrandDetailBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "creation_timestamp")
    private Date               creationTimestamp;

    @Column(name = "brand_id")
    private Integer            brandId;

    @Column(name = "brand")
    private String             brand;
    
    @Column(name = "brand_url")
    private String             brandUrl;

    public DWHBrandDetailBean() {}

    public DWHBrandDetailBean(BrandDataDetail brandDataDetail) {
        this.brandId = brandDataDetail.getBrandId();
        this.brand = brandDataDetail.getBrand();
        this.brandUrl = brandDataDetail.getBrandUrl();
    }

    public BrandDataDetail toBrandDataDetail() {
        BrandDataDetail brandDataDetail = new BrandDataDetail();
        brandDataDetail.setBrandId(this.brandId);
        brandDataDetail.setBrand(this.brand);
        brandDataDetail.setBrandUrl(this.brandUrl);

        return brandDataDetail;
    }

    public Long getId() {
        return id;
    }

    public String getBrandUrl() {
        return brandUrl;
    }

    public void setBrandUrl(String brandUrl) {
        this.brandUrl = brandUrl;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

}
