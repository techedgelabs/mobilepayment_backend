package com.techedge.mp.core.business.model.parking;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.parking.ParkingTransactionItemEvent;

@Entity
@Table(name = "PARKING_TRANSACTION_ITEM_EVENT")
@NamedQueries({
//    @NamedQuery(name = "TransactionEventBean.findTransactionEventByTransaction", query = "select t from TransactionEventBean t where t.transactionBean = :transactionBean"),
//    @NamedQuery(name = "TransactionEventBean.findTransactionEventById", query = "select t from TransactionEventBean t where t.id = :id")
})
public class ParkingTransactionItemEventBean {

    //public static final String FIND_BY_TRANSACTION = "TransactionEventBean.findTransactionEventByTransaction";
    //public static final String FIND_BY_ID = "TransactionEventBean.findTransactionEventById";
    //public static final String FIND_BY_TRANSACTION_AND_STATUS_ID = "TransactionEventBean.findTransactionStatusByTransactionAndStatusID";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parking_transaction_item_id", nullable = false)
    private ParkingTransactionItemBean    parkingTransactionItemBean;

    @Column(name = "sequence_id", nullable = false)
    private Integer            sequenceID;

    @Column(name = "event_type", nullable = false)
    private String             eventType;

    @Column(name = "event_amount", nullable = false)
    private Double             eventAmount;

    @Column(name = "transaction_result", nullable = false)
    private String             transactionResult;

    @Column(name = "error_code", nullable = true)
    private String             errorCode;

    @Column(name = "error_description", nullable = true)
    private String             errorDescription;

    public ParkingTransactionItemEventBean() {}

    public ParkingTransactionItemEventBean(ParkingTransactionItemEvent parkingTransactionItemEvent) {

        this.id = parkingTransactionItemEvent.getId();
        this.sequenceID = parkingTransactionItemEvent.getSequenceID();
        this.eventType = parkingTransactionItemEvent.getEventType();
        this.eventAmount = parkingTransactionItemEvent.getEventAmount();
        this.transactionResult = parkingTransactionItemEvent.getTransactionResult();
        this.errorCode = parkingTransactionItemEvent.getErrorCode();
        this.errorDescription = parkingTransactionItemEvent.getErrorDescription();
    }


    public ParkingTransactionItemEvent toParkingTransactionItemEvent() {

        ParkingTransactionItemEvent parkingTransactionItemEvent = new ParkingTransactionItemEvent();
        parkingTransactionItemEvent.setId(this.id);
        parkingTransactionItemEvent.setSequenceID(this.sequenceID);
        parkingTransactionItemEvent.setEventType(this.eventType);
        parkingTransactionItemEvent.setEventAmount(this.eventAmount);
        parkingTransactionItemEvent.setTransactionResult(this.transactionResult);
        parkingTransactionItemEvent.setErrorCode(this.errorCode);
        parkingTransactionItemEvent.setErrorDescription(this.errorDescription);
        return parkingTransactionItemEvent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public ParkingTransactionItemBean getParkingTransactionItemBean() {
        return parkingTransactionItemBean;
    }

    public void setParkingTransactionItemBean(ParkingTransactionItemBean parkingTransactionItemBean) {
        this.parkingTransactionItemBean = parkingTransactionItemBean;
    }

    public Integer getSequenceID() {
        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {
        this.sequenceID = sequenceID;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Double getEventAmount() {
        return eventAmount;
    }

    public void setEventAmount(Double eventAmount) {
        this.eventAmount = eventAmount;
    }

    public String getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {
        this.transactionResult = transactionResult;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

}
