package com.techedge.mp.core.business.model.loyalty;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CHECK_PAYMENT")


public class CheckPaymentBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                   id;

    @Column(name = "operation_id", nullable = false)
    private String                 operationID;

    @Column(name = "request_timestamp", nullable = false)
    private Date                   requestTimestamp;

    @Column(name = "station_id", nullable = true)
    private String            stationID;

    @Column(name = "pump_number")
    private String                 pumpNumber;

    @Column(name = "status_code")
    private String                 statusCode;
    
    @Column(name = "status_message")
    private String                 statusMessage;

    public CheckPaymentBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(String pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
