package com.techedge.mp.core.business.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;


@Entity
@Table(name="ACTIVITY_LOG")
@NamedQuery(name="ActivityLogBean.search", query="select l from ActivityLogBean l where l.timestamp >= :start and l.timestamp <= :end and l.level >= :minLevel and l.level <= :maxLevel and l.source LIKE :source and l.groupId LIKE :groupId and l.phaseId LIKE :phaseId and l.message LIKE :messagePattern ORDER BY timestamp")
public class ActivityLogBean {
	
	public static final String SEARCH = "ActivityLogBean.search";

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	
	@Column(name="timestamp", nullable=true)
	private Date timestamp;
	
	@Column(name="level", nullable=true)
	private ErrorLevel level;
	
	@Column(name="source", nullable=true)
	private String source;
	
	@Column(name="group_id", nullable=true)
	private String groupId;
	
	@Column(name="phase_id", nullable=true)
	private String phaseId;
	
	@Lob @Basic(fetch=FetchType.LAZY)
	@Column(name="message", nullable=true)
	private String message;
		
	public ActivityLogBean() {
	}
	
	public ActivityLogBean( Date timestamp,
							ErrorLevel level,
							String source,
							String groupId,
							String phaseId,
							String message ) {
		
		this.timestamp = timestamp;
		this.level = level;
		this.source = source;
		this.groupId = groupId;
		this.phaseId = phaseId;
		this.message = message;
	}
	
	public ActivityLogBean( ActivityLog activityLog ) {

		this.timestamp = activityLog.getTimestamp();
		this.level = activityLog.getLevel();
		this.source = activityLog.getSource();
		this.groupId = activityLog.getGroupId();
		this.phaseId = activityLog.getPhaseId();
		this.message = activityLog.getMessage();
	}
	
	public ActivityLog toActivityLog() {

		ActivityLog activityLog = new ActivityLog();
		
		activityLog.setTimestamp(this.timestamp);
		activityLog.setLevel(this.level);
		activityLog.setSource(this.source);
		activityLog.setGroupId(this.groupId);
		activityLog.setPhaseId(this.phaseId);
		activityLog.setMessage(this.message);
		
		return activityLog;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public ErrorLevel getLevel() {
		return level;
	}
	public void setLevel(ErrorLevel level) {
		this.level = level;
	}

	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getPhaseId() {
		return phaseId;
	}
	public void setPhaseId(String phaseId) {
		this.phaseId = phaseId;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String toString() {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss.SSS");
		
		return simpleDateFormat.format(this.timestamp) + " " + this.level.toString() + " [" + this.source + "] " + this.groupId + " " + this.phaseId + " " + this.message;
	}
}
