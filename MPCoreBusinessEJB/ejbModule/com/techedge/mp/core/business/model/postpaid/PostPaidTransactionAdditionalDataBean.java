package com.techedge.mp.core.business.model.postpaid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionAdditionalData;

@Entity
@Table(name = "POSTPAIDTRANSACTION_ADDITIONAL_DATA")
/*@NamedQueries({
    @NamedQuery(name = "TransactionEventBean.findTransactionEventByTransaction", query = "select t from TransactionEventBean t where t.transactionBean = :transactionBean"),
    @NamedQuery(name = "TransactionEventBean.findTransactionEventById", query = "select t from TransactionEventBean t where t.id = :id")
})*/
public class PostPaidTransactionAdditionalDataBean {

    //public static final String FIND_BY_TRANSACTION = "TransactionEventBean.findTransactionEventByTransaction";
    //public static final String FIND_BY_ID = "TransactionEventBean.findTransactionEventById";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    private PostPaidTransactionBean    postPaidTransactionBean;

    @Column(name = "data_key", nullable = true)
    private String             dataKey;

    @Column(name = "data_value", nullable = true)
    private String             dataValue;

    public PostPaidTransactionAdditionalDataBean() {}

    public PostPaidTransactionAdditionalDataBean(PostPaidTransactionAdditionalData postPaidTransactionAdditionalData) {

        this.id = postPaidTransactionAdditionalData.getId();
        this.dataKey = postPaidTransactionAdditionalData.getDataKey();
        this.dataValue = postPaidTransactionAdditionalData.getDataValue();
    }
    
    public PostPaidTransactionAdditionalDataBean(PostPaidTransactionHistoryAdditionalDataBean postPaidTransactionHistoryAdditionalDataBean) {

        this.id = postPaidTransactionHistoryAdditionalDataBean.getId();
        this.dataKey = postPaidTransactionHistoryAdditionalDataBean.getDataKey();
        this.dataValue = postPaidTransactionHistoryAdditionalDataBean.getDataValue();
    }

    public PostPaidTransactionAdditionalData toPostPaidTransactionAdditionalData() {

        PostPaidTransactionAdditionalData postPaidTransactionAdditionalData = new PostPaidTransactionAdditionalData();
        postPaidTransactionAdditionalData.setId(this.id);
        postPaidTransactionAdditionalData.setDataKey(this.dataKey);
        postPaidTransactionAdditionalData.setDataValue(this.dataValue);
        return postPaidTransactionAdditionalData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PostPaidTransactionBean getPostPaidTransactionBean() {
        return postPaidTransactionBean;
    }

    public void setPostPaidTransactionBean(PostPaidTransactionBean postPaidTransactionBean) {
        this.postPaidTransactionBean = postPaidTransactionBean;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }

}
