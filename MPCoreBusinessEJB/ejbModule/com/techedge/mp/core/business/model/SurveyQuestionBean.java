package com.techedge.mp.core.business.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="SURVEY_QUESTION")
@NamedQuery(name = "SurveyQuestionBean.findByCode", query = "select s from SurveyQuestionBean s where code = :code")
public class SurveyQuestionBean {
	
    public static final String      FIND_BY_CODE        = "SurveyQuestionBean.findByCode";

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	
    @Column(name="code", nullable=true)
    private String code;
    
    @Column(name="text", nullable=true)
    private String text;
    
    @Column(name="type", nullable=true)
    private String type;
    
    @Column(name="sequence", nullable = false, length = 1)
    private Integer sequence;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "survey_id", nullable = false)
    private SurveyBean           surveyBean;
	
		
	public SurveyQuestionBean() {
	}


    /**
     * @return the id
     */
    public long getId() {
        return id;
    }


    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }


    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }


    /**
     * @return the text
     */
    public String getText() {
        return text;
    }


    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }


    /**
     * @return the type
     */
    public String getType() {
        return type;
    }


    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }


    /**
     * @return the sequence
     */
    public Integer getSequence() {
        return sequence;
    }


    /**
     * @param sequence the sequence to set
     */
    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }


    /**
     * @return the surveyBean
     */
    public SurveyBean getSurveyBean() {
        return surveyBean;
    }


    /**
     * @param surveyBean the surveyBean to set
     */
    public void setSurveyBean(SurveyBean surveyBean) {
        this.surveyBean = surveyBean;
    }
	

	
	
}
