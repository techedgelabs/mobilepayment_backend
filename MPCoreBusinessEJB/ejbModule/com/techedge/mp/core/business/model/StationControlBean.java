package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.StationControlType;

@Entity
@Table(name = "STATIONS_CONTROL")
@NamedQueries({ 
    @NamedQuery(name = "StationControlBean.findControlByStation", query = "select sc from StationControlBean sc where sc.stationBean = :stationBean and sc.control = :control")
})
public class StationControlBean {

    public static final String             FIND_BY_STATION                                           = "StationControlBean.findControlByStation";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long               id;

    @OneToOne
    @JoinColumn(name = "station_id", nullable = false)
    private StationBean                    stationBean;

    @Column(name = "control", nullable = false)
    private String             control;

    @Column(name = "control_timestamp", nullable = true)
    private Date            controlTimestamp;


    public StationControlBean() {}


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public StationBean getStationBean() {
        return stationBean;
    }


    public void setStationBean(StationBean stationBean) {
        this.stationBean = stationBean;
    }


    public StationControlType getActivity() {
        StationControlType type = StationControlType.getType(this.control);
        return type;
    }
    

    public void setControl(StationControlType type) {
        this.control = type.getCode();
    }


    public Date getControlTimestamp() {
        return controlTimestamp;
    }


    public void setControlTimestamp(Date controlTimestamp) {
        this.controlTimestamp = controlTimestamp;
    }


}
