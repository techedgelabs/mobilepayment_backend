package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PROMOTION_USERS_ONHOLD")
@NamedQueries({ 
    @NamedQuery(name = "PromoUsersOnHoldBean.findUsersOnHold", query = "select p from PromoUsersOnHoldBean p where p.promotionBean != null and p.status = 1 and userBean != null"),
    @NamedQuery(name = "PromoUsersOnHoldBean.findByPromotion", query = "select p from PromoUsersOnHoldBean p where p.promotionBean = :promotionBean and p.status = 1 and userBean != null")
})
public class PromoUsersOnHoldBean {

    public static final String FIND_USERS_ON_HOLD = "PromoUsersOnHoldBean.findUsersOnHold";
    public static final String FIND_BY_PROMOTION = "PromoUsersOnHoldBean.findByPromotion";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "promotion_id", nullable = false)
    private PromotionBean      promotionBean;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", nullable = false)
    private UserBean           userBean;

    @Column(name = "inserted_data", nullable = false)
    private Date               insertedData;

    @Column(name = "status", nullable = false)
    private Integer            status;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the promotion
     */
    public PromotionBean getPromotionBean() {
        return promotionBean;
    }

    /**
     * @param promotion
     *            the promotion to set
     */
    public void setPromotionBean(PromotionBean promotionBean) {
        this.promotionBean = promotionBean;
    }

    /**
     * @return the user
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param user
     *            the user to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * @return the insertedData
     */
    public Date getInsertedData() {
        return insertedData;
    }

    /**
     * @param insertedData
     *            the insertedData to set
     */
    public void setInsertedData(Date insertedData) {
        this.insertedData = insertedData;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

}
