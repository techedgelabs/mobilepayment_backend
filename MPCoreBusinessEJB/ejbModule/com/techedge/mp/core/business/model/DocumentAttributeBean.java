package com.techedge.mp.core.business.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "DOCUMENTATTRIBUTE")
@NamedQueries({ @NamedQuery(name = "DocumentAttribute.findDocumentAttributeCheckKey", query = "select p from DocumentAttributeBean p where p.checkKey = :checkKey") })
public class DocumentAttributeBean {

    public static final String FIND_BY_CHECKKEY = "DocumentAttribute.findDocumentAttributeCheckKey";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "checkKey", nullable = false)
    private String             checkKey;

    @Column(name = "position", nullable = false)
    private Integer            position;

    @Column(name = "mandatory", nullable = false)
    private boolean            mandatory;

    @Column(name = "conditionText", nullable = false)
    private String             conditionText;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "extendedConditionText", nullable = true)
    private String             extendedConditionText;
    
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "subTitle", nullable = true)
    private String             subTitle;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCheckKey() {
        return checkKey;
    }

    public void setCheckKey(String checkKey) {
        this.checkKey = checkKey;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public String getConditionText() {
        return conditionText;
    }

    public void setConditionText(String conditionText) {
        this.conditionText = conditionText;
    }

    public String getExtendedConditionText() {
        return extendedConditionText;
    }

    public void setExtendedConditionText(String extendedConditionText) {
        this.extendedConditionText = extendedConditionText;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

}
