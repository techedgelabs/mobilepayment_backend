package com.techedge.mp.core.business.model.postpaid;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCreditsVoucher;

@Entity
@Table(name = "POSTPAIDLOADLOYALTYCREDITS")
@NamedQueries({
    @NamedQuery(name = "PostPaidLoadLoyaltyCreditsBean.findPostPaidLoadLoyaltyCreditsBeanByStatusCode", query = "select l from PostPaidLoadLoyaltyCreditsBean l where l.statusCode = :statusCode ")
})
public class PostPaidLoadLoyaltyCreditsBean {
    
    public static final String FIND_BY_STATUS_CODE = "PostPaidLoadLoyaltyCreditsBean.findPostPaidLoadLoyaltyCreditsBeanByStatusCode";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                    id;

    @Column(name = "cs_transaction_id", nullable = true)
    private String                  csTransactionID;

    @Column(name = "operation_id", nullable = true)
    private String                  operationID;

    @Column(name = "operation_id_reversed", nullable = true)
    private String                                operationIDReversed;
    
    @Column(name = "operation_type", nullable = true)
    private String                  operationType;

    @Column(name = "request_timestamp", nullable = true)
    private Long                    requestTimestamp;

    @Column(name = "marketing_msg", nullable = true, length = 500)
    private String                  marketingMsg;

    @Column(name = "warning_msg", nullable = true)
    private String                                warningMsg;

    @Column(name = "message_code", nullable = true)
    private String                  messageCode;

    @Column(name = "status_code", nullable = true)
    private String                  statusCode;

    @Column(name = "credits", nullable = true)
    private Integer                 credits;

    @Column(name = "balance", nullable = true)
    private Integer                 balance;

    @Column(name = "balance_amount", nullable = true)
    private Double                  balanceAmount;

    @Column(name = "card_code_issuer", nullable = true)
    private String                  cardCodeIssuer;

    @Column(name = "ean_code", nullable = true)
    private String                  eanCode;

    @Column(name = "card_status", nullable = true)
    private String                  cardStatus;

    @Column(name = "card_type", nullable = true)
    private String                  cardType;

    @Column(name = "card_classification", nullable = true)
    private String                  cardClassification;

    @Column(name = "reconciled", nullable = true)
    private Boolean                         reconciled = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "postPaidTransactionBean", nullable = false)
    private PostPaidTransactionBean postPaidTransactionBean;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidLoadLoyaltyCreditsBean")
    private Set<PostPaidLoadLoyaltyCreditsVoucherBean> postPaidLoadLoyaltyCreditsVoucherBean = new HashSet<PostPaidLoadLoyaltyCreditsVoucherBean>(0);

    public PostPaidLoadLoyaltyCreditsBean() {}

    public PostPaidLoadLoyaltyCreditsBean(PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits) {

        this.id = postPaidLoadLoyaltyCredits.getId();
        this.csTransactionID = postPaidLoadLoyaltyCredits.getCsTransactionID();
        this.operationID = postPaidLoadLoyaltyCredits.getOperationID();
        this.operationIDReversed = postPaidLoadLoyaltyCredits.getOperationIDReversed();
        this.operationType = postPaidLoadLoyaltyCredits.getOperationType();
        this.requestTimestamp = postPaidLoadLoyaltyCredits.getRequestTimestamp();
        this.marketingMsg = postPaidLoadLoyaltyCredits.getMarketingMsg();
        this.warningMsg = postPaidLoadLoyaltyCredits.getWarningMsg();
        this.messageCode = postPaidLoadLoyaltyCredits.getMessageCode();
        this.statusCode = postPaidLoadLoyaltyCredits.getStatusCode();
        this.credits = postPaidLoadLoyaltyCredits.getCredits();
        this.balance = postPaidLoadLoyaltyCredits.getBalance();
        this.balanceAmount = postPaidLoadLoyaltyCredits.getBalanceAmount();
        this.cardCodeIssuer = postPaidLoadLoyaltyCredits.getCardCodeIssuer();
        this.eanCode = postPaidLoadLoyaltyCredits.getEanCode();
        this.cardStatus = postPaidLoadLoyaltyCredits.getCardStatus();
        this.cardType = postPaidLoadLoyaltyCredits.getCardType();
        this.cardClassification = postPaidLoadLoyaltyCredits.getCardClassification();
        this.reconciled = postPaidLoadLoyaltyCredits.getReconciled();
        
        if (postPaidLoadLoyaltyCredits.getPostPaidLoadLoyaltyCreditsVoucherList() != null) {
            for (PostPaidLoadLoyaltyCreditsVoucher postPaidLoadLoyaltyCreditsVoucher : postPaidLoadLoyaltyCredits.getPostPaidLoadLoyaltyCreditsVoucherList()) {
                PostPaidLoadLoyaltyCreditsVoucherBean postPaidLoadLoyaltyCreditsVoucherBean = new PostPaidLoadLoyaltyCreditsVoucherBean(postPaidLoadLoyaltyCreditsVoucher);
                postPaidLoadLoyaltyCreditsVoucherBean.setPostPaidLoadLoyaltyCreditsBean(this);
                this.postPaidLoadLoyaltyCreditsVoucherBean.add(postPaidLoadLoyaltyCreditsVoucherBean);
            }
        }

    }

    public PostPaidLoadLoyaltyCreditsBean(PostPaidLoadLoyaltyCreditsHistoryBean postPaidLoadLoyaltyCreditsBean) {

        this.csTransactionID = postPaidLoadLoyaltyCreditsBean.getCsTransactionID();
        this.operationID = postPaidLoadLoyaltyCreditsBean.getOperationID();
        this.operationIDReversed = postPaidLoadLoyaltyCreditsBean.getOperationIDReversed();
        this.operationType = postPaidLoadLoyaltyCreditsBean.getOperationType();
        this.requestTimestamp = postPaidLoadLoyaltyCreditsBean.getRequestTimestamp();
        this.marketingMsg = postPaidLoadLoyaltyCreditsBean.getMarketingMsg();
        this.warningMsg = postPaidLoadLoyaltyCreditsBean.getWarningMsg();
        this.messageCode = postPaidLoadLoyaltyCreditsBean.getMessageCode();
        this.statusCode = postPaidLoadLoyaltyCreditsBean.getStatusCode();
        this.credits = postPaidLoadLoyaltyCreditsBean.getCredits();
        this.balance = postPaidLoadLoyaltyCreditsBean.getBalance();
        this.balanceAmount = postPaidLoadLoyaltyCreditsBean.getBalanceAmount();
        this.cardCodeIssuer = postPaidLoadLoyaltyCreditsBean.getCardCodeIssuer();
        this.eanCode = postPaidLoadLoyaltyCreditsBean.getEanCode();
        this.cardStatus = postPaidLoadLoyaltyCreditsBean.getCardStatus();
        this.cardType = postPaidLoadLoyaltyCreditsBean.getCardType();
        this.cardClassification = postPaidLoadLoyaltyCreditsBean.getCardClassification();
        //this.reconciled = postPaidLoadLoyaltyCreditsBean.getReconciled();
    }

    
    public PostPaidLoadLoyaltyCredits toPostPaidLoadLoyaltyCredits() {

        PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits = new PostPaidLoadLoyaltyCredits();
        postPaidLoadLoyaltyCredits.setId(this.id);
        postPaidLoadLoyaltyCredits.setCsTransactionID(this.csTransactionID);
        postPaidLoadLoyaltyCredits.setOperationID(this.operationID);
        postPaidLoadLoyaltyCredits.setOperationIDReversed(this.operationIDReversed);
        postPaidLoadLoyaltyCredits.setOperationType(this.operationType);
        postPaidLoadLoyaltyCredits.setRequestTimestamp(this.requestTimestamp);
        postPaidLoadLoyaltyCredits.setMarketingMsg(this.marketingMsg);
        postPaidLoadLoyaltyCredits.setWarningMsg(this.warningMsg);
        postPaidLoadLoyaltyCredits.setMessageCode(this.messageCode);
        postPaidLoadLoyaltyCredits.setStatusCode(this.statusCode);
        postPaidLoadLoyaltyCredits.setCredits(this.credits);
        postPaidLoadLoyaltyCredits.setBalance(this.balance);
        postPaidLoadLoyaltyCredits.setBalanceAmount(this.balanceAmount);
        postPaidLoadLoyaltyCredits.setCardCodeIssuer(this.cardCodeIssuer);
        postPaidLoadLoyaltyCredits.setEanCode(this.eanCode);
        postPaidLoadLoyaltyCredits.setCardStatus(this.cardStatus);
        postPaidLoadLoyaltyCredits.setCardType(this.cardType);
        postPaidLoadLoyaltyCredits.setCardClassification(this.cardClassification);
        postPaidLoadLoyaltyCredits.setReconciled(this.reconciled);
        
        if (!this.postPaidLoadLoyaltyCreditsVoucherBean.isEmpty()) {

            for (PostPaidLoadLoyaltyCreditsVoucherBean postPaidLoadLoyaltyCreditsVoucherBean : this.postPaidLoadLoyaltyCreditsVoucherBean) {
                PostPaidLoadLoyaltyCreditsVoucher postPaidLoadLoyaltyCreditsVoucher = postPaidLoadLoyaltyCreditsVoucherBean.toPostPaidLoadLoyaltyCreditsVoucher();
                postPaidLoadLoyaltyCredits.getPostPaidLoadLoyaltyCreditsVoucherList().add(postPaidLoadLoyaltyCreditsVoucher);
            }
        }
        
        return postPaidLoadLoyaltyCredits;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public void setOperationIDReversed(String operationIDReversed) {
        this.operationIDReversed = operationIDReversed;
    }

    public String getOperationIDReversed() {
        return operationIDReversed;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getCardCodeIssuer() {
        return cardCodeIssuer;
    }

    public void setCardCodeIssuer(String cardCodeIssuer) {
        this.cardCodeIssuer = cardCodeIssuer;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardClassification() {
        return cardClassification;
    }

    public void setCardClassification(String cardClassification) {
        this.cardClassification = cardClassification;
    }

    public Boolean getReconciled() {
        return reconciled;
    }

    public void setReconciled(Boolean reconciled) {
        this.reconciled = reconciled;
    }

    public PostPaidTransactionBean getPostPaidTransactionBean() {
        return postPaidTransactionBean;
    }

    public void setPostPaidTransactionBean(PostPaidTransactionBean postPaidTransactionBean) {
        this.postPaidTransactionBean = postPaidTransactionBean;
    }

    public Set<PostPaidLoadLoyaltyCreditsVoucherBean> getPostPaidLoadLoyaltyCreditsVoucherBean() {
        return postPaidLoadLoyaltyCreditsVoucherBean;
    }

    public void setPostPaidLoadLoyaltyCreditsVoucherBean(Set<PostPaidLoadLoyaltyCreditsVoucherBean> postPaidLoadLoyaltyCreditsVoucherBean) {
        this.postPaidLoadLoyaltyCreditsVoucherBean = postPaidLoadLoyaltyCreditsVoucherBean;
    }

}
