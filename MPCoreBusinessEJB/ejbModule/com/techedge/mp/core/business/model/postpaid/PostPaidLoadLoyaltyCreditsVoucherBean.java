package com.techedge.mp.core.business.model.postpaid;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCreditsVoucher;

@Entity
@Table(name = "POSTPAIDLOADLOYALTYCREDITSVOUCHER")
public class PostPaidLoadLoyaltyCreditsVoucherBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "code", nullable = false, length = 16)
    private String             code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "postPaidLoadLoyaltyCreditsBean", nullable = false)
    private PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean;

    @Column(name = "status", nullable = false, length = 1)
    private String             status;

    @Column(name = "type", nullable = false, length = 10)
    private String             type;

    @Column(name = "value", nullable = false)
    private Double             value;

    @Column(name = "initial_value", nullable = false)
    private Double             initialValue;

    @Column(name = "consumed_value", nullable = false)
    private Double             consumedValue;

    @Column(name = "voucher_balance_due", nullable = false)
    private Double             voucherBalanceDue;

    @Column(name = "expiration_date", nullable = true)
    private Date               expirationDate;

    @Column(name = "promo_code", nullable = true, length = 10)
    private String             promoCode;

    @Column(name = "promo_description", nullable = true)
    private String             promoDescription;

    @Column(name = "promo_doc", nullable = true)
    private String             promoDoc;

    @Column(name = "min_amount", nullable = true)
    private Double             minAmount;

    @Column(name = "min_quantity", nullable = true)
    private Double             minQuantity;

    @Column(name = "is_combinable", nullable = true, length = 1)
    private String             isCombinable;

    @Column(name = "valid_pv", nullable = true, length = 1)
    private String             validPV;

    @Column(name = "promo_partner", nullable = true)
    private String             promoPartner;

    @Column(name = "icon", nullable = true)
    private String             icon;

    
    public PostPaidLoadLoyaltyCreditsVoucherBean() {}

    public PostPaidLoadLoyaltyCreditsVoucherBean(PostPaidLoadLoyaltyCreditsVoucher postPaidLoadLoyaltyCreditsVoucher) {

        this.id = postPaidLoadLoyaltyCreditsVoucher.getId();
        this.code = postPaidLoadLoyaltyCreditsVoucher.getCode();
        this.status = postPaidLoadLoyaltyCreditsVoucher.getStatus();
        this.type = postPaidLoadLoyaltyCreditsVoucher.getType();
        this.value = postPaidLoadLoyaltyCreditsVoucher.getValue();
        this.initialValue = postPaidLoadLoyaltyCreditsVoucher.getInitialValue();
        this.consumedValue = postPaidLoadLoyaltyCreditsVoucher.getConsumedValue();
        this.voucherBalanceDue = postPaidLoadLoyaltyCreditsVoucher.getVoucherBalanceDue();
        this.expirationDate = postPaidLoadLoyaltyCreditsVoucher.getExpirationDate();
        this.promoCode = postPaidLoadLoyaltyCreditsVoucher.getPromoCode();
        this.promoDescription = postPaidLoadLoyaltyCreditsVoucher.getPromoDescription();
        this.minAmount = postPaidLoadLoyaltyCreditsVoucher.getMinAmount();
        this.minQuantity = postPaidLoadLoyaltyCreditsVoucher.getMinQuantity();
        this.isCombinable = postPaidLoadLoyaltyCreditsVoucher.getIsCombinable();
        this.validPV = postPaidLoadLoyaltyCreditsVoucher.getValidPV();
        this.promoPartner = postPaidLoadLoyaltyCreditsVoucher.getPromoPartner();
        this.icon = postPaidLoadLoyaltyCreditsVoucher.getIcon();

    }

    public PostPaidLoadLoyaltyCreditsVoucher toPostPaidLoadLoyaltyCreditsVoucher() {

        PostPaidLoadLoyaltyCreditsVoucher postPaidLoadLoyaltyCreditsVoucher = new PostPaidLoadLoyaltyCreditsVoucher();

        postPaidLoadLoyaltyCreditsVoucher.setId(this.id);
        postPaidLoadLoyaltyCreditsVoucher.setCode(this.code);
        postPaidLoadLoyaltyCreditsVoucher.setStatus(this.status);
        postPaidLoadLoyaltyCreditsVoucher.setType(this.type);
        postPaidLoadLoyaltyCreditsVoucher.setValue(this.value);
        postPaidLoadLoyaltyCreditsVoucher.setInitialValue(this.initialValue);
        postPaidLoadLoyaltyCreditsVoucher.setConsumedValue(this.consumedValue);
        postPaidLoadLoyaltyCreditsVoucher.setVoucherBalanceDue(this.voucherBalanceDue);
        postPaidLoadLoyaltyCreditsVoucher.setExpirationDate(this.expirationDate);
        postPaidLoadLoyaltyCreditsVoucher.setPromoCode(this.promoCode);
        postPaidLoadLoyaltyCreditsVoucher.setPromoDescription(this.promoDescription);
        postPaidLoadLoyaltyCreditsVoucher.setPromoDoc(this.promoDoc);
        postPaidLoadLoyaltyCreditsVoucher.setMinAmount(this.minAmount);
        postPaidLoadLoyaltyCreditsVoucher.setMinQuantity(this.minQuantity);
        postPaidLoadLoyaltyCreditsVoucher.setIsCombinable(this.isCombinable);
        postPaidLoadLoyaltyCreditsVoucher.setValidPV(this.validPV);
        postPaidLoadLoyaltyCreditsVoucher.setPromoPartner(this.promoPartner);
        postPaidLoadLoyaltyCreditsVoucher.setIcon(this.icon);

        return postPaidLoadLoyaltyCreditsVoucher;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PostPaidLoadLoyaltyCreditsBean getPostPaidLoadLoyaltyCreditsBean() {
        return postPaidLoadLoyaltyCreditsBean;
    }

    public void setPostPaidLoadLoyaltyCreditsBean(PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean) {
        this.postPaidLoadLoyaltyCreditsBean = postPaidLoadLoyaltyCreditsBean;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Double initialValue) {
        this.initialValue = initialValue;
    }

    public Double getConsumedValue() {
        return consumedValue;
    }

    public void setConsumedValue(Double consumedValue) {
        this.consumedValue = consumedValue;
    }

    public Double getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Double voucherBalanceDue) {
        this.voucherBalanceDue = voucherBalanceDue;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {
        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {
        this.promoDoc = promoDoc;
    }

    public Double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Double minAmount) {
        this.minAmount = minAmount;
    }

    public Double getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(Double minQuantity) {
        this.minQuantity = minQuantity;
    }

    public String getIsCombinable() {
        return isCombinable;
    }

    public void setIsCombinable(String isCombinable) {
        this.isCombinable = isCombinable;
    }

    public String getValidPV() {
        return validPV;
    }

    public void setValidPV(String validPV) {
        this.validPV = validPV;
    }

    public String getPromoPartner() {
        return promoPartner;
    }

    public void setPromoPartner(String promoPartner) {
        this.promoPartner = promoPartner;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}
