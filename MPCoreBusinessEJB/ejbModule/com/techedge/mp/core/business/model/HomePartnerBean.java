package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "HOME_PARTNER")
@NamedQueries({ @NamedQuery(name = "HomePartnerBean.findAll", query = "select hp from HomePartnerBean hp"), })
public class HomePartnerBean {

    public static final String FIND_ALL = "HomePartnerBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "sequence", nullable = false)
    private Integer            sequence;

    @Column(name = "name", nullable = false)
    private String             name;

    public HomePartnerBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
