package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.TermsOfService;

@Entity
@Table(name = "TERMSOFSERVICE")
@NamedQueries({ @NamedQuery(name = "TermsOfServiceBean.findTermOfServiceByKeyValAndId", query = "select t from TermsOfServiceBean t where t.keyval = :keyval and t.personalDataBean = :personalDataId"),
    @NamedQuery(name = "TermsOfServiceBean.findTermOfServiceByKeyVal", query = "select t from TermsOfServiceBean t where t.keyval = :keyval"),
    @NamedQuery(name = "TermsOfServiceBean.findTermOfServiceByPersonalDataId", query = "select t from TermsOfServiceBean t where t.personalDataBean = :personalDataId"),
@NamedQuery(name = "TermsOfServiceBean.findTermOfServiceAll", query = "select t from TermsOfServiceBean t")
})
public class TermsOfServiceBean {

    public static final String FIND_BY_ID_AND_KEYVAL = "TermsOfServiceBean.findTermOfServiceByKeyValAndId";
    public static final String FIND_BY_PERSONALDATA_ID = "TermsOfServiceBean.findTermOfServiceByPersonalDataId";
    public static final String FIND_BY_KEYVAL = "TermsOfServiceBean.findTermOfServiceByKeyVal";
    public static final String FIND_ALL = "TermsOfServiceBean.findTermOfServiceAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personalDataId", nullable = false)
    private PersonalDataBean   personalDataBean;

    @Column(name = "keyval", nullable = false)
    private String             keyval;

    @Column(name = "accepted", nullable = false)
    private Boolean            accepted;

    @Column(name = "valid", nullable = true)
    private Boolean            valid;

    public TermsOfServiceBean() {}

    public TermsOfServiceBean(TermsOfService termsOfService) {
        this.id = termsOfService.getId();
        this.keyval = termsOfService.getKeyval();
        this.accepted = termsOfService.getAccepted();
        this.valid = termsOfService.getValid();
    }

    public TermsOfService toTermsOfService() {
        TermsOfService termsOfService = new TermsOfService();
        termsOfService.setAccepted(this.accepted);
        termsOfService.setKeyval(this.keyval);
        termsOfService.setId(this.id);
        termsOfService.setValid(this.valid);
        //termsOfService.setPersonalData(personalDataBean.toPersonalData());
        return termsOfService;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PersonalDataBean getPersonalDataBean() {
        return personalDataBean;
    }

    public void setPersonalDataBean(PersonalDataBean personalDataBean) {
        this.personalDataBean = personalDataBean;
    }

    public String getKeyval() {
        return keyval;
    }

    public void setKeyval(String keyval) {
        this.keyval = keyval;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
