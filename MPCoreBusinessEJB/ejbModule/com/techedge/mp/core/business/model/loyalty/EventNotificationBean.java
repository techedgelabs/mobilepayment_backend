package com.techedge.mp.core.business.model.loyalty;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.loyalty.EventNotificationType;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.UserBean;

@Entity
@Table(name = "EVENT_NOTIFICATION")
@NamedQueries({
        @NamedQuery(name = "EventNotificationBean.findById", query = "select e from EventNotificationBean e where e.id = :id"),
        @NamedQuery(name = "EventNotificationBean.findByPushNotification", query = "select e from EventNotificationBean e where (e.pushNotificationBean  != null and "
                + "e.pushNotificationBean = :pushnotificationbean)"),
        
        @NamedQuery(name = "EventNotificationBean.findSuccessBySrcTransactionID", query = "select e from EventNotificationBean e where e.srcTransactionID = :srcTransactionID and e.transactionResult = '00' and e.eventType = :eventType"),
        
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisTotalLoyalty", query = "select "
                + "(select count(e1.id) from EventNotificationBean e1 where e1.eventType = 'LOYALTY' and e1.transactionResult = '00' "
                + "and e1.credits > 0 and e1.requestTimestamp >= :dailyStartDate and e1.requestTimestamp < :dailyEndDate), "
                + "(select count(e2.id) from EventNotificationBean e2 where e2.eventType = 'LOYALTY' and e2.transactionResult = '00' "
                + "and e2.credits > 0 and e2.requestTimestamp >= :weeklyStartDate and e2.requestTimestamp < :weeklyEndDate), "
                + "(select count(e3.id) from EventNotificationBean e3 where e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate) from EventNotificationBean e"),
                
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisAllTotalLoyalty", query = "select "
                + "count(e3.id) from EventNotificationBean e3 where e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate"),
        
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisLoyaltyRefuelmode", query = "select "
                + "(select count(e1.id) from EventNotificationBean e1 join e1.loyaltyTransactionDetail ltd1 join ltd1.refuelDetails rd1 where rd1.refuelMode = :refuelMode and e1.eventType = 'LOYALTY' and e1.transactionResult = '00' "
                + "and e1.credits > 0 and e1.requestTimestamp >= :dailyStartDate and e1.requestTimestamp < :dailyEndDate), "
                + "(select count(e2.id) from EventNotificationBean e2 join e2.loyaltyTransactionDetail ltd2 join ltd2.refuelDetails rd2 where rd2.refuelMode = :refuelMode and e2.eventType = 'LOYALTY' and e2.transactionResult = '00' "
                + "and e2.credits > 0 and e2.requestTimestamp >= :weeklyStartDate and e2.requestTimestamp < :weeklyEndDate), "
                + "(select count(e3.id) from EventNotificationBean e3 join e3.loyaltyTransactionDetail ltd3 join ltd3.refuelDetails rd3 where rd3.refuelMode = :refuelMode and e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate) from EventNotificationBean e"),
                
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisAllLoyaltyRefuelmode", query = "select "
                + "count(e3.id) from EventNotificationBean e3 join e3.loyaltyTransactionDetail ltd3 join ltd3.refuelDetails rd3 where rd3.refuelMode = :refuelMode and e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate"),
        
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisLoyaltyNonoil", query = "select "
                + "(select count(e1.id) from EventNotificationBean e1 join e1.loyaltyTransactionDetail ltd1 join ltd1.nonOilDetails nd1 where e1.eventType = 'LOYALTY' and e1.transactionResult = '00' "
                + "and e1.credits > 0 and e1.requestTimestamp >= :dailyStartDate and e1.requestTimestamp < :dailyEndDate), "
                + "(select count(e2.id) from EventNotificationBean e2 join e2.loyaltyTransactionDetail ltd2 join ltd2.nonOilDetails nd2 where e2.eventType = 'LOYALTY' and e2.transactionResult = '00' "
                + "and e2.credits > 0 and e2.requestTimestamp >= :weeklyStartDate and e2.requestTimestamp < :weeklyEndDate), "
                + "(select count(e3.id) from EventNotificationBean e3 join e3.loyaltyTransactionDetail ltd3 join ltd3.nonOilDetails nd3 where e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate) from EventNotificationBean e"),
                
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisAllLoyaltyNonoil", query = "select "
                + "count(e3.id) from EventNotificationBean e3 join e3.loyaltyTransactionDetail ltd3 join ltd3.nonOilDetails nd3 where e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate"),
        
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisLoyaltyOilAndNonoil", query = "select "
                + "(select count(e1.id) from EventNotificationBean e1 join e1.loyaltyTransactionDetail ltd1 join ltd1.nonOilDetails nd1 where ltd1.refuelDetails IS NOT EMPTY and e1.eventType = 'LOYALTY' and e1.transactionResult = '00' "
                + "and e1.credits > 0 and e1.requestTimestamp >= :dailyStartDate and e1.requestTimestamp < :dailyEndDate), "
                + "(select count(e2.id) from EventNotificationBean e2 join e2.loyaltyTransactionDetail ltd2 join ltd2.nonOilDetails nd2 where ltd2.refuelDetails IS NOT EMPTY and e2.eventType = 'LOYALTY' and e2.transactionResult = '00' "
                + "and e2.credits > 0 and e2.requestTimestamp >= :weeklyStartDate and e2.requestTimestamp < :weeklyEndDate), "
                + "(select count(e3.id) from EventNotificationBean e3 join e3.loyaltyTransactionDetail ltd3 join ltd3.nonOilDetails nd3 where ltd3.refuelDetails IS NOT EMPTY and e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate) from EventNotificationBean e"),
        
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisTotalLoyaltyArea", query = "select "
                + "(select count(e1.id) from EventNotificationBean e1, StationBean s1, ProvinceInfoBean p1 "
                + "where e1.stationID = s1.stationID and s1.province = p1.name and p1.area = :area and e1.eventType = 'LOYALTY' and e1.transactionResult = '00' "
                + "and e1.credits > 0 and e1.requestTimestamp >= :dailyStartDate and e1.requestTimestamp < :dailyEndDate), "
                + "(select count(e2.id) from EventNotificationBean e2, StationBean s2, ProvinceInfoBean p2 "
                + "where e2.stationID = s2.stationID and s2.province = p2.name and p2.area = :area and e2.eventType = 'LOYALTY' and e2.transactionResult = '00' "
                + "and e2.credits > 0 and e2.requestTimestamp >= :weeklyStartDate and e2.requestTimestamp < :weeklyEndDate), "
                + "(select count(e3.id) from EventNotificationBean e3, StationBean s3, ProvinceInfoBean p3 "
                + "where e3.stationID = s3.stationID and s3.province = p3.name and p3.area = :area and e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate) from EventNotificationBean e"),
                
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisAllTotalLoyaltyArea", query = "select "
                + "count(e3.id) from EventNotificationBean e3, StationBean s3, ProvinceInfoBean p3 "
                + "where e3.stationID = s3.stationID and s3.province = p3.name and p3.area = :area and e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate"),
                        
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisTotalLoyaltyCredits", query = "select "
                + "(select case when sum(e1.credits) is null then 0 else sum(e1.credits) end from EventNotificationBean e1 where e1.eventType = 'LOYALTY' and e1.transactionResult = '00' "
                + "and e1.credits > 0 and e1.requestTimestamp >= :dailyStartDate and e1.requestTimestamp < :dailyEndDate), "
                + "(select case when sum(e2.credits) is null then 0 else sum(e2.credits) end from EventNotificationBean e2 where e2.eventType = 'LOYALTY' and e2.transactionResult = '00' "
                + "and e2.credits > 0 and e2.requestTimestamp >= :weeklyStartDate and e2.requestTimestamp < :weeklyEndDate), "
                + "(select case when sum(e3.credits) is null then 0 else sum(e3.credits) end from EventNotificationBean e3 where e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate) from EventNotificationBean e"),
                
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisAllTotalLoyaltyCredits", query = "select "
                + "case when sum(e3.credits) is null then 0 else sum(e3.credits) end from EventNotificationBean e3 where e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate"),

        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisTotalLoyaltyFuelQuantity", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from EventNotificationBean e1 join e1.loyaltyTransactionDetail l1 join l1.refuelDetails r1 "
                + "where e1.eventType = 'LOYALTY' and e1.transactionResult = '00' and e1.credits > 0 and e1.requestTimestamp >= :dailyStartDate and e1.requestTimestamp < :dailyEndDate), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from EventNotificationBean e2 join e2.loyaltyTransactionDetail l2 join l2.refuelDetails r2 "
                + "where e2.eventType = 'LOYALTY' and e2.transactionResult = '00' and e2.credits > 0 and e2.requestTimestamp >= :weeklyStartDate and e2.requestTimestamp < :weeklyEndDate), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from EventNotificationBean e3 join e3.loyaltyTransactionDetail l3 join l3.refuelDetails r3 "
                + "where e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate) from EventNotificationBean e"),
                
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantity", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from EventNotificationBean e3 join e3.loyaltyTransactionDetail l3 join l3.refuelDetails r3 "
                + "where e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisTotalLoyaltyFuelQuantityByProduct", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from EventNotificationBean e1 join e1.loyaltyTransactionDetail l1 join l1.refuelDetails r1 "
                + "where r1.productDescription = :productDescription and e1.eventType = 'LOYALTY' and e1.transactionResult = '00' and e1.credits > 0 and e1.requestTimestamp >= :dailyStartDate and e1.requestTimestamp < :dailyEndDate), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from EventNotificationBean e2 join e2.loyaltyTransactionDetail l2 join l2.refuelDetails r2 "
                + "where r2.productDescription = :productDescription and e2.eventType = 'LOYALTY' and e2.transactionResult = '00' and e2.credits > 0 and e2.requestTimestamp >= :weeklyStartDate and e2.requestTimestamp < :weeklyEndDate), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from EventNotificationBean e3 join e3.loyaltyTransactionDetail l3 join l3.refuelDetails r3 "
                + "where r3.productDescription = :productDescription and e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate) from EventNotificationBean e"),
                
        @NamedQuery(name = "EventNotificationBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantityByProduct", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from EventNotificationBean e3 join e3.loyaltyTransactionDetail l3 join l3.refuelDetails r3 "
                + "where r3.productDescription = :productDescription and e3.eventType = 'LOYALTY' and e3.transactionResult = '00' "
                + "and e3.credits > 0 and e3.requestTimestamp >= :totalStartDate and e3.requestTimestamp < :dailyEndDate"),
})
public class EventNotificationBean {

    public static final String     FIND_BY_ID                                              = "EventNotificationBean.findById";
    public static final String     FIND_BY_PUSHNOTIFICATION                                = "EventNotificationBean.findByPushNotification";
    public static final String     FIND_SUCCESS_BY_SRCTRANSACTIONID                        = "EventNotificationBean.findSuccessBySrcTransactionID";
    public static final String     STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY               = "EventNotificationBean.statisticsReportSynthesisTotalLoyalty";
    public static final String     STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY           = "EventNotificationBean.statisticsReportSynthesisAllTotalLoyalty";
    public static final String     STATISTICS_REPORT_SYNTHESIS_LOYALTY_REFUELMODE          = "EventNotificationBean.statisticsReportSynthesisLoyaltyRefuelmode";
    public static final String     STATISTICS_REPORT_SYNTHESIS_ALL_LOYALTY_REFUELMODE      = "EventNotificationBean.statisticsReportSynthesisAllLoyaltyRefuelmode";
    public static final String     STATISTICS_REPORT_SYNTHESIS_LOYALTY_NONOIL              = "EventNotificationBean.statisticsReportSynthesisLoyaltyNonoil";
    public static final String     STATISTICS_REPORT_SYNTHESIS_ALL_LOYALTY_NONOIL          = "EventNotificationBean.statisticsReportSynthesisAllLoyaltyNonoil";
    public static final String     STATISTICS_REPORT_SYNTHESIS_LOYALTY_OILANDNONOIL        = "EventNotificationBean.statisticsReportSynthesisLoyaltyOilAndNonoil";
    public static final String     STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_AREA          = "EventNotificationBean.statisticsReportSynthesisTotalLoyaltyArea";
    public static final String     STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_AREA      = "EventNotificationBean.statisticsReportSynthesisAllTotalLoyaltyArea";
    public static final String     STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_CREDITS       = "EventNotificationBean.statisticsReportSynthesisTotalLoyaltyCredits";
    public static final String     STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_CREDITS   = "EventNotificationBean.statisticsReportSynthesisAllTotalLoyaltyCredits";
    public static final String     STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY = "EventNotificationBean.statisticsReportSynthesisTotalLoyaltyFuelQuantity";
    public static final String     STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY = "EventNotificationBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantity";
    public static final String     STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT = "EventNotificationBean.statisticsReportSynthesisTotalLoyaltyFuelQuantityByProduct";
    public static final String     STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT = "EventNotificationBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantityByProduct";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                   id;

    @Column(name = "event_type", nullable = false)
    private String                 eventType;

    @Column(name = "fiscal_code")
    private String                 fiscalCode;

    @Column(name = "src_transaction_id", nullable = false)
    private String                 srcTransactionID;

    @Column(name = "request_timestamp", nullable = false)
    private Date                   requestTimestamp;

    @Column(name = "session_id", length = 32)
    private String                 sessionID;

    @Column(name = "card_status")
    private String                 cardStatus;

    @Column(name = "pan_code")
    private String                 panCode;

    @Column(name = "station_id")
    private String            stationID;

    @Column(name = "terminal_id")
    private String                 terminalID;

    @Column(name = "transaction_result")
    private String                 transactionResult;

    @Column(name = "transaction_date")
    private Date                   transactionDate;

    @Column(name = "credits")
    private Integer                credits;

    @Column(name = "balance")
    private Integer                balance;

    @Column(name = "balance_amount")
    private Integer                balanceAmount;

    @Column(name = "marketing_msg")
    private String                 marketingMsg;

    @Column(name = "warning_msg")
    private String                 warningMsg;

    @Column(name = "operation_type")
    private String                 operationType;

    @Column(name = "subtype")
    private String                 subtype;

    @Column(name = "mp_status_code")
    private String                 mpStatusCode;

    @Column(name = "mp_status_message")
    private String                 mpStatusMessage;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "user_id")
    private UserBean               userBean;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "eventNotificationBean")
    private RewardTransactionBean  rewardTransactionDetail;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "eventNotificationBean")
    private LoyaltyTransactionBean loyaltyTransactionDetail;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "push_notification_id")
    private PushNotificationBean   pushNotificationBean;

    public EventNotificationBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public EventNotificationType getEventType() {
        return EventNotificationType.getEventNotification(eventType);
    }

    public void setEventType(EventNotificationType eventType) {
        this.eventType = eventType.getValue();
    }

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public LoyaltyTransactionBean getLoyaltyTransactionDetail() {
        return loyaltyTransactionDetail;
    }

    public void setLoyaltyTransactionDetail(LoyaltyTransactionBean loyaltyTransactionDetail) {
        this.loyaltyTransactionDetail = loyaltyTransactionDetail;
    }

    public RewardTransactionBean getRewardTransactionDetail() {
        return rewardTransactionDetail;
    }

    public void setRewardTransactionDetail(RewardTransactionBean rewardTransactionDetail) {
        this.rewardTransactionDetail = rewardTransactionDetail;
    }

    public PushNotificationBean getPushNotificationBean() {
        return pushNotificationBean;
    }

    public void setPushNotificationBean(PushNotificationBean pushNotificationBean) {
        this.pushNotificationBean = pushNotificationBean;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getPanCode() {
        return panCode;
    }

    public void setPanCode(String panCode) {
        this.panCode = panCode;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {
        this.transactionResult = transactionResult;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getMpStatusCode() {
        return mpStatusCode;
    }

    public void setMpStatusCode(String mpStatusCode) {
        this.mpStatusCode = mpStatusCode;
    }

    public String getMpStatusMessage() {
        return mpStatusMessage;
    }

    public void setMpStatusMessage(String mpStatusMessage) {
        this.mpStatusMessage = mpStatusMessage;
    }

    public Integer getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Integer balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

}
