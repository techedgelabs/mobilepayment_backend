package com.techedge.mp.core.business.model.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PromoVoucherBeanTest.class, PromoBeanTest.class })
public class AllTests {

}
