package com.techedge.mp.core.business.model.postpaid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionAdditionalData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionHistoryAdditionalData;

@Entity
@Table(name = "POSTPAIDTRANSACTION_HISTORY_ADDITIONAL_DATA")
/*@NamedQueries({
    @NamedQuery(name = "TransactionEventBean.findTransactionEventByTransaction", query = "select t from TransactionEventBean t where t.transactionBean = :transactionBean"),
    @NamedQuery(name = "TransactionEventBean.findTransactionEventById", query = "select t from TransactionEventBean t where t.id = :id")
})*/
public class PostPaidTransactionHistoryAdditionalDataBean {

    //public static final String FIND_BY_TRANSACTION = "TransactionEventBean.findTransactionEventByTransaction";
    //public static final String FIND_BY_ID = "TransactionEventBean.findTransactionEventById";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    private PostPaidTransactionHistoryBean    postPaidTransactionHistoryBean;

    @Column(name = "data_key", nullable = true)
    private String             dataKey;

    @Column(name = "data_value", nullable = true)
    private String             dataValue;

    public PostPaidTransactionHistoryAdditionalDataBean() {}

    public PostPaidTransactionHistoryAdditionalDataBean(PostPaidTransactionHistoryAdditionalData postPaidTransactionHistoryAdditionalData) {

        this.id = postPaidTransactionHistoryAdditionalData.getId();
        this.dataKey = postPaidTransactionHistoryAdditionalData.getDataKey();
        this.dataValue = postPaidTransactionHistoryAdditionalData.getDataValue();
    }
    
    public PostPaidTransactionHistoryAdditionalDataBean(PostPaidTransactionAdditionalDataBean postPaidTransactionAdditionalDataBean) {

        this.id = postPaidTransactionAdditionalDataBean.getId();
        this.dataKey = postPaidTransactionAdditionalDataBean.getDataKey();
        this.dataValue = postPaidTransactionAdditionalDataBean.getDataValue();
    }

    public PostPaidTransactionHistoryAdditionalData toPostPaidTransactionHistoryAdditionalData() {

        PostPaidTransactionHistoryAdditionalData postPaidTransactionHistoryAdditionalData = new PostPaidTransactionHistoryAdditionalData();
        postPaidTransactionHistoryAdditionalData.setId(this.id);
        postPaidTransactionHistoryAdditionalData.setDataKey(this.dataKey);
        postPaidTransactionHistoryAdditionalData.setDataValue(this.dataValue);
        return postPaidTransactionHistoryAdditionalData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PostPaidTransactionHistoryBean getPostPaidTransactionHistoryBean() {
        return postPaidTransactionHistoryBean;
    }

    public void setPostPaidTransactionHistoryBean(PostPaidTransactionHistoryBean postPaidTransactionHistoryBean) {
        this.postPaidTransactionHistoryBean = postPaidTransactionHistoryBean;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }

}
