package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PrefixNumberData;

@Entity
@Table(name = "PREFIXNUMBER")
@NamedQueries({ @NamedQuery(name = "PrefixNumber.findPrefixByCode", query = "select p from PrefixNumberBean p where p.code = :code"),
        @NamedQuery(name = "PrefixNumber.findAllPrefix", query = "select p from PrefixNumberBean p") })
public class PrefixNumberBean {
    public static final String FIND_PREFIX_BYCODE      = "PrefixNumber.findPrefixByCode";
    public static final String FIND_ALLPREFIX = "PrefixNumber.findAllPrefix";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "code", nullable = false)
    private String             code;

    public PrefixNumberBean() {}

    public PrefixNumberBean(PrefixNumberData prefixNumber) {

        this.id = prefixNumber.getId();
        this.code = prefixNumber.getCode();

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
