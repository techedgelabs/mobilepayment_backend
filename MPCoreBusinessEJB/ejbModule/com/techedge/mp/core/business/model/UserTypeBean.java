package com.techedge.mp.core.business.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.user.UserType;

@Entity
@Table(name = "USER_TYPE")
@NamedQueries({ @NamedQuery(name = "UserTypeBean.findUserTypeByName", query = "select p from UserTypeBean p where p.code = :code"),
        @NamedQuery(name = "UserTypeBean.findAllUserTypes", query = "select p from UserTypeBean p") })
public class UserTypeBean {
    public static final String    FIND_BY_NAME     = "UserTypeBean.findUserTypeByName";
    public static final String    FIND_ALL       = "UserTypeBean.findAllUserTypes";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                  id;

    @Column(name = "code", nullable = true)
    private Integer               code;

    @ManyToMany(mappedBy = "userTypes")
    private Set<UserCategoryBean> userCategories = new HashSet<UserCategoryBean>(0);

    public UserTypeBean() {}

    public UserTypeBean(UserType userCategory) {
        this.id = userCategory.getId();
        this.code = userCategory.getCode();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Set<UserCategoryBean> getUserCategories() {
        return userCategories;
    }

    public void setUserCategories(Set<UserCategoryBean> userCategories) {
        this.userCategories = userCategories;
    }

}
