package com.techedge.mp.core.business.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucherDetail;

@Entity
@Table(name = "PREPAIDCONSUMEVOUCHERHISTORY")
public class PrePaidConsumeVoucherHistoryBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                                        id;

    @Column(name = "cs_transaction_id", nullable = true)
    private String                                      csTransactionID;

    @Column(name = "operation_id", nullable = true)
    private String                                      operationID;

    @Column(name = "operation_id_reversed", nullable = true)
    private String                                      operationIDReversed;

    @Column(name = "operation_type", nullable = true)
    private String                                      operationType;

    @Column(name = "request_timestamp", nullable = true)
    private Long                                        requestTimestamp;

    @Column(name = "marketing_msg", nullable = true, length = 500)
    private String                                      marketingMsg;

    @Column(name = "warning_msg", nullable = true)
    private String                                      warningMsg;

    @Column(name = "message_code", nullable = true)
    private String                                      messageCode;

    @Column(name = "status_code", nullable = true)
    private String                                      statusCode;

    @Column(name = "total_consumed", nullable = true)
    private Double                                      totalConsumed;

    @Column(name = "amount", nullable = true)
    private Double                                amount;

    @Column(name = "reconciled", nullable = true)
    private Boolean                                     reconciled                             = false;
    
    @Column(name = "preauth_operation_id", nullable = true)
    private String                                preAuthOperationID;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    private TransactionHistoryBean                      transactionHistoryBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "prePaidConsumeVoucherHistoryBean")
    private Set<PrePaidConsumeVoucherDetailHistoryBean> prePaidConsumeVoucherDetailHistoryBean = new HashSet<PrePaidConsumeVoucherDetailHistoryBean>(0);

    public PrePaidConsumeVoucherHistoryBean() {}

    public PrePaidConsumeVoucherHistoryBean(PrePaidConsumeVoucherBean prePaidConsumeVoucherBean) {

        this.csTransactionID = prePaidConsumeVoucherBean.getCsTransactionID();
        this.operationID = prePaidConsumeVoucherBean.getOperationID();
        this.operationIDReversed = prePaidConsumeVoucherBean.getOperationIDReversed();
        this.operationType = prePaidConsumeVoucherBean.getOperationType();
        this.requestTimestamp = prePaidConsumeVoucherBean.getRequestTimestamp();
        this.marketingMsg = prePaidConsumeVoucherBean.getMarketingMsg();
        this.warningMsg = prePaidConsumeVoucherBean.getWarningMsg();
        this.messageCode = prePaidConsumeVoucherBean.getMessageCode();
        this.statusCode = prePaidConsumeVoucherBean.getStatusCode();
        this.totalConsumed = prePaidConsumeVoucherBean.getTotalConsumed();
        this.amount = prePaidConsumeVoucherBean.getAmount();
        this.reconciled = prePaidConsumeVoucherBean.getReconciled();
        this.preAuthOperationID = prePaidConsumeVoucherBean.getPreAuthOperationID();
        
        for (PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean : prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailBean()) {

            PrePaidConsumeVoucherDetailHistoryBean postPaidConsumeVoucherDetailHistoryBean = new PrePaidConsumeVoucherDetailHistoryBean(prePaidConsumeVoucherDetailBean);

            this.prePaidConsumeVoucherDetailHistoryBean.add(postPaidConsumeVoucherDetailHistoryBean);
        }
    }

    public PrePaidConsumeVoucherHistoryBean(PrePaidConsumeVoucher prePaidConsumeVoucher) {

        this.csTransactionID = prePaidConsumeVoucher.getCsTransactionID();
        this.operationID = prePaidConsumeVoucher.getOperationID();
        this.operationIDReversed = prePaidConsumeVoucher.getOperationIDReversed();
        this.operationType = prePaidConsumeVoucher.getOperationType();
        this.requestTimestamp = prePaidConsumeVoucher.getRequestTimestamp();
        this.marketingMsg = prePaidConsumeVoucher.getMarketingMsg();
        this.warningMsg = prePaidConsumeVoucher.getWarningMsg();
        this.messageCode = prePaidConsumeVoucher.getMessageCode();
        this.statusCode = prePaidConsumeVoucher.getStatusCode();
        this.totalConsumed = prePaidConsumeVoucher.getTotalConsumed();
        this.amount = prePaidConsumeVoucher.getAmount();
        this.reconciled = prePaidConsumeVoucher.getReconciled();
        this.preAuthOperationID = prePaidConsumeVoucher.getPreAuthOperationID();

        for (PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetailBean : prePaidConsumeVoucher.getPrePaidConsumeVoucherDetail()) {

            PrePaidConsumeVoucherDetailHistoryBean postPaidConsumeVoucherDetailHistoryBean = new PrePaidConsumeVoucherDetailHistoryBean(prePaidConsumeVoucherDetailBean);

            this.prePaidConsumeVoucherDetailHistoryBean.add(postPaidConsumeVoucherDetailHistoryBean);
        }
    }

    public PrePaidConsumeVoucher toPrePaidConsumeVoucher() {

        PrePaidConsumeVoucher prePaidConsumeVoucher = new PrePaidConsumeVoucher();
        prePaidConsumeVoucher.setCsTransactionID(this.csTransactionID);
        prePaidConsumeVoucher.setOperationID(this.operationID);
        prePaidConsumeVoucher.setOperationIDReversed(this.operationIDReversed);
        prePaidConsumeVoucher.setOperationType(this.operationType);
        prePaidConsumeVoucher.setRequestTimestamp(this.requestTimestamp);
        prePaidConsumeVoucher.setMarketingMsg(this.marketingMsg);
        prePaidConsumeVoucher.setWarningMsg(this.warningMsg);
        prePaidConsumeVoucher.setMessageCode(this.messageCode);
        prePaidConsumeVoucher.setStatusCode(this.statusCode);
        prePaidConsumeVoucher.setTotalConsumed(this.totalConsumed);
        prePaidConsumeVoucher.setAmount(this.amount);
        prePaidConsumeVoucher.setReconciled(this.reconciled);
        prePaidConsumeVoucher.setPreAuthOperationID(this.preAuthOperationID);

        if (!this.prePaidConsumeVoucherDetailHistoryBean.isEmpty()) {

            for (PrePaidConsumeVoucherDetailHistoryBean prePaidConsumeVoucherDetailHistoryBean : this.prePaidConsumeVoucherDetailHistoryBean) {
                PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail = prePaidConsumeVoucherDetailHistoryBean.toPrePaidConsumeVoucherDetail();
                prePaidConsumeVoucher.getPrePaidConsumeVoucherDetail().add(prePaidConsumeVoucherDetail);
            }
        }

        return prePaidConsumeVoucher;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public void setOperationIDReversed(String operationIDReversed) {
        this.operationIDReversed = operationIDReversed;
    }

    public String getOperationIDReversed() {
        return operationIDReversed;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotalConsumed() {
        return totalConsumed;
    }

    public void setTotalConsumed(Double totalConsumed) {
        this.totalConsumed = totalConsumed;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Boolean getReconciled() {
        return reconciled;
    }

    public void setReconciled(Boolean reconciled) {
        this.reconciled = reconciled;
    }

    public TransactionHistoryBean getPrePaidTransactionHistoryBean() {
        return transactionHistoryBean;
    }

    public void setTransactionHistoryBean(TransactionHistoryBean transactionHistoryBean) {
        this.transactionHistoryBean = transactionHistoryBean;
    }

    public String getPreAuthOperationID() {
        return preAuthOperationID;
    }

    public void setPreAuthOperationID(String preAuthOperationID) {
        this.preAuthOperationID = preAuthOperationID;
    }

    public Set<PrePaidConsumeVoucherDetailHistoryBean> getPrePaidConsumeVoucherDetailHistoryBean() {
        return prePaidConsumeVoucherDetailHistoryBean;
    }

    public void setPrePaidConsumeVoucherDetailHistoryBean(Set<PrePaidConsumeVoucherDetailHistoryBean> prePaidConsumeVoucherDetailHistoryBean) {
        this.prePaidConsumeVoucherDetailHistoryBean = prePaidConsumeVoucherDetailHistoryBean;
    }

}
