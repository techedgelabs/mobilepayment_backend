package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "STATIONS_CONTROL_GET_LAST_REFUEL")

public class StationControlGetLastRefuelBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long               id;

    @OneToOne
    @JoinColumn(name = "stations_control_id", nullable = false)
    private StationControlBean   stationControlBean;
    
    @Column(name = "gfg_timestamp", nullable = true)
    private Date            gfgTimestamp;

    @Column(name = "mp_timestamp", nullable = true)
    private Date            mpTimestamp;
    
    @Column(name = "offset", nullable = true)
    private Long            offset;
    
    @Column(name = "current_interval", nullable = true)
    private Long            currentInterval;
    
    @Column(name = "status", nullable = true)
    private String            status;
    
    
    public StationControlGetLastRefuelBean() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StationControlBean getStationControlBean() {
        return stationControlBean;
    }

    public void setStationControlBean(StationControlBean stationControlBean) {
        this.stationControlBean = stationControlBean;
    }

    public Date getGfgTimestamp() {
        return gfgTimestamp;
    }

    public void setGfgTimestamp(Date gfgTimestamp) {
        this.gfgTimestamp = gfgTimestamp;
    }

    
    public Date getMpTimestamp() {
        return mpTimestamp;
    }

    public void setMpTimestamp(Date mpTimestamp) {
        this.mpTimestamp = mpTimestamp;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getCurrentInterval() {
        return currentInterval;
    }
    
    public void setCurrentInterval(Long currentInterval) {
        this.currentInterval = currentInterval;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    
}
