package com.techedge.mp.core.business.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.PersonalData;

@Entity
@Table(name = "PERSONALDATA")
@NamedQueries({
        @NamedQuery(name = "PersonalData.findPersonalDataByEmail", query = "select p from PersonalDataBean p where p.securityDataEmail = :email"),
        @NamedQuery(name = "PersonalData.findPersonalDataByFiscalCode", query = "select p from PersonalDataBean p where p.fiscalCode = :fiscalCode"),
        @NamedQuery(name = "PersonalData.findPersonalDataBySearchInput", query = "select p from PersonalDataBean p where p.firstName like :firstname and p.lastName like :lastname and p.fiscalCode = :fiscalCode and p.securityDataEmail = :email"),
        @NamedQuery(name = "PersonalData.findPersonalDataBySearchInputNoCF", query = "select p from PersonalDataBean p where p.firstName like :firstname and p.lastName like :lastname and p.securityDataEmail = :email"),
        @NamedQuery(name = "PersonalData.findPersonalDataID", query = "select p from PersonalDataBean p where p.id = :id")

})
public class PersonalDataBean {

    public static final String       FIND_BY_EMAIL           = "PersonalData.findPersonalDataByEmail";
    public static final String       FIND_BY_FISCALCODE      = "PersonalData.findPersonalDataByFiscalCode";
    public static final String       FIND_BY_SEARCHINPUT     = "PersonalData.findPersonalDataBySearchInput";
    public static final String       FIND_BY_SEARCHINPUTNOCF = "PersonalData.findPersonalDataBySearchInputNoCF";
    public static final String       FIND_BY_ID = "PersonalData.findPersonalDataID";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                     id;

    @Column(name = "firstname", nullable = false, length = 40)
    private String                   firstName;

    @Column(name = "lastname", nullable = false, length = 40)
    private String                   lastName;

    @Column(name = "fiscalcode", nullable = false, length = 16)
    private String                   fiscalCode;

    @Column(name = "birthdate", nullable = false)
    private Date                     birthDate;

    @Column(name = "birthmunicipality", nullable = false, length = 40)
    private String                   birthMunicipality;

    @Column(name = "birthprovince", nullable = false, length = 40)
    private String                   birthProvince;

    @Column(name = "language", nullable = false, length = 2)
    private String                   language;

    @Column(name = "sex", nullable = false, length = 1)
    private String                   sex;

    @Column(name = "securitydata_email", nullable = false, length = 50)
    private String                   securityDataEmail;

    @Column(name = "securitydata_password", nullable = false, length = 24)
    private String                   securityDataPassword;

    @Column(name = "rescuePassword", nullable = true)
    private String                   rescuePassword;

    @Column(name = "expirationDateRescuePassword", nullable = true)
    private Date                     expirationDateRescuePassword;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private AddressBean              address;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "billingaddress_id")
    private AddressBean              billingAddress;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "personalDataBean", cascade = CascadeType.ALL)
    private Set<HistoryPasswordBean> historyPasswordBeanData = new HashSet<HistoryPasswordBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "personalDataBean", cascade = CascadeType.ALL)
    private Set<TermsOfServiceBean>  termsOfServiceBeanData  = new HashSet<TermsOfServiceBean>(0);

    public PersonalDataBean() {}

    public PersonalDataBean(PersonalData personalData) {

        this.id = personalData.getId();
        this.firstName = personalData.getFirstName();
        this.lastName = personalData.getLastName();
        this.fiscalCode = personalData.getFiscalCode();
        this.birthDate = personalData.getBirthDate();
        this.birthMunicipality = personalData.getBirthMunicipality();
        this.birthProvince = personalData.getBirthProvince();
        this.language = personalData.getLanguage();
        this.sex = personalData.getSex();
        this.securityDataEmail = personalData.getSecurityDataEmail();
        this.securityDataPassword = personalData.getSecurityDataPassword();

        if (personalData.getAddress() != null) {
            this.address = new AddressBean(personalData.getAddress());
        }
        else {
            this.address = null;
        }

        if (personalData.getBillingAddress() != null) {
            this.billingAddress = new AddressBean(personalData.getBillingAddress());
        }
        else {
            this.billingAddress = null;
        }

        if (personalData.getTermsOfServiceData() != null) {
            Set<TermsOfService> termsOfServiceData = personalData.getTermsOfServiceData();
            for (TermsOfService termsOfService : termsOfServiceData) {
                TermsOfServiceBean termsOfServiceBean = new TermsOfServiceBean(termsOfService);
                termsOfServiceBean.setPersonalDataBean(this);
                this.termsOfServiceBeanData.add(termsOfServiceBean);
            }
        }

    }

    public PersonalData toPersonalData() {

        PersonalData personalData = new PersonalData();

        personalData.setId(this.id);
        personalData.setFirstName(this.firstName);
        personalData.setLastName(this.lastName);
        personalData.setFiscalCode(this.fiscalCode);
        personalData.setBirthDate(this.birthDate);
        personalData.setBirthMunicipality(this.birthMunicipality);
        personalData.setBirthProvince(this.birthProvince);
        personalData.setLanguage(this.language);
        personalData.setSex(this.sex);
        personalData.setSecurityDataEmail(this.securityDataEmail);
        personalData.setSecurityDataPassword(this.securityDataPassword);

        if (this.address != null) {
            personalData.setAddress(this.address.toAddress());
        }
        else {
            personalData.setAddress(null);
        }

        if (this.billingAddress != null) {
            personalData.setBillingAddress(this.billingAddress.toAddress());
        }
        else {
            personalData.setBillingAddress(null);
        }

        if (this.termsOfServiceBeanData != null) {
            Set<TermsOfServiceBean> termsOfServiceData = this.termsOfServiceBeanData;
            for (TermsOfServiceBean termsOfServiceBean : termsOfServiceData) {
                TermsOfService termsOfService = termsOfServiceBean.toTermsOfService();
                termsOfService.setPersonalData(personalData);
                personalData.getTermsOfServiceData().add(termsOfService);

            }
        }

        return personalData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthMunicipality() {
        return birthMunicipality;
    }

    public void setBirthMunicipality(String birthMunicipality) {
        this.birthMunicipality = birthMunicipality;
    }

    public String getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(String birthProvince) {
        this.birthProvince = birthProvince;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSecurityDataEmail() {
        return securityDataEmail;
    }

    public void setSecurityDataEmail(String securityDataEmail) {
        this.securityDataEmail = securityDataEmail;
    }

    public String getSecurityDataPassword() {
        return securityDataPassword;
    }

    public void setSecurityDataPassword(String securityDataPassword) {
        this.securityDataPassword = securityDataPassword;
    }

    public String getRescuePassword() {
        return rescuePassword;
    }

    public void setRescuePassword(String rescuePassword) {
        this.rescuePassword = rescuePassword;
    }

    public Date getExpirationDateRescuePassword() {
        return expirationDateRescuePassword;
    }

    public void setExpirationDateRescuePassword(Date expirationDateRescuePassword) {
        this.expirationDateRescuePassword = expirationDateRescuePassword;
    }

    public AddressBean getAddress() {
        return address;
    }

    public void setAddress(AddressBean address) {
        this.address = address;
    }

    public AddressBean getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(AddressBean billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Set<HistoryPasswordBean> getHistoryPasswordBeanData() {
        return historyPasswordBeanData;
    }

    public void setHistoryPasswordBeanData(Set<HistoryPasswordBean> historyPasswordBeanData) {
        this.historyPasswordBeanData = historyPasswordBeanData;
    }

    public Set<TermsOfServiceBean> getTermsOfServiceBeanData() {
        return termsOfServiceBeanData;
    }

    public void setTermsOfServiceBeanData(Set<TermsOfServiceBean> termsOfServiceBeanData) {
        this.termsOfServiceBeanData = termsOfServiceBeanData;
    }

}
