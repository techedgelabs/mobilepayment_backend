package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.UnavailabilityPeriod;

@Entity
@Table(name = "UNAVAILABILITY_PERIOD")
@NamedQueries({ @NamedQuery(name = "UnavailabilityPeriodBean.findByCode", query = "select up from UnavailabilityPeriodBean up where up.code = :code"),
        @NamedQuery(name = "UnavailabilityPeriodBean.findAll", query = "select up from UnavailabilityPeriodBean up"),
        @NamedQuery(name = "UnavailabilityPeriodBean.findActive", query = "select up from UnavailabilityPeriodBean up where up.active = true"),
        @NamedQuery(name = "UnavailabilityPeriodBean.findByActive", query = "select up from UnavailabilityPeriodBean up where up.active = :active")})
public class UnavailabilityPeriodBean {
    public static final String FIND_BY_CODE = "UnavailabilityPeriodBean.findByCode";
    public static final String FIND_ALL     = "UnavailabilityPeriodBean.findAll";
    public static final String FIND_ACTIVE  = "UnavailabilityPeriodBean.findActive";
    public static final String FIND_BY_ACTIVE  = "UnavailabilityPeriodBean.findByActive";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "code", nullable = true)
    private String             code;

    @Column(name = "start_date", nullable = true)
    private String             startDate;

    @Column(name = "end_date", nullable = true)
    private String             endDate;

    @Column(name = "start_time", nullable = true)
    private String             startTime;

    @Column(name = "end_time", nullable = true)
    private String             endTime;

    @Column(name = "operation", nullable = true)
    private String             operation;

    @Column(name = "active", nullable = true)
    private Boolean            active;

    @Column(name = "status_code", nullable = true)
    private String             statusCode;

    @Column(name = "status_message", nullable = true)
    private String             statusMessage;

    public UnavailabilityPeriodBean() {}

    public UnavailabilityPeriodBean(UnavailabilityPeriod unavailabilityPeriod) {

        this.id = unavailabilityPeriod.getId();
        this.code = unavailabilityPeriod.getCode();
        this.startDate = unavailabilityPeriod.getStartDate();
        this.endDate = unavailabilityPeriod.getEndDate();
        this.startTime = unavailabilityPeriod.getStartTime();
        this.endTime = unavailabilityPeriod.getEndTime();
        this.operation = unavailabilityPeriod.getOperation();
        this.active = unavailabilityPeriod.getActive();
        this.statusCode = unavailabilityPeriod.getStatusCode();
        this.statusMessage = unavailabilityPeriod.getStatusMessage();
    }

    public UnavailabilityPeriod toUnavailabilityPeriod() {

        UnavailabilityPeriod unavailabilityPeriod = new UnavailabilityPeriod();

        unavailabilityPeriod.setId(this.id);
        unavailabilityPeriod.setCode(this.code);
        unavailabilityPeriod.setStartDate(this.startDate);
        unavailabilityPeriod.setEndDate(this.endDate);
        unavailabilityPeriod.setStartTime(this.startTime);
        unavailabilityPeriod.setEndTime(this.endTime);
        unavailabilityPeriod.setOperation(this.operation);
        unavailabilityPeriod.setActive(this.active);
        unavailabilityPeriod.setStatusCode(this.statusCode);
        unavailabilityPeriod.setStatusMessage(this.statusMessage);

        return unavailabilityPeriod;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

}
