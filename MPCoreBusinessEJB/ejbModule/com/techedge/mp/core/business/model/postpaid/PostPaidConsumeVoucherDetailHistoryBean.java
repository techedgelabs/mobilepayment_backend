package com.techedge.mp.core.business.model.postpaid;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucherDetail;

@Entity
@Table(name = "POSTPAIDCONSUMEVOUCHERDETAILHISTORY")
public class PostPaidConsumeVoucherDetailHistoryBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                              id;

    @Column(name = "voucherCode", nullable = true)
    private String                            voucherCode;

    @Column(name = "voucherStatus", nullable = true)
    private String                            voucherStatus;

    @Column(name = "voucherType", nullable = true)
    private String                            voucherType;

    @Column(name = "voucherValue", nullable = true)
    private Double                            voucherValue;

    @Column(name = "initialValue", nullable = true)
    private Double                            initialValue;

    @Column(name = "consumedValue", nullable = true)
    private Double                            consumedValue;

    @Column(name = "voucherBalanceDue", nullable = true)
    private Double                            voucherBalanceDue;

    @Column(name = "expirationDate", nullable = true)
    private Date                              expirationDate;

    @Column(name = "promoCode", nullable = true)
    private String                            promoCode;

    @Column(name = "promoDescription", nullable = true)
    private String                            promoDescription;

    @Column(name = "promoDoc", nullable = true)
    private String                            promoDoc;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "postPaidConsumeVoucherHistoryBean", nullable = false)
    private PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean;

    public PostPaidConsumeVoucherDetailHistoryBean() {}

    public PostPaidConsumeVoucherDetailHistoryBean(PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean) {

        this.voucherCode = postPaidConsumeVoucherDetailBean.getVoucherCode();
        this.voucherStatus = postPaidConsumeVoucherDetailBean.getVoucherStatus();
        this.voucherType = postPaidConsumeVoucherDetailBean.getVoucherType();
        this.voucherValue = postPaidConsumeVoucherDetailBean.getVoucherValue();
        this.initialValue = postPaidConsumeVoucherDetailBean.getInitialValue();
        this.consumedValue = postPaidConsumeVoucherDetailBean.getConsumedValue();
        this.voucherBalanceDue = postPaidConsumeVoucherDetailBean.getVoucherBalanceDue();
        this.expirationDate = postPaidConsumeVoucherDetailBean.getExpirationDate();
        this.promoCode = postPaidConsumeVoucherDetailBean.getPromoCode();
        this.promoDescription = postPaidConsumeVoucherDetailBean.getPromoDescription();
        this.promoDoc = postPaidConsumeVoucherDetailBean.getPromoDoc();
    }

    public PostPaidConsumeVoucherDetail toPostPaidConsumeVoucherDetail() {

        PostPaidConsumeVoucherDetail postPaidConsumeVoucherDetail = new PostPaidConsumeVoucherDetail();
        postPaidConsumeVoucherDetail.setVoucherCode(this.voucherCode);
        postPaidConsumeVoucherDetail.setVoucherStatus(this.voucherStatus);
        postPaidConsumeVoucherDetail.setVoucherType(this.voucherType);
        postPaidConsumeVoucherDetail.setVoucherValue(this.voucherValue);
        postPaidConsumeVoucherDetail.setInitialValue(this.initialValue);
        postPaidConsumeVoucherDetail.setConsumedValue(this.consumedValue);
        postPaidConsumeVoucherDetail.setVoucherBalanceDue(this.voucherBalanceDue);
        postPaidConsumeVoucherDetail.setExpirationDate(this.expirationDate);
        postPaidConsumeVoucherDetail.setPromoCode(this.promoCode);
        postPaidConsumeVoucherDetail.setPromoDescription(this.promoDescription);
        postPaidConsumeVoucherDetail.setPromoDoc(this.promoDoc);
        return postPaidConsumeVoucherDetail;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public Double getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(Double voucherValue) {
        this.voucherValue = voucherValue;
    }

    public Double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Double initialValue) {
        this.initialValue = initialValue;
    }

    public Double getConsumedValue() {
        return consumedValue;
    }

    public void setConsumedValue(Double consumedValue) {
        this.consumedValue = consumedValue;
    }

    public Double getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Double voucherBalanceDue) {
        this.voucherBalanceDue = voucherBalanceDue;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {
        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {
        this.promoDoc = promoDoc;
    }

    public PostPaidConsumeVoucherHistoryBean getPostPaidConsumeVoucherHistoryBean() {
        return postPaidConsumeVoucherHistoryBean;
    }

    public void setPostPaidConsumeVoucherHistoryBean(PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean) {
        this.postPaidConsumeVoucherHistoryBean = postPaidConsumeVoucherHistoryBean;
    }

}
