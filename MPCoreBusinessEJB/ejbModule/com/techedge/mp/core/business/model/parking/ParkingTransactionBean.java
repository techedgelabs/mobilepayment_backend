package com.techedge.mp.core.business.model.parking;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransaction;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransactionItem;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransactionStatus;
import com.techedge.mp.core.business.model.ParkingTransactionInterfaceBean;
import com.techedge.mp.core.business.model.UserBean;

@Entity
@Table(name = "PARKING_TRANSACTION")
@NamedQueries({
    @NamedQuery(name = "ParkingTransactionBean.findByParkingTransactionId", query = "select pt from ParkingTransactionBean pt where pt.userBean = :userBean and pt.parkingTransactionId = :parkingTransactionId"),
    @NamedQuery(name = "ParkingTransactionBean.findActive", query = "select pt from ParkingTransactionBean pt where pt.userBean = :userBean and pt.userNotified = false and ( pt.status = :statusStarted or pt.status = :statusEnded )"),
    @NamedQuery(name = "ParkingTransactionBean.findToBeClosed", query = "select pt from ParkingTransactionBean pt where pt.status = :status and pt.estimatedParkingEndTime < :maxEstimatedParkingEndTime )"),
    @NamedQuery(name = "ParkingTransactionBean.findToBeReconciled", query = "select pt from ParkingTransactionBean pt where pt.status = :status and pt.toReconcile = true )"),
    @NamedQuery(name = "ParkingTransactionBean.findParkingTransactionSuccessfulByUserAndDate", query = "select p from ParkingTransactionBean p where p.userBean = :userBean "
            + "and ( p.timestamp >= :startDate and p.timestamp < :endDate) and p.status = 'ENDED' and p.finalPrice > 0 order by p.timestamp desc"),
    
    @NamedQuery(name = "ParkingTransactionBean.statisticsReportSynthesis", query = "select "
            + "(select count(p1.id) from ParkingTransactionBean p1 where p1.status = '" + StatusHelper.PARKING_STATUS_ENDED + "' and p1.finalPrice > 0 "
            + "and (p1.timestamp >= :dailyStartDate and p1.timestamp < :dailyEndDate)), "
            + "(select count(p2.id) from ParkingTransactionBean p2 where p2.status = '" + StatusHelper.PARKING_STATUS_ENDED + "' and p2.finalPrice > 0 "
            + "and (p2.timestamp >= :weeklyStartDate and p2.timestamp < :weeklyEndDate)), "
            + "(select count(p3.id) from ParkingTransactionBean p3 where p3.status = '" + StatusHelper.PARKING_STATUS_ENDED + "' and p3.finalPrice > 0 "
            + "and p3.timestamp >= :totalStartDate and p3.timestamp < :dailyEndDate) "
            + "from ParkingTransactionBean p"),
            
    @NamedQuery(name = "ParkingTransactionBean.statisticsReportSynthesisAll", query = "select "
            + "count(p3.id) from ParkingTransactionBean p3 where p3.status = '" + StatusHelper.PARKING_STATUS_ENDED + "' and p3.finalPrice > 0 "
            + "and p3.timestamp >= :totalStartDate and p3.timestamp < :dailyEndDate"),
    
    @NamedQuery(name = "ParkingTransactionBean.statisticsReportSynthesisTotalAmount", query = "select "
            + "(select case when sum(p1.finalPrice) is null then 0.00 else sum(p1.finalPrice) end from ParkingTransactionBean p1 " + "where p1.status = '"
            + StatusHelper.PARKING_STATUS_ENDED + "' "
            + "and (p1.timestamp >= :dailyStartDate and p1.timestamp < :dailyEndDate)), "
            + "(select case when sum(p2.finalPrice) is null then 0.00 else sum(p2.finalPrice) end from ParkingTransactionBean p2 " + "where p2.status = '"
            + StatusHelper.PARKING_STATUS_ENDED + "' "
            + "and (p2.timestamp >= :weeklyStartDate and p2.timestamp < :weeklyEndDate)), "
            + "(select case when sum(p3.finalPrice) is null then 0.00 else sum(p3.finalPrice) end from ParkingTransactionBean p3 " + "where p3.status = '"
            + StatusHelper.PARKING_STATUS_ENDED + "' "
            + "and p3.timestamp >= :totalStartDate and p3.timestamp < :dailyEndDate) "
            + "from ParkingTransactionBean p"),
            
    @NamedQuery(name = "ParkingTransactionBean.statisticsReportSynthesisAllTotalAmount", query = "select "
            + "case when sum(p3.finalPrice) is null then 0.00 else sum(p3.finalPrice) end from ParkingTransactionBean p3 " + "where p3.status = '"
            + StatusHelper.PARKING_STATUS_ENDED + "' "
            + "and p3.timestamp >= :totalStartDate and p3.timestamp < :dailyEndDate")
})
public class ParkingTransactionBean implements ParkingTransactionInterfaceBean{

    public static final String              FIND_BY_PARKING_TRANSACTION_ID           	= "ParkingTransactionBean.findByParkingTransactionId";
    public static final String              FIND_ACTIVE_BY_USER                      	= "ParkingTransactionBean.findActive";
    public static final String              FIND_TRANSACTION_TO_BE_CLOSED            	= "ParkingTransactionBean.findToBeClosed";
    public static final String              FIND_TRANSACTION_TO_BE_RECONCILED        	= "ParkingTransactionBean.findToBeReconciled";
    public static final String              FIND_SUCCESSFUL_BY_USER_AND_DATE         	= "ParkingTransactionBean.findParkingTransactionSuccessfulByUserAndDate";
    public static final String              STATISTICS_REPORT_SYNTHESIS              	= "ParkingTransactionBean.statisticsReportSynthesis";
    public static final String              STATISTICS_REPORT_SYNTHESIS_ALL           	= "ParkingTransactionBean.statisticsReportSynthesisAll";
    public static final String              STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT 	 = "ParkingTransactionBean.statisticsReportSynthesisTotalAmount";
    public static final String              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT = "ParkingTransactionBean.statisticsReportSynthesisAllTotalAmount";
    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                            id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserBean                        userBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parkingTransactionBean", cascade = CascadeType.ALL)
    private Set<ParkingTransactionItemBean> parkingTransactionItemList     = new HashSet<ParkingTransactionItemBean>(0);
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parkingTransactionBean", cascade = CascadeType.ALL)
    private Set<ParkingTransactionStatusBean> parkingTransactionStatusList    = new HashSet<ParkingTransactionStatusBean>(0);

    @Column(name = "plate_number", nullable = true)
    private String                          plateNumber;
    
    @Column(name = "plate_number_description", nullable = true)
    private String                          plateNumberDescription;

    @Column(name = "timestamp", nullable = true)
    private Timestamp                       timestamp;

    @Column(name = "status", nullable = true)
    private String                          status;

    @Column(name = "parking_zone_id", nullable = false)
    private String                          parkingZoneId;

    @Column(name = "city_id", nullable = false)
    private String                          cityId;

    @Column(name = "city_name", nullable = true)
    private String                          cityName;

    @Column(name = "administrative_area_level_code", nullable = true)
    private String                          administrativeAreaLevel2Code;
    
    @Column(name = "parking_zone_name", nullable = true)
    private String                          parkingZoneName;

    @Column(name = "parking_zone_description", nullable = true)
    private String                          parkingZoneDescription;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "parking_zone_notice", nullable = true)
    private String                          parkingZoneNotice;

    @Column(name = "stall_code_required", nullable = true)
    private Boolean                         stallCodeRequired;

    @Column(name = "user_notified", nullable = true)
    private Boolean                         userNotified;

    @Column(name = "parking_transaction_id", nullable = true)
    private String                          parkingTransactionId;

    @Column(name = "parking_id", nullable = true)
    private String                          parkingId;

    @Column(name = "stall_code", nullable = true)
    private String                          stallCode;

    @Column(name = "sticker_required", nullable = true)
    private Boolean                         stickerRequired;

    @Column(name = "sticker_required_notice", nullable = true)
    private String                          stickerRequiredNotice;

    @Column(name = "final_price", nullable = true)
    private BigDecimal                      finalPrice;
    
    @Column(name = "final_parking_end_time", nullable = true)
    private Date                            finalParkingEndTime;
    
    @Column(name = "estimated_parking_end_time", nullable = true)
    private Date                            estimatedParkingEndTime;
    
    @Column(name = "to_reconcile", nullable = true)
    private Boolean                         toReconcile;
    
    @Column(name = "reconciliation_attempts_left", nullable = true)
    private Integer                        reconciliationAttemptsLeft;

    public ParkingTransactionBean() {}

    public ParkingTransaction toParkingTransaction() {

        ParkingTransaction parkingTransaction = new ParkingTransaction();
        parkingTransaction.setUser(this.userBean.toUser());
        parkingTransaction.setId(this.id);
        parkingTransaction.setParkingId(this.parkingId);
        parkingTransaction.setParkingTransactionId(this.parkingTransactionId);
        parkingTransaction.setParkingZoneDescription(this.parkingZoneDescription);
        parkingTransaction.setParkingZoneId(this.parkingZoneId);
        parkingTransaction.setCityId(this.cityId);
        parkingTransaction.setCityName(this.cityName);
        parkingTransaction.setAdministrativeAreaLevel2Code(this.administrativeAreaLevel2Code);
        parkingTransaction.setParkingZoneName(this.parkingZoneName);
        parkingTransaction.setParkingZoneNotice(this.parkingZoneNotice);
        parkingTransaction.setPlateNumber(this.plateNumber);
        parkingTransaction.setPlateNumberDescription(this.plateNumberDescription);
        parkingTransaction.setStallCode(this.stallCode);
        parkingTransaction.setStallCodeRequired(this.stallCodeRequired);
        parkingTransaction.setUserNotified(this.userNotified);
        parkingTransaction.setStatus(this.status);
        parkingTransaction.setTimestamp(this.timestamp);
        parkingTransaction.setStickerRequired(this.stickerRequired);
        parkingTransaction.setStickerRequiredNotice(this.stickerRequiredNotice);
        parkingTransaction.setFinalPrice(this.finalPrice);
        parkingTransaction.setFinalParkingEndTime(this.finalParkingEndTime);
        parkingTransaction.setEstimatedParkingEndTime(this.estimatedParkingEndTime);
        parkingTransaction.setToReconcile(this.toReconcile);
        
        for(ParkingTransactionItemBean parkingTransactionItemBean : this.parkingTransactionItemList) {
            
            ParkingTransactionItem parkingTransactionItem = parkingTransactionItemBean.toParkingTransactionItem();
            parkingTransactionItem.setParkingTransaction(parkingTransaction);
            parkingTransaction.getParkingTransactionItemList().add(parkingTransactionItem);
        }
        
        for(ParkingTransactionStatusBean parkingTransactionStatusBean : this.parkingTransactionStatusList) {
            
            ParkingTransactionStatus parkingTransactionStatus = parkingTransactionStatusBean.toParkingTransactionStatus();
            parkingTransactionStatus.setParkingTransaction(parkingTransaction);
            parkingTransaction.getParkingTransactionStatusList().add(parkingTransactionStatus);
        }

        return parkingTransaction;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getPlateNumberDescription() {
        return plateNumberDescription;
    }

    public void setPlateNumberDescription(String plateNumberDescription) {
        this.plateNumberDescription = plateNumberDescription;
    }

    public String getParkingZoneId() {
        return parkingZoneId;
    }

    public void setParkingZoneId(String parkingZoneId) {
        this.parkingZoneId = parkingZoneId;
    }

    public String getParkingZoneName() {
        return parkingZoneName;
    }

    public void setParkingZoneName(String parkingZoneName) {
        this.parkingZoneName = parkingZoneName;
    }

    public String getParkingZoneDescription() {
        return parkingZoneDescription;
    }

    public void setParkingZoneDescription(String parkingZoneDescription) {
        this.parkingZoneDescription = parkingZoneDescription;
    }

    public String getParkingZoneNotice() {
        return parkingZoneNotice;
    }

    public void setParkingZoneNotice(String parkingZoneNotice) {
        this.parkingZoneNotice = parkingZoneNotice;
    }

    public Boolean getStallCodeRequired() {
        return stallCodeRequired;
    }

    public void setStallCodeRequired(Boolean stallCodeRequired) {
        this.stallCodeRequired = stallCodeRequired;
    }

    public Boolean getUserNotified() {
        return userNotified;
    }

    public void setUserNotified(Boolean userNotified) {
        this.userNotified = userNotified;
    }

    public String getParkingTransactionId() {
        return parkingTransactionId;
    }

    public void setParkingTransactionId(String parkingTransactionId) {
        this.parkingTransactionId = parkingTransactionId;
    }

    public String getParkingId() {
        return parkingId;
    }

    public void setParkingId(String parkingId) {
        this.parkingId = parkingId;
    }

    public String getStallCode() {
        return stallCode;
    }

    public void setStallCode(String stallCode) {
        this.stallCode = stallCode;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public Boolean getStickerRequired() {
        return stickerRequired;
    }

    public void setStickerRequired(Boolean stickerRequired) {
        this.stickerRequired = stickerRequired;
    }

    public String getStickerRequiredNotice() {
        return stickerRequiredNotice;
    }

    public void setStickerRequiredNotice(String stickerRequiredNotice) {
        this.stickerRequiredNotice = stickerRequiredNotice;
    }

    public Set<ParkingTransactionItemBean> getParkingTransactionItemList() {
        return parkingTransactionItemList;
    }

    public void setParkingTransactionItemList(Set<ParkingTransactionItemBean> parkingTransactionItemList) {
        this.parkingTransactionItemList = parkingTransactionItemList;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAdministrativeAreaLevel2Code() {
        return administrativeAreaLevel2Code;
    }

    public void setAdministrativeAreaLevel2Code(String administrativeAreaLevel2Code) {
        this.administrativeAreaLevel2Code = administrativeAreaLevel2Code;
    }

    public Date getFinalParkingEndTime() {
        return finalParkingEndTime;
    }

    public void setFinalParkingEndTime(Date finalParkingEndTime) {
        this.finalParkingEndTime = finalParkingEndTime;
    }

    public Boolean getToReconcile() {
        return toReconcile;
    }

    public void setToReconcile(Boolean toReconcile) {
        this.toReconcile = toReconcile;
    }

    public Date getEstimatedParkingEndTime() {
        return estimatedParkingEndTime;
    }

    public void setEstimatedParkingEndTime(Date estimatedParkingEndTime) {
        this.estimatedParkingEndTime = estimatedParkingEndTime;
    }

    public Set<ParkingTransactionStatusBean> getParkingTransactionStatusList() {
        return parkingTransactionStatusList;
    }

    public void setParkingTransactionStatusList(Set<ParkingTransactionStatusBean> parkingTransactionStatusList) {
        this.parkingTransactionStatusList = parkingTransactionStatusList;
    }

    public Integer getLastStatusSequenceID() {
        Integer lastSequenceId = 0;
        
        if (this.parkingTransactionStatusList != null && !parkingTransactionStatusList.isEmpty()) {
            
            for(ParkingTransactionStatusBean parkingTransactionStatusBean : this.parkingTransactionStatusList) {
                if (parkingTransactionStatusBean.getSequenceID() > lastSequenceId) {
                    lastSequenceId = parkingTransactionStatusBean.getSequenceID();
                }
            }
        }
        
        return lastSequenceId;
    }

    @Override
    public String getFinalStatus() {
        return this.status;
    }

    @Override
    public void setFinalStatus(String status) {
        this.status = status;
        
    }

    @Override
    public void setToReconcilie(Boolean toReconcilie) {
        this.toReconcile = toReconcilie;
        
    }

    @Override
    public Boolean getToReconcilie() {
        return this.toReconcile;
    }

    @Override
    public Integer getReconciliationAttemptsLeft() {
        return this.reconciliationAttemptsLeft;
    }

    @Override
    public void setReconciliationAttemptsLeft(Integer attemptsLeft) {
        this.reconciliationAttemptsLeft = attemptsLeft;
    }

}
