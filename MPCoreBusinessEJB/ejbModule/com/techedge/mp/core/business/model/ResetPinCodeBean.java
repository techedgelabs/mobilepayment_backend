package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "RESET_PIN_CODE")
@NamedQueries({
    @NamedQuery(name="ResetPinCodeBean.find", query="select rpc from ResetPinCodeBean rpc where rpc.code = :code and rpc.user = :userBean and rpc.status = :status and rpc.expirationTimestamp >= :now"),
})
public class ResetPinCodeBean {

    public static final Integer STATUS_NEW                 = 1;
    public static final Integer STATUS_USED                = 2;

    public static final String  FIND = "ResetPinCodeBean.find";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                id;

    @Column(name = "code", nullable = false, length = 10)
    private String              code;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "user_id")
    private UserBean            user;

    @Column(name = "status", nullable = false, length = 10)
    private Integer             status;

    @Column(name = "creation_timestamp", nullable = false)
    private Date                creationTimestamp;

    @Column(name = "lastused_timestamp", nullable = false)
    private Date                lastUsedTimestamp;

    @Column(name = "expiration_timestamp", nullable = false)
    private Date                expirationTimestamp;

    public ResetPinCodeBean() {}

    public ResetPinCodeBean(String code, UserBean user, Integer status, Date creationTimeStamp, Date lastUsedTimestamp, Date expirationTimestamp) {

        this.code = code;
        this.user = user;
        this.status = status;
        this.creationTimestamp = creationTimeStamp;
        this.lastUsedTimestamp = lastUsedTimestamp;
        this.expirationTimestamp = expirationTimestamp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getLastUsedTimestamp() {
        return lastUsedTimestamp;
    }

    public void setLastUsedTimestamp(Date lastUsedTimestamp) {
        this.lastUsedTimestamp = lastUsedTimestamp;
    }

    public Date getExpirationTimestamp() {
        return expirationTimestamp;
    }

    public void setExpirationTimestamp(Date expirationTimestamp) {
        this.expirationTimestamp = expirationTimestamp;
    }

    public Boolean isValid() {

        if (status != STATUS_NEW) {
            return false;
        }

        Date now = new Date();
        if (this.expirationTimestamp.before(now)) {
            // Verification code scaduto
            return false;
        }

        return true;
    }

}
