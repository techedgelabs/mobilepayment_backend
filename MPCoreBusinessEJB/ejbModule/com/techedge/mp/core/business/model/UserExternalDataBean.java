package com.techedge.mp.core.business.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.UserExternalData;

@Entity
@Table(name = "USER_EXTERNAL_DATA")
@NamedQueries({ @NamedQuery(name = "UserExternalDataBean.findExternalUserByUUIDAndProvider", query = "select uxdb from UserExternalDataBean uxdb where UPPER(uxdb.uuid) = UPPER(:uuid) and uxdb.provider = :provider and uxdb.userBean.userStatus != 9") })
public class UserExternalDataBean {

    public static final String FIND_EXTERNAL_USER_BY_UUID_AND_PROVIDER = "UserExternalDataBean.findExternalUserByUUIDAndProvider";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "provider", nullable = false, length = 40)
    private String             provider;

    @Column(name = "uuid", nullable = false, length = 50)
    private String             uuid;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "user_id")
    private UserBean           userBean;

    public UserExternalDataBean() {}

    public UserExternalDataBean(UserExternalData userMulticardData) {
        this.id = userMulticardData.getId();
        this.provider = userMulticardData.getProvider();

    }

    public UserExternalData toUserData() {
        UserExternalData userExternalData = new UserExternalData();
        userExternalData.setId(this.id);
        userExternalData.setProvider(this.provider);
        userExternalData.setUuid(this.uuid);

        return userExternalData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

}
