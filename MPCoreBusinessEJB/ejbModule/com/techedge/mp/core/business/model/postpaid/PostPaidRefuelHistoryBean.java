package com.techedge.mp.core.business.model.postpaid;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShopRefuelDetail;

@Entity
@Table(name = "POSTPAIDREFUEL_HISTORY")
//@NamedQueries({
//	@NamedQuery(name="TransactionBean.findTransactionByID", query="select t from TransactionBean t where t.transactionID = :transactionID"),
//	@NamedQuery(name="TransactionBean.findTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean"),
//	@NamedQuery(name="TransactionBean.findTransactionByUserAndDate", query="select t from TransactionBean t where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),
//	@NamedQuery(name="TransactionBean.findTransactionSuccessfulByUserAndDate", query="select t from TransactionBean t where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) and t.finalStatusType = 'SUCCESSFUL' order by t.creationTimestamp desc"),
//	//@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and (( t.finalStatusType is null ) or ( t.finalStatusType <> 'FAILED' and t.finalStatusType <> 'SUCCESSFUL' and t.finalStatusType <> 'MISSING_NOTIFICATION' ))")
//	@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and t.confirmed = false"),
//	@NamedQuery(name="TransactionBean.findActiveTransaction", query="select t from TransactionBean t where t.confirmed = false"),
//	@NamedQuery(name="TransactionBean.findActiveTransactionNew", query="select t from TransactionBean t where t.finalStatusType = null"),
//	@NamedQuery(name="TransactionBean.findTransactionByFinalStatusType", query="select t from TransactionBean t where t.finalStatusType = :finalStatusType"),
//	@NamedQuery(name="TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus", query="select t from TransactionBean t where t.finalStatusType = :finalStatusType and t.GFGNotification = :gfgNotification"),
//})
public class PostPaidRefuelHistoryBean {

    //	public static final String FIND_BY_ID = "TransactionBean.findTransactionByID";
    //	public static final String FIND_BY_USER = "TransactionBean.findTransactionByUser";
    //	public static final String FIND_ACTIVE_BY_USER = "TransactionBean.findActiveTransactionByUser";
    //	public static final String FIND_ACTIVE = "TransactionBean.findActiveTransaction";
    //	public static final String FIND_ACTIVE_NEW = "TransactionBean.findActiveTransactionNew";
    //	public static final String FIND_BY_FINAL_STATUS_TYPE = "TransactionBean.findTransactionByFinalStatusType";
    //	public static final String FIND_BY_FINAL_STATUS_TYPE_AND_NOTIFICATION = "TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus";
    //	public static final String FIND_BY_USER_AND_DATE = "TransactionBean.findTransactionByUserAndDate";
    //	public static final String FIND_SUCCESSFUL_BY_USER_AND_DATE = "TransactionBean.findTransactionSuccessfulByUserAndDate";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                           id;

    @Column(name = "fuelType", nullable = true)
    private String                         fuelType;

    @Column(name = "productId", nullable = true)
    private String                         productId;

    @Column(name = "productDescription", nullable = true)
    private String                         productDescription;

    @Column(name = "fuelQuantity", nullable = true)
    private Double                         fuelQuantity;

    @Column(name = "fuelAmount", nullable = true)
    private Double                         fuelAmount;
    
    @Column(name = "unitPrice", nullable = true)
    private Double                         unitPrice;

    @Column(name = "pumpId", nullable = true)
    private String                         pumpId;

    @Column(name = "pumpNumber", nullable = true)
    private Integer                        pumpNumber;

    @Column(name = "refuelMode", nullable = true)
    private String                         refuelMode;

    @Column(name = "timestampEndRefuel", nullable = true)
    private Date                           timestampEndRefuel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactionID", nullable = false)
    private PostPaidTransactionHistoryBean postPaidTransactionHistoryBean;

    public PostPaidRefuelHistoryBean() {}

    public PostPaidRefuelHistoryBean(PostPaidRefuelBean postPaidRefuelBean) {
        this.fuelType = postPaidRefuelBean.getFuelType();
        this.fuelQuantity = postPaidRefuelBean.getFuelQuantity();
        this.fuelAmount = postPaidRefuelBean.getFuelAmount();
        this.productDescription = postPaidRefuelBean.getProductDescription();
        this.productId = postPaidRefuelBean.getProductId();
        this.pumpId = postPaidRefuelBean.getPumpId();
        if (postPaidRefuelBean.getPumpNumber() != null) {
            this.pumpNumber = Integer.valueOf(postPaidRefuelBean.getPumpNumber());
        }
        this.refuelMode = postPaidRefuelBean.getRefuelMode();
        this.unitPrice = postPaidRefuelBean.getUnitPrice();
        this.timestampEndRefuel = postPaidRefuelBean.getTimestampEndRefuel();
    }

    public PostPaidRefuel toPostPaidRefuel() {

        PostPaidRefuel postPaidRefuel = new PostPaidRefuel();
        postPaidRefuel.setFuelType(this.fuelType);
        postPaidRefuel.setFuelQuantity(this.fuelQuantity);
        postPaidRefuel.setFuelAmount(this.fuelAmount);
        postPaidRefuel.setProductDescription(this.productDescription);
        postPaidRefuel.setProductId(this.productId);
        postPaidRefuel.setPumpId(this.pumpId);
        postPaidRefuel.setPumpNumber(this.pumpNumber);
        postPaidRefuel.setRefuelMode(this.refuelMode);
        postPaidRefuel.setTimestampEndRefuel(this.timestampEndRefuel);
        postPaidRefuel.setUnitPrice(this.unitPrice);
        return postPaidRefuel;
    }

    public PostPaidRefuelHistoryBean(ShopRefuelDetail shopRefuelDetail, PostPaidTransactionHistoryBean postPaidTransactionHistoryBean) {
        this.fuelType = shopRefuelDetail.getFuelType();
        this.fuelQuantity = shopRefuelDetail.getFuelQuantity();
        this.fuelAmount = shopRefuelDetail.getAmount();
        this.productDescription = shopRefuelDetail.getProductDescription();
        this.productId = shopRefuelDetail.getProductId();
        this.pumpId = shopRefuelDetail.getPumpID();
        if (shopRefuelDetail.getPumpNumber() != null) {
            this.pumpNumber = Integer.valueOf(shopRefuelDetail.getPumpNumber());
        }
        this.refuelMode = shopRefuelDetail.getRefuelMode();
        this.unitPrice = shopRefuelDetail.getUnitPrice();
        
        this.postPaidTransactionHistoryBean = postPaidTransactionHistoryBean;
    }

/*
 * public String getFuelType() {
 * return fuelType;
 * }
 * 
 * public void setFuelType(String fuelType) {
 * this.fuelType = fuelType;
 * }
 * 
 * public String getProductId() {
 * return productId;
 * }
 * 
 * public void setProductId(String productId) {
 * this.productId = productId;
 * }
 * 
 * public String getProductDescription() {
 * return productDescription;
 * }
 * 
 * public void setProductDescription(String productDescription) {
 * this.productDescription = productDescription;
 * }
 * 
 * public Double getFuelQuantity() {
 * return fuelQuantity;
 * }
 * 
 * public void setFuelQuantity(Double fuelQuantity) {
 * this.fuelQuantity = fuelQuantity;
 * }
 * 
 * public Double getFuelAmount() {
 * return fuelAmount;
 * }
 * 
 * public void setFuelAmount(Double fuelAmount) {
 * this.fuelAmount = fuelAmount;
 * }
 * 
 * public PostPaidTransactionHistoryBean getTransactionHistoryBean() {
 * return ppTransactionBean;
 * }
 * 
 * public void setTransactionHistoryBean(PostPaidTransactionHistoryBean ppTransactionBean) {
 * this.ppTransactionBean = ppTransactionBean;
 * }
 */

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public Double getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(Double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public PostPaidTransactionHistoryBean getPostPaidTransactionHistoryBean() {
        return postPaidTransactionHistoryBean;
    }

    public void setPostPaidTransactionHistoryBean(PostPaidTransactionHistoryBean postPaidTransactionHistoryBean) {
        this.postPaidTransactionHistoryBean = postPaidTransactionHistoryBean;
    }

    public String getPumpId() {
        return pumpId;
    }

    public void setPumpId(String pumpId) {
        this.pumpId = pumpId;
    }

    public Date getTimestampEndRefuel() {
        return timestampEndRefuel;
    }

    public void setTimestampEndRefuel(Date timestampEndRefuel) {
        this.timestampEndRefuel = timestampEndRefuel;
    }

    public Integer getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(Integer pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

}
