package com.techedge.mp.core.business.model.voucher;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.voucher.VoucherTransactionOperation;

@Entity
@Table(name = "VOUCHER_TRANSACTION_OPERATION")
@NamedQueries({ 
    @NamedQuery(name = "VoucherTransactionOperationBean.findByTransaction", 
            query = "select o from VoucherTransactionOperationBean o where o.voucherTransactionBean = :voucherTransactionBean")
    
})
public class VoucherTransactionOperationBean {

    public static final String FIND_BY_TRANSACTION               = "VoucherTransactionOperationBean.findByTransaction";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "voucher_transaction_id", nullable = false)
    private VoucherTransactionBean    voucherTransactionBean;

    @Column(name = "cs_transaction_id", nullable = true)
    private String                 csTransactionID;

    @Column(name = "operation_id", nullable = true)
    private String                 operationID;
    
    @Column(name = "operation_id_reversed", nullable = true)
    private String                 operationIDReversed;
    
    @Column(name = "operation_type", nullable = true)
    private String                 operationType;
    
    @Column(name = "request_timestamp", nullable = true)
    private Date                   requestTimestamp;
    
    @Column(name = "marketing_msg", nullable = true)
    private String                 marketingMsg;
    
    @Column(name = "warning_msg", nullable = true)
    private String                 warningMsg;
    
    @Column(name = "message_code", nullable = true)
    private String                 messageCode;
    
    @Column(name = "status_code", nullable = true)
    private String                 statusCode;
    
    @Column(name = "amount", nullable = true)
    private Double                 amount;

    public VoucherTransactionOperationBean() {}

    public VoucherTransactionOperationBean(VoucherTransactionOperation voucherTransactionOperation) {

        this.id = 0;
        this.amount = voucherTransactionOperation.getAmount();
        this.csTransactionID = voucherTransactionOperation.getCsTransactionID();
        this.marketingMsg = voucherTransactionOperation.getMarketingMsg();
        this.messageCode = voucherTransactionOperation.getMessageCode();
        this.operationID = voucherTransactionOperation.getOperationID();
        this.operationIDReversed = voucherTransactionOperation.getOperationIDReversed();
        this.operationType = voucherTransactionOperation.getOperationType();
        this.requestTimestamp = voucherTransactionOperation.getRequestTimestamp();
        this.statusCode = voucherTransactionOperation.getStatusCode();
        this.warningMsg = voucherTransactionOperation.getWarningMsg();
        this.voucherTransactionBean = new VoucherTransactionBean(voucherTransactionOperation.getVoucherTransaction());
    }

    public VoucherTransactionOperation toVoucherOperation() {

        VoucherTransactionOperation voucherTransactionOperation = new VoucherTransactionOperation();
        voucherTransactionOperation.setId(this.id);
        voucherTransactionOperation.setAmount(this.amount);
        voucherTransactionOperation.setCsTransactionID(this.csTransactionID);
        voucherTransactionOperation.setMarketingMsg(this.marketingMsg);
        voucherTransactionOperation.setMessageCode(this.messageCode);
        voucherTransactionOperation.setOperationID(this.operationID);
        voucherTransactionOperation.setOperationIDReversed(this.operationIDReversed);
        voucherTransactionOperation.setOperationType(this.operationType);
        voucherTransactionOperation.setWarningMsg(this.warningMsg);
        voucherTransactionOperation.setRequestTimestamp(this.requestTimestamp);
        voucherTransactionOperation.setStatusCode(this.statusCode);
        return voucherTransactionOperation;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public VoucherTransactionBean getVoucherTransactionBean() {
        return voucherTransactionBean;
    }

    public void setVoucherTransactionBean(VoucherTransactionBean voucherTransactionBean) {
        this.voucherTransactionBean = voucherTransactionBean;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getOperationIDReversed() {
        return operationIDReversed;
    }

    public void setOperationIDReversed(String operationIDReversed) {
        this.operationIDReversed = operationIDReversed;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }


}
