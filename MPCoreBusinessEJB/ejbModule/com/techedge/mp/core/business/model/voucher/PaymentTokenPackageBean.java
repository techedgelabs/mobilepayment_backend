package com.techedge.mp.core.business.model.voucher;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.voucher.PaymentTokenPackage;
import com.techedge.mp.core.business.interfaces.voucher.VoucherTransaction;

@Entity
@Table(name = "PAYMENT_TOKEN_PACKAGE")
/*
 * @NamedQueries({
 * 
 * @NamedQuery(name = "VoucherTransactionBean.findTransactionByID", query = "select v from VoucherTransactionBean v where v.voucherTransactionID = :voucherTransactionID"),
 * })
 */
public class PaymentTokenPackageBean {

    //public static final String             FIND_BY_ID                                           = "VoucherTransactionBean.findTransactionByID";

    public static final String        TYPE_MULTICARD = "multicard";
    public static final String        TYPE_APPLEPAY  = "applepay";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long   id;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data", nullable = true)
    private String data;
    
    @Column(name = "type", nullable = true)
    private String type;

    public PaymentTokenPackageBean() {}
    
    public PaymentTokenPackageBean(PaymentTokenPackage paymentTokenPackage) {
        
        this.id = 0;
        this.data = paymentTokenPackage.getData();
        this.type = paymentTokenPackage.getType();
    }
    
    public PaymentTokenPackage toPaymentTokenPackage() {
        
        PaymentTokenPackage paymentTokenPackage = new PaymentTokenPackage();
        paymentTokenPackage.setId(this.id);
        paymentTokenPackage.setData(this.data);
        paymentTokenPackage.setType(this.type);
        return paymentTokenPackage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
