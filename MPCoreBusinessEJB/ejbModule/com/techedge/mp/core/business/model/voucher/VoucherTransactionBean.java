package com.techedge.mp.core.business.model.voucher;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.TransactionInterfaceType;
import com.techedge.mp.core.business.interfaces.voucher.VoucherTransaction;
import com.techedge.mp.core.business.interfaces.voucher.VoucherTransactionEvent;
import com.techedge.mp.core.business.interfaces.voucher.VoucherTransactionOperation;
import com.techedge.mp.core.business.interfaces.voucher.VoucherTransactionStatus;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.voucher.EventType;

@Entity
@Table(name = "VOUCHER_TRANSACTIONS")
@NamedQueries({
        @NamedQuery(name = "VoucherTransactionBean.findTransactionByID", query = "select v from VoucherTransactionBean v where v.voucherTransactionID = :voucherTransactionID"),

        @NamedQuery(name = "VoucherTransactionBean.findTransactionByUser", query = "select v from VoucherTransactionBean v where v.userBean = :userBean"),

        @NamedQuery(name = "VoucherTransactionBean.findTransactionByUserAndDate", query = "select v from VoucherTransactionBean v where v.userBean = :userBean "
                + "and ( v.creationTimestamp >= :startDate and v.creationTimestamp < :endDate) order by v.creationTimestamp desc"),

        @NamedQuery(name = "VoucherTransactionBean.findTransactionSuccessfulByUserAndDate", query = "select v from VoucherTransactionBean v where v.userBean = :userBean "
                + "and ( v.creationTimestamp >= :startDate and v.creationTimestamp < :endDate) and v.finalStatusType = 'SUCCESSFUL' order by v.creationTimestamp desc"),

        @NamedQuery(name = "VoucherTransactionBean.findTransactionByFinalStatusType", query = "select v from VoucherTransactionBean v where v.finalStatusType = :finalStatusType"),
        
        @NamedQuery(name = "VoucherTransactionBean.findTransactionByDate", query = "select v from VoucherTransactionBean v where v.creationTimestamp >= :startDate "
                + "and v.creationTimestamp < :endDate order by v.creationTimestamp desc"),
        
        @NamedQuery(name = "VoucherTransactionBean.findTransactionByVoucherCode", query = "select v from VoucherTransactionBean v where v.voucherCode = :voucherCode") })

public class VoucherTransactionBean implements TransactionInterfaceBean {

    public static final String             FIND_BY_ID                                           = "VoucherTransactionBean.findTransactionByID";
    public static final String             FIND_BY_USER                                         = "VoucherTransactionBean.findTransactionByUser";
    public static final String             FIND_BY_USER_AND_DATE                                = "VoucherTransactionBean.findTransactionByUserAndDate";
    public static final String             FIND_SUCCESSFUL_BY_USER_AND_DATE                     = "VoucherTransactionBean.findTransactionSuccessfulByUserAndDate";
    public static final String             FIND_BY_DATE                                         = "VoucherTransactionBean.findTransactionByDate";
    public static final String             FIND_BY_FINAL_STATUS_TYPE                            = "VoucherTransactionBean.findTransactionByFinalStatusType";
    public static final String             FIND_BY_VOUCHER_CODE                                 = "VoucherTransactionBean.findTransactionByVoucherCode";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                           id;

    @Column(name = "voucher_code", nullable = true)
    private String                         voucherCode;

    @Column(name = "voucher_transaction_id", nullable = true)
    private String                         voucherTransactionID;

    @OneToOne()
    @JoinColumn(name = "user_id")
    private UserBean                       userBean;

    @Column(name = "creation_timestamp", nullable = false)
    private Date                           creationTimestamp;

    @Column(name = "end_timestamp", nullable = true)
    private Date                           endTimestamp;

    @Column(name = "amount", nullable = true)
    private Double                         amount;

    @Column(name = "currency", nullable = true)
    private String                         currency;

    @Column(name = "shop_login", nullable = true)
    private String                         shopLogin;

    @Column(name = "acquirer_id", nullable = true)
    private String                         acquirerID;

    @Column(name = "bank_transaction_id", nullable = true)
    private String                         bankTansactionID;

    @Column(name = "authorization_code", nullable = true)
    private String                         authorizationCode;

    @Column(name = "token", nullable = true)
    private String                         token;

    @Column(name = "payment_type", nullable = true)
    private String                         paymentType;

    @Column(name = "final_status_type", nullable = true)
    private String                         finalStatusType;

    @Column(name = "confirmed", nullable = false)
    private Boolean                        confirmed;

    @Column(name = "payment_method_id", nullable = true)
    private Long                           paymentMethodId;

    @Column(name = "payment_method_type", nullable = true)
    private String                         paymentMethodType;

    @Column(name = "reconciliation_attempts_left", nullable = true)
    private Integer                        reconciliationAttemptsLeft;

    @Column(name = "src_transaction_id", nullable = true)
    private String                         srcTransactionID;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "voucherTransactionBean")
    private List<VoucherTransactionStatusBean>     transactionStatusBeanList             = new ArrayList<VoucherTransactionStatusBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "voucherTransactionBean")
    private List<VoucherTransactionEventBean>      transactionEventBeanList              = new ArrayList<VoucherTransactionEventBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "voucherTransactionBean")
    private List<VoucherTransactionOperationBean>      transactionOperationBeanList                            = new ArrayList<VoucherTransactionOperationBean>(0);

    @OneToOne()
    @JoinColumn(name = "payment_token_package_id")
    private PaymentTokenPackageBean paymentTokenPackageBean;
    
    public VoucherTransactionBean() {}

    public VoucherTransactionBean(VoucherTransaction voucherTransaction) {

        this.id = 0;
        this.acquirerID = voucherTransaction.getAcquirerID();
        this.amount = voucherTransaction.getAmount();
        this.authorizationCode = voucherTransaction.getAuthorizationCode();
        this.bankTansactionID = voucherTransaction.getBankTansactionID();
        this.confirmed = voucherTransaction.getConfirmed();
        this.creationTimestamp = voucherTransaction.getCreationTimestamp();
        this.currency = voucherTransaction.getCurrency();
        this.endTimestamp = voucherTransaction.getEndTimestamp();
        this.finalStatusType = voucherTransaction.getFinalStatusType();
        this.paymentMethodId = voucherTransaction.getPaymentMethodId();
        this.paymentMethodType = voucherTransaction.getPaymentMethodType();
        this.paymentType = voucherTransaction.getPaymentType();
        this.reconciliationAttemptsLeft = voucherTransaction.getReconciliationAttemptsLeft();
        this.shopLogin = voucherTransaction.getShopLogin();
        this.srcTransactionID = voucherTransaction.getSrcTransactionID();
        this.token = voucherTransaction.getToken();
        this.voucherTransactionID = voucherTransaction.getVocherTransactionID();
        this.voucherCode = voucherTransaction.getVoucherCode();
        this.userBean = new UserBean(voucherTransaction.getUser());
        
        if (!voucherTransaction.getVoucherOperationList().isEmpty()) {
            for (VoucherTransactionOperation voucherTransactionOperation : voucherTransaction.getVoucherOperationList()) {
                VoucherTransactionOperationBean voucherTransactionOperationBean = new VoucherTransactionOperationBean(voucherTransactionOperation);
                voucherTransactionOperationBean.setVoucherTransactionBean(this);
                this.transactionOperationBeanList.add(voucherTransactionOperationBean);
            }
        }

        if (!voucherTransaction.getVoucherTransactionStatusList().isEmpty()) {
            for (VoucherTransactionStatus voucherTransactionStatus : voucherTransaction.getVoucherTransactionStatusList()) {
                VoucherTransactionStatusBean voucherTransactionStatusBean = new VoucherTransactionStatusBean(voucherTransactionStatus);
                voucherTransactionStatusBean.setVoucherTransactionBean(this);
                this.transactionStatusBeanList.add(voucherTransactionStatusBean);
            }
        }

        if (!voucherTransaction.getVoucherTransactionEventList().isEmpty()) {
            for (VoucherTransactionEvent voucherTransactionEvent : voucherTransaction.getVoucherTransactionEventList()) {
                VoucherTransactionEventBean voucherTransactionEventBean = new VoucherTransactionEventBean(voucherTransactionEvent);
                voucherTransactionEventBean.setVoucherTransactionBean(this);
                this.transactionEventBeanList.add(voucherTransactionEventBean);
            }
        }

        this.paymentTokenPackageBean = new PaymentTokenPackageBean(voucherTransaction.getPaymentTokenPackage());
    }

    public VoucherTransaction toVoucherTransaction() {

        VoucherTransaction voucherTransaction = new VoucherTransaction();
        voucherTransaction.setId(this.id);
        voucherTransaction.setAcquirerID(this.acquirerID);
        voucherTransaction.setAmount(this.amount);
        voucherTransaction.setAuthorizationCode(this.authorizationCode);
        voucherTransaction.setBankTansactionID(this.bankTansactionID);
        voucherTransaction.setConfirmed(this.confirmed);
        voucherTransaction.setCreationTimestamp(this.creationTimestamp);
        voucherTransaction.setCurrency(this.currency);
        voucherTransaction.setEndTimestamp(this.endTimestamp);
        voucherTransaction.setFinalStatusType(this.finalStatusType);
        voucherTransaction.setPaymentMethodId(this.paymentMethodId);
        voucherTransaction.setPaymentMethodType(this.paymentMethodType);
        voucherTransaction.setPaymentType(this.paymentType);
        voucherTransaction.setReconciliationAttemptsLeft(this.reconciliationAttemptsLeft);
        voucherTransaction.setShopLogin(this.shopLogin);
        voucherTransaction.setSrcTransactionID(this.srcTransactionID);
        voucherTransaction.setToken(this.token);
        voucherTransaction.setVoucherTransactionID(this.voucherTransactionID);
        voucherTransaction.setVoucherCode(this.voucherCode);
        voucherTransaction.setUser(this.userBean.toUser());

        if (!this.transactionOperationBeanList.isEmpty()) {
            for (VoucherTransactionOperationBean voucherTransactionOperationBean : this.transactionOperationBeanList) {
                VoucherTransactionOperation voucherTransactionOperation = voucherTransactionOperationBean.toVoucherOperation();
                voucherTransaction.getVoucherOperationList().add(voucherTransactionOperation);
            }
        }
        
        if (!this.transactionStatusBeanList.isEmpty()) {
            for (VoucherTransactionStatusBean voucherTransactionStatusBean : this.transactionStatusBeanList) {
                VoucherTransactionStatus voucherTransactionStatus = voucherTransactionStatusBean.toVoucherTransactionStatus();
                voucherTransaction.getVoucherTransactionStatusList().add(voucherTransactionStatus);
            }
        }

        if (!this.transactionEventBeanList.isEmpty()) {
            for (VoucherTransactionEventBean voucherTransactionEventBean : this.transactionEventBeanList) {
                VoucherTransactionEvent voucherTransactionEvent = voucherTransactionEventBean.toVoucherTransactionEvent();
                voucherTransaction.getVoucherTransactionEventList().add(voucherTransactionEvent);
            }
        }
        
        voucherTransaction.setPaymentTokenPackage(this.paymentTokenPackageBean.toPaymentTokenPackage());

        return voucherTransaction;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherTransactionID() {
        return voucherTransactionID;
    }

    public void setVoucherTransactionID(String voucherTransactionID) {
        this.voucherTransactionID = voucherTransactionID;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(Date endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
        this.acquirerID = acquirerID;
    }

    public String getBankTansactionID() {
        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {
        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getFinalStatusType() {
        return finalStatusType;
    }

    public void setFinalStatusType(String finalStatusType) {
        this.finalStatusType = finalStatusType;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public List<VoucherTransactionStatusBean> getTransactionStatusBeanList() {
        return transactionStatusBeanList;
    }

    public void setVoucherTransactionStatusBeanData(List<VoucherTransactionStatusBean> transactionStatusBeanList) {
        this.transactionStatusBeanList = transactionStatusBeanList;
    }

    public List<VoucherTransactionEventBean> getTransactionEventBeanList() {
        return transactionEventBeanList;
    }

    public void setVoucherTransactionEventBeanData(List<VoucherTransactionEventBean> transactionEventBeanList) {
        this.transactionEventBeanList = transactionEventBeanList;
    }

    public List<VoucherTransactionOperationBean> getTransactionOperationBeanList() {
        return transactionOperationBeanList;
    }

    public void setVoucherOperationBeanData(List<VoucherTransactionOperationBean> transactionOperationBeanList) {
        this.transactionOperationBeanList = transactionOperationBeanList;
    }
    
    public VoucherTransactionStatusBean getLastTransactionStatus() {
        VoucherTransactionStatusBean voucherTransactionStatus = null;
        int sequence = 0;

        for (VoucherTransactionStatusBean transactionStatusBean : getTransactionStatusBeanList()) {
            //System.out.println("sequenceID:" + transactionStatusBean.getSequenceID());
            if (transactionStatusBean.getSequenceID() != null && transactionStatusBean.getSequenceID().intValue() > sequence) {
                voucherTransactionStatus = transactionStatusBean;
                sequence = transactionStatusBean.getSequenceID().intValue();
                //System.out.println("sequence selected:" + sequence);
            }
        }

        //System.out.println("transactionStatus sequenceID:" + (transactionStatus != null ? transactionStatus.getSequenceID().intValue() : 0));
        return voucherTransactionStatus;
    }    

    @Override
    public String getTransactionStatus() {
        return getFinalStatusType();
    }
    
    @Override
    public void setTransactionStatus(String transactionStatus) {
        setFinalStatusType(transactionStatus);
    }

    @Override
    public Double getAmount() {
        return amount;
    }

    @Override
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String getLastStatusEvent() {
        VoucherTransactionStatusBean voucherTransactionStatusBean = getLastTransactionStatus();

        if (voucherTransactionStatusBean != null) {
            return voucherTransactionStatusBean.getStatus();
        }

        return null;
    }

    @Override
    public String getLastStatusEventCode() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getLastStatusEventResult() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionInterfaceType getTransactionType() {
        return TransactionInterfaceType.TRANSACTION_VOUCHER;
    }

    @Override
    public String getTransactionID() {
        return getVoucherTransactionID();
    }

    @Override
    public StationBean getStationBean() {
        return null;
    }

    @Override
    public Boolean getUseVoucher() {
        return false;
    }

    @Override
    public Date getEndRefuelTimestamp() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Double getInitialAmount() {
        return getAmount();
    }

    @Override
    public String getPumpID() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer getPumpNumber() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getProductID() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getProductDescription() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Boolean getGFGNotification() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getRefuelMode() {
        // TODO Auto-generated method stub
        return null;
    }
    
    private Integer getLastTransactionStatusSequenceID() {
        Integer sequence = 0;

        for (VoucherTransactionStatusBean voucherTransactionStatusBean : getTransactionStatusBeanList()) {
            if (voucherTransactionStatusBean.getSequenceID() != null && voucherTransactionStatusBean.getSequenceID() > sequence) {
                sequence = voucherTransactionStatusBean.getSequenceID();
            }
        }

        return sequence;
    }
    
    private Integer getLastTransactionEventSequenceID() {
        Integer sequence = 0;

        for (VoucherTransactionEventBean voucherTransactionEventBean : getTransactionEventBeanList()) {
            if (voucherTransactionEventBean.getSequenceID() != null && voucherTransactionEventBean.getSequenceID() > sequence) {
                sequence = voucherTransactionEventBean.getSequenceID();
            }
        }

        return sequence;
    }

    public VoucherTransactionStatusBean addVoucherTransactionStatusBean(Integer sequenceID, String status, String subStatus, String subStatusDescription) {
        VoucherTransactionStatusBean voucherTransactionStatusBean = new VoucherTransactionStatusBean();
        Timestamp statusTimestamp = new Timestamp(new Date().getTime());
        String requestID = String.valueOf(new Date().getTime());
        
        if (sequenceID == null) {
            sequenceID = getLastTransactionStatusSequenceID() + 1;
        }
        
        voucherTransactionStatusBean.setRequestID(requestID);
        voucherTransactionStatusBean.setSequenceID(sequenceID);
        voucherTransactionStatusBean.setStatus(status);
        voucherTransactionStatusBean.setSubStatus(subStatus);
        voucherTransactionStatusBean.setTimestamp(statusTimestamp);
        voucherTransactionStatusBean.setSubStatusDescription(subStatusDescription);
        voucherTransactionStatusBean.setVoucherTransactionBean(this);

        getTransactionStatusBeanList().add(voucherTransactionStatusBean);
        
        return voucherTransactionStatusBean;
        
    }
    
    public VoucherTransactionStatusBean addVoucherTransactionStatusBean(String status, String subStatus, String subStatusDescription) {
        return addVoucherTransactionStatusBean(null, status, subStatus, subStatusDescription);
    }

    public VoucherTransactionEventBean addVoucherTransactionEventBean(EventType eventType, Double eventAmount, Integer sequenceID, String errorCode, String errorDescription, String transactionResult) {
        VoucherTransactionEventBean voucherTransactionEventBean = new VoucherTransactionEventBean();

        if (sequenceID == null) {
            sequenceID = getLastTransactionEventSequenceID() + 1;
        }
        
        voucherTransactionEventBean.setSequenceID(sequenceID);
        voucherTransactionEventBean.setErrorCode(errorCode);
        voucherTransactionEventBean.setErrorDescription(errorDescription);
        voucherTransactionEventBean.setEventAmount(eventAmount);
        voucherTransactionEventBean.setEventType(eventType.value());
        voucherTransactionEventBean.setTransactionResult(transactionResult);
        voucherTransactionEventBean.setVoucherTransactionBean(this);

        getTransactionEventBeanList().add(voucherTransactionEventBean);
        
        return voucherTransactionEventBean;
    }

    public VoucherTransactionEventBean addVoucherTransactionEventBean(EventType eventType, Double eventAmount, String errorCode, String errorDescription, String transactionResult) {
        return addVoucherTransactionEventBean(eventType, eventAmount, null, errorCode, errorDescription, transactionResult);
    }

    public PaymentTokenPackageBean getPaymentTokenPackageBean() {
        return paymentTokenPackageBean;
    }

    public void setPaymentTokenPackageBean(PaymentTokenPackageBean paymentTokenPackageBean) {
        this.paymentTokenPackageBean = paymentTokenPackageBean;
    }

    @Override
    public String getEncodedSecretKey() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getGroupAcquirer() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionCategoryType getTransactionCategory() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Double getUnitPrice() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getServerName() {
        return "";
    }
    
}
