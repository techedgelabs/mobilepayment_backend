package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.MobilePhone;

@Entity
@Table(name = "MOBILE_PHONE")
@NamedQueries({ @NamedQuery(name = "MobilePhoneBean.findByNumber", query = "select t from MobilePhoneBean t where t.number = :number"),
    @NamedQuery(name = "MobilePhoneBean.findByNumberAndPrefix", query = "select t from MobilePhoneBean t where t.number = :number and t.prefix = :prefix"),
    @NamedQuery(name = "MobilePhoneBean.findActiveByNumberAndPrefix", query = "select t from MobilePhoneBean t where t.number = :number and t.prefix = :prefix and ( t.status = 0 or t.status = 1 )"),
    @NamedQuery(name = "MobilePhoneBean.findById", query = "select t from MobilePhoneBean t where t.id = :id"),
    @NamedQuery(name = "MobilePhoneBean.findPendingNumber", query = "select t from MobilePhoneBean t where status = 0 and t.creationTimestamp < :time"),
    @NamedQuery(name = "MobilePhoneBean.findByUser", query = "select t from MobilePhoneBean t where t.user = :userBean")})
public class MobilePhoneBean {
    public static final String FIND_BY_NUMBER = "MobilePhoneBean.findByNumber";
    public static final String FIND_BY_NUMBER_AND_PREFIX = "MobilePhoneBean.findByNumberAndPrefix";
    public static final String FIND_BY_ID = "MobilePhoneBean.findById";
    public static final String FIND_ACTIVE_BY_NUMBER_AND_PREFIX = "MobilePhoneBean.findActiveByNumberAndPrefix";
    public static final String FIND_BY_USER = "MobilePhoneBean.findByUser";
    public static final String FIND_PENDING_NUMBER = "MobilePhoneBean.findPendingNumber";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserBean           user;

    @Column(name = "prefix", nullable = false)
    private String             prefix;

    @Column(name = "number", nullable = false)
    private String             number;

    @Column(name = "status", nullable = false)
    private Integer            status;

    @Column(name = "verification_code", nullable = true)
    private String             verificationCode;

    @Column(name = "creation_timestamp", nullable = true)
    private Date               creationTimestamp;

    @Column(name = "last_used_timestamp", nullable = true)
    private Date               lastUsedTimestamp;

    @Column(name = "expiration_timestamp", nullable = true)
    private Date               expirationTimestamp;

    public MobilePhoneBean() {}

    public MobilePhoneBean(MobilePhone mobilePhone) {
        this.prefix = mobilePhone.getPrefix();
        this.number = mobilePhone.getNumber();
        this.status = mobilePhone.getStatus();
        this.verificationCode = mobilePhone.getVerificationCode();
        this.creationTimestamp = mobilePhone.getCreationTimestamp();
        this.lastUsedTimestamp = mobilePhone.getLastUsedTimestamp();
        this.expirationTimestamp = mobilePhone.getExpirationTimestamp();
    }

    public MobilePhone toMobilePhone() {
        MobilePhone mobilePhone = new MobilePhone();
        mobilePhone.setId(this.id);
        mobilePhone.setPrefix(this.prefix);
        mobilePhone.setNumber(this.number);
        mobilePhone.setStatus(this.status);
        mobilePhone.setVerificationCode(this.verificationCode);
        mobilePhone.setCreationTimestamp(this.creationTimestamp);
        mobilePhone.setLastUsedTimestamp(this.lastUsedTimestamp);
        mobilePhone.setExpirationTimestamp(this.expirationTimestamp);

        return mobilePhone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getLastUsedTimestamp() {
        return lastUsedTimestamp;
    }

    public void setLastUsedTimestamp(Date lastUsedTimestamp) {
        this.lastUsedTimestamp = lastUsedTimestamp;
    }

    public Date getExpirationTimestamp() {
        return expirationTimestamp;
    }

    public void setExpirationTimestamp(Date expirationTimestamp) {
        this.expirationTimestamp = expirationTimestamp;
    }

    public Boolean isValid() {
        Date now = new Date();
        if (this.expirationTimestamp == null) {
            return false;
        }
        
        if (this.expirationTimestamp.before(now)) {
            // Verification code scaduto
            return false;
        }

        return true;
    }

}
