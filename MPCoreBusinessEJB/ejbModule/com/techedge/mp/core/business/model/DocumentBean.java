package com.techedge.mp.core.business.model;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "DOCUMENT")
@NamedQueries({ @NamedQuery(name = "Document.findDocumentByKey", query = "select d from DocumentBean d where d.documentkey = :docukey"),
        @NamedQuery(name = "Document.findDocumentAll", query = "select d from DocumentBean d "),
        @NamedQuery(name = "Document.findDocumentByGroupCategory", query = "select d from DocumentBean d where d.groupCategory = :groupCategory"),
//	@NamedQuery(name="PersonalData.findPersonalDataBySearchInput", query="select p from PersonalDataBean p where p.firstName like :firstname and p.lastName like :lastname and p.fiscalCode = :fiscalCode and p.securityDataEmail = :email"),
//	@NamedQuery(name="PersonalData.findPersonalDataBySearchInputNoCF", query="select p from PersonalDataBean p where p.firstName like :firstname and p.lastName like :lastname and p.securityDataEmail = :email")
})
public class DocumentBean {

    public static final String          FIND_BY_DOCUMENTKEY       = "Document.findDocumentByKey";
    public static final String          FIND_BY_DOCUMENT_ALL      = "Document.findDocumentAll";
    public static final String          FIND_BY_DOCUMENT_BY_GROUP = "Document.findDocumentByGroupCategory";
    //	public static final String FIND_BY_SEARCHINPUTNOCF = "PersonalData.findPersonalDataBySearchInputNoCF";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                        id;

    @Column(name = "documentkey", nullable = false, length = 40)
    private String                      documentkey;

    @Column(name = "title", nullable = false)
    private String                      title;
    
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "sub_title", nullable = true)
    private String                      subTitle;

    @Column(name = "position", nullable = false)
    private Integer                     position;

    @Column(name = "templateFile", nullable = false)
    private String                      templateFile;

    @Column(name = "user_category", nullable = true)
    private String                      userCategory;
    
    @Column(name = "group_category", nullable = true)
    private String                      groupCategory;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "documentCheck")
    private List<DocumentAttributeBean> documentCheck;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDocumentkey() {
        return documentkey;
    }

    public void setDocumentkey(String documentkey) {
        this.documentkey = documentkey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getTemplateFile() {
        return templateFile;
    }

    public void setTemplateFile(String templateFile) {
        this.templateFile = templateFile;
    }

    public List<DocumentAttributeBean> getDocumentCheck() {
        return documentCheck;
    }

    public void setDocumentCheck(List<DocumentAttributeBean> documentCheck) {
        this.documentCheck = documentCheck;
    }

    public String getUserCategory() {
        return userCategory;
    }

    public void setUserCategory(String userCategory) {
        this.userCategory = userCategory;
    }

    public String getGroupCategory() {
        return groupCategory;
    }

    public void setGroupCategory(String groupCategory) {
        this.groupCategory = groupCategory;
    }

}
