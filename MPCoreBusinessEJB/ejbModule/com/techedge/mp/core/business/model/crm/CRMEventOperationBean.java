package com.techedge.mp.core.business.model.crm;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.crm.CommandType;
import com.techedge.mp.core.business.interfaces.crm.Response;
import com.techedge.mp.core.business.interfaces.crm.StatusCode;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;

@Entity
@Table(name = "CRM_EVENT_OPERATION")
public class CRMEventOperationBean {

    public static final String   COMMAND_PARAMETERS_SEPARATOR      = ";";
    public static final String   COMMAND_PARAMETERS_PAIR_SEPARATOR = ":";
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long         id;

    @Column(name = "command", nullable = false)
    private String               command;

    @Column(name = "status_code")
    private Integer               statusCode;

    @Column(name = "message_detail")
    private String               messageDetail;

    @Column(name = "message")
    private String               message;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "extended_message_detail")
    private String               extendedMessageDetail;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "extended_message")
    private String               extendedMessage;

    @Column(name = "message_code")
    private Integer              messageCode;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "command_parameters")
    private String               commandParameters;

    @Column(name = "message_status_level")
    private Integer              messageStatusLevel;

    @Column(name = "request_timestamp")
    private Date       requestTimestamp;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "event_id", nullable = false)
    private CRMEventBean eventBean;
    
    public CRMEventOperationBean() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CRMEventBean getEventBean() {
        return eventBean;
    }

    public void setEventBean(CRMEventBean eventBean) {
        this.eventBean = eventBean;
    }

    public CommandType getCommand() {
        return CommandType.getCommand(command);
    }

    public void setCommand(CommandType command) {
        this.command = command.getValue();
    }

    public String getExtendedMessageDetail() {
        return extendedMessageDetail;
    }

    public void setExtendedMessageDetail(String messageDetail) {
        this.extendedMessageDetail = messageDetail;
    }

    public String getExtendedMessage() {
        return extendedMessage;
    }

    public void setExtendedMessage(String message) {
        this.extendedMessage = message;
    }

    public Integer getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(Integer messageCode) {
        this.messageCode = messageCode;
    }

    public Integer getMessageStatusLevel() {
        return messageStatusLevel;
    }

    public void setMessageStatusLevel(Integer messageStatusLevel) {
        this.messageStatusLevel = messageStatusLevel;
    }
    
    public Date getRequestTimestamp() {
        return requestTimestamp;
    }
    
    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public StatusCode getStatusCode() {
        return StatusCode.valueOf(statusCode);
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode.getValue();
    }

    public HashMap<UserProfile.Parameter, Object> getCommandParameters() {
        HashMap<UserProfile.Parameter, Object> params = new HashMap<UserProfile.Parameter, Object>();
        
        if (commandParameters != null) {
            String[] pairs = commandParameters.split(COMMAND_PARAMETERS_SEPARATOR);
    
            for (int i = 0; i < pairs.length; i++) {
                String[] pair = pairs[i].split(COMMAND_PARAMETERS_PAIR_SEPARATOR, 2);
                //System.out.println("PAIRS -> key:" + pair[0] + "  value:" + pair[1]);
                params.put(UserProfile.Parameter.getValueOf(pair[0]), pair[1]);
            }
        }
        
        return params;
    }

    public void setCommandParameters(HashMap<UserProfile.Parameter, Object> commandParameters) {
        String value = "";
        boolean isFirst = true;

        for (UserProfile.Parameter key : commandParameters.keySet()) {
            if (!isFirst) {
                value += COMMAND_PARAMETERS_SEPARATOR;
            }

            value += key + COMMAND_PARAMETERS_PAIR_SEPARATOR + commandParameters.get(key);

            if (isFirst) {
                isFirst = false;
            }
        }

        this.commandParameters = value.isEmpty() ? null : value;
    }

    public static CRMEventOperationBean createOperation(CRMEventBean crmEventBean, CommandType command, Response response) {
        CRMEventOperationBean crmEventOperationBean = new CRMEventOperationBean();
        crmEventOperationBean.setEventBean(crmEventBean);
        crmEventOperationBean.setCommand(command);
        crmEventOperationBean.setRequestTimestamp(new Date());
        crmEventOperationBean.setStatusCode(response.getStatusCode());

        if (response.getEventParameters().size() > 0) {
            HashMap<UserProfile.Parameter, Object> commandParameters = new HashMap<UserProfile.Parameter, Object>();
            for (String key : response.getEventParameters().keySet()) {
                UserProfile.Parameter parameter = UserProfile.Parameter.getValueOf(key);
                commandParameters.put(parameter, response.getEventParameters().get(key));
            }
            
            if (!commandParameters.isEmpty()) {
                crmEventOperationBean.setCommandParameters(commandParameters);
            }
            //eventBean.setCommandParameters(response.getEventParameters());
        }

        if (response.getAdvisoryMessages().size() > 0) {
            crmEventOperationBean.setExtendedMessage(response.getAdvisoryMessages().get(0).getMessage());
            crmEventOperationBean.setMessageCode(response.getAdvisoryMessages().get(0).getMessageCode());
            crmEventOperationBean.setExtendedMessageDetail(response.getAdvisoryMessages().get(0).getDetailMessage());
            crmEventOperationBean.setMessageStatusLevel(response.getAdvisoryMessages().get(0).getStatusLevel().getValue());
        }

        return crmEventOperationBean;
    }    
}
