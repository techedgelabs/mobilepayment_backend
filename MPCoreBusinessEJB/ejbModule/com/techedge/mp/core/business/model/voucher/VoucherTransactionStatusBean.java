package com.techedge.mp.core.business.model.voucher;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.voucher.VoucherTransactionStatus;

@Entity
@Table(name = "VOUCHER_TRANSACTION_STATUS")
@NamedQueries({ 
    @NamedQuery(name = "VoucherTransactionStatusBean.findByTransaction", 
            query = "select s from VoucherTransactionStatusBean s where s.voucherTransactionBean = :voucherTransactionBean")
    
})
public class VoucherTransactionStatusBean {

    public static final String FIND_BY_TRANSACTION               = "VoucherTransactionStatusBean.findByTransaction";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "voucher_transaction_id", nullable = false)
    private VoucherTransactionBean    voucherTransactionBean;

    @Column(name = "sequence_id", nullable = true)
    private Integer            sequenceID;

    @Column(name = "timestamp", nullable = true)
    private Timestamp          timestamp;

    @Column(name = "status", nullable = true)
    private String             status;

    @Column(name = "sub_status", nullable = true)
    private String             subStatus;

    @Column(name = "sub_status_description", nullable = true)
    private String             subStatusDescription;

    @Column(name = "request_id", nullable = true)
    private String             requestID;

    public VoucherTransactionStatusBean() {}

    public VoucherTransactionStatusBean(VoucherTransactionStatus voucherTransactionStatus) {

        this.id = voucherTransactionStatus.getId();
        this.sequenceID = voucherTransactionStatus.getSequenceID();
        this.timestamp = voucherTransactionStatus.getTimestamp();
        this.status = voucherTransactionStatus.getStatus();
        this.subStatus = voucherTransactionStatus.getStatus();
        this.subStatusDescription = voucherTransactionStatus.getSubStatusDescription();
        this.requestID = voucherTransactionStatus.getRequestID();
    }

    public VoucherTransactionStatus toVoucherTransactionStatus() {

        VoucherTransactionStatus voucherTransactionStatus = new VoucherTransactionStatus();
        voucherTransactionStatus.setId(this.id);
        voucherTransactionStatus.setSequenceID(this.sequenceID);
        voucherTransactionStatus.setTimestamp(this.timestamp);
        voucherTransactionStatus.setStatus(this.status);
        voucherTransactionStatus.setSubStatus(this.subStatus);
        voucherTransactionStatus.setSubStatusDescription(this.subStatusDescription);
        voucherTransactionStatus.setRequestID(this.requestID);
        return voucherTransactionStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public VoucherTransactionBean getVoucherTransactionBean() {
        return voucherTransactionBean;
    }

    public void setVoucherTransactionBean(VoucherTransactionBean voucherTransactionBean) {
        this.voucherTransactionBean = voucherTransactionBean;
    }

    public Integer getSequenceID() {
        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {
        this.sequenceID = sequenceID;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubStatus() {
        return subStatus;
    }

    public String getSubStatusDescription() {
        return subStatusDescription;
    }

    public void setSubStatusDescription(String subStatusDescription) {
        this.subStatusDescription = subStatusDescription;
    }

    public void setSubStatus(String subStatus) {
        this.subStatus = subStatus;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }
}
