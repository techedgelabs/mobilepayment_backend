package com.techedge.mp.core.business.model.dwh;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.CategoryBurnDataDetail;
import com.techedge.mp.core.business.interfaces.CategoryEarnDataDetail;
import com.techedge.mp.core.business.interfaces.PartnerDetailInfoData;

@Entity
@Table(name = "DWH_CATEGORY_BURN")
@NamedQueries({ @NamedQuery(name = "DWHCategoryBurnDetailBean.findById", query = "select cd from DWHCategoryBurnDetailBean cd where cd.id = :id"),
        @NamedQuery(name = "DWHCategoryBurnDetailBean.findAll", query = "select cd from DWHCategoryBurnDetailBean cd") })
public class DWHCategoryBurnDetailBean {

    public static final String FIND_By_ID = "DWHCategoryBurnDetailBean.findById";
    public static final String FIND_ALL   = "DWHCategoryBurnDetailBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "creation_timestamp")
    private Date               creationTimestamp;

    @Column(name = "category_id")
    private Integer             categoryId;

    @Column(name = "category")
    private String             category;

    @Column(name = "category_image_url")
    private String             categoryImageUrl;

    public DWHCategoryBurnDetailBean() {}

    public DWHCategoryBurnDetailBean(CategoryBurnDataDetail categoryDataDetail) {
        this.categoryId = categoryDataDetail.getCategoryId();
        this.category = categoryDataDetail.getCategory();
        this.categoryImageUrl = categoryDataDetail.getCategoryImageUrl();
    }

    public CategoryBurnDataDetail toCategoryDataDetail() {
        CategoryBurnDataDetail categoryDataDetail = new CategoryBurnDataDetail();
        categoryDataDetail.setCategoryId(this.categoryId);
        categoryDataDetail.setCategory(this.category);
        categoryDataDetail.setCategoryImageUrl(this.categoryImageUrl);

        return categoryDataDetail;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryImageUrl() {
        return categoryImageUrl;
    }

    public void setCategoryImageUrl(String categoryImageUrl) {
        this.categoryImageUrl = categoryImageUrl;
    }

    public Integer getCategoryId() {
        return categoryId;
    }
    
    

}
