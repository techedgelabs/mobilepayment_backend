package com.techedge.mp.core.business.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.exceptions.AdminException;
import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminRole;
import com.techedge.mp.core.business.utilities.EncoderHelper;

@Entity
@Table(name = "ADMIN")
@NamedQueries({

@NamedQuery(name = "AdminBean.findAdminByID", query = "select a from AdminBean a where a.id = :id"),
        @NamedQuery(name = "AdminBean.findAdminByEmail", query = "select a from AdminBean a where a.email = :email"),
        @NamedQuery(name = "AdminBean.findAdmins", query = "select a from AdminBean a"),
        @NamedQuery(name = "AdminBean.findAdminByStatus", query = "select a from AdminBean a where a.status = :status") })
public class AdminBean {

    public static final String FIND_BY_ID     = "AdminBean.findAdminByID";
    public static final String FIND_BY_EMAIL  = "AdminBean.findAdminByEmail";
    public static final String FIND_BY_STATUS = "AdminBean.findAdminByStatus";
    public static final String FIND_ALL       = "AdminBean.findAdmins";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "email", nullable = false)
    private String             email;

    @Column(name = "password", nullable = false)
    private String             password;

    @Column(name = "firstname", nullable = false)
    private String             firstname;

    @Column(name = "lastname", nullable = false)
    private String             lastname;

    @Column(name = "status", nullable = false)
    private Integer            status;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ADMIN_ROLE_RELATION")
    private Set<AdminRoleBean> roles          = new HashSet<AdminRoleBean>(0);

    public AdminBean() {}

    public AdminBean(Admin admin) {

        this.email = admin.getEmail();
        this.password = admin.getPassword();
        this.firstname = admin.getFirstName();
        this.lastname = admin.getLastName();
        this.status = admin.getStatus();

    }

    public Admin toAdmin() {

        Admin admin = new Admin();

        admin.setId(this.id);
        admin.setEmail(this.email);
        admin.setPassword(this.password);
        admin.setFirstName(this.firstname);
        admin.setLastName(this.lastname);
        admin.setStatus(this.status);
        for (AdminRoleBean adminRoleBean : this.roles) {
            AdminRole adminRole = adminRoleBean.toAdminRole();
            admin.getRoles().add(adminRole);
        }
        return admin;

    }

    public static AdminBean createNewAdmin(Admin admin) throws AdminException {

        try {

            AdminBean adminBean = new AdminBean(admin);

            adminBean.password = EncoderHelper.encode(admin.getPassword());
            adminBean.status = Admin.ADMIN_STATUS_VERIFIED;

            return adminBean;
        }
        catch (Exception ex) {

            throw new AdminException("Error in admin creation: " + ex.getMessage());
        }

    }

    public void setStatusToVerified() throws AdminException {

        this.status = Admin.ADMIN_STATUS_VERIFIED;
    }

    public void blockAdmin() throws AdminException {

        this.status = Admin.ADMIN_STATUS_BLOCKED;
    }

    public Boolean canLogin() throws AdminException {

        return (this.status == Admin.ADMIN_STATUS_VERIFIED);
    }

    public Boolean isPasswordValid(String encryptedPassword) throws AdminException {

        return (this.password.equals(encryptedPassword));
    }

    public String getEmail() {
        return email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<AdminRoleBean> getRoles() {
        return roles;
    }

    public void setRoles(Set<AdminRoleBean> roles) {
        this.roles = roles;
    }

}
