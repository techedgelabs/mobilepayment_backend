package com.techedge.mp.core.business.model.station;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.station.StationChangeLogType;

@Entity
@Table(name = "STATIONS_CHANGELOG")

public class StationChangeLogBean {

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long    id;

    @Column(name = "date_log")
    private Date  dateLog;

    @Column(name = "request_id")
    private String  requestID;

    @Column(name = "change_type")
    private String changeType;

    @Column(name = "field_name")
    private String fieldName;
    
    @Column(name = "new_value", nullable = true)
    private String newValue;
    
    @Column(name = "old_value", nullable = true)
    private String oldValue;
    
    @Column(name = "station_id")
    private String stationID;
    
    public StationChangeLogBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDateLog() {
        return dateLog;
    }

    public void setDateLog(Date dateLog) {
        this.dateLog = dateLog;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public StationChangeLogType getChangeType() {
        return StationChangeLogType.valueOf(changeType);
    }

    public void setChangeType(StationChangeLogType changeType) {
        this.changeType = changeType.name();
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(Object newValue) {
        this.newValue = (newValue != null) ? newValue.toString() : null;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(Object oldValue) {
        this.oldValue = (oldValue != null) ? oldValue.toString() : null;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    
    
    
    
}
