package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "ES_PROMOTION")
@NamedQueries({ 
	@NamedQuery(name = "EsPromotionBean.findPromotionByFiscalCode", query = "select t from EsPromotionBean t where t.fiscalCode = :fiscalCode and start_date >= :firstDayOfMonth order by start_date asc"),
	@NamedQuery(name = "EsPromotionBean.findPromotionByCurrentMonthAndVoucherCode", query = "select t from EsPromotionBean t where t.voucherCode=:voucherCode and start_date >= :firstDayOfMonth and start_date <= :lastDayOfMonth order by start_date asc"),
	@NamedQuery(name = "EsPromotionBean.findPromotionByCurrentMonthWithNullVoucherCode", query = "select t from EsPromotionBean t where t.voucherCode is null and start_date >= :firstDayOfMonth and start_date <= :lastDayOfMonth order by start_date asc"),
	@NamedQuery(name = "EsPromotionBean.findPromotionToBeProcessed", query = "select t from EsPromotionBean t where t.voucherCode is null and t.toBeProcessed = true order by start_date asc"),
	@NamedQuery(name = "EsPromotionBean.findPromotionToBeProcessedWithNullVoucherCodeByDate", query = "select t from EsPromotionBean t where t.voucherCode is null and t.toBeProcessed = false and t.userBean = :userBean and start_date <= :checkDate and end_date >= :checkDate")
	})
public class EsPromotionBean {

	public static final String FIND_BY_FISCALCODE = "EsPromotionBean.findPromotionByFiscalCode";
	public static final String FIND_BY_CURRENT_MONTH_VOUCHER_CODE = "EsPromotionBean.findPromotionByCurrentMonthAndVoucherCode";
	public static final String FIND_BY_CURRENT_MONTH_WITH_NULL_VOUCHER_CODE = "EsPromotionBean.findPromotionByCurrentMonthWithNullVoucherCode";
	public static final String FIND_TO_BE_PROCESSED = "EsPromotionBean.findPromotionToBeProcessed";
	public static final String FIND_TO_BE_PROCESSED_WITH_NULL_VOUCHER_CODE_BY_DATE = "EsPromotionBean.findPromotionToBeProcessedWithNullVoucherCodeByDate";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "promotion_code", nullable = false)
	private String promotionCode;

	@Column(name = "fiscal_code", nullable = false, length = 16)
	private String fiscalCode;

	@Column(name = "start_date", nullable = false)
	private Date startDate;

	@Column(name = "end_date", nullable = false)
	private Date endDate;

	@Column(name = "type", nullable = false)
	private String type;

	@Column(name = "voucher_code", nullable = true)
	private String voucherCode;

	@Column(name = "creation_date", nullable = false)
	private Date creationDate;
	
	@Column(name = "to_be_processed")
    private Boolean toBeProcessed;

    @ManyToOne
    @JoinColumn(name = "push_notification_id")
    private PushNotificationBean pushNotificationBean;
    
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserBean userBean;

	public EsPromotionBean() {
	}

	public EsPromotionBean(String promotionCode, String fiscalCode,
			Date startDate, Date endDate, String type, String voucherCode,
			Date creationDate, UserBean userBean) {
		this.promotionCode = promotionCode;
		this.fiscalCode = fiscalCode;
		this.startDate = startDate;
		this.endDate = endDate;
		this.type = type;
		this.voucherCode = voucherCode;
		this.creationDate = creationDate;
		this.userBean = userBean;
		this.toBeProcessed = Boolean.FALSE;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

    public PushNotificationBean getPushNotificationBean() {
        return pushNotificationBean;
    }

    public void setPushNotificationBean(PushNotificationBean pushNotificationBean) {
        this.pushNotificationBean = pushNotificationBean;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Boolean getToBeProcessed() {
        return toBeProcessed;
    }

    public void setToBeProcessed(Boolean toBeProcessed) {
        this.toBeProcessed = toBeProcessed;
    }
    
}
