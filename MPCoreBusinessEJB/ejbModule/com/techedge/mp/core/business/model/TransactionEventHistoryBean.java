package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.TransactionEventHistory;


@Entity
@Table(name="TRANSACTION_EVENT_HISTORY")
@NamedQueries({
	@NamedQuery(name="TransactionEventHistoryBean.findTransactionEventByTransaction", query="select t from TransactionEventHistoryBean t where t.transactionHistoryBean = :transactionHistoryBean")
})

	public class TransactionEventHistoryBean {
	
	public static final String FIND_BY_TRANSACTION = "TransactionEventHistoryBean.findTransactionEventByTransaction";
	//public static final String FIND_BY_TRANSACTION_AND_STATUS_ID = "TransactionEventBean.findTransactionStatusByTransactionAndStatusID";

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transaction_id", nullable = false)
	private TransactionHistoryBean transactionHistoryBean;
	
	@Column(name="sequence_id", nullable=false)
	private Integer sequenceID;
	
	@Column(name="event_type", nullable=false)
	private String eventType;
	
	@Column(name="event_amount", nullable=false)
	private Double eventAmount;
	
	@Column(name="transaction_result", nullable=false)
	private String transactionResult;
	
	@Column(name="error_code", nullable=true)
	private String errorCode;
	
	@Column(name="error_description", nullable=true)
	private String errorDescription;
	
	
	public TransactionEventHistoryBean() {	}
	
	
	public TransactionEventHistoryBean(TransactionEventHistory transactionEvent) {
		
		this.sequenceID = transactionEvent.getSequenceID();
		this.eventType = transactionEvent.getEventType();
		this.eventAmount = transactionEvent.getEventAmount();
		this.transactionResult = transactionEvent.getTransactionResult();
		this.errorCode = transactionEvent.getErrorCode();
		this.errorDescription = transactionEvent.getErrorDescription();
	}
	
	
	public TransactionEventHistoryBean(TransactionEventBean transactionEvent) {
		
		this.transactionHistoryBean = null;
		this.sequenceID = transactionEvent.getSequenceID();
		this.eventType = transactionEvent.getEventType();
		this.eventAmount = transactionEvent.getEventAmount();
		this.transactionResult = transactionEvent.getTransactionResult();
		this.errorCode = transactionEvent.getErrorCode();
		this.errorDescription = transactionEvent.getErrorDescription();
	}
	
	
	
	public TransactionEventHistory toTransactionEventHistory() {
		
		TransactionEventHistory transactionEvent = new TransactionEventHistory();
		transactionEvent.setId(this.id);
		transactionEvent.setSequenceID(this.sequenceID);
		transactionEvent.setEventType(this.eventType);
		transactionEvent.setEventAmount(this.eventAmount);
		transactionEvent.setTransactionResult(this.transactionResult);
		transactionEvent.setErrorCode(this.errorCode);
		transactionEvent.setErrorDescription(this.errorDescription);
		return transactionEvent;
	}
	



	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public TransactionHistoryBean getTransactionHistoryBean() {
		return transactionHistoryBean;
	}
	public void setTransactionHistoryBean(TransactionHistoryBean transactionBean) {
		this.transactionHistoryBean = transactionBean;
	}

	public Integer getSequenceID() {
		return sequenceID;
	}
	public void setSequenceID(Integer sequenceID) {
		this.sequenceID = sequenceID;
	}


	public String getEventType() {
		return eventType;
	}


	public void setEventType(String eventType) {
		this.eventType = eventType;
	}


	public Double getEventAmount() {
		return eventAmount;
	}


	public void setEventAmount(Double eventAmount) {
		this.eventAmount = eventAmount;
	}


	public String getTransactionResult() {
		return transactionResult;
	}


	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}


	public String getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}


	public String getErrorDescription() {
		return errorDescription;
	}


	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	
}
