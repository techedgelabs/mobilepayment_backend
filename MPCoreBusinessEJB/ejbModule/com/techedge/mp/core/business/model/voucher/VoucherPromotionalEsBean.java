package com.techedge.mp.core.business.model.voucher;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Entity
@Table(name = "VOUCHER_PROMOTIONAL_ES")
//@NamedQueries({ @NamedQuery(name = "", query = "") })
public class VoucherPromotionalEsBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long        id;

    @Column(name = "voucher_type")
    private String      voucherType;

    @Column(name = "partner_type")
    private String      partnerType;

    @Column(name = "fiscal_code", nullable = false)
    private String      fiscalCode;

    @Column(name = "promo_code", nullable = false)
    private String      promoCode;

    @Column(name = "total_amount", nullable = false)
    private Double      totalAmount;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "voucher_id")
    private VoucherBean voucherBean;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "voucherPromotionalEsBean")
    private Set<VoucherPromotionalEsDetailBean> voucherPromotionalEsDetailBeanList = new HashSet<VoucherPromotionalEsDetailBean>(0);

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public PartnerType getPartnerType() {
        return PartnerType.valueOf(partnerType);
    }

    public void setPartnerType(PartnerType partnerType) {
        this.partnerType = partnerType.getValue();
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public VoucherBean getVoucherBean() {
        return voucherBean;
    }

    public void setVoucherBean(VoucherBean voucherBean) {
        this.voucherBean = voucherBean;
    }

	public Set<VoucherPromotionalEsDetailBean> getVoucherPromotionalEsDetailBeanList() {
		return voucherPromotionalEsDetailBeanList;
	}

	public void setVoucherPromotionalEsDetailBeanList(
			Set<VoucherPromotionalEsDetailBean> voucherPromotionalEsDetailBeanList) {
		this.voucherPromotionalEsDetailBeanList = voucherPromotionalEsDetailBeanList;
	}
    
    
}
