package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SURVEY_SUBMISSION_ANSWER")
//@NamedQuery(name = "SurveySubmissionAnswerBean.search", query = "")
public class SurveySubmissionAnswerBean {

    //public static final String      SEARCH        = "SurveySubmissionAnswerBean.search";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                    id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "survey_submission_id", nullable = false)
    private SurveySubmissionBean           surveySubmissionBean;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "survey_question_id", nullable = false)
    private SurveyQuestionBean           surveyQuestionBean;

    @Column(name = "answer", nullable = true)
    private String                  answer;

    public SurveySubmissionAnswerBean() {}

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the surveySubmissionBean
     */
    public SurveySubmissionBean getSurveySubmissionBean() {
        return surveySubmissionBean;
    }

    /**
     * @param surveySubmissionBean the surveySubmissionBean to set
     */
    public void setSurveySubmissionBean(SurveySubmissionBean surveySubmissionBean) {
        this.surveySubmissionBean = surveySubmissionBean;
    }

    /**
     * @return the surveyQuestionBean
     */
    public SurveyQuestionBean getSurveyQuestionBean() {
        return surveyQuestionBean;
    }

    /**
     * @param surveyQuestionBean the surveyQuestionBean to set
     */
    public void setSurveyQuestionBean(SurveyQuestionBean surveyQuestionBean) {
        this.surveyQuestionBean = surveyQuestionBean;
    }

    /**
     * @return the answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * @param answer the answer to set
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    
    
}
