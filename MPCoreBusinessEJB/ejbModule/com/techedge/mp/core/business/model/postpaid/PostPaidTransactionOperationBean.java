package com.techedge.mp.core.business.model.postpaid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionOperation;

@Entity
@Table(name = "POSTPAIDTRANSACTION_OPERATION")
public class PostPaidTransactionOperationBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                 id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    private PostPaidTransactionBean postPaidTransactionBean;
    
    @Column(name = "operation_type", nullable = false)
    private String               operationType;

    @Column(name = "sequence_id", nullable = false)
    private Integer              sequenceID;

    @Column(name = "operation_id", nullable = false)
    private String               operationId;

    @Column(name = "request_timestamp", nullable = false)
    private Long                 requestTimestamp;

    @Column(name = "remote_transaction_id", nullable = false)
    private String               remoteTransactionId;

    @Column(name = "status", nullable = false)
    private String               status;

    @Column(name = "code", nullable = false)
    private String               code;

    @Column(name = "message", nullable = false)
    private String               message;
    
    @Column(name = "amount", nullable = true)
    private Integer              amount;

    public PostPaidTransactionOperationBean() {}
    
    public PostPaidTransactionOperationBean(PostPaidTransactionOperation postPaidTransactionOperation) {
        
        this.id                  = postPaidTransactionOperation.getId();
        this.operationType       = postPaidTransactionOperation.getOperationType();
        this.sequenceID          = postPaidTransactionOperation.getSequenceID();
        this.operationId         = postPaidTransactionOperation.getOperationId();
        this.requestTimestamp    = postPaidTransactionOperation.getRequestTimestamp();
        this.remoteTransactionId = postPaidTransactionOperation.getRemoteTransactionId();
        this.status              = postPaidTransactionOperation.getStatus();
        this.code                = postPaidTransactionOperation.getCode();
        this.message             = postPaidTransactionOperation.getMessage();
        this.amount              = postPaidTransactionOperation.getAmount();
    }
    
    public PostPaidTransactionOperation toPostPaidTransactionOperation() {
        
        PostPaidTransactionOperation postPaidTransactionOperation = new PostPaidTransactionOperation();
        postPaidTransactionOperation.setId(this.id);
        postPaidTransactionOperation.setOperationType(this.operationType);
        postPaidTransactionOperation.setSequenceID(this.sequenceID);
        postPaidTransactionOperation.setOperationId(this.operationId);
        postPaidTransactionOperation.setRequestTimestamp(this.requestTimestamp);
        postPaidTransactionOperation.setRemoteTransactionId(this.remoteTransactionId);
        postPaidTransactionOperation.setStatus(this.status);
        postPaidTransactionOperation.setCode(this.code);
        postPaidTransactionOperation.setMessage(this.message);
        postPaidTransactionOperation.setAmount(this.amount);
        return postPaidTransactionOperation;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PostPaidTransactionBean getPostPaidTransactionBean() {
        return postPaidTransactionBean;
    }

    public void setPostPaidTransactionBean(PostPaidTransactionBean postPaidTransactionBean) {
        this.postPaidTransactionBean = postPaidTransactionBean;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Integer getSequenceID() {
        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {
        this.sequenceID = sequenceID;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getRemoteTransactionId() {
        return remoteTransactionId;
    }

    public void setRemoteTransactionId(String remoteTransactionId) {
        this.remoteTransactionId = remoteTransactionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

}
