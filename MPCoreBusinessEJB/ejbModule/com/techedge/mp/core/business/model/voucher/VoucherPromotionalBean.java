package com.techedge.mp.core.business.model.voucher;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.crm.CRMOfferVoucherPromotionalBean;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Entity
@Table(name = "VOUCHER_PROMOTIONAL")
//@NamedQueries({ @NamedQuery(name = "", query = "") })
public class VoucherPromotionalBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long        id;

    @Column(name = "voucher_type")
    private String      voucherType;

    @Column(name = "partner_type")
    private String      partnerType;

    @Column(name = "fiscal_code", nullable = false)
    private String      fiscalCode;

    @Column(name = "promo_code", nullable = false)
    private String      promoCode;

    @Column(name = "total_amount", nullable = false)
    private Double      totalAmount;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "voucher_id")
    private VoucherBean voucherBean;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "crm_offer_voucher_promotional_id")
    private CRMOfferVoucherPromotionalBean crmOfferVoucherPromotionalBean;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "voucherPromotionalBean")
    private Set<VoucherPromotionalDetailBean> voucherPromotionalDetailBeanList = new HashSet<VoucherPromotionalDetailBean>(0);

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public PartnerType getPartnerType() {
        return PartnerType.valueOf(partnerType);
    }

    public void setPartnerType(PartnerType partnerType) {
        this.partnerType = partnerType.getValue();
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public VoucherBean getVoucherBean() {
        return voucherBean;
    }

    public void setVoucherBean(VoucherBean voucherBean) {
        this.voucherBean = voucherBean;
    }

    public CRMOfferVoucherPromotionalBean getCrmOfferVoucherPromotionalBean() {
        return crmOfferVoucherPromotionalBean;
    }

    public void setCrmOfferVoucherPromotionalBean(CRMOfferVoucherPromotionalBean crmOfferVoucherPromotionalBean) {
        this.crmOfferVoucherPromotionalBean = crmOfferVoucherPromotionalBean;
    }

    public Set<VoucherPromotionalDetailBean> getVoucherPromotionalDetailBeanList() {
        return voucherPromotionalDetailBeanList;
    }
    
    public void setVoucherPromotionalDetailBeanList(Set<VoucherPromotionalDetailBean> voucherPromotionalDetailList) {
        this.voucherPromotionalDetailBeanList = voucherPromotionalDetailList;
    }
    
    public VoucherPromotionalDetailBean getLastVoucherPromotionalDetailBean() {
        VoucherPromotionalDetailBean voucherPromotionalDetailBean = null;
        
        for (VoucherPromotionalDetailBean tmpVoucherPromotionalDetailBean : getVoucherPromotionalDetailBeanList()) {
            if (voucherPromotionalDetailBean != null) {
                if (tmpVoucherPromotionalDetailBean.getRequestTimestamp().after(voucherPromotionalDetailBean.getRequestTimestamp())) {
                    voucherPromotionalDetailBean = tmpVoucherPromotionalDetailBean;
                }
            }
            else {
                voucherPromotionalDetailBean = tmpVoucherPromotionalDetailBean;
            }
        }

        return voucherPromotionalDetailBean;
    }
    
}
