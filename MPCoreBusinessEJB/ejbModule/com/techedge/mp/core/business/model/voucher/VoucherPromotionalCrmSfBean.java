package com.techedge.mp.core.business.model.voucher;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "VOUCHER_PROMOTIONAL_CRM_SF")
@NamedQueries({ @NamedQuery(name = "VoucherPromotionalCrmSfBean.selectByRequestId", query = "select t from VoucherPromotionalCrmSfBean t where sourceRequestId= :sourceRequestId"),
	@NamedQuery(name = "VoucherPromotionalCrmSfBean.selectByOperationtId", query = "select t from VoucherPromotionalCrmSfBean t where sourceOperationId= :sourceOperationId")})
public class VoucherPromotionalCrmSfBean {
	
	public static final String SELECT_BY_REQUEST_ID = "VoucherPromotionalCrmSfBean.selectByRequestId";
	public static final String SELECT_BY_OPERATION_ID = "VoucherPromotionalCrmSfBean.selectByOperationtId";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "operation_id", nullable = false)
	private String operationId;

	@Column(name = "voucher_type", nullable = false)
	private String voucherType;

	@Column(name = "request_timestamp", nullable = false)
	private Date requestTimestamp;

	@Column(name = "partner_type", nullable = false)
	private String partnerType;

	@Column(name = "fiscal_code", nullable = false)
	private String fiscalCode;

	@Column(name = "promo_code", nullable = false)
	private String promoCode;

	@Column(name = "total_amount", nullable = false)
	private Double totalAmount;

	@Column(name = "cs_transaction_id")
	private String transactionID;

	@Column(name = "message_code")
	private String messageCode;

	@Column(name = "status_code")
	private String statusCode;

	@Column(name = "voucher_code")
	private String voucherCode;

	@Column(name = "source_operation_id")
	private String sourceOperationId;

	@Column(name = "source_request_id")
	private String sourceRequestId;

	public VoucherPromotionalCrmSfBean() {
	}

	public VoucherPromotionalCrmSfBean(String operationId, String voucherType,
			Date requestTimestamp, String partnerType, String fiscalCode,
			String promoCode, Double totalAmount, String transactionID,
			String messageCode, String statusCode, String voucherCode,
			String sourceOperationId, String sourceRequestId) {
		this.operationId = operationId;
		this.voucherType = voucherType;
		this.requestTimestamp = requestTimestamp;
		this.partnerType = partnerType;
		this.fiscalCode = fiscalCode;
		this.promoCode = promoCode;
		this.totalAmount = totalAmount;
		this.transactionID = transactionID;
		this.messageCode = messageCode;
		this.statusCode = statusCode;
		this.voucherCode = voucherCode;
		this.sourceOperationId = sourceOperationId;
		this.sourceRequestId = sourceRequestId;
	}

	public long getId() {
		return id;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public Date getRequestTimestamp() {
		return requestTimestamp;
	}

	public void setRequestTimestamp(Date requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public String getSourceOperationId() {
		return sourceOperationId;
	}

	public void setSourceOperationId(String sourceOperationId) {
		this.sourceOperationId = sourceOperationId;
	}

	public String getSourceRequestId() {
		return sourceRequestId;
	}

	public void setSourceRequestId(String sourceRequestId) {
		this.sourceRequestId = sourceRequestId;
	}

}
