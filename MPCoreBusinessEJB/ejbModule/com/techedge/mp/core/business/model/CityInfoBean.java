package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.CityInfo;

@Entity
@Table(name="CITIES")
@NamedQueries({
    @NamedQuery(name="CityInfoBean.findCityByKey", query="select c from CityInfoBean c where c.name LIKE :key"),
    @NamedQuery(name="CityInfoBean.findCityByName", query="select c from CityInfoBean c where c.name = :name"),
    @NamedQuery(name="CityInfoBean.findCityByNameAndProvince", query="select c from CityInfoBean c where c.name = :name and c.province = :province")
})

public class CityInfoBean {

	public static final String FIND_BY_KEY = "CityInfoBean.findCityByKey";
	public static final String FIND_BY_NAME = "CityInfoBean.findCityByName";
    public static final String FIND_BY_NAME_AND_PROVINCE = "CityInfoBean.findCityByNameAndProvince";
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="name", nullable=false)
	private String name;
	
	@Column(name="province", nullable=false, length=3)
	private String province;

    @Column(name="code", length=4)
    private String code;
	
	
	public CityInfoBean() {}
	
	public CityInfoBean(CityInfo cityInfo) {
		
		this.name = cityInfo.getName().toUpperCase();
		this.province = cityInfo.getProvince().toUpperCase();
		
		if (cityInfo.getCode() != null) {
		    this.code = cityInfo.getCode().toUpperCase();
		}
	}
	
	public CityInfo toCityInfo() {
		
		CityInfo cityInfo = new CityInfo();
		cityInfo.setName(this.name.toUpperCase());
		cityInfo.setProvince(this.province.toUpperCase());
		
		if (this.code != null) {
		    cityInfo.setCode(this.code.toUpperCase());
		}
		
		return cityInfo;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getCode() {
        return code;
    }
	
	public void setCode(String code) {
        this.code = code;
    }
}
