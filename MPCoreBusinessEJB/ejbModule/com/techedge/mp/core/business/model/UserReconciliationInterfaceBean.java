package com.techedge.mp.core.business.model;

import java.util.Date;


public interface UserReconciliationInterfaceBean {


    public Long getId();
    
    public UserBean getUserBean();

    public String getFinalStatus();
    
    public void setFinalStatus(String status);
    
    public void setToReconcilie(Boolean toReconcilie);
    
    public Boolean getToReconcilie();

    public Integer getReconciliationAttemptsLeft();
    
    public void setReconciliationAttemptsLeft(Integer attemptsLeft);
    
    public String getOperationName();
    
    public Date getOperationTimestamp();
    
    public String getFinalStatusMessage();
    
    public String getServerEndpoint();
}
