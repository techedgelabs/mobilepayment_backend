package com.techedge.mp.core.business.model.crmsf;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.crm.CrmSfDataElement;

@Entity
@Table(name = "CRM_SF_OUTBOUND")
@NamedQueries({ @NamedQuery(name = "CRMSfOutboundBean.findDeliveryStatus", query = "select t from CRMSfOutboundBean t where t.deliveryStatus is null or t.deliveryStatus = :deliveryStatus"),
				@NamedQuery(name = "CRMSfOutboundBean.selectAll", query = "select t from CRMSfOutboundBean t"),
				@NamedQuery(name = "CRMSfOutboundBean.rowsCount", query = "select count(t) from CRMSfOutboundBean t"),})
public class CRMSfOutboundBean {
	
	public static final String FIND_DELIVERY_STATUS="CRMSfOutboundBean.findDeliveryStatus";
	public static final String SELECT_ALL="CRMSfOutboundBean.selectAll";
	public static final String ROWS_COUNT = "CRMSfOutboundBean.rowsCount";
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String contactCode;
	private String cardCode;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String channel;
	private String initiativeCode;
	private String offertCode;
	private String idCrm;
	private String fiscalCode;
	private String createdDate;
	private String type;
	private String voucherCode;
	private String voucherAmount;
	private String parameter1;
	private String parameter2;
	private String parameter3;
	private String parameter4;
	private String deliveryStatus;
	private Date insertDate;
	private Date processedDate;
	

	public CRMSfOutboundBean() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContactCode() {
		return contactCode;
	}

	public void setContactCode(String contactCode) {
		this.contactCode = contactCode;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getInitiativeCode() {
		return initiativeCode;
	}

	public void setInitiativeCode(String initiativeCode) {
		this.initiativeCode = initiativeCode;
	}

	public String getOffertCode() {
		return offertCode;
	}

	public void setOffertCode(String offertCode) {
		this.offertCode = offertCode;
	}

	public String getIdCrm() {
		return idCrm;
	}

	public void setIdCrm(String idCrm) {
		this.idCrm = idCrm;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public String getVoucherAmount() {
		return voucherAmount;
	}

	public void setVoucherAmount(String voucherAmount) {
		this.voucherAmount = voucherAmount;
	}

	public String getParameter1() {
		return parameter1;
	}

	public void setParameter1(String parameter1) {
		this.parameter1 = parameter1;
	}

	public String getParameter2() {
		return parameter2;
	}

	public void setParameter2(String parameter2) {
		this.parameter2 = parameter2;
	}

	public String getParameter3() {
		return parameter3;
	}

	public void setParameter3(String parameter3) {
		this.parameter3 = parameter3;
	}

	public String getParameter4() {
		return parameter4;
	}

	public void setParameter4(String parameter4) {
		this.parameter4 = parameter4;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	
	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}
	
	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public static CRMSfOutboundBean generate(CrmSfDataElement crmDataElement,
			String deliveryStatus) {

		CRMSfOutboundBean crmSfOutboundBean = new CRMSfOutboundBean();

		crmSfOutboundBean.contactCode = crmDataElement.contactCode;
		crmSfOutboundBean.cardCode = crmDataElement.cardCode;
		crmSfOutboundBean.firstName = crmDataElement.firstName;
		crmSfOutboundBean.lastName = crmDataElement.lastName;
		crmSfOutboundBean.phone = crmDataElement.phone;
		crmSfOutboundBean.email = crmDataElement.email;
		crmSfOutboundBean.channel = crmDataElement.channel;
		crmSfOutboundBean.initiativeCode = crmDataElement.initiativeCode;
		crmSfOutboundBean.offertCode = crmDataElement.offertCode;
		crmSfOutboundBean.idCrm = crmDataElement.id;
		crmSfOutboundBean.fiscalCode = crmDataElement.fiscalCode;
		crmSfOutboundBean.createdDate = crmDataElement.createDate;
		crmSfOutboundBean.type = crmDataElement.type;
		crmSfOutboundBean.voucherCode = crmDataElement.voucherCode;
		crmSfOutboundBean.voucherAmount = crmDataElement.voucherAmount;
		crmSfOutboundBean.parameter1 = crmDataElement.parameter1;
		crmSfOutboundBean.parameter2 = crmDataElement.parameter2;
		crmSfOutboundBean.parameter3 = crmDataElement.parameter3;
		crmSfOutboundBean.parameter4 = crmDataElement.parameter4;
		crmSfOutboundBean.deliveryStatus = deliveryStatus;
		crmSfOutboundBean.insertDate = new Date();

		return crmSfOutboundBean;
	}
}
