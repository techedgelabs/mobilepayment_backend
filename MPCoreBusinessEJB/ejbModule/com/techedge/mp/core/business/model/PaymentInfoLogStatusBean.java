package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PAYMENTINFO_STATUS_LOG")
public class PaymentInfoLogStatusBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "dpanBefore", nullable = true)
    private String             dpanBefore;

    @Column(name = "dpanAfter", nullable = true)
    private String             dpanAfter;

    @Column(name = "statusBefore", nullable = true)
    private Integer               statusBefore;

    @Column(name = "statusAfter", nullable = true)
    private Integer               statusAfter;

    @Column(name = "timestamp", nullable = true)
    private Date               timestamp;
    
    @Column(name = "operationID", nullable = true)
    private String             operationID;

    @Column(name = "statusCode", nullable = true)
    private String             statusCode;

    @Column(name = "message", nullable = true)
    private String             message;

    
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="paymentinfo_id")
    private PaymentInfoBean paymentInfo;

    public PaymentInfoLogStatusBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getDpanBefore() {
        return dpanBefore;
    }

    public void setDpanBefore(String dpanBefore) {
        this.dpanBefore = dpanBefore;
    }

    public String getDpanAfter() {
        return dpanAfter;
    }

    public void setDpanAfter(String dpanAfter) {
        this.dpanAfter = dpanAfter;
    }

    public Integer getStatusBefore() {
        return statusBefore;
    }

    public void setStatusBefore(Integer statusBefore) {
        this.statusBefore = statusBefore;
    }

    public Integer getStatusAfter() {
        return statusAfter;
    }

    public void setStatusAfter(Integer statusAfter) {
        this.statusAfter = statusAfter;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public PaymentInfoBean getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfoBean paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
