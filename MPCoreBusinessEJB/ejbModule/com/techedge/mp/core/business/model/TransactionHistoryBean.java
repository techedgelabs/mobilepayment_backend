package com.techedge.mp.core.business.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PrePaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.TransactionEventHistory;
import com.techedge.mp.core.business.interfaces.TransactionHistory;
import com.techedge.mp.core.business.interfaces.TransactionHistoryAdditionalData;
import com.techedge.mp.core.business.interfaces.TransactionStatusHistory;

@Entity
@Table(name = "TRANSACTIONS_HISTORY")
@NamedQueries({
        @NamedQuery(name = "TransactionHistoryBean.findTransactionByID", query = "select t from TransactionHistoryBean t where t.transactionID = :transactionID"),
        @NamedQuery(name = "TransactionHistoryBean.findTransactionByUser", query = "select t from TransactionHistoryBean t where t.userBean = :userBean"),
        @NamedQuery(name = "TransactionHistoryBean.findActiveTransactionByUser", query = "select t from TransactionHistoryBean t where t.userBean = :userBean and t.confirmed = false"),
        @NamedQuery(name = "TransactionHistoryBean.findTransactionByUserAndDate", query = "select t from TransactionHistoryBean t "
                + "where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),
        @NamedQuery(name = "TransactionHistoryBean.findTransactionSuccessfulByUserAndDate", query = "select t from TransactionHistoryBean t "
                + "where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) and t.finalStatusType = 'SUCCESSFUL' "
                + "order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesis", query = "select "
                + "(select count(t1.id) from TransactionHistoryBean t1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionHistoryBean t2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionHistoryBean t3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),

	    @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAll", query = "select "
	            + "count(t3.id) from TransactionHistoryBean t3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
	            + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
	            + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisBusiness", query = "select "
                + "(select count(t1.id) from TransactionHistoryBean t1 where t1.transactionCategory = 'BUSINESS' and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionHistoryBean t2 where t2.transactionCategory = 'BUSINESS' and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionHistoryBean t3 where t3.transactionCategory = 'BUSINESS' and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllBusiness", query = "select "
                + "count(t3.id) from TransactionHistoryBean t3 where t3.transactionCategory = 'BUSINESS' and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisArea", query = "select "
                + "(select count(t1.id) from TransactionHistoryBean t1, ProvinceInfoBean p1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and "
                + "t1.stationBean.province = p1.name and p1.area = :area and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t1.finalAmount > 0 "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionHistoryBean t2, ProvinceInfoBean p2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and "
                + "t2.stationBean.province = p2.name and p2.area = :area and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t2.finalAmount > 0 "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionHistoryBean t3, ProvinceInfoBean p3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),

	    @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllArea", query = "select "
	            + "count(t3.id) from TransactionHistoryBean t3, ProvinceInfoBean p3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and "
	            + "t3.stationBean.province = p3.name and p3.area = :area and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
	            + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
	            + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAreaBusiness", query = "select "
                + "(select count(t1.id) from TransactionHistoryBean t1, ProvinceInfoBean p1 where t1.transactionCategory = 'BUSINESS' and "
                + "t1.stationBean.province = p1.name and p1.area = :area and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t1.finalAmount > 0 "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionHistoryBean t2, ProvinceInfoBean p2 where t2.transactionCategory = 'BUSINESS' and "
                + "t2.stationBean.province = p2.name and p2.area = :area and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t2.finalAmount > 0 "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionHistoryBean t3, ProvinceInfoBean p3 where t3.transactionCategory = 'BUSINESS' and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllAreaBusiness", query = "select "
                + "count(t3.id) from TransactionHistoryBean t3, ProvinceInfoBean p3 where t3.transactionCategory = 'BUSINESS' and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                        
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisProvince", query = "select "
                + "(select count(t1.id) from TransactionHistoryBean t1 where t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 2 and t1.stationBean.province = :stationProvince) "
                + "and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionHistoryBean t2 where t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 2 and t2.stationBean.province = :stationProvince) "
                + "and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionHistoryBean t3 where t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 2 and t3.stationBean.province = :stationProvince) "
                + "and t3.creationTimestamp <= :dailyEndDate) from TransactionHistoryBean t"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisVoucherConsumed", query = "select "
                + "(select count(t1.id) from TransactionHistoryBean t1 join t1.prePaidConsumeVoucherHistoryBeanList cv1 join cv1.prePaidConsumeVoucherDetailHistoryBean cvd1 "
                + "where cvd1.voucherValue = cvd1.consumedValue and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionHistoryBean t2 join t2.prePaidConsumeVoucherHistoryBeanList cv2 join cv2.prePaidConsumeVoucherDetailHistoryBean cvd2 "
                + "where cvd2.voucherValue = cvd2.consumedValue and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionHistoryBean t3 join t3.prePaidConsumeVoucherHistoryBeanList cv3 join cv3.prePaidConsumeVoucherDetailHistoryBean cvd3 "
                + "where cvd3.voucherValue = cvd3.consumedValue and t3.creationTimestamp <= :dailyEndDate) from TransactionHistoryBean t"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisPromoVoucherConsumed", query = "select "
                + "(select count(t1.id) from TransactionHistoryBean t1 join t1.prePaidConsumeVoucherHistoryBeanList cv1 join cv1.prePaidConsumeVoucherDetailHistoryBean cvd1 "
                + "where cvd1.voucherValue = cvd1.consumedValue and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate) "
                + "and cvd1.voucherCode IN (select pv1.code from PromoVoucherBean pv1 where pv1.code = cvd1.voucherCode and pv1.promotionBean = :promotionBean)), "
                + "(select count(t2.id) from TransactionHistoryBean t2 join t2.prePaidConsumeVoucherHistoryBeanList cv2 join cv2.prePaidConsumeVoucherDetailHistoryBean cvd2 "
                + "where cvd2.voucherValue = cvd2.consumedValue and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate) "
                + "and cvd2.voucherCode IN (select pv2.code from PromoVoucherBean pv2 where pv2.code = cvd2.voucherCode and pv2.promotionBean = :promotionBean)), "
                + "(select count(t3.id) from TransactionHistoryBean t3 join t3.prePaidConsumeVoucherHistoryBeanList cv3 join cv3.prePaidConsumeVoucherDetailHistoryBean cvd3 "
                + "where cvd3.voucherValue = cvd3.consumedValue and t3.creationTimestamp <= :dailyEndDate "
                + "and cvd3.voucherCode IN (select pv3.code from PromoVoucherBean pv3 where pv3.code = cvd3.voucherCode and pv3.promotionBean = :promotionBean)) from TransactionHistoryBean t"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalAmount", query = "select "
                + "(select case when sum(t1.finalAmount) is null then 0.00 else sum(t1.finalAmount) end from TransactionHistoryBean t1 where t1.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.finalAmount) is null then 0.00 else sum(t2.finalAmount) end from TransactionHistoryBean t2 where t2.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.finalAmount) is null then 0.00 else sum(t3.finalAmount) end from TransactionHistoryBean t3 where t3.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalAmount", query = "select "
                + "case when sum(t3.finalAmount) is null then 0.00 else sum(t3.finalAmount) end from TransactionHistoryBean t3 where t3.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalAmountBusiness", query = "select "
                + "(select case when sum(t1.finalAmount) is null then 0.00 else sum(t1.finalAmount) end from TransactionHistoryBean t1 where t1.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t1.transactionCategory = 'BUSINESS' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.finalAmount) is null then 0.00 else sum(t2.finalAmount) end from TransactionHistoryBean t2 where t2.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t2.transactionCategory = 'BUSINESS' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.finalAmount) is null then 0.00 else sum(t3.finalAmount) end from TransactionHistoryBean t3 where t3.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.transactionCategory = 'BUSINESS' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalAmountBusiness", query = "select "
                + "case when sum(t3.finalAmount) is null then 0.00 else sum(t3.finalAmount) end from TransactionHistoryBean t3 where t3.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.transactionCategory = 'BUSINESS' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantity", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionHistoryBean t1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') "
                + "and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionHistoryBean t2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') "
                + "and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') "
                + "and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantity", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') "
                + "and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityBusiness", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionHistoryBean t1 where t1.transactionCategory = 'BUSINESS' "
                + "and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionHistoryBean t2 where t2.transactionCategory = 'BUSINESS' "
                + "and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 where t3.transactionCategory = 'BUSINESS' "
                + "and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),

		@NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityBusiness", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 where t3.transactionCategory = 'BUSINESS' "
                + "and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityByProduct", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionHistoryBean t1 where (t1.transactionCategory = 'CUSTOMER' or t1.transactionCategory is null) "
                + "and t1.productDescription = :productDescription and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionHistoryBean t2 where (t2.transactionCategory = 'CUSTOMER' or t2.transactionCategory is null)  "
                + "and t2.productDescription = :productDescription and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 where (t3.transactionCategory = 'CUSTOMER'or t3.transactionCategory is null) "
                + "and t3.productDescription = :productDescription and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityByProduct", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 where (t3.transactionCategory = 'CUSTOMER' or t3.transactionCategory is null) "
                + "and t3.productDescription = :productDescription and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityByProductBusiness", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionHistoryBean t1 where t1.transactionCategory = 'BUSINESS' "
                + "and t1.productDescription = :productDescription and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionHistoryBean t2 where t2.transactionCategory = 'BUSINESS' "
                + "and t2.productDescription = :productDescription and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 where t3.transactionCategory = 'BUSINESS' "
                + "and t3.productDescription = :productDescription and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityByProductBusiness", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 where t3.transactionCategory = 'BUSINESS' "
                + "and t3.productDescription = :productDescription and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportDetail", query = "select t from TransactionHistoryBean t where (t.transactionCategory is null or t.transactionCategory = 'CUSTOMER') "
                + "and t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t.finalAmount > 0 and t.stationBean.stationStatus = 2 "
                + "and t.creationTimestamp <= :dailyEndDate order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportDetailBusiness", query = "select t from TransactionHistoryBean t where t.transactionCategory = 'BUSINESS' "
                + "and t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t.finalAmount > 0 and t.stationBean.stationStatus = 2 "
                + "and t.creationTimestamp <= :dailyEndDate order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportDetailByDate", query = "select t from TransactionHistoryBean t where (t.transactionCategory is null or t.transactionCategory = 'CUSTOMER') "
                + "and t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t.finalAmount > 0 and t.stationBean.stationStatus = 2 "
                + "and (t.creationTimestamp >= :dailyStartDate and t.creationTimestamp < :dailyEndDate) order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionHistoryBean.statisticsReportDetailByDateBusiness", query = "select t from TransactionHistoryBean t where t.transactionCategory = 'BUSINESS' "
                + "and t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t.finalAmount > 0 and t.stationBean.stationStatus = 2 "
                + "and (t.creationTimestamp >= :dailyStartDate and t.creationTimestamp < :dailyEndDate) order by t.creationTimestamp desc"),
                                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalLoyalty", query = "select "
                + "(select count(t1.id) from TransactionHistoryBean t1 join t1.prePaidLoadLoyaltyCreditsHistoryBeanList tl1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionHistoryBean t2 join t2.prePaidLoadLoyaltyCreditsHistoryBeanList tl2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionHistoryBean t3 join t3.prePaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalLoyalty", query = "select "
                + "count(t3.id) from TransactionHistoryBean t3 join t3.prePaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyArea", query = "select "
                + "(select count(t1.id) from TransactionHistoryBean t1, ProvinceInfoBean p1 join t1.prePaidLoadLoyaltyCreditsHistoryBeanList tl1 "
                + "where t1.stationBean.province = p1.name and p1.area = :area and tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 "
                + "and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionHistoryBean t2, ProvinceInfoBean p2 join t2.prePaidLoadLoyaltyCreditsHistoryBeanList tl2 "
                + "where t2.stationBean.province = p2.name and p2.area = :area and tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 "
                + "and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionHistoryBean t3, ProvinceInfoBean p3 join t3.prePaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where t3.stationBean.province = p3.name and p3.area = :area and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 "
                + "and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyArea", query = "select "
                + "count(t3.id) from TransactionHistoryBean t3, ProvinceInfoBean p3 join t3.prePaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where t3.stationBean.province = p3.name and p3.area = :area and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 "
                + "and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyCredits", query = "select "
                + "(select case when sum(tl1.credits) is null then 0 else sum(tl1.credits) end from TransactionHistoryBean t1 join t1.prePaidLoadLoyaltyCreditsHistoryBeanList tl1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(tl2.credits) is null then 0 else sum(tl2.credits) end from TransactionHistoryBean t2 join t2.prePaidLoadLoyaltyCreditsHistoryBeanList tl2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(tl3.credits) is null then 0 else sum(tl3.credits) end from TransactionHistoryBean t3 join t3.prePaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),
        
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyCredits", query = "select "
                + "case when sum(tl3.credits) is null then 0 else sum(tl3.credits) end from TransactionHistoryBean t3 join t3.prePaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
        
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyFuelQuantity", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionHistoryBean t1 join t1.prePaidLoadLoyaltyCreditsHistoryBeanList tl1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionHistoryBean t2 join t2.prePaidLoadLoyaltyCreditsHistoryBeanList tl2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 join t3.prePaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantity", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 join t3.prePaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyFuelQuantityByProduct", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionHistoryBean t1 join t1.prePaidLoadLoyaltyCreditsHistoryBeanList tl1 "
                + "where t1.productDescription = :productDescription and tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionHistoryBean t2 join t2.prePaidLoadLoyaltyCreditsHistoryBeanList tl2 "
                + "where t2.productDescription = :productDescription and tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 join t3.prePaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where t3.productDescription = :productDescription and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionHistoryBean t"),
                
        @NamedQuery(name = "TransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantityByProduct", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionHistoryBean t3 join t3.prePaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where t3.productDescription = :productDescription and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
})
public class TransactionHistoryBean {

    public static final String                        FIND_BY_ID                                              = "TransactionHistoryBean.findTransactionByID";
    public static final String                        FIND_BY_USER                                            = "TransactionHistoryBean.findTransactionByUser";
    public static final String                        FIND_ACTIVE_BY_USER                                     = "TransactionHistoryBean.findActiveTransactionByUser";
    public static final String                        FIND_BY_USER_AND_DATE                                   = "TransactionHistoryBean.findTransactionByUserAndDate";
    public static final String                        FIND_SUCCESSFUL_BY_USER_AND_DATE                        = "TransactionHistoryBean.findTransactionSuccessfulByUserAndDate";
    public static final String                        STATISTICS_REPORT_DETAIL                                = "TransactionHistoryBean.statisticsReportDetail";
    public static final String                        STATISTICS_REPORT_DETAIL_BUSINESS                       = "TransactionHistoryBean.statisticsReportDetailBusiness";
    public static final String                        STATISTICS_REPORT_DETAIL_BY_DATE                        = "TransactionHistoryBean.statisticsReportDetailByDate";
    public static final String                        STATISTICS_REPORT_DETAIL_BY_DATE_BUSINESS               = "TransactionHistoryBean.statisticsReportDetailByDateBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS                             = "TransactionHistoryBean.statisticsReportSynthesis";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL                         = "TransactionHistoryBean.statisticsReportSynthesisAll";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_BUSINESS                    = "TransactionHistoryBean.statisticsReportSynthesisBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_BUSINESS                = "TransactionHistoryBean.statisticsReportSynthesisAllBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_AREA                        = "TransactionHistoryBean.statisticsReportSynthesisArea";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_AREA                    = "TransactionHistoryBean.statisticsReportSynthesisAllArea";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_AREA_BUSINESS               = "TransactionHistoryBean.statisticsReportSynthesisAreaBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_AREA_BUSINESS           = "TransactionHistoryBean.statisticsReportSynthesisAllAreaBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_PROVINCE                    = "TransactionHistoryBean.statisticsReportSynthesisProvince";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_VOUCHER_CONSUMED            = "TransactionHistoryBean.statisticsReportSynthesisVoucherConsumed";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_PROMO_VOUCHER_CONSUMED      = "TransactionHistoryBean.statisticsReportSynthesisPromoVoucherConsumed";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT                = "TransactionHistoryBean.statisticsReportSynthesisTotalAmount";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT            = "TransactionHistoryBean.statisticsReportSynthesisAllTotalAmount";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT_BUSINESS       = "TransactionHistoryBean.statisticsReportSynthesisTotalAmountBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT_BUSINESS   = "TransactionHistoryBean.statisticsReportSynthesisAllTotalAmountBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY         = "TransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantity";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY     = "TransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantity";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BUSINESS = "TransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BUSINESS = "TransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT = "TransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityByProduct";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT = "TransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityByProduct";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS = "TransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityByProductBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS = "TransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityByProductBusiness";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY               = "TransactionHistoryBean.statisticsReportSynthesisTotalLoyalty";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY           = "TransactionHistoryBean.statisticsReportSynthesisAllTotalLoyalty";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_AREA          = "TransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyArea";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_AREA      = "TransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyArea";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_CREDITS       = "TransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyCredits";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_CREDITS   = "TransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyCredits";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY = "TransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyFuelQuantity";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY = "TransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantity";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT = "TransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyFuelQuantityByProduct";
    public static final String                        STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT = "TransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantityByProduct";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                                  id;

    @Column(name = "transactionID", nullable = true)
    private String                                transactionID;

    @Column(name = "archiving_date", nullable = false)
    private Date                                  archivingDate;

    @OneToOne()
    @JoinColumn(name = "user_id")
    private UserBean                              userBean;

    @OneToOne()
    @JoinColumn(name = "station_id")
    private StationBean                           stationBean;

    @Column(name = "use_voucher", nullable = true)
    private Boolean                               useVoucher;

    @Column(name = "creation_timestamp", nullable = false)
    private Date                                  creationTimestamp;

    @Column(name = "end_refuel_timestamp", nullable = true)
    private Date                                  endRefuelTimestamp;

    @Column(name = "initial_amount", nullable = true)
    private Double                                initialAmount;

    @Column(name = "final_amount", nullable = true)
    private Double                                finalAmount;

    @Column(name = "fuel_quantity", nullable = true)
    private Double                                fuelQuantity;

    @Column(name = "fuel_amount", nullable = true)
    private Double                                fuelAmount;
    
    @Column(name = "unit_price", nullable = true)
    private Double                                unitPrice;

    @Column(name = "pump_id", nullable = true)
    private String                                pumpID;

    @Column(name = "pump_number", nullable = true)
    private Integer                               pumpNumber;

    @Column(name = "product_id", nullable = true)
    private String                                productID;

    @Column(name = "product_description", nullable = true)
    private String                                productDescription;

    @Column(name = "currency", nullable = true)
    private String                                currency;

    @Column(name = "shop_login", nullable = true)
    private String                                shopLogin;

    @Column(name = "acquirer_id", nullable = true)
    private String                                acquirerID;

    @Column(name = "bank_transaction_id", nullable = true)
    private String                                bankTansactionID;

    @Column(name = "authorization_code", nullable = true)
    private String                                authorizationCode;

    @Column(name = "token", nullable = true)
    private String                                token;

    @Column(name = "payment_type", nullable = true)
    private String                                paymentType;

    @Column(name = "final_status_type", nullable = true)
    private String                                finalStatusType;

    @Column(name = "gfg_notification", nullable = false)
    private Boolean                               GFGNotification;

    @Column(name = "confirmed", nullable = false)
    private Boolean                               confirmed;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionHistoryBean")
    private Set<TransactionStatusHistoryBean>     transactionStatusHistoryBeanData                   = new HashSet<TransactionStatusHistoryBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionHistoryBean")
    private Set<TransactionEventHistoryBean>      transactionEventHistoryBeanData                    = new HashSet<TransactionEventHistoryBean>(0);

    @Column(name = "payment_method_id", nullable = true)
    private Long                                  paymentMethodId;

    @Column(name = "payment_method_type", nullable = true)
    private String                                paymentMethodType;

    @Column(name = "server_name", nullable = true)
    private String                                serverName;

    @Column(name = "status_attempts_left", nullable = true)
    private Integer                               statusAttemptsLeft;

    @Column(name = "out_of_range", nullable = true)
    private String                                outOfRange;

    @Column(name = "refuel_mode", nullable = true)
    private String                                refuelMode;

    @Column(name = "voucher_reconciliation", nullable = true)
    private Boolean                               voucherReconciliation;

    @Column(name = "reconciliation_attempts_left", nullable = true)
    private Integer                        reconciliationAttemptsLeft;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionHistoryBean")
    private Set<PrePaidConsumeVoucherHistoryBean> prePaidConsumeVoucherHistoryBeanList               = new HashSet<PrePaidConsumeVoucherHistoryBean>(0);
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionHistoryBean", cascade = CascadeType.ALL)
    private Set<PrePaidLoadLoyaltyCreditsHistoryBean>      prePaidLoadLoyaltyCreditsHistoryBeanList                 = new HashSet<PrePaidLoadLoyaltyCreditsHistoryBean>(0);

    @Column(name = "voucherStatus", nullable = true)
    private String                         voucherHistoryStatus;
    
    @Column(name = "src_transaction_id", nullable = true)
    private String                         srcTransactionID;
    
    @Column(name = "encoded_secret_key", nullable = true)
    private String                         encodedSecretKey;
    
    @Column(name = "group_acquirer", nullable = true)
    private String                         groupAcquirer;
    
    @Column(name = "gfg_electronic_Invoice_id", nullable = true)
    private String                         gfgElectronicInvoiceID;

    @Column(name = "transaction_category", nullable = true)
    private String                            transactionCategory;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionBean", cascade = CascadeType.ALL)
    private Set<TransactionHistoryAdditionalDataBean> transactionHistoryAdditionalDataBeanList = new HashSet<TransactionHistoryAdditionalDataBean>(0);


    public TransactionHistoryBean() {}

    public TransactionHistoryBean(TransactionHistory transaction) {

        //this.id = transaction.getId();
        this.transactionID = transaction.getTransactionID();
        this.archivingDate = transaction.getArchivingDate();
        this.userBean = new UserBean(transaction.getUser());
        this.stationBean = new StationBean(transaction.getStation());
        this.useVoucher = transaction.getUseVoucher();
        this.creationTimestamp = transaction.getCreationTimestamp();
        this.endRefuelTimestamp = transaction.getEndRefuelTimestamp();
        this.initialAmount = transaction.getInitialAmount();
        this.finalAmount = transaction.getFinalAmount();
        this.fuelAmount = transaction.getFuelAmount();
        this.fuelQuantity = transaction.getFuelQuantity();
        this.unitPrice = transaction.getUnitPrice();
        this.pumpID = transaction.getPumpID();
        this.pumpNumber = transaction.getPumpNumber();
        this.productID = transaction.getProductID();
        this.productDescription = transaction.getProductDescription();
        this.currency = transaction.getCurrency();
        this.shopLogin = transaction.getShopLogin();
        this.acquirerID = transaction.getAcquirerID();
        this.bankTansactionID = transaction.getBankTansactionID();
        this.authorizationCode = transaction.getAuthorizationCode();
        this.token = transaction.getToken();
        this.productID = transaction.getProductID();
        this.paymentType = transaction.getPaymentType();
        this.finalStatusType = transaction.getFinalStatusType();
        this.GFGNotification = transaction.getGFGNotification();
        this.confirmed = transaction.getConfirmed();
        this.paymentMethodId = transaction.getPaymentMethodId();
        this.paymentMethodType = transaction.getPaymentMethodType();
        this.serverName = transaction.getServerName();
        this.statusAttemptsLeft = transaction.getStatusAttemptsLeft();
        this.outOfRange = transaction.getOutOfRange();
        this.refuelMode = transaction.getRefuelMode();
        this.voucherReconciliation = transaction.getVoucherReconciliation();
        this.reconciliationAttemptsLeft = transaction.getReconciliationAttemptsLeft();
        this.voucherHistoryStatus = transaction.getVoucherHistoryStatus();
        this.srcTransactionID = transaction.getSrcTransactionID();
        this.encodedSecretKey = transaction.getEncodedSecretKey();
        this.groupAcquirer = transaction.getGroupAcquirer();
        this.gfgElectronicInvoiceID = transaction.getGFGElectronicInvoiceID();
        this.transactionCategory = transaction.getTransactionCategory().getValue();

        if (transaction.getTransactionStatusHistoryData() != null) {
            Set<TransactionStatusHistory> transactionStatusData = transaction.getTransactionStatusHistoryData();
            for (TransactionStatusHistory transactionStatus : transactionStatusData) {
                TransactionStatusHistoryBean transactionStatusBean = new TransactionStatusHistoryBean(transactionStatus);
                transactionStatusBean.setTransactionHistoryBean(this);
                this.transactionStatusHistoryBeanData.add(transactionStatusBean);
            }
        }

        if (transaction.getTransactionEventHistoryData() != null) {
            Set<TransactionEventHistory> transactionEventData = transaction.getTransactionEventHistoryData();
            for (TransactionEventHistory transactionEvent : transactionEventData) {
                TransactionEventHistoryBean transactionEventBean = new TransactionEventHistoryBean(transactionEvent);
                transactionEventBean.setTransactionHistoryBean(this);
                this.transactionEventHistoryBeanData.add(transactionEventBean);
            }
        }

        if (transaction.getPrePaidConsumeVoucherList() != null) {
            for (PrePaidConsumeVoucher prePaidConsumeVoucher : transaction.getPrePaidConsumeVoucherList()) {
                PrePaidConsumeVoucherHistoryBean prePaidConsumeVoucherHistoryBean = new PrePaidConsumeVoucherHistoryBean(prePaidConsumeVoucher);
                prePaidConsumeVoucherHistoryBean.setTransactionHistoryBean(this);
                this.prePaidConsumeVoucherHistoryBeanList.add(prePaidConsumeVoucherHistoryBean);
            }
        }
        
        if (transaction.getPrePaidLoadLoyaltyCreditsBeanList() != null) {
            for (PrePaidLoadLoyaltyCredits prePaidLoadLoyaltyCredits : transaction.getPrePaidLoadLoyaltyCreditsBeanList()) {
                PrePaidLoadLoyaltyCreditsHistoryBean prePaidLoadLoyaltyCreditsHistoryBean = new PrePaidLoadLoyaltyCreditsHistoryBean(prePaidLoadLoyaltyCredits);
                prePaidLoadLoyaltyCreditsHistoryBean.setTransactionHistoryBean(this);
                this.prePaidLoadLoyaltyCreditsHistoryBeanList.add(prePaidLoadLoyaltyCreditsHistoryBean);
            }
        }

        if (transaction.getTransactionHistoryAdditionalDataList() != null) {
            for (TransactionHistoryAdditionalData transactionHistoryAdditionalData : transaction.getTransactionHistoryAdditionalDataList()) {
                TransactionHistoryAdditionalDataBean transactionHistoryAdditionalDataBean = new TransactionHistoryAdditionalDataBean(transactionHistoryAdditionalData);
                transactionHistoryAdditionalDataBean.setTransactionBean(this);
                this.transactionHistoryAdditionalDataBeanList.add(transactionHistoryAdditionalDataBean);
            }
        }
    }

    public TransactionHistoryBean(TransactionBean transactionBean) {

        System.out.println("Conversione TransactionBean -> TransactionHistoryBean");
        
        this.transactionID = transactionBean.getTransactionID();
        this.archivingDate = null;
        this.userBean = transactionBean.getUserBean();
        this.stationBean = transactionBean.getStationBean();
        this.useVoucher = transactionBean.getUseVoucher();
        this.creationTimestamp = transactionBean.getCreationTimestamp();
        this.endRefuelTimestamp = transactionBean.getEndRefuelTimestamp();
        this.initialAmount = transactionBean.getInitialAmount();
        this.finalAmount = transactionBean.getFinalAmount();
        this.fuelAmount = transactionBean.getFuelAmount();
        this.fuelQuantity = transactionBean.getFuelQuantity();
        this.unitPrice = transactionBean.getUnitPrice();
        this.pumpID = transactionBean.getPumpID();
        this.pumpNumber = transactionBean.getPumpNumber();
        this.productID = transactionBean.getProductID();
        this.productDescription = transactionBean.getProductDescription();
        this.currency = transactionBean.getCurrency();
        this.shopLogin = transactionBean.getShopLogin();
        this.acquirerID = transactionBean.getAcquirerID();
        this.bankTansactionID = transactionBean.getBankTansactionID();
        this.authorizationCode = transactionBean.getAuthorizationCode();
        this.token = transactionBean.getToken();
        this.productID = transactionBean.getProductID();
        this.paymentType = transactionBean.getPaymentType();
        this.finalStatusType = transactionBean.getFinalStatusType();
        this.GFGNotification = transactionBean.getGFGNotification();
        this.confirmed = transactionBean.getConfirmed();
        this.paymentMethodId = transactionBean.getPaymentMethodId();
        this.paymentMethodType = transactionBean.getPaymentMethodType();
        this.serverName = transactionBean.getServerName();
        this.statusAttemptsLeft = transactionBean.getStatusAttemptsLeft();
        this.outOfRange = transactionBean.getOutOfRange();
        this.refuelMode = transactionBean.getRefuelMode();
        this.voucherReconciliation = transactionBean.getVoucherReconciliation();
        this.reconciliationAttemptsLeft = transactionBean.getReconciliationAttemptsLeft();
        this.voucherHistoryStatus = transactionBean.getVoucherStatus();
        this.srcTransactionID = transactionBean.getSrcTransactionID();
        this.encodedSecretKey = transactionBean.getEncodedSecretKey();
        this.groupAcquirer = transactionBean.getGroupAcquirer();
        this.gfgElectronicInvoiceID = transactionBean.getGFGElectronicInvoiceID();
        this.transactionCategory = transactionBean.getTransactionCategory().getValue();

        if (transactionBean.getTransactionStatusBeanData() != null) {
            Set<TransactionStatusBean> transactionStatusBeanData = transactionBean.getTransactionStatusBeanData();
            for (TransactionStatusBean transactionStatusBean : transactionStatusBeanData) {
                TransactionStatusHistoryBean transactionStatusHistoryBean = new TransactionStatusHistoryBean(transactionStatusBean);
                transactionStatusHistoryBean.setTransactionHistoryBean(this);
                this.transactionStatusHistoryBeanData.add(transactionStatusHistoryBean);
            }
        }

        if (transactionBean.getTransactionEventBeanData() != null) {
            Set<TransactionEventBean> transactionEventBeanData = transactionBean.getTransactionEventBeanData();
            for (TransactionEventBean transactionEventBean : transactionEventBeanData) {
                TransactionEventHistoryBean transactionEventHistoryBean = new TransactionEventHistoryBean(transactionEventBean);
                transactionEventHistoryBean.setTransactionHistoryBean(this);
                this.transactionEventHistoryBeanData.add(transactionEventHistoryBean);
            }
        }

        if (transactionBean.getPrePaidConsumeVoucherBeanList() != null) {
            for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {
                PrePaidConsumeVoucherHistoryBean prePaidConsumeVoucherHistoryBean = new PrePaidConsumeVoucherHistoryBean(prePaidConsumeVoucherBean);
                prePaidConsumeVoucherHistoryBean.setTransactionHistoryBean(this);
                this.prePaidConsumeVoucherHistoryBeanList.add(prePaidConsumeVoucherHistoryBean);
            }
        }
        
        if (transactionBean.getPrePaidLoadLoyaltyCreditsBeanList() != null) {
            System.out.println("Elaborazione PrePaidLoadLoyaltyCreditsBean");
            for (PrePaidLoadLoyaltyCreditsBean prePaidLoadLoyaltyCreditsBean : transactionBean.getPrePaidLoadLoyaltyCreditsBeanList()) {
                PrePaidLoadLoyaltyCreditsHistoryBean prePaidLoadLoyaltyCreditsHistoryBean = new PrePaidLoadLoyaltyCreditsHistoryBean(prePaidLoadLoyaltyCreditsBean);
                prePaidLoadLoyaltyCreditsHistoryBean.setTransactionHistoryBean(this);
                this.prePaidLoadLoyaltyCreditsHistoryBeanList.add(prePaidLoadLoyaltyCreditsHistoryBean);
            }
        }
        
        if (transactionBean.getTransactionAdditionalDataBeanList() != null) {
            System.out.println("Elaborazione TransactionAdditionalDataBeanList");
            for (TransactionAdditionalDataBean transactionAdditionalDataBean : transactionBean.getTransactionAdditionalDataBeanList()) {
                TransactionHistoryAdditionalDataBean transactionHistoryAdditionalDataBean = new TransactionHistoryAdditionalDataBean(transactionAdditionalDataBean);
                transactionHistoryAdditionalDataBean.setTransactionBean(this);
                System.out.println("Inserimento AdditionalDataBean in transactionHistory");
                this.transactionHistoryAdditionalDataBeanList.add(transactionHistoryAdditionalDataBean);
            }
        }
    }

    public TransactionStatusHistoryBean getLastTransactionStatusHistory() {
        TransactionStatusHistoryBean transactionStatusHistory = null;
        int sequence = 0;

        for (TransactionStatusHistoryBean transactionStatusHistoryBean : getTransactionStatusHistoryBeanData()) {
            //System.out.println("sequenceID:" + transactionStatusHistoryBean.getSequenceID());
            if (transactionStatusHistoryBean.getSequenceID() != null && transactionStatusHistoryBean.getSequenceID().intValue() > sequence) {
                transactionStatusHistory = transactionStatusHistoryBean;
                sequence = transactionStatusHistoryBean.getSequenceID().intValue();
                //System.out.println("sequence selected:" + sequence);
            }
        }

        //System.out.println("transactionStatusHistory sequenceID:" + (transactionStatusHistory != null ? transactionStatusHistory.getSequenceID().intValue() : 0));
        return transactionStatusHistory;
    }

    public TransactionHistory toTransactionHistory() {

        TransactionHistory transaction = new TransactionHistory();
        transaction.setId(this.id);
        transaction.setTransactionID(this.transactionID);
        transaction.setArchivingDate(this.archivingDate);
        transaction.setUser(this.userBean.toUser());
        transaction.setStation(this.stationBean.toStation());
        transaction.setUseVoucher(this.useVoucher);
        transaction.setCreationTimestamp(this.creationTimestamp);
        transaction.setEndRefuelTimestamp(this.endRefuelTimestamp);
        transaction.setInitialAmount(this.initialAmount);
        transaction.setFinalAmount(this.finalAmount);
        transaction.setFuelAmount(this.fuelAmount);
        transaction.setFuelQuantity(this.fuelQuantity);
        transaction.setUnitPrice(this.unitPrice);
        transaction.setPumpID(this.pumpID);
        transaction.setPumpNumber(this.pumpNumber);
        transaction.setProductID(this.productID);
        transaction.setProductDescription(this.productDescription);
        transaction.setCurrency(this.currency);
        transaction.setShopLogin(this.shopLogin);
        transaction.setAcquirerID(this.acquirerID);
        transaction.setBankTansactionID(this.bankTansactionID);
        transaction.setAuthorizationCode(this.authorizationCode);
        transaction.setToken(this.token);
        transaction.setProductID(this.productID);
        transaction.setPaymentType(this.paymentType);
        transaction.setFinalStatusType(this.finalStatusType);
        transaction.setGFGNotification(this.GFGNotification);
        transaction.setConfirmed(this.confirmed);
        transaction.setPaymentMethodId(this.paymentMethodId);
        transaction.setPaymentMethodType(this.paymentMethodType);
        transaction.setServerName(this.serverName);
        transaction.setStatusAttemptsLeft(this.statusAttemptsLeft);
        transaction.setOutOfRange(this.outOfRange);
        transaction.setRefuelMode(this.refuelMode);
        transaction.setVoucherReconciliation(this.voucherReconciliation);
        transaction.setReconciliationAttemptsLeft(this.reconciliationAttemptsLeft);
        transaction.setVoucherHistoryStatus(voucherHistoryStatus);
        transaction.setSrcTransactionID(this.srcTransactionID);
        transaction.setEncodedSecretKey(this.encodedSecretKey);
        transaction.setGroupAcquirer(this.groupAcquirer);
        transaction.setGFGElectronicInvoiceID(this.gfgElectronicInvoiceID);
        transaction.setTransactionCategory(getTransactionCategory());

        if (!this.transactionStatusHistoryBeanData.isEmpty()) {

            Set<TransactionStatusHistoryBean> transactionStatusBeanData = this.transactionStatusHistoryBeanData;
            for (TransactionStatusHistoryBean transactionStatusBean : transactionStatusBeanData) {

                TransactionStatusHistory transactionStatus = transactionStatusBean.toTransactionStatus();
                transaction.getTransactionStatusHistoryData().add(transactionStatus);
            }
        }

        if (!this.transactionEventHistoryBeanData.isEmpty()) {

            Set<TransactionEventHistoryBean> transactionEventBeanData = this.transactionEventHistoryBeanData;
            for (TransactionEventHistoryBean transactionEventBean : transactionEventBeanData) {

                TransactionEventHistory transactionEvent = transactionEventBean.toTransactionEventHistory();
                transaction.getTransactionEventHistoryData().add(transactionEvent);
            }
        }

        if (!this.prePaidConsumeVoucherHistoryBeanList.isEmpty()) {

            Set<PrePaidConsumeVoucherHistoryBean> prePaidConsumeVoucherHistoryBeanList = this.prePaidConsumeVoucherHistoryBeanList;
            for (PrePaidConsumeVoucherHistoryBean prePaidConsumeVoucherHistoryBean : prePaidConsumeVoucherHistoryBeanList) {

                PrePaidConsumeVoucher prePaidConsumeVoucher = prePaidConsumeVoucherHistoryBean.toPrePaidConsumeVoucher();
                transaction.getPrePaidConsumeVoucherList().add(prePaidConsumeVoucher);
            }
        }
        
        if (!this.prePaidLoadLoyaltyCreditsHistoryBeanList.isEmpty()) {

            Set<PrePaidLoadLoyaltyCreditsHistoryBean> prePaidLoadLoyaltyCreditsHistoryBeanList = this.prePaidLoadLoyaltyCreditsHistoryBeanList;
            for (PrePaidLoadLoyaltyCreditsHistoryBean prePaidLoadLoyaltyCreditsHistoryBean : prePaidLoadLoyaltyCreditsHistoryBeanList) {

                PrePaidLoadLoyaltyCredits prePaidLoadLoyaltyCredits = prePaidLoadLoyaltyCreditsHistoryBean.toPrePaidLoadLoyaltyCredits();
                transaction.getPrePaidLoadLoyaltyCreditsBeanList().add(prePaidLoadLoyaltyCredits);
            }
        }
        
        if (!this.transactionHistoryAdditionalDataBeanList.isEmpty()) {

            Set<TransactionHistoryAdditionalDataBean> transactionHistoryAdditionalDataBeanList = this.transactionHistoryAdditionalDataBeanList;
            for (TransactionHistoryAdditionalDataBean transactionHistoryAdditionalDataBean : transactionHistoryAdditionalDataBeanList) {

                TransactionHistoryAdditionalData transactionHistoryAdditionalData = transactionHistoryAdditionalDataBean.toTransactionHistoryAdditionalData();
                transaction.getTransactionHistoryAdditionalDataList().add(transactionHistoryAdditionalData);
            }
        }

        return transaction;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public StationBean getStationBean() {
        return stationBean;
    }

    public void setStationBean(StationBean stationBean) {
        this.stationBean = stationBean;
    }

    public Boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getEndRefuelTimestamp() {
        return endRefuelTimestamp;
    }

    public void setEndRefuelTimestamp(Date endRefuelTimestamp) {
        this.endRefuelTimestamp = endRefuelTimestamp;
    }

    public Double getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(Double initialAmount) {
        this.initialAmount = initialAmount;
    }

    public Double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(Double finalAmount) {
        this.finalAmount = finalAmount;
    }

    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public Double getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(Double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public String getPumpID() {
        return pumpID;
    }

    public void setPumpID(String pumpID) {
        this.pumpID = pumpID;
    }

    public Integer getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(Integer pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
        this.acquirerID = acquirerID;
    }

    public String getBankTansactionID() {
        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {
        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getFinalStatusType() {
        return finalStatusType;
    }

    public void setFinalStatusType(String finalStatusType) {
        this.finalStatusType = finalStatusType;
    }

    public Boolean getGFGNotification() {
        return GFGNotification;
    }

    public void setGFGNotification(Boolean gFGNotification) {
        GFGNotification = gFGNotification;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Set<TransactionStatusHistoryBean> getTransactionStatusHistoryBeanData() {
        return transactionStatusHistoryBeanData;
    }

    public void setTransactionStatusBeanData(Set<TransactionStatusHistoryBean> transactionStatusBeanData) {
        this.transactionStatusHistoryBeanData = transactionStatusBeanData;
    }

    public Set<TransactionEventHistoryBean> getTransactionEventHistoryBeanData() {
        return transactionEventHistoryBeanData;
    }

    public void setTransactionEventHistoryBeanData(Set<TransactionEventHistoryBean> transactionEventHistoryBeanData) {
        this.transactionEventHistoryBeanData = transactionEventHistoryBeanData;
    }

    public void setTransactionStatusHistoryBeanData(Set<TransactionStatusHistoryBean> transactionStatusHistoryBeanData) {
        this.transactionStatusHistoryBeanData = transactionStatusHistoryBeanData;
    }

    public Date getArchivingDate() {
        return archivingDate;
    }

    public void setArchivingDate(Date archivingDate) {
        this.archivingDate = archivingDate;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public Integer getStatusAttemptsLeft() {
        return statusAttemptsLeft;
    }

    public void setStatusAttemptsLeft(Integer statusAttemptsLeft) {
        this.statusAttemptsLeft = statusAttemptsLeft;
    }

    public String getOutOfRange() {
        return outOfRange;
    }

    public void setOutOfRange(String outOfRange) {
        this.outOfRange = outOfRange;
    }

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    public Boolean getVoucherReconciliation() {
        return voucherReconciliation;
    }

    /**
     * @param voucherReconciliation
     *            the voucherReconciliation to set
     */
    public void setVoucherReconciliation(Boolean voucherReconciliation) {
        this.voucherReconciliation = voucherReconciliation;
    }

    public Set<PrePaidConsumeVoucherHistoryBean> getPrePaidConsumeVoucherHistoryBeanList() {
        return prePaidConsumeVoucherHistoryBeanList;
    }

    public void setPrePaidConsumeVoucherHistoryBeanList(Set<PrePaidConsumeVoucherHistoryBean> prePaidConsumeVoucherHistoryBeanList) {
        this.prePaidConsumeVoucherHistoryBeanList = prePaidConsumeVoucherHistoryBeanList;
    }


    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public String getVoucherHistoryStatus() {
        return voucherHistoryStatus;
    }

    public void setVoucherHistoryStatus(String voucherHistoryStatus) {
        this.voucherHistoryStatus = voucherHistoryStatus;
    }

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public String getEncodedSecretKey() {
        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {
        this.encodedSecretKey = encodedSecretKey;
    }

    public String getGroupAcquirer() {
        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {
        this.groupAcquirer = groupAcquirer;
    }

    public Set<PrePaidLoadLoyaltyCreditsHistoryBean> getPrePaidLoadLoyaltyCreditsHistoryBeanList() {
        return prePaidLoadLoyaltyCreditsHistoryBeanList;
    }

    public void setPrePaidLoadLoyaltyCreditsHistoryBeanList(Set<PrePaidLoadLoyaltyCreditsHistoryBean> prePaidLoadLoyaltyCreditsHistoryBeanList) {
        this.prePaidLoadLoyaltyCreditsHistoryBeanList = prePaidLoadLoyaltyCreditsHistoryBeanList;
    }

    public String getGFGElectronicInvoiceID() {
        return gfgElectronicInvoiceID;
    }
    
    public void setGFGElectronicInvoiceID(String gfgElectronicInvoiceID) {
        this.gfgElectronicInvoiceID = gfgElectronicInvoiceID;
    }

    public TransactionCategoryType getTransactionCategory() {
        if (transactionCategory == null) {
            return TransactionCategoryType.TRANSACTION_CUSTOMER;
        }
        
        return TransactionCategoryType.getValueOf(transactionCategory);
    }
    
    public void setTransactionCategory(TransactionCategoryType transactionCategory) {
        this.transactionCategory = transactionCategory.getValue();
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Set<TransactionHistoryAdditionalDataBean> getTransactionHistoryAdditionalDataBeanList() {
        return transactionHistoryAdditionalDataBeanList;
    }

    public void setTransactionHistoryAdditionalDataBeanList(Set<TransactionHistoryAdditionalDataBean> transactionHistoryAdditionalDataBeanList) {
        this.transactionHistoryAdditionalDataBeanList = transactionHistoryAdditionalDataBeanList;
    }

}
