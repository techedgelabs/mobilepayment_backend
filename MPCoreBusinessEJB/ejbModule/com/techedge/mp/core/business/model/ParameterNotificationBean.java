package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "PARAMS_NOTIFICATION")
@NamedQueries({ @NamedQuery(name = "ParameterNotificationBean.findByName", query = "select p from ParameterNotificationBean p where p.name = :name"),
        @NamedQuery(name = "ParameterNotificationBean.findAll", query = "select p from ParameterNotificationBean p") })
public class ParameterNotificationBean {

    public static final String   FIND_BY_NAME = "ParameterNotificationBean.findByName";
    public static final String   FIND_ALL     = "ParameterNotificationBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                 id;

    @Column(name = "name", nullable = true)
    private String               name;

    @Column(name = "value", nullable = true)
    private String               value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pushNotificationBean", nullable = true)
    private PushNotificationBean pushNotificationBean;

    public ParameterNotificationBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public PushNotificationBean getPushNotificationBean() {
        return pushNotificationBean;
    }

    public void setPushNotificationBean(PushNotificationBean pushNotificationBean) {
        this.pushNotificationBean = pushNotificationBean;
    }

}
