package com.techedge.mp.core.business.model.postpaid;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.core.business.interfaces.PostPaidTransactionEvent;
import com.techedge.mp.core.business.interfaces.PostPaidTransactionHistory;
import com.techedge.mp.core.business.interfaces.PostPaidTransactionPaymentEvent;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionAdditionalData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionHistoryAdditionalData;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.UserBean;

@Entity
@Table(name = "POSTPAIDTRANSACTIONS_HISTORY")
@NamedQueries({
        @NamedQuery(name = "PostPaidTransactionHistoryBean.findTransactionByMPID", query = "select t from PostPaidTransactionHistoryBean t where t.mpTransactionID = :mpTransactionID "),
        // @NamedQuery(name="PostPaidTransactionHistoryBean.findTransactionByQRCode",
        // query="select t from PostPaidTransactionHistoryBean t where t.sourceID = :sourceID and t.mpTransactionStatus = :mpTransactionStatus order by t.creationTimestamp desc"),
        @NamedQuery(name = "PostPaidTransactionHistoryBean.findTransactionByUserAndDate", query = "select t from PostPaidTransactionHistoryBean t "
                + "where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),
        @NamedQuery(name = "PostPaidTransactionHistoryBean.findTransactionPaidByUserAndDate", query = "select t from PostPaidTransactionHistoryBean t where "
                + "t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) and " + "t.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' order by t.creationTimestamp desc"),
        @NamedQuery(name = "PostPaidTransactionHistoryBean.findLastTransactionBySourceId", query = "select t from PostPaidTransactionHistoryBean t "
                + "where t.sourceID = :sourceID and ( t.creationTimestamp >= :startDate and t.creationTimestamp <= :endDate ) order by t.creationTimestamp desc"),
        @NamedQuery(name = "PostPaidTransactionHistoryBean.findDistinctSourceIdByStationId", query = "select distinct(t.sourceID) from PostPaidTransactionHistoryBean t "
                + "where t.stationBean = :stationBean order by t.sourceID"),
        @NamedQuery(name = "PostPaidTransactionHistoryBean.findTransactionByStationIdAndDate", query = "select t from PostPaidTransactionHistoryBean t "
                + "where t.stationBean = :stationBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesis", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and t1.source = :source "
                + "and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and t2.source = :source "
                + "and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAll", query = "select "
                + "count(t3.id) from PostPaidTransactionHistoryBean t3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisBusiness", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1 where t1.transactionCategory = 'BUSINESS' and t1.source = :source "
                + "and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2 where t2.transactionCategory = 'BUSINESS' and t2.source = :source "
                + "and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3 where t3.transactionCategory = 'BUSINESS' and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllBusiness", query = "select "
                + "count(t3.id) from PostPaidTransactionHistoryBean t3 where t3.transactionCategory = 'BUSINESS' and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisArea", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1, ProvinceInfoBean p1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and "
                + "t1.stationBean.province = p1.name and p1.area = :area and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2, ProvinceInfoBean p2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and "
                + "t2.stationBean.province = p2.name and p2.area = :area and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3, ProvinceInfoBean p3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllArea", query = "select "
                + "count(t3.id) from PostPaidTransactionHistoryBean t3, ProvinceInfoBean p3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAreaBusiness", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1, ProvinceInfoBean p1 where t1.transactionCategory = 'BUSINESS' and "
                + "t1.stationBean.province = p1.name and p1.area = :area and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2, ProvinceInfoBean p2 where t2.transactionCategory = 'BUSINESS' and "
                + "t2.stationBean.province = p2.name and p2.area = :area and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3, ProvinceInfoBean p3 where t3.transactionCategory = 'BUSINESS' and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),
                        
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllAreaBusiness", query = "select "
                + "count(t3.id) from PostPaidTransactionHistoryBean t3, ProvinceInfoBean p3 where t3.transactionCategory = 'BUSINESS' and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
        
        
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisRefuelMode", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1 join t1.refuelHistoryBean r1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') "
                + "and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate) and r1.refuelMode = :refuelMode), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2 join t2.refuelHistoryBean r2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') "
                + "and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate) and r2.refuelMode = :refuelMode), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') "
                + "and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode) from PostPaidTransactionHistoryBean t"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllRefuelMode", query = "select "
                + "count(t3.id) from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') "
                + "and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisRefuelModeBusiness", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1 join t1.refuelHistoryBean r1 where t1.transactionCategory = 'BUSINESS' "
                + "and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate) and r1.refuelMode = :refuelMode), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2 join t2.refuelHistoryBean r2 where t2.transactionCategory = 'BUSINESS' "
                + "and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate) and r2.refuelMode = :refuelMode), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 where t3.transactionCategory = 'BUSINESS' "
                + "and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode) from PostPaidTransactionHistoryBean t"),
                        
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllRefuelModeBusiness", query = "select "
                + "count(t3.id) from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 where t3.transactionCategory = 'BUSINESS' "
                + "and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode"),
                       
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisProvince", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1 where t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 2 and t1.stationBean.province = :stationProvince) "
                + "and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2 where t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 2 and t2.stationBean.province = :stationProvince) "
                + "and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3 where t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 2 and t3.stationBean.province = :stationProvince) and t3.creationTimestamp <= :dailyEndDate) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisVoucherConsumed", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1 join t1.postPaidConsumeVoucherHistoryBeanList cv1 join cv1.postPaidConsumeVoucherDetailHistoryBean cvd1 "
                + "where cvd1.voucherValue = cvd1.consumedValue and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2 join t2.postPaidConsumeVoucherHistoryBeanList cv2 join cv2.postPaidConsumeVoucherDetailHistoryBean cvd2 "
                + "where cvd2.voucherValue = cvd2.consumedValue and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3 join t3.postPaidConsumeVoucherHistoryBeanList cv3 join cv3.postPaidConsumeVoucherDetailHistoryBean cvd3 "
                + "where cvd3.voucherValue = cvd3.consumedValue and t3.creationTimestamp <= :dailyEndDate) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisPromoVoucherConsumed", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1 join t1.postPaidConsumeVoucherHistoryBeanList cv1 join cv1.postPaidConsumeVoucherDetailHistoryBean cvd1 "
                + "where cvd1.voucherValue = cvd1.consumedValue and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate) "
                + "and cvd1.voucherCode IN (select pv1.code from PromoVoucherBean pv1 where pv1.code = cvd1.voucherCode and pv1.promotionBean = :promotionBean)), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2 join t2.postPaidConsumeVoucherHistoryBeanList cv2 join cv2.postPaidConsumeVoucherDetailHistoryBean cvd2 "
                + "where cvd2.voucherValue = cvd2.consumedValue and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate) "
                + "and cvd2.voucherCode IN (select pv2.code from PromoVoucherBean pv2 where pv2.code = cvd2.voucherCode and pv2.promotionBean = :promotionBean)), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3 join t3.postPaidConsumeVoucherHistoryBeanList cv3 join cv3.postPaidConsumeVoucherDetailHistoryBean cvd3 "
                + "where cvd3.voucherValue = cvd3.consumedValue and t3.creationTimestamp <= :dailyEndDate "
                + "and cvd3.voucherCode IN (select pv3.code from PromoVoucherBean pv3 where pv3.code = cvd3.voucherCode and pv3.promotionBean = :promotionBean)) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalAmount", query = "select "
                + "(select case when sum(t1.amount) is null then 0.00 else sum(t1.amount) end from PostPaidTransactionHistoryBean t1 "
                + "where t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.amount) is null then 0.00 else sum(t2.amount) end from PostPaidTransactionHistoryBean t2 "
                + "where t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.amount) is null then 0.00 else sum(t3.amount) end from PostPaidTransactionHistoryBean t3 "
                + "where t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalAmount", query = "select "
                + "case when sum(t3.amount) is null then 0.00 else sum(t3.amount) end from PostPaidTransactionHistoryBean t3 "
                + "where t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalAmountBusiness", query = "select "
                + "(select case when sum(t1.amount) is null then 0.00 else sum(t1.amount) end from PostPaidTransactionHistoryBean t1 "
                + "where t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t1.transactionCategory = 'BUSINESS' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.amount) is null then 0.00 else sum(t2.amount) end from PostPaidTransactionHistoryBean t2 "
                + "where t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t2.transactionCategory = 'BUSINESS' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.amount) is null then 0.00 else sum(t3.amount) end from PostPaidTransactionHistoryBean t3 "
                + "where t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t3.transactionCategory = 'BUSINESS' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalAmountBusiness", query = "select "
                + "case when sum(t3.amount) is null then 0.00 else sum(t3.amount) end from PostPaidTransactionHistoryBean t3 "
                + "where t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t3.transactionCategory = 'BUSINESS' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantity", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionHistoryBean t1 join t1.refuelHistoryBean r1 "
                + "where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionHistoryBean t2 join t2.refuelHistoryBean r2 "
                + "where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantity", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityBusiness", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionHistoryBean t1 join t1.refuelHistoryBean r1 "
                + "where t1.transactionCategory = 'BUSINESS' and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionHistoryBean t2 join t2.refuelHistoryBean r2 "
                + "where t2.transactionCategory = 'BUSINESS' and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityBusiness", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityByProduct", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionHistoryBean t1 join t1.refuelHistoryBean r1 "
                + "where (t1.transactionCategory = 'CUSTOMER' or t1.transactionCategory is null) and r1.productDescription = :productDescription and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionHistoryBean t2 join t2.refuelHistoryBean r2 "
                + "where (t2.transactionCategory = 'CUSTOMER' or t2.transactionCategory is null) and r2.productDescription = :productDescription and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where (t3.transactionCategory = 'CUSTOMER'or t3.transactionCategory is null) and r3.productDescription = :productDescription and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityByProduct", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where (t3.transactionCategory = 'CUSTOMER' or t3.transactionCategory is null) and r3.productDescription = :productDescription "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityByProductBusiness", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionHistoryBean t1 join t1.refuelHistoryBean r1 "
                + "where t1.transactionCategory = 'BUSINESS' and r1.productDescription = :productDescription and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionHistoryBean t2 join t2.refuelHistoryBean r2 "
                + "where t2.transactionCategory = 'BUSINESS' and r2.productDescription = :productDescription and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and r3.productDescription = :productDescription and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityByProductBusiness", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and r3.productDescription = :productDescription and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisRefuelModeTotalFuelQuantity", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionHistoryBean t1 join t1.refuelHistoryBean r1 "
                + "where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate) and r1.refuelMode = :refuelMode), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionHistoryBean t2 join t2.refuelHistoryBean r2 "
                + "where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate) and r2.refuelMode = :refuelMode), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllRefuelModeTotalFuelQuantity", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisRefuelModeTotalFuelQuantityBusiness", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionHistoryBean t1 join t1.refuelHistoryBean r1 "
                + "where t1.transactionCategory = 'BUSINESS' and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate) and r1.refuelMode = :refuelMode), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionHistoryBean t2 join t2.refuelHistoryBean r2 "
                + "where t2.transactionCategory = 'BUSINESS' and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate) and r2.refuelMode = :refuelMode), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllRefuelModeTotalFuelQuantityBusiness", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode"),

                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportDetail", query = "select t from PostPaidTransactionHistoryBean t "
                + "where (t.transactionCategory is null or t.transactionCategory = 'CUSTOMER') and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t.amount > 0 and t.stationBean.stationStatus = 2 "
                + "and t.creationTimestamp <= :dailyEndDate order by t.creationTimestamp desc"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportDetailBusiness", query = "select t from PostPaidTransactionHistoryBean t "
                + "where t.transactionCategory = 'BUSINESS' and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t.amount > 0 and t.stationBean.stationStatus = 2 "
                + "and t.creationTimestamp <= :dailyEndDate order by t.creationTimestamp desc"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportDetailByDate", query = "select t from PostPaidTransactionHistoryBean t "
                + "where (t.transactionCategory is null or t.transactionCategory = 'CUSTOMER') and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t.amount > 0 and t.stationBean.stationStatus = 2 "
                + "and (t.creationTimestamp >= :dailyStartDate and t.creationTimestamp < :dailyEndDate) order by t.creationTimestamp desc"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportDetailByDateBusiness", query = "select t from PostPaidTransactionHistoryBean t "
                + "where t.transactionCategory = 'BUSINESS' and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t.amount > 0 and t.stationBean.stationStatus = 2 "
                + "and (t.creationTimestamp >= :dailyStartDate and t.creationTimestamp < :dailyEndDate) order by t.creationTimestamp desc"),
                        
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalLoyalty", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1 join t1.postPaidLoadLoyaltyCreditsHistoryBeanList tl1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2 join t2.postPaidLoadLoyaltyCreditsHistoryBeanList tl2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),
        
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalLoyalty", query = "select "
                + "count(t3.id) from PostPaidTransactionHistoryBean t3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                        + "and t3.creationTimestamp < :dailyEndDate"),
        
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisLoyaltyRefuelmode", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1 join t1.refuelHistoryBean r1 join t1.postPaidLoadLoyaltyCreditsHistoryBeanList tl1 "
                + "where r1.refuelMode = :refuelMode and tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2 join t2.refuelHistoryBean r2 join t2.postPaidLoadLoyaltyCreditsHistoryBeanList tl2 "
                + "where r2.refuelMode = :refuelMode and tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where r3.refuelMode = :refuelMode and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllLoyaltyRefuelmode", query = "select "
                + "count(t3.id) from PostPaidTransactionHistoryBean t3 join t3.refuelHistoryBean r3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where r3.refuelMode = :refuelMode and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyArea", query = "select "
                + "(select count(t1.id) from PostPaidTransactionHistoryBean t1, ProvinceInfoBean p1 join t1.postPaidLoadLoyaltyCreditsHistoryBeanList tl1 "
                + "where t1.stationBean.province = p1.name and p1.area = :area and tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 "
                + "and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionHistoryBean t2, ProvinceInfoBean p2 join t2.postPaidLoadLoyaltyCreditsHistoryBeanList tl2 "
                + "where t2.stationBean.province = p2.name and p2.area = :area and tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 "
                + "and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionHistoryBean t3, ProvinceInfoBean p3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where t3.stationBean.province = p3.name and p3.area = :area and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyArea", query = "select "
                + "count(t3.id) from PostPaidTransactionHistoryBean t3, ProvinceInfoBean p3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where t3.stationBean.province = p3.name and p3.area = :area and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyCredits", query = "select "
                + "(select case when sum(tl1.credits) is null then 0 else sum(tl1.credits) end from PostPaidTransactionHistoryBean t1 join t1.postPaidLoadLoyaltyCreditsHistoryBeanList tl1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(tl2.credits) is null then 0 else sum(tl2.credits) end from PostPaidTransactionHistoryBean t2 join t2.postPaidLoadLoyaltyCreditsHistoryBeanList tl2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(tl3.credits) is null then 0 else sum(tl3.credits) end from PostPaidTransactionHistoryBean t3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyCredits", query = "select "
                + "case when sum(tl3.credits) is null then 0 else sum(tl3.credits) end from PostPaidTransactionHistoryBean t3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyFuelQuantity", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionHistoryBean t1 join t1.postPaidLoadLoyaltyCreditsHistoryBeanList tl1 join t1.refuelHistoryBean r1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionHistoryBean t2 join t2.postPaidLoadLoyaltyCreditsHistoryBeanList tl2 join t2.refuelHistoryBean r2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 join t3.refuelHistoryBean r3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantity", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 join t3.refuelHistoryBean r3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyFuelQuantityByProduct", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionHistoryBean t1 join t1.postPaidLoadLoyaltyCreditsHistoryBeanList tl1 join t1.refuelHistoryBean r1 "
                + "where r1.productDescription = :productDescription and tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionHistoryBean t2 join t2.postPaidLoadLoyaltyCreditsHistoryBeanList tl2 join t2.refuelHistoryBean r2 "
                + "where r2.productDescription = :productDescription and tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 join t3.refuelHistoryBean r3 "
                + "where r3.productDescription = :productDescription and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionHistoryBean t"),    
                
        @NamedQuery(name = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantityByProduct", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionHistoryBean t3 join t3.postPaidLoadLoyaltyCreditsHistoryBeanList tl3 join t3.refuelHistoryBean r3 "
                + "where r3.productDescription = :productDescription and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),  
})
public class PostPaidTransactionHistoryBean {

    public static final String                              FIND_BY_MP_ID                                               = "PostPaidTransactionHistoryBean.findTransactionByMPID";
    public static final String                              FIND_BY_USER_AND_DATE                                       = "PostPaidTransactionHistoryBean.findTransactionByUserAndDate";
    public static final String                              FIND_PAID_BY_USER_AND_DATE                                  = "PostPaidTransactionHistoryBean.findTransactionPaidByUserAndDate";
    public static final String                              FIND_LAST_BY_SOURCE_ID                                      = "PostPaidTransactionHistoryBean.findLastTransactionBySourceId";
    public static final String                              FIND_DISTINCT_SOURCE_ID_BY_STATION_ID                       = "PostPaidTransactionHistoryBean.findDistinctSourceIdByStationId";
    public static final String                              FIND_BY_STATION_AND_DATE                                    = "PostPaidTransactionHistoryBean.findTransactionByStationIdAndDate";
    public static final String                              STATISTICS_REPORT_DETAIL                                    = "PostPaidTransactionHistoryBean.statisticsReportDetail";
    public static final String                              STATISTICS_REPORT_DETAIL_BUSINESS                           = "PostPaidTransactionHistoryBean.statisticsReportDetailBusiness";
    public static final String                              STATISTICS_REPORT_DETAIL_BY_DATE                            = "PostPaidTransactionHistoryBean.statisticsReportDetailByDate";
    public static final String                              STATISTICS_REPORT_DETAIL_BY_DATE_BUSINESS                   = "PostPaidTransactionHistoryBean.statisticsReportDetailByDateBusiness";
    public static final String                              STATISTICS_REPORT_BY_DATE                                   = "PostPaidTransactionHistoryBean.statisticsReportByDate";
    public static final String                              STATISTICS_REPORT_SYNTHESIS                                 = "PostPaidTransactionHistoryBean.statisticsReportSynthesis";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL                             = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAll";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_BUSINESS                        = "PostPaidTransactionHistoryBean.statisticsReportSynthesisBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_All_BUSINESS                    = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_AREA                            = "PostPaidTransactionHistoryBean.statisticsReportSynthesisArea";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_AREA                        = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllArea";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_AREA_BUSINESS                   = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAreaBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_AREA_BUSINESS               = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllAreaBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE                     = "PostPaidTransactionHistoryBean.statisticsReportSynthesisRefuelMode";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE                 = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllRefuelMode";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_BUSINESS            = "PostPaidTransactionHistoryBean.statisticsReportSynthesisRefuelModeBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_BUSINESS        = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllRefuelModeBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_TOTAL_FUEL_QUANTITY = "PostPaidTransactionHistoryBean.statisticsReportSynthesisRefuelModeTotalFuelQuantity";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_TOTAL_FUEL_QUANTITY = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllRefuelModeTotalFuelQuantity";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_TOTAL_FUEL_QUANTITY_BUSINESS = "PostPaidTransactionHistoryBean.statisticsReportSynthesisRefuelModeTotalFuelQuantityBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_TOTAL_FUEL_QUANTITY_BUSINESS = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllRefuelModeTotalFuelQuantityBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_PROVINCE                        = "PostPaidTransactionHistoryBean.statisticsReportSynthesisProvince";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_VOUCHER_CONSUMED                = "PostPaidTransactionHistoryBean.statisticsReportSynthesisVoucherConsumed";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_PROMO_VOUCHER_CONSUMED          = "PostPaidTransactionHistoryBean.statisticsReportSynthesisPromoVoucherConsumed";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT                    = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalAmount";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT                = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalAmount";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT_BUSINESS           = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalAmountBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT_BUSINESS       = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalAmountBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY             = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantity";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY             = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantity";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BUSINESS    = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BUSINESS= "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT    = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityByProduct";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT    = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityByProduct";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS    = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalFuelQuantityByProductBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS    = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalFuelQuantityByProductBusiness";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY                   = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalLoyalty";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY               = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalLoyalty";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_LOYALTY_REFUELMODE              = "PostPaidTransactionHistoryBean.statisticsReportSynthesisLoyaltyRefuelmode";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_LOYALTY_REFUELMODE          = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllLoyaltyRefuelmode";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_AREA              = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyArea";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_AREA          = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyArea";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_CREDITS           = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyCredits";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_CREDITS       = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyCredits";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY     = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyFuelQuantity";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantity";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT = "PostPaidTransactionHistoryBean.statisticsReportSynthesisTotalLoyaltyFuelQuantityByProduct";
    public static final String                              STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT = "PostPaidTransactionHistoryBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantityByProduct";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                                            id;

    // @Column(name="transactionID", nullable=true)
    // private String transactionID;

    @Column(name = "requestID", nullable = true)
    private String                                          requestID;

    @Column(name = "source", nullable = true)
    private String                                          source;

    @Column(name = "sourceID", nullable = true)
    private String                                          sourceID;

    @Column(name = "sourceNumber", nullable = true)
    private String                                          sourceNumber;

    @Column(name = "srcTransactionID", nullable = true)
    private String                                          srcTransactionID;

    @Column(name = "mpTransactionID", nullable = true)
    private String                                          mpTransactionID;

    @Column(name = "bank_transaction_id", nullable = true)
    private String                                          bankTansactionID;

    @Column(name = "authorization_code", nullable = true)
    private String                                          authorizationCode;

    @Column(name = "token", nullable = true)
    private String                                          token;

    @Column(name = "payment_type", nullable = true)
    private String                                          paymentType;

    @Column(name = "amount", nullable = true)
    private Double                                          amount;

    @Column(name = "currency", nullable = true)
    private String                                          currency;

    @Column(name = "product_type", nullable = false)
    private String                                          productType;                                                                                              // OIL||NON_OIL||MIXED

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionHistoryBean")
    private Set<PostPaidCartHistoryBean>                    cartHistoryBean                              = new HashSet<PostPaidCartHistoryBean>(0);
    //
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionHistoryBean")
    private Set<PostPaidRefuelHistoryBean>                  refuelHistoryBean                            = new HashSet<PostPaidRefuelHistoryBean>(0);

    @Column(name = "statusType", nullable = true)
    private String                                          statusType;                                                                                               // Reconciliation-Standard

    @Column(name = "mpTransactionStatus", nullable = true)
    private String                                          mpTransactionStatus;

    @Column(name = "srcTransactionStatus", nullable = true)
    private String                                          srcTransactionStatus;

    @Column(name = "creation_timestamp", nullable = false)
    private Date                                            creationTimestamp;

    @Column(name = "lastModify_timestamp", nullable = true)
    private Date                                            lastModifyTimestamp;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionHistoryBean")
    private Set<PostPaidTransactionEventHistoryBean>        postPaidTransactionEventHistoryBean          = new HashSet<PostPaidTransactionEventHistoryBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionHistoryBean")
    private Set<PostPaidTransactionPaymentEventHistoryBean> postPaidTransactionPaymentEventHistoryBean   = new HashSet<PostPaidTransactionPaymentEventHistoryBean>(0);

    @Column(name = "shop_login", nullable = true)
    private String                                          shopLogin;

    @Column(name = "acquirer_id", nullable = true)
    private String                                          acquirerID;

    @Column(name = "paymentMode", nullable = true)
    private String                                          paymentMode;

    @Column(name = "payment_method_id", nullable = true)
    private Long                                            paymentMethodId;

    @Column(name = "payment_method_type", nullable = true)
    private String                                          paymentMethodType;

    @Column(name = "notificationCreated", nullable = true)
    private Boolean                                         notificationCreated;

    @Column(name = "notificationPaid", nullable = true)
    private Boolean                                         notificationPaid;

    @Column(name = "notificationUser", nullable = true)
    private Boolean                                         notificationUser;

    @OneToOne()
    @JoinColumn(name = "user_id", nullable = true)
    private UserBean                                        userBean;

    @OneToOne()
    @JoinColumn(name = "station_id", nullable = true)
    private StationBean                                     stationBean;

    @Column(name = "archiving_date", nullable = false)
    private Date                                            archivingDate;

    @Column(name = "use_voucher", nullable = true)
    private Boolean                                         useVoucher;
    
    @Column(name = "loyalty_reconcile", nullable = true)
    private Boolean                                  loyaltyReconcile;

    @Column(name = "voucher_reconcile", nullable = true)
    private Boolean                                  voucherReconcile;

    @Column(name = "to_reconcile", nullable = true)
    private Boolean                                  toReconcile;

    @Column(name = "reconciliation_attempts_left", nullable = true)
    private Integer                                  reconciliationAttemptsLeft;    

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionHistoryBean")
    private Set<PostPaidConsumeVoucherHistoryBean>          postPaidConsumeVoucherHistoryBeanList        = new HashSet<PostPaidConsumeVoucherHistoryBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionHistoryBean")
    private Set<PostPaidLoadLoyaltyCreditsHistoryBean>      postPaidLoadLoyaltyCreditsHistoryBeanList    = new HashSet<PostPaidLoadLoyaltyCreditsHistoryBean>(0);
    
    @Column(name = "encoded_secret_key", nullable = true)
    private String                         encodedSecretKey;
    
    @Column(name = "group_acquirer", nullable = true)
    private String                         groupAcquirer;
    
    @Column(name = "gfg_electronic_Invoice_id", nullable = true)
    private String                         gfgElectronicInvoiceID;
    
    @Column(name = "transaction_category", nullable = true)
    private String                            transactionCategory;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionHistoryBean", cascade = CascadeType.ALL)
    private Set<PostPaidTransactionHistoryAdditionalDataBean> postPaidTransactionHistoryAdditionalDataBeanList = new HashSet<PostPaidTransactionHistoryAdditionalDataBean>(0);


    public PostPaidTransactionHistoryBean() {}

    public PostPaidTransactionHistoryBean(PostPaidTransactionBean poPTransactionBean) {

        this.requestID = poPTransactionBean.getRequestID();
        this.source = poPTransactionBean.getSource();
        this.sourceID = poPTransactionBean.getSourceID();
        this.sourceNumber = poPTransactionBean.getSourceNumber();
        this.srcTransactionID = poPTransactionBean.getSrcTransactionID();
        this.mpTransactionID = poPTransactionBean.getMpTransactionID();
        this.bankTansactionID = poPTransactionBean.getBankTansactionID();
        this.authorizationCode = poPTransactionBean.getAuthorizationCode();
        this.token = poPTransactionBean.getToken();
        this.paymentType = poPTransactionBean.getPaymentType();
        this.amount = poPTransactionBean.getAmount();
        this.currency = poPTransactionBean.getCurrency();
        this.productType = poPTransactionBean.getProductType();
        this.toReconcile = poPTransactionBean.getToReconcile();
        this.voucherReconcile = poPTransactionBean.getVoucherReconcile();
        this.loyaltyReconcile = poPTransactionBean.getLoyaltyReconcile();
        this.reconciliationAttemptsLeft = poPTransactionBean.getReconciliationAttemptsLeft();

        if (poPTransactionBean.getCartBean() != null) {
            Set<PostPaidCartBean> cartBeanData = poPTransactionBean.getCartBean();
            for (PostPaidCartBean cartBean : cartBeanData) {
                PostPaidCartHistoryBean postPaidCartHistoryBean = new PostPaidCartHistoryBean(cartBean);
                postPaidCartHistoryBean.setPostPaidTransactionHistoryBean(this);
                this.cartHistoryBean.add(postPaidCartHistoryBean);
            }
        }
        if (poPTransactionBean.getRefuelBean() != null) {
            Set<PostPaidRefuelBean> refuelBeanData = poPTransactionBean.getRefuelBean();
            for (PostPaidRefuelBean refuelBean : refuelBeanData) {
                PostPaidRefuelHistoryBean postPaidRefuelHistoryBean = new PostPaidRefuelHistoryBean(refuelBean);
                postPaidRefuelHistoryBean.setPostPaidTransactionHistoryBean(this);
                this.refuelHistoryBean.add(postPaidRefuelHistoryBean);
            }
        }

        this.statusType = poPTransactionBean.getStatusType();
        this.mpTransactionStatus = poPTransactionBean.getMpTransactionStatus();
        this.srcTransactionStatus = poPTransactionBean.getSrcTransactionStatus();
        this.creationTimestamp = poPTransactionBean.getCreationTimestamp();
        this.lastModifyTimestamp = poPTransactionBean.getLastModifyTimestamp();

        if (poPTransactionBean.getPostPaidTransactionEventBean() != null) {
            Set<PostPaidTransactionEventBean> postPaidTransactionEventBeanData = poPTransactionBean.getPostPaidTransactionEventBean();
            for (PostPaidTransactionEventBean postPaidTransactionEventBean : postPaidTransactionEventBeanData) {
                PostPaidTransactionEventHistoryBean postPaidTransactionEventHistoryBean = new PostPaidTransactionEventHistoryBean(postPaidTransactionEventBean);
                postPaidTransactionEventHistoryBean.setPostPaidTransactionHistoryBean(this);
                this.postPaidTransactionEventHistoryBean.add(postPaidTransactionEventHistoryBean);
            }
        }

        if (poPTransactionBean.getPostPaidTransactionPaymentEventBean() != null) {
            Set<PostPaidTransactionPaymentEventBean> postPaidTransactionPaymentEventBeanData = poPTransactionBean.getPostPaidTransactionPaymentEventBean();
            for (PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean : postPaidTransactionPaymentEventBeanData) {
                PostPaidTransactionPaymentEventHistoryBean postPaidTransactionPaymentEventHistoryBean = new PostPaidTransactionPaymentEventHistoryBean(
                        postPaidTransactionPaymentEventBean);
                postPaidTransactionPaymentEventHistoryBean.setPostPaidTransactionHistoryBean(this);
                this.postPaidTransactionPaymentEventHistoryBean.add(postPaidTransactionPaymentEventHistoryBean);
            }
        }

        this.shopLogin = poPTransactionBean.getShopLogin();
        this.acquirerID = poPTransactionBean.getAcquirerID();
        this.paymentMode = poPTransactionBean.getPaymentMode();
        this.paymentMethodId = poPTransactionBean.getPaymentMethodId();
        this.paymentMethodType = poPTransactionBean.getPaymentMethodType();
        this.notificationCreated = poPTransactionBean.getNotificationCreated();
        this.notificationPaid = poPTransactionBean.getNotificationPaid();
        this.notificationUser = poPTransactionBean.getNotificationUser();
        this.userBean = poPTransactionBean.getUserBean();
        this.stationBean = poPTransactionBean.getStationBean();
        this.useVoucher = poPTransactionBean.getUseVoucher();

        if (poPTransactionBean.getPostPaidConsumeVoucherBeanList() != null) {
            Set<PostPaidConsumeVoucherBean> postPaidConsumeVoucherBeanData = poPTransactionBean.getPostPaidConsumeVoucherBeanList();
            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidConsumeVoucherBeanData) {
                PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean = new PostPaidConsumeVoucherHistoryBean(postPaidConsumeVoucherBean);
                postPaidConsumeVoucherHistoryBean.setPostPaidTransactionHistoryBean(this);
                this.postPaidConsumeVoucherHistoryBeanList.add(postPaidConsumeVoucherHistoryBean);
            }
        }

        if (poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList() != null) {
            Set<PostPaidLoadLoyaltyCreditsBean> postPaidLoadLoyaltyCreditsBeanData = poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList();
            for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : postPaidLoadLoyaltyCreditsBeanData) {
                PostPaidLoadLoyaltyCreditsHistoryBean postPaidLoadLoyaltyCreditsHistoryBean = new PostPaidLoadLoyaltyCreditsHistoryBean(postPaidLoadLoyaltyCreditsBean);
                postPaidLoadLoyaltyCreditsHistoryBean.setPostPaidTransactionHistoryBean(this);
                this.postPaidLoadLoyaltyCreditsHistoryBeanList.add(postPaidLoadLoyaltyCreditsHistoryBean);
            }
        }

        this.groupAcquirer = poPTransactionBean.getGroupAcquirer();
        this.encodedSecretKey = poPTransactionBean.getEncodedSecretKey();
        this.gfgElectronicInvoiceID = poPTransactionBean.getGFGElectronicInvoiceID();
        this.transactionCategory = poPTransactionBean.getTransactionCategory().getValue();
        
        if (poPTransactionBean.getPostPaidTransactionAdditionalDataBeanList() != null) {
            Set<PostPaidTransactionAdditionalDataBean> postPaidTransactionAdditionalDataBeanList = poPTransactionBean.getPostPaidTransactionAdditionalDataBeanList();
            for (PostPaidTransactionAdditionalDataBean postPaidTransactionAdditionalDataBean : postPaidTransactionAdditionalDataBeanList) {
                PostPaidTransactionHistoryAdditionalDataBean postPaidTransactionHistoryAdditionalDataBean = new PostPaidTransactionHistoryAdditionalDataBean(postPaidTransactionAdditionalDataBean);
                postPaidTransactionHistoryAdditionalDataBean.setPostPaidTransactionHistoryBean(this);
                this.postPaidTransactionHistoryAdditionalDataBeanList.add(postPaidTransactionHistoryAdditionalDataBean);
            }
        }
    }

    public PostPaidTransactionHistory toPostPaidTransactionHistory() {

        PostPaidTransactionHistory postPaidTransactionHistory = new PostPaidTransactionHistory();
        postPaidTransactionHistory.setId(this.id);
        postPaidTransactionHistory.setMpTransactionID(this.mpTransactionID);
        
        if ( this.getUserBean() != null ) {
            postPaidTransactionHistory.setUser(this.getUserBean().toUser());
        }
        else {
            postPaidTransactionHistory.setUser(null);
        }
        
        if ( this.getStationBean() != null ) {
            postPaidTransactionHistory.setStation(this.getStationBean().toStation());
        }
        else {
            postPaidTransactionHistory.setStation(null);
        }
        
        postPaidTransactionHistory.setSource(this.source);
        postPaidTransactionHistory.setSourceID(this.sourceID);
        postPaidTransactionHistory.setSourceNumber(this.sourceNumber);
        postPaidTransactionHistory.setSrcTransactionID(this.srcTransactionID);
        postPaidTransactionHistory.setBankTansactionID(this.bankTansactionID);
        postPaidTransactionHistory.setAuthorizationCode(this.authorizationCode);
        postPaidTransactionHistory.setToken(this.token);
        postPaidTransactionHistory.setPaymentType(this.paymentType);
        postPaidTransactionHistory.setAmount(this.amount);
        postPaidTransactionHistory.setCurrency(this.currency);
        postPaidTransactionHistory.setProductType(this.productType);
        postPaidTransactionHistory.setGFGElectronicInvoiceID(this.gfgElectronicInvoiceID);
        
        if (!this.cartHistoryBean.isEmpty()) {

            for (PostPaidCartHistoryBean postPaidCartHistoryBean : this.cartHistoryBean) {
                PostPaidCart postPaidCart = postPaidCartHistoryBean.toPostPaidCart();
                postPaidTransactionHistory.getCartBean().add(postPaidCart);
            }
        }

        if (!this.refuelHistoryBean.isEmpty()) {

            for (PostPaidRefuelHistoryBean postPaidRefuelHistoryBean : this.refuelHistoryBean) {
                PostPaidRefuel postPaidRefuel = postPaidRefuelHistoryBean.toPostPaidRefuel();
                postPaidTransactionHistory.getRefuelBean().add(postPaidRefuel);
            }
        }

        postPaidTransactionHistory.setStatusType(this.statusType);
        postPaidTransactionHistory.setMpTransactionStatus(this.mpTransactionStatus);
        postPaidTransactionHistory.setSrcTransactionStatus(this.srcTransactionStatus);
        postPaidTransactionHistory.setCreationTimestamp(this.creationTimestamp);
        postPaidTransactionHistory.setLastModifyTimestamp(this.lastModifyTimestamp);

        if (!this.postPaidTransactionEventHistoryBean.isEmpty()) {

            for (PostPaidTransactionEventHistoryBean postPaidTransactionEventHistoryBean : this.postPaidTransactionEventHistoryBean) {
                PostPaidTransactionEvent postPaidTransactionEvent = postPaidTransactionEventHistoryBean.toPostPaidTransactionEvent();
                postPaidTransactionHistory.getPostPaidTransactionEventBean().add(postPaidTransactionEvent);
            }
        }

        if (!this.postPaidTransactionPaymentEventHistoryBean.isEmpty()) {

            for (PostPaidTransactionPaymentEventHistoryBean postPaidTransactionPaymentEventHistoryBean : this.postPaidTransactionPaymentEventHistoryBean) {
                PostPaidTransactionPaymentEvent postPaidTransactionPaymentEvent = postPaidTransactionPaymentEventHistoryBean.toPostPaidTransactionPaymentEvent();
                postPaidTransactionHistory.getPostPaidTransactionPaymentEventBean().add(postPaidTransactionPaymentEvent);
            }
        }

        postPaidTransactionHistory.setShopLogin(this.shopLogin);
        postPaidTransactionHistory.setAcquirerID(this.acquirerID);
        postPaidTransactionHistory.setPaymentMode(this.paymentMode);
        postPaidTransactionHistory.setPaymentMethodId(this.paymentMethodId);
        postPaidTransactionHistory.setPaymentMethodType(this.paymentMethodType);
        postPaidTransactionHistory.setNotificationCreated(this.notificationCreated);
        postPaidTransactionHistory.setNotificationPaid(this.notificationPaid);
        postPaidTransactionHistory.setNotificationUser(this.notificationUser);
        postPaidTransactionHistory.setUseVoucher(this.useVoucher);
        postPaidTransactionHistory.setToReconcile(this.toReconcile);

        if (!this.postPaidConsumeVoucherHistoryBeanList.isEmpty()) {

            for (PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean : this.postPaidConsumeVoucherHistoryBeanList) {
                PostPaidConsumeVoucher PostPaidConsumeVoucher = postPaidConsumeVoucherHistoryBean.toPostPaidConsumeVoucher();
                postPaidTransactionHistory.getPostPaidConsumeVoucherBeanList().add(PostPaidConsumeVoucher);
            }
        }

        if (!this.postPaidLoadLoyaltyCreditsHistoryBeanList.isEmpty()) {

            for (PostPaidLoadLoyaltyCreditsHistoryBean postPaidLoadLoyaltyCreditsHistoryBean : this.postPaidLoadLoyaltyCreditsHistoryBeanList) {
                PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits = postPaidLoadLoyaltyCreditsHistoryBean.toPostPaidLoadLoyaltyCredits();
                postPaidTransactionHistory.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCredits);
            }
        }
        
        if (!this.postPaidTransactionHistoryAdditionalDataBeanList.isEmpty()) {

            for (PostPaidTransactionHistoryAdditionalDataBean postPaidTransactionHistoryAdditionalDataBean : this.postPaidTransactionHistoryAdditionalDataBeanList) {
                PostPaidTransactionAdditionalDataBean postPaidTransactionAdditionalDataBean = new PostPaidTransactionAdditionalDataBean(postPaidTransactionHistoryAdditionalDataBean);
                PostPaidTransactionAdditionalData postPaidTransactionAdditionalData = postPaidTransactionAdditionalDataBean.toPostPaidTransactionAdditionalData();
                PostPaidTransactionHistoryAdditionalData postPaidTransactionHistoryAdditionalData = new PostPaidTransactionHistoryAdditionalData(postPaidTransactionAdditionalData);
                postPaidTransactionHistoryAdditionalData.setPostPaidTransactionHistory(postPaidTransactionHistory);
                postPaidTransactionHistory.getPostPaidTransactionAdditionalDataList().add(postPaidTransactionHistoryAdditionalData);
            }
        }

        return postPaidTransactionHistory;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceID() {
        return sourceID;
    }

     public Boolean getToReconcile() {
        return toReconcile;
    }

    public void setToReconcile(Boolean toReconcile) {
        this.toReconcile = toReconcile;
    }

    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public String getSourceNumber() {
        return sourceNumber;
    }

    public void setSourceNumber(String sourceNumber) {
        this.sourceNumber = sourceNumber;
    }

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public String getBankTansactionID() {
        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {
        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Set<PostPaidCartHistoryBean> getCartHistoryBean() {
        return cartHistoryBean;
    }

    public Set<PostPaidTransactionPaymentEventHistoryBean> getPostPaidTransactionPaymentEventHistoryBean() {
        return postPaidTransactionPaymentEventHistoryBean;
    }

    public void setPostPaidTransactionPaymentEventHistoryBean(Set<PostPaidTransactionPaymentEventHistoryBean> postPaidTransactionPaymentEventBean) {
        this.postPaidTransactionPaymentEventHistoryBean = postPaidTransactionPaymentEventBean;
    }

    public void setCartHistoryBean(Set<PostPaidCartHistoryBean> cartHistoryBean) {
        this.cartHistoryBean = cartHistoryBean;
    }

    public Set<PostPaidRefuelHistoryBean> getRefuelHistoryBean() {
        return refuelHistoryBean;
    }

    public void setRefuelHistoryBean(Set<PostPaidRefuelHistoryBean> refuelHistoryBean) {
        this.refuelHistoryBean = refuelHistoryBean;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public String getMpTransactionStatus() {
        return mpTransactionStatus;
    }

    public void setMpTransactionStatus(String mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }

    public String getSrcTransactionStatus() {
        return srcTransactionStatus;
    }

    public void setSrcTransactionStatus(String srcTransactionStatus) {
        this.srcTransactionStatus = srcTransactionStatus;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getLastModifyTimestamp() {
        return lastModifyTimestamp;
    }

    public void setLastModifyTimestamp(Date lastModifyTimestamp) {
        this.lastModifyTimestamp = lastModifyTimestamp;
    }

    public Set<PostPaidTransactionEventHistoryBean> getPostPaidTransactionEventHistoryBean() {
        return postPaidTransactionEventHistoryBean;
    }
    
    public PostPaidTransactionEventHistoryBean getPostPaidTransactionEventHistoryBean(String event, boolean firstMatched) {
        PostPaidTransactionEventHistoryBean poPTransactionEvent = null;

        for (PostPaidTransactionEventHistoryBean poPTransactionEventHistoryBean : getPostPaidTransactionEventHistoryBean()) {
            //System.out.println("sequenceID:" + postPaidTransactionEventBean.getSequenceID());
            if (poPTransactionEventHistoryBean.getEvent().equals(event)) {
                poPTransactionEvent = poPTransactionEventHistoryBean;
                if (firstMatched) {
                    return poPTransactionEvent;
                }
                //System.out.println("sequence selected:" + sequence);
            }
        }

        //System.out.println("poPTransactionEvent sequenceID:" + (poPTransactionEvent != null ? poPTransactionEvent.getSequenceID().intValue() : 0));

        return poPTransactionEvent;
    }

    public void setPostPaidTransactionEventHistoryBean(Set<PostPaidTransactionEventHistoryBean> postPaidTransactionEventBean) {
        this.postPaidTransactionEventHistoryBean = postPaidTransactionEventBean;
    }

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
        this.acquirerID = acquirerID;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public Boolean getNotificationCreated() {
        return notificationCreated;
    }

    public void setNotificationCreated(Boolean notificationCreated) {
        this.notificationCreated = notificationCreated;
    }

    public Boolean getNotificationPaid() {
        return notificationPaid;
    }

    public void setNotificationPaid(Boolean notificationPaid) {
        this.notificationPaid = notificationPaid;
    }

    public Boolean getNotificationUser() {
        return notificationUser;
    }

    public void setNotificationUser(Boolean notificationUser) {
        this.notificationUser = notificationUser;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public StationBean getStationBean() {
        return stationBean;
    }

    public void setStationBean(StationBean stationBean) {
        this.stationBean = stationBean;
    }

    public Date getArchivingDate() {
        return archivingDate;
    }

    public void setArchivingDate(Date archivingDate) {
        this.archivingDate = archivingDate;
    }

    public Boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }

    public Set<PostPaidConsumeVoucherHistoryBean> getPostPaidConsumeVoucherBeanList() {
        return postPaidConsumeVoucherHistoryBeanList;
    }

    public void setPostPaidConsumeVoucherBeanList(Set<PostPaidConsumeVoucherHistoryBean> postPaidConsumeVoucherBeanList) {
        this.postPaidConsumeVoucherHistoryBeanList = postPaidConsumeVoucherBeanList;
    }

    public Set<PostPaidLoadLoyaltyCreditsHistoryBean> getPostPaidLoadLoyaltyCreditsBeanList() {
        return postPaidLoadLoyaltyCreditsHistoryBeanList;
    }

    public void setPostPaidLoadLoyaltyCreditsBeanList(Set<PostPaidLoadLoyaltyCreditsHistoryBean> postPaidLoadLoyaltyCreditsBeanList) {
        this.postPaidLoadLoyaltyCreditsHistoryBeanList = postPaidLoadLoyaltyCreditsBeanList;
    }
    
    
    public PostPaidConsumeVoucherHistoryBean getLastPostPaidConsumeVoucherBean() {

        PostPaidConsumeVoucherHistoryBean lastPostPaidConsumeVoucherHistoryBean = null;

        Long maxRequestTimestamp = 0l;

        for (PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean : this.getPostPaidConsumeVoucherBeanList()) {

            if (postPaidConsumeVoucherHistoryBean.getRequestTimestamp() > maxRequestTimestamp) {

                lastPostPaidConsumeVoucherHistoryBean = postPaidConsumeVoucherHistoryBean;

                maxRequestTimestamp = postPaidConsumeVoucherHistoryBean.getRequestTimestamp();
            }
        }

        return lastPostPaidConsumeVoucherHistoryBean;
    }
    
    
    public PostPaidLoadLoyaltyCreditsHistoryBean getLastPostPaidLoadLoyaltyCreditsBean() {

        PostPaidLoadLoyaltyCreditsHistoryBean lastPostPaidLoadLoyaltyCreditsHistoryBean = null;

        Long maxRequestTimestamp = 0l;

        for (PostPaidLoadLoyaltyCreditsHistoryBean postPaidLoadLoyaltyCreditsHistoryBean : this.getPostPaidLoadLoyaltyCreditsBeanList()) {

            if (postPaidLoadLoyaltyCreditsHistoryBean.getRequestTimestamp() > maxRequestTimestamp) {

                lastPostPaidLoadLoyaltyCreditsHistoryBean = postPaidLoadLoyaltyCreditsHistoryBean;

                maxRequestTimestamp = postPaidLoadLoyaltyCreditsHistoryBean.getRequestTimestamp();
            }
        }

        return lastPostPaidLoadLoyaltyCreditsHistoryBean;
    }
    
    public PostPaidTransactionEventHistoryBean getLastPostPaidTransactionEventHistoryBean() {
        PostPaidTransactionEventHistoryBean poPTransactionEventHistory = null;
        int sequence = 0;

        for (PostPaidTransactionEventHistoryBean postPaidTransactionEventHistoryBean : getPostPaidTransactionEventHistoryBean()) {
            //System.out.println("sequenceID:" + postPaidTransactionEventHistoryBean.getSequenceID());
            if (postPaidTransactionEventHistoryBean.getSequenceID() != null && postPaidTransactionEventHistoryBean.getSequenceID().intValue() > sequence) {
                poPTransactionEventHistory = postPaidTransactionEventHistoryBean;
                sequence = postPaidTransactionEventHistoryBean.getSequenceID().intValue();
                //System.out.println("sequence selected:" + sequence);
            }
        }
        
        //System.out.println("poPTransactionEventHistory sequenceID:" + (poPTransactionEventHistory != null ? poPTransactionEventHistory.getSequenceID().intValue() : 0));
        
        return poPTransactionEventHistory;
    }

    public Boolean getLoyaltyReconcile() {
        return loyaltyReconcile;
    }

    public void setLoyaltyReconcile(Boolean loyaltyReconcile) {
        this.loyaltyReconcile = loyaltyReconcile;
    }

    public Boolean getVoucherReconcile() {
        return voucherReconcile;
    }

    public void setVoucherReconcile(Boolean voucherReconcile) {
        this.voucherReconcile = voucherReconcile;
    }

    public Set<PostPaidConsumeVoucherHistoryBean> getPostPaidConsumeVoucherHistoryBeanList() {
        return postPaidConsumeVoucherHistoryBeanList;
    }

    public void setPostPaidConsumeVoucherHistoryBeanList(Set<PostPaidConsumeVoucherHistoryBean> postPaidConsumeVoucherHistoryBeanList) {
        this.postPaidConsumeVoucherHistoryBeanList = postPaidConsumeVoucherHistoryBeanList;
    }

    public Set<PostPaidLoadLoyaltyCreditsHistoryBean> getPostPaidLoadLoyaltyCreditsHistoryBeanList() {
        return postPaidLoadLoyaltyCreditsHistoryBeanList;
    }

    public void setPostPaidLoadLoyaltyCreditsHistoryBeanList(Set<PostPaidLoadLoyaltyCreditsHistoryBean> postPaidLoadLoyaltyCreditsHistoryBeanList) {
        this.postPaidLoadLoyaltyCreditsHistoryBeanList = postPaidLoadLoyaltyCreditsHistoryBeanList;
    }

    public String getEncodedSecretKey() {
        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {
        this.encodedSecretKey = encodedSecretKey;
    }

    public String getGroupAcquirer() {
        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {
        this.groupAcquirer = groupAcquirer;
    }
    
    public String getGFGElectronicInvoiceID() {
        return gfgElectronicInvoiceID;
    }
   
    public void setGFGElectronicInvoiceID(String gfgElectronicInvoiceID) {
        this.gfgElectronicInvoiceID = gfgElectronicInvoiceID;
    }

    public TransactionCategoryType getTransactionCategory() {
        if (transactionCategory == null) {
            return TransactionCategoryType.TRANSACTION_CUSTOMER;
        }
       
        return TransactionCategoryType.getValueOf(transactionCategory);
    }
   
    public void setTransactionCategory(TransactionCategoryType transactionCategory) {
        this.transactionCategory = transactionCategory.getValue();
    }

    public Set<PostPaidTransactionHistoryAdditionalDataBean> getPostPaidTransactionHistoryAdditionalDataBeanList() {
        return postPaidTransactionHistoryAdditionalDataBeanList;
    }

    public void setPostPaidTransactionHistoryAdditionalDataBeanList(Set<PostPaidTransactionHistoryAdditionalDataBean> postPaidTransactionHistoryAdditionalDataBeanList) {
        this.postPaidTransactionHistoryAdditionalDataBeanList = postPaidTransactionHistoryAdditionalDataBeanList;
    }
   
}
