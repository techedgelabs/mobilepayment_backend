package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "REFUELING_MAPPING_ERROR")
@NamedQueries({ @NamedQuery(name = "RefuelingMappingErrorBean.getErrorByErrorCode", query = "select d from RefuelingMappingErrorBean d where d.errorCode = :errorCode"),
        @NamedQuery(name = "RefuelingMappingErrorBean.getErrorByID", query = "select d from RefuelingMappingErrorBean d where d.id = :id"),
        @NamedQuery(name = "RefuelingMappingErrorBean.findAll", query = "select d from RefuelingMappingErrorBean d") })
public class RefuelingMappingErrorBean {

    public static final String FIND_BY_ERRORCODE = "RefuelingMappingErrorBean.getErrorByErrorCode";
    public static final String FIND_BY_ID        = "RefuelingMappingErrorBean.getErrorByID";
    public static final String FIND_ALL          = "RefuelingMappingErrorBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "errorCode", nullable = false, length = 40)
    private String             errorCode;

    @Column(name = "statusCode", nullable = false)
    private String             statusCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}