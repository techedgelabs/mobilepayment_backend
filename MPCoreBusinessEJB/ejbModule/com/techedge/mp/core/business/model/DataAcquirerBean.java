package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.DataAcquirer;

@Entity
@Table(name = "DATA_ACQUIRER")
public class DataAcquirerBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long   id;

    @Column(name = "acquirer_id", nullable = true)
    private String acquirerID;

    @Column(name = "apikey", nullable = true)
    private String apiKey;

    @Column(name = "encoded_secretkey", nullable = true)
    private String encodedSecretKey;

    @Column(name = "currency", nullable = true)
    private String currency;

    @Column(name = "group_acquirer", nullable = true)
    private String groupAcquirer;

    public DataAcquirerBean() {}

    public DataAcquirerBean(DataAcquirer dataAcquire) {

        this.apiKey = dataAcquire.getApiKey();
        this.acquirerID = dataAcquire.getAcquirerID();
        this.currency = dataAcquire.getCurrency();
        this.encodedSecretKey = dataAcquire.getEncodedSecretKey();
        this.groupAcquirer = dataAcquire.getGroupAcquirer();
    }

    public DataAcquirer toDataAcquired() {

        DataAcquirer dataAcquire = new DataAcquirer();
        dataAcquire.setId(this.id);
        dataAcquire.setApiKey(this.apiKey);
        dataAcquire.setAcquirerID(this.acquirerID);
        dataAcquire.setCurrency(this.currency);
        dataAcquire.setEncodedSecretKey(this.encodedSecretKey);
        dataAcquire.setGroupAcquirer(this.groupAcquirer);

        return dataAcquire;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String aquiredID) {
        this.acquirerID = aquiredID;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getEncodedSecretKey() {
        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {
        this.encodedSecretKey = encodedSecretKey;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getGroupAcquirer() {
        return groupAcquirer;
    }

    public void setGroupAcquirer(String group) {
        this.groupAcquirer = group;
    }

}
