package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.TransactionAdditionalData;

@Entity
@Table(name = "TRANSACTION_ADDITIONAL_DATA")
/*@NamedQueries({
    @NamedQuery(name = "TransactionEventBean.findTransactionEventByTransaction", query = "select t from TransactionEventBean t where t.transactionBean = :transactionBean"),
    @NamedQuery(name = "TransactionEventBean.findTransactionEventById", query = "select t from TransactionEventBean t where t.id = :id")
})*/
public class TransactionAdditionalDataBean {

    //public static final String FIND_BY_TRANSACTION = "TransactionEventBean.findTransactionEventByTransaction";
    //public static final String FIND_BY_ID = "TransactionEventBean.findTransactionEventById";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    private TransactionBean    transactionBean;

    @Column(name = "data_key", nullable = true)
    private String             dataKey;

    @Column(name = "data_value", nullable = true)
    private String             dataValue;

    public TransactionAdditionalDataBean() {}

    public TransactionAdditionalDataBean(TransactionAdditionalData transactionAdditionalData) {

        this.id = transactionAdditionalData.getId();
        this.dataKey = transactionAdditionalData.getDataKey();
        this.dataValue = transactionAdditionalData.getDataValue();
    }

    public TransactionAdditionalData toTransactionAdditionalData() {

        TransactionAdditionalData transactionAdditionalData = new TransactionAdditionalData();
        transactionAdditionalData.setId(this.id);
        transactionAdditionalData.setDataKey(this.dataKey);
        transactionAdditionalData.setDataValue(this.dataValue);
        return transactionAdditionalData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TransactionBean getTransactionBean() {
        return transactionBean;
    }

    public void setTransactionBean(TransactionBean transactionBean) {
        this.transactionBean = transactionBean;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }

}
