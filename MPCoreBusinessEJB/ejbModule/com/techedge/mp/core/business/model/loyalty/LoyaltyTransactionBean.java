package com.techedge.mp.core.business.model.loyalty;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "LOYALTY_TRANSACTION")
@NamedQueries({ @NamedQuery(name = "LoyaltyTransactionBean.findByEventNotification", query = "select t from LoyaltyTransactionBean t "
        + "where t.eventNotificationBean = :eventNotificationBean") })
public class LoyaltyTransactionBean {

    public static final String                FIND_BY_EVENT_NOTIFICATION = "LoyaltyTransactionBean.findByEventNotification";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                              id;

    @Column(name = "payment_mode")
    private String                            paymentMode;

    @OneToOne()
    @JoinColumn(name = "event_notification_id", nullable = false)
    private EventNotificationBean             eventNotificationBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "loyaltyTransactionBean")
    private Set<LoyaltyTransactionRefuelBean> refuelDetails              = new HashSet<LoyaltyTransactionRefuelBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "loyaltyTransactionBean")
    private Set<LoyaltyTransactionNonOilBean> nonOilDetails              = new HashSet<LoyaltyTransactionNonOilBean>(0);

    public LoyaltyTransactionBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public EventNotificationBean getEventNotificationBean() {
        return eventNotificationBean;
    }

    public void setEventNotificationBean(EventNotificationBean eventNotificationBean) {
        this.eventNotificationBean = eventNotificationBean;
    }

    public Set<LoyaltyTransactionRefuelBean> getRefuelDetails() {
        return refuelDetails;
    }

    public void setRefuelDetails(Set<LoyaltyTransactionRefuelBean> loyaltyTransactionRefuelDetails) {
        this.refuelDetails = loyaltyTransactionRefuelDetails;
    }

    public Set<LoyaltyTransactionNonOilBean> getNonOilDetails() {
        return nonOilDetails;
    }

    public void setNonOilDetails(Set<LoyaltyTransactionNonOilBean> loyaltyTransactionNonOilDetails) {
        this.nonOilDetails = loyaltyTransactionNonOilDetails;
    }
}
