package com.techedge.mp.core.business.model.crmsf;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.model.UserBean;

@Entity
@Table(name = "CRM_SF_CONTACT_KEY")
@NamedQueries({ @NamedQuery(name = "CRMContactKeyBean.SelectAll", query = "select t from CRMSfContactKeyBean t"), 
				@NamedQuery(name = "CRMContactKeyBean.SelectByUser", query = "select t from CRMSfContactKeyBean t where userBean.id=:userId") })
public class CRMSfContactKeyBean implements Serializable {

	private static final long serialVersionUID = 487505038069695604L;

	public static final String SELECT_ALL = "CRMContactKeyBean.SelectAll";
	public static final String SELECT_BY_USER = "CRMContactKeyBean.SelectByUser";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "contact_key", nullable = false)
	private String contactKey;

	@Column(name = "creation_date", nullable = false)
	private Date creationDate;

	@Column(name = "request_id", nullable = false)
	private String requestId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserBean userBean;

	public CRMSfContactKeyBean() {
	}

	public CRMSfContactKeyBean(String contactKey, Date creationDate,
			String requestId, UserBean userBean) {
		super();
		this.contactKey = contactKey;
		this.creationDate = creationDate;
		this.requestId = requestId;
		this.userBean = userBean;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContactKey() {
		return contactKey;
	}

	public void setContactKey(String contactKey) {
		this.contactKey = contactKey;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public UserBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

}
