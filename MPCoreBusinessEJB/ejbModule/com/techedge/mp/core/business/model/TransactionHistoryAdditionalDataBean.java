package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.TransactionAdditionalData;
import com.techedge.mp.core.business.interfaces.TransactionHistoryAdditionalData;

@Entity
@Table(name = "TRANSACTION_HISTORY_ADDITIONAL_DATA")
/*@NamedQueries({
    @NamedQuery(name = "TransactionEventBean.findTransactionEventByTransaction", query = "select t from TransactionEventBean t where t.transactionBean = :transactionBean"),
    @NamedQuery(name = "TransactionEventBean.findTransactionEventById", query = "select t from TransactionEventBean t where t.id = :id")
})*/
public class TransactionHistoryAdditionalDataBean {

    //public static final String FIND_BY_TRANSACTION = "TransactionEventBean.findTransactionEventByTransaction";
    //public static final String FIND_BY_ID = "TransactionEventBean.findTransactionEventById";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    private TransactionHistoryBean    transactionBean;

    @Column(name = "data_key", nullable = true)
    private String             dataKey;

    @Column(name = "data_value", nullable = true)
    private String             dataValue;

    public TransactionHistoryAdditionalDataBean() {}

    public TransactionHistoryAdditionalDataBean(TransactionAdditionalData transactionAdditionalData) {

        this.id = transactionAdditionalData.getId();
        this.dataKey = transactionAdditionalData.getDataKey();
        this.dataValue = transactionAdditionalData.getDataValue();
    }
    
    public TransactionHistoryAdditionalDataBean(TransactionAdditionalDataBean transactionAdditionalDataBean) {
        
        this.id = transactionAdditionalDataBean.getId();
        this.dataKey = transactionAdditionalDataBean.getDataKey();
        this.dataValue = transactionAdditionalDataBean.getDataValue();
    }
    
    public TransactionHistoryAdditionalDataBean(TransactionHistoryAdditionalData transactionHistoryAdditionalData) {

        this.id = transactionHistoryAdditionalData.getId();
        this.dataKey = transactionHistoryAdditionalData.getDataKey();
        this.dataValue = transactionHistoryAdditionalData.getDataValue();
    }

    public TransactionAdditionalData toTransactionAdditionalData() {

        TransactionAdditionalData transactionAdditionalData = new TransactionAdditionalData();
        transactionAdditionalData.setId(this.id);
        transactionAdditionalData.setDataKey(this.dataKey);
        transactionAdditionalData.setDataValue(this.dataValue);
        return transactionAdditionalData;
    }
    
    public TransactionHistoryAdditionalData toTransactionHistoryAdditionalData() {

        TransactionHistoryAdditionalData transactionHistoryAdditionalData = new TransactionHistoryAdditionalData();
        transactionHistoryAdditionalData.setId(this.id);
        transactionHistoryAdditionalData.setDataKey(this.dataKey);
        transactionHistoryAdditionalData.setDataValue(this.dataValue);
        return transactionHistoryAdditionalData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TransactionHistoryBean getTransactionBean() {
        return transactionBean;
    }

    public void setTransactionBean(TransactionHistoryBean transactionBean) {
        this.transactionBean = transactionBean;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }

}
