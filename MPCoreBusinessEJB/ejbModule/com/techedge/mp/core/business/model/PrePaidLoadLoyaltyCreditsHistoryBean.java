package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PrePaidLoadLoyaltyCredits;

@Entity
@Table(name = "PREPAIDLOADLOYALTYCREDITSHISTORY")
/*@NamedQueries({
    @NamedQuery(name = "PrePaidLoadLoyaltyCreditsBean.findPrePaidLoadLoyaltyCreditsBeanByStatusCode", query = "select l from PrePaidLoadLoyaltyCreditsBean l where l.statusCode = :statusCode ")
})*/
public class PrePaidLoadLoyaltyCreditsHistoryBean {
    
    //public static final String FIND_BY_STATUS_CODE = "PrePrLoadLoyaltyCreditsBean.findPrePaidLoadLoyaltyCreditsBeanByStatusCode";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                    id;

    @Column(name = "cs_transaction_id", nullable = true)
    private String                  csTransactionID;

    @Column(name = "operation_id", nullable = true)
    private String                  operationID;

    @Column(name = "operation_id_reversed", nullable = true)
    private String                                operationIDReversed;
    
    @Column(name = "operation_type", nullable = true)
    private String                  operationType;

    @Column(name = "request_timestamp", nullable = true)
    private Long                    requestTimestamp;

    @Column(name = "marketing_msg", nullable = true, length = 500)
    private String                  marketingMsg;

    @Column(name = "warning_msg", nullable = true)
    private String                                warningMsg;

    @Column(name = "message_code", nullable = true)
    private String                  messageCode;

    @Column(name = "status_code", nullable = true)
    private String                  statusCode;

    @Column(name = "credits", nullable = true)
    private Integer                 credits;

    @Column(name = "balance", nullable = true)
    private Integer                 balance;

    @Column(name = "balance_amount", nullable = true)
    private Double                  balanceAmount;

    @Column(name = "card_code_issuer", nullable = true)
    private String                  cardCodeIssuer;

    @Column(name = "ean_code", nullable = true)
    private String                  eanCode;

    @Column(name = "card_status", nullable = true)
    private String                  cardStatus;

    @Column(name = "card_type", nullable = true)
    private String                  cardType;

    @Column(name = "card_classification", nullable = true)
    private String                  cardClassification;

    @Column(name = "reconciled", nullable = true)
    private Boolean                         reconciled = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    private TransactionHistoryBean                      transactionHistoryBean;
    

    public PrePaidLoadLoyaltyCreditsHistoryBean() {}
    
    public PrePaidLoadLoyaltyCreditsHistoryBean(PrePaidLoadLoyaltyCreditsBean prePaidLoadLoyaltyCreditsBean) {

        this.id = prePaidLoadLoyaltyCreditsBean.getId();
        this.csTransactionID = prePaidLoadLoyaltyCreditsBean.getCsTransactionID();
        this.operationID = prePaidLoadLoyaltyCreditsBean.getOperationID();
        this.operationIDReversed = prePaidLoadLoyaltyCreditsBean.getOperationIDReversed();
        this.operationType = prePaidLoadLoyaltyCreditsBean.getOperationType();
        this.requestTimestamp = prePaidLoadLoyaltyCreditsBean.getRequestTimestamp();
        this.marketingMsg = prePaidLoadLoyaltyCreditsBean.getMarketingMsg();
        this.warningMsg = prePaidLoadLoyaltyCreditsBean.getWarningMsg();
        this.messageCode = prePaidLoadLoyaltyCreditsBean.getMessageCode();
        this.statusCode = prePaidLoadLoyaltyCreditsBean.getStatusCode();
        this.credits = prePaidLoadLoyaltyCreditsBean.getCredits();
        this.balance = prePaidLoadLoyaltyCreditsBean.getBalance();
        this.balanceAmount = prePaidLoadLoyaltyCreditsBean.getBalanceAmount();
        this.cardCodeIssuer = prePaidLoadLoyaltyCreditsBean.getCardCodeIssuer();
        this.eanCode = prePaidLoadLoyaltyCreditsBean.getEanCode();
        this.cardStatus = prePaidLoadLoyaltyCreditsBean.getCardStatus();
        this.cardType = prePaidLoadLoyaltyCreditsBean.getCardType();
        this.cardClassification = prePaidLoadLoyaltyCreditsBean.getCardClassification();
        this.reconciled = prePaidLoadLoyaltyCreditsBean.getReconciled();

    }

    public PrePaidLoadLoyaltyCreditsHistoryBean(PrePaidLoadLoyaltyCredits prePaidLoadLoyaltyCredits) {

        this.id = prePaidLoadLoyaltyCredits.getId();
        this.csTransactionID = prePaidLoadLoyaltyCredits.getCsTransactionID();
        this.operationID = prePaidLoadLoyaltyCredits.getOperationID();
        this.operationIDReversed = prePaidLoadLoyaltyCredits.getOperationIDReversed();
        this.operationType = prePaidLoadLoyaltyCredits.getOperationType();
        this.requestTimestamp = prePaidLoadLoyaltyCredits.getRequestTimestamp();
        this.marketingMsg = prePaidLoadLoyaltyCredits.getMarketingMsg();
        this.warningMsg = prePaidLoadLoyaltyCredits.getWarningMsg();
        this.messageCode = prePaidLoadLoyaltyCredits.getMessageCode();
        this.statusCode = prePaidLoadLoyaltyCredits.getStatusCode();
        this.credits = prePaidLoadLoyaltyCredits.getCredits();
        this.balance = prePaidLoadLoyaltyCredits.getBalance();
        this.balanceAmount = prePaidLoadLoyaltyCredits.getBalanceAmount();
        this.cardCodeIssuer = prePaidLoadLoyaltyCredits.getCardCodeIssuer();
        this.eanCode = prePaidLoadLoyaltyCredits.getEanCode();
        this.cardStatus = prePaidLoadLoyaltyCredits.getCardStatus();
        this.cardType = prePaidLoadLoyaltyCredits.getCardType();
        this.cardClassification = prePaidLoadLoyaltyCredits.getCardClassification();
        this.reconciled = prePaidLoadLoyaltyCredits.getReconciled();

    }

    /*
    public PrePaidLoadLoyaltyCreditsBean(PrePaidLoadLoyaltyCreditsHistoryBean postPaidLoadLoyaltyCreditsBean) {

        this.csTransactionID = postPaidLoadLoyaltyCreditsBean.getCsTransactionID();
        this.operationID = postPaidLoadLoyaltyCreditsBean.getOperationID();
        this.operationIDReversed = postPaidLoadLoyaltyCreditsBean.getOperationIDReversed();
        this.operationType = postPaidLoadLoyaltyCreditsBean.getOperationType();
        this.requestTimestamp = postPaidLoadLoyaltyCreditsBean.getRequestTimestamp();
        this.marketingMsg = postPaidLoadLoyaltyCreditsBean.getMarketingMsg();
        this.warningMsg = postPaidLoadLoyaltyCreditsBean.getWarningMsg();
        this.messageCode = postPaidLoadLoyaltyCreditsBean.getMessageCode();
        this.statusCode = postPaidLoadLoyaltyCreditsBean.getStatusCode();
        this.credits = postPaidLoadLoyaltyCreditsBean.getCredits();
        this.balance = postPaidLoadLoyaltyCreditsBean.getBalance();
        this.balanceAmount = postPaidLoadLoyaltyCreditsBean.getBalanceAmount();
        this.cardCodeIssuer = postPaidLoadLoyaltyCreditsBean.getCardCodeIssuer();
        this.eanCode = postPaidLoadLoyaltyCreditsBean.getEanCode();
        this.cardStatus = postPaidLoadLoyaltyCreditsBean.getCardStatus();
        this.cardType = postPaidLoadLoyaltyCreditsBean.getCardType();
        this.cardClassification = postPaidLoadLoyaltyCreditsBean.getCardClassification();
        //this.reconciled = postPaidLoadLoyaltyCreditsBean.getReconciled();
    }
    */
    
    public PrePaidLoadLoyaltyCredits toPrePaidLoadLoyaltyCredits() {

        PrePaidLoadLoyaltyCredits prePaidLoadLoyaltyCredits = new PrePaidLoadLoyaltyCredits();
        prePaidLoadLoyaltyCredits.setId(this.id);
        prePaidLoadLoyaltyCredits.setCsTransactionID(this.csTransactionID);
        prePaidLoadLoyaltyCredits.setOperationID(this.operationID);
        prePaidLoadLoyaltyCredits.setOperationIDReversed(this.operationIDReversed);
        prePaidLoadLoyaltyCredits.setOperationType(this.operationType);
        prePaidLoadLoyaltyCredits.setRequestTimestamp(this.requestTimestamp);
        prePaidLoadLoyaltyCredits.setMarketingMsg(this.marketingMsg);
        prePaidLoadLoyaltyCredits.setWarningMsg(this.warningMsg);
        prePaidLoadLoyaltyCredits.setMessageCode(this.messageCode);
        prePaidLoadLoyaltyCredits.setStatusCode(this.statusCode);
        prePaidLoadLoyaltyCredits.setCredits(this.credits);
        prePaidLoadLoyaltyCredits.setBalance(this.balance);
        prePaidLoadLoyaltyCredits.setBalanceAmount(this.balanceAmount);
        prePaidLoadLoyaltyCredits.setCardCodeIssuer(this.cardCodeIssuer);
        prePaidLoadLoyaltyCredits.setEanCode(this.eanCode);
        prePaidLoadLoyaltyCredits.setCardStatus(this.cardStatus);
        prePaidLoadLoyaltyCredits.setCardType(this.cardType);
        prePaidLoadLoyaltyCredits.setCardClassification(this.cardClassification);
        prePaidLoadLoyaltyCredits.setReconciled(this.reconciled);
        return prePaidLoadLoyaltyCredits;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public void setOperationIDReversed(String operationIDReversed) {
        this.operationIDReversed = operationIDReversed;
    }

    public String getOperationIDReversed() {
        return operationIDReversed;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getCardCodeIssuer() {
        return cardCodeIssuer;
    }

    public void setCardCodeIssuer(String cardCodeIssuer) {
        this.cardCodeIssuer = cardCodeIssuer;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardClassification() {
        return cardClassification;
    }

    public void setCardClassification(String cardClassification) {
        this.cardClassification = cardClassification;
    }

    public Boolean getReconciled() {
        return reconciled;
    }

    public void setReconciled(Boolean reconciled) {
        this.reconciled = reconciled;
    }

    public TransactionHistoryBean getTransactionHistoryBean() {
        return transactionHistoryBean;
    }

    public void setTransactionHistoryBean(TransactionHistoryBean transactionHistoryBean) {
        this.transactionHistoryBean = transactionHistoryBean;
    }

}
