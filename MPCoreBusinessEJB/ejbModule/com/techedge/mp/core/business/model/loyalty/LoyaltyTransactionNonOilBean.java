package com.techedge.mp.core.business.model.loyalty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "LOYALTY_TRANSACTION_NONOIL")
@NamedQueries({ @NamedQuery(name = "LoyaltyTransactionNonOilBean.findByLoyaltyTransaction", query = "select r from LoyaltyTransactionNonOilBean r "
        + "where r.loyaltyTransactionBean = :loyaltyTransactionBean") })

public class LoyaltyTransactionNonOilBean {

    public static final String     FIND_BY_LOYALTY_TRANSACTION = "LoyaltyTransactionNonOilBean.findByLoyaltyTransaction";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                   id;

    @Column(name = "productid", nullable = false)
    private String                 productID;

    @Column(name = "product_description")
    private String                 productDescription;

    @Column(name = "amount", nullable = false)
    private Double                 amount;

    @Column(name = "credits", nullable = false)
    private Integer                credits;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loyalty_transaction_id", nullable = false)
    private LoyaltyTransactionBean loyaltyTransactionBean;

    public LoyaltyTransactionNonOilBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public LoyaltyTransactionBean getLoyaltyTransactionBean() {
        return loyaltyTransactionBean;
    }

    public void setLoyaltyTransactionBean(LoyaltyTransactionBean loyaltyTransactionBean) {
        this.loyaltyTransactionBean = loyaltyTransactionBean;
    }

}
