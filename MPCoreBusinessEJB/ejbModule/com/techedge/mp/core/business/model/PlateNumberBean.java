package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PlateNumber;
import com.techedge.mp.core.business.interfaces.UserSocialData;

@Entity
@Table(name = "PLATE_NUMBER")
//@NamedQueries({ @NamedQuery(name = "UserSocialDataBean.findSocialUserByUUIDandStatus", query = "select usdb from UserSocialDataBean usdb where usdb.uuid = :uuid and usdb.provider = :provider and usdb.userBean.userStatus != :userStatus") })
public class PlateNumberBean {

    public static final String FIND_SOCIAL_USER_BY_UUID_AND_STATUS = "UserSocialDataBean.findSocialUserByUUIDandStatus";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "plate_number", nullable = false)
    private String             plateNumber;

    @Column(name = "description", nullable = false)
    private String             description;
    
    @Column(name = "creation_timestamp", nullable = false)
    private Date               creationTimestamp;
    
    @Column(name = "default_plate_number", nullable = false)
    private Boolean            defaultPlateNumber;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "user_id")
    private UserBean           userBean;

    public PlateNumberBean() {}

    public PlateNumberBean(PlateNumber plateNumber) {
        this.id = plateNumber.getId();
        this.plateNumber = plateNumber.getPlateNumber();
        this.description = plateNumber.getDescription();
        this.creationTimestamp = plateNumber.getCreationTimestamp();
        this.defaultPlateNumber = plateNumber.getDefaultPlateNumber();
    }
    
    public PlateNumber toPlateNumber() {
        PlateNumber plateNumber = new PlateNumber();
        plateNumber.setId(this.id);
        plateNumber.setPlateNumber(this.plateNumber);
        plateNumber.setDescription(this.description);
        plateNumber.setCreationTimestamp(this.creationTimestamp);
        plateNumber.setDefaultPlateNumber(this.defaultPlateNumber);
        return plateNumber;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Boolean getDefaultPlateNumber() {
        return defaultPlateNumber;
    }

    public void setDefaultPlateNumber(Boolean defaultPlateNumber) {
        this.defaultPlateNumber = defaultPlateNumber;
    }

}
