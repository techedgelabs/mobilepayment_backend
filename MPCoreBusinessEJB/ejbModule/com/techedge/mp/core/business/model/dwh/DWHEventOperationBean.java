package com.techedge.mp.core.business.model.dwh;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "DWH_EVENT_OPERATION")
@NamedQueries({ @NamedQuery(name = "DWHEventOperationBean.findById", query = "select upo from DWHEventOperationBean upo where upo.id = :id") })
public class DWHEventOperationBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                           id;
    
    @Column(name = "request_timestamp")
    private Date                           requestTimestamp;

    @Column(name = "request_id")
    private String                         requestId;

    @Column(name = "status_code")
    private String                         statusCode;

    @Column(name = "message_code")
    private String                         messageCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id", nullable = false)
    private DWHEventBean dwhEventBean;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }
    
    public DWHEventBean getDWHEventBean() {
        return dwhEventBean;
    }

    public void setDWHEventBean(DWHEventBean dwEventBean) {
        this.dwhEventBean = dwEventBean;
    }

}
