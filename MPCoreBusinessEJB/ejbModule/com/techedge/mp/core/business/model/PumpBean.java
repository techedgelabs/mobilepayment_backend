package com.techedge.mp.core.business.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.Pump;


@Entity
@Table(name="PUMPS")
@NamedQuery(name="PumpBean.findPumpByID", query="select t from PumpBean t where t.pumpID = :pumpID")
public class PumpBean {

	public static final String FIND_BY_ID = "PumpBean.findPumpByID";
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	
	@Column(name="pump_id", nullable=true)
	private String pumpID;
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
	@JoinColumn(name="station_id", nullable=true)
	private StationBean stationBean;
	
	@Column(name="device_id", nullable=true)
	private String deviceID;
	
	
	public PumpBean() {
	}
	
	public PumpBean(Pump pump) {
		
		this.id = pump.getId();
		this.pumpID = pump.getPumpID();
		this.stationBean = new StationBean(pump.getStation());
		this.deviceID = pump.getDeviceID();
	}
	
	public Pump toPump() {
		
		Pump pump = new Pump();
		pump.setId(this.id);
		pump.setPumpID(this.pumpID);
		pump.setStation(this.stationBean.toStation());
		pump.setDeviceID(this.deviceID);
		return pump;
	}
	
	
	public final String getPumpID() {
		return pumpID;
	}
	public final void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}


	public StationBean getStationBean() {
		return stationBean;
	}

	public void setStationBean(StationBean stationBean) {
		this.stationBean = stationBean;
	}

	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
}
