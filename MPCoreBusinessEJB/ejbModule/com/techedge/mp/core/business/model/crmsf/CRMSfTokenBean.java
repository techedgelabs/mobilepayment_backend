package com.techedge.mp.core.business.model.crmsf;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_SF_TOKEN")
@NamedQueries({ @NamedQuery(name = "CRMSfTokenBean.SelectToken", query = "select t from CRMSfTokenBean t") })
public class CRMSfTokenBean {

	public static final String SELECT_TOKEN = "CRMSfTokenBean.SelectToken";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "token", nullable = false)
	private String token;

	public CRMSfTokenBean() {
	}

	public CRMSfTokenBean(String token) {
		this.token = token;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
