package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.utilities.EncryptionAlgorithmType;

@Entity
@Table(name = "EVENT_CAMPAIGN")
@NamedQueries({ @NamedQuery(name = "EventCampaignBean.findEventByPromoCode", query = "select ecb from EventCampaignBean ecb where ecb.promoCode = :promoCode") })
public class EventCampaignBean {

    public static final String FIND_BY_PROMOCODE = "EventCampaignBean.findEventByPromoCode";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "code", nullable = false, length = 40)
    private String             code;

    @Column(name = "description", nullable = false)
    private String             description;

    @Column(name = "startdata", nullable = false)
    private Date               startData;

    @Column(name = "enddata", nullable = false)
    private Date               endData;

    @Column(name = "decode_secretkey", nullable = true)
    private String             decodeSecretKey;

    @Column(name = "decode_algorithm", nullable = true)
    private String             decodeAlgorithm;

    @Column(name = "voucherAmount", nullable = true)
    private Double             totalAmount;

    @Column(name = "promo_code", nullable = true)
    private String             promoCode;

    public EventCampaignBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartData() {
        return startData;
    }

    public void setStartData(Date startData) {
        this.startData = startData;
    }

    public Date getEndData() {
        return endData;
    }

    public void setEndData(Date endData) {
        this.endData = endData;
    }

    public String getDecodeSecretKey() {
        return decodeSecretKey;
    }

    public void setDecodeSecretKey(String decodeSecretKey) {
        this.decodeSecretKey = decodeSecretKey;
    }

    public EncryptionAlgorithmType getDecodeAlgorithm() {
        return EncryptionAlgorithmType.valueOf(decodeAlgorithm);
    }

    public void setDecodeAlgorithm(EncryptionAlgorithmType decodeAlgorithm) {
        this.decodeAlgorithm = decodeAlgorithm.name();
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setDecodeAlgorithm(String decodeAlgorithm) {
        this.decodeAlgorithm = decodeAlgorithm;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

}
