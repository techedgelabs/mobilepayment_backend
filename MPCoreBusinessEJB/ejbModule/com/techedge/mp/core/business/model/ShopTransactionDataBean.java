package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="SHOPTRANSACTIONDATA")
@NamedQueries({
    @NamedQuery(name="ShopTransactionDataBean.findShopTransactionData",
    		    query="select s from ShopTransactionDataBean s where s.transactionId = :transactionId and s.status =:status"),
    @NamedQuery(name="ShopTransactionDataBean.findShopTransactionDataByPaymentInfo",
                query="select s from ShopTransactionDataBean s where s.paymentInfo = :paymentInfo"),
                @NamedQuery(name="ShopTransactionDataBean.findShopTransactionDataByID",
                query="select s from ShopTransactionDataBean s where s.transactionId = :transactionId"),
}) 
public class ShopTransactionDataBean {

	public static final String FIND_BY_TRANSACTIONIDANDSTATUS = "ShopTransactionDataBean.findShopTransactionData";
	public static final String FIND_BY_PAYMENTINFO = "ShopTransactionDataBean.findShopTransactionDataByPaymentInfo";
	   public static final String FIND_BY_TRANSACTIONID = "ShopTransactionDataBean.findShopTransactionDataByID";

	public static final Integer STATUS_NEW       = 1;
	public static final Integer STATUS_USED      = 2;
	public static final Integer STATUS_TIMEOUT   = 3;
	public static final Integer STATUS_CANCELLED = 4;
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="transactionid", nullable=false, length=40)
	private String transactionId;
	
	@Column(name="creation_timestamp", nullable=false)
	private Date creationTimestamp;

	@Column(name="lastused_timestamp", nullable=false)
	private Date lastUsedTimestamp;
	
	@Column(name="status", nullable=false)
	private Integer status;
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
	@JoinColumn(name="paymentinfo_id")
	private PaymentInfoBean paymentInfo;

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUsedTimestamp() {
		return lastUsedTimestamp;
	}

	public void setLastUsedTimestamp(Date lastUsedTimestamp) {
		this.lastUsedTimestamp = lastUsedTimestamp;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public PaymentInfoBean getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfoBean paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public ShopTransactionDataBean() {}
	
	public static ShopTransactionDataBean createNewShopTransactionDataBean(String shopTransactionId, PaymentInfoBean paymentInfoBean) {
		
		ShopTransactionDataBean shopTransactionDataBean = new ShopTransactionDataBean();
		
		Date now = new Date();
		
		shopTransactionDataBean.creationTimestamp = now;
		shopTransactionDataBean.lastUsedTimestamp = now;
		shopTransactionDataBean.status            = ShopTransactionDataBean.STATUS_NEW;
		shopTransactionDataBean.transactionId     = shopTransactionId;
		shopTransactionDataBean.paymentInfo       = paymentInfoBean;
		
		return shopTransactionDataBean;
	}
	
	public void setStatusToCanceled() {
		
		this.status = ShopTransactionDataBean.STATUS_CANCELLED;
	}
	
	public void setStatusToTimeout() {
		
		this.status = ShopTransactionDataBean.STATUS_TIMEOUT;
	}
	
	public void setStatusToUsed() {
		
		this.status = ShopTransactionDataBean.STATUS_USED;
	}
}
