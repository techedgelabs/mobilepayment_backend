package com.techedge.mp.core.business.model.crm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.crm.Offer;
import com.techedge.mp.core.business.interfaces.crm.OfferList;
import com.techedge.mp.core.business.interfaces.crm.Response;

@Entity
@Table(name = "CRM_OFFER")
public class CRMOfferBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long         id;

    @Column(name = "offer_name")
    private String       offerName;

    @Column(name = "offer_code")
    private String       offerCode;

    @Column(name = "description")
    private String       description;

    @Column(name = "treatment_code")
    private String       treatmentCode;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "event_id", nullable = false)
    private CRMEventBean eventBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "crmOfferBean", cascade = CascadeType.ALL)
    private Set<CRMOfferParametersBean> crmParametersBeanList = new HashSet<CRMOfferParametersBean>(0);
    
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "offer_voucher_promotional_id")
    private CRMOfferVoucherPromotionalBean      crmOfferVoucherPromotionalBean;
    
    public CRMOfferBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getOfferCode() {
        return offerCode;
    }

    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTreatmentCode() {
        return treatmentCode;
    }

    public void setTreatmentCode(String treatmentCode) {
        this.treatmentCode = treatmentCode;
    }

    public CRMEventBean getEventBean() {
        return eventBean;
    }

    public void setEventBean(CRMEventBean eventBean) {
        this.eventBean = eventBean;
    }

    public Set<CRMOfferParametersBean> getCRMParametersBeanList() {
        return crmParametersBeanList;
    }

    public void setCRMParametersBeanList(Set<CRMOfferParametersBean> parametersBeanList) {
        this.crmParametersBeanList = parametersBeanList;
    }

    public CRMOfferVoucherPromotionalBean getCrmOfferVoucherPromotionalBean() {
        return crmOfferVoucherPromotionalBean;
    }

    public void setCrmOfferVoucherPromotionalBean(CRMOfferVoucherPromotionalBean crmOfferVoucherPromotionalBean) {
        this.crmOfferVoucherPromotionalBean = crmOfferVoucherPromotionalBean;
    }

    
    public static List<CRMOfferBean> createOffers(CRMEventBean crmEventBean, Response response, boolean useAllOfferLists) {

        List<CRMOfferBean> crmOffersList = new ArrayList<CRMOfferBean>();

        if (useAllOfferLists) {
            for (OfferList offerList : response.getAllOfferLists()) {
                for (Offer offer : offerList.getRecommendedOffers()) {
                    CRMOfferBean offerBean = new CRMOfferBean();
                    offerBean.setEventBean(crmEventBean);
                    offerBean.setDescription(offer.getDescription());
                    offerBean.setOfferCode(offer.getOfferCodeToString(";"));
                    offerBean.setOfferName(offer.getOfferName());
                    offerBean.setTreatmentCode(offer.getTreatmentCode());

                    //System.out.println("parametersBean lenght: " + offer.getAdditionalAttributes().size());

                    for (String key : offer.getAdditionalAttributes().keySet()) {
                        HashMap<String, Object> attributes = offer.getAdditionalAttributes();
                        CRMOfferParametersBean parametersBean = new CRMOfferParametersBean();
                        parametersBean.setParameterName(key);
                        Object value = attributes.get(key);

                        if (value == null) {
                            parametersBean.setParameterValue(null);
                            //System.out.println("parametersBean [name: " + key + "; value: null]");
                        }
                        else if (value.getClass().equals(String.class)) {
                            parametersBean.setParameterValue((String) value);
                            //System.out.println("parametersBean [name: " + key + "; value: " + (String) value + "]");
                        }
                        else if (value.getClass().equals(Double.class)) {
                            parametersBean.setParameterValue(((Double) value).toString());
                            //System.out.println("parametersBean [name: " + key + "; value: " + ((Double) value).toString() + "]");
                        }
                        else if (value.getClass().equals(Integer.class)) {
                            parametersBean.setParameterValue(value.toString());
                            //System.out.println("parametersBean [name: " + key + "; value: " + value.toString() + "]");
                        }
                        else if (value.getClass().equals(Date.class)) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            parametersBean.setParameterValue(sdf.format((Date) value));
                            //System.out.println("parametersBean [name: " + key + "; value: " + sdf.format((Date) value) + "]");
                        }
                        else if (value.getClass().equals(Boolean.class)) {
                            parametersBean.setParameterValue(((Boolean) value).toString());
                            //System.out.println("parametersBean [name: " + key + "; value: " + ((Boolean) value).toString() + "]");
                        }
                        else {
                            parametersBean.setParameterValue(value.toString());
                            //System.out.println("parametersBean [name: " + key + "; value: " + value.toString() + "]");
                        }

                        parametersBean.setCRMOfferBean(offerBean);

                        offerBean.getCRMParametersBeanList().add(parametersBean);
                    }

                    crmOffersList.add(offerBean);
                }
            }
        }
        else {
            if (response.getOfferList() != null) {
                for (Offer offer : response.getOfferList().getRecommendedOffers()) {
                    CRMOfferBean offerBean = new CRMOfferBean();
                    offerBean.setEventBean(crmEventBean);
                    offerBean.setDescription(offer.getDescription());
                    offerBean.setOfferCode(offer.getOfferCodeToString(";"));
                    offerBean.setOfferName(offer.getOfferName());
                    offerBean.setTreatmentCode(offer.getTreatmentCode());

                    //System.out.println("parametersBean lenght: " + offer.getAdditionalAttributes().size());

                    for (String key : offer.getAdditionalAttributes().keySet()) {
                        HashMap<String, Object> attributes = offer.getAdditionalAttributes();
                        CRMOfferParametersBean parametersBean = new CRMOfferParametersBean();
                        parametersBean.setParameterName(key);
                        Object value = attributes.get(key);

                        if (value == null) {
                            parametersBean.setParameterValue(null);
                            //System.out.println("parametersBean [name: " + key + "; value: null]");
                        }
                        else if (value.getClass().equals(String.class)) {
                            parametersBean.setParameterValue((String) value);
                            //System.out.println("parametersBean [name: " + key + "; value: " + (String) value + "]");
                        }
                        else if (value.getClass().equals(Double.class)) {
                            parametersBean.setParameterValue(((Double) value).toString());
                            //System.out.println("parametersBean [name: " + key + "; value: " + ((Double) value).toString() + "]");
                        }
                        else if (value.getClass().equals(Integer.class)) {
                            parametersBean.setParameterValue(value.toString());
                            //System.out.println("parametersBean [name: " + key + "; value: " + value.toString() + "]");
                        }
                        else if (value.getClass().equals(Date.class)) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            parametersBean.setParameterValue(sdf.format((Date) value));
                            //System.out.println("parametersBean [name: " + key + "; value: " + sdf.format((Date) value) + "]");
                        }
                        else if (value.getClass().equals(Boolean.class)) {
                            parametersBean.setParameterValue(((Boolean) value).toString());
                            //System.out.println("parametersBean [name: " + key + "; value: " + ((Boolean) value).toString() + "]");
                        }
                        else {
                            parametersBean.setParameterValue(value.toString());
                            //System.out.println("parametersBean [name: " + key + "; value: " + value.toString() + "]");
                        }

                        parametersBean.setCRMOfferBean(offerBean);

                        offerBean.getCRMParametersBeanList().add(parametersBean);
                    }

                    crmOffersList.add(offerBean);

                }
            }
        }

        return crmOffersList;
    }
    
}
