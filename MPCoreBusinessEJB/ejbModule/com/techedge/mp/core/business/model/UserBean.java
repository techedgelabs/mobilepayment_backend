package com.techedge.mp.core.business.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.exceptions.PaymentInfoNotFoundException;
import com.techedge.mp.core.business.exceptions.PinException;
import com.techedge.mp.core.business.exceptions.UserException;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PlateNumber;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.UserSocialData;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.user.PersonalDataBusiness;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserDevice;
import com.techedge.mp.core.business.model.crmsf.CRMSfContactKeyBean;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.PinHelper;

@Entity
@Table(name = "USERS")
@NamedQueries({
        @NamedQuery(name = "UserBean.findUserById", query = "select u from UserBean u where u.id = :id"),

        @NamedQuery(name = "UserBean.findUserByPersonalDataBean", query = "select u from UserBean u where u.personalDataBean = :personalDataBean"),

        @NamedQuery(name = "UserBean.findUserByUserSearchPDYesUserStatusYesExternalId", query = "select u from  UserBean u Join u.personalDataBean p where p.firstName like :firstname "
                + "and p.lastName like :lastname and p.fiscalCode like :fiscalcode and p.securityDataEmail like :email and u.externalUserId like :externaluserid "
                + "and u.capAvailable >= :capavailablemin and u.capAvailable <= :capavailablemax and u.capEffective >= :capeffectivemin and u.capEffective <= :capeffectivemax "
                + "and u.userStatus = :userstatus and u.userType = :type"),

        @NamedQuery(name = "UserBean.findUserByUserSearchPDYesUserStatusNoExternalId", query = "select u from  UserBean u Join u.personalDataBean p where p.firstName like :firstname "
                + "and p.lastName like :lastname and p.fiscalCode like :fiscalcode and p.securityDataEmail like :email and u.capAvailable >= :capavailablemin "
                + "and u.capAvailable <= :capavailablemax and u.capEffective >= :capeffectivemin and u.capEffective <= :capeffectivemax and u.userStatus = :userstatus and u.userType = :type"),

        @NamedQuery(name = "UserBean.findUserByUserSearchPDNoUserStatusYesExternalId", query = "select u from  UserBean u Join u.personalDataBean p where p.firstName like :firstname "
                + "and p.lastName like :lastname and p.fiscalCode like :fiscalcode and p.securityDataEmail like :email and u.externalUserId like :externaluserid "
                + "and u.capAvailable >= :capavailablemin and u.capAvailable <= :capavailablemax and u.capEffective >= :capeffectivemin and u.capEffective <= :capeffectivemax  and u.userType = :type"),

        @NamedQuery(name = "UserBean.findUserByUserSearchPDNoUserStatusNoExternalId", query = "select u from  UserBean u Join u.personalDataBean p where p.firstName like :firstname "
                + "and p.lastName like :lastname and p.fiscalCode like :fiscalcode and p.securityDataEmail like :email and u.externalUserId = NULL "
                + "and u.capAvailable >= :capavailablemin and u.capAvailable <= :capavailablemax and u.capEffective >= :capeffectivemin and u.capEffective <= :capeffectivemax and u.userType = :type"),
        /*
         * @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveLoyaltyCard", query = "select "
         * + "(select count(u1.id) from UserBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
         * + "and (select size(u11.loyaltyCardList) from UserBean u11 where u11.id = u1) > 0 "
         * + "and u1.id in (select p1.user from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) and p1.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER
         * + "' " + "and (p1.insertTimestamp >= :dailyStartDate and p1.insertTimestamp < :dailyEndDate))), "
         * + "(select count(u2.id) from UserBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
         * + "and (select size(u21.loyaltyCardList) from UserBean u21 where u21.id = u2) > 0 "
         * + "and u2.id in (select p2.user from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) and p2.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER
         * + "' " + "and (p2.insertTimestamp >= :weeklyStartDate and p2.insertTimestamp < :weeklyEndDate))), "
         * + "(select count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
         * + "and (select size(u31.loyaltyCardList) from UserBean u31 where u31.id = u3) > 0 "
         * + "and u3.id in (select p3.user from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and p3.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER
         * + "' " + "and p3.insertTimestamp <= :dailyEndDate)) from UserBean u"),
         * 
         * @NamedQuery(name = "UserBean.statisticsReportSynthesisActive", query = "select "
         * + "(select count(u1.id) from UserBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
         * + "and u1.id in (select p1.user from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) and p1.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER
         * + "' " + "and (p1.insertTimestamp >= :dailyStartDate and p1.insertTimestamp < :dailyEndDate))), "
         * + "(select count(u2.id) from UserBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
         * + "and u2.id in (select p2.user from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) and p2.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER
         * + "' " + "and (p2.insertTimestamp >= :weeklyStartDate and p2.insertTimestamp < :weeklyEndDate))), "
         * + "(select count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
         * + "and u3.id in (select p3.user from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and p3.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER
         * + "' " + "and p3.insertTimestamp <= :dailyEndDate)) from UserBean u"),
         * 
         * @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveMasterCard", query = "select "
         * + "(select count(u1.id) from UserBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
         * + "and u1.id in (select p1.user from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) "
         * + "and (p1.insertTimestamp >= :dailyStartDate and p1.insertTimestamp < :dailyEndDate) and p1.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER + "') "
         * + "and u1.id in (select p1.user from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) and upper(p1.brand) like 'MASTERCARD')), "
         * + "(select count(u2.id) from UserBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
         * + "and u2.id in (select p2.user from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) "
         * + "and (p2.insertTimestamp >= :weeklyStartDate and p2.insertTimestamp < :weeklyEndDate) and p2.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER + "') "
         * + "and u2.id in (select p2.user from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) and upper(p2.brand) like 'MASTERCARD')), "
         * + "(select count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
         * + "and u3.id in (select p3.user from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) " + "and p3.insertTimestamp <= :dailyEndDate and p3.type = '"
         * + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER + "') "
         * + "and u3.id in (select p3.user from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and upper(p3.brand) like 'MASTERCARD')) from UserBean u"),
         * 
         * @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveCreditCard", query = "select "
         * + "(select count(u1.id) from UserBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
         * + "and u1.id in (select p1.user from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) "
         * + "and (p1.insertTimestamp >= :dailyStartDate and p1.insertTimestamp < :dailyEndDate) and p1.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER + "') "
         * + "and u1.id in (select distinct(p1.user) from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) and p1.type = '"
         * + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')), "
         * + "(select count(u2.id) from UserBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
         * + "and u2.id in (select p2.user from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) "
         * + "and (p2.insertTimestamp >= :weeklyStartDate and p2.insertTimestamp < :weeklyEndDate) and p2.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER + "') "
         * + "and u2.id in (select distinct(p2.user) from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) and p2.type = '"
         * + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')), "
         * + "(select count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
         * + "and u3.id in (select p3.user from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) " + "and p3.insertTimestamp <= :dailyEndDate and p3.type = '"
         * + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER + "') "
         * + "and u3.id in (select distinct(p3.user) from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and p3.type = '"
         * + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')) from UserBean u"),
         * 
         * @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveNoCreditCard", query = "select "
         * + "(select count(u1.id) from UserBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
         * + "and u1.id in (select p1.user from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) "
         * + "and (p1.insertTimestamp >= :dailyStartDate and p1.insertTimestamp < :dailyEndDate) and p1.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER + "') "
         * + "and u1.id not in (select distinct(p1.user) from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) and p1.type = '"
         * + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')), "
         * + "(select count(u2.id) from UserBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
         * + "and u2.id in (select p2.user from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) "
         * + "and (p2.insertTimestamp >= :weeklyStartDate and p2.insertTimestamp < :weeklyEndDate) and p2.type = '" + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER + "') "
         * + "and u2.id not in (select distinct(p2.user) from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) and p2.type = '"
         * + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')), "
         * + "(select count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
         * + "and u3.id in (select p3.user from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) " + "and p3.insertTimestamp <= :dailyEndDate and p3.type = '"
         * + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER + "') "
         * + "and u3.id not in (select distinct(p3.user) from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and p3.type = '"
         * + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')) from UserBean u"),
         */
        @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveLoyaltyCard", query = "select "
                + "(select count(u1.id) from UserBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.virtualizationCompleted = 1 and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate), "
                + "(select count(u2.id) from UserBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.virtualizationCompleted = 1 and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate), "
                + "(select count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.virtualizationCompleted = 1 and u3.userStatusRegistrationTimestamp >= :totalStartDate "
                + "and u3.userStatusRegistrationTimestamp < :dailyEndDate) from UserBean u"),
                
        @NamedQuery(name = "UserBean.statisticsReportSynthesisAllActiveLoyaltyCard", query = "select "
                + "count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.virtualizationCompleted = 1 and u3.userStatusRegistrationTimestamp >= :totalStartDate "
                + "and u3.userStatusRegistrationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "UserBean.statisticsReportSynthesisActive", query = "select "
                + "(select count(u1.id) from UserBean u1 where (u1.userType = 1 or u1.userType = 8) and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate), "
                + "(select count(u2.id) from UserBean u2 where (u2.userType = 1 or u2.userType = 8) and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate), "
                + "(select count(u3.id) from UserBean u3 where (u3.userType = 1 or u3.userType = 8) and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate) from UserBean u"),

        @NamedQuery(name = "UserBean.statisticsReportSynthesisAllActive", query = "select "
                + "count(u3.id) from UserBean u3 where (u3.userType = 1 or u3.userType = 8) and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveBySource", query = "select "
                + "(select count(u1.id) from UserBean u1 where u1.source = :source and (u1.userType = 1 or u1.userType = 8) and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate), "
                + "(select count(u2.id) from UserBean u2 where u2.source = :source and (u2.userType = 1 or u2.userType = 8) and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate), "
                + "(select count(u3.id) from UserBean u3 where u3.source = :source and (u3.userType = 1 or u3.userType = 8) and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate) from UserBean u"),

        @NamedQuery(name = "UserBean.statisticsReportSynthesisAllActiveBySource", query = "select "
                + "count(u3.id) from UserBean u3 where u3.source = :source and (u3.userType = 1 or u3.userType = 8) and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "UserBean.statisticsReportSynthesisSocialActive", query = "select "
                + "(select count(u1.id) from UserBean u1 join u1.userSocialData usd1 where (usd1.provider = :socialProvider and u1.userType = 1 or u1.userType = 8) and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate), "
                + "(select count(u2.id) from UserBean u2 join u2.userSocialData usd2 where (usd2.provider = :socialProvider and u2.userType = 1 or u2.userType = 8) and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate), "
                + "(select count(u3.id) from UserBean u3 join u3.userSocialData usd3 where (usd3.provider = :socialProvider and u3.userType = 1 or u3.userType = 8) and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate) from UserBean u"),
                
        @NamedQuery(name = "UserBean.statisticsReportSynthesisAllSocialActive", query = "select "
                + "count(u3.id) from UserBean u3 join u3.userSocialData usd3 where (usd3.provider = :socialProvider and u3.userType = 1 or u3.userType = 8) and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveGuest", query = "select "
                + "(select count(u1.id) from UserBean u1 where u1.userType = 8 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate), "
                + "(select count(u2.id) from UserBean u2 where u2.userType = 8 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate), "
                + "(select count(u3.id) from UserBean u3 where u3.userType = 8 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate) from UserBean u"),
                
        @NamedQuery(name = "UserBean.statisticsReportSynthesisAllActiveGuest", query = "select "
                + "count(u3.id) from UserBean u3 where u3.userType = 8 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveMasterCard", query = "select "
                + "(select count(u1.id) from UserBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and u1.id in (select p1.user from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) and upper(p1.brand) like 'MASTERCARD')), "
                + "(select count(u2.id) from UserBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate "
                + "and u2.id in (select p2.user from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) and upper(p2.brand) like 'MASTERCARD')), "
                + "(select count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp <= :dailyEndDate "
                + "and u3.id in (select p3.user from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and upper(p3.brand) like 'MASTERCARD')) from UserBean u"),

        @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveCreditCard", query = "select "
                + "(select count(u1.id) from UserBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and u1.id in (select distinct(p1.user) from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) and p1.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')), "
                + "(select count(u2.id) from UserBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate "
                + "and u2.id in (select distinct(p2.user) from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) and p2.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')), "
                + "(select count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and u3.id in (select distinct(p3.user) from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and p3.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')) from UserBean u"),
                
        @NamedQuery(name = "UserBean.statisticsReportSynthesisAllActiveCreditCard", query = "select "
                + "count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and u3.id in (select distinct(p3.user) from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and p3.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')"),

        @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveCreditCardBusiness", query = "select "
                + "(select count(u1.id) from UserBean u1 where u1.userType = 9 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and u1.id in (select distinct(p1.user) from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) and p1.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')), "
                + "(select count(u2.id) from UserBean u2 where u2.userType = 9 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate "
                + "and u2.id in (select distinct(p2.user) from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) and p2.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')), "
                + "(select count(u3.id) from UserBean u3 where u3.userType = 9 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and u3.id in (select distinct(p3.user) from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and p3.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')) from UserBean u"),
                
        @NamedQuery(name = "UserBean.statisticsReportSynthesisAllActiveCreditCardBusiness", query = "select "
                + "count(u3.id) from UserBean u3 where u3.userType = 9 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp >= :totalStartDate and u3.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and u3.id in (select distinct(p3.user) from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and p3.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')"),

        @NamedQuery(name = "UserBean.statisticsReportSynthesisActiveNoCreditCard", query = "select "
                + "(select count(u1.id) from UserBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and u1.id not in (select distinct(p1.user) from PaymentInfoBean p1 where (p1.status = 1 or p1.status = 2) and p1.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')), "
                + "(select count(u2.id) from UserBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate "
                + "and u2.id not in (select distinct(p2.user) from PaymentInfoBean p2 where (p2.status = 1 or p2.status = 2) and p2.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')), "
                + "(select count(u3.id) from UserBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.userStatusRegistrationTimestamp <= :dailyEndDate "
                + "and u3.id not in (select distinct(p3.user) from PaymentInfoBean p3 where (p3.status = 1 or p3.status = 2) and p3.type = '"
                + PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD + "')) from UserBean u"),

        @NamedQuery(name = "UserBean.findUserByCreation", query = "select u from UserBean u where u.createDate >= :creationStart and u.createDate <= :creationEnd"),

        @NamedQuery(name = "UserBean.findNewUserByCreation", query = "select u from UserBean u where u.createDate >= :creationStart and u.createDate <= :creationEnd and u.userStatus = 5"),

        @NamedQuery(name = "UserBean.findUserByStatus", query = "select u from UserBean u where u.userStatus = :userstatus and (u.userType = 1 or u.userType = 9)"),

        @NamedQuery(name = "UserBean.findUserByMobilePhone", query = "select u from UserBean u where u.contactDataMobilephone = :mobilephone"),

        @NamedQuery(name = "UserBean.findCustomerUserWithRegistrationCompleted", query = "select u from UserBean u where u.userStatusRegistrationCompleted = 1 and u.userType = 1"),

        @NamedQuery(name = "UserBean.findCustomerUserNotMigrated", query = "select u from UserBean u where u.userStatus = 2 and u.userType = 1 "
                + "and (u.oldUser is null or u.oldUser = 1) and u.createDate <= :createDateLimit"),

        @NamedQuery(name = "UserBean.findUserByDeviceID", query = "select u from UserBean u where u.deviceID = :deviceID"),

        @NamedQuery(name = "UserBean.findUserByPersonalDataBeanAndNotStatus", query = "select u from UserBean u where u.personalDataBean = :personalDataBean "
                + "and u.userStatus <> :userStatus"),

        @NamedQuery(name = "UserBean.findForNotification", query = "select u from UserBean u where u.userStatus = 2 and u.userType = 1 and u.userStatusRegistrationCompleted = 1 "
                + "and u.virtualizationCompleted = 1 and (u.lastLoginDataBean != null and u.lastLoginDataBean.deviceEndpoint != null)"), 

        @NamedQuery(name = "UserBean.findByEmailAndSource", query = "select u from UserBean u where u.source = :source and u.personalDataBean.securityDataEmail = :email"),
        
        @NamedQuery(name = "UserBean.findByUserType", query = "select u from UserBean u where u.userType = :userType"),
        
        @NamedQuery(name = "UserBean.findByFiscalCode", query = "select u from UserBean u where u.personalDataBean.fiscalCode = :fiscalCode"),
        
        @NamedQuery(name = "UserBean.findByEmail", query = "select u from UserBean u where u.personalDataBean.securityDataEmail = :email")


})
public class UserBean {

    public static final String            FIND_BY_ID                                              		= "UserBean.findUserById";
    public static final String            FIND_BY_PERSONALDATABEAN                                		= "UserBean.findUserByPersonalDataBean";
    public static final String            FIND_BY_USERSEARCHPD_YES_USERSTATUS_YES_EXTERNALID      		= "UserBean.findUserByUserSearchPDYesUserStatusYesExternalId";
    public static final String            FIND_BY_USERSEARCHPD_YES_USERSTATUS_NO_EXTERNALID       		= "UserBean.findUserByUserSearchPDYesUserStatusNoExternalId";
    public static final String            FIND_BY_USERSEARCHPD_NO_USERSTATUS_YES_EXTERNALID       		= "UserBean.findUserByUserSearchPDNoUserStatusYesExternalId";
    public static final String            FIND_BY_USERSEARCHPD_NO_USERSTATUS_NO_EXTERNALID        		= "UserBean.findUserByUserSearchPDNoUserStatusNoExternalId";
    public static final String            FIND_BY_CREATION_DATE                                   		= "UserBean.findUserByCreation";
    public static final String            FIND_NEW_BY_CREATION_DATE                               		= "UserBean.findNewUserByCreation";
    public static final String            FIND_BY_STATUS                                         		= "UserBean.findUserByStatus";
    public static final String            FIND_BY_PERSONALDATABEANANDNOTSTATUS                   		= "UserBean.findUserByPersonalDataBeanAndNotStatus";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ACTIVE                      		= "UserBean.statisticsReportSynthesisActive";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE                      	= "UserBean.statisticsReportSynthesisAllActive";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ACTIVE_BY_SOURCE            		= "UserBean.statisticsReportSynthesisActiveBySource";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE_BY_SOURCE            	= "UserBean.statisticsReportSynthesisAllActiveBySource";
    public static final String            STATISTICS_REPORT_SYNTHESIS_SOCIAL_ACTIVE               		= "UserBean.statisticsReportSynthesisSocialActive";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ALL_SOCIAL_ACTIVE               	= "UserBean.statisticsReportSynthesisAllSocialActive";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ACTIVE_GUEST                		= "UserBean.statisticsReportSynthesisActiveGuest";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE_GUEST                	= "UserBean.statisticsReportSynthesisAllActiveGuest";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ACTIVE_LOYALTYCARD          		= "UserBean.statisticsReportSynthesisActiveLoyaltyCard";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE_LOYALTYCARD          	= "UserBean.statisticsReportSynthesisAllActiveLoyaltyCard";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ACTIVE_MASTERCARD           		= "UserBean.statisticsReportSynthesisActiveMasterCard";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ACTIVE_CREDIT_CARD          		= "UserBean.statisticsReportSynthesisActiveCreditCard";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE_CREDIT_CARD         	= "UserBean.statisticsReportSynthesisAllActiveCreditCard";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ACTIVE_CREDIT_CARD_BUSINESS 		= "UserBean.statisticsReportSynthesisActiveCreditCardBusiness";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ALL_ACTIVE_CREDIT_CARD_BUSINESS 	= "UserBean.statisticsReportSynthesisAllActiveCreditCardBusiness";
    public static final String            STATISTICS_REPORT_SYNTHESIS_ACTIVE_NO_CREDIT_CARD       		= "UserBean.statisticsReportSynthesisActiveNoCreditCard";
    public static final String            FIND_BY_MOBILE_PHONE                                    		= "UserBean.findUserByMobilePhone";
    public static final String            FIND_CUSTOMER_USER_WITH_REGISTRATION_COMPLETED          		= "UserBean.findCustomerUserWithRegistrationCompleted";
    public static final String            FIND_CUSTOMER_USER_NOT_MIGRATED                         		= "UserBean.findCustomerUserNotMigrated";
    public static final String            FIND_USER_BY_DEVICEID                                   		= "UserBean.findUserByDeviceID";
    public static final String            FIND_FOR_NOTIFICATION                                   		= "UserBean.findForNotification";
    public static final String            FIND_BY_EMAIL_AND_SOURCE                                		= "UserBean.findByEmailAndSource";
    public static final String            FIND_BY_USER_TYPE                                       		= "UserBean.findByUserType";
    public static final String            FIND_BY_FISCALCODE                                      		= "UserBean.findByFiscalCode";
    public static final String            FIND_BY_EMAIL                                           		= "UserBean.findByEmail";

    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                          id;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "personaldata_id")
    private PersonalDataBean              personalDataBean;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "lastlogindata_id")
    private LastLoginDataBean             lastLoginDataBean;

    @Column(name = "userstatus_status", nullable = false, length = 1)
    private Integer                       userStatus;

    @Column(name = "userstatus_registrationcompleted", nullable = false)
    private Boolean                       userStatusRegistrationCompleted;

    @Column(name = "userstatus_registrationtimestamp", nullable = true)
    private Date                          userStatusRegistrationTimestamp;

    @Column(name = "cap_available", nullable = true)
    private Double                        capAvailable;

    @Column(name = "cap_effective", nullable = true)
    private Double                        capEffective;

    @Column(name = "contactdata_mobilephone", nullable = true, length = 20)
    private String                        contactDataMobilephone;

    @Column(name = "contactdata_status", nullable = true, length = 1)
    private Integer                       contactDataStatus;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    //.PERSIST
    private Set<PaymentInfoBean>          paymentData                                             = new HashSet<PaymentInfoBean>(0);

    @Column(name = "creditdata_voucher", nullable = true, length = 1)
    private Integer                       creditDataVoucher;

    @Column(name = "user_type", nullable = false, length = 1)
    private Integer                       userType;

    @Column(name = "external_user_id", nullable = true)
    private String                        externalUserId;

    @Column(name = "create_date", nullable = true)
    private Date                          createDate;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "lastloginerrordata_id")
    private LastLoginErrorDataBean        lastLoginErrorDataBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userBean", cascade = CascadeType.ALL)
    //.PERSIST
    private Set<VoucherBean>              voucherList                                             = new HashSet<VoucherBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userBean", cascade = CascadeType.ALL)
    //.PERSIST
    private Set<LoyaltyCardBean>          loyaltyCardList                                         = new HashSet<LoyaltyCardBean>(0);

    @Column(name = "use_voucher", nullable = true)
    private Boolean                       useVoucher;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    //.PERSIST
    private Set<MobilePhoneBean>          mobilePhoneList                                         = new HashSet<MobilePhoneBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    //.PERSIST
    private Set<SmsLogBean>               smsLogList                                              = new HashSet<SmsLogBean>(0);

    @Column(name = "old_user", nullable = true)
    private Boolean                       oldUser;

    @Column(name = "device_id", nullable = true)
    private String                        deviceID;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userBean", cascade = CascadeType.ALL)
    private Set<UserDeviceBean>           userDeviceBean                                          = new HashSet<UserDeviceBean>(0);

    @Column(name = "virtualization_completed", nullable = true)
    private Boolean                       virtualizationCompleted;

    @Column(name = "virtualization_timestamp", nullable = true)
    private Date                          virtualizationTimestamp;

    @Column(name = "virtualization_attempts_left", nullable = true)
    private Integer                       virtualizationAttemptsLeft;

    @Column(name = "eniStation_usertype", nullable = true)
    private Boolean                       eniStationUserType;

    @Column(name = "deposit_card_step_completed", nullable = true)
    private Boolean                       depositCardStepCompleted;

    @Column(name = "deposit_card_step_timestamp", nullable = true)
    private Date                          depositCardStepTimestamp;

    @Column(name = "updated_mission", nullable = true)
    private Boolean                       updatedMission;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userBean", cascade = CascadeType.ALL)
    private Set<UserSocialDataBean>       userSocialData                                          = new HashSet<UserSocialDataBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userBean", cascade = CascadeType.ALL)
    private Set<UserExternalDataBean>       userExternalData                                          = new HashSet<UserExternalDataBean>(0);
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userBean", cascade = CascadeType.ALL)
    private Set<PlateNumberBean>          plateNumberList                                         = new HashSet<PlateNumberBean>(0);

    @Column(name = "source", nullable = true, length = 50)
    private String                        source;

    @Column(name = "source_notified", nullable = true, length = 50)
    private Boolean                       sourceNotified;

    @Column(name = "landing_displayed", nullable = true)
    private Boolean                       landingDisplayed;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userBean", cascade = CascadeType.ALL)
    private Set<PersonalDataBusinessBean> personalDataBusinessList                                = new HashSet<PersonalDataBusinessBean>(0);

    @Column(name = "active_mc_card_dpan", nullable = true, length = 255)
    private String                        activeMcCardDpan;
    
    /*@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id" , nullable=true)
    private CRMSfContactKeyBean crmSfContactKey;*/
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userBean", cascade = CascadeType.ALL)
    private List<CRMSfContactKeyBean> crmSfContactKey = new ArrayList<>();

    
    public UserBean() {}

    public UserBean(User user) {

        this.id = user.getId();

        this.personalDataBean = new PersonalDataBean(user.getPersonalData());

        if (user.getLastLoginData() != null) {
            this.lastLoginDataBean = new LastLoginDataBean(user.getLastLoginData());
        }
        else {
            this.lastLoginDataBean = null;
        }

        if (user.getLastLoginErrorData() != null) {
            this.lastLoginErrorDataBean = new LastLoginErrorDataBean(user.getLastLoginErrorData());
        }
        else {
            this.lastLoginErrorDataBean = null;
        }

        this.userStatus = user.getUserStatus();
        this.userStatusRegistrationCompleted = user.getUserStatusRegistrationCompleted();
        this.userStatusRegistrationTimestamp = user.getUserStatusRegistrationTimestamp();
        this.capAvailable = user.getCapAvailable();
        this.capEffective = user.getCapEffective();
        this.contactDataMobilephone = user.getContactDataMobilephone();
        this.source = user.getSource();
        this.contactDataStatus = user.getContactDataStatus();

        if (user.getPaymentData() != null) {
            Set<PaymentInfo> paymentData = user.getPaymentData();
            for (PaymentInfo paymentInfo : paymentData) {
                PaymentInfoBean paymentInfoBean = new PaymentInfoBean(paymentInfo);
                paymentInfoBean.setUser(this);
                this.paymentData.add(paymentInfoBean);
            }
        }

        this.creditDataVoucher = user.getCreditDataVoucher();
        this.userType = user.getUserType();
        this.externalUserId = user.getExternalUserId();
        this.createDate = user.getCreateDate();
        this.useVoucher = user.getUseVoucher();
        this.oldUser = user.getOldUser();
        this.deviceID = user.getDeviceId();

        if (user.getVoucherList() != null) {
            Set<Voucher> voucherList = user.getVoucherList();
            for (Voucher voucher : voucherList) {
                VoucherBean voucherBean = new VoucherBean(voucher);
                voucherBean.setUserBean(this);
                this.voucherList.add(voucherBean);
            }
        }

        if (user.getLoyaltyCardList() != null) {
            Set<LoyaltyCard> loyaltyCardList = user.getLoyaltyCardList();
            for (LoyaltyCard loyaltyCard : loyaltyCardList) {
                LoyaltyCardBean loyaltyCardBean = new LoyaltyCardBean(loyaltyCard);
                loyaltyCardBean.setUserBean(this);
                this.loyaltyCardList.add(loyaltyCardBean);
            }
        }

        if (user.getMobilePhoneList() != null) {
            for (MobilePhone mobilePhone : user.getMobilePhoneList()) {
                MobilePhoneBean mobilePhoneBean = new MobilePhoneBean(mobilePhone);
                mobilePhoneBean.setUser(this);
                this.mobilePhoneList.add(mobilePhoneBean);
            }
        }

        this.virtualizationCompleted = user.getVirtualizationCompleted();
        this.virtualizationAttemptsLeft = user.getVirtualizationAttemptsLeft();
        this.depositCardStepCompleted = user.getDepositCardStepCompleted();
        this.updatedMission = user.getUpdatedMission();
        this.eniStationUserType = user.getEniStationUserType();

        if (user.getUserDeviceList() != null) {
            for (UserDevice userDevice : user.getUserDeviceList()) {
                UserDeviceBean userDeviceBean = new UserDeviceBean(userDevice);
                userDeviceBean.setUserBean(this);
                this.userDeviceBean.add(userDeviceBean);
            }
        }

        if (user.getUserSocialData() != null) {
            for (UserSocialData userSocialData : user.getUserSocialData()) {
                UserSocialDataBean userSocialBean = new UserSocialDataBean(userSocialData);
                userSocialBean.setUserBean(this);
                this.userSocialData.add(userSocialBean);
            }
        }

        if (user.getPlateNumberList() != null) {
            for (PlateNumber plateNumber : user.getPlateNumberList()) {
                PlateNumberBean plateNumberBean = new PlateNumberBean(plateNumber);
                plateNumberBean.setUserBean(this);
                this.plateNumberList.add(plateNumberBean);
            }
        }

        if (user.getPersonalDataBusinessList() != null) {
            for (PersonalDataBusiness personalDataBusiness : user.getPersonalDataBusinessList()) {
                PersonalDataBusinessBean personalDataBusinessBean = new PersonalDataBusinessBean(personalDataBusiness);
                personalDataBusinessBean.setUserBean(this);
                this.personalDataBusinessList.add(personalDataBusinessBean);
            }
        }
        
        

    }

    public User toUser() {

        User user = new User();

        user.setId(this.id);
        user.setPersonalData(this.personalDataBean.toPersonalData());

        if (this.lastLoginDataBean != null) {
            user.setLastLoginData(this.lastLoginDataBean.toLastLoginData());
        }
        else {
            user.setLastLoginData(null);
        }

        if (this.lastLoginErrorDataBean != null) {
            user.setLastLoginErrorData(this.lastLoginErrorDataBean.toLastLoginErrorData());
        }
        else {
            user.setLastLoginErrorData(null);
        }

        user.setUserStatus(this.userStatus);
        user.setUserStatusRegistrationCompleted(this.userStatusRegistrationCompleted);
        user.setUserStatusRegistrationTimestamp(this.userStatusRegistrationTimestamp);
        user.setCapAvailable(this.capAvailable);
        user.setCapEffective(this.capEffective);
        user.setContactDataMobilephone(this.contactDataMobilephone);
        user.setSource(this.source);
        user.setContactDataStatus(this.contactDataStatus);

        if (!this.paymentData.isEmpty()) {

            Set<PaymentInfoBean> paymentData = this.paymentData;

            // Prima si aggiunge alla lista il metodo di default 
            for (PaymentInfoBean paymentInfoBean : paymentData) {
                if (paymentInfoBean.getDefaultMethod()) {
                    PaymentInfo paymentInfo = paymentInfoBean.toPaymentInfo();
                    user.getPaymentData().add(paymentInfo);
                }
            }

            // Poi si aggiungono gli altri 
            for (PaymentInfoBean paymentInfoBean : paymentData) {
                if (!paymentInfoBean.getDefaultMethod()) {
                    PaymentInfo paymentInfo = paymentInfoBean.toPaymentInfo();
                    user.getPaymentData().add(paymentInfo);
                }
            }
        }

        user.setCreditDataVoucher(this.creditDataVoucher);
        user.setUserType(this.userType);
        user.setExternalUserId(this.externalUserId);
        user.setCreateDate(this.createDate);
        user.setUseVoucher(this.useVoucher);
        user.setOldUser(this.oldUser);
        user.setDeviceId(this.deviceID);

        if (!this.voucherList.isEmpty()) {

            Set<VoucherBean> voucherList = this.voucherList;
            for (VoucherBean voucherBean : voucherList) {
                Voucher voucher = voucherBean.toVoucher();
                user.getVoucherList().add(voucher);
            }
        }

        if (!this.loyaltyCardList.isEmpty()) {

            Set<LoyaltyCardBean> loyaltyCardBeanList = this.loyaltyCardList;
            for (LoyaltyCardBean loyaltyCardBean : loyaltyCardBeanList) {
                LoyaltyCard loyaltyCard = loyaltyCardBean.toLoyaltyCard();
                user.getLoyaltyCardList().add(loyaltyCard);
            }
        }

        if (!this.mobilePhoneList.isEmpty()) {

            for (MobilePhoneBean mobilePhoneBean : mobilePhoneList) {
                MobilePhone mobilePhone = mobilePhoneBean.toMobilePhone();
                user.getMobilePhoneList().add(mobilePhone);
            }
        }

        user.setVirtualizationCompleted(this.virtualizationCompleted);
        user.setVirtualizationAttemptsLeft(this.virtualizationAttemptsLeft);
        user.setDepositCardStepCompleted(this.depositCardStepCompleted);
        user.setUpdatedMission(this.updatedMission);
        user.setEniStationUserType(this.eniStationUserType);
        user.setSource(this.source);

        if (!this.userDeviceBean.isEmpty()) {

            for (UserDeviceBean userDeviceBean : this.userDeviceBean) {
                UserDevice userDevice = userDeviceBean.toUserDevice();
                user.getUserDeviceList().add(userDevice);
            }
        }

        if (!this.userSocialData.isEmpty()) {

            for (UserSocialDataBean userSocialDataBean : this.userSocialData) {
                UserSocialData userSocialData = userSocialDataBean.toUserSocialData();
                user.getUserSocialData().add(userSocialData);
            }
        }

        if (!this.plateNumberList.isEmpty()) {

            for (PlateNumberBean plateNumberBean : this.plateNumberList) {
                PlateNumber plateNumber = plateNumberBean.toPlateNumber();
                user.getPlateNumberList().add(plateNumber);
            }
        }

        if (!this.personalDataBusinessList.isEmpty()) {
            for (PersonalDataBusinessBean personalDataBusinessBean : this.personalDataBusinessList) {
                PersonalDataBusiness personalDataBusiness = personalDataBusinessBean.toPersonalDataBusiness();
                user.getPersonalDataBusinessList().add(personalDataBusiness);
            }
        }
        
        //System.out.println("####---->>>> "+this.crmSfContactKey);
        
        if(this.crmSfContactKey!=null && this.crmSfContactKey.size()>0){
        	//System.out.println("####---->>>> "+this.crmSfContactKey.get(0).getContactKey());
        	user.setContactKey(this.crmSfContactKey.get(0).getContactKey());
        }

        return user;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PersonalDataBean getPersonalDataBean() {
        return personalDataBean;
    }

    public void setPersonalDataBean(PersonalDataBean personalDataBean) {
        this.personalDataBean = personalDataBean;
    }

    public LastLoginDataBean getLastLoginDataBean() {
        return lastLoginDataBean;
    }

    public void setLastLoginDataBean(LastLoginDataBean lastLoginDataBean) {
        this.lastLoginDataBean = lastLoginDataBean;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Boolean getUserStatusRegistrationCompleted() {
        if (userStatusRegistrationCompleted == null) {
            return Boolean.FALSE;
        }
        return userStatusRegistrationCompleted;
    }

    public void setUserStatusRegistrationCompleted(Boolean userStatusRegistrationCompleted) {
        this.userStatusRegistrationCompleted = userStatusRegistrationCompleted;
    }

    public Double getCapAvailable() {
        return capAvailable;
    }

    public void setCapAvailable(Double capAvailable) {
        this.capAvailable = capAvailable;
    }

    public Double getCapEffective() {
        return capEffective;
    }

    public void setCapEffective(Double capEffective) {
        this.capEffective = capEffective;
    }

    public String getContactDataMobilephone() {
        return contactDataMobilephone;
    }

    public void setContactDataMobilephone(String contactDataMobilephone) {
        this.contactDataMobilephone = contactDataMobilephone;
    }

    public Integer getContactDataStatus() {
        return contactDataStatus;
    }

    public void setContactDataStatus(Integer contactDataStatus) {
        this.contactDataStatus = contactDataStatus;
    }

    public Set<PaymentInfoBean> getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(Set<PaymentInfoBean> paymentData) {
        this.paymentData = paymentData;
    }

    public Integer getCreditDataVoucher() {
        return creditDataVoucher;
    }

    public void setCreditDataVoucher(Integer creditDataVoucher) {
        this.creditDataVoucher = creditDataVoucher;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getExternalUserId() {
        return externalUserId;
    }

    public void setExternalUserId(String externalUserId) {
        this.externalUserId = externalUserId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public LastLoginErrorDataBean getLastLoginErrorDataBean() {
        return lastLoginErrorDataBean;
    }

    public void setLastLoginErrorDataBean(LastLoginErrorDataBean lastLoginErrorDataBean) {
        this.lastLoginErrorDataBean = lastLoginErrorDataBean;
    }

    public Set<VoucherBean> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(Set<VoucherBean> voucherList) {
        this.voucherList = voucherList;
    }

    public Set<LoyaltyCardBean> getLoyaltyCardList() {
        return loyaltyCardList;
    }

    public void setLoyaltyCardList(Set<LoyaltyCardBean> loyaltyCardList) {
        this.loyaltyCardList = loyaltyCardList;
    }

    public Set<MobilePhoneBean> getMobilePhoneList() {
        return mobilePhoneList;
    }

    public void setMobilePhoneList(Set<MobilePhoneBean> mobilePhoneList) {
        this.mobilePhoneList = mobilePhoneList;
    }

    public Set<SmsLogBean> getSmsLogList() {
        return smsLogList;
    }

    public void setSmsLogList(Set<SmsLogBean> smsLogList) {
        this.smsLogList = smsLogList;
    }

    public Boolean getOldUser() {
        return oldUser;
    }

    public void setOldUser(Boolean oldUser) {
        this.oldUser = oldUser;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceId) {
        this.deviceID = deviceId;
    }

    public Set<UserDeviceBean> getUserDeviceBean() {
        return userDeviceBean;
    }

    public void setUserDeviceBean(Set<UserDeviceBean> userDeviceBean) {
        this.userDeviceBean = userDeviceBean;
    }

    public Boolean getVirtualizationCompleted() {
        if (virtualizationCompleted == null) {
            return Boolean.FALSE;
        }
        return virtualizationCompleted;
    }

    public void setVirtualizationCompleted(Boolean virtualizationCompleted) {
        this.virtualizationCompleted = virtualizationCompleted;
    }

    public Date getVirtualizationTimestamp() {
        return virtualizationTimestamp;
    }

    public void setVirtualizationTimestamp(Date virtualizationTimestamp) {
        this.virtualizationTimestamp = virtualizationTimestamp;
    }

    public Boolean getUpdatedMission() {
        if (updatedMission == null) {
            return Boolean.FALSE;
        }
        return updatedMission;
    }

    public void setUpdatedMission(Boolean updatedMission) {
        this.updatedMission = updatedMission;
    }

    public Boolean getSourceNotified() {
        if (sourceNotified == null) {
            return Boolean.FALSE;
        }
        return sourceNotified;
    }

    public void setSourceNotified(Boolean sourceNotified) {
        this.sourceNotified = sourceNotified;
    }

    public static UserBean createNewCustomerUser(User user, Double initialCap) throws UserException {

        UserBean userBean = new UserBean(user);

        // TODO - verificare i dati in TermsOfService

        String encodedPassword = EncoderHelper.encode(user.getPersonalData().getSecurityDataPassword());

        PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
        personalDataBean.setSecurityDataPassword(encodedPassword);
        personalDataBean.setExpirationDateRescuePassword(null);
        personalDataBean.setRescuePassword(null);

        HistoryPasswordBean historyPasswordBean = new HistoryPasswordBean();
        historyPasswordBean.setPersonalDataBean(personalDataBean);
        historyPasswordBean.setPassword(encodedPassword);
        personalDataBean.getHistoryPasswordBeanData().add(historyPasswordBean);

        userBean.setPersonalDataBean(personalDataBean);
        userBean.setUserStatus(User.USER_STATUS_NEW);
        userBean.setUserStatusRegistrationCompleted(false);
        userBean.setCapAvailable(initialCap);
        userBean.setCapEffective(initialCap);
        userBean.setCreateDate(new Date());
        userBean.setUseVoucher(true);
        userBean.setOldUser(false);
        userBean.setDeviceID(user.getDeviceId());
        userBean.setEniStationUserType(user.getEniStationUserType());
        userBean.setSource(user.getSource());
        return userBean;
    }

    public static UserBean createNewTesterUser(User user, Double initialCap) throws UserException {
        UserBean userBean = new UserBean(user);

        String encodedPassword = EncoderHelper.encode(user.getPersonalData().getSecurityDataPassword());

        PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
        personalDataBean.setSecurityDataPassword(encodedPassword);
        personalDataBean.setExpirationDateRescuePassword(null);
        personalDataBean.setRescuePassword(null);

        HistoryPasswordBean historyPasswordBean = new HistoryPasswordBean();
        historyPasswordBean.setPersonalDataBean(personalDataBean);
        historyPasswordBean.setPassword(encodedPassword);
        personalDataBean.getHistoryPasswordBeanData().add(historyPasswordBean);

        userBean.setPersonalDataBean(personalDataBean);
        userBean.setUserStatus(User.USER_STATUS_NEW);
        userBean.setUserStatusRegistrationCompleted(false);
        userBean.setUserType(user.getUserType());
        userBean.setCapAvailable(initialCap);
        userBean.setCapEffective(initialCap);
        userBean.setCreateDate(new Date());
        userBean.setUseVoucher(false);
        userBean.setOldUser(false);
        userBean.setVirtualizationCompleted(true);
        userBean.setVirtualizationTimestamp(new Date());

        return userBean;
    }

    public static UserBean createNewBusinessUser(User user, Double initialCap) throws UserException {

        UserBean userBean = new UserBean(user);

        String encodedPassword = EncoderHelper.encode(user.getPersonalData().getSecurityDataPassword());

        PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
        personalDataBean.setSecurityDataPassword(encodedPassword);
        personalDataBean.setExpirationDateRescuePassword(null);
        personalDataBean.setRescuePassword(null);

        HistoryPasswordBean historyPasswordBean = new HistoryPasswordBean();
        historyPasswordBean.setPersonalDataBean(personalDataBean);
        historyPasswordBean.setPassword(encodedPassword);
        personalDataBean.getHistoryPasswordBeanData().add(historyPasswordBean);

        userBean.setPersonalDataBean(personalDataBean);
        userBean.setUserStatus(User.USER_STATUS_NEW);
        userBean.setUserStatusRegistrationCompleted(false);
        userBean.setCapAvailable(initialCap);
        userBean.setCapEffective(initialCap);
        userBean.setCreateDate(new Date());
        userBean.setUseVoucher(false);
        userBean.setOldUser(false);
        userBean.setDeviceID(user.getDeviceId());
        userBean.setEniStationUserType(false);
        userBean.setSource(null);
        userBean.setVirtualizationCompleted(false);
        userBean.setUserType(User.USER_TYPE_BUSINESS);

        return userBean;
    }

    public static UserBean createMigrateBusinessUser(User user, Double initialCap) throws UserException {

        UserBean userBean = new UserBean(user);
        //userBean.setUserStatus(User.USER_STATUS_MIGRATING_BUSINESS);
        userBean.setUserStatus(User.USER_STATUS_VERIFIED);
        userBean.setSource("ENISTATION+");
        userBean.setUserType(User.USER_TYPE_BUSINESS);
        userBean.setUserStatusRegistrationCompleted(false);
        userBean.setCapAvailable(initialCap);
        userBean.setCapEffective(initialCap);
        userBean.setCreateDate(new Date());
        userBean.setUseVoucher(false);
        userBean.setOldUser(false);
        userBean.setDeviceID(user.getDeviceId());
        userBean.setEniStationUserType(false);
        userBean.setVirtualizationCompleted(false);

        return userBean;
    }
    
    public void setStatusToVerified() throws UserException {

        this.setUserStatus(User.USER_STATUS_VERIFIED);
    }

    public Boolean setRegistrationCompleted() throws UserException {

        if (!this.userStatusRegistrationCompleted) {
            this.userStatusRegistrationCompleted = true;
            return true;
        }
        else {
            return false;
        }
    }

    public Boolean unsetRegistrationCompleted() throws UserException {

        if (this.userStatusRegistrationCompleted) {
            this.userStatusRegistrationCompleted = false;
            return true;
        }
        else {
            return false;
        }
    }

    public void changeStatus(Integer status) throws UserException {

        if (status != User.USER_STATUS_BLOCKED && status != User.USER_STATUS_NEW && status != User.USER_STATUS_VERIFIED && status != User.USER_STATUS_TEMPORARY_PASSWORD
                && status != User.USER_STATUS_CANCELLED) {
            throw new UserException("Invalid status: " + status);
        }

        this.userStatus = status;
    }

    public PaymentInfoBean addNewPendingPaymentInfo(String paymentMethodType, String newPin, Boolean defaultMethod, Integer pinCheckMaxAttempts, Double checkAmount,
            String checkBankTransactionID, String checkCurrency, String checkShopLogin, String checkShopTransactionID) throws UserException {

        PaymentInfoBean paymentInfoBean = new PaymentInfoBean();

        paymentInfoBean.setType(paymentMethodType);
        paymentInfoBean.setBrand(null);
        paymentInfoBean.setExpirationDate(null);
        paymentInfoBean.setPan(null);
        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_PENDING);
        paymentInfoBean.setMessage(null);
        paymentInfoBean.setToken(null);
        paymentInfoBean.setToken3ds(null);
        paymentInfoBean.setDefaultMethod(defaultMethod);
        paymentInfoBean.setAttemptsLeft(0);
        paymentInfoBean.setCheckAmount(0.0);
        paymentInfoBean.setInsertTimestamp(new Date());
        paymentInfoBean.setVerifiedTimestamp(null);
        paymentInfoBean.setPin(newPin);
        paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);
        paymentInfoBean.setCheckAmount(checkAmount);
        paymentInfoBean.setCheckBankTransactionID(checkBankTransactionID);
        paymentInfoBean.setCheckCurrency(checkCurrency);
        paymentInfoBean.setCheckShopLogin(checkShopLogin);
        paymentInfoBean.setCheckShopTransactionID(checkShopTransactionID);

        paymentInfoBean.setUser(this);

        if (defaultMethod == true) {

            // Se si sta inserendo un nuovo metodo di pagamento di default tutti gli altri sono impostati a false 

            if (!this.getPaymentData().isEmpty()) {

                Set<PaymentInfoBean> paymentData = this.getPaymentData();
                for (PaymentInfoBean paymentInfoBeanTemp : paymentData) {

                    if (paymentInfoBeanTemp.getDefaultMethod() == true) {

                        paymentInfoBeanTemp.setDefaultMethod(false);
                    }
                }
            }
        }

        this.getPaymentData().add(paymentInfoBean);

        return paymentInfoBean;
    }

    /*
     * public PaymentInfoBean removePaymentInfo(PaymentInfoBean paymentInfoBean) throws UserException {
     * 
     * paymentInfoBean.setStatusToCanceled();
     * 
     * return paymentInfoBean;
     * }
     */

    public PaymentInfoBean removePaymentInfo(Long paymentMethodId, String paymentMethodType) {

        PaymentInfoBean paymentInfoBean = this.findPaymentInfoBean(paymentMethodId, paymentMethodType);

        if (paymentInfoBean == null) {

            return null;
        }

        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED
                || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

            // Modifica lo stato del metodo di pagamento in cancellato e imposta il relativo messaggio
            paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
            paymentInfoBean.setMessage("Payment method canceled");

            // Se il metodo cancellato era quello di default bisogna impostare un altro metodo di default
            if (paymentInfoBean.getDefaultMethod()) {

                paymentInfoBean.setDefaultMethod(false);

                this.setNewDefaultPaymentMethod();
            }

            return paymentInfoBean;
        }
        else {

            return null;
        }

    }

    public PaymentInfoBean findDefaultPaymentInfoBean() {

        PaymentInfoBean paymentInfoBean = null;
        if (!this.getPaymentData().isEmpty()) {

            Set<PaymentInfoBean> paymentData = this.getPaymentData();
            for (PaymentInfoBean paymentInfoBeanTemp : paymentData) {

                if ((paymentInfoBeanTemp.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfoBeanTemp.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)
                        && (paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD))) {

                    if (paymentInfoBeanTemp.getDefaultMethod() == true) {

                        paymentInfoBean = paymentInfoBeanTemp;
                        break;
                    }
                }
            }
        }

        return paymentInfoBean;
    }
    
    public PaymentInfoBean findPaymentInfoCreditCardBean() {

        PaymentInfoBean paymentInfoBeanFirst   = null;
        PaymentInfoBean paymentInfoBeanDefault = null;
        
        // Ricerca tra tutti i metodi di pagamento di tipo carta di credito
        
        // Se � presente il metodo di default resittuisci quello
        
        // Altrimenti il primo della lista
        
        PaymentInfoBean paymentInfoBean = null;
        if (!this.getPaymentData().isEmpty()) {

            Set<PaymentInfoBean> paymentData = this.getPaymentData();
            for (PaymentInfoBean paymentInfoBeanTemp : paymentData) {

                if ((paymentInfoBeanTemp.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfoBeanTemp.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)
                        && (paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD))) {

                    if (paymentInfoBeanFirst == null) {
                        paymentInfoBeanFirst = paymentInfoBeanTemp;
                    }
                    
                    if (paymentInfoBeanTemp.getDefaultMethod() == true) {
                        paymentInfoBeanDefault = paymentInfoBeanTemp;
                        break;
                    }
                }
            }
        }
        
        if (paymentInfoBeanDefault != null)
            return paymentInfoBeanDefault;

        return paymentInfoBean;
    }

    public void setNewDefaultPaymentMethod() {

        if (!this.getPaymentData().isEmpty()) {

            // Se esiste un metodo di pagamento verificato si imposta come default e si esce
            Set<PaymentInfoBean> paymentData = this.getPaymentData();
            for (PaymentInfoBean paymentInfoBeanTemp : paymentData) {

                if (paymentInfoBeanTemp.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED
                        && !paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {

                    paymentInfoBeanTemp.setDefaultMethod(true);
                    return;
                }
            }

            // Se esiste un metodo di pagamento non verificato si imposta come default e si esce
            for (PaymentInfoBean paymentInfoBeanTemp : paymentData) {

                if (paymentInfoBeanTemp.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED
                        && !paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {

                    paymentInfoBeanTemp.setDefaultMethod(true);
                    return;
                }
            }

            /*
             * // Se esiste un metodo di pagamento pending si imposta come default e si esce
             * for (PaymentInfoBean paymentInfoBeanTemp : paymentData) {
             * 
             * if ( paymentInfoBeanTemp.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING ) {
             * 
             * paymentInfoBeanTemp.setDefaultMethod(true);
             * return;
             * }
             * }
             */
        }
    }

    public PaymentInfoBean findPaymentInfoBean(Long paymentMethodId, String paymentMethodType) {

        PaymentInfoBean paymentInfoBean = null;
        if (!this.getPaymentData().isEmpty()) {

            Set<PaymentInfoBean> paymentData = this.getPaymentData();
            for (PaymentInfoBean paymentInfoBeanTemp : paymentData) {

                if (paymentInfoBeanTemp.getId() == paymentMethodId && paymentInfoBeanTemp.getType().equals(paymentMethodType)) {

                    paymentInfoBean = paymentInfoBeanTemp;
                    break;
                }
            }
        }

        return paymentInfoBean;
    }

    public boolean checkPaymentVoucherType() {

        boolean check = false;
        if (!this.getPaymentData().isEmpty()) {

            Set<PaymentInfoBean> paymentData = this.getPaymentData();
            for (PaymentInfoBean paymentInfoBeanTemp : paymentData) {

                if (paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {
                    check = true;
                    break;
                }
            }
        }

        return check;
    }

    public PaymentInfoBean updatePin(Long cardId, String cardType, String oldPin, String newPin, Integer pinCheckMaxAttemptsLeft) throws PaymentInfoNotFoundException, PinException {

        PaymentInfoBean paymentInfoBean = this.findPaymentInfoBean(cardId, cardType);

        if (paymentInfoBean != null) {

            String actualPin = paymentInfoBean.getPin();

            if (actualPin == null) {
                throw new PinException(ResponseHelper.USER_PIN_OLD_DB_LESS, "Unable to update pin. Old pin null.", paymentInfoBean.getPinCheckAttemptsLeft());
            }
            else {
                Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();

                if (!actualPin.equals(oldPin) || pinCheckAttemptsLeft == 0) {

                    // Si sottrae uno al numero di tentativi residui

                    if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                        pinCheckAttemptsLeft--;
                    }
                    else {
                        pinCheckAttemptsLeft = 0;
                    }

                    if (pinCheckAttemptsLeft == 0) {

                        // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                        paymentInfoBean.setDefaultMethod(false);
                        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
                    }

                    paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

                    throw new PinException(ResponseHelper.USER_PIN_OLD_WRONG, "Unable to update pin. Old pin wrong.", paymentInfoBean.getPinCheckAttemptsLeft());
                }

                String encodedNewPin = EncoderHelper.encode(newPin);

                if (actualPin.equals(encodedNewPin)) {

                    throw new PinException(ResponseHelper.USER_PIN_OLD_NEW_EQUALS, "Unable to update pin. Old pin and new pin must be different.",
                            paymentInfoBean.getPinCheckAttemptsLeft());
                }

                String checkPin = PinHelper.checkPin(newPin, null);
                if (!checkPin.equals("OK")) {

                    throw new PinException(ResponseHelper.USER_PIN_NOT_STRONG, "Unable to update pin. Pin not strong.", paymentInfoBean.getPinCheckAttemptsLeft());
                }

                paymentInfoBean.setPin(encodedNewPin);
                paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttemptsLeft);

                return paymentInfoBean;
            }
        }
        else {
            throw new PaymentInfoNotFoundException("Unable to find PaymentInfo object for cardId " + cardId + " and cardType " + cardType);
        }
    }

    public PaymentInfoBean updatePinNew(PaymentInfoBean paymentInfoBean, String oldPin, String newPin, Integer pinCheckMaxAttemptsLeft) throws PaymentInfoNotFoundException,
            PinException {

        if (paymentInfoBean != null) {

            String actualPin = paymentInfoBean.getPin();

            if (actualPin == null) {
                throw new PinException(ResponseHelper.USER_PIN_OLD_DB_LESS, "Unable to update pin. Old pin null.", paymentInfoBean.getPinCheckAttemptsLeft());
            }
            else {

                if (!actualPin.equals(oldPin)) {

                    // Il pin inserito non � valido

                    String response = ResponseHelper.USER_PIN_OLD_WRONG;

                    // Si sottrae uno al numero di tentativi residui
                    Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
                    if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                        pinCheckAttemptsLeft--;
                    }
                    else {
                        pinCheckAttemptsLeft = 0;
                    }

                    if (pinCheckAttemptsLeft == 0) {

                        // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                        paymentInfoBean.setDefaultMethod(false);
                        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
                    }
                    else {

                        if (pinCheckAttemptsLeft == 1) {

                            response = ResponseHelper.USER_PIN_ERROR_PIN_LAST_ATTEMPT;
                        }
                        else {

                            response = ResponseHelper.USER_PIN_ERROR_PIN_ATTEMPTS_LEFT;
                        }
                    }

                    paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

                    throw new PinException(response, "Unable to update pin. Old pin wrong.", paymentInfoBean.getPinCheckAttemptsLeft());
                }

                /*
                 * Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
                 * 
                 * if (!actualPin.equals(oldPin) || pinCheckAttemptsLeft == 0) {
                 * 
                 * // Si sottrae uno al numero di tentativi residui
                 * 
                 * if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                 * pinCheckAttemptsLeft--;
                 * }
                 * else {
                 * pinCheckAttemptsLeft = 0;
                 * }
                 * 
                 * if (pinCheckAttemptsLeft == 0) {
                 * 
                 * // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                 * paymentInfoBean.setDefaultMethod(false);
                 * paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
                 * }
                 * 
                 * paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);
                 * 
                 * throw new PinException(ResponseHelper.USER_PIN_OLD_WRONG, "Unable to update pin. Old pin wrong.", paymentInfoBean.getPinCheckAttemptsLeft());
                 * }
                 */

                String encodedNewPin = EncoderHelper.encode(newPin);

                if (actualPin.equals(encodedNewPin)) {

                    throw new PinException(ResponseHelper.USER_PIN_OLD_NEW_EQUALS, "Unable to update pin. Old pin and new pin must be different.",
                            paymentInfoBean.getPinCheckAttemptsLeft());
                }

                String checkPin = PinHelper.checkPin(newPin, null);
                if (!checkPin.equals("OK")) {

                    throw new PinException(ResponseHelper.USER_PIN_NOT_STRONG, "Unable to update pin. Pin not strong.", paymentInfoBean.getPinCheckAttemptsLeft());
                }

                paymentInfoBean.setPin(encodedNewPin);
                paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttemptsLeft);

                return paymentInfoBean;
            }
        }
        else {
            throw new PaymentInfoNotFoundException("PaymentInfo null");
        }
    }
    
    public String getPaymentEncodedPin() {
        
        String encodedPin = null;
        
        if (!this.getPaymentData().isEmpty()) {

            Set<PaymentInfoBean> paymentData = this.getPaymentData();
            for (PaymentInfoBean paymentInfoBean : paymentData) {

                if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER) && paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                    encodedPin = paymentInfoBean.getPin();
                    break;
                }
            }
        }
        
        return encodedPin;
    }

    public LoyaltyCardBean getFirstLoyaltyCard() {

        LoyaltyCardBean loyaltyCardBean = null;

        for (LoyaltyCardBean loyaltyCardBeanTemp : this.loyaltyCardList) {

            loyaltyCardBean = loyaltyCardBeanTemp;
        }

        return loyaltyCardBean;
    }

    public LoyaltyCardBean getVirtualLoyaltyCard() {

        LoyaltyCardBean loyaltyCardBean = null;

        for (LoyaltyCardBean loyaltyCardBeanTemp : this.loyaltyCardList) {

            if (loyaltyCardBeanTemp.getCardType() != null && loyaltyCardBeanTemp.getCardType().equals("V")
                    || (loyaltyCardBeanTemp.getIsVirtual() != null && loyaltyCardBeanTemp.getIsVirtual())) {
                loyaltyCardBean = loyaltyCardBeanTemp;
                break;
            }
        }

        return loyaltyCardBean;
    }

    public Boolean getUseVoucher() {
        if (useVoucher == null)
            return false;
        if (userType == User.USER_TYPE_REFUELING) {
            System.out.println("Utente refuelling: forzatura useVoucher a false");
            return false;
        }

        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }

    public PaymentInfoBean getVoucherPaymentMethod() {

        PaymentInfoBean paymentInfoBean = null;
        if (!this.getPaymentData().isEmpty()) {

            Set<PaymentInfoBean> paymentData = this.getPaymentData();
            for (PaymentInfoBean paymentInfoBeanTemp : paymentData) {

                if (paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)
                        && paymentInfoBeanTemp.getStatus().equals(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                    paymentInfoBean = paymentInfoBeanTemp;
                    break;
                }
            }
        }

        return paymentInfoBean;
    }

    public List<PaymentInfoBean> getPaymentMethodTypeCreditCardList() {

        List<PaymentInfoBean> paymentInfoBeanList = new ArrayList<>();
        
        if (!this.getPaymentData().isEmpty()) {

            for (PaymentInfoBean paymentInfoBean : getPaymentData()) {

                if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                        && paymentInfoBean.getStatus().equals(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                    paymentInfoBeanList.add(paymentInfoBean);
                    break;
                }
            }
        }

        return paymentInfoBeanList;
    }

    public PaymentInfoBean getPaymentMethodTypeCreditCard() {

        PaymentInfoBean paymentInfoBean = null;
        if (!this.getPaymentData().isEmpty()) {

            for (PaymentInfoBean paymentInfoBeanTemp : getPaymentData()) {

                if (paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                        && paymentInfoBeanTemp.getStatus().equals(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                    paymentInfoBean = paymentInfoBeanTemp;
                    break;
                }
            }
        }

        return paymentInfoBean;
    }

    public String getEncodedPin() {

        String encodedPin = "";

        PaymentInfoBean paymentInfoBean = this.getVoucherPaymentMethod();

        if (paymentInfoBean != null) {

            encodedPin = paymentInfoBean.getPin();
        }

        return encodedPin;
    }

    public Boolean paymentCardExists() {

        PaymentInfoBean paymentInfoBean = null;
        if (!this.getPaymentData().isEmpty()) {

            Set<PaymentInfoBean> paymentData = this.getPaymentData();
            for (PaymentInfoBean paymentInfoBeanTemp : paymentData) {

                if (paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                        && (paymentInfoBeanTemp.getStatus().equals(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) || paymentInfoBeanTemp.getStatus().equals(
                                PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED))) {

                    paymentInfoBean = paymentInfoBeanTemp;
                    break;
                }
            }
        }

        return (paymentInfoBean != null);
    }

    public MobilePhoneBean activeMobilePhone() {
        MobilePhoneBean mobilePhoneBean = null;
        for (MobilePhoneBean item : this.mobilePhoneList) {
            if (item.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE)) {
                mobilePhoneBean = item;
                break;
            }
        }

        return mobilePhoneBean;
    }

    public String getSocialProvider() {
        String providerFound = null;
        if (!this.getUserSocialData().isEmpty()) {
            for (UserSocialDataBean userSocialDataBean : this.getUserSocialData()) {
                providerFound = userSocialDataBean.getProvider();
                break;
            }
        }

        return providerFound;
    }
    
    public String getExternalProvider() {
        String providerFound = null;
        if (!this.getUserExternalData().isEmpty()) {
            for (UserExternalDataBean userExternalDataBean : this.getUserExternalData()) {
                providerFound = userExternalDataBean.getProvider();
                break;
            }
        }

        return providerFound;
    }

    public Integer getVirtualizationAttemptsLeft() {
        return virtualizationAttemptsLeft;
    }

    public void setVirtualizationAttemptsLeft(Integer virtualizationAttemptsLeft) {
        this.virtualizationAttemptsLeft = virtualizationAttemptsLeft;
    }

    public Boolean getEniStationUserType() {
        return eniStationUserType;
    }

    public void setEniStationUserType(Boolean eniStationUserType) {
        this.eniStationUserType = eniStationUserType;
    }

    public Boolean getDepositCardStepCompleted() {
        return depositCardStepCompleted;
    }

    public void setDepositCardStepCompleted(Boolean depositCardStepCompleted) {
        this.depositCardStepCompleted = depositCardStepCompleted;
    }

    public Date getDepositCardStepTimestamp() {
        return depositCardStepTimestamp;
    }

    public void setDepositCardStepTimestamp(Date depositCardStepTimestamp) {
        this.depositCardStepTimestamp = depositCardStepTimestamp;
    }

    public Date getUserStatusRegistrationTimestamp() {
        return userStatusRegistrationTimestamp;
    }

    public void setUserStatusRegistrationTimestamp(Date userStatusRegistrationTimestamp) {
        this.userStatusRegistrationTimestamp = userStatusRegistrationTimestamp;
    }

    public Set<UserSocialDataBean> getUserSocialData() {
        return userSocialData;
    }

    public void setUserSocialData(Set<UserSocialDataBean> userSocialData) {
        this.userSocialData = userSocialData;
    }

    public Set<PlateNumberBean> getPlateNumberList() {
        return plateNumberList;
    }

    public void setPlateNumberList(Set<PlateNumberBean> plateNumberList) {
        this.plateNumberList = plateNumberList;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Set<PersonalDataBusinessBean> getPersonalDataBusinessList() {
        return personalDataBusinessList;
    }

    public void setPersonalDataBusinessList(Set<PersonalDataBusinessBean> personalDataBusinessList) {
        this.personalDataBusinessList = personalDataBusinessList;
    }
    
    public Boolean getLandingDisplayed() {
        if (landingDisplayed == null)
            return Boolean.FALSE;
        return landingDisplayed;
    }

    public void setLandingDisplayed(Boolean landingDisplayed) {
        this.landingDisplayed = landingDisplayed;
    }

    public Set<UserExternalDataBean> getUserExternalData() {
        return userExternalData;
    }

    public void setUserExternalData(Set<UserExternalDataBean> userExternalData) {
        this.userExternalData = userExternalData;
    }

    public String getActiveMcCardDpan() {
        return activeMcCardDpan;
    }

    public void setActiveMcCardDpan(String activeMcCardDpan) {
        this.activeMcCardDpan = activeMcCardDpan;
    }
    
    public String getActiveMobilePhone(){
    	String activeNumber="";
    	
    	for(MobilePhoneBean ele:mobilePhoneList){
    		if (ele.getStatus()==1){
    			activeNumber=ele.getNumber();
    			break;
    		}
    	}
    	
    	return activeNumber;
    }

	public List<CRMSfContactKeyBean> getCrmSfContactKey() {
		return crmSfContactKey;
	}

	public void setCrmSfContactKey(List<CRMSfContactKeyBean> crmSfContactKey) {
		this.crmSfContactKey = crmSfContactKey;
	}

	
}
