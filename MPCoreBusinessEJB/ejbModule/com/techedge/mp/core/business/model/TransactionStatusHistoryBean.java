package com.techedge.mp.core.business.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.TransactionStatusHistory;

@Entity
@Table(name="TRANSACTION_STATUS_HISTORY")
@NamedQueries({
	@NamedQuery(name="TransactionStatusHistoryBean.findTransactionStatusByTransaction", query="select t from TransactionStatusHistoryBean t where t.transactionHistoryBean = :transactionHistoryBean")
})

	public class TransactionStatusHistoryBean {
	
	public static final String FIND_BY_TRANSACTION = "TransactionStatusHistoryBean.findTransactionStatusByTransaction";
	public static final String FIND_BY_TRANSACTION_AND_STATUS_ID = "TransactionStatusHistoryBean.findTransactionStatusByTransactionAndStatusID";

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transaction_id", nullable = false)
	private TransactionHistoryBean transactionHistoryBean;
	
	@Column(name="sequence_id", nullable=true)
	private Integer sequenceID;
	
	@Column(name="timestamp", nullable=true)
	private Timestamp timestamp;
	
	@Column(name="status", nullable=true)
	private String status;
	
	@Column(name="sub_status", nullable=true)
	private String subStatus;
	
	@Column(name="sub_status_description", nullable=true)
	private String subStatusDescription;
	
	@Column(name="request_id", nullable=true)
	private String requestID;
	
	
	public TransactionStatusHistoryBean() {	}
	
	
	public TransactionStatusHistoryBean(TransactionStatusHistory transactionStatus) {
		
		this.id = transactionStatus.getId();
		this.sequenceID = transactionStatus.getSequenceID();
		this.timestamp = transactionStatus.getTimestamp();
		this.status = transactionStatus.getStatus();
		this.subStatus = transactionStatus.getSubStatus();
		this.subStatusDescription = transactionStatus.getSubStatusDescription();
		this.requestID = transactionStatus.getRequestID();
	}
	
	
	public TransactionStatusHistoryBean(TransactionStatusBean transactionStatus) {
		
		this.transactionHistoryBean = null;
		this.sequenceID = transactionStatus.getSequenceID();
		this.timestamp = transactionStatus.getTimestamp();
		this.status = transactionStatus.getStatus();
		this.subStatus = transactionStatus.getSubStatus();
		this.subStatusDescription = transactionStatus.getSubStatusDescription();
		this.requestID = transactionStatus.getRequestID();
	}
	
	
	public TransactionStatusHistory toTransactionStatus() {
		
		TransactionStatusHistory transactionStatus = new TransactionStatusHistory();
		transactionStatus.setId(this.id);
		transactionStatus.setSequenceID(this.sequenceID);
		transactionStatus.setTimestamp(this.timestamp);
		transactionStatus.setStatus(this.status);
		transactionStatus.setSubStatus(this.subStatus);
		transactionStatus.setSubStatusDescription(this.subStatusDescription);
		transactionStatus.setRequestID(this.requestID);
		transactionStatus.setTransactionHistory(transactionHistoryBean.toTransactionHistory());
		return transactionStatus;
	}
    

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public TransactionHistoryBean getTransactionHistoryBean() {
		return transactionHistoryBean;
	}
	public void setTransactionHistoryBean(TransactionHistoryBean transactionBean) {
		this.transactionHistoryBean = transactionBean;
	}

	public Integer getSequenceID() {
		return sequenceID;
	}
	public void setSequenceID(Integer sequenceID) {
		this.sequenceID = sequenceID;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubStatus() {
		return subStatus;
	}
	
	public String getSubStatusDescription() {
		return subStatusDescription;
	}
	public void setSubStatusDescription(String subStatusDescription) {
		this.subStatusDescription = subStatusDescription;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
}
