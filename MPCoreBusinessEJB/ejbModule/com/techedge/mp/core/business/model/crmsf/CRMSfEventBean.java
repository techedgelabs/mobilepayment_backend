package com.techedge.mp.core.business.model.crmsf;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.json.JSONObject;

import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;

@Entity
@Table(name = "CRM_SF_EVENT")
@NamedQueries({
    @NamedQuery(name = "CRMSfEventBean.SelectByRequestId", query = "select t from CRMSfEventBean t where requestId = :requestId"),
    @NamedQuery(name = "CRMSfEventBean.findToReconcilie", query = "select e from CRMSfEventBean e where e.toReconcile = 1")
})
public class CRMSfEventBean implements UserReconciliationInterfaceBean {

	public static final String SELECT_BY_REQUEST_ID = "CRMSfEventBean.SelectByRequestId";
	public static final String FIND_TO_RECONCILIE   = "CRMSfEventBean.findToReconcilie";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private UserBean userBean;

	@Column(name = "request_id", nullable = false)
	private String requestId;

	@Column(name = "request_timestamp")
	private Date requestTimestamp;

	@Column(name = "request_obj")
	private String requestJson;

	@Column(name = "success")
	private boolean success;

	@Column(name = "error_code", nullable = false)
	private String errorCode;

	@Column(name = "message", nullable = false)
	private String message;

	@Column(name = "to_reconcile")
	private boolean toReconcile;

	@Column(name = "reconciliation_attempts_left", nullable = false)
	private Integer reconciliationAttemptsLeft;

	public CRMSfEventBean() {
	}

	public CRMSfEventBean(UserBean userBean, String requestId, Date requestTimestamp,
			String requestJson, boolean success, String errorCode,
			String message, boolean toReconcile,
			Integer reconciliationAttemptsLeft) {
		this.userBean = userBean;
		this.requestId = requestId;
		this.requestTimestamp = requestTimestamp;
		this.requestJson = requestJson;
		this.success = success;
		this.errorCode = errorCode;
		this.message = message;
		this.toReconcile = toReconcile;
		this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
	}

	public static CRMSfEventBean crateCrmSfEventBean(
			NotifyEventResponse notifyEventResponse, String requestId,
			String fiscalCode, Date date, String stationId, String productId,
			Boolean paymentFlag, Integer credits, Double quantity,
			String refuelMode, String firstName, String lastName, String email,
			Date birthDate, Boolean notificationFlag, Boolean paymentCardFlag,
			String brand, String cluster, Double amount, Boolean privacyFlag1,
			Boolean privacyFlag2, String mobilePhone, String loyaltyCard,
			String eventType, String parameter1, String parameter2, String paymentMode,
			UserBean userBean) {

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("requestId", requestId);
		jsonObj.put("fiscalCode", fiscalCode);
		jsonObj.put("date", date);
		jsonObj.put("stationId", stationId);
		jsonObj.put("productId", productId);
		jsonObj.put("paymentFlag", paymentFlag);
		jsonObj.put("credits", credits);
		jsonObj.put("quantity", quantity);
		jsonObj.put("refuelMode", refuelMode);
		jsonObj.put("firstName", firstName);
		jsonObj.put("lastName", lastName);
		jsonObj.put("email", email);
		jsonObj.put("birthDate", birthDate);
		jsonObj.put("notificationFlag", notificationFlag);
		jsonObj.put("paymentCardFlag", paymentCardFlag);
		jsonObj.put("brand", brand);
		jsonObj.put("cluster", cluster);
		jsonObj.put("amount", amount);
		jsonObj.put("privacyFlag1", privacyFlag1);
		jsonObj.put("privacyFlag2", privacyFlag2);
		jsonObj.put("mobilePhone", mobilePhone);
		jsonObj.put("loyaltyCard", loyaltyCard);
		jsonObj.put("eventType", eventType);
		jsonObj.put("parameter1", parameter1);
		jsonObj.put("parameter2", parameter2);
		jsonObj.put("paymentMode", paymentMode);
		jsonObj.put("userId", userBean.getId());

		CRMSfEventBean result = new CRMSfEventBean(userBean, requestId, new Date(),
				jsonObj.toString(), notifyEventResponse.getSuccess(),
				notifyEventResponse.getErrorCode(),
				notifyEventResponse.getMessage(), !notifyEventResponse.getSuccess(), 5);
		
		return result;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public Date getRequestTimestamp() {
		return requestTimestamp;
	}

	public void setRequestTimestamp(Date requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	public String getRequestJson() {
		return requestJson;
	}

	public void setRequestJson(String requestJson) {
		this.requestJson = requestJson;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isToReconcile() {
		return toReconcile;
	}

	public void setToReconcile(boolean toReconcile) {
		this.toReconcile = toReconcile;
	}

	public Integer getReconciliationAttemptsLeft() {
		return reconciliationAttemptsLeft;
	}

	public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
		this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
	}

	
    @Override
    public UserBean getUserBean() {
        return this.userBean;
    }

    @Override
    public String getFinalStatus() {
        if (this.success) {
            return "SUCCESS";
        }
        else {
            return this.errorCode;
        }
    }

    @Override
    public void setFinalStatus(String status) {
        if (status.equals("SUCCESS")) {
            this.success = true;
        }
        else {
            this.success = false;
            this.errorCode = status;
        }
    }

    @Override
    public void setToReconcilie(Boolean toReconcilie) {
        this.toReconcile = toReconcilie;
    }

    @Override
    public Boolean getToReconcilie() {
        return toReconcile;
    }

    @Override
    public String getOperationName() {
        return "notifyEvent";
    }

    @Override
    public Date getOperationTimestamp() {
        return requestTimestamp;
    }

    @Override
    public String getFinalStatusMessage() {
        return this.message;
    }

    @Override
    public String getServerEndpoint() {
        return "CRM SF";
    }

}
