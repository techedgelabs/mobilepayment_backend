package com.techedge.mp.core.business.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.LandingData;
import com.techedge.mp.core.business.interfaces.user.UserCategory;
import com.techedge.mp.core.business.interfaces.user.UserType;

@Entity
@Table(name = "USER_CATEGORY")
@NamedQueries({ @NamedQuery(name = "UserCategoryBean.findCategoryByName", query = "select p from UserCategoryBean p where p.name = :name"),
        @NamedQuery(name = "UserCategoryBean.findCategoryById", query = "select p from UserCategoryBean p where p.id = :id"),
        @NamedQuery(name = "UserCategoryBean.findAllCategory", query = "select p from UserCategoryBean p") })
public class UserCategoryBean {

    public static final String   FIND_BY_NAME  = "UserCategoryBean.findCategoryByName";
    public static final String   FIND_BY_ID    = "UserCategoryBean.findCategoryById";
    public static final String   FIND_LIST_ALL = "UserCategoryBean.findAllCategory";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                 id;

    @Column(name = "name", nullable = false, length = 40)
    private String               name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "USERCATEGORIES_USERTYPES")
    private Set<UserTypeBean>    userTypes     = new HashSet<UserTypeBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userCategoryBean", cascade = CascadeType.ALL)
    private Set<LandingDataBean> landingData   = new HashSet<LandingDataBean>(0);

    public UserCategoryBean() {}

    public UserCategoryBean(UserCategory userCategory) {
        this.id = userCategory.getId();
        this.name = userCategory.getName();
        if (userCategory.getUserTypes() != null && !userCategory.getUserTypes().isEmpty()) {

            for (UserType item : userCategory.getUserTypes()) {
                UserTypeBean userTypeBean = new UserTypeBean(item);
                userTypeBean.getUserCategories().add(this);
            }
        }
    }
    
    public UserCategory toUserCategory() {
        UserCategory userCategory = new UserCategory();
        
        userCategory.setId(this.id);
        userCategory.setName(this.name);
        
        if (this.userTypes != null && !this.userTypes.isEmpty()) {

            for (UserTypeBean userTypeBean : this.userTypes) {
                
                UserType userType = new UserType();
                
                userType.setId(userTypeBean.getId());
                userType.setCode(userTypeBean.getCode());
                
                userCategory.getUserTypes().add(userType);
            }
        }
        
        return userCategory;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserTypeBean> getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(Set<UserTypeBean> userTypes) {
        this.userTypes = userTypes;
    }

    public Set<LandingDataBean> getLandingData() {
        return landingData;
    }

    public void setLandingData(Set<LandingDataBean> landingData) {
        this.landingData = landingData;
    }

}
