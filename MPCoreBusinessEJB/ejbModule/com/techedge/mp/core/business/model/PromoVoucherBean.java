package com.techedge.mp.core.business.model;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.exceptions.PromoVoucherException;
import com.techedge.mp.core.business.interfaces.PromotionVoucherStatus;
import com.techedge.mp.core.business.interfaces.user.User;

@Entity
@Table(name = "PROMOTIONVOUCHER")
@NamedQueries({
        @NamedQuery(name = "PromoVoucherBean.findPromotionByVoucherCode", query = "select p from PromoVoucherBean pv, PromotionBean p where pv.code = :code AND pv.promotionBean = p.id"),

        @NamedQuery(name = "PromoVoucherBean.findByUser", query = "select pv from PromoVoucherBean pv where pv.userBean = :user"),

        @NamedQuery(name = "PromoVoucherBean.findByUserAndStatusNotConsumed", query = "select pv from PromoVoucherBean pv where (pv.status = 2 or pv.status = 4) "
                + "and pv.userBean = :userBean"),

        @NamedQuery(name = "PromoVoucherBean.findNewByPromotion", query = "select pv from PromoVoucherBean pv where pv.promotionBean = :promotionBean "
                + "and pv.status = 1 and pv.userBean = null"),

        @NamedQuery(name = "PromoVoucherBean.findByUserAndVerificationValue", query = "select pv from PromoVoucherBean pv where pv.promotionBean = :promotionBean "
                + "and (pv.verificationValue = :verificationValue or (pv.userBean != null and pv.userBean = :userBean))"),

        @NamedQuery(name = "PromoVoucherBean.findByUserAndPromotion", query = "select pv from PromoVoucherBean pv where pv.promotionBean = :promotionBean "
                + "and pv.userBean = :userBean))"),

        @NamedQuery(name = "PromoVoucherBean.statisticsReportSynthesis", query = "select "
                + "(select count(pv1.id) from PromoVoucherBean pv1 where (pv1.status = 3 or pv1.status = 4) and pv1.promotionBean = :promotionBean "
                + "and (pv1.insertedTimestamp >= :dailyStartDate and pv1.insertedTimestamp < :dailyEndDate)), "
                + "(select count(pv2.id) from PromoVoucherBean pv2 where (pv2.status = 3 or pv2.status = 4) and pv2.promotionBean = :promotionBean "
                + "and (pv2.insertedTimestamp >= :weeklyStartDate and pv2.insertedTimestamp < :weeklyEndDate)), "
                + "(select count(pv3.id) from PromoVoucherBean pv3 where (pv3.status = 3 or pv3.status = 4) and pv3.promotionBean = :promotionBean and pv3.insertedTimestamp <= :dailyEndDate) from PromoVoucherBean pv"),

        @NamedQuery(name = "PromoVoucherBean.findByVoucherCode", query = "select pv from PromoVoucherBean pv where pv.code = :voucherCode"),

        @NamedQuery(name = "PromoVoucherBean.findByDateAndStatusConsumed", query = "select pv from PromoVoucherBean pv where pv.status = 3 and pv.promotionBean != null "
                + "and pv.userBean != null and insertedTimestamp <= :insertedTimestamp"),

        @NamedQuery(name = "PromoVoucherBean.findByStatusConsumed", query = "select pv from PromoVoucherBean pv where pv.status = 3 and pv.promotionBean != null and pv.userBean != null"),

        @NamedQuery(name = "PromoVoucherBean.findVoucherNotAssigned", query = "select pv from PromoVoucherBean pv where pv.status = 1 and pv.promotionBean != null and pv.userBean = null") })
public class PromoVoucherBean {

    public static final String FIND_PROMOTION_BY_VOUCHER_CODE       = "PromoVoucherBean.findPromotionByVoucherCode";
    public static final String FIND_BY_USER                         = "PromoVoucherBean.findByUser";
    public static final String FIND_BY_USER_AND_STATUS_NOT_CONSUMED = "PromoVoucherBean.findByUserAndStatusNotConsumed";
    public static final String FIND_NEW_BY_PROMOTION                = "PromoVoucherBean.findNewByPromotion";
    public static final String FIND_BY_VOUCHER_CODE                 = "PromoVoucherBean.findByVoucherCode";
    public static final String FIND_BY_USER_AND_VERIFICATION_VALUE  = "PromoVoucherBean.findByUserAndVerificationValue";
    public static final String FIND_BY_DATE_AND_STATUS_CONSUMED     = "PromoVoucherBean.findByDateAndStatusConsumed";
    public static final String FIND_BY_STATUS_CONSUMED              = "PromoVoucherBean.findByStatusConsumed";
    public static final String FIND_VOUCHER_NOT_ASSIGNED            = "PromoVoucherBean.findVoucherNotAssigned";
    public static final String STATISTICS_REPORT_SYNTHESIS          = "PromoVoucherBean.statisticsReportSynthesis";
    public static final String FIND_BY_USER_AND_PROMOTION           = "PromoVoucherBean.findByUserAndPromotion";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "code", nullable = false, length = 40)
    private String             code;

    @Column(name = "value", nullable = false)
    private Double             value;

    @Column(name = "status", nullable = false)
    private Integer            status;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private UserBean           userBean;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "promotion_id", nullable = false)
    private PromotionBean      promotionBean;

    @Column(name = "inserted_timestamp")
    private Date               insertedTimestamp;

    @Column(name = "verification_value")
    private String             verificationValue;

    public PromoVoucherBean() {}

    public PromoVoucherBean(PromoVoucherBean promoVoucher) {
        this.id = promoVoucher.getId();
        this.code = promoVoucher.getCode();
        this.value = promoVoucher.getValue();
        this.status = promoVoucher.getStatus();
        this.userBean = promoVoucher.getUserBean();
        this.promotionBean = promoVoucher.getPromotionBean();
        this.insertedTimestamp = promoVoucher.getInsertedTimestamp();
        this.verificationValue = promoVoucher.getVerificationValue();
    }

    public PromoVoucherBean(long id, String code, Double value, Integer status, UserBean userBean, PromotionBean promotionBean) {
        this.id = id;
        this.code = code;
        this.value = value;
        this.status = status;
        this.userBean = userBean;
        this.promotionBean = promotionBean;
    }

    public PromoVoucherBean(String code, Double value) {
        this.code = code;
        this.value = value;
        //1 = new, 2 = assigned; 3 = consumed, 4 = deleted
        //Crea costante/enumerato per gli stati
        this.status = PromotionVoucherStatus.NEW.getValue();
        this.insertedTimestamp = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public PromotionBean getPromotionBean() {
        return promotionBean;
    }

    public void setPromotionBean(PromotionBean promotionBean) {
        this.promotionBean = promotionBean;
    }

    public Date getInsertedTimestamp() {
        return insertedTimestamp;
    }

    public void setInsertedTimestamp(Date insertedTimestamp) {
        this.insertedTimestamp = insertedTimestamp;
    }

    public String getVerificationValue() {
        return verificationValue;
    }

    public void setVerificationValue(String verificationValue) {
        this.verificationValue = verificationValue;
    }

    /**
     * Associa un voucher ad una promozione
     * 
     * @param voucherCode
     *            codice del Voucher
     * @param voucherValue
     *            valore del Voucher
     * @param promotionBean
     *            Promozione a cui viene associato il voucher
     * @throws PromoVoucherException
     */
    public void create(String voucherCode, Double voucherValue, PromotionBean promotionBean) throws PromoVoucherException {

        if (voucherCode != null) {

            setCode(voucherCode);
        }
        else {

            throw new PromoVoucherException("Voucher code is null");
        }

        if (voucherValue != null) {

            setValue(voucherValue);
        }
        else {

            throw new PromoVoucherException("Voucher value is null");
        }

        if (promotionBean != null) {

            Set<String> setPromoVoucher = getPromoVoucherListCode(promotionBean.getPromoVoucherList());
            if (!setPromoVoucher.contains(voucherCode)) {

                promotionBean.getPromoVoucherList().add(this);
                this.setPromotionBean(promotionBean);
            }
            else {
                //esistone gi� una promozione con quel codice
                throw new PromoVoucherException("Voucher with same code " + voucherCode + " already exist in promotion");
            }

        }
        else {

            throw new PromoVoucherException("Promotion is null");
        }

        setStatus(PromotionVoucherStatus.NEW.getValue());

        setInsertedTimestamp(new Date());
    }

    /**
     * Converto la lista di promoVoucher in un Set<String> dove la String � il codice di PromoVoucher
     * 
     * @param promoVoucherList
     *            lista dei promoVoucher associati alla promozione
     * @return Set<String> insieme di code dei PromoVoucher
     */
    private Set<String> getPromoVoucherListCode(List<PromoVoucherBean> promoVoucherList) {

        Set<String> key = new HashSet<String>();
        for (PromoVoucherBean promoVoucher : promoVoucherList) {
            key.add(promoVoucher.getCode());
        }

        return key;
    }

    /**
     * Associa il voucher ad un utente, setta lo stato del voucher a Assegnato
     * e inserisce il dato inserito dall'utente per accedere alla promozione
     * 
     * @param userBean
     *            Utente a cui assegnare il voucher
     * @param verificationValue
     *            dato insertito dall'utente per accedere alla promozione
     */
    public void assignUser(UserBean userBean, String verificationValue) {
        
        if (this.status == PromotionVoucherStatus.NEW.getValue()) {
            if (userBean != null && userBean.getUserStatus() == User.USER_STATUS_VERIFIED) {

                this.status = PromotionVoucherStatus.ASSIGNED.getValue();
                this.userBean = userBean;
                this.verificationValue = verificationValue;
                this.insertedTimestamp = new Date();
            }
        }
    }

    /**
     * Metto il voucher in stato consumed
     * 
     * @param userBean
     *            Utente che "consuma" il voucher
     * @throws PromoVoucherException
     */
    public void consume(UserBean userBean) throws PromoVoucherException {
        
        if (this.status == PromotionVoucherStatus.ASSIGNED.getValue()) {
            if (userBean.equals(this.userBean)) {

                this.status = PromotionVoucherStatus.CONSUMED.getValue();
            }
            else {

                throw new PromoVoucherException("User is not the same");
            }
        }

    }

    /**
     * Metto il voucher in stato sent
     * 
     * @throws PromoVoucherException
     */
    public void sent() throws PromoVoucherException {

        if (this.status == PromotionVoucherStatus.CONSUMED.getValue()) {
            this.status = PromotionVoucherStatus.SENT.getValue();
        }
        else {
            throw new PromoVoucherException("Status is not in consumed state");
        }
    }

    /**
     * Setta lo stato del voucher a deleted
     */
    public void delete() {

        this.status = PromotionVoucherStatus.DELETED.getValue();
    }

}
