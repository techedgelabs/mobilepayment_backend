package com.techedge.mp.core.business.model.postpaid;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidTransactionEvent;

@Entity
@Table(name = "POSTPAIDTRANSACTIONEVENT")
//@NamedQueries({
//	@NamedQuery(name="TransactionBean.findTransactionByID", query="select t from TransactionBean t where t.transactionID = :transactionID"),
//	@NamedQuery(name="TransactionBean.findTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean"),
//	@NamedQuery(name="TransactionBean.findTransactionByUserAndDate", query="select t from TransactionBean t where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),
//	@NamedQuery(name="TransactionBean.findTransactionSuccessfulByUserAndDate", query="select t from TransactionBean t where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) and t.finalStatusType = 'SUCCESSFUL' order by t.creationTimestamp desc"),
//	//@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and (( t.finalStatusType is null ) or ( t.finalStatusType <> 'FAILED' and t.finalStatusType <> 'SUCCESSFUL' and t.finalStatusType <> 'MISSING_NOTIFICATION' ))")
//	@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and t.confirmed = false"),
//	@NamedQuery(name="TransactionBean.findActiveTransaction", query="select t from TransactionBean t where t.confirmed = false"),
//	@NamedQuery(name="TransactionBean.findActiveTransactionNew", query="select t from TransactionBean t where t.finalStatusType = null"),
//	@NamedQuery(name="TransactionBean.findTransactionByFinalStatusType", query="select t from TransactionBean t where t.finalStatusType = :finalStatusType"),
//	@NamedQuery(name="TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus", query="select t from TransactionBean t where t.finalStatusType = :finalStatusType and t.GFGNotification = :gfgNotification"),
//})
public class PostPaidTransactionEventBean {

    //	public static final String FIND_BY_ID = "TransactionBean.findTransactionByID";
    //	public static final String FIND_BY_USER = "TransactionBean.findTransactionByUser";
    //	public static final String FIND_ACTIVE_BY_USER = "TransactionBean.findActiveTransactionByUser";
    //	public static final String FIND_ACTIVE = "TransactionBean.findActiveTransaction";
    //	public static final String FIND_ACTIVE_NEW = "TransactionBean.findActiveTransactionNew";
    //	public static final String FIND_BY_FINAL_STATUS_TYPE = "TransactionBean.findTransactionByFinalStatusType";
    //	public static final String FIND_BY_FINAL_STATUS_TYPE_AND_NOTIFICATION = "TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus";
    //	public static final String FIND_BY_USER_AND_DATE = "TransactionBean.findTransactionByUserAndDate";
    //	public static final String FIND_SUCCESSFUL_BY_USER_AND_DATE = "TransactionBean.findTransactionSuccessfulByUserAndDate";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                    id;

    @Column(name = "sequenceID", nullable = true)
    private Integer                 sequenceID;

    @Column(name = "requestID", nullable = true)
    private String                  requestID;

    @Column(name = "eventTimestamp", nullable = true)
    private Date                    eventTimestamp;

    @Column(name = "event", nullable = true)
    private String                  event;

    @Column(name = "result", nullable = true)
    private String                  result;

    @Column(name = "errorCode", nullable = true)
    private String                  errorCode;

    @Column(name = "errorDescription", nullable = true)
    private String                  errorDescription;

    @Column(name = "oldState", nullable = true)
    private String                  oldState;

    @Column(name = "newState", nullable = true)
    private String                  newState;

    @Column(name = "stateType", nullable = true)
    private String                  stateType;        // Standard/Reconciliation

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactionID", nullable = false)
    private PostPaidTransactionBean ppTransactionBean;

    public PostPaidTransactionEventBean() {}

    public PostPaidTransactionEventBean(PostPaidTransactionEvent postPaidTransactionEvent) {

        this.id = postPaidTransactionEvent.getId();
        this.sequenceID = postPaidTransactionEvent.getSequenceID();
        this.requestID = postPaidTransactionEvent.getRequestID();
        this.eventTimestamp = postPaidTransactionEvent.getEventTimestamp();
        this.event = postPaidTransactionEvent.getEvent();
        this.result = postPaidTransactionEvent.getResult();
        this.errorCode = postPaidTransactionEvent.getErrorCode();
        this.errorDescription = postPaidTransactionEvent.getErrorDescription();
        this.oldState = postPaidTransactionEvent.getOldState();
        this.newState = postPaidTransactionEvent.getNewState();
        this.stateType = postPaidTransactionEvent.getStateType();

    }
    
    public PostPaidTransactionEventBean(PostPaidTransactionEventHistoryBean postPaidTransactionEventBean) {
        this.sequenceID = postPaidTransactionEventBean.getSequenceID();
        this.requestID = postPaidTransactionEventBean.getRequestID();
        this.eventTimestamp = postPaidTransactionEventBean.getEventTimestamp();
        this.event = postPaidTransactionEventBean.getEvent();
        this.result = postPaidTransactionEventBean.getResult();
        this.errorCode = postPaidTransactionEventBean.getErrorCode();
        this.errorDescription = postPaidTransactionEventBean.getErrorDescription();
        this.oldState = postPaidTransactionEventBean.getOldState();
        this.newState = postPaidTransactionEventBean.getNewState();
        this.stateType = postPaidTransactionEventBean.getStateType();

    }

    public PostPaidTransactionEvent toPostPaidTransactionEvent() {

        PostPaidTransactionEvent postPaidTransactionEvent = new PostPaidTransactionEvent();
        postPaidTransactionEvent.setId(this.id);
        postPaidTransactionEvent.setSequenceID(this.sequenceID);
        postPaidTransactionEvent.setRequestID(this.requestID);
        postPaidTransactionEvent.setEventTimestamp(this.eventTimestamp);
        postPaidTransactionEvent.setEvent(this.errorCode);
        postPaidTransactionEvent.setResult(this.result);
        postPaidTransactionEvent.setErrorCode(this.errorCode);
        postPaidTransactionEvent.setErrorDescription(this.errorDescription);
        postPaidTransactionEvent.setOldState(this.oldState);
        postPaidTransactionEvent.setNewState(this.newState);
        postPaidTransactionEvent.setStateType(this.stateType);
        return postPaidTransactionEvent;

    }

    public Date getEventTimestamp() {
        return eventTimestamp;
    }

    public void setEventTimestamp(Date eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getOldState() {
        return oldState;
    }

    public void setOldState(String oldState) {
        this.oldState = oldState;
    }

    public String getNewState() {
        return newState;
    }

    public void setNewState(String newState) {
        this.newState = newState;
    }

    public String getStateType() {
        return stateType;
    }

    public void setStateType(String stateType) {
        this.stateType = stateType;
    }

    public PostPaidTransactionBean getTransactionBean() {
        return ppTransactionBean;
    }

    public void setTransactionBean(PostPaidTransactionBean ppTransactionBean) {
        this.ppTransactionBean = ppTransactionBean;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public Integer getSequenceID() {
        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {
        this.sequenceID = sequenceID;
    }

}
