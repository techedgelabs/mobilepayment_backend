package com.techedge.mp.core.business.model.crm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.crm.CrmDataElement;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundDeliveryStatusType;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundProcessedStatusType;
import com.techedge.mp.core.business.model.PushNotificationBean;

@Entity
@Table(name = "CRM_OUTBOUND")
@NamedQueries({ @NamedQuery(name = "CRMOutboundBean.findByPushNotificationAndFiscalCode", query = "select cob from CRMOutboundBean cob where (cob.pushNotificationBean != null and "
        + "cob.pushNotificationBean = :pushnotificationbean) and cob.codiceFiscale = :fiscalCode"),
        @NamedQuery(name = "CRMOutboundBean.findNotProcessed", query = "select cob from CRMOutboundBean cob where cob.processed is null or cob.processed = 0"),
        @NamedQuery(name = "CRMOutboundBean.findNotProcessedCount", query = "select count(cob) from CRMOutboundBean cob where cob.processed is null or cob.processed = 0"),
        @NamedQuery(name = "CRMOutboundBean.findNotNotificationSentCount", query = "select count(cob) from CRMOutboundBean cob where cob.processed is null or cob.processed != 2"),
        @NamedQuery(name = "CRMOutboundBean.RowsCount", query = "select count(cob) from CRMOutboundBean cob"),
        @NamedQuery(name = "CRMOutboundBean.SelectAll", query = "select cob from CRMOutboundBean cob"),
        })
public class CRMOutboundBean {

    public static final String FIND_BY_PUSHNOTIFICATION_AND_FISCALCODE = "CRMOutboundBean.findByPushNotificationAndFiscalCode";
    public static final String FIND_NOT_PROCESSED = "CRMOutboundBean.findNotProcessed";
    public static final String FIND_NOT_PROCESSED_COUNT = "CRMOutboundBean.findNotProcessedCount";
    public static final String FIND_NOT_NOTIFICATION_SENT_COUNT = "CRMOutboundBean.findNotNotificationSentCount";
    public static final String ROWS_COUNT = "CRMOutboundBean.RowsCount";
    public static final String SELECT_ALL = "CRMOutboundBean.SelectAll";
    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long              id;

    @Column(name = "cd_cliente")
    private String            cdCliente;
    
    @Column(name = "cd_carta")
    private String            cdCarta;
    
    @Column(name = "nome")
    private String            Nome;
    
    @Column(name = "cognome")
    private String            Cognome;
    
    @Column(name = "te_cellulare")
    private String            telCellulare;
    
    @Column(name = "email")
    private String            email;
    
    @Column(name = "sesso")
    private String            sesso;
    
    @Column(name = "dataNascita")
    private String            dataNascita;
    
    @Column(name = "msg")
    private String            msg;
    
    @Column(name = "canale")
    private String            canale;
    
    @Column(name = "cod_iniziativa")
    private String            codIniziativa;
    
    @Column(name = "cod_offerta")
    private String            codOfferta;
    
    @Column(name = "treatment_code")
    private String            treatmentCode;
    
    @Column(name = "flag_elab")
    private String            flagElab;
    
    @Column(name = "codice_fiscale")
    private String            codiceFiscale;
    
    @Column(name = "c2")
    private String            c2;
    
    @Column(name = "c3")
    private String            c3;
    
    @Column(name = "c4")
    private String            c4;
    
    @Column(name = "c5")
    private String            c5;
    
    @Column(name = "tms_inserimento")
    private String            tmsInserimento;
    
    @Column(name = "obiettivo_punti")
    private String            obiettivoPunti;
    
    @Column(name = "obiettivo_litri")
    private String            obiettivoLitri;
    
    @Column(name = "c6")
    private String            c6;
    
    @Column(name = "c7")
    private String            c7;
    
    @Column(name = "c8")
    private String            c8;
    
    @Column(name = "c9")
    private String            c9;

    @Column(name = "c10")
    private String            c10;
    
    @Column(name = "category_desc")
    public String categoryDesc;

    @Column(name = "delivery_id")
    public String deliveryId;
    
    @Column(name = "delivery_message")
    public String deliveryMessage;
    
    @Column(name = "processed")
    public Integer processed;

    @ManyToOne
    @JoinColumn(name = "push_notification_id")
    private PushNotificationBean          pushNotificationBean;
    
    public CRMOutboundBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCdCliente() {
        return cdCliente;
    }

    public void setCdCliente(String cdCliente) {
        this.cdCliente = cdCliente;
    }

    public String getCdCarta() {
        return cdCarta;
    }

    public void setCdCarta(String cdCarta) {
        this.cdCarta = cdCarta;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getCognome() {
        return Cognome;
    }

    public void setCognome(String cognome) {
        Cognome = cognome;
    }

    public String getTelCellulare() {
        return telCellulare;
    }

    public void setTelCellulare(String telCellulare) {
        this.telCellulare = telCellulare;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public String getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(String dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public String getCodIniziativa() {
        return codIniziativa;
    }

    public void setCodIniziativa(String codIniziativa) {
        this.codIniziativa = codIniziativa;
    }

    public String getCodOfferta() {
        return codOfferta;
    }

    public void setCodOfferta(String codOfferta) {
        this.codOfferta = codOfferta;
    }

    public String getTreatmentCode() {
        return treatmentCode;
    }

    public void setTreatmentCode(String treatmentCode) {
        this.treatmentCode = treatmentCode;
    }

    public String getFlagElab() {
        return flagElab;
    }

    public void setFlagElab(String flagElab) {
        this.flagElab = flagElab;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public String getC2() {
        return c2;
    }

    public void setC2(String c2) {
        this.c2 = c2;
    }

    public String getC3() {
        return c3;
    }

    public void setC3(String c3) {
        this.c3 = c3;
    }

    public String getC4() {
        return c4;
    }

    public void setC4(String c4) {
        this.c4 = c4;
    }

    public String getC5() {
        return c5;
    }

    public void setC5(String c5) {
        this.c5 = c5;
    }

    public String getTmsInserimento() {
        return tmsInserimento;
    }

    public void setTmsInserimento(String tmsInserimento) {
        this.tmsInserimento = tmsInserimento;
    }

    public String getObiettivoPunti() {
        return obiettivoPunti;
    }

    public void setObiettivoPunti(String obiettivoPunti) {
        this.obiettivoPunti = obiettivoPunti;
    }

    public String getObiettivoLitri() {
        return obiettivoLitri;
    }

    public void setObiettivoLitri(String obiettivoLitri) {
        this.obiettivoLitri = obiettivoLitri;
    }

    public String getC6() {
        return c6;
    }

    public void setC6(String c6) {
        this.c6 = c6;
    }

    public String getC7() {
        return c7;
    }

    public void setC7(String c7) {
        this.c7 = c7;
    }

    public String getC8() {
        return c8;
    }

    public void setC8(String c8) {
        this.c8 = c8;
    }

    public String getC9() {
        return c9;
    }

    public void setC9(String c9) {
        this.c9 = c9;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    public CrmOutboundDeliveryStatusType getDeliveryId() {
        return CrmOutboundDeliveryStatusType.getStatusType(deliveryId);
    }

    public void setDeliveryId(CrmOutboundDeliveryStatusType deliveryId) {
        this.deliveryId = deliveryId.getCode();
    }

    public String getDeliveryMessage() {
        return deliveryMessage;
    }

    public void setDeliveryMessage(String deliveryMessage) {
        this.deliveryMessage = deliveryMessage;
    }

    public PushNotificationBean getPushNotificationBean() {
        return pushNotificationBean;
    }

    public void setPushNotificationBean(PushNotificationBean pushNotificationBean) {
        this.pushNotificationBean = pushNotificationBean;
    }
    
    public String getC10() {
        return c10;
    }

    public void setC10(String c10) {
        this.c10 = c10;
    }
    
    public void setProcessed(CrmOutboundProcessedStatusType processed) {
        this.processed = processed.getValue();
    }
    
    public CrmOutboundProcessedStatusType getProcessed() {
        return CrmOutboundProcessedStatusType.getValueType(processed);
    }

    public static CRMOutboundBean generate(CrmDataElement crmDataElement, String categoryDesc, CrmOutboundDeliveryStatusType deliveryStatus, 
            String deliveryMessage, PushNotificationBean pushNotificationBean) {
        
        CRMOutboundBean crmOutboundBean = new CRMOutboundBean();
        
        crmOutboundBean.cdCliente = crmDataElement.cdCliente;
        crmOutboundBean.cdCarta = crmDataElement.cdCarta;
        crmOutboundBean.Nome = crmDataElement.Nome;
        crmOutboundBean.Cognome = crmDataElement.Cognome;
        crmOutboundBean.telCellulare = crmDataElement.telCellulare;
        crmOutboundBean.email = crmDataElement.email;
        crmOutboundBean.sesso = crmDataElement.sesso;
        crmOutboundBean.dataNascita = crmDataElement.dataNascita;
        crmOutboundBean.msg = crmDataElement.msg;
        crmOutboundBean.canale = crmDataElement.canale;
        crmOutboundBean.codIniziativa = crmDataElement.codIniziativa;
        crmOutboundBean.codOfferta = crmDataElement.codOfferta;
        crmOutboundBean.treatmentCode = crmDataElement.treatmentCode;
        crmOutboundBean.flagElab = crmDataElement.flagElab;
        crmOutboundBean.codiceFiscale = crmDataElement.codiceFiscale;
        crmOutboundBean.c2 = crmDataElement.c2;
        crmOutboundBean.c3 = crmDataElement.c3;
        crmOutboundBean.c4 = crmDataElement.c4;
        crmOutboundBean.c5 = crmDataElement.c5;
        
        if (crmDataElement.tmsInserimento != null && crmDataElement.tmsInserimento.length() >= 19 ) {
            crmOutboundBean.tmsInserimento = crmDataElement.tmsInserimento.substring(0, 19);
        }
        
        crmOutboundBean.obiettivoPunti = crmDataElement.obiettivoPunti;
        crmOutboundBean.obiettivoLitri = crmDataElement.obiettivoLitri;
        crmOutboundBean.c6 = crmDataElement.c6;
        crmOutboundBean.c7 = crmDataElement.c7;
        crmOutboundBean.c8 = crmDataElement.c8;
        crmOutboundBean.c9 = crmDataElement.c9;
        crmOutboundBean.c10 = crmDataElement.c10;
        crmOutboundBean.categoryDesc = categoryDesc;
        crmOutboundBean.deliveryId = deliveryStatus.getCode();
        crmOutboundBean.deliveryMessage = deliveryMessage;
        crmOutboundBean.pushNotificationBean = pushNotificationBean;
        crmOutboundBean.processed = CrmOutboundProcessedStatusType.CREATED.getValue();

        return crmOutboundBean;
    }
}
