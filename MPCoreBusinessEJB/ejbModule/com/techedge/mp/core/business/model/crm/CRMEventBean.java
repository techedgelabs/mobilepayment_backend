package com.techedge.mp.core.business.model.crm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.crm.CommandType;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.StatusCode;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;

@Entity
@Table(name = "CRM_EVENT")
@NamedQueries({ 
    @NamedQuery(name = "CRMEventBean.findByPushNotification", query = "select e from CRMEventBean e where (e.pushNotificationBean != null and "
        + "e.pushNotificationBean = :pushnotificationbean)"), 
    @NamedQuery(name = "CRMEventBean.findToReconcilie", query = "select e from CRMEventBean e where e.toReconcilie = 1")
})
public class CRMEventBean implements UserReconciliationInterfaceBean {

    public static final String   FIND_BY_PUSHNOTIFICATION          = "CRMEventBean.findByPushNotification";
    public static final String   FIND_TO_RECONCILIE                = "CRMEventBean.findToReconcilie";

    public static final String   INTERACTION_POINT_SEPARATOR       = "@";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                 id;

    @Column(name = "session_id", nullable = false)
    private String               sessionID;

    @Column(name = "interaction_point", nullable = false)
    private String               interactionPoint;

    @Column(name = "source_id")
    private Long               sourceID;

    @Column(name = "source_type")
    private String               sourceType;
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private UserBean             userBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "eventBean", cascade = CascadeType.ALL)
    //.PERSIST
    private Set<CRMOfferBean>    offerBeanList                     = new HashSet<CRMOfferBean>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "eventBean", cascade = CascadeType.ALL)
    //.PERSIST
    private Set<CRMEventOperationBean>    operationBeanList                     = new HashSet<CRMEventOperationBean>();
    
    @Column(name = "to_reconcilie")
    private Boolean              toReconcilie;

    @Column(name = "reconciliation_attempts_left")
    private Integer              reconciliationAttemptsLeft;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "push_notification_id")
    private PushNotificationBean pushNotificationBean;

    public CRMEventBean() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public Long getSourceID() {
        return sourceID;
    }
    
    public void setSourceID(Long sourceID) {
        this.sourceID = sourceID;
    }
    
    public CRMEventSourceType getSourceType() {
        return CRMEventSourceType.getSourceType(sourceType);
    }
    
    public void setSourceType(CRMEventSourceType sourceType) {
        if (sourceType != null) {
            this.sourceType = sourceType.getValue();
        }
    }
    
    public List<String> getInteractionPointsToString() {
        if (interactionPoint == null) {
            return null;
        }

        String[] interactionPointArray = interactionPoint.split(INTERACTION_POINT_SEPARATOR);
        List<String> interactionPoints = new ArrayList<String>();

        for (int index = 0; index < interactionPointArray.length; index++) {
            interactionPoints.add(interactionPointArray[index]);
        }

        return interactionPoints;
    }

    public String getInteractionPointToString() {
        List<String> interactionPoints = getInteractionPointsToString();

        if (interactionPoints != null)
            return interactionPoints.get(0);
        else
            return null;
    }
    

    public InteractionPointType getInteractionPoint() {
        List<InteractionPointType> interactionPoints = getInteractionPoints();

        if (interactionPoints != null)
            return interactionPoints.get(0);
        else
            return null;
    }
    
    public List<InteractionPointType> getInteractionPoints() {
        if (interactionPoint == null) {
            return null;
        }

        String[] interactionPointArray = interactionPoint.split(INTERACTION_POINT_SEPARATOR);
        List<InteractionPointType> interactionPoints = new ArrayList<InteractionPointType>();

        for (int index = 0; index < interactionPointArray.length; index++) {
            InteractionPointType interactionPoint = InteractionPointType.valueOf(interactionPointArray[index]);
            interactionPoints.add(interactionPoint);
        }

        return interactionPoints;
    }
    
    public void setInteractionPoints(List<InteractionPointType> interactionPoints) {
        String interactionPointToString = "";

        for (int index = 0; index < interactionPoints.size(); index++) {
            if (index > 0) {
                interactionPointToString += INTERACTION_POINT_SEPARATOR;
            }

            InteractionPointType interactionPoint = interactionPoints.get(index);
            interactionPointToString += interactionPoint.getPoint();
        }

        this.interactionPoint = interactionPointToString;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Set<CRMOfferBean> getOfferBeanList() {
        return offerBeanList;
    }

    public void setOfferList(HashSet<CRMOfferBean> offerBeanList) {
        this.offerBeanList = offerBeanList;
    }

    public Set<CRMEventOperationBean> getOperationBeanList() {
        return operationBeanList;
    }

    public void setOperationList(HashSet<CRMEventOperationBean> operationBeanList) {
        this.operationBeanList = operationBeanList;
    }

    public PushNotificationBean getPushNotificationBean() {
        return pushNotificationBean;
    }

    public void setPushNotificationBean(PushNotificationBean pushNotificationBean) {
        this.pushNotificationBean = pushNotificationBean;
    }

    public Boolean getToReconcilie() {
        return toReconcilie;
    }

    public void setToReconcilie(Boolean toReconcilie) {
        this.toReconcilie = toReconcilie;
    }

    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public CRMEventOperationBean getOperationBean(CommandType command) {
        CRMEventOperationBean operationBean = null;

        for (CRMEventOperationBean tmpOperationBean : getOperationBeanList()) {
            if (tmpOperationBean.getCommand().equals(command)) {
                operationBean = tmpOperationBean;
            }
        }

        return operationBean;
    }
    
    @Override
    public String getFinalStatus() {
         StatusCode status = StatusCode.SUCCESS;
         
         for (CRMEventOperationBean eventOperation : getOperationBeanList()) {
             System.out.println("EVENT OPERATION STATUS: " + eventOperation.getStatusCode().getName());
             if (!eventOperation.getStatusCode().equals(status)) {
                 status = eventOperation.getStatusCode();
                 break;
             }
         }
         
         return status.getName();
    }

    @Override
    public void setFinalStatus(String status) {
    }

    @Override
    public String getOperationName() {
        CRMEventOperationBean eventOperationBean = getOperationBean(CommandType.GET_OFFERS);
        
        if (eventOperationBean == null) {
            eventOperationBean = getOperationBean(CommandType.GET_OFFERS_FOR_MULTIPLE_INTERACTION_POINTS);
        }
        
        if (eventOperationBean != null) {
            return eventOperationBean.getCommand().getName();
        }
        
        return CommandType.GET_OFFERS.getName();
    }

    @Override
    public Date getOperationTimestamp() {
        CRMEventOperationBean eventOperationBean = getOperationBean(CommandType.START_SESSION);
        
        if (eventOperationBean == null) {
            eventOperationBean = getOperationBean(CommandType.GET_OFFERS);
        }

        if (eventOperationBean == null) {
            eventOperationBean = getOperationBean(CommandType.GET_OFFERS_FOR_MULTIPLE_INTERACTION_POINTS);
        }
        
        if (eventOperationBean != null) {
            return eventOperationBean.getRequestTimestamp();
        }
        
        return null;
    }
    
    @Override
    public String getFinalStatusMessage() {
        String message = null;
        CRMEventOperationBean operationGetOffersBean = getOperationBean(CommandType.GET_OFFERS);
        
        if (operationGetOffersBean == null) {
            CRMEventOperationBean tmpOperationGetOffersBean = getOperationBean(CommandType.START_SESSION);
            
            if (tmpOperationGetOffersBean != null) {
                message = tmpOperationGetOffersBean.getExtendedMessage();
            }
        }
        else {
            message = operationGetOffersBean.getExtendedMessage();
        }
        
        return message;
    }
    
    @Override
    public String getServerEndpoint() {
        return "CRM IBM";
    }
    
    public static CRMEventBean createEvent(String sessionID, InteractionPointType interactionPoint, Long sourceID, CRMEventSourceType sourceType, UserBean userBean, Integer maxAttemptsLeft) {
        ArrayList<InteractionPointType> interactionPoints = new ArrayList<InteractionPointType>();
        interactionPoints.add(interactionPoint);
        return createEvent(sessionID, interactionPoints, sourceID, sourceType, userBean, maxAttemptsLeft);
    }

    public static CRMEventBean createEvent(String sessionID, List<InteractionPointType> interactionPoints, Long sourceID, CRMEventSourceType sourceType, UserBean userBean, Integer maxAttemptsLeft) {

        CRMEventBean eventBean = new CRMEventBean();
        eventBean.setSessionID(sessionID);
        eventBean.setUserBean(userBean);
        eventBean.setSourceID(sourceID);
        eventBean.setSourceType(sourceType);
        eventBean.setInteractionPoints(interactionPoints);
        eventBean.setToReconcilie(false);
        eventBean.setReconciliationAttemptsLeft(maxAttemptsLeft);

        return eventBean;
    }

    
    
}
