package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.Address;

@Entity
@Table(name="ADDRESS")
public class AddressBean {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	
	@Column(name="city", nullable=false, length=40)
	private String city;
	
	@Column(name="countrycodeid", nullable=false, length=40)
	private String countryCodeId;
	
	@Column(name="countrycodename", nullable=false, length=40)
	private String countryCodeName;
	
	@Column(name="housenumber", nullable=false, length=40)
	private String houseNumber;
	
	@Column(name="region", nullable=false, length=40)
	private String region;
	
	@Column(name="street", nullable=false, length=50)
	private String street;
	
	@Column(name="zipcode", nullable=false, length=5)
	private String zipcode;
	
	public AddressBean() {}
	
	public AddressBean(Address address) {
		
		this.city = address.getCity();
		this.countryCodeId = address.getCountryCodeId();
		this.countryCodeName = address.getCountryCodeName();
		this.houseNumber = address.getHouseNumber();
		this.region = address.getRegion();
		this.street = address.getStreet();
		this.zipcode = address.getZipcode();
	}
	
	public Address toAddress() {
		
		Address addressData = new Address();
		
		addressData.setCity(this.city);
		addressData.setCountryCodeId(this.countryCodeId);
		addressData.setCountryCodeName(this.countryCodeName);
		addressData.setHouseNumber(this.houseNumber);
		addressData.setRegion(this.region);
		addressData.setStreet(this.street);
		addressData.setZipcode(this.zipcode);
		
		return addressData;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCodeId() {
		return countryCodeId;
	}

	public void setCountryCodeId(String countryCodeId) {
		this.countryCodeId = countryCodeId;
	}

	public String getCountryCodeName() {
		return countryCodeName;
	}

	public void setCountryCodeName(String countryCodeName) {
		this.countryCodeName = countryCodeName;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}
