package com.techedge.mp.core.business.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucherDetail;

@Entity
@Table(name = "PREPAIDCONSUMEVOUCHER")
@NamedQueries({
    @NamedQuery(name = "PrePaidConsumeVoucherBean.findPrePaidConsumeVoucherBeanById", query = "select v from PrePaidConsumeVoucherBean v where v.id = :id "),
    @NamedQuery(name = "PrePaidConsumeVoucherBean.findPrePaidConsumeVoucherBeanByStatusCode", query = "select v from PrePaidConsumeVoucherBean v where v.statusCode = :statusCode "),
    @NamedQuery(name = "PrePaidConsumeVoucherBean.findConsumedPrePaidConsumeVoucherBeanByStatusCodeAndTimestamp", query = "select v from PrePaidConsumeVoucherBean v where v.statusCode = :statusCode and v.totalConsumed > 0.0 and v.requestTimestamp >= :startTimestamp and v.requestTimestamp <= :endTimestamp")
})
public class PrePaidConsumeVoucherBean {
    
    public static final String FIND_BY_ID = "PrePaidConsumeVoucherBean.findPrePaidConsumeVoucherBeanById";
    public static final String FIND_BY_STATUS_CODE = "PrePaidConsumeVoucherBean.findPrePaidConsumeVoucherBeanByStatusCode";
    public static final String FIND_CONSUMED_BY_STATUS_CODE_AND_TIMESTAMP = "PrePaidConsumeVoucherBean.findConsumedPrePaidConsumeVoucherBeanByStatusCodeAndTimestamp";

    public static final String STATUS_CONSUME_TO_VERIFY = "AAAA";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                                  id;

    @Column(name = "cs_transaction_id", nullable = true)
    private String                                csTransactionID;

    @Column(name = "operation_id", nullable = true)
    private String                                operationID;
    
    @Column(name = "operation_id_reversed", nullable = true)
    private String                                operationIDReversed;
    
    @Column(name = "operation_type", nullable = true)
    private String                                operationType;

    @Column(name = "request_timestamp", nullable = true)
    private Long                                  requestTimestamp;

    @Column(name = "marketing_msg", nullable = true, length = 500)
    private String                                marketingMsg;

    @Column(name = "warning_msg", nullable = true)
    private String                                warningMsg;

    @Column(name = "message_code", nullable = true)
    private String                                messageCode;

    @Column(name = "status_code", nullable = true)
    private String                                statusCode;

    @Column(name = "total_consumed", nullable = true)
    private Double                                totalConsumed;

    @Column(name = "amount", nullable = true)
    private Double                                amount;

    @Column(name = "reconciled", nullable = true)
    private Boolean                         reconciled = false;
    
    @Column(name = "preauth_operation_id", nullable = true)
    private String                                preAuthOperationID;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TransactionBean", nullable = false)
    private TransactionBean transactionBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "prePaidConsumeVoucherBean")
    private Set<PrePaidConsumeVoucherDetailBean> prePaidConsumeVoucherDetailBean = new HashSet<PrePaidConsumeVoucherDetailBean>(0);

    public PrePaidConsumeVoucherBean() {}

    public PrePaidConsumeVoucherBean(PrePaidConsumeVoucher prePaidConsumeVoucher) {

        this.id = prePaidConsumeVoucher.getId();
        this.csTransactionID = prePaidConsumeVoucher.getCsTransactionID();
        this.operationID = prePaidConsumeVoucher.getOperationID();
        this.operationIDReversed = prePaidConsumeVoucher.getOperationIDReversed();
        this.operationType = prePaidConsumeVoucher.getOperationType();
        this.requestTimestamp = prePaidConsumeVoucher.getRequestTimestamp();
        this.marketingMsg = prePaidConsumeVoucher.getMarketingMsg();
        this.warningMsg = prePaidConsumeVoucher.getWarningMsg();
        this.messageCode = prePaidConsumeVoucher.getMessageCode();
        this.statusCode = prePaidConsumeVoucher.getStatusCode();
        this.totalConsumed = prePaidConsumeVoucher.getTotalConsumed();
        this.amount = prePaidConsumeVoucher.getAmount();
        this.reconciled = prePaidConsumeVoucher.getReconciled();
        this.preAuthOperationID = prePaidConsumeVoucher.getPreAuthOperationID();

        if (prePaidConsumeVoucher.getPrePaidConsumeVoucherDetail() != null) {
            for (PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail : prePaidConsumeVoucher.getPrePaidConsumeVoucherDetail()) {
                PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean = new PrePaidConsumeVoucherDetailBean(prePaidConsumeVoucherDetail);
                prePaidConsumeVoucherDetailBean.setPrePaidConsumeVoucherBean(this);
                this.prePaidConsumeVoucherDetailBean.add(prePaidConsumeVoucherDetailBean);
            }
        }
    }

    public PrePaidConsumeVoucherBean(PrePaidConsumeVoucherHistoryBean prePaidConsumeVoucherBean) {

        this.csTransactionID = prePaidConsumeVoucherBean.getCsTransactionID();
        this.operationID = prePaidConsumeVoucherBean.getOperationID();
        this.operationIDReversed = prePaidConsumeVoucherBean.getOperationIDReversed();
        this.operationType = prePaidConsumeVoucherBean.getOperationType();
        this.requestTimestamp = prePaidConsumeVoucherBean.getRequestTimestamp();
        this.marketingMsg = prePaidConsumeVoucherBean.getMarketingMsg();
        this.warningMsg = prePaidConsumeVoucherBean.getWarningMsg();
        this.messageCode = prePaidConsumeVoucherBean.getMessageCode();
        this.statusCode = prePaidConsumeVoucherBean.getStatusCode();
        this.totalConsumed = prePaidConsumeVoucherBean.getTotalConsumed();
        this.amount = prePaidConsumeVoucherBean.getAmount();
        this.reconciled = prePaidConsumeVoucherBean.getReconciled();
        this.preAuthOperationID = prePaidConsumeVoucherBean.getPreAuthOperationID();

        for (PrePaidConsumeVoucherDetailHistoryBean prePaidConsumeVoucherDetailBean : prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailHistoryBean()) {

            PrePaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailHistoryBean = new PrePaidConsumeVoucherDetailBean(prePaidConsumeVoucherDetailBean);

            this.prePaidConsumeVoucherDetailBean.add(postPaidConsumeVoucherDetailHistoryBean);
        }
    }
    
    public PrePaidConsumeVoucher toPrePaidConsumeVoucher() {

        PrePaidConsumeVoucher prePaidConsumeVoucher = new PrePaidConsumeVoucher();
        prePaidConsumeVoucher.setId(this.id);
        prePaidConsumeVoucher.setCsTransactionID(this.csTransactionID);
        prePaidConsumeVoucher.setOperationID(this.operationID);
        prePaidConsumeVoucher.setOperationIDReversed(this.operationIDReversed);
        prePaidConsumeVoucher.setOperationType(this.operationType);
        prePaidConsumeVoucher.setRequestTimestamp(this.requestTimestamp);
        prePaidConsumeVoucher.setMarketingMsg(this.marketingMsg);
        prePaidConsumeVoucher.setWarningMsg(this.warningMsg);
        prePaidConsumeVoucher.setMessageCode(this.messageCode);
        prePaidConsumeVoucher.setStatusCode(this.statusCode);
        prePaidConsumeVoucher.setTotalConsumed(this.totalConsumed);
        prePaidConsumeVoucher.setAmount(this.amount);
        prePaidConsumeVoucher.setReconciled(this.reconciled);
        prePaidConsumeVoucher.setPreAuthOperationID(this.preAuthOperationID);

        if (!this.prePaidConsumeVoucherDetailBean.isEmpty()) {

            for (PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean : this.prePaidConsumeVoucherDetailBean) {
                PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail = prePaidConsumeVoucherDetailBean.toPrePaidConsumeVoucherDetail();
                prePaidConsumeVoucher.getPrePaidConsumeVoucherDetail().add(prePaidConsumeVoucherDetail);
            }
        }

        return prePaidConsumeVoucher;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public void setOperationIDReversed(String operationIDReversed) {
        this.operationIDReversed = operationIDReversed;
    }

    public String getOperationIDReversed() {
        return operationIDReversed;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotalConsumed() {
        return totalConsumed;
    }

    public void setTotalConsumed(Double totalConsumed) {
        this.totalConsumed = totalConsumed;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Boolean getReconciled() {
        return reconciled;
    }

    public void setReconciled(Boolean reconciled) {
        this.reconciled = reconciled;
    }

    public TransactionBean getTransactionBean() {
        return transactionBean;
    }

    public void setTransactionBean(TransactionBean transactionBean) {
        this.transactionBean = transactionBean;
    }

    public String getPreAuthOperationID() {
        return preAuthOperationID;
    }

    public void setPreAuthOperationID(String preAuthOperationID) {
        this.preAuthOperationID = preAuthOperationID;
    }

    public Set<PrePaidConsumeVoucherDetailBean> getPrePaidConsumeVoucherDetailBean() {
        return prePaidConsumeVoucherDetailBean;
    }

    public void setPrePaidConsumeVoucherDetailBean(Set<PrePaidConsumeVoucherDetailBean> prePaidConsumeVoucherDetailBean) {
        this.prePaidConsumeVoucherDetailBean = prePaidConsumeVoucherDetailBean;
    }

}
