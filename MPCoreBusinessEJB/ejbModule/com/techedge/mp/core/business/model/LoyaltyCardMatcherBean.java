package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "LOYALTYCARDMATCHER")
@NamedQueries({
    @NamedQuery(name = "LoyaltyCardMatcherBean.findById", query = "select l from LoyaltyCardMatcherBean l where l.id = :id"),
    @NamedQuery(name = "LoyaltyCardMatcherBean.findByOperationID", query = "select l from LoyaltyCardMatcherBean l where l.operationID = :operationID")})
public class LoyaltyCardMatcherBean {

    public static final String FIND_BY_ID = "LoyaltyCardMatcherBean.findById";
    public static final String FIND_BY_OPERATION_ID = "LoyaltyCardMatcherBean.findByOperationID";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "ean_code", nullable = true)
    private String             eanCode;

    @Column(name = "pan_code", nullable = true)
    private String             panCode;

    @Column(name = "card_type", nullable = true)
    private String             cardType;

    @Column(name = "operation_id", nullable = true)
    private String             operationID;

    public LoyaltyCardMatcherBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getPanCode() {
        return panCode;
    }

    public void setPanCode(String panCode) {
        this.panCode = panCode;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

}
