package com.techedge.mp.core.business.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.techedge.mp.core.business.exceptions.PromoVoucherException;
import com.techedge.mp.core.business.exceptions.PromotionException;
import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.core.business.interfaces.PromotionInfo;
import com.techedge.mp.core.business.interfaces.PromotionStatus;

@Entity
@Table(name = "PROMOTION")
@NamedQueries({
    @NamedQuery(name = "PromotionBean.findByCode",       query = "select p from PromotionBean p where p.code = :code AND p.endData >= :date and status = :status"),
    @NamedQuery(name = "PromotionBean.findSingleByCode", query = "select p from PromotionBean p where p.code = :code"),
    @NamedQuery(name = "PromotionBean.findAll",          query = "select p from PromotionBean p")
})
//select * from promotion where code = "00002221" AND DATE(`startData`) >= '2016-02-04 21:00:00'
public class PromotionBean {

    public static final String     START_DATE_OR_END_DATE_OF_PROMOTION_ARE_INCORRECT = "Start date or End date of promotion are incorrect";

    public static final String     START_DATE_OR_END_DATE_OF_PROMOTION_ARE_NULL      = "Start date or End date of promotion are null";

    public static final String     NAME_OF_PROMOTION_CANNOT_BE_NULL                  = "Name of promotion cannot be null";

    public static final String     DESCRIPTION_OF_PROMOTION_CANNOT_BE_NULL           = "Description of promotion cannot be null";

    public static final String     FIND_PROMOTION_BY_CODE                            = "PromotionBean.findByCode";
    public static final String     FIND_SINGLE_PROMOTION_BY_CODE                     = "PromotionBean.findSingleByCode";
    public static final String     FIND_ALL                                          = "PromotionBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                   id;

    @Column(name = "code", nullable = false, length = 40)
    private String                 code;

    @Column(name = "description", nullable = false, length = 40)
    private String                 description;

    @Column(name = "startdata", nullable = false)
    private Date                   startData;

    @Column(name = "enddata", nullable = false)
    private Date                   endData;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotionBean", cascade = CascadeType.ALL)
    private List<PromoVoucherBean> promoVoucherList                                  = new ArrayList<PromoVoucherBean>(0);

    @Column(name = "status", nullable = false)
    private Integer                status;

    public PromotionBean() {
        super();
    }

    public PromotionBean(PromotionBean promotion) {
        this.id = promotion.getId();
        this.code = promotion.getCode();
        this.description = promotion.getDescription();
        this.status = promotion.getStatus();
        this.startData = promotion.getStartData();
        this.endData = promotion.getEndData();
        if (promotion.getPromoVoucherList() != null) {
            this.promoVoucherList = promotion.getPromoVoucherList();
        }
        else
            this.promoVoucherList = new ArrayList<PromoVoucherBean>(0);
    }

    public PromotionBean(String code, String description, Date startData, Date endData, List<PromoVoucherBean> promoVoucherList) {
        this.code = code;
        this.description = description;
        this.status = PromotionStatus.ACTIVE.getValue();
        this.startData = startData;
        this.endData = endData;
        this.promoVoucherList = promoVoucherList;
    }
    
    public PromotionBean(String code, String description, Date startData, Date endData) {
        if (code != null) {
            this.code = code;
        }
        if (description != null) {
            this.description = description;
        }
        
        this.status = PromotionStatus.ACTIVE.getValue();
        
        if (startData != null) {
            this.startData = startData;
        }
        if (endData != null) {
            this.endData = endData;
        }
    }
    
    public PromotionInfo toPromotionInfo() {
        
        PromotionInfo promotionInfo = new PromotionInfo();
        promotionInfo.setCode(this.code);
        promotionInfo.setDescription(this.description);
        promotionInfo.setEndData(this.endData);
        promotionInfo.setStartData(this.startData);
        promotionInfo.setStatus(this.status);
        return promotionInfo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartData() {
        return startData;
    }

    public void setStartData(Date startData) {
        this.startData = startData;
    }

    public Date getEndData() {
        return endData;
    }

    public void setEndData(Date endData) {
        this.endData = endData;
    }

    public List<PromoVoucherBean> getPromoVoucherList() {
        return promoVoucherList;
    }

    public void setPromoVoucherList(List<PromoVoucherBean> promoVoucherList) {
        this.promoVoucherList = promoVoucherList;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    /**
     * Crea un promotionBean
     * 
     * @param code
     *            Code della promozione
     * @param description
     *            Descrizione della promozione
     * @param startDate
     *            Inizio della promozione
     * @param endDate
     *            Fine della promozione
     * @return Promotion object
     * @throws PromotionException
     */
    public PromotionBean createPromotion(String code, String description, Date startDate, Date endDate) throws PromotionException {

        PromotionBean createdPromotion = new PromotionBean(code, description, startDate, endDate);

        return createdPromotion;

    }

    /**
     * Update della Promozione
     * 
     * @param code
     *            Codice della promozione
     * @param description
     *            Descrizione della promozione
     * @param startDate
     *            Inizio della promozione
     * @param endDate
     *            Fine della promozione
     * @throws PromotionException
     */
    public void update(String code, String description, Date startDate, Date endDate) throws PromotionException {

        if (code != null) {

            this.code = code;
        }
        else {

            throw new PromotionException(NAME_OF_PROMOTION_CANNOT_BE_NULL);
        }

        if (description != null) {

            this.description = description;
        }
        else {

            throw new PromotionException(DESCRIPTION_OF_PROMOTION_CANNOT_BE_NULL);
        }

        if (startDate == null || endDate == null) {

            throw new PromotionException(START_DATE_OR_END_DATE_OF_PROMOTION_ARE_NULL);
        }
        else if (endDate.before(startDate) || startDate.after(endDate)) {

            throw new PromotionException(START_DATE_OR_END_DATE_OF_PROMOTION_ARE_INCORRECT);
        }
        else {

            this.startData = startDate;
            this.endData = endDate;
        }
    }

    /**
     * Crea n oggetti di tipo PromoVoucher e li associa alla promozione.
     * Converte da PromoVoucherInput a PromoVoucher per poter associarli alla promozione
     * 
     * @param promoVoucherList
     *            Lista di n oggetti promoVoucherInput da aggiungere alla Promozione
     * @throws PromotionException
     */
    public void addPromoVouchers(PromoVoucherInput promoVoucherList) throws PromotionException {

        PromoVoucherBean promoVoucherBean;

        Set<String> promoVoucherInputKey = promoVoucherList.getPromoAssociation().keySet();
        for (String key : promoVoucherInputKey) {
            try {

                promoVoucherBean = new PromoVoucherBean();
                promoVoucherBean.create(key, promoVoucherList.getPromoAssociation().get(key), this);
            }
            catch (PromoVoucherException promoVE) {

                throw new PromotionException(promoVE.getMessage());
            }
        }
    }

    /**
     * Elimina l'associazione tra il promoVoucher e la Promozione e modifica lo stato del promoVoucher in deleted
     * 
     * @param voucherCode
     *            codice del promoVoucher da disaccoppiare e settargli lo stato in deleted
     * @throws PromotionException
     */
    public void removePromoVoucher(String voucherCode) throws PromotionException {

        List<PromoVoucherBean> voucherList = new ArrayList<PromoVoucherBean>();
        voucherList.addAll(getPromoVoucherList());

        for (PromoVoucherBean promoVoucher : getPromoVoucherList()) {
            if (promoVoucher.getCode().equalsIgnoreCase(voucherCode)) {

                promoVoucher.delete();
                voucherList.remove(promoVoucher);
            }
        }

        setPromoVoucherList(voucherList);
    }

}
