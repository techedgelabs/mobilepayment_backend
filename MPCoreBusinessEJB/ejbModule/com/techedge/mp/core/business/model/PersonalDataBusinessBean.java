package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.user.PersonalDataBusiness;

@Entity
@Table(name="PERSONALDATA_BUSINESS")
public class PersonalDataBusinessBean {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "first_name")
    private String                   firstName;

    @Column(name = "last_name")
    private String                   lastName;

    @Column(name = "business_name")
    private String                   businessName;

    @Column(name = "vat_number", nullable=false)
    private String                   vatNumber;
    
    @Column(name="pec_email")
    private String                   pecEmail;
    
    @Column(name="sdi_code")
    private String                   sdiCode;

    @Column(name="city", nullable=false)
    private String city;
    
    @Column(name="province", nullable=false)
    private String province;
    
    @Column(name="country", nullable=false)
    private String country;
    
    @Column(name="zipcode", nullable=false, length=5)
    private String zipCode;
    
    @Column(name="address", nullable=false)
    private String address;
    
    @Column(name="street_number", nullable=false)
    private String streetNumber;
    
    @Column(name="licensePlate")
    private String licensePlate;
    
    @Column(name = "fiscal_code")
    private String fiscalCode;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserBean userBean;
    
    public PersonalDataBusinessBean() {}
    
    public PersonalDataBusinessBean(PersonalDataBusiness personalDataBusiness) {
        this.address = personalDataBusiness.getAddress();
        this.businessName = personalDataBusiness.getBusinessName();
        this.city = personalDataBusiness.getCity();
        this.firstName = personalDataBusiness.getFirstName();
        this.fiscalCode = personalDataBusiness.getFiscalCode();
        this.lastName = personalDataBusiness.getLastName();
        this.licensePlate = personalDataBusiness.getLicensePlate();
        this.pecEmail = personalDataBusiness.getPecEmail();
        this.province = personalDataBusiness.getProvince();
        this.sdiCode = personalDataBusiness.getSdiCode();
        this.vatNumber = personalDataBusiness.getVatNumber();
        this.zipCode = personalDataBusiness.getZipCode();
        this.streetNumber = personalDataBusiness.getStreetNumber();
        this.country = personalDataBusiness.getCountry();
    }
    
    public PersonalDataBusiness toPersonalDataBusiness() {
        PersonalDataBusiness personalDataBusiness = new PersonalDataBusiness();
        personalDataBusiness.setAddress(this.address);
        personalDataBusiness.setBusinessName(this.businessName);
        personalDataBusiness.setCity(this.city);
        personalDataBusiness.setFirstName(this.firstName);
        personalDataBusiness.setFiscalCode(this.fiscalCode);
        personalDataBusiness.setLastName(this.lastName);
        personalDataBusiness.setLicensePlate(this.licensePlate);
        personalDataBusiness.setPecEmail(this.pecEmail);
        personalDataBusiness.setProvince(this.province);
        personalDataBusiness.setSdiCode(this.sdiCode);
        personalDataBusiness.setVatNumber(this.vatNumber);
        personalDataBusiness.setZipCode(this.zipCode);
        personalDataBusiness.setStreetNumber(this.streetNumber);
        personalDataBusiness.setCountry(this.country);
        
        return personalDataBusiness;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getPecEmail() {
        return pecEmail;
    }

    public void setPecEmail(String pecEmail) {
        this.pecEmail = pecEmail;
    }

    public String getSdiCode() {
        return sdiCode;
    }

    public void setSdiCode(String sdiCode) {
        this.sdiCode = sdiCode;
    }

    public String getStreetNumber() {
        return streetNumber;
    }
    
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getCountry() {
        return country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }
    
}
