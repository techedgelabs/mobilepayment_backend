package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "PROMOPOPUP")
@NamedQueries({ @NamedQuery(name = "PromoPopupBean.findActivePromoPopupBySectionId", query = "select p from PromoPopupBean p where p.sectionId = :sectionId and p.status = 1 and p.startValidity <= :checkDate and p.endValidity >= :checkDate") })
public class PromoPopupBean {

    public static final String FIND_BY_SECTIONID = "PromoPopupBean.findActivePromoPopupBySectionId";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "url", nullable = true)
    private String             url;

    @Column(name = "type", nullable = true)
    private String             type;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "body", nullable = true)
    private String             body;

    @Column(name = "banner_url", nullable = true)
    private String             bannerUrl;

    @Column(name = "show_always", nullable = true)
    private Boolean            showAlways;

    @Column(name = "promo_id", nullable = false)
    private String             promoId;

    @Column(name = "section_id", nullable = true)
    private String             sectionId;
    
    @Column(name = "status", nullable = true)
    private Integer            status; // 0 - inactive  / 1 - active
    
    @Column(name = "start_validity", nullable = true)
    private Date            startValidity;
    
    @Column(name = "end_validity", nullable = true)
    private Date            endValidity;
    
    @Column(name = "accept_required", nullable = true)
    private Boolean            acceptRequired;

    public PromoPopupBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public Boolean getShowAlways() {
        return showAlways;
    }

    public void setShowAlways(Boolean showAlways) {
        this.showAlways = showAlways;
    }

    public String getPromoId() {
        return promoId;
    }

    public void setPromoId(String promoId) {
        this.promoId = promoId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getStartValidity() {
        return startValidity;
    }

    public void setStartValidity(Date startValidity) {
        this.startValidity = startValidity;
    }

    public Date getEndValidity() {
        return endValidity;
    }

    public void setEndValidity(Date endValidity) {
        this.endValidity = endValidity;
    }

    public Boolean getAcceptRequired() {
        return acceptRequired;
    }

    public void setAcceptRequired(Boolean acceptRequired) {
        this.acceptRequired = acceptRequired;
    }
    
}
