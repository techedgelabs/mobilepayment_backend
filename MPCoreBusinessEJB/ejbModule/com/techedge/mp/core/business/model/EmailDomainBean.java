package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.EmailDomain;

@Entity
@Table(name = "BLACKLIST_EMAIL_DOMAIN")
@NamedQueries({ @NamedQuery(name = "EmailDomainBean.findById", query = "select a from EmailDomainBean a where a.id = :id"),
        @NamedQuery(name = "EmailDomainBean.findAll", query = "select a from EmailDomainBean a"),
        @NamedQuery(name = "EmailDomainBean.findByEmail", query = "select a from EmailDomainBean a where a.email = :email") })

public class EmailDomainBean {
    public static final String FIND_BY_ID    = "EmailDomainBean.findById";
    public static final String FIND_BY_EMAIL = "EmailDomainBean.findByEmail";
    public static final String FIND_ALL      = "EmailDomainBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "email", nullable = false)
    private String             email;

    public EmailDomainBean() {}

    public EmailDomainBean(EmailDomain emailDomain) {

        this.id = emailDomain.getId();
        this.email = emailDomain.getEmail();

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
