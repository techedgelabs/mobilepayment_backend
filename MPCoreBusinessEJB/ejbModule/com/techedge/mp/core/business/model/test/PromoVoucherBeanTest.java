package com.techedge.mp.core.business.model.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import com.techedge.mp.core.business.exceptions.PromoVoucherException;
import com.techedge.mp.core.business.interfaces.PromotionVoucherStatus;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PromoVoucherBean;
import com.techedge.mp.core.business.model.PromotionBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.test.factory.ConcreteModelFactory;
import com.techedge.mp.core.business.model.test.factory.ModelFactory;

public class PromoVoucherBeanTest {

    PromoVoucherBean voucherPromoBean;
    UserBean         user;
    PromotionBean    promoBean;

    @Before
    public void setUp() {

        try {
//            Class.forName("H2");
            DriverManager.getConnection("jdbc:h2:~/test", "sa", "sa");
        }
        catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        ModelFactory modelObjec = new ConcreteModelFactory();

        voucherPromoBean = modelObjec.createPromoVoucher();
        user = modelObjec.createUserBean();

        promoBean = modelObjec.createPromotion();
    }

    @Test
    public void testCreatePromotionVaucher() {

        checkNotNull();
        assertEquals(1, voucherPromoBean.getStatus().intValue());
    }

    private void checkNotNull() {

        assertNotNull(voucherPromoBean.getCode());
        assertNotNull(voucherPromoBean.getValue());
        assertNotNull(voucherPromoBean.getStatus());
    }

    @Test
    public void testCreatePromotionVoucher() throws PromoVoucherException {

        voucherPromoBean.create("007", 250.3, promoBean);

        assertEquals(1, voucherPromoBean.getPromotionBean().getPromoVoucherList().size());
        assertEquals("007", voucherPromoBean.getCode());
        assertEquals(250.3d, voucherPromoBean.getValue().doubleValue(), 0.0);
        assertEquals(PromotionVoucherStatus.NEW.getValue(), voucherPromoBean.getStatus().intValue());
        assertEquals(null, voucherPromoBean.getUserBean());
    }

    @Test
    public void testAssignUserPromotionVoucher() throws PromoVoucherException {

        voucherPromoBean.assignUser(user, user.getPersonalDataBean().getFiscalCode());

        assertEquals(User.USER_STATUS_VERIFIED.intValue(), voucherPromoBean.getUserBean().getUserStatus().intValue());
        assertEquals(PromotionVoucherStatus.ASSIGNED.getValue(), voucherPromoBean.getStatus().intValue());
    }

    @Test(expected = PromoVoucherException.class)
    public void testConsumePromotionVoucherUserDifferent() throws PromoVoucherException {

        voucherPromoBean.assignUser(user, user.getPersonalDataBean().getFiscalCode());
        UserBean user1 = new UserBean();
        user1.setUserStatus(User.USER_STATUS_VERIFIED);

        try {

            voucherPromoBean.consume(user1);
        }
        catch (PromoVoucherException e) {
            String message = e.getMessage();
            assertEquals("User is not the same", message);
            throw e;
        }
    }

    @Test
    public void testConsumePromotionVoucher() throws PromoVoucherException {

        voucherPromoBean.assignUser(user, user.getPersonalDataBean().getFiscalCode());
        voucherPromoBean.consume(user);

        assertEquals(User.USER_STATUS_VERIFIED.intValue(), voucherPromoBean.getUserBean().getUserStatus().intValue());
        assertEquals(PromotionVoucherStatus.CONSUMED.getValue(), voucherPromoBean.getStatus().intValue());
    }

    @Test
    public void testDeletePromotionVoucher() throws PromoVoucherException {

        voucherPromoBean.delete();

        assertEquals(PromotionVoucherStatus.DELETED.getValue(), voucherPromoBean.getStatus().intValue());
    }

}
