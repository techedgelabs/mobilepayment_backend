package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PaymentInfo;

@Entity
@Table(name = "PAYMENTINFO")
@NamedQueries({
    
        @NamedQuery(name = "PaymentInfoBean.findPaymentNotVerifiedInfoByUserBean", query = "select p from PaymentInfoBean p where p.status = 1 and p.insertTimestamp <= :dayToCompare"),
        @NamedQuery(name = "PaymentInfoBean.findPaymentInfoByIdAndType", query = "select p from PaymentInfoBean p where p.id = :id and p.type = :type"),
        @NamedQuery(name = "PaymentInfoBean.findPaymentInfoByUserBean", query = "select p from PaymentInfoBean p where p.user = :userBean and ( p.status = 1 or p.status = 2 )"),
        @NamedQuery(name="PaymentInfoBean.findTokenExists", query="select p from PaymentInfoBean p where p.token = :token and ( p.status = 1 or p.status = 2 )" ),
        @NamedQuery(name = "PaymentInfoBean.findHashExists", query = "select p from PaymentInfoBean p where p.hash = :hash and ( p.status = 1 or p.status = 2 )"),
        @NamedQuery(name = "PaymentInfoBean.countActiveToken", query = "select count(p) from PaymentInfoBean p where p.token = :token and ( p.status = 1 or p.status = 2 )"),
        @NamedQuery(name = "PaymentInfoBean.findU", query = "select p from PaymentInfoBean p where p.hash = :hash and ( p.status = 1 or p.status = 2 )"),
        @NamedQuery(name = "PaymentInfoBean.findPaymentInfoByUserBeanAndType", query = "select p from PaymentInfoBean p where p.user = :userBean and p.type=:type"),

})
public class PaymentInfoBean {

    public static final String FIND_OLD_BY_ID                                       = "PaymentInfoBean.findPaymentNotVerifiedInfoByUserBean";
    public static final String FIND_BY_ID_AND_TYPE                                  = "PaymentInfoBean.findPaymentInfoByIdAndType";
    public static final String FIND_BY_USERBEAN                                     = "PaymentInfoBean.findPaymentInfoByUserBean";
    public static final String FIND_PAYMENT_HASH_EXISTS                             = "PaymentInfoBean.findHashExists";
    public static final String FIND_PAYMENT_TOKEN_EXISTS                             = "PaymentInfoBean.findTokenExists";
    public static final String COUNT_ACTIVE_TOKEN                                   = "PaymentInfoBean.countActiveToken";
    public static final String FIND_PAYMENT_BY_USER                                  = "PaymentInfoBean.findPaymentInfoByUserBeanAndType";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "type", nullable = true)
    private String             type;

    @Column(name = "brand", nullable = true, length = 40)
    private String             brand;

    @Column(name = "expirationdate", nullable = true)
    private Date               expirationDate;

    @Column(name = "pan", nullable = true, length = 40)
    private String             pan;

    @Column(name = "card_bin", nullable = true, length = 6)
    private String             cardBin;

    @Column(name = "status", nullable = true)
    private Integer            status;

    @Column(name = "message", nullable = true, length = 40)
    private String             message;

    @Column(name = "token", nullable = true, length = 40)
    private String             token;

    @Column(name = "token3ds", nullable = true, length = 40)
    private String             token3ds;

    @Column(name = "default_method", nullable = true)
    private Boolean            defaultMethod;

    @Column(name = "attempts_left", nullable = true)
    private Integer            attemptsLeft;

    @Column(name = "check_amount", nullable = true)
    private Double             checkAmount;

    @Column(name = "insert_timestamp", nullable = true)
    private Date               insertTimestamp;

    @Column(name = "verified_timestamp", nullable = true)
    private Date               verifiedTimestamp;

    @Column(name = "pin", nullable = true)
    private String             pin;

    @Column(name = "check_shop_login", nullable = true)
    private String             checkShopLogin;

    @Column(name = "check_shop_transaction_id", nullable = true)
    private String             checkShopTransactionID;

    @Column(name = "check_bank_transaction_id", nullable = true)
    private String             checkBankTransactionID;

    @Column(name = "check_currency", nullable = true)
    private String             checkCurrency;

    @Column(name = "pin_check_attempts_left", nullable = true)
    private Integer            pinCheckAttemptsLeft;

    @Column(name = "hash", nullable = true)
    private String            hash;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserBean           user;

    public PaymentInfoBean() {}

    public PaymentInfoBean(PaymentInfo paymentInfo) {

        this.id = paymentInfo.getId();
        this.type = paymentInfo.getType();
        this.brand = paymentInfo.getBrand();
        this.expirationDate = paymentInfo.getExpirationDate();
        this.pan = paymentInfo.getPan();
        this.status = paymentInfo.getStatus();
        this.message = paymentInfo.getMessage();
        this.token = paymentInfo.getToken();
        this.token3ds = paymentInfo.getToken3ds();
        this.defaultMethod = paymentInfo.getDefaultMethod();
        this.attemptsLeft = paymentInfo.getAttemptsLeft();
        this.checkAmount = paymentInfo.getCheckAmount();
        this.insertTimestamp = paymentInfo.getInsertTimestamp();
        this.verifiedTimestamp = paymentInfo.getVerifiedTimestamp();
        this.pin = paymentInfo.getPin();
        this.checkShopLogin = paymentInfo.getCheckShopLogin();
        this.checkShopTransactionID = paymentInfo.getCheckShopTransactionId();
        this.checkBankTransactionID = paymentInfo.getCheckBankTransactionID();
        this.checkCurrency = paymentInfo.getCheckCurrency();
        this.pinCheckAttemptsLeft = paymentInfo.getPinCheckAttemptsLeft();
    }

    public PaymentInfo toPaymentInfo() {

        PaymentInfo paymentInfo = new PaymentInfo();

        paymentInfo.setId(this.id);
        paymentInfo.setType(this.type);
        paymentInfo.setBrand(this.brand);
        paymentInfo.setExpirationDate(this.expirationDate);
        paymentInfo.setPan(this.pan);
        paymentInfo.setStatus(this.status);
        paymentInfo.setMessage(this.message);
        paymentInfo.setToken(this.token);
        paymentInfo.setToken3ds(this.token3ds);
        paymentInfo.setDefaultMethod(this.defaultMethod);
        paymentInfo.setAttemptsLeft(this.attemptsLeft);
        paymentInfo.setCheckAmount(this.checkAmount);
        paymentInfo.setInsertTimestamp(this.insertTimestamp);
        paymentInfo.setVerifiedTimestamp(this.verifiedTimestamp);
        paymentInfo.setPin(this.pin);
        paymentInfo.setCheckShopLogin(this.checkShopLogin);
        paymentInfo.setCheckShopTransactionId(this.checkShopTransactionID);
        paymentInfo.setCheckBankTransactionID(this.checkBankTransactionID);
        paymentInfo.setCheckCurrency(this.checkCurrency);
        paymentInfo.setPinCheckAttemptsLeft(this.pinCheckAttemptsLeft);

        return paymentInfo;
    }

    /*
     * public void setStatusToError() {
     * 
     * this.status = PaymentInfo.PAYMENTINFO_STATUS_ERROR;
     * this.defaultMethod = false;
     * this.message = "Deposit payment method timeout";
     * }
     * 
     * 
     * public void setStatusToCanceled() {
     * 
     * this.status = PaymentInfo.PAYMENTINFO_STATUS_CANCELED;
     * this.defaultMethod = false;
     * this.message = "Payment method canceled";
     * }
     */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getCardBin() {
        return cardBin;
    }

    public void setCardBin(String cardBin) {
        this.cardBin = cardBin;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken3ds() {
        return token3ds;
    }

    public void setToken3ds(String token3ds) {
        this.token3ds = token3ds;
    }

    public Boolean getDefaultMethod() {
        return defaultMethod;
    }

    public void setDefaultMethod(Boolean defaultMethod) {
        this.defaultMethod = defaultMethod;
    }

    public Integer getAttemptsLeft() {
        return attemptsLeft;
    }

    public void setAttemptsLeft(Integer attemptsLeft) {
        this.attemptsLeft = attemptsLeft;
    }

    public Double getCheckAmount() {
        return checkAmount;
    }

    public void setCheckAmount(Double checkAmount) {
        this.checkAmount = checkAmount;
    }

    public Date getInsertTimestamp() {
        return insertTimestamp;
    }

    public void setInsertTimestamp(Date insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    public Date getVerifiedTimestamp() {
        return verifiedTimestamp;
    }

    public void setVerifiedTimestamp(Date verifiedTimestamp) {
        this.verifiedTimestamp = verifiedTimestamp;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getCheckShopLogin() {
        return checkShopLogin;
    }

    public void setCheckShopLogin(String checkShopLogin) {
        this.checkShopLogin = checkShopLogin;
    }

    public String getCheckShopTransactionID() {
        return checkShopTransactionID;
    }

    public void setCheckShopTransactionID(String checkShopTransactionID) {
        this.checkShopTransactionID = checkShopTransactionID;
    }

    public String getCheckBankTransactionID() {
        return checkBankTransactionID;
    }

    public void setCheckBankTransactionID(String checkBankTransactionID) {
        this.checkBankTransactionID = checkBankTransactionID;
    }

    public String getCheckCurrency() {
        return checkCurrency;
    }

    public void setCheckCurrency(String checkCurrency) {
        this.checkCurrency = checkCurrency;
    }

    public Integer getPinCheckAttemptsLeft() {
        return pinCheckAttemptsLeft;
    }

    public void setPinCheckAttemptsLeft(Integer pinCheckAttemptsLeft) {
        this.pinCheckAttemptsLeft = pinCheckAttemptsLeft;
    }

    public String getHash() {
        return hash;
    }
    
    public void setHash(String hash) {
        this.hash = hash;
    }
    
    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }
    
    public boolean isValid() {
        return (getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED);
    }
}
