package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "SMS_LOG")
@NamedQueries({
    @NamedQuery(name = "SmsLogBean.findByCorrelationID", query = "select s from SmsLogBean s where s.correlationID = :correlationID"),
    @NamedQuery(name = "SmsLogBean.findAll", query = "select s from SmsLogBean s"),
    @NamedQuery(name = "SmsLogBean.findByStatusPending", query = "select s from SmsLogBean s where s.status = 2 and s.retryAttemptsLeft > 0")
})

public class SmsLogBean {

    public static final String FIND_BY_CORRELATIONID = "SmsLogBean.findByCorrelationID";
    public static final String FIND_BY_STATUS_PENDING = "SmsLogBean.findByStatusPending";
    public static final String FIND_ALL = "SmsLogBean.findAll";

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "correlation_id", nullable = false)
    private String             correlationID;

    @Column(name = "destination_address", nullable = false)
    private String             destinationAddress;

    @Column(name = "creation_timestamp", nullable = false)
    private Date               creationTimestamp;
    
    @Column(name = "provider_timestamp")
    private Date                providerTimestamp;

    @Column(name = "operator_timestamp")
    private Date                operatorTimestamp;

    @Column(name = "last_retry_timestamp")
    private Date               lastRetryTimestamp;

    @Column(name = "mt_message_id")
    private String             mtMessageID;

    @Column(name = "operator")
    private String             operator;

    @Column(name = "message")
    private String             message;

    @Lob
    @Column(name = "response_message")
    private String             responseMessage;

    @Column(name = "status", nullable = false)
    private Integer            status;

    @Column(name = "response_code")
    private Integer            responseCode;

    @Column(name = "reason_code")
    private Integer            reasonCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserBean           user;

    @Column(name = "retry_attempts_left", nullable = false)
    private Integer            retryAttemptsLeft;

    public SmsLogBean() {}

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getProviderTimestamp() {
        return providerTimestamp;
    }

    public void setProviderTimestamp(Date providerTimestamp) {
        this.providerTimestamp = providerTimestamp;
    }

    public Date getOperatorTimestamp() {
        return operatorTimestamp;
    }

    public void setOperatorTimestamp(Date operatorTimestamp) {
        this.operatorTimestamp = operatorTimestamp;
    }

    public Date getLastRetryTimestamp() {
        return lastRetryTimestamp;
    }

    public void setLastRetryTimestamp(Date lastRetryTimestamp) {
        this.lastRetryTimestamp = lastRetryTimestamp;
    }

    public String getCorrelationID() {
        return correlationID;
    }

    public void setCorrelationID(String correlationID) {
        this.correlationID = correlationID;
    }

    public String getMtMessageID() {
        return mtMessageID;
    }

    public void setMtMessageID(String mtMessageID) {
        this.mtMessageID = mtMessageID;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public Integer getRetryAttemptsLeft() {
        return retryAttemptsLeft;
    }

    public void setRetryAttemptsLeft(Integer retryAttemptsLeft) {
        this.retryAttemptsLeft = retryAttemptsLeft;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
    
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
