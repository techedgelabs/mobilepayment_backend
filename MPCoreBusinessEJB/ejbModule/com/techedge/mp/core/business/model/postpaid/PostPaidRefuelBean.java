package com.techedge.mp.core.business.model.postpaid;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShopRefuelDetail;

@Entity
@Table(name = "POSTPAIDREFUEL")
@NamedQueries({
//	@NamedQuery(name="TransactionBean.findTransactionByID", query="select t from TransactionBean t where t.transactionID = :transactionID"),
//	@NamedQuery(name="TransactionBean.findTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean"),
//	@NamedQuery(name="TransactionBean.findTransactionByUserAndDate", query="select t from TransactionBean t where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),
//	@NamedQuery(name="TransactionBean.findTransactionSuccessfulByUserAndDate", query="select t from TransactionBean t where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) and t.finalStatusType = 'SUCCESSFUL' order by t.creationTimestamp desc"),
//	//@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and (( t.finalStatusType is null ) or ( t.finalStatusType <> 'FAILED' and t.finalStatusType <> 'SUCCESSFUL' and t.finalStatusType <> 'MISSING_NOTIFICATION' ))")
//	@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and t.confirmed = false"),
//	@NamedQuery(name="TransactionBean.findActiveTransaction", query="select t from TransactionBean t where t.confirmed = false"),
//	@NamedQuery(name="TransactionBean.findActiveTransactionNew", query="select t from TransactionBean t where t.finalStatusType = null"),
//	@NamedQuery(name="TransactionBean.findTransactionByFinalStatusType", query="select t from TransactionBean t where t.finalStatusType = :finalStatusType"),
//	@NamedQuery(name="TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus", query="select t from TransactionBean t where t.finalStatusType = :finalStatusType and t.GFGNotification = :gfgNotification"),
@NamedQuery(name = "TransactionBean.findTransactionByPump", query = "select t from PostPaidRefuelBean t where t.pumpNumber = :pumpNumber order by timestampEndRefuel desc")

})
public class PostPaidRefuelBean {

    //	public static final String FIND_BY_ID = "TransactionBean.findTransactionByID";
    //	public static final String FIND_BY_USER = "TransactionBean.findTransactionByUser";
    //	public static final String FIND_ACTIVE_BY_USER = "TransactionBean.findActiveTransactionByUser";
    //	public static final String FIND_ACTIVE = "TransactionBean.findActiveTransaction";
    //	public static final String FIND_ACTIVE_NEW = "TransactionBean.findActiveTransactionNew";
    //	public static final String FIND_BY_FINAL_STATUS_TYPE = "TransactionBean.findTransactionByFinalStatusType";
    //	public static final String FIND_BY_FINAL_STATUS_TYPE_AND_NOTIFICATION = "TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus";
    //	public static final String FIND_BY_USER_AND_DATE = "TransactionBean.findTransactionByUserAndDate";
    //	public static final String FIND_SUCCESSFUL_BY_USER_AND_DATE = "TransactionBean.findTransactionSuccessfulByUserAndDate";
    public static final String      FIND_TRANSACTION_BY_PUMP = "TransactionBean.findTransactionByPump";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                    id;

    @Column(name = "fuelType", nullable = true)
    private String                  fuelType;

    @Column(name = "productId", nullable = true)
    private String                  productId;

    @Column(name = "productDescription", nullable = true)
    private String                  productDescription;

    @Column(name = "fuelQuantity", nullable = true)
    private Double                  fuelQuantity;

    @Column(name = "fuelAmount", nullable = true)
    private Double                  fuelAmount;
    
    @Column(name = "unitPrice", nullable = true)
    private Double                  unitPrice;

    @Column(name = "pumpId", nullable = true)
    private String                  pumpId;

    @Column(name = "pumpNumber", nullable = true)
    private Integer                 pumpNumber;

    @Column(name = "refuelMode", nullable = true)
    private String                  refuelMode;

    @Column(name = "timestampEndRefuel", nullable = true)
    private Date                    timestampEndRefuel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactionID", nullable = false)
    private PostPaidTransactionBean ppTransactionBean;

    public PostPaidRefuelBean() {}

    public PostPaidRefuelBean(PostPaidRefuel postPaidRefuel) {

        this.id = postPaidRefuel.getId();
        this.fuelType = postPaidRefuel.getFuelType();
        this.productId = postPaidRefuel.getProductId();
        this.productDescription = postPaidRefuel.getProductDescription();
        this.fuelQuantity = postPaidRefuel.getFuelQuantity();
        this.fuelAmount = postPaidRefuel.getFuelAmount();
        this.unitPrice = postPaidRefuel.getUnitPrice();
        this.pumpId = postPaidRefuel.getPumpId();
        this.pumpNumber = postPaidRefuel.getPumpNumber();
        this.refuelMode = postPaidRefuel.getRefuelMode();
        this.timestampEndRefuel = postPaidRefuel.getTimestampEndRefuel();
    }

    public PostPaidRefuel toPostPaidRefuel() {

        PostPaidRefuel postPaidRefuel = new PostPaidRefuel();
        postPaidRefuel.setId(this.id);
        postPaidRefuel.setFuelType(this.fuelType);
        postPaidRefuel.setProductId(this.productId);
        postPaidRefuel.setProductDescription(this.productDescription);
        postPaidRefuel.setFuelQuantity(this.fuelQuantity);
        postPaidRefuel.setFuelAmount(this.fuelAmount);
        postPaidRefuel.setUnitPrice(this.unitPrice);
        postPaidRefuel.setPumpId(this.pumpId);
        postPaidRefuel.setPumpNumber(this.pumpNumber);
        postPaidRefuel.setRefuelMode(this.refuelMode);
        postPaidRefuel.setTimestampEndRefuel(this.timestampEndRefuel);
        return postPaidRefuel;
    }

    public PostPaidRefuelBean(PostPaidRefuelHistoryBean postPaidRefuelBean) {
        this.fuelType = postPaidRefuelBean.getFuelType();
        this.fuelQuantity = postPaidRefuelBean.getFuelQuantity();
        this.fuelAmount = postPaidRefuelBean.getFuelAmount();
        this.productDescription = postPaidRefuelBean.getProductDescription();
        this.productId = postPaidRefuelBean.getProductId();
        this.pumpId = postPaidRefuelBean.getPumpId();
        if (postPaidRefuelBean.getPumpNumber() != null) {
            this.pumpNumber = Integer.valueOf(postPaidRefuelBean.getPumpNumber());
        }
        this.refuelMode = postPaidRefuelBean.getRefuelMode();
        this.unitPrice = postPaidRefuelBean.getUnitPrice();
        this.timestampEndRefuel = postPaidRefuelBean.getTimestampEndRefuel();
    }

    public PostPaidRefuelBean(ShopRefuelDetail shopRefuelDetail, PostPaidTransactionBean ppTransactionBean) {
        this.fuelType = shopRefuelDetail.getFuelType();
        this.fuelQuantity = shopRefuelDetail.getFuelQuantity();
        this.fuelAmount = shopRefuelDetail.getAmount();
        this.productDescription = shopRefuelDetail.getProductDescription();
        this.productId = shopRefuelDetail.getProductId();
        this.pumpId = shopRefuelDetail.getPumpID();
        if (shopRefuelDetail.getPumpNumber() != null) {
            this.pumpNumber = Integer.valueOf(shopRefuelDetail.getPumpNumber());
        }
        this.refuelMode = shopRefuelDetail.getRefuelMode();
        this.unitPrice = shopRefuelDetail.getUnitPrice();

        this.ppTransactionBean = ppTransactionBean;

    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public Double getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(Double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public PostPaidTransactionBean getTransactionBean() {
        return ppTransactionBean;
    }

    public void setTransactionBean(PostPaidTransactionBean ppTransactionBean) {
        this.ppTransactionBean = ppTransactionBean;
    }

    public String getPumpId() {
        return pumpId;
    }

    public void setPumpId(String pumpId) {
        this.pumpId = pumpId;
    }

    public Date getTimestampEndRefuel() {
        return timestampEndRefuel;
    }

    public void setTimestampEndRefuel(Date timestampEndRefuel) {
        this.timestampEndRefuel = timestampEndRefuel;
    }

    public Integer getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(Integer pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    public PostPaidTransactionBean getPpTransactionBean() {
        return ppTransactionBean;
    }

    public void setPpTransactionBean(PostPaidTransactionBean ppTransactionBean) {
        this.ppTransactionBean = ppTransactionBean;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
