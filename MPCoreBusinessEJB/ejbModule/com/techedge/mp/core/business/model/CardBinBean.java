package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="CARD_BIN")
@NamedQuery(name="CardBinBean.findByNumber", query="select b from CardBinBean b where b.number = :number")

public class CardBinBean {

    public static final String FIND_BY_NUMBER       = "CardBinBean.findByNumber";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "number", nullable = false)
    private String             number;

    public CardBinBean() {}
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
