package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "POLLING_INTERVAL")

@NamedQueries({ @NamedQuery(name = "PollingIntervalBean.findByStatus", query = "select i from PollingIntervalBean i where i.status = :status"),
    @NamedQuery(name = "PollingIntervalBean.findAll", query = "select i from PollingIntervalBean i")})
public class PollingIntervalBean {

    public static final String FIND_BY_STATUS = "PollingIntervalBean.findByStatus";
    public static final String FIND_ALL = "PollingIntervalBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "status", nullable = false)
    private String             status;

    @Column(name = "next_polling_interval", nullable = false)
    private Integer            nextPollingInterval;

    public PollingIntervalBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNextPollingInterval() {
        return nextPollingInterval;
    }

    public void setNextPollingInterval(Integer nextPollingInterval) {
        this.nextPollingInterval = nextPollingInterval;
    }

}
