package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.LastLoginErrorData;

@Entity
@Table(name="LASTLOGINERRORDATA")
public class LastLoginErrorDataBean {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	
//	@Column(name="user_id", nullable=true)
//	private String userId;
	
	@Column(name="time", nullable=true)
	private Date time;

	@Column(name="attempt", nullable=true)
	private Integer attempt = 0;
	
	
    public LastLoginErrorDataBean() {}
    
    public LastLoginErrorDataBean(LastLoginErrorData lastLoginErrorData) {
    	
    	this.id = lastLoginErrorData.getId();
    	this.time = lastLoginErrorData.getTime();
//    	this.userId = lastLoginErrorData.getUserId();
    	this.attempt = lastLoginErrorData.getAttempt();
    	
    }
    
    public LastLoginErrorData toLastLoginErrorData() {
    	
    	LastLoginErrorData lastLoginData = new LastLoginErrorData();
    	
    	lastLoginData.setId(this.id);
    	lastLoginData.setTime(this.time);
//    	lastLoginData.setUserId(this.userId);
    	lastLoginData.setAttempt(this.attempt);
    			
		return lastLoginData;
    }

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}

//	public String getUserId() {
//		return userId;
//	}
//
//	public void setUserId(String userId) {
//		this.userId = userId;
//	}

	public Integer getAttempt() {
		return attempt;
	}

	public void setAttempt(Integer attempt) {
		this.attempt = attempt;
	}
	
	public void addAttempt() {
		this.attempt = this.attempt+1;
	}
	
	
	public void resetAttempts() {
		this.attempt = 0;
		this.time = new Date();
	}
	
	
	
}
