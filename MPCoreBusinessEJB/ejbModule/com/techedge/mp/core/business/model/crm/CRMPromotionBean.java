package com.techedge.mp.core.business.model.crm;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_PROMOTION")
@NamedQueries({
    @NamedQuery(name = "CRMPromotionBean.SelectAll", query = "select cpb from CRMPromotionBean cpb")
})
public class CRMPromotionBean {

    public static final String SELECT_ALL = "CRMPromotionBean.SelectAll";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long   id;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "treatment_code")
    private String treatmentCode;

    @Column(name = "start_date")
    private Date   startDate;

    @Column(name = "end_date")
    private Date   endDate;

    @Column(name = "link")
    private String link;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "C_2")
    private String C_2;

    @Column(name = "C_3")
    private String C_3;

    @Column(name = "C_4")
    private String C_4;

    @Column(name = "C_5")
    private String C_5;

    @Column(name = "C_6")
    private String C_6;

    @Column(name = "C_7")
    private String C_7;

    @Column(name = "C_8")
    private String C_8;

    @Column(name = "C_9")
    private String C_9;

    @Column(name = "C_10")
    private String C_10;

    public CRMPromotionBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTreatmentCode() {
        return treatmentCode;
    }

    public void setTreatmentCode(String treatmentCode) {
        this.treatmentCode = treatmentCode;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getC_2() {
        return C_2;
    }

    public void setC_2(String c_2) {
        C_2 = c_2;
    }

    public String getC_3() {
        return C_3;
    }

    public void setC_3(String c_3) {
        C_3 = c_3;
    }

    public String getC_4() {
        return C_4;
    }

    public void setC_4(String c_4) {
        C_4 = c_4;
    }

    public String getC_5() {
        return C_5;
    }

    public void setC_5(String c_5) {
        C_5 = c_5;
    }

    public String getC_6() {
        return C_6;
    }

    public void setC_6(String c_6) {
        C_6 = c_6;
    }

    public String getC_7() {
        return C_7;
    }

    public void setC_7(String c_7) {
        C_7 = c_7;
    }

    public String getC_8() {
        return C_8;
    }

    public void setC_8(String c_8) {
        C_8 = c_8;
    }

    public String getC_9() {
        return C_9;
    }

    public void setC_9(String c_9) {
        C_9 = c_9;
    }

    public String getC_10() {
        return C_10;
    }

    public void setC_10(String c_10) {
        C_10 = c_10;
    }

}
