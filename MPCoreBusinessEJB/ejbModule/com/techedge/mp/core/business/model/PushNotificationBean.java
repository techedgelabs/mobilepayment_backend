package com.techedge.mp.core.business.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.model.loyalty.EventNotificationBean;

@Entity
@Table(name = "PUSH_NOTIFICATION")
@NamedQueries({ 
    @NamedQuery(name = "PushNotificationBean.findByID", query = "select p from PushNotificationBean p where p.id = :id"),
    @NamedQuery(name = "PushNotificationBean.findToReconcilie", query = "select p from PushNotificationBean p where p.toReconcilie = 1"),
})
public class PushNotificationBean implements UserReconciliationInterfaceBean {

    public static final String             FIND_BY_ID                = "PushNotificationBean.findByID";
    public static final String FIND_TO_RECONCILIE                    = "PushNotificationBean.findToReconcilie";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                           id;

    @Column(name = "publish_message_id")
    private String                         publishMessageID;

    @Column(name = "endpoint")
    private String                         endpoint;

    @Column(name = "sending_timestamp", nullable = false)
    private Date                           sendingTimestamp;

    @Column(name = "expiring_timestamp", nullable = false)
    private Date                           expiringTimestamp;

    @Column(name = "source")
    private String                         source;

    @Column(name = "title")
    private String                         title;

    @Column(name = "message")
    private String                         message;

    @Column(name = "content_type")
    private String                         contentType;

    @Column(name = "status_code")
    private Integer                        statusCode;

    @Column(name = "status_message")
    private String                         statusMessage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserBean                       user;

    @Column(name = "retry_attempts_left", nullable = false)
    private Integer                        retryAttemptsLeft;
    
    @Column(name = "to_reconcilie")
    private Boolean            toReconcilie;
    
    @Column(name = "notification_read", nullable = true, columnDefinition="bit default 0")
    private Boolean                notificationRead = false;

    @Column(name = "notification_read_timestamp", nullable = true)
    private Date                   notificationReadTimestamp;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pushNotificationBean")
    private Set<ParameterNotificationBean> parameterNotificationBean = new HashSet<ParameterNotificationBean>(0);

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "pushNotificationBean")
    private EventNotificationBean          eventNotificationBean;
    /*
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pushNotificationBean")
    private Set<CRMOutboundBean>          crmOutboundBeanList = new HashSet<CRMOutboundBean>();
    */
    public PushNotificationBean() {}

    public Long getId() {
        return id;
    }

    public Date getSendingTimestamp() {
        return sendingTimestamp;
    }

    public void setSendingTimestamp(Date sendingTimestamp) {
        this.sendingTimestamp = sendingTimestamp;
    }

    public Date getExpiringTimestamp() {
        return expiringTimestamp;
    }

    public void setExpiringTimestamp(Date expiringTimestamp) {
        this.expiringTimestamp = expiringTimestamp;
    }

    public String getPublishMessageID() {
        return publishMessageID;
    }

    public void setPublishMessageID(String publishMessageID) {
        this.publishMessageID = publishMessageID;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }
    
    public PushNotificationSourceType getSource() {
        return PushNotificationSourceType.valueOf(source);
    }

    public void setSource(PushNotificationSourceType source) {
        this.source = source.name();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PushNotificationContentType getContentType() {
        return PushNotificationContentType.valueOf(contentType);
    }

    public void setContentType(PushNotificationContentType contentType) {
        this.contentType = contentType.name();
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(PushNotificationStatusType statusCode) {
        this.statusCode = statusCode.getCode();
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public Integer getRetryAttemptsLeft() {
        return retryAttemptsLeft;
    }

    public void setRetryAttemptsLeft(Integer retryAttemptsLeft) {
        this.retryAttemptsLeft = retryAttemptsLeft;
    }

    public Set<ParameterNotificationBean> getParameterNotificationBean() {
        return parameterNotificationBean;
    }

    public void setParameterNotificationBean(Set<ParameterNotificationBean> parameterNotificationBean) {
        this.parameterNotificationBean = parameterNotificationBean;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public EventNotificationBean getEventNotificationBean() {
        return eventNotificationBean;
    }

    public void setEventNotificationBean(EventNotificationBean eventNotificationBean) {
        this.eventNotificationBean = eventNotificationBean;
    }

    public void setId(Long id) {
        this.id = id;
    }
    /*
    public Set<CRMOutboundBean> getCrmOutboundBeanList() {
        return crmOutboundBeanList;
    }

    public void setCrmOutboundBeanList(Set<CRMOutboundBean> crmOutboundBeanList) {
        this.crmOutboundBeanList = crmOutboundBeanList;
    }
    */
    @Override
    public UserBean getUserBean() {
        return getUser();
    }

    @Override
    public String getFinalStatus() {
        return PushNotificationStatusType.getStatusType(getStatusCode()).name();
    }

    @Override
    public void setFinalStatus(String status) {
        PushNotificationStatusType statusType = PushNotificationStatusType.valueOf(status);
        setStatusCode(statusType);
    }

    @Override
    public void setToReconcilie(Boolean toReconcilie) {
        //this.toReconcilie = toReconcilie;
        this.toReconcilie = false;
    }

    @Override
    public Boolean getToReconcilie() {
        return toReconcilie;
    }

    @Override
    public Integer getReconciliationAttemptsLeft() {
        return retryAttemptsLeft;
    }

    @Override
    public void setReconciliationAttemptsLeft(Integer attemptsLeft) {
        this.retryAttemptsLeft = attemptsLeft;
    }
    
    @Override
    public String getOperationName() {
        return "PUBLISH_MESSAGE";
    }
    
    @Override
    public Date getOperationTimestamp() {
        return getSendingTimestamp();
    }

    public Boolean getNotificationRead() {
        if (notificationRead == null) {
            return Boolean.FALSE;
        }
        
        return notificationRead;
    }

    public void setNotificationRead(Boolean notificationRead) {
        this.notificationRead = notificationRead;
    }

    public Date getNotificationReadTimestamp() {
        return notificationReadTimestamp;
    }

    public void setNotificationReadTimestamp(Date notificationReadTimestamp) {
        this.notificationReadTimestamp = notificationReadTimestamp;
    }
    
    @Override
    public String getFinalStatusMessage() {
        return statusMessage;
    }
    
    @Override
    public String getServerEndpoint() {
        return "Push Notification Service AWS";
    }
}
