package com.techedge.mp.core.business.model;

import java.math.BigDecimal;
import java.util.Date;


public interface ParkingTransactionInterfaceBean {


    public long getId();
    
    public UserBean getUserBean();

    public String getFinalStatus();
    
    public void setFinalStatus(String status);
    
    public void setToReconcilie(Boolean toReconcilie);
    
    public Boolean getToReconcilie();

    public Integer getReconciliationAttemptsLeft();
    
    public void setReconciliationAttemptsLeft(Integer attemptsLeft);
    
    public Date getTimestamp();
    
    public String getCityName();
    
    public String getParkingZoneName();
    
    public String getParkingId();
    
    public BigDecimal getFinalPrice();
}
