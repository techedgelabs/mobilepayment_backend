package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="HISTORYPASSWORDDATA")
public class HistoryPasswordBean {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="personalDataId", nullable=false)
    private PersonalDataBean personalDataBean;
    
    @Column(name="password", nullable=false)
    private String password;
    
    
    public HistoryPasswordBean(){};

    
	public PersonalDataBean getPersonalDataBean() {
		return personalDataBean;
	}
	public void setPersonalDataBean(PersonalDataBean personalDataBean) {
		this.personalDataBean = personalDataBean;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
