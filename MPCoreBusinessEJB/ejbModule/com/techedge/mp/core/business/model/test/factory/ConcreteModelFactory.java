package com.techedge.mp.core.business.model.test.factory;

import java.sql.Date;
import java.util.concurrent.ConcurrentHashMap;

import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PromoVoucherBean;
import com.techedge.mp.core.business.model.PromotionBean;
import com.techedge.mp.core.business.model.UserBean;

public class ConcreteModelFactory extends ModelFactory {

    public static final String VOUCHER_CODE_002 = "002";
    public static final String VOUCHER_CODE_001 = "001";

    public ConcreteModelFactory() {}

    @SuppressWarnings("deprecation")
    @Override
    public PromotionBean createPromotion() {

        return new PromotionBean("Promo1", "Promozione", new Date(2016, 1, 12), new Date(2016, 3, 12));
    }

    @Override
    public PromoVoucherBean createPromoVoucher() {
        return new PromoVoucherBean(VOUCHER_CODE_001, 100.0);
    }

    @Override
    public PromoVoucherInput createPromoVoucherInput() {

        PromoVoucherInput voucher = new PromoVoucherInput();

        ConcurrentHashMap<String, Double> promoAssociation = voucher.getPromoAssociation();
        promoAssociation.put(VOUCHER_CODE_001, 100D);
        promoAssociation.put(VOUCHER_CODE_002, 200D);

        voucher.setPromoAssociation(promoAssociation);

        return voucher;
    }

    @Override
    public UserBean createUserBean() {

        UserBean user = new UserBean();
        user.setUserStatus(User.USER_STATUS_VERIFIED);

        return user;
    }

}
