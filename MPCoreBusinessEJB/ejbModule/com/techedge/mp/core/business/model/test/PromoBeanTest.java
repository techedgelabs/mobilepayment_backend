package com.techedge.mp.core.business.model.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.techedge.mp.core.business.exceptions.PromotionException;
import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.core.business.model.PromoVoucherBean;
import com.techedge.mp.core.business.model.PromotionBean;
import com.techedge.mp.core.business.model.test.factory.ConcreteModelFactory;
import com.techedge.mp.core.business.model.test.factory.ModelFactory;

public class PromoBeanTest {

    PromotionBean           promoBean;
    PromoVoucherBean        voucherPromoBean;
    PromoVoucherInput       promoVoucherInput;
//    PromoVoucherInput       listInputPromoVoucher;

    @Before
    public void setUp() {

        ModelFactory modelObjec = new ConcreteModelFactory();
        promoBean = modelObjec.createPromotion();
        voucherPromoBean = modelObjec.createPromoVoucher();

        List<PromoVoucherBean> listVoucherPromo = new ArrayList<PromoVoucherBean>();
        listVoucherPromo.add(voucherPromoBean);
        promoBean.setPromoVoucherList(listVoucherPromo);

        promoVoucherInput = modelObjec.createPromoVoucherInput();

        modelObjec.createPromoVoucherInput();
//        PromoVoucherInput voucherInputTwo = modelObjec.createPromoVoucherInput();
    }

    @Test
    public void testPromotionBuilder() {

        checkNotNull();
    }

    private void checkNotNull() {

        assertNotNull(promoBean.getDescription());
        assertNotNull(promoBean.getCode());
        assertNotNull(promoBean.getStartData());
        assertNotNull(promoBean.getEndData());
        assertNotNull(promoBean.getPromoVoucherList());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testCreatePromotion() throws PromotionException {

        Date startDate = new Date(2016, 2, 12);
        Date endDate = new Date(2016, 4, 12);

        promoBean = promoBean.createPromotion("PromotionCreate", "New promotion created", startDate, endDate);

        assertEquals("New promotion created", promoBean.getDescription());
        assertEquals("PromotionCreate", promoBean.getCode());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUpdatePromotion() throws PromotionException {

        Date startDate = new Date(2016, 2, 12);
        Date endDate = new Date(2016, 4, 12);

        promoBean.update("Updated", "Description Updated", startDate, endDate);
        assertEquals("Updated", promoBean.getCode());
        assertEquals("Description Updated", promoBean.getDescription());
        assertEquals(startDate, promoBean.getStartData());
        assertEquals(endDate, promoBean.getEndData());
    }

    @SuppressWarnings("deprecation")
    @Test(expected = PromotionException.class)
    public void testUpdateNameException() throws PromotionException {
        try {
            promoBean.update(null, "Description Updated", new Date(2016, 2, 12), new Date(2016, 4, 12));
        }
        catch (PromotionException e) {
            String message = e.getMessage();
            assertEquals(PromotionBean.NAME_OF_PROMOTION_CANNOT_BE_NULL, message);
            throw e;
        }
    }

    @SuppressWarnings("deprecation")
    @Test(expected = PromotionException.class)
    public void testUpdateDesciptionException() throws PromotionException {
        try {
            promoBean.update("NewPromotion", null, new Date(2016, 2, 12), new Date(2016, 4, 12));
        }
        catch (PromotionException e) {
            String message = e.getMessage();
            assertEquals(PromotionBean.DESCRIPTION_OF_PROMOTION_CANNOT_BE_NULL, message);
            throw e;
        }
    }

    @SuppressWarnings("deprecation")
    @Test(expected = PromotionException.class)
    public void testUpdateDateException() throws PromotionException {
        try {
            promoBean.update("NewPromotion", "Updated Description", null, new Date(2016, 4, 12));
        }
        catch (PromotionException e) {
            String message = e.getMessage();
            assertEquals(PromotionBean.START_DATE_OR_END_DATE_OF_PROMOTION_ARE_NULL, message);
            throw e;
        }
    }

    @SuppressWarnings("deprecation")
    @Test(expected = PromotionException.class)
    public void testUpdateEndDateBefore() throws PromotionException {
        try {
            promoBean.update("NewPromotion", "Updated Description", new Date(2016, 4, 12), new Date(2016, 2, 12));
        }
        catch (PromotionException e) {
            String message = e.getMessage();
            assertEquals(PromotionBean.START_DATE_OR_END_DATE_OF_PROMOTION_ARE_INCORRECT, message);
            throw e;
        }
    }

    @Test(expected = PromotionException.class)
    public void testAddPromoVouchersWithCodeExist() throws PromotionException {

        try {
            
        promoBean.addPromoVouchers(promoVoucherInput);
        }
        catch (PromotionException e) {
            
            String message = e.getMessage();
            assertEquals("Promotion with same code already exist", message);
            throw e;
        }
    }
    
    @Test
    public void testAddPromoVouchers() throws PromotionException {

        promoBean.setPromoVoucherList(new ArrayList<PromoVoucherBean>(0));
                
        promoBean.addPromoVouchers(promoVoucherInput);

        assertEquals(2, promoBean.getPromoVoucherList().size());
    }

    @Test
    public void testRemovePromoVouchers() throws PromotionException {

        promoBean.removePromoVoucher(ConcreteModelFactory.VOUCHER_CODE_001);

        assertEquals(0, promoBean.getPromoVoucherList().size());
    }
}
