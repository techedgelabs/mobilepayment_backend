package com.techedge.mp.core.business.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.exceptions.ManagerException;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.utilities.EncoderHelper;

@Entity
@Table(name = "MANAGER")
@NamedQueries({
    @NamedQuery(name = "ManagerBean.findManagerByUsername", query = "select a from ManagerBean a where a.username = :username"),
    @NamedQuery(name = "ManagerBean.findManagerByLikeUsername", query = "select a from ManagerBean a where a.username like :username order by a.id"),
    @NamedQuery(name = "ManagerBean.findManagerByEmail", query = "select a from ManagerBean a where a.email = :email"),
    @NamedQuery(name = "ManagerBean.findManagerByLikeEmail", query = "select a from ManagerBean a where a.email like :email order by a.id"),
    @NamedQuery(name = "ManagerBean.findManagerByUsernameOREmail", query = "select a from ManagerBean a where a.username = :username or a.email = :email"),
    @NamedQuery(name = "ManagerBean.findManagerByLikeUsernameORLikeEmail", query = "select a from ManagerBean a where a.username like :username or a.email like :email order by a.id"),
    @NamedQuery(name = "ManagerBean.findManagerById", query = "select a from ManagerBean a where a.id = :managerId order by a.id"),
    @NamedQuery(name = "ManagerBean.getAll", query = "select a from ManagerBean a order by a.id")
})

public class ManagerBean {

  public static final String FIND_BY_USERNAME = "ManagerBean.findManagerByUsername";
  public static final String FIND_BY_LIKE_USERNAME = "ManagerBean.findManagerByLikeUsername";
  public static final String FIND_BY_EMAIL = "ManagerBean.findManagerByEmail";
  public static final String FIND_BY_LIKE_EMAIL = "ManagerBean.findManagerByLikeEmail";
  public static final String FIND_BY_USERNAME_OR_EMAIL = "ManagerBean.findManagerByUsernameOREmail";
  public static final String FIND_BY_LIKE_USERNAME_OR_LIKE_EMAIL = "ManagerBean.findManagerByLikeUsernameORLikeEmail";
  public static final String FIND_BY_ID = "ManagerBean.findManagerById";
  public static final String GET_ALL = "ManagerBean.getAll";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "username", nullable = false)
  private String username;

  @Column(name = "password", nullable = false)
  private String password;

  @Column(name = "email", nullable = false)
  private String email;

  @Column(name = "firstname", nullable = true)
  private String firstName;

  @Column(name = "lastname", nullable = true)
  private String lastName;

  @Column(name = "status", nullable = false)
  private Integer status;
  
  @Column(name="type", nullable=false, length=1)
  private Integer type;
  
  @Column(name="rescuePassword", nullable=true)
  private String rescuePassword;

  @Column(name="expirationDateRescuePassword", nullable=true)
  private Date expirationDateRescuePassword;
  
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "MANAGERS_STATIONS")
  private Set<StationBean> stations = new HashSet<StationBean>(0);

  public ManagerBean() {}

  public ManagerBean(Manager manager) {

    this.id = manager.getId();
    this.username = manager.getUsername();
    this.email = manager.getEmail();
    this.password = manager.getPassword();
    this.firstName = manager.getFirstName();
    this.lastName = manager.getLastName();
    this.status = manager.getStatus();
    this.type = manager.getType();

    if (manager.getStations() != null || !manager.getStations().isEmpty()) {

      for (Station station : manager.getStations()) {
        StationBean stationBean = new StationBean(station);
        stationBean.getManagers().add(this);
      }
    }
  }

  public Manager toManager() {

    Manager manager = new Manager();
    manager.setId(this.id);
    manager.setUsername(this.username);
    manager.setEmail(this.email);
    manager.setPassword(this.password);
    manager.setFirstName(this.firstName);
    manager.setLastName(this.lastName);
    manager.setStatus(this.status);
    manager.setType(this.type);

    if (this.stations != null || !this.stations.isEmpty()) {

      for (StationBean stationBean : this.stations) {
        manager.getStations().add(stationBean.toStation());
      }
    }

    return manager;
  }

  public static ManagerBean createNewManager(Manager manager) throws ManagerException {

    try {

      ManagerBean managerBean = new ManagerBean(manager);

      managerBean.password = EncoderHelper.encode(manager.getPassword());
      managerBean.status = Manager.STATUS_ACTIVE;

      return managerBean;
    }
    catch (Exception ex) {

      throw new ManagerException("Error in manager creation: " + ex.getMessage());
    }

  }

  public void updateManager(Manager manager) throws ManagerException {

      try {

        this.username = manager.getUsername();
        this.email = manager.getEmail();
        this.firstName = manager.getFirstName();
        this.lastName = manager.getLastName();
        this.status = manager.getStatus();
        this.type = manager.getType();
      }
      catch (Exception ex) {

        throw new ManagerException("Error in manager updating: " + ex.getMessage());
      }

    }

  public void setStatusToActive() throws ManagerException {

    this.status = Manager.STATUS_ACTIVE;
  }

  public void blockManager() throws ManagerException {

    this.status = Manager.STATUS_BLOCKED;
  }

  public Boolean canLogin() throws ManagerException {

    return (this.status == Manager.STATUS_ACTIVE);
  }

  public Boolean isPasswordValid(String encryptedPassword) throws ManagerException {

    return (this.password.equals(encryptedPassword));
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastname(String lastName) {
    this.lastName = lastName;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Set<StationBean> getStations() {
    return stations;
  }

  public void setStations(Set<StationBean> stations) {
    this.stations = stations;
  }
  
  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getRescuePassword() {
    return rescuePassword;
  }

  public void setRescuePassword(String rescuePassword) {
    this.rescuePassword = rescuePassword;
  }

  public Date getExpirationDateRescuePassword() {
    return expirationDateRescuePassword;
  }

  public void setExpirationDateRescuePassword(Date expirationDateRescuePassword) {
    this.expirationDateRescuePassword = expirationDateRescuePassword;
  }

  
}
