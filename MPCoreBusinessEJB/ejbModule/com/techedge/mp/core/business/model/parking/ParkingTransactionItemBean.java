package com.techedge.mp.core.business.model.parking;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.parking.ParkingTransactionItem;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransactionItemEvent;

@Entity
@Table(name = "PARKING_TRANSACTION_ITEM")
@NamedQueries({
        @NamedQuery(name = "ParkingTransactionItemBean.findUnsettledTransactionItemByParkingTransaction", query = "select pti from ParkingTransactionItemBean pti where pti.parkingTransactionBean.parkingTransactionId = :parkingTransactionId and parkingItemStatus != :status order by pti.creationTimestamp asc"),
        @NamedQuery(name = "ParkingTransactionItemBean.findActiveTransactionByUser", query = "select pti from ParkingTransactionItemBean pti where pti.parkingTransactionBean.userBean = :userBean and pti.parkingTransactionBean.status != :status1 and pti.parkingTransactionBean.status != :status2 and pti.parkingTransactionBean.status != :status3 order by pti.creationTimestamp asc"),
        @NamedQuery(name = "ParkingTransactionItemBean.findActiveTransactionItemByParkingTransaction", query = "select pti from ParkingTransactionItemBean pti where pti.parkingTransactionBean.parkingTransactionId = :parkingTransactionId and parkingItemStatus = :status order by pti.creationTimestamp asc"),

})
public class ParkingTransactionItemBean {

    public static final String                    FIND_UNSETTLED_TRANSACTION_ITEM_BY_PARKING_TRANSACTION = "ParkingTransactionItemBean.findUnsettledTransactionItemByParkingTransaction";
    public static final String                    FIND_ACTIVE_TRANSACTION_ITEM_BY_PARKING_TRANSACTION    = "ParkingTransactionItemBean.findActiveTransactionItemByParkingTransaction";
    public static final String                    FIND_ACTIVE_TRANSACTION_ITEM_BY_USER                   = "ParkingTransactionItemBean.findActiveTransactionByUser";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                                  id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parking_transaction_id", nullable = false)
    private ParkingTransactionBean                parkingTransactionBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parkingTransactionItemBean", cascade = CascadeType.ALL)
    private Set<ParkingTransactionItemEventBean>  parkingTransactionItemEventList                        = new HashSet<ParkingTransactionItemEventBean>(0);

    @Column(name = "parking_transaction_item_id", nullable = true)
    private String                                parkingTransactionItemId;

    @Column(name = "parking_start_time", nullable = true)
    private Date                                  parkingStartTime;

    @Column(name = "parking_end_time", nullable = true)
    private Date                                  parkingEndTime;

    @Column(name = "parking_time_correction", nullable = true)
    private String                                parkingTimeCorrection;

    @Column(name = "parking_item_status", nullable = false)
    private String                                parkingItemStatus;

    @Column(name = "price", nullable = true)
    private BigDecimal                            price;

    @Column(name = "current_price", nullable = true)
    private BigDecimal                            currentPrice;

    @Column(name = "price_difference", nullable = true)
    private BigDecimal                            priceDifference;

    @Column(name = "previous_price", nullable = true)
    private BigDecimal                            previousPrice;

    @Column(name = "requested_end_time", nullable = true)
    private Date                                  requestedEndTime;

    @Column(name = "payment_method_id", nullable = true)
    private Long                                  paymentMethodId;

    @Column(name = "payment_method_type", nullable = true)
    private String                                paymentMethodType;

    @Column(name = "server_name", nullable = true)
    private String                                serverName;

    @Column(name = "reconciliation_attempts_left", nullable = true)
    private Integer                               reconciliationAttemptsLeft;

    @Column(name = "group_acquirer", nullable = true)
    private String                                groupAcquirer;

    @Column(name = "currency", nullable = true)
    private String                                currency;

    @Column(name = "shop_login", nullable = true)
    private String                                shopLogin;

    @Column(name = "acquirer_id", nullable = true)
    private String                                acquirerID;

    @Column(name = "bank_transaction_id", nullable = true)
    private String                                bankTansactionID;

    @Column(name = "authorization_code", nullable = true)
    private String                                authorizationCode;

    @Column(name = "payment_token", nullable = true)
    private String                                paymentToken;
    
    @Column(name = "pan", nullable = true, length = 40)
    private String                                pan;
    
    @Column(name = "creation_timestamp", nullable = true)
    private Timestamp                             creationTimestamp;

    public ParkingTransactionItemBean() {
        this.creationTimestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
    }

    public ParkingTransactionItem toParkingTransactionItem() {

        ParkingTransactionItem parkingTransactionItem = new ParkingTransactionItem();
        parkingTransactionItem.setId(this.id);
        //parkingTransactionItem.setParkingTransaction(this.parkingTransactionBean.toParkingTransaction());
        parkingTransactionItem.setAcquirerID(this.acquirerID);
        parkingTransactionItem.setAuthorizationCode(this.authorizationCode);
        parkingTransactionItem.setBankTansactionID(this.bankTansactionID);
        parkingTransactionItem.setCurrency(this.currency);
        parkingTransactionItem.setGroupAcquirer(this.groupAcquirer);
        parkingTransactionItem.setParkingEndTime(this.parkingEndTime);
        parkingTransactionItem.setParkingItemStatus(this.parkingItemStatus);
        parkingTransactionItem.setParkingStartTime(this.parkingStartTime);
        parkingTransactionItem.setParkingTimeCorrection(this.parkingTimeCorrection);
        parkingTransactionItem.setPaymentMethodId(this.paymentMethodId);
        parkingTransactionItem.setPaymentMethodType(this.paymentMethodType);
        parkingTransactionItem.setPaymentToken(this.paymentToken);
        parkingTransactionItem.setPrice(this.price);
        parkingTransactionItem.setCurrentPrice(this.currentPrice);
        parkingTransactionItem.setPriceDifference(this.priceDifference);
        parkingTransactionItem.setPreviousPrice(this.previousPrice);        
        parkingTransactionItem.setReconciliationAttemptsLeft(this.reconciliationAttemptsLeft);
        parkingTransactionItem.setRequestedEndTime(this.requestedEndTime);
        parkingTransactionItem.setServerName(this.serverName);
        parkingTransactionItem.setShopLogin(this.shopLogin);
        parkingTransactionItem.setPan(this.pan);
        parkingTransactionItem.setCreationTimestamp(this.creationTimestamp);
        
        for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : this.parkingTransactionItemEventList) {
            
            ParkingTransactionItemEvent parkingTransactionItemEvent = parkingTransactionItemEventBean.toParkingTransactionItemEvent();
            parkingTransactionItemEvent.setParkingTransactionItem(parkingTransactionItem);
            parkingTransactionItem.getParkingTransactionItemEventList().add(parkingTransactionItemEvent);
        }

        return parkingTransactionItem;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ParkingTransactionBean getParkingTransactionBean() {
        return parkingTransactionBean;
    }

    public void setParkingTransactionBean(ParkingTransactionBean parkingTransactionBean) {
        this.parkingTransactionBean = parkingTransactionBean;
    }

    public Date getParkingStartTime() {
        return parkingStartTime;
    }

    public void setParkingStartTime(Date parkingStartTime) {
        this.parkingStartTime = parkingStartTime;
    }

    public Date getParkingEndTime() {
        return parkingEndTime;
    }

    public void setParkingEndTime(Date parkingEndTime) {
        this.parkingEndTime = parkingEndTime;
    }

    public void setRequestedEndTime(Date requestedEndTime) {
        this.requestedEndTime = requestedEndTime;
    }

    public String getParkingTimeCorrection() {
        return parkingTimeCorrection;
    }

    public void setParkingTimeCorrection(String parkingTimeCorrection) {
        this.parkingTimeCorrection = parkingTimeCorrection;
    }

    public String getParkingItemStatus() {
        return parkingItemStatus;
    }

    public void setParkingItemStatus(String parkingItemStatus) {
        this.parkingItemStatus = parkingItemStatus;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getRequestedEndTime() {
        return requestedEndTime;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public String getGroupAcquirer() {
        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {
        this.groupAcquirer = groupAcquirer;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
        this.acquirerID = acquirerID;
    }

    public String getBankTansactionID() {
        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {
        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getPaymentToken() {
        return paymentToken;
    }

    public void setPaymentToken(String paymentToken) {
        this.paymentToken = paymentToken;
    }

    public Set<ParkingTransactionItemEventBean> getParkingTransactionItemEventList() {
        return parkingTransactionItemEventList;
    }

    public void setParkingTransactionItemEventList(Set<ParkingTransactionItemEventBean> parkingTransactionItemEventList) {
        this.parkingTransactionItemEventList = parkingTransactionItemEventList;
    }

    public String getParkingTransactionItemId() {
        return parkingTransactionItemId;
    }

    public void setParkingTransactionItemId(String parkingTransactionItemId) {
        this.parkingTransactionItemId = parkingTransactionItemId;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public BigDecimal getPriceDifference() {
        return priceDifference;
    }

    public void setPriceDifference(BigDecimal priceDifference) {
        this.priceDifference = priceDifference;
    }

    public BigDecimal getPreviousPrice() {
        return previousPrice;
    }

    public void setPreviousPrice(BigDecimal previousPrice) {
        this.previousPrice = previousPrice;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }
    
    public Timestamp getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Timestamp creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Integer getLastEventSequenceID() {
        Integer lastSequenceId = 0;
        
        if (this.parkingTransactionItemEventList != null && !parkingTransactionItemEventList.isEmpty()) {
            
            for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : this.parkingTransactionItemEventList) {
                if (parkingTransactionItemEventBean.getSequenceID() > lastSequenceId) {
                    lastSequenceId = parkingTransactionItemEventBean.getSequenceID();
                }
            }
        }
        
        return lastSequenceId;
    }
    
}
