package com.techedge.mp.core.business.model.parking;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "PARKING_AUTH_TOKEN")
@NamedQueries({ @NamedQuery(name = "ParkingAuthTokenBean.getAuthTokenData", query = "select patb from ParkingAuthTokenBean patb") })
public class ParkingAuthTokenBean {

    public static final String GET_TOKEN_DATA = "ParkingAuthTokenBean.getAuthTokenData";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Lob
    @Column(name = "access_token", nullable = false)
    private String             accessToken;

    @Lob
    @Column(name = "refresh_token", nullable = false)
    private String             refreshToken;

    @Column(name = "expires_in", nullable = false)
    private Integer            expires_in;
    @Column(name = "refresh_expires_in", nullable = false)
    private Integer            refresh_expires_in;

    //@Column(name = "access_token_creation_date", nullable = false)
    //private Date accessTokenCreationDate;
    
    @Column(name = "access_token_expiration_date", nullable = false)
    private Date accessTokenExpirationDate;
    
    @Column(name = "refresh_token_expiration_date", nullable = false)
    private Date refreshTokenExpirationDate;
    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Integer getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Integer expires_in) {
        this.expires_in = expires_in;
    }

    public Integer getRefresh_expires_in() {
        return refresh_expires_in;
    }

    public void setRefresh_expires_in(Integer refresh_expires_in) {
        this.refresh_expires_in = refresh_expires_in;
    }

    public Date getAccessTokenExpirationDate() {
        return accessTokenExpirationDate;
    }

    public void setAccessTokenExpirationDate(Date accessTokenExpirationDate) {
        this.accessTokenExpirationDate = accessTokenExpirationDate;
    }

    public Date getRefreshTokenExpirationDate() {
        return refreshTokenExpirationDate;
    }

    public void setRefreshTokenExpirationDate(Date refreshTokenExpirationDate) {
        this.refreshTokenExpirationDate = refreshTokenExpirationDate;
    }
    
    

}
