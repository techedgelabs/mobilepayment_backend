package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="CARDDEPOSITTRANSACTIONS")
@NamedQueries({
    
    @NamedQuery(name = "CardDepositTransactionBean.findByShopTransactionID", query = "select c from CardDepositTransactionBean c where c.shopTransactionID = :shopTransactionID")})
public class CardDepositTransactionBean {

    public static final String FIND_BY_SHOPTRANSACTIONID = "CardDepositTransactionBean.findByShopTransactionID";
    
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="creation_timestamp", nullable=false)
	private Date creationTimestamp;
	
	@Column(name="transactiontype", nullable=true, length=50)
	private String transactionType;
	
	@Column(name="transactionresult", nullable=true, length=50)
	private String transactionResult;
	
	@Column(name="shoptransactionid", nullable=true, length=50)
	private String shopTransactionID;
	
	@Column(name="banktransactionid", nullable=true, length=50)
	private String bankTransactionID;
	
	@Column(name="authorizationcode", nullable=true, length=50)
	private String authorizationCode;
	
	@Column(name="currency", nullable=true, length=20)
	private String currency;
	
	@Column(name="amount", nullable=true, length=50)
	private String amount;
	
	@Column(name="country", nullable=true, length=50)
	private String country;
	
	@Column(name="buyer_name", nullable=true, length=100)
	private String buyerName;
	
	@Column(name="buyer_email", nullable=true, length=100)
	private String buyerEmail;
	
	@Column(name="errorcode", nullable=true, length=50)
	private String errorCode;
	
	@Column(name="errordescription", nullable=true, length=100)
	private String errorDescription;
	
	@Column(name="alertcode", nullable=true, length=50)
	private String alertCode;
	
	@Column(name="alertdescription", nullable=true, length=100)
	private String alertDescription;
	
	@Column(name="transactionkey", nullable=true, length=16)
	private String TransactionKey;
	
	@Column(name="token", nullable=true, length=40)
	private String token;
	
	@Column(name="tokenexpiry_month", nullable=true, length=2)
	private String tokenExpiryMonth;
	
	@Column(name="tokenexpiry_year", nullable=true, length=2)
	private String tokenExpiryYear;
	
	@Column(name="cardbin", nullable=true, length=6)
    private String cardBIN;

    @Column(name="td_level", nullable=true)
    private String tdLevel;

    @Column(name="hash", nullable=true)
    private String hash;

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public String getShopTransactionID() {
		return shopTransactionID;
	}

	public void setShopTransactionID(String shopTransactionID) {
		this.shopTransactionID = shopTransactionID;
	}

	public String getBankTransactionID() {
		return bankTransactionID;
	}

	public void setBankTransactionID(String bankTransactionID) {
		this.bankTransactionID = bankTransactionID;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getBuyerEmail() {
		return buyerEmail;
	}

	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getAlertCode() {
		return alertCode;
	}

	public void setAlertCode(String alertCode) {
		this.alertCode = alertCode;
	}

	public String getAlertDescription() {
		return alertDescription;
	}

	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}

	public String getTransactionKey() {
		return TransactionKey;
	}

	public void setTransactionKey(String transactionKey) {
		TransactionKey = transactionKey;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTokenExpiryMonth() {
		return tokenExpiryMonth;
	}

	public void setTokenExpiryMonth(String tokenExpiryMonth) {
		this.tokenExpiryMonth = tokenExpiryMonth;
	}

	public String getTokenExpiryYear() {
		return tokenExpiryYear;
	}

	public void setTokenExpiryYear(String tokenExpiryYear) {
		this.tokenExpiryYear = tokenExpiryYear;
	}

    public String getCardBIN() {
        return cardBIN;
    }

    public void setCardBIN(String cardBIN) {
        this.cardBIN = cardBIN;
    }

    public String getTdLevel() {
        return cardBIN;
    }

    public void setTdLevel(String tdLevel) {
        this.tdLevel = tdLevel;
    }
    
    public String getHash() {
        return hash;
    }
    
    public void setHash(String hash) {
        this.hash = hash;
    }
}
