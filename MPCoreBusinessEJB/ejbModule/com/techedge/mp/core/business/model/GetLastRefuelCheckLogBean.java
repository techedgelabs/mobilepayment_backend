package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GET_LAST_REFUEL_CHECK_LOG")
public class GetLastRefuelCheckLogBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long   id;

    @Column(name = "gfg_end_refuel_timestamp", nullable = true)
    private Date   gfgEndRefuelTimestamp;

    @Column(name = "offset", nullable = true)
    private Long   offset;

    @Column(name = "mp_end_refuel_timestamp", nullable = true)
    private Date   mpEndRefuelTimestamp;

    @Column(name = "mp_current_timestamp", nullable = true)
    private Date   mpCurrentTimestamp;

    @Column(name = "delta", nullable = true)
    private Long   delta;

    @Column(name = "max_interval", nullable = true)
    private Long   maxInterval;

    @Column(name = "station_id", nullable = true)
    private String stationId;
    
    @Column(name = "result", nullable = true)
    private String result;

    public GetLastRefuelCheckLogBean() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGfgEndRefuelTimestamp() {
        return gfgEndRefuelTimestamp;
    }

    public void setGfgEndRefuelTimestamp(Date gfgEndRefuelTimestamp) {
        this.gfgEndRefuelTimestamp = gfgEndRefuelTimestamp;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Date getMpEndRefuelTimestamp() {
        return mpEndRefuelTimestamp;
    }

    public void setMpEndRefuelTimestamp(Date mpEndRefuelTimestamp) {
        this.mpEndRefuelTimestamp = mpEndRefuelTimestamp;
    }

    public Date getMpCurrentTimestamp() {
        return mpCurrentTimestamp;
    }

    public void setMpCurrentTimestamp(Date mpCurrentTimestamp) {
        this.mpCurrentTimestamp = mpCurrentTimestamp;
    }

    public Long getDelta() {
        return delta;
    }

    public void setDelta(Long delta) {
        this.delta = delta;
    }

    public Long getMaxInterval() {
        return maxInterval;
    }

    public void setMaxInterval(Long maxInterval) {
        this.maxInterval = maxInterval;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

}
