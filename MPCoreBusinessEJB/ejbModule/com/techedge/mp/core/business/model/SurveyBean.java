package com.techedge.mp.core.business.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "SURVEY")
@NamedQueries({ @NamedQuery(name = "SurveyBean.findAll", query = "select s from SurveyBean s"),
    @NamedQuery(name = "SurveyBean.findActive", query = "select s from SurveyBean s where status = 1 and (startDate <= :today and endDate >= :today)"),
        @NamedQuery(name = "SurveyBean.findByCode", query = "select s from SurveyBean s where code = :code") })
public class SurveyBean {

    public static final String      FIND_ALL      = "SurveyBean.findAll";
    public static final String      FIND_ACTIVE      = "SurveyBean.findActive";
    public static final String      FIND_BY_CODE  = "SurveyBean.findByCode";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                    id;

    @Column(name = "code", nullable = true)
    private String                  code;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "description", nullable = true)
    private String                  description;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "note", nullable = true)
    private String                  note;

    @Column(name = "start_date", nullable = true)
    private Date                    startDate;

    @Column(name = "end_date", nullable = true)
    private Date                    endDate;

    @Column(name = "status", nullable = false, length = 1)
    private Integer                 status;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surveyBean", cascade = CascadeType.ALL)
    private Set<SurveyQuestionBean> questionsList = new HashSet<SurveyQuestionBean>(0);

    public SurveyBean() {}

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note
     *            the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the questionsList
     */
    public Set<SurveyQuestionBean> getQuestionsList() {
        return questionsList;
    }

    /**
     * @param questionsList
     *            the questionsList to set
     */
    public void setQuestionsList(Set<SurveyQuestionBean> questionsList) {
        this.questionsList = questionsList;
    }

}
