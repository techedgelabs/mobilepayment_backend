package com.techedge.mp.core.business.model.loyalty;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "REWARD_TRANSACTION")
@NamedQueries({ @NamedQuery(name = "RewardTransactionBean.findByEventNotification", query = "select t from RewardTransactionBean t "
        + "where t.eventNotificationBean = :eventNotificationBean") })
public class RewardTransactionBean {

    public static final String                  FIND_BY_EVENT_NOTIFICATION_ID = "RewardTransactionBean.findByEventNotification";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                                id;

    @OneToOne()
    @JoinColumn(name = "event_notification_id", nullable = false)
    private EventNotificationBean               eventNotificationBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rewardTransactionBean")
    private Set<RewardTransactionParameterBean> parameterDetails              = new HashSet<RewardTransactionParameterBean>(0);

    public RewardTransactionBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public EventNotificationBean getEventNotificationBean() {
        return eventNotificationBean;
    }

    public void setEventNotificationBean(EventNotificationBean eventNotificationBean) {
        this.eventNotificationBean = eventNotificationBean;
    }

    public Set<RewardTransactionParameterBean> getParameterDetails() {
        return parameterDetails;
    }

    public void setParameterDetails(Set<RewardTransactionParameterBean> parameterDetails) {
        this.parameterDetails = parameterDetails;
    }

}
