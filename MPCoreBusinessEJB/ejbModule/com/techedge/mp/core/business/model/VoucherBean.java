package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.Voucher;

@Entity
@Table(name = "VOUCHERS")
@NamedQueries({ @NamedQuery(name = "VoucherBean.findVoucherById", query = "select v from VoucherBean v where v.id = :id"),
        @NamedQuery(name = "VoucherBean.findVoucherByVoucherCode", query = "select v from VoucherBean v where v.code = :code"),
        @NamedQuery(name = "VoucherBean.findActiveVoucherByUser", query = "select v from VoucherBean v where v.userBean = :userBean and status = 'V'") })
public class VoucherBean {

    public static final String FIND_BY_ID          = "VoucherBean.findVoucherById";
    public static final String FIND_BY_CODE        = "VoucherBean.findVoucherByVoucherCode";
    public static final String FIND_ACTIVE_BY_USER = "VoucherBean.findActiveVoucherByUser";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "code", nullable = false, length = 16)
    private String             code;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "user_id")
    private UserBean           userBean;

    @Column(name = "status", nullable = false, length = 1)
    private String             status;

    @Column(name = "type", nullable = false, length = 10)
    private String             type;

    @Column(name = "value", nullable = false)
    private Double             value;

    @Column(name = "initial_value", nullable = false)
    private Double             initialValue;

    @Column(name = "consumed_value", nullable = false)
    private Double             consumedValue;

    @Column(name = "voucher_balance_due", nullable = false)
    private Double             voucherBalanceDue;

    @Column(name = "expiration_date", nullable = true)
    private Date               expirationDate;

    @Column(name = "promo_code", nullable = true, length = 10)
    private String             promoCode;

    @Column(name = "promo_description", nullable = true)
    private String             promoDescription;

    @Column(name = "promo_doc", nullable = true)
    private String             promoDoc;

    @Column(name = "min_amount", nullable = true)
    private Double             minAmount;

    @Column(name = "min_quantity", nullable = true)
    private Double             minQuantity;

    @Column(name = "is_combinable", nullable = true, length = 1)
    private String             isCombinable;

    @Column(name = "valid_pv", nullable = true, length = 1)
    private String             validPV;

    @Column(name = "promo_partner", nullable = true)
    private String             promoPartner;

    @Column(name = "icon", nullable = true)
    private String             icon;

    /*
     * @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
     * //@JoinTable(name = "VOUCHER_VALID_PRODUCTS", joinColumns=@JoinColumn(name = "voucher_id"))
     * 
     * @Column(name= "product_code", table = "VOUCHER_VALID_PRODUCTS")
     * private Set<String> validProducts = new HashSet<String>(0);
     * 
     * @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
     * //@JoinTable(name = "VOUCHER_VALID_REFULMODE", joinColumns=@JoinColumn(name = "voucher_id"))
     * 
     * @Column(name= "refuelmode", table = "VOUCHER_VALID_REFUELMODE")
     * private Set<String> validRefuelMode = new HashSet<String>(0);
     */
    public VoucherBean() {}

    public VoucherBean(Voucher voucher) {

        this.id = voucher.getId();
        this.code = voucher.getCode();
        //this.userBean = new UserBean(voucher.getUser());
        this.status = voucher.getStatus();
        this.type = voucher.getType();
        this.value = voucher.getValue();
        this.initialValue = voucher.getInitialValue();
        this.consumedValue = voucher.getConsumedValue();
        this.voucherBalanceDue = voucher.getVoucherBalanceDue();
        this.expirationDate = voucher.getExpirationDate();
        this.promoCode = voucher.getPromoCode();
        this.promoDescription = voucher.getPromoDescription();
        this.minAmount = voucher.getMinAmount();
        this.minQuantity = voucher.getMinQuantity();
        this.isCombinable = voucher.getIsCombinable();
        this.validPV = voucher.getValidPV();
        this.promoPartner = voucher.getPromoPartner();
        this.icon = voucher.getIcon();

    }

    public Voucher toVoucher() {

        Voucher voucher = new Voucher();

        voucher.setId(this.id);
        voucher.setCode(this.code);
        voucher.setStatus(this.status);
        voucher.setType(this.type);
        voucher.setValue(this.value);
        voucher.setInitialValue(this.initialValue);
        voucher.setConsumedValue(this.consumedValue);
        voucher.setVoucherBalanceDue(this.voucherBalanceDue);
        voucher.setExpirationDate(this.expirationDate);
        voucher.setPromoCode(this.promoCode);
        voucher.setPromoDescription(this.promoDescription);
        voucher.setPromoDoc(this.promoDoc);
        voucher.setMinAmount(this.minAmount);
        voucher.setMinQuantity(this.minQuantity);
        voucher.setIsCombinable(this.isCombinable);
        voucher.setValidPV(this.validPV);
        voucher.setPromoPartner(this.promoPartner);
        voucher.setIcon(this.icon);

        return voucher;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Double initialValue) {
        this.initialValue = initialValue;
    }

    public Double getConsumedValue() {
        return consumedValue;
    }

    public void setConsumedValue(Double consumedValue) {
        this.consumedValue = consumedValue;
    }

    public Double getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Double voucherBalanceDue) {
        this.voucherBalanceDue = voucherBalanceDue;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {
        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {
        this.promoDoc = promoDoc;
    }

    public Double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Double minAmount) {
        this.minAmount = minAmount;
    }

    public Double getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(Double minQuantity) {
        this.minQuantity = minQuantity;
    }

    public String getIsCombinable() {
        return isCombinable;
    }

    public void setIsCombinable(String isCombinable) {
        this.isCombinable = isCombinable;
    }

    public String getValidPV() {
        return validPV;
    }

    public void setValidPV(String validPV) {
        this.validPV = validPV;
    }

    /*
     * public Set<String> getValidProducts() {
     * return validProducts;
     * }
     * 
     * public Set<String> getValidRefuelMode() {
     * return validRefuelMode;
     * }
     */
    public String getPromoPartner() {
        return promoPartner;
    }

    public void setPromoPartner(String promoPartner) {
        this.promoPartner = promoPartner;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}
