package com.techedge.mp.core.business.model.crm;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.core.business.model.voucher.VoucherPromotionalBean;

@Entity
@Table(name = "CRM_OFFER_VOUCHER_PROMOTIONAL")
@NamedQueries({ @NamedQuery(name = "CRMOfferVoucherPromotionalBean.findToReconcilie", query = "select cvp from CRMOfferVoucherPromotionalBean cvp where cvp.voucherPromotionalToReconcilie = 1") })
public class CRMOfferVoucherPromotionalBean implements UserReconciliationInterfaceBean {

    public static final String FIND_TO_RECONCILIE = "CRMOfferVoucherPromotionalBean.findToReconcilie";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long         id;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "offer_id")
    private CRMOfferBean          crmOfferBean;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "voucher_promotional_id")
    private VoucherPromotionalBean          voucherPromotionalBean;
    
    @Column(name = "voucher_promotional_to_reconcilie")
    private boolean       voucherPromotionalToReconcilie;
    
    @Column(name = "reconciliation_attempts_left")
    private Integer              reconciliationAttemptsLeft;
    

    public CRMOfferVoucherPromotionalBean() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CRMOfferBean getCrmOfferBean() {
        return crmOfferBean;
    }

    public void setCrmOfferBean(CRMOfferBean crmOfferBean) {
        this.crmOfferBean = crmOfferBean;
    }

    public boolean isVoucherPromotionalToReconcilie() {
        return voucherPromotionalToReconcilie;
    }

    public void setVoucherPromotionalToReconcilie(boolean voucherPromotionalToReconcilie) {
        this.voucherPromotionalToReconcilie = voucherPromotionalToReconcilie;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }
    
    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }

    @Override
    public UserBean getUserBean() {
        if (getCrmOfferBean() != null) {
            return getCrmOfferBean().getEventBean().getUserBean();
        }

        return null;
    }

    @Override
    public String getFinalStatus() {
        if (getVoucherPromotionalBean() != null && getVoucherPromotionalBean().getLastVoucherPromotionalDetailBean() != null) {
            return getVoucherPromotionalBean().getLastVoucherPromotionalDetailBean().getStatusCode();
        }

        return null;
    }

    @Override
    public void setFinalStatus(String status) {
        if (getVoucherPromotionalBean() != null && getVoucherPromotionalBean().getLastVoucherPromotionalDetailBean() != null) {
            getVoucherPromotionalBean().getLastVoucherPromotionalDetailBean().setStatusCode(status);
        }
    }

    @Override
    public void setToReconcilie(Boolean toReconcilie) {
        setVoucherPromotionalToReconcilie(toReconcilie);
    }

    @Override
    public Boolean getToReconcilie() {
        return isVoucherPromotionalToReconcilie();
    }

    @Override
    public String getOperationName() {
        return "CRM_OFFER_VOUCHER_PROMOTIONAL";
    }

    @Override
    public Date getOperationTimestamp() {
        if (getVoucherPromotionalBean() != null && getVoucherPromotionalBean().getLastVoucherPromotionalDetailBean() != null) {
            return getVoucherPromotionalBean().getLastVoucherPromotionalDetailBean().getRequestTimestamp();
        }

        return null;
    }

    public VoucherPromotionalBean getVoucherPromotionalBean() {
        return voucherPromotionalBean;
    }

    public void setVoucherPromotionalBean(VoucherPromotionalBean voucherPromotionalBean) {
        this.voucherPromotionalBean = voucherPromotionalBean;
    }

    @Override
    public String getFinalStatusMessage() {
        String message = null;
        
        if (getVoucherPromotionalBean() != null && getVoucherPromotionalBean().getLastVoucherPromotionalDetailBean() != null) {
            message = getVoucherPromotionalBean().getLastVoucherPromotionalDetailBean().getMessageCode();
        }
        
        return message;
    }
    
    @Override
    public String getServerEndpoint() {
        return "DWH Serijakala";
    }
    
    public Double getVoucherAmount() {
        Double amount = null;
        
        for (CRMOfferParametersBean crmOfferParametersBean : getCrmOfferBean().getCRMParametersBeanList()) {
            if (crmOfferParametersBean.getParameterName() != null && crmOfferParametersBean.getParameterName().equals("VoucherAmount")) {
                if (crmOfferParametersBean.getParameterValue() != null) {
                    amount = new Double(crmOfferParametersBean.getParameterValue());
                    break;
                }
            }
        }
        
        return amount;
    }
    
    public String getVoucherPromoCode() {
        String promoCode = null;
        
        for (CRMOfferParametersBean crmOfferParametersBean : getCrmOfferBean().getCRMParametersBeanList()) {
            if (crmOfferParametersBean.getParameterName() != null && crmOfferParametersBean.getParameterName().equals("VoucherPromoCode")) {
                if (crmOfferParametersBean.getParameterValue() != null) {
                    promoCode = crmOfferParametersBean.getParameterValue();
                    break;
                }
            }
        }
        
        return promoCode;
    }
    
}
