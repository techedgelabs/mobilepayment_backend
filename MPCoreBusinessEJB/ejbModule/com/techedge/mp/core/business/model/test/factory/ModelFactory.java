package com.techedge.mp.core.business.model.test.factory;

import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.core.business.model.PromoVoucherBean;
import com.techedge.mp.core.business.model.PromotionBean;
import com.techedge.mp.core.business.model.UserBean;

public abstract class ModelFactory {

    
    public abstract UserBean createUserBean();
    public abstract PromotionBean createPromotion();
    public abstract PromoVoucherBean createPromoVoucher();
    public abstract PromoVoucherInput createPromoVoucherInput();

}
