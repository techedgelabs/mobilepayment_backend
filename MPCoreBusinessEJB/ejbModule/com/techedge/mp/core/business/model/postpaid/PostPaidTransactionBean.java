package com.techedge.mp.core.business.model.postpaid;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.core.business.interfaces.PostPaidTransaction;
import com.techedge.mp.core.business.interfaces.PostPaidTransactionEvent;
import com.techedge.mp.core.business.interfaces.PostPaidTransactionPaymentEvent;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.TransactionInterfaceType;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionAdditionalData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionOperation;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TransactionAdditionalDataBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.model.TransactionOperationBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.voucher.PaymentTokenPackageBean;

@Entity
@Table(name = "POSTPAIDTRANSACTIONS")
@NamedQueries({
        @NamedQuery(name = "PostPaidTransactionBean.findTransactionByMPID", query = "select t from PostPaidTransactionBean t where t.mpTransactionID = :mpTransactionID"),

        @NamedQuery(name = "PostPaidTransactionBean.findOnHoldTransaction", query = "select t from PostPaidTransactionBean t where t.mpTransactionStatus = :mpTransaction and t.creationTimestamp < :time"),

        @NamedQuery(name = "PostPaidTransactionBean.findTransactionBySRCID", query = "select t from PostPaidTransactionBean t where t.srcTransactionID = :srcTransactionID "),
        
        @NamedQuery(name = "PostPaidTransactionBean.findTransactionBySRCIDAndSource", query = "select t from PostPaidTransactionBean t where t.srcTransactionID = :srcTransactionID and t.sourceID = :sourceID"),

        @NamedQuery(name = "PostPaidTransactionBean.findTransactionByQRCode", query = "select t from PostPaidTransactionBean t where t.sourceID = :sourceID "
                + "and t.mpTransactionStatus = :mpTransactionStatus order by t.creationTimestamp desc"),

        @NamedQuery(name = "PostPaidTransactionBean.findLastTransactionByQRCode", query = "select t from PostPaidTransactionBean t where t.sourceID = :sourceID order by t.creationTimestamp desc"),

        @NamedQuery(name = "PostPaidTransactionBean.findTransactionByMPStatus", query = "select t from PostPaidTransactionBean t where t.mpTransactionStatus = :mpTransactionStatus"),
        //	@NamedQuery(name="TransactionBean.findTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean"),

        @NamedQuery(name = "PostPaidTransactionBean.findTransactionByUserAndDate", query = "select t from PostPaidTransactionBean t where t.userBean = :userBean "
                + "and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),

        @NamedQuery(name = "PostPaidTransactionBean.findTransactionPaidByUserAndDate", query = "select t from PostPaidTransactionBean t where t.userBean = :userBean "
                + "and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID
                + "' order by t.creationTimestamp desc"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisRefuelMode", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1 join t1.refuelBean r1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and t1.source = :source "
                + "and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate) and r1.refuelMode = :refuelMode), "
                + "(select count(t2.id) from PostPaidTransactionBean t2 join t2.refuelBean r2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and t2.source = :source "
                + "and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate) and r2.refuelMode = :refuelMode), "
                + "(select count(t3.id) from PostPaidTransactionBean t3 join t3.refuelBean r3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllRefuelMode", query = "select "
                + "count(t3.id) from PostPaidTransactionBean t3 join t3.refuelBean r3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisRefuelModeBusiness", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1 join t1.refuelBean r1 where t1.transactionCategory = 'BUSINESS' and t1.source = :source "
                + "and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate) and r1.refuelMode = :refuelMode), "
                + "(select count(t2.id) from PostPaidTransactionBean t2 join t2.refuelBean r2 where t2.transactionCategory = 'BUSINESS' and t2.source = :source "
                + "and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate) and r2.refuelMode = :refuelMode), "
                + "(select count(t3.id) from PostPaidTransactionBean t3 join t3.refuelBean r3 where t3.transactionCategory = 'BUSINESS' and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllRefuelModeBusiness", query = "select "
                + "count(t3.id) from PostPaidTransactionBean t3 join t3.refuelBean r3 where t3.transactionCategory = 'BUSINESS' and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesis", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and t1.source = :source "
                + "and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionBean t2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and t2.source = :source "
                + "and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionBean t3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAll", query = "select "
                + "count(t3.id) from PostPaidTransactionBean t3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisBusiness", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1 where t1.transactionCategory = 'BUSINESS' and t1.source = :source "
                + "and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionBean t2 where t2.transactionCategory = 'BUSINESS' and t2.source = :source "
                + "and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionBean t3 where t3.transactionCategory = 'BUSINESS' and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllBusiness", query = "select "
                + "count(t3.id) from PostPaidTransactionBean t3 where t3.transactionCategory = 'BUSINESS' and t3.source = :source "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisArea", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1, ProvinceInfoBean p1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and "
                + "t1.stationBean.province = p1.name and p1.area = :area and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionBean t2, ProvinceInfoBean p2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and "
                + "t2.stationBean.province = p2.name and p2.area = :area and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionBean t3, ProvinceInfoBean p3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllArea", query = "select "
                + "count(t3.id) from PostPaidTransactionBean t3, ProvinceInfoBean p3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAreaBusiness", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1, ProvinceInfoBean p1 where t1.transactionCategory = 'BUSINESS' and "
                + "t1.stationBean.province = p1.name and p1.area = :area and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionBean t2, ProvinceInfoBean p2 where t2.transactionCategory = 'BUSINESS' and "
                + "t2.stationBean.province = p2.name and p2.area = :area and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionBean t3, ProvinceInfoBean p3 where t3.transactionCategory = 'BUSINESS' and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllAreaBusiness", query = "select "
                + "count(t3.id) from PostPaidTransactionBean t3, ProvinceInfoBean p3 where t3.transactionCategory = 'BUSINESS' and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisProvince", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1 where t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 2 and t1.stationBean.province = :stationProvince) "
                + "and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionBean t2 where t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 2 and t2.stationBean.province = :stationProvince) "
                + "and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionBean t3 where t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 2 and t3.stationBean.province = :stationProvince) and t3.creationTimestamp <= :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisVoucherConsumed", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1 join t1.postPaidConsumeVoucherBeanList cv1 join cv1.postPaidConsumeVoucherDetailBean cvd1 "
                + "where cvd1.voucherValue = cvd1.consumedValue and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionBean t2 join t2.postPaidConsumeVoucherBeanList cv2 join cv2.postPaidConsumeVoucherDetailBean cvd2 "
                + "where cvd2.voucherValue = cvd2.consumedValue and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionBean t3 join t3.postPaidConsumeVoucherBeanList cv3 join cv3.postPaidConsumeVoucherDetailBean cvd3 "
                + "where cvd3.voucherValue = cvd3.consumedValue and t3.creationTimestamp <= :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisPromoVoucherConsumed", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1 join t1.postPaidConsumeVoucherBeanList cv1 join cv1.postPaidConsumeVoucherDetailBean cvd1 "
                + "where cvd1.voucherValue = cvd1.consumedValue and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate) "
                + "and cvd1.voucherCode IN (select pv1.code from PromoVoucherBean pv1 where pv1.code = cvd1.voucherCode and pv1.promotionBean = :promotionBean)), "
                + "(select count(t2.id) from PostPaidTransactionBean t2 join t2.postPaidConsumeVoucherBeanList cv2 join cv2.postPaidConsumeVoucherDetailBean cvd2 "
                + "where cvd2.voucherValue = cvd2.consumedValue and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate) "
                + "and cvd2.voucherCode IN (select pv2.code from PromoVoucherBean pv2 where pv2.code = cvd2.voucherCode and pv2.promotionBean = :promotionBean)), "
                + "(select count(t3.id) from PostPaidTransactionBean t3 join t3.postPaidConsumeVoucherBeanList cv3 join cv3.postPaidConsumeVoucherDetailBean cvd3 "
                + "where cvd3.voucherValue = cvd3.consumedValue and t3.creationTimestamp <= :dailyEndDate "
                + "and cvd3.voucherCode IN (select pv3.code from PromoVoucherBean pv3 where pv3.code = cvd3.voucherCode and pv3.promotionBean = :promotionBean)) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalAmount", query = "select "
                + "(select case when sum(t1.amount) is null then 0.00 else sum(t1.amount) end from PostPaidTransactionBean t1 where t1.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.amount) is null then 0.00 else sum(t2.amount) end from PostPaidTransactionBean t2 where t2.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.amount) is null then 0.00 else sum(t3.amount) end from PostPaidTransactionBean t3 where t3.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalAmount", query = "select "
                + "case when sum(t3.amount) is null then 0.00 else sum(t3.amount) end from PostPaidTransactionBean t3 where t3.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalAmountBusiness", query = "select "
                + "(select case when sum(t1.amount) is null then 0.00 else sum(t1.amount) end from PostPaidTransactionBean t1 where t1.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t1.transactionCategory = 'BUSINESS' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.amount) is null then 0.00 else sum(t2.amount) end from PostPaidTransactionBean t2 where t2.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t2.transactionCategory = 'BUSINESS' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.amount) is null then 0.00 else sum(t3.amount) end from PostPaidTransactionBean t3 where t3.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t3.transactionCategory = 'BUSINESS' and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalAmountBusiness", query = "select "
                + "case when sum(t3.amount) is null then 0.00 else sum(t3.amount) end from PostPaidTransactionBean t3 where t3.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t3.transactionCategory = 'BUSINESS' and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalFuelQuantity", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionBean t1 join t1.refuelBean r1 "
                + "where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionBean t2 join t2.refuelBean r2 "
                + "where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalFuelQuantity", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalFuelQuantityBusiness", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionBean t1 join t1.refuelBean r1 "
                + "where t1.transactionCategory = 'BUSINESS' and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionBean t2 join t2.refuelBean r2 "
                + "where t2.transactionCategory = 'BUSINESS' and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalFuelQuantityBusiness", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalFuelQuantityByProduct", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionBean t1 join t1.refuelBean r1 "
                + "where (t1.transactionCategory = 'CUSTOMER' or t1.transactionCategory is null) and r1.productDescription = :productDescription and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionBean t2 join t2.refuelBean r2 "
                + "where (t2.transactionCategory = 'CUSTOMER' or t2.transactionCategory is null) and r2.productDescription = :productDescription and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where (t3.transactionCategory = 'CUSTOMER' or t3.transactionCategory is null) and r3.productDescription = :productDescription and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),
                
        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalFuelQuantityByProduct", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where (t3.transactionCategory = 'CUSTOMER' or t3.transactionCategory is null) and r3.productDescription = :productDescription "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),                

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalFuelQuantityByProductBusiness", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionBean t1 join t1.refuelBean r1 "
                + "where t1.transactionCategory = 'BUSINESS' and r1.productDescription = :productDescription and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionBean t2 join t2.refuelBean r2 "
                + "where t2.transactionCategory = 'BUSINESS' and r2.productDescription = :productDescription and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and r3.productDescription = :productDescription and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),
                
        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalFuelQuantityByProductBusiness", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and r3.productDescription = :productDescription and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisRefuelModeTotalFuelQuantity", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionBean t1 join t1.refuelBean r1 "
                + "where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate) and r1.refuelMode = :refuelMode), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionBean t2 join t2.refuelBean r2 "
                + "where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate) and r2.refuelMode = :refuelMode), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllRefuelModeTotalFuelQuantity", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisRefuelModeTotalFuelQuantityBusiness", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionBean t1 join t1.refuelBean r1 "
                + "where t1.transactionCategory = 'BUSINESS' and t1.source = :source and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate) and r1.refuelMode = :refuelMode), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionBean t2 join t2.refuelBean r2 "
                + "where t2.transactionCategory = 'BUSINESS' and t2.source = :source and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate) and r2.refuelMode = :refuelMode), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllRefuelModeTotalFuelQuantityBusiness", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.refuelBean r3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.source = :source and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate and r3.refuelMode = :refuelMode"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportDetail", query = "select t from PostPaidTransactionBean t where (t.transactionCategory is null or t.transactionCategory = 'CUSTOMER') "
                + "and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t.amount > 0 and t.stationBean.stationStatus = 2 "
                + "and t.creationTimestamp <= :endDate order by t.creationTimestamp desc"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportDetailBusiness", query = "select t from PostPaidTransactionBean t where t.transactionCategory = 'BUSINESS' "
                + "and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t.amount > 0 and t.stationBean.stationStatus = 2 "
                + "and t.creationTimestamp <= :endDate order by t.creationTimestamp desc"),
                
        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportDetailByDate", query = "select t from PostPaidTransactionBean t where (t.transactionCategory is null or t.transactionCategory = 'CUSTOMER') "
                + "and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t.amount > 0 and t.stationBean.stationStatus = 2 "
                + "and (t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportDetailByDateBusiness", query = "select t from PostPaidTransactionBean t where t.transactionCategory = 'BUSINESS' "
                + "and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and t.amount > 0 and t.stationBean.stationStatus = 2 "
                + "and (t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),
                
        @NamedQuery(name = "PostPaidTransactionBean.findActiveTransactionByUser", query = "select t from PostPaidTransactionBean t where t.userBean = :userBean and t.notificationUser = false"),
        //	@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and t.confirmed = false"),
        //	@NamedQuery(name="TransactionBean.findActiveTransaction", query="select t from TransactionBean t where t.confirmed = false"),
        //	@NamedQuery(name="TransactionBean.findActiveTransactionNew", query="select t from TransactionBean t where t.finalStatusType = null"),

        @NamedQuery(name = "PostPaidTransactionBean.findTransactionToHistoryByStatusType", query = "select t from PostPaidTransactionBean t where t.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED + "' or t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "' or "
                + "t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_REVERSED + "'"),

        @NamedQuery(name = "PostPaidTransactionBean.findTransactionToHistory", query = "select t from PostPaidTransactionBean t where ((t.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED + "' or t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_REVERSED
                + "') and t.creationTimestamp < :time) or ((t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' or t.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "')"
                + "and t.notificationPaid = true and t.toReconcile = 0 and t.loyaltyReconcile = 0 and t.voucherReconcile = 0 and t.creationTimestamp < :time)"),

        @NamedQuery(name = "PostPaidTransactionBean.findTransactionPaidByDate", query = "select t from PostPaidTransactionBean t where t.mpTransactionStatus = '"
                + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' and "
                + "t.notificationPaid = true and t.notificationUser = true and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate)"),

        @NamedQuery(name = "PostPaidTransactionBean.findLastTransactionBySourceId", query = "select t from PostPaidTransactionBean t where t.sourceID = :sourceID "
                + "and ( t.creationTimestamp >= :startDate and t.creationTimestamp <= :endDate ) order by t.creationTimestamp desc"),

        @NamedQuery(name = "PostPaidTransactionBean.findDistinctSourceIdByStationId", query = "select distinct(t.sourceID) from PostPaidTransactionBean t "
                + "where t.stationBean = :stationBean order by t.sourceID"),

        @NamedQuery(name = "PostPaidTransactionBean.findTransactionByStationIdAndDate", query = "select t from PostPaidTransactionBean t "
                + "where t.stationBean = :stationBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),

        @NamedQuery(name = "PostPaidTransactionBean.findPaymentReconciliation", query = "select t from PostPaidTransactionBean t where t.toReconcile = 1"),

        @NamedQuery(name = "PostPaidTransactionBean.findVoucherReconciliation", query = "select t from PostPaidTransactionBean t where t.voucherReconcile = 1 and t.toReconcile = 0"),

        @NamedQuery(name = "PostPaidTransactionBean.findLoyaltyReconciliation", query = "select t from PostPaidTransactionBean t where t.loyaltyReconcile = 1 and t.toReconcile = 0"),

        @NamedQuery(name = "PostPaidTransactionBean.findTransactionsByPaymentMethodId", query = "select t from PostPaidTransactionBean t where t.paymentMethodId = :paymentMethodId"),

        @NamedQuery(name = "PostPaidTransactionBean.findOnHoldByUserAndStation", query = "select t from PostPaidTransactionBean t "
                + "where t.stationBean = :stationBean and t.userBean = :userBean and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD + "' order by creationTimestamp desc"),
        
        @NamedQuery(name = "PostPaidTransactionBean.findByStationIdAndSourceNumber", query = "select t from PostPaidTransactionBean t "
                + "where t.stationBean = :stationBean and t.sourceNumber = :sourceNumber order by creationTimestamp desc"),
                
        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalLoyalty", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1 join t1.postPaidLoadLoyaltyCreditsBeanList tl1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionBean t2 join t2.postPaidLoadLoyaltyCreditsBeanList tl2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionBean t3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalLoyalty", query = "select "
                + "count(t3.id) from PostPaidTransactionBean t3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisLoyaltyRefuelmode", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1 join t1.refuelBean r1 join t1.postPaidLoadLoyaltyCreditsBeanList tl1 "
                + "where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and r1.refuelMode = :refuelMode and tl1.statusCode = '00' and tl1.operationType = 'LOAD' "
                + "and tl1.credits > 0 and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionBean t2 join t2.refuelBean r2 join t2.postPaidLoadLoyaltyCreditsBeanList tl2 "
                + "where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and r2.refuelMode = :refuelMode and tl2.statusCode = '00' and tl2.operationType = 'LOAD' "
                + "and tl2.credits > 0 and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionBean t3 join t3.refuelBean r3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and r3.refuelMode = :refuelMode and tl3.statusCode = '00' and tl3.operationType = 'LOAD' "
                + "and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllLoyaltyRefuelmode", query = "select "
                + "count(t3.id) from PostPaidTransactionBean t3 join t3.refuelBean r3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and r3.refuelMode = :refuelMode and tl3.statusCode = '00' and tl3.operationType = 'LOAD' "
                + "and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalLoyaltyArea", query = "select "
                + "(select count(t1.id) from PostPaidTransactionBean t1, ProvinceInfoBean p1 join t1.postPaidLoadLoyaltyCreditsBeanList tl1 "
                + "where t1.stationBean.province = p1.name and p1.area = :area and tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 "
                + "and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from PostPaidTransactionBean t2, ProvinceInfoBean p2 join t2.postPaidLoadLoyaltyCreditsBeanList tl2 "
                + "where t2.stationBean.province = p2.name and p2.area = :area and tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 "
                + "and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from PostPaidTransactionBean t3, ProvinceInfoBean p3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 "
                + "where t3.stationBean.province = p3.name and p3.area = :area and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalLoyaltyArea", query = "select "
                + "count(t3.id) from PostPaidTransactionBean t3, ProvinceInfoBean p3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 "
                + "where t3.stationBean.province = p3.name and p3.area = :area and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 "
                + "and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalLoyaltyCredits", query = "select "
                + "(select case when sum(tl1.credits) is null then 0 else sum(tl1.credits) end from PostPaidTransactionBean t1 join t1.postPaidLoadLoyaltyCreditsBeanList tl1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(tl2.credits) is null then 0 else sum(tl2.credits) end from PostPaidTransactionBean t2 join t2.postPaidLoadLoyaltyCreditsBeanList tl2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(tl3.credits) is null then 0 else sum(tl3.credits) end from PostPaidTransactionBean t3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalLoyaltyCredits", query = "select "
                + "case when sum(tl3.credits) is null then 0 else sum(tl3.credits) end from PostPaidTransactionBean t3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalLoyaltyFuelQuantity", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionBean t1 join t1.postPaidLoadLoyaltyCreditsBeanList tl1 join t1.refuelBean r1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionBean t2 join t2.postPaidLoadLoyaltyCreditsBeanList tl2 join t2.refuelBean r2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 join t3.refuelBean r3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),
                
        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantity", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 join t3.refuelBean r3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisTotalLoyaltyFuelQuantityByProduct", query = "select "
                + "(select case when sum(r1.fuelQuantity) is null then 0.00 else sum(r1.fuelQuantity) end from PostPaidTransactionBean t1 join t1.postPaidLoadLoyaltyCreditsBeanList tl1 join t1.refuelBean r1 "
                + "where r1.productDescription = :productDescription and tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t1.amount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(r2.fuelQuantity) is null then 0.00 else sum(r2.fuelQuantity) end from PostPaidTransactionBean t2 join t2.postPaidLoadLoyaltyCreditsBeanList tl2 join t2.refuelBean r2 "
                + "where r2.productDescription = :productDescription and tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t2.amount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 join t3.refuelBean r3 "
                + "where r3.productDescription = :productDescription and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from PostPaidTransactionBean t"),
                
        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantityByProduct", query = "select "
                + "case when sum(r3.fuelQuantity) is null then 0.00 else sum(r3.fuelQuantity) end from PostPaidTransactionBean t3 join t3.postPaidLoadLoyaltyCreditsBeanList tl3 join t3.refuelBean r3 "
                + "where r3.productDescription = :productDescription and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t3.amount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "PostPaidTransactionBean.statisticsReportMyciceroPostpaidTransactions", query = "select NEW "
                + "com.techedge.mp.core.actions.scheduler.SchedulerJobStatisticsMyCiceroRefuelingReportAction$CustomReportDTO(ppt.mpTransactionID, ppt.amount, "
                + "ref.fuelQuantity, ref.productDescription, station.stationID, ppt.creationTimestamp) "
                + "from PostPaidTransactionBean ppt join ppt.refuelBean ref join ppt.stationBean station where ppt.userBean.source = :source and ref.refuelMode = :refuelMode "
                + "and ppt.amount > 0.0 and ppt.mpTransactionStatus = :transactionStatus and (ppt.creationTimestamp between :startDate and :endDate)"),
        
        @NamedQuery(name = "PostPaidTransactionBean.amountRefuelForPeriod", query = "select "
                + "case when sum(t.amount) is null then 0 else sum(t.amount) end from PostPaidTransactionBean t where t.userBean.id=:userId "
                + "and t.mpTransactionStatus = '" + StatusHelper.POST_PAID_FINAL_STATUS_PAID + "' "
                + "and t.amount > 0 and t.creationTimestamp >= :periodStartDate "
                + "and t.creationTimestamp < :periodEndDate "
                + "and t.acquirerID <> 'MULTICARD'")

})
public class PostPaidTransactionBean implements TransactionInterfaceBean {

    public static final String                       FIND_ONHOLD                                                 = "PostPaidTransactionBean.findOnHoldTransaction";
    public static final String                       FIND_BY_MP_ID                                               = "PostPaidTransactionBean.findTransactionByMPID";
    public static final String                       FIND_BY_QR_CODE                                             = "PostPaidTransactionBean.findTransactionByQRCode";
    public static final String                       FIND_BY_SRC_ID                                              = "PostPaidTransactionBean.findTransactionBySRCID";
    public static final String                       FIND_BY_SRC_ID_AND_SOURCE                                   = "PostPaidTransactionBean.findTransactionBySRCIDAndSource";
    public static final String                       FIND_LAST_BY_QR_CODE                                        = "PostPaidTransactionBean.findLastTransactionByQRCode";
    public static final String                       FIND_BY_STATUS                                              = "PostPaidTransactionBean.findTransactionByMPStatus";
    //	public static final String FIND_BY_USER = "TransactionBean.findTransactionByUser";
    public static final String                       FIND_ACTIVE_BY_USER                                         = "PostPaidTransactionBean.findActiveTransactionByUser";
    //	public static final String FIND_ACTIVE = "TransactionBean.findActiveTransaction";
    //	public static final String FIND_ACTIVE_NEW = "TransactionBean.findActiveTransactionNew";
    public static final String                       FIND_TO_HISTORY_BY_STATUS_TYPE                              = "PostPaidTransactionBean.findTransactionToHistoryByStatusType";
    public static final String                       FIND_TO_HISTORY                                             = "PostPaidTransactionBean.findTransactionToHistory";
    public static final String                       FIND_PAID_BY_DATE                                           = "PostPaidTransactionBean.findTransactionPaidByDate";
    public static final String                       FIND_BY_USER_AND_DATE                                       = "PostPaidTransactionBean.findTransactionByUserAndDate";
    public static final String                       FIND_PAID_BY_USER_AND_DATE                                  = "PostPaidTransactionBean.findTransactionPaidByUserAndDate";
    public static final String                       FIND_LAST_BY_SOURCE_ID                                      = "PostPaidTransactionBean.findLastTransactionBySourceId";
    public static final String                       FIND_DISTINCT_SOURCE_ID_BY_STATION_ID                       = "PostPaidTransactionBean.findDistinctSourceIdByStationId";
    public static final String                       FIND_BY_STATION_AND_DATE                                    = "PostPaidTransactionBean.findTransactionByStationIdAndDate";
    public static final String                       FIND_FOR_REPORT                                             = "PostPaidTransactionBean.findTransactionForReport";
    public static final String                       FIND_FOR_REPORT_BY_DATE                                     = "PostPaidTransactionBean.findTransactionForReportByDate";
    public static final String                       STATISTICS_REPORT_DETAIL                                    = "PostPaidTransactionBean.statisticsReportDetail";
    public static final String                       STATISTICS_REPORT_DETAIL_BUSINESS                           = "PostPaidTransactionBean.statisticsReportDetailBusiness";
    public static final String                       STATISTICS_REPORT_DETAIL_BY_DATE                            = "PostPaidTransactionBean.statisticsReportDetailByDate";
    public static final String                       STATISTICS_REPORT_DETAIL_BY_DATE_BUSINESS                   = "PostPaidTransactionBean.statisticsReportDetailByDateBusiness";
    public static final String                       STATISTICS_REPORT_BY_DATE                                   = "PostPaidTransactionBean.statisticsReportByDate";
    public static final String                       STATISTICS_REPORT_SYNTHESIS                                 = "PostPaidTransactionBean.statisticsReportSynthesis";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL                             = "PostPaidTransactionBean.statisticsReportSynthesisAll";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_BUSINESS                        = "PostPaidTransactionBean.statisticsReportSynthesisBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_BUSINESS                    = "PostPaidTransactionBean.statisticsReportSynthesisAllBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_AREA                            = "PostPaidTransactionBean.statisticsReportSynthesisArea";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_AREA                        = "PostPaidTransactionBean.statisticsReportSynthesisAllArea";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_AREA_BUSINESS                   = "PostPaidTransactionBean.statisticsReportSynthesisAreaBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_AREA_BUSINESS               = "PostPaidTransactionBean.statisticsReportSynthesisAllAreaBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE                     = "PostPaidTransactionBean.statisticsReportSynthesisRefuelMode";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE                 = "PostPaidTransactionBean.statisticsReportSynthesisAllRefuelMode";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_BUSINESS            = "PostPaidTransactionBean.statisticsReportSynthesisRefuelModeBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_BUSINESS        = "PostPaidTransactionBean.statisticsReportSynthesisAllRefuelModeBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_PROVINCE                        = "PostPaidTransactionBean.statisticsReportSynthesisProvince";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_VOUCHER_CONSUMED                = "PostPaidTransactionBean.statisticsReportSynthesisVoucherConsumed";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_PROMO_VOUCHER_CONSUMED          = "PostPaidTransactionBean.statisticsReportSynthesisPromoVoucherConsumed";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT                    = "PostPaidTransactionBean.statisticsReportSynthesisTotalAmount";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT                = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalAmount";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT_BUSINESS           = "PostPaidTransactionBean.statisticsReportSynthesisTotalAmountBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT_BUSINESS       = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalAmountBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY             = "PostPaidTransactionBean.statisticsReportSynthesisTotalFuelQuantity";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY         = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalFuelQuantity";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BUSINESS    = "PostPaidTransactionBean.statisticsReportSynthesisTotalFuelQuantityBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BUSINESS= "PostPaidTransactionBean.statisticsReportSynthesisAllTotalFuelQuantityBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT    = "PostPaidTransactionBean.statisticsReportSynthesisTotalFuelQuantityByProduct";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT    = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalFuelQuantityByProduct";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS    = "PostPaidTransactionBean.statisticsReportSynthesisTotalFuelQuantityByProductBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS    = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalFuelQuantityByProductBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_TOTAL_FUEL_QUANTITY = "PostPaidTransactionBean.statisticsReportSynthesisRefuelModeTotalFuelQuantity";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_TOTAL_FUEL_QUANTITY = "PostPaidTransactionBean.statisticsReportSynthesisAllRefuelModeTotalFuelQuantity";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_REFUEL_MODE_TOTAL_FUEL_QUANTITY_BUSINESS = "PostPaidTransactionBean.statisticsReportSynthesisRefuelModeTotalFuelQuantityBusiness";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_REFUEL_MODE_TOTAL_FUEL_QUANTITY_BUSINESS = "PostPaidTransactionBean.statisticsReportSynthesisAllRefuelModeTotalFuelQuantityBusiness";
    public static final String                       FIND_PAYMENT_TO_RECONCILIATION                              = "PostPaidTransactionBean.findPaymentReconciliation";
    public static final String                       FIND_VOUCHER_TO_RECONCILIATION                              = "PostPaidTransactionBean.findVoucherReconciliation";
    public static final String                       FIND_LOYALTY_TO_RECONCILIATION                              = "PostPaidTransactionBean.findLoyaltyReconciliation";
    public static final String                       FIND_BY_PAYMENT_METHOD_ID                                   = "PostPaidTransactionBean.findTransactionsByPaymentMethodId";
    public static final String                       FIND_BY_STATIONID_SOURCE_NUMBER                             = "PostPaidTransactionBean.findByStationIdAndSourceNumber";
    public static final String                       FIND_ONHOLD_BY_USER_AND_STATION                             = "PostPaidTransactionBean.findOnHoldByUserAndStation";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY                   = "PostPaidTransactionBean.statisticsReportSynthesisTotalLoyalty";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY               = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalLoyalty";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_LOYALTY_REFUELMODE              = "PostPaidTransactionBean.statisticsReportSynthesisLoyaltyRefuelmode";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_LOYALTY_REFUELMODE          = "PostPaidTransactionBean.statisticsReportSynthesisAllLoyaltyRefuelmode";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_AREA              = "PostPaidTransactionBean.statisticsReportSynthesisTotalLoyaltyArea";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_AREA          = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalLoyaltyArea";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_CREDITS           = "PostPaidTransactionBean.statisticsReportSynthesisTotalLoyaltyCredits";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_CREDITS       = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalLoyaltyCredits";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY     = "PostPaidTransactionBean.statisticsReportSynthesisTotalLoyaltyFuelQuantity";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantity";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT = "PostPaidTransactionBean.statisticsReportSynthesisTotalLoyaltyFuelQuantityByProduct";
    public static final String                       STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT = "PostPaidTransactionBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantityByProduct";    
    public static final String                       STATISTICS_REPORT_MYCICERO_POSTPAYED_TRANSACTIONS     = "PostPaidTransactionBean.statisticsReportMyciceroPostpaidTransactions";
    public static final String                       AMOUNT_REFUEL_FOR_PERIOD						     		= "PostPaidTransactionBean.amountRefuelForPeriod";
    
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                                     id;

    //	@Column(name="transactionID", nullable=true)
    //	private String transactionID;

    @Column(name = "requestID", nullable = true)
    private String                                   requestID;

    @Column(name = "source", nullable = true)
    private String                                   source;

    @Column(name = "sourceID", nullable = true)
    private String                                   sourceID;

    @Column(name = "sourceNumber", nullable = true)
    private String                                   sourceNumber;

    @Column(name = "srcTransactionID", nullable = true)
    private String                                   srcTransactionID;

    @Column(name = "mpTransactionID", nullable = true)
    private String                                   mpTransactionID;

    @Column(name = "bank_transaction_id", nullable = true)
    private String                                   bankTansactionID;

    @Column(name = "authorization_code", nullable = true)
    private String                                   authorizationCode;

    @Column(name = "token", nullable = true)
    private String                                   token;

    @Column(name = "payment_type", nullable = true)
    private String                                   paymentType;

    @Column(name = "amount", nullable = true)
    private Double                                   amount;

    @Column(name = "currency", nullable = true)
    private String                                   currency;

    @Column(name = "product_type", nullable = false)
    private String                                   productType;                                                                                                                 // OIL||NON_OIL||MIXED

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ppTransactionBean", cascade = CascadeType.ALL)
    private Set<PostPaidCartBean>                    cartBean                                           = new HashSet<PostPaidCartBean>(0);
    //	
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ppTransactionBean", cascade = CascadeType.ALL)
    private Set<PostPaidRefuelBean>                  refuelBean                                         = new HashSet<PostPaidRefuelBean>(0);

    @Column(name = "statusType", nullable = true)
    private String                                   statusType;                                                                                                                  //Reconciliation-Standard

    @Column(name = "mpTransactionStatus", nullable = true)
    private String                                   mpTransactionStatus;

    @Column(name = "srcTransactionStatus", nullable = true)
    private String                                   srcTransactionStatus;

    @Column(name = "creation_timestamp", nullable = false)
    private Date                                     creationTimestamp;

    @Column(name = "lastModify_timestamp", nullable = true)
    private Date                                     lastModifyTimestamp;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ppTransactionBean", cascade = CascadeType.ALL)
    private Set<PostPaidTransactionEventBean>        postPaidTransactionEventBean                       = new HashSet<PostPaidTransactionEventBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ppTransactionBean", cascade = CascadeType.ALL)
    private Set<PostPaidTransactionPaymentEventBean> postPaidTransactionPaymentEventBean                = new HashSet<PostPaidTransactionPaymentEventBean>(0);
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionBean", cascade = CascadeType.ALL)
    private Set<PostPaidTransactionOperationBean>    postPaidTransactionOperationBean                   = new HashSet<PostPaidTransactionOperationBean>(0);

    @Column(name = "shop_login", nullable = true)
    private String                                   shopLogin;

    @Column(name = "acquirer_id", nullable = true)
    private String                                   acquirerID;

    @Column(name = "paymentMode", nullable = true)
    private String                                   paymentMode;

    @Column(name = "payment_method_id", nullable = true)
    private Long                                     paymentMethodId;

    @Column(name = "payment_method_type", nullable = true)
    private String                                   paymentMethodType;

    @Column(name = "notificationCreated", nullable = true)
    private Boolean                                  notificationCreated;

    @Column(name = "notificationPaid", nullable = true)
    private Boolean                                  notificationPaid;

    @Column(name = "notificationUser", nullable = true)
    private Boolean                                  notificationUser;

    @OneToOne()
    @JoinColumn(name = "user_id", nullable = true)
    private UserBean                                 userBean;

    @OneToOne()
    @JoinColumn(name = "station_id", nullable = true)
    private StationBean                              stationBean;

    @Column(name = "use_voucher", nullable = true)
    private Boolean                                  useVoucher;

    @Column(name = "loyalty_reconcile", nullable = true)
    private Boolean                                  loyaltyReconcile;

    @Column(name = "voucher_reconcile", nullable = true)
    private Boolean                                  voucherReconcile;

    @Column(name = "to_reconcile", nullable = true)
    private Boolean                                  toReconcile;

    @Column(name = "reconciliation_attempts_left", nullable = true)
    private Integer                                  reconciliationAttemptsLeft;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionBean", cascade = CascadeType.ALL)
    private Set<PostPaidConsumeVoucherBean>          postPaidConsumeVoucherBeanList                     = new HashSet<PostPaidConsumeVoucherBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionBean", cascade = CascadeType.ALL)
    private Set<PostPaidLoadLoyaltyCreditsBean>      postPaidLoadLoyaltyCreditsBeanList                 = new HashSet<PostPaidLoadLoyaltyCreditsBean>(0);
    
    @Column(name = "encoded_secret_key", nullable = true)
    private String                         encodedSecretKey;
    
    @Column(name = "group_acquirer", nullable = true)
    private String                         groupAcquirer;
    
    @Column(name = "gfg_electronic_Invoice_id", nullable = true)
    private String                         gfgElectronicInvoiceID;

    @Column(name = "transaction_category", nullable = true)
    private String                            transactionCategory;
    
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "payment_token_package_id")
    private PaymentTokenPackageBean paymentTokenPackageBean;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidTransactionBean", cascade = CascadeType.ALL)
    private Set<PostPaidTransactionAdditionalDataBean> postPaidTransactionAdditionalDataBeanList = new HashSet<PostPaidTransactionAdditionalDataBean>(0);

    public PostPaidTransactionBean() {}

    public PostPaidTransactionBean(PostPaidTransaction postPaidTransaction) {

        this.id = postPaidTransaction.getId();
        this.mpTransactionID = postPaidTransaction.getMpTransactionID();
        this.userBean = new UserBean(postPaidTransaction.getUser());
        this.stationBean = new StationBean(postPaidTransaction.getStation());
        this.source = postPaidTransaction.getSource();
        this.sourceID = postPaidTransaction.getSourceID();
        this.sourceNumber = postPaidTransaction.getSourceNumber();
        this.srcTransactionID = postPaidTransaction.getSrcTransactionID();
        this.bankTansactionID = postPaidTransaction.getBankTansactionID();
        this.authorizationCode = postPaidTransaction.getAuthorizationCode();
        this.token = postPaidTransaction.getToken();
        this.paymentType = postPaidTransaction.getPaymentType();
        this.amount = postPaidTransaction.getAmount();
        this.currency = postPaidTransaction.getCurrency();
        this.productType = postPaidTransaction.getProductType();

        if (postPaidTransaction.getCartList() != null) {
            for (PostPaidCart postPaidCart : postPaidTransaction.getCartList()) {
                PostPaidCartBean postPaidCartBean = new PostPaidCartBean(postPaidCart);
                postPaidCartBean.setTransactionBean(this);
                this.cartBean.add(postPaidCartBean);
            }
        }

        if (postPaidTransaction.getRefuelList() != null) {
            for (PostPaidRefuel postPaidRefuel : postPaidTransaction.getRefuelList()) {
                PostPaidRefuelBean postPaidRefuelBean = new PostPaidRefuelBean(postPaidRefuel);
                postPaidRefuelBean.setTransactionBean(this);
                this.refuelBean.add(postPaidRefuelBean);
            }
        }

        this.statusType = postPaidTransaction.getStatusType();
        this.mpTransactionStatus = postPaidTransaction.getMpTransactionStatus();
        this.srcTransactionStatus = postPaidTransaction.getSrcTransactionStatus();
        this.creationTimestamp = postPaidTransaction.getCreationTimestamp();
        this.lastModifyTimestamp = postPaidTransaction.getLastModifyTimestamp();

        if (postPaidTransaction.getPostPaidTransactionEventList() != null) {
            for (PostPaidTransactionEvent postPaidTransactionEvent : postPaidTransaction.getPostPaidTransactionEventList()) {
                PostPaidTransactionEventBean postPaidTransactionEventBean = new PostPaidTransactionEventBean(postPaidTransactionEvent);
                postPaidTransactionEventBean.setTransactionBean(this);
                this.postPaidTransactionEventBean.add(postPaidTransactionEventBean);
            }
        }

        if (postPaidTransaction.getPostPaidTransactionPaymentEventList() != null) {
            for (PostPaidTransactionPaymentEvent postPaidTransactionPaymentEvent : postPaidTransaction.getPostPaidTransactionPaymentEventList()) {
                PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean = new PostPaidTransactionPaymentEventBean(postPaidTransactionPaymentEvent);
                postPaidTransactionPaymentEventBean.setTransactionBean(this);
                this.postPaidTransactionPaymentEventBean.add(postPaidTransactionPaymentEventBean);
            }
        }

        this.shopLogin = postPaidTransaction.getShopLogin();
        this.acquirerID = postPaidTransaction.getAcquirerID();
        this.paymentMode = postPaidTransaction.getPaymentMode();
        this.paymentMethodId = postPaidTransaction.getPaymentMethodId();
        this.paymentMethodType = postPaidTransaction.getPaymentMethodType();
        this.notificationCreated = postPaidTransaction.getNotificationCreated();
        this.notificationPaid = postPaidTransaction.getNotificationPaid();
        this.notificationUser = postPaidTransaction.getNotificationUser();
        this.useVoucher = postPaidTransaction.getUseVoucher();
        this.voucherReconcile = false;
        this.loyaltyReconcile = false;
        this.toReconcile = false;

        if (postPaidTransaction.getPostPaidConsumeVoucherList() != null) {
            for (PostPaidConsumeVoucher postPaidConsumeVoucher : postPaidTransaction.getPostPaidConsumeVoucherList()) {
                PostPaidConsumeVoucherBean postPaidConsumeVoucherBean = new PostPaidConsumeVoucherBean(postPaidConsumeVoucher);
                postPaidConsumeVoucherBean.setPostPaidTransactionBean(this);
                this.postPaidConsumeVoucherBeanList.add(postPaidConsumeVoucherBean);
            }
        }

        if (postPaidTransaction.getPostPaidLoadLoyaltyCreditsList() != null) {
            for (PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits : postPaidTransaction.getPostPaidLoadLoyaltyCreditsList()) {
                PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean(postPaidLoadLoyaltyCredits);
                postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(this);
                this.postPaidLoadLoyaltyCreditsBeanList.add(postPaidLoadLoyaltyCreditsBean);
            }
        }
        
        this.groupAcquirer = postPaidTransaction.getGroupAcquirer();
        this.encodedSecretKey = postPaidTransaction.getEncodedSecretKey();
        this.gfgElectronicInvoiceID = postPaidTransaction.getGFGElectronicInvoiceID();
        
        if (postPaidTransaction.getPostPaidTransactionAdditionalDataList() != null) {
            for (PostPaidTransactionAdditionalData postPaidTransactionAdditionalData : postPaidTransaction.getPostPaidTransactionAdditionalDataList()) {
                PostPaidTransactionAdditionalDataBean postPaidTransactionAdditionalDataBean = new PostPaidTransactionAdditionalDataBean(postPaidTransactionAdditionalData);
                postPaidTransactionAdditionalDataBean.setPostPaidTransactionBean(this);
                this.postPaidTransactionAdditionalDataBeanList.add(postPaidTransactionAdditionalDataBean);
            }
        }
        
        if (postPaidTransaction.getPostPaidTransactionOperationBeanList() != null) {
            for (PostPaidTransactionOperation postPaidTransactionOperation : postPaidTransaction.getPostPaidTransactionOperationBeanList()) {
                PostPaidTransactionOperationBean postPaidTransactionOperationBean = new PostPaidTransactionOperationBean(postPaidTransactionOperation);
                postPaidTransactionOperationBean.setPostPaidTransactionBean(this);
                this.postPaidTransactionOperationBean.add(postPaidTransactionOperationBean);
            }
        }
    }

    public PostPaidTransactionBean(PostPaidTransactionHistoryBean poPTransactionBean) {

        //this.id = poPTransactionBean.getId();
        this.requestID = poPTransactionBean.getRequestID();
        this.source = poPTransactionBean.getSource();
        this.sourceID = poPTransactionBean.getSourceID();
        this.sourceNumber = poPTransactionBean.getSourceNumber();
        this.srcTransactionID = poPTransactionBean.getSrcTransactionID();
        this.mpTransactionID = poPTransactionBean.getMpTransactionID();
        this.bankTansactionID = poPTransactionBean.getBankTansactionID();
        this.authorizationCode = poPTransactionBean.getAuthorizationCode();
        this.token = poPTransactionBean.getToken();
        this.paymentType = poPTransactionBean.getPaymentType();
        this.amount = poPTransactionBean.getAmount();
        this.currency = poPTransactionBean.getCurrency();
        this.productType = poPTransactionBean.getProductType();
        this.toReconcile = poPTransactionBean.getToReconcile();

        if (poPTransactionBean.getCartHistoryBean() != null) {
            Set<PostPaidCartHistoryBean> cartBeanData = poPTransactionBean.getCartHistoryBean();
            for (PostPaidCartHistoryBean cartBean : cartBeanData) {
                PostPaidCartBean postPaidCartBean = new PostPaidCartBean(cartBean);
                postPaidCartBean.setTransactionBean(this);
                this.cartBean.add(postPaidCartBean);
            }
        }
        if (poPTransactionBean.getRefuelHistoryBean() != null) {
            Set<PostPaidRefuelHistoryBean> refuelBeanData = poPTransactionBean.getRefuelHistoryBean();
            for (PostPaidRefuelHistoryBean refuelBean : refuelBeanData) {
                PostPaidRefuelBean postPaidRefuelBean = new PostPaidRefuelBean(refuelBean);
                postPaidRefuelBean.setTransactionBean(this);
                this.refuelBean.add(postPaidRefuelBean);
            }
        }

        this.statusType = poPTransactionBean.getStatusType();
        this.mpTransactionStatus = poPTransactionBean.getMpTransactionStatus();
        this.srcTransactionStatus = poPTransactionBean.getSrcTransactionStatus();
        this.creationTimestamp = poPTransactionBean.getCreationTimestamp();
        this.lastModifyTimestamp = poPTransactionBean.getLastModifyTimestamp();

        if (poPTransactionBean.getPostPaidTransactionEventHistoryBean() != null) {
            Set<PostPaidTransactionEventHistoryBean> postPaidTransactionEventBeanData = poPTransactionBean.getPostPaidTransactionEventHistoryBean();
            for (PostPaidTransactionEventHistoryBean postPaidTransactionEventHistoryBean : postPaidTransactionEventBeanData) {
                PostPaidTransactionEventBean postPaidTransactionEventBean = new PostPaidTransactionEventBean(postPaidTransactionEventHistoryBean);
                postPaidTransactionEventBean.setTransactionBean(this);
                this.postPaidTransactionEventBean.add(postPaidTransactionEventBean);
            }
        }

        if (poPTransactionBean.getPostPaidTransactionPaymentEventHistoryBean() != null) {
            Set<PostPaidTransactionPaymentEventHistoryBean> postPaidTransactionPaymentEventBeanData = poPTransactionBean.getPostPaidTransactionPaymentEventHistoryBean();
            for (PostPaidTransactionPaymentEventHistoryBean postPaidTransactionPaymentEventHistoryBean : postPaidTransactionPaymentEventBeanData) {
                PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean = new PostPaidTransactionPaymentEventBean(postPaidTransactionPaymentEventHistoryBean);
                postPaidTransactionPaymentEventBean.setTransactionBean(this);
                this.postPaidTransactionPaymentEventBean.add(postPaidTransactionPaymentEventBean);
            }
        }

        this.shopLogin = poPTransactionBean.getShopLogin();
        this.acquirerID = poPTransactionBean.getAcquirerID();
        this.paymentMode = poPTransactionBean.getPaymentMode();
        this.paymentMethodId = poPTransactionBean.getPaymentMethodId();
        this.paymentMethodType = poPTransactionBean.getPaymentMethodType();
        this.notificationCreated = poPTransactionBean.getNotificationCreated();
        this.notificationPaid = poPTransactionBean.getNotificationPaid();
        this.notificationUser = poPTransactionBean.getNotificationUser();
        this.userBean = poPTransactionBean.getUserBean();
        this.stationBean = poPTransactionBean.getStationBean();
        this.useVoucher = poPTransactionBean.getUseVoucher();

        if (poPTransactionBean.getPostPaidConsumeVoucherBeanList() != null) {
            Set<PostPaidConsumeVoucherHistoryBean> postPaidConsumeVoucherHistoryBeanData = poPTransactionBean.getPostPaidConsumeVoucherBeanList();
            for (PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean : postPaidConsumeVoucherHistoryBeanData) {
                PostPaidConsumeVoucherBean postPaidConsumeVoucherBean = new PostPaidConsumeVoucherBean(postPaidConsumeVoucherHistoryBean);
                postPaidConsumeVoucherBean.setPostPaidTransactionBean(this);
                this.postPaidConsumeVoucherBeanList.add(postPaidConsumeVoucherBean);
            }
        }

        if (poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList() != null) {
            Set<PostPaidLoadLoyaltyCreditsHistoryBean> postPaidLoadLoyaltyCreditsHistoryBeanData = poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList();
            for (PostPaidLoadLoyaltyCreditsHistoryBean postPaidLoadLoyaltyCreditsHistoryBean : postPaidLoadLoyaltyCreditsHistoryBeanData) {
                PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean(postPaidLoadLoyaltyCreditsHistoryBean);
                postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(this);
                this.postPaidLoadLoyaltyCreditsBeanList.add(postPaidLoadLoyaltyCreditsBean);
            }
        }

        this.groupAcquirer = poPTransactionBean.getGroupAcquirer();
        this.encodedSecretKey = poPTransactionBean.getEncodedSecretKey();
        this.gfgElectronicInvoiceID = poPTransactionBean.getGFGElectronicInvoiceID();
        this.transactionCategory = poPTransactionBean.getTransactionCategory().getValue();
        
        if (poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList() != null) {
            Set<PostPaidLoadLoyaltyCreditsHistoryBean> postPaidLoadLoyaltyCreditsHistoryBeanData = poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList();
            for (PostPaidLoadLoyaltyCreditsHistoryBean postPaidLoadLoyaltyCreditsHistoryBean : postPaidLoadLoyaltyCreditsHistoryBeanData) {
                PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean(postPaidLoadLoyaltyCreditsHistoryBean);
                postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(this);
                this.postPaidLoadLoyaltyCreditsBeanList.add(postPaidLoadLoyaltyCreditsBean);
            }
        }
    }

    public PostPaidTransaction toPostPaidTransaction() {

        PostPaidTransaction postPaidTransaction = new PostPaidTransaction();
        postPaidTransaction.setId(this.id);
        postPaidTransaction.setMpTransactionID(this.mpTransactionID);
        postPaidTransaction.setUser(this.getUserBean().toUser());
        postPaidTransaction.setStation(this.getStationBean().toStation());
        postPaidTransaction.setSource(this.source);
        postPaidTransaction.setSourceID(this.sourceID);
        postPaidTransaction.setSourceNumber(this.sourceNumber);
        postPaidTransaction.setSrcTransactionID(this.srcTransactionID);
        postPaidTransaction.setBankTansactionID(this.bankTansactionID);
        postPaidTransaction.setAuthorizationCode(this.authorizationCode);
        postPaidTransaction.setToken(this.token);
        postPaidTransaction.setPaymentType(this.paymentType);
        postPaidTransaction.setAmount(this.amount);
        postPaidTransaction.setCurrency(this.currency);
        postPaidTransaction.setProductType(this.productType);

        if (!this.cartBean.isEmpty()) {

            for (PostPaidCartBean postPaidCartBean : this.cartBean) {
                PostPaidCart postPaidCart = postPaidCartBean.toPostPaidCart();
                postPaidTransaction.getCartList().add(postPaidCart);
            }
        }

        if (!this.refuelBean.isEmpty()) {

            for (PostPaidRefuelBean postPaidRefuelBean : this.refuelBean) {
                PostPaidRefuel postPaidRefuel = postPaidRefuelBean.toPostPaidRefuel();
                postPaidTransaction.getRefuelList().add(postPaidRefuel);
            }
        }

        postPaidTransaction.setStatusType(this.statusType);
        postPaidTransaction.setMpTransactionStatus(this.mpTransactionStatus);
        postPaidTransaction.setSrcTransactionStatus(this.srcTransactionStatus);
        postPaidTransaction.setCreationTimestamp(this.creationTimestamp);
        postPaidTransaction.setLastModifyTimestamp(this.lastModifyTimestamp);

        if (!this.postPaidTransactionEventBean.isEmpty()) {

            for (PostPaidTransactionEventBean postPaidTransactionEventBean : this.postPaidTransactionEventBean) {
                PostPaidTransactionEvent postPaidTransactionEvent = postPaidTransactionEventBean.toPostPaidTransactionEvent();
                postPaidTransaction.getPostPaidTransactionEventList().add(postPaidTransactionEvent);
            }
        }

        if (!this.postPaidTransactionPaymentEventBean.isEmpty()) {

            for (PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean : this.postPaidTransactionPaymentEventBean) {
                PostPaidTransactionPaymentEvent postPaidTransactionPaymentEvent = postPaidTransactionPaymentEventBean.toPostPaidTransactionPaymentEvent();
                postPaidTransaction.getPostPaidTransactionPaymentEventList().add(postPaidTransactionPaymentEvent);
            }
        }

        postPaidTransaction.setShopLogin(this.shopLogin);
        postPaidTransaction.setAcquirerID(this.acquirerID);
        postPaidTransaction.setPaymentMode(this.paymentMode);
        postPaidTransaction.setPaymentMethodId(this.paymentMethodId);
        postPaidTransaction.setPaymentMethodType(this.paymentMethodType);
        postPaidTransaction.setNotificationCreated(this.notificationCreated);
        postPaidTransaction.setNotificationPaid(this.notificationPaid);
        postPaidTransaction.setNotificationUser(this.notificationUser);

        if (!this.postPaidConsumeVoucherBeanList.isEmpty()) {

            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : this.postPaidConsumeVoucherBeanList) {
                PostPaidConsumeVoucher PostPaidConsumeVoucher = postPaidConsumeVoucherBean.toPostPaidConsumeVoucher();
                postPaidTransaction.getPostPaidConsumeVoucherList().add(PostPaidConsumeVoucher);
            }
        }

        if (!this.postPaidLoadLoyaltyCreditsBeanList.isEmpty()) {

            for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : this.postPaidLoadLoyaltyCreditsBeanList) {
                PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits = postPaidLoadLoyaltyCreditsBean.toPostPaidLoadLoyaltyCredits();
                postPaidTransaction.getPostPaidLoadLoyaltyCreditsList().add(postPaidLoadLoyaltyCredits);
            }
        }
        
        if (!this.postPaidTransactionOperationBean.isEmpty()) {

            for (PostPaidTransactionOperationBean postPaidTransactionOperationBean : this.postPaidTransactionOperationBean) {
                PostPaidTransactionOperation postPaidTransactionOperation = postPaidTransactionOperationBean.toPostPaidTransactionOperation();
                postPaidTransaction.getPostPaidTransactionOperationBeanList().add(postPaidTransactionOperation);
            }
        }
        
        postPaidTransaction.setGroupAcquirer(this.groupAcquirer);
        postPaidTransaction.setEncodedSecretKey(this.encodedSecretKey);
        postPaidTransaction.setEncodedSecretKey(this.encodedSecretKey);
        postPaidTransaction.setGFGElectronicInvoiceID(this.gfgElectronicInvoiceID);
        postPaidTransaction.setTransactionCategory(getTransactionCategory());

        return postPaidTransaction;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public String getBankTansactionID() {
        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {
        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Set<PostPaidCartBean> getCartBean() {
        return cartBean;
    }

    public Set<PostPaidTransactionPaymentEventBean> getPostPaidTransactionPaymentEventBean() {
        return postPaidTransactionPaymentEventBean;
    }

    public void setPostPaidTransactionPaymentEventBean(Set<PostPaidTransactionPaymentEventBean> postPaidTransactionPaymentEventBean) {
        this.postPaidTransactionPaymentEventBean = postPaidTransactionPaymentEventBean;
    }

    public void setCartBean(Set<PostPaidCartBean> cartBean) {
        this.cartBean = cartBean;
    }

    public Set<PostPaidRefuelBean> getRefuelBean() {
        return refuelBean;
    }

    public void setRefuelBean(Set<PostPaidRefuelBean> refuelBean) {
        this.refuelBean = refuelBean;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public String getMpTransactionStatus() {
        return mpTransactionStatus;
    }

    public void setMpTransactionStatus(String mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }

    public String getSrcTransactionStatus() {
        return srcTransactionStatus;
    }

    public void setSrcTransactionStatus(String srcTransactionStatus) {
        this.srcTransactionStatus = srcTransactionStatus;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getLastModifyTimestamp() {
        return lastModifyTimestamp;
    }

    public void setLastModifyTimestamp(Date lastModifyTimestamp) {
        this.lastModifyTimestamp = lastModifyTimestamp;
    }

    public Set<PostPaidTransactionEventBean> getPostPaidTransactionEventBean() {
        return postPaidTransactionEventBean;
    }

    public void setPostPaidTransactionEventBean(Set<PostPaidTransactionEventBean> postPaidTransactionEventBean) {
        this.postPaidTransactionEventBean = postPaidTransactionEventBean;
    }

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
        this.acquirerID = acquirerID;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public Boolean getNotificationCreated() {
        return notificationCreated;
    }

    public void setNotificationCreated(Boolean notificationCreated) {
        this.notificationCreated = notificationCreated;
    }

    public Boolean getNotificationPaid() {
        return notificationPaid;
    }

    public void setNotificationPaid(Boolean notificationPaid) {
        this.notificationPaid = notificationPaid;
    }

    public Boolean getNotificationUser() {
        return notificationUser;
    }

    public void setNotificationUser(Boolean notificationUser) {
        this.notificationUser = notificationUser;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public StationBean getStationBean() {
        return stationBean;
    }

    public void setStationBean(StationBean stationBean) {
        this.stationBean = stationBean;
    }

    public String getSourceNumber() {
        return sourceNumber;
    }

    public void setSourceNumber(String sourceNumber) {
        this.sourceNumber = sourceNumber;
    }

    public Boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }

    public Boolean getLoyaltyReconcile() {
        return loyaltyReconcile;
    }

    public void setLoyaltyReconcile(Boolean loyaltyReconcile) {
        this.loyaltyReconcile = loyaltyReconcile;
    }

    public Boolean getVoucherReconcile() {
        return voucherReconcile;
    }

    public void setVoucherReconcile(Boolean voucherReconcile) {
        this.voucherReconcile = voucherReconcile;
    }

    public Boolean getToReconcile() {
        return toReconcile;
    }

    public void setToReconcile(Boolean toReconcile) {
        this.toReconcile = toReconcile;
    }

    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public Set<PostPaidConsumeVoucherBean> getPostPaidConsumeVoucherBeanList() {
        return postPaidConsumeVoucherBeanList;
    }

    public void setPostPaidConsumeVoucherBeanList(Set<PostPaidConsumeVoucherBean> postPaidConsumeVoucherBeanList) {
        this.postPaidConsumeVoucherBeanList = postPaidConsumeVoucherBeanList;
    }

    public Set<PostPaidLoadLoyaltyCreditsBean> getPostPaidLoadLoyaltyCreditsBeanList() {
        return postPaidLoadLoyaltyCreditsBeanList;
    }

    public void setPostPaidLoadLoyaltyCreditsBeanList(Set<PostPaidLoadLoyaltyCreditsBean> postPaidLoadLoyaltyCreditsBeanList) {
        this.postPaidLoadLoyaltyCreditsBeanList = postPaidLoadLoyaltyCreditsBeanList;
    }

    public PostPaidConsumeVoucherBean getLastPostPaidConsumeVoucherBean() {

        PostPaidConsumeVoucherBean lastPostPaidConsumeVoucherBean = null;

        Long maxRequestTimestamp = 0l;

        for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : this.getPostPaidConsumeVoucherBeanList()) {

            if (postPaidConsumeVoucherBean.getRequestTimestamp() > maxRequestTimestamp) {

                lastPostPaidConsumeVoucherBean = postPaidConsumeVoucherBean;

                maxRequestTimestamp = postPaidConsumeVoucherBean.getRequestTimestamp();
            }
        }

        return lastPostPaidConsumeVoucherBean;
    }

    public PostPaidLoadLoyaltyCreditsBean getLastPostPaidLoadLoyaltyCreditsBean() {

        PostPaidLoadLoyaltyCreditsBean lastPostPaidLoadLoyaltyCreditsBean = null;

        Long maxRequestTimestamp = 0l;

        for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : this.getPostPaidLoadLoyaltyCreditsBeanList()) {

            if (postPaidLoadLoyaltyCreditsBean.getRequestTimestamp() > maxRequestTimestamp) {

                lastPostPaidLoadLoyaltyCreditsBean = postPaidLoadLoyaltyCreditsBean;

                maxRequestTimestamp = postPaidLoadLoyaltyCreditsBean.getRequestTimestamp();
            }
        }

        return lastPostPaidLoadLoyaltyCreditsBean;
    }

    public PostPaidTransactionEventBean getLastPostPaidTransactionEventBean() {
        PostPaidTransactionEventBean poPTransactionEvent = null;
        int sequence = 0;

        for (PostPaidTransactionEventBean postPaidTransactionEventBean : getPostPaidTransactionEventBean()) {
            //System.out.println("sequenceID:" + postPaidTransactionEventBean.getSequenceID());
            if (postPaidTransactionEventBean.getSequenceID() != null && postPaidTransactionEventBean.getSequenceID().intValue() > sequence) {
                poPTransactionEvent = postPaidTransactionEventBean;
                sequence = postPaidTransactionEventBean.getSequenceID().intValue();
                //System.out.println("sequence selected:" + sequence);
            }
        }

        //System.out.println("poPTransactionEvent sequenceID:" + (poPTransactionEvent != null ? poPTransactionEvent.getSequenceID().intValue() : 0));

        return poPTransactionEvent;
    }

    public PostPaidTransactionEventBean getPostPaidTransactionEventBean(String event, boolean firstMatched) {
        PostPaidTransactionEventBean poPTransactionEvent = null;

        for (PostPaidTransactionEventBean postPaidTransactionEventBean : getPostPaidTransactionEventBean()) {
            //System.out.println("sequenceID:" + postPaidTransactionEventBean.getSequenceID());
            if (postPaidTransactionEventBean.getEvent().equals(event)) {
                poPTransactionEvent = postPaidTransactionEventBean;
                if (firstMatched) {
                    return poPTransactionEvent;
                }
                //System.out.println("sequence selected:" + sequence);
            }
        }

        //System.out.println("poPTransactionEvent sequenceID:" + (poPTransactionEvent != null ? poPTransactionEvent.getSequenceID().intValue() : 0));

        return poPTransactionEvent;
    }

    public PostPaidTransactionPaymentEventBean getLastPostPaidTransactionPaymentEvent() {
        PostPaidTransactionPaymentEventBean poPTransactionPaymentEvent = null;
        int sequence = 0;

        for (PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean : getPostPaidTransactionPaymentEventBean()) {
            if (postPaidTransactionPaymentEventBean.getSequence() != null && postPaidTransactionPaymentEventBean.getSequence().intValue() > sequence) {
                poPTransactionPaymentEvent = postPaidTransactionPaymentEventBean;
                sequence = postPaidTransactionPaymentEventBean.getSequence().intValue();
            }
        }

        return poPTransactionPaymentEvent;
    }

    public PostPaidTransactionData toPoPTransactionData() {
        PostPaidTransactionData postPaidTransactionData = new PostPaidTransactionData();
        postPaidTransactionData.setAcquirerID(this.getAcquirerID());
        postPaidTransactionData.setAmount(this.getAmount());
        postPaidTransactionData.setAuthorizationCode(this.getAuthorizationCode());
        postPaidTransactionData.setCreationTimestamp(this.creationTimestamp);

        return postPaidTransactionData;

    }

    @Override
    public String getTransactionID() {
        return getMpTransactionID();
    }

    @Override
    public Date getEndRefuelTimestamp() {
        if (!getRefuelBean().isEmpty()) {
            return getRefuelBean().iterator().next().getTimestampEndRefuel();
        }
        return null;
    }

    @Override
    public String getPumpID() {
        if (!getRefuelBean().isEmpty()) {
            return getRefuelBean().iterator().next().getPumpId();
        }
        return null;
    }

    @Override
    public Integer getPumpNumber() {
        if (!getRefuelBean().isEmpty()) {
            return getRefuelBean().iterator().next().getPumpNumber();
        }
        return null;
    }

    @Override
    public String getProductID() {
        if (!getRefuelBean().isEmpty()) {
            return getRefuelBean().iterator().next().getProductId();
        }
        return null;
    }

    @Override
    public String getProductDescription() {
        if (!getRefuelBean().isEmpty()) {
            return getRefuelBean().iterator().next().getProductDescription();
        }
        return null;
    }

    @Override
    public String getTransactionStatus() {
        return getMpTransactionStatus();
    }

    @Override
    public Boolean getGFGNotification() {
        return getNotificationPaid();
    }

    @Override
    public String getRefuelMode() {
        if (!getRefuelBean().isEmpty()) {
            return getRefuelBean().iterator().next().getRefuelMode();
        }
        return null;
    }
    
    @Override
    public Double getUnitPrice() {
        if (!getRefuelBean().isEmpty()) {
            return getRefuelBean().iterator().next().getUnitPrice();
        }
        return null;
    }

    @Override
    public void setTransactionStatus(String transactionStatus) {
        setMpTransactionStatus(transactionStatus);
    }

    @Override
    public String getLastStatusEventCode() {
        PostPaidTransactionEventBean postPaidTransactionEvent = getLastPostPaidTransactionEventBean();

        if (postPaidTransactionEvent != null) {
            return postPaidTransactionEvent.getNewState();
        }

        return null;
    }

    @Override
    public String getLastStatusEvent() {
        PostPaidTransactionEventBean postPaidTransactionEvent = getLastPostPaidTransactionEventBean();

        if (postPaidTransactionEvent != null) {
            return postPaidTransactionEvent.getEvent();
        }

        return null;
    }

    public String getLastStatusEventResult() {
        PostPaidTransactionEventBean postPaidTransactionEvent = getLastPostPaidTransactionEventBean();

        if (postPaidTransactionEvent != null) {
            return postPaidTransactionEvent.getResult();
        }

        return null;
    }
    
    public Double getInitialAmount() {
        return amount;
    }
    
    public TransactionInterfaceType getTransactionType() {
        return TransactionInterfaceType.TRANSACTION_POSTPAID;
    }

    public String getEncodedSecretKey() {
        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {
        this.encodedSecretKey = encodedSecretKey;
    }
    
    public String getGroupAcquirer() {
        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {
        this.groupAcquirer = groupAcquirer;
    }
    
    public String getGFGElectronicInvoiceID() {
        return gfgElectronicInvoiceID;
    }
    
    public void setGFGElectronicInvoiceID(String gfgElectronicInvoiceID) {
        this.gfgElectronicInvoiceID = gfgElectronicInvoiceID;
    }
    
    public TransactionCategoryType getTransactionCategory() {
        if (transactionCategory == null) {
            return TransactionCategoryType.TRANSACTION_CUSTOMER;
        }
        
        return TransactionCategoryType.getValueOf(transactionCategory);
    }
    
    public void setTransactionCategory(TransactionCategoryType transactionCategory) {
        this.transactionCategory = transactionCategory.getValue();
    }

    public Set<PostPaidTransactionAdditionalDataBean> getPostPaidTransactionAdditionalDataBeanList() {
        return postPaidTransactionAdditionalDataBeanList;
    }

    public void setPostPaidTransactionAdditionalDataBeanList(Set<PostPaidTransactionAdditionalDataBean> postPaidTransactionAdditionalDataBeanList) {
        this.postPaidTransactionAdditionalDataBeanList = postPaidTransactionAdditionalDataBeanList;
    }

    public PaymentTokenPackageBean getPaymentTokenPackageBean() {
        return paymentTokenPackageBean;
    }

    public void setPaymentTokenPackageBean(PaymentTokenPackageBean paymentTokenPackageBean) {
        this.paymentTokenPackageBean = paymentTokenPackageBean;
    }

    public Set<PostPaidTransactionOperationBean> getPostPaidTransactionOperationBean() {
        return postPaidTransactionOperationBean;
    }

    public void setPostPaidTransactionOperationBean(Set<PostPaidTransactionOperationBean> postPaidTransactionOperationBean) {
        this.postPaidTransactionOperationBean = postPaidTransactionOperationBean;
    }

    @Override
    public String getServerName() {
        return "";
    }
    
}
