package com.techedge.mp.core.business.model;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="SURVEY_SUBMISSION")
@NamedQueries({
    @NamedQuery(name="SurveySubmissionBean.findByKey", query="select s from SurveySubmissionBean s where key = :key"),
    @NamedQuery(name="SurveySubmissionBean.findBySurveyCode", query="select s from SurveySubmissionBean s where surveyBean.code = :surveyCode")
})

public class SurveySubmissionBean {
	
	public static final String FIND_BY_KEY = "SurveySubmissionBean.findByKey";
	public static final String FIND_BY_SURVEY_CODE = "SurveySubmissionBean.findBySurveyCode";

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	
    @Column(name="[key]", nullable=true)
    private String key;
    
    @Column(name="[timestamp]", nullable=true)
    private Date timpestamp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "survey_id", nullable = false)
    private SurveyBean           surveyBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surveySubmissionBean", cascade = CascadeType.ALL)
    private Set<SurveySubmissionAnswerBean> answersList = new HashSet<SurveySubmissionAnswerBean>(0);
	
		
	public SurveySubmissionBean() {
	}


    /**
     * @return the id
     */
    public long getId() {
        return id;
    }


    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }


    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }


    /**
     * @return the timpestamp
     */
    public Date getTimpestamp() {
        return timpestamp;
    }


    /**
     * @param timpestamp the timpestamp to set
     */
    public void setTimpestamp(Date timpestamp) {
        this.timpestamp = timpestamp;
    }


    /**
     * @return the surveyBean
     */
    public SurveyBean getSurveyBean() {
        return surveyBean;
    }


    /**
     * @param surveyBean the surveyBean to set
     */
    public void setSurveyBean(SurveyBean surveyBean) {
        this.surveyBean = surveyBean;
    }


    /**
     * @return the answersList
     */
    public Set<SurveySubmissionAnswerBean> getAnswersList() {
        return answersList;
    }


    /**
     * @param answersList the answersList to set
     */
    public void setAnswersList(Set<SurveySubmissionAnswerBean> answersList) {
        this.answersList = answersList;
    }



	
	
}
