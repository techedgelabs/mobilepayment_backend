package com.techedge.mp.core.business.model.voucher;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.model.EventCampaignBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;

@Entity
@Table(name = "VOUCHER_EVENT_CAMPAIGN")
@NamedQueries({ @NamedQuery(name = "VoucherEventCampaignBean.findVoucherByUserID", query = "select v from VoucherEventCampaignBean v where v.userBean = :userBean and v.eventCampaignBean = :eventCampaignBean" ) })
public class VoucherEventCampaignBean {

    public static final String FIND_BY_USER = "VoucherEventCampaignBean.findVoucherByUserID";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "insert_timestamp", nullable = false)
    private Date               insertTimestamp;

    @Column(name = "status", nullable = true)
    private String             status;

    @Column(name = "request", nullable = false)
    private String             request;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_campaign_id", nullable = false)
    private EventCampaignBean  eventCampaignBean;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserBean           userBean;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "voucher_id")
    private VoucherBean        voucherBean;

    public VoucherEventCampaignBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getInsertTimestamp() {
        return insertTimestamp;
    }

    public void setInsertTimestamp(Date insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public EventCampaignBean getEventCampaignBean() {
        return eventCampaignBean;
    }

    public void setEventCampaignBean(EventCampaignBean eventCampaignBean) {
        this.eventCampaignBean = eventCampaignBean;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public VoucherBean getVoucherBean() {
        return voucherBean;
    }

    public void setVoucherBean(VoucherBean voucherBean) {
        this.voucherBean = voucherBean;
    }

}
