package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "REFUELING_SUBSCRIPTIONS")
@NamedQueries({ @NamedQuery(name = "RefuelingSubscriptionsBean.getNotifyByRequestId", query = "select d from RefuelingSubscriptionsBean d where d.requestId = :requestId"),
        @NamedQuery(name = "RefuelingSubscriptionsBean.getNotifyByID", query = "select d from RefuelingSubscriptionsBean d where d.id = :id"),
        @NamedQuery(name = "RefuelingSubscriptionsBean.findAll", query = "select d from RefuelingSubscriptionsBean d"),
        @NamedQuery(name = "RefuelingSubscriptionsBean.findToReconcilie", query = "select d from RefuelingSubscriptionsBean d where d.toReconcile = 1") })
public class RefuelingSubscriptionsBean implements UserReconciliationInterfaceBean {

    public static final String FIND_BY_NOTIFY_REQUESTID = "RefuelingSubscriptionsBean.getNotifyByRequestId";
    public static final String FIND_BY_ID               = "RefuelingSubscriptionsBean.getNotifyByID";
    public static final String FIND_ALL                 = "RefuelingSubscriptionsBean.findAll";
    public static final String FIND_TO_RECONCILIE       = "RefuelingSubscriptionsBean.findToReconcilie";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "operation", nullable = false, length = 56)
    private String             operation;

    @Column(name = "request_id", nullable = false)
    private String             requestId;
    
    @Column(name = "status_code", nullable = false)
    private String             statusCode;
    
    @Column(name = "status_message", nullable = false)
    private String             statusMessage;
    
    @Column(name = "to_reconcile", nullable = true)
    private Boolean            toReconcile;
    
    @Column(name = "reconciliation_attempts_left", nullable = true)
    private Integer                               reconciliationAttemptsLeft;

    @Column(name = "operation_timestamp")
    private Date    operationTimestamp;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserBean           user;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Boolean getToReconcile() {
        return getToReconcilie();
    }

    public void setToReconcile(Boolean toReconcile) {
        setToReconcilie(toReconcile);
    }

    @Override
    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }

    @Override
    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    @Override
    public UserBean getUserBean() {
        return user;
    }

    @Override
    public String getFinalStatus() {
         return getStatusCode();
    }

    @Override
    public void setFinalStatus(String status) {
        setStatusCode(status);
    }

    
    public void setToReconcilie(Boolean toReconcilie) {
        this.toReconcile = toReconcilie;
    }

    @Override
    public Boolean getToReconcilie() {
        return toReconcile;
    }

    @Override
    public String getOperationName() {
        return getOperation();
    }

    @Override
    public Date getOperationTimestamp() {
        return operationTimestamp;
    }

    public void setOperationTimestamp(Date operationTimestamp) {
        this.operationTimestamp = operationTimestamp;
    }
    
    @Override
    public String getFinalStatusMessage() {
        return getStatusMessage();
    }

    @Override
    public String getServerEndpoint() {
        return "Backend Enjoy";
    }

}