package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.MailingList;

@Entity
@Table(name = "MAILING_LIST")
@NamedQueries({
    @NamedQuery(name = "MailingListBean.findAll", query = "select m from MailingListBean m"),
    @NamedQuery(name = "MailingListBean.findById", query = "select m from MailingListBean m where m.id = :id"),
    @NamedQuery(name = "MailingListBean.findByStatus", query = "select m from MailingListBean m where m.status = :status")
})
public class MailingListBean {

    public static final String FIND_ALL       = "MailingListBean.findAll";
    public static final String FIND_BY_ID = "MailingListBean.findById";
    public static final String FIND_BY_STATUS = "MailingListBean.findByStatus";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "email", nullable = false)
    private String             email;
    
    @Column(name = "name", nullable = false)
    private String             name;

    @Column(name = "status", nullable = false)
    private Integer            status;

    @Column(name = "timestamp", nullable = false)
    private Date               timestamp;

    @Column(name = "template", nullable = false)
    private String             template;

    public MailingListBean() {}
    
    public MailingListBean(MailingList mailingList) {
        this.id = mailingList.getId();
        this.email = mailingList.getEmail();
        this.name = mailingList.getName();
        this.status = mailingList.getStatus();
        this.timestamp = mailingList.getTimestamp();
        this.template = mailingList.getTemplate();
    }
    
    public MailingList toMailingList() {
        MailingList mailingList = new MailingList();
        mailingList.setId(this.id);
        mailingList.setEmail(this.email);
        mailingList.setName(this.name);
        mailingList.setStatus(this.status);
        mailingList.setTimestamp(this.timestamp);
        mailingList.setTemplate(this.template);
        return mailingList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

}
