package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "STATISTICAL_REPORTS")
@NamedQueries({
    //@NamedQuery(name = "StatisticalReportsBean.findReportForType", query = "select t from StatisticalReportsBean t where t.typeExtraction = :typeExtraction and dateRif >= :dateRif and dateRif <= :dateRif")
	@NamedQuery(name = "StatisticalReportsBean.findReportForType", query = "select t from StatisticalReportsBean t where t.typeExtraction = :typeExtraction and idRif = :idRif")
})
public class StatisticalReportsBean {
	
    public static final String                              STATISTICS_REPORT_SYNTHESIS_FIND_REPORT_FOR_TYPE = "StatisticalReportsBean.findReportForType";
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name = "id_rif", nullable = false)
    private String idRif;
	
	@Column(name = "date_rif", nullable = true)
    private Date dateRif;
	
	@Column(name = "date_extraction", nullable = false)
    private Date dateExtraction;
	
	@Column(name = "type_extraction", nullable = false)
    private String typeExtraction ;
	
	@Column(name = "type_period", nullable = false)
    private String typePeriod;
	
	@Column(name = "chiave", nullable = true)
    private String chiave;
	
	@Column(name = "valore", nullable = true)
    private String valore;
	
	public StatisticalReportsBean() {
	}

	public StatisticalReportsBean(String idRif, Date dateRif,
			Date dateExtraction, String typeExtraction, String typePeriod,
			String chiave, String valore) {
		this.idRif = idRif;
		this.dateRif = dateRif;
		this.dateExtraction = dateExtraction;
		this.typeExtraction = typeExtraction;
		this.typePeriod = typePeriod;
		this.chiave = chiave;
		this.valore = valore;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdRif() {
		return idRif;
	}

	public void setIdRif(String idRif) {
		this.idRif = idRif;
	}

	public Date getDateRif() {
		return dateRif;
	}

	public void setDateRif(Date dateRif) {
		this.dateRif = dateRif;
	}

	public Date getDateExtraction() {
		return dateExtraction;
	}

	public void setDateExtraction(Date dateExtraction) {
		this.dateExtraction = dateExtraction;
	}

	public String getTypeExtraction() {
		return typeExtraction;
	}

	public void setTypeExtraction(String typeExtraction) {
		this.typeExtraction = typeExtraction;
	}

	public String getTypePeriod() {
		return typePeriod;
	}

	public void setTypePeriod(String typePeriod) {
		this.typePeriod = typePeriod;
	}

	public String getChiave() {
		return chiave;
	}

	public void setChiave(String chiave) {
		this.chiave = chiave;
	}

	public String getValore() {
		return valore;
	}

	public void setValore(String valore) {
		this.valore = valore;
	}
	
	

	
	
}
