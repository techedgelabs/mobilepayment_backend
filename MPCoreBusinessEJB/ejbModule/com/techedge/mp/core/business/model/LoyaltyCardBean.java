package com.techedge.mp.core.business.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.LoyaltyCard;

@Entity
@Table(name = "LOYALTYCARDS")
@NamedQueries({ @NamedQuery(name = "LoyaltyCardBean.findLoyaltyCardById", query = "select c from LoyaltyCardBean c where c.id = :id"),
        @NamedQuery(name = "LoyaltyCardBean.findLoyaltyCardByPanCode", query = "select c from LoyaltyCardBean c where c.panCode = :panCode"),
        @NamedQuery(name = "LoyaltyCardBean.findLoyaltyCardByEanCode", query = "select c from LoyaltyCardBean c where c.eanCode = :eanCode"),
        @NamedQuery(name = "LoyaltyCardBean.findLoyaltyCardByUser", query = "select c from LoyaltyCardBean c where c.userBean = :userBean"),
        @NamedQuery(name = "LoyaltyCardBean.statisticsReportSynthesisActiveLoyaltyCardVirtual", query = "select "
                + "(select count(u1.id) from LoyaltyCardBean l1 Join l1.userBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.virtualizationCompleted = 1 and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and l1.isVirtual = 1 and l1.cardType = 'V'), "
                + "(select count(u2.id) from LoyaltyCardBean l2 Join l2.userBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.virtualizationCompleted = 1 and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate "
                + "and l2.isVirtual = 1 and l2.cardType = 'V'), "
                + "(select count(u3.id) from LoyaltyCardBean l3 Join l3.userBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.virtualizationCompleted = 1 and u3.userStatusRegistrationTimestamp >= :totalStartDate "
                + "and u3.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and l3.isVirtual = 1 and l3.cardType = 'V') from UserBean u"),
        @NamedQuery(name = "LoyaltyCardBean.statisticsReportSynthesisAllActiveLoyaltyCardVirtual", query = "select "
                + "count(u3.id) from LoyaltyCardBean l3 Join l3.userBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.virtualizationCompleted = 1 and u3.userStatusRegistrationTimestamp >= :totalStartDate "
                + "and u3.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and l3.isVirtual = 1 and l3.cardType = 'V'"),
        @NamedQuery(name = "LoyaltyCardBean.statisticsReportSynthesisActiveLoyaltyCardNoVirtual", query = "select "
                + "(select count(u1.id) from LoyaltyCardBean l1 Join l1.userBean u1 where u1.userType = 1 and u1.userStatus = 2 and u1.userStatusRegistrationCompleted = 1 "
                + "and u1.virtualizationCompleted = 1 and u1.userStatusRegistrationTimestamp >= :dailyStartDate and u1.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and l1.isVirtual = 1 and l1.cardType != 'V'), "
                + "(select count(u2.id) from LoyaltyCardBean l2 Join l2.userBean u2 where u2.userType = 1 and u2.userStatus = 2 and u2.userStatusRegistrationCompleted = 1 "
                + "and u2.virtualizationCompleted = 1 and u2.userStatusRegistrationTimestamp >= :weeklyStartDate and u2.userStatusRegistrationTimestamp < :weeklyEndDate "
                + "and l2.isVirtual = 1 and l2.cardType != 'V'), "
                + "(select count(u3.id) from LoyaltyCardBean l3 Join l3.userBean u3 where u3.userType = 1 and u3.userStatus = 2 and u3.userStatusRegistrationCompleted = 1 "
                + "and u3.virtualizationCompleted = 1 and u3.userStatusRegistrationTimestamp >= :totalStartDate "
                + "and u3.userStatusRegistrationTimestamp < :dailyEndDate "
                + "and l3.isVirtual = 1 and l3.cardType != 'V') from UserBean u")})
public class LoyaltyCardBean {

    public static final String FIND_BY_ID       = "LoyaltyCardBean.findLoyaltyCardById";
    public static final String FIND_BY_PAN_CODE = "LoyaltyCardBean.findLoyaltyCardByPanCode";
    public static final String FIND_BY_EAN_CODE = "LoyaltyCardBean.findLoyaltyCardByEanCode";
    public static final String FIND_BY_USER     = "LoyaltyCardBean.findLoyaltyCardByUser";
    public static final String FIND_VIRTUAL     = "LoyaltyCardBean.statisticsReportSynthesisActiveLoyaltyCardVirtual";
    public static final String FIND_ALL_VIRTUAL = "LoyaltyCardBean.statisticsReportSynthesisAllActiveLoyaltyCardVirtual";
    public static final String FIND_NO_VIRTUAL  = "LoyaltyCardBean.statisticsReportSynthesisActiveLoyaltyCardNoVirtual";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "pan_code", nullable = true)
    private String             panCode;

    @Column(name = "ean_code", nullable = true)
    private String             eanCode;

    @Column(name = "card_type", nullable = true)
    private String             cardType;

    @Column(name = "isvirtual", nullable = true)
    private Boolean            isVirtual;

    @Column(name = "status", nullable = false, length = 1)
    private String             status;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "user_id")
    private UserBean           userBean;

    public LoyaltyCardBean() {}

    public LoyaltyCardBean(LoyaltyCard loyaltyCard) {

        this.id = loyaltyCard.getId();
        this.panCode = loyaltyCard.getPanCode();
        this.eanCode = loyaltyCard.getEanCode();
        this.cardType = loyaltyCard.getType();
        this.status = loyaltyCard.getStatus();
        this.isVirtual = loyaltyCard.getIsVirtual();
        //this.userBean = new UserBean(voucher.getUser());
    }

    public LoyaltyCard toLoyaltyCard() {

        LoyaltyCard loyaltyCard = new LoyaltyCard();

        loyaltyCard.setId(this.id);
        loyaltyCard.setPanCode(this.panCode);
        loyaltyCard.setEanCode(this.eanCode);
        loyaltyCard.setType(this.cardType);
        loyaltyCard.setIsVirtual(this.isVirtual);

        return loyaltyCard;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPanCode() {
        return panCode;
    }

    public void setPanCode(String panCode) {
        this.panCode = panCode;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Boolean getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(Boolean isVirtual) {
        this.isVirtual = isVirtual;
    }

}
