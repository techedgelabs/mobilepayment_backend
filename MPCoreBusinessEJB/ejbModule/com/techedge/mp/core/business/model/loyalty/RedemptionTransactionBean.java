package com.techedge.mp.core.business.model.loyalty;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.core.business.model.VoucherBean;

@Entity
@Table(name = "REDEMPTION_TRANSACTIONS")
@NamedQueries({ @NamedQuery(name = "RedemptionTransactionBean.findToReconcilie", query = "select r from RedemptionTransactionBean r where r.toReconcilie = 1") })
public class RedemptionTransactionBean implements UserReconciliationInterfaceBean {

    public static final String FIND_TO_RECONCILIE = "RedemptionTransactionBean.findToReconcilie";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long        id;

    @Column(name = "operation_id")
    private String      operationID;

    @Column(name = "cs_transaction_id", nullable = true)
    private String      csTransactionID;

    @Column(name = "redemption_code")
    private Integer      redemptionCode;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "user_id")
    private UserBean           userBean;

    @OneToOne()
    @JoinColumn(name = "voucher_id")
    private VoucherBean voucherBean;

    @Column(name = "credits", nullable = true)
    private Integer     credits;

    @Column(name = "balance", nullable = true)
    private Double     balance;

    @Column(name = "balance_amount", nullable = true)
    private Double      balanceAmount;

    @Column(name = "request_timestamp", nullable = true)
    private Date        requestTimestamp;

    @Column(name = "marketing_msg", nullable = true)
    private String      marketingMsg;

    @Column(name = "warning_msg", nullable = true)
    private String      warningMsg;

    @Column(name = "message_code", nullable = true)
    private String      messageCode;

    @Column(name = "status_code", nullable = true)
    private String      statusCode;

    @Column(name = "to_reconcilie")
    private Boolean              toReconcilie;

    @Column(name = "reconciliation_attempts_left")
    private Integer              reconciliationAttemptsLeft;

    
    public RedemptionTransactionBean() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public Integer getRedemptionCode() {
        return redemptionCode;
    }

    public void setRedemptionCode(Integer redemptionCode) {
        this.redemptionCode = redemptionCode;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public VoucherBean getVoucherBean() {
        return voucherBean;
    }

    public void setVoucherBean(VoucherBean voucherBean) {
        this.voucherBean = voucherBean;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Boolean getToReconcilie() {
        return toReconcilie;
    }

    public void setToReconcilie(Boolean toReconcilie) {
        this.toReconcilie = toReconcilie;
    }

    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    @Override
    public String getFinalStatus() {
        return getStatusCode();
    }

    @Override
    public void setFinalStatus(String status) {
        setStatusCode(status);
    }

    @Override
    public String getOperationName() {
        return "REDEMPTION";
    }

    @Override
    public Date getOperationTimestamp() {
        return getRequestTimestamp();
    }

    @Override
    public String getFinalStatusMessage() {
        return messageCode;
    }
    
    @Override
    public String getServerEndpoint() {
        return "Backend Quenit";
    }
}
