package com.techedge.mp.core.business.model.voucher;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "VOUCHER_PROMOTIONAL_ES_DETAIL")
public class VoucherPromotionalEsDetailBean {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "operation_id", nullable = false)
	private String operationID;

	@Column(name = "cs_transaction_id")
	private String csTransactionID;

	@Column(name = "request_timestamp")
	private Date requestTimestamp;

	@Column(name = "message_code")
	private String messageCode;

	@Column(name = "status_code")
	private String statusCode;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "voucher_promotional_es_id", nullable = false)
	private VoucherPromotionalEsBean voucherPromotionalEsBean;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOperationID() {
		return operationID;
	}

	public void setOperationID(String operationID) {
		this.operationID = operationID;
	}

	public Date getRequestTimestamp() {
		return requestTimestamp;
	}

	public void setRequestTimestamp(Date requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	public String getCsTransactionID() {
		return csTransactionID;
	}

	public void setCsTransactionID(String csTransactionID) {
		this.csTransactionID = csTransactionID;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public VoucherPromotionalEsBean getVoucherPromotionalEsBean() {
		return voucherPromotionalEsBean;
	}

	public void setVoucherPromotionalEsBean(
			VoucherPromotionalEsBean voucherPromotionalEsBean) {
		this.voucherPromotionalEsBean = voucherPromotionalEsBean;
	}

}
