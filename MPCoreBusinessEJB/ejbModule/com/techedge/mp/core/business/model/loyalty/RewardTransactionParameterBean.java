package com.techedge.mp.core.business.model.loyalty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "REWARD_TRANSACTION_PARAMETER")
@NamedQueries({ @NamedQuery(name = "RewardTransactionParameterBean.findByRewardTransaction", query = "select p from RewardTransactionParameterBean p "
        + "where p.rewardTransactionBean = :rewardTransactionBean") })
public class RewardTransactionParameterBean {

    public static final String    FIND_BY_LOYALTY_TRANSACTION = "RewardTransactionParameterBean.findByRewardTransaction";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                  id;

    @Column(name = "parameter_id", nullable = false)
    private String                parameterID;

    @Column(name = "parameter_value", nullable = false)
    private String                parameterValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reward_transaction_id", nullable = false)
    private RewardTransactionBean rewardTransactionBean;

    public RewardTransactionParameterBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getParameterID() {
        return parameterID;
    }

    public void setParameterID(String parameterID) {
        this.parameterID = parameterID;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public RewardTransactionBean getRewardTransactionBean() {
        return rewardTransactionBean;
    }

    public void setRewardTransactionBean(RewardTransactionBean rewardTransactionBean) {
        this.rewardTransactionBean = rewardTransactionBean;
    }

}
