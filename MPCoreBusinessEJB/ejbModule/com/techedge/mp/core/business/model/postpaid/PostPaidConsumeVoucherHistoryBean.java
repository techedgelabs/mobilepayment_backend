package com.techedge.mp.core.business.model.postpaid;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucherDetail;

@Entity
@Table(name = "POSTPAIDCONSUMEVOUCHERHISTORY")
public class PostPaidConsumeVoucherHistoryBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                                         id;

    @Column(name = "cs_transaction_id", nullable = true)
    private String                                       csTransactionID;

    @Column(name = "operation_id", nullable = true)
    private String                                       operationID;

    @Column(name = "operation_id_reversed", nullable = true)
    private String                                operationIDReversed;
    
    @Column(name = "operation_type", nullable = true)
    private String                                       operationType;

    @Column(name = "request_timestamp", nullable = true)
    private Long                                         requestTimestamp;

    @Column(name = "marketing_msg", nullable = true, length = 500)
    private String                                       marketingMsg;

    @Column(name = "warning_msg", nullable = true)
    private String                                warningMsg;

    @Column(name = "message_code", nullable = true)
    private String                                       messageCode;

    @Column(name = "status_code", nullable = true)
    private String                                       statusCode;

    @Column(name = "total_consumed", nullable = true)
    private Double                                       totalConsumed;
    
    @Column(name = "amount", nullable = true)
    private Double                                amount;

    @Column(name = "reconciled", nullable = true)
    private Boolean                         reconciled = false;
    
    @Column(name = "preauth_operation_id", nullable = true)
    private String                                preAuthOperationID;    

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "postPaidTransactionHistoryBean", nullable = false)
    private PostPaidTransactionHistoryBean               postPaidTransactionHistoryBean;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postPaidConsumeVoucherHistoryBean")
    private Set<PostPaidConsumeVoucherDetailHistoryBean> postPaidConsumeVoucherDetailHistoryBean = new HashSet<PostPaidConsumeVoucherDetailHistoryBean>(0);

    public PostPaidConsumeVoucherHistoryBean() {}

    public PostPaidConsumeVoucherHistoryBean(PostPaidConsumeVoucherBean postPaidConsumeVoucherBean) {

        this.csTransactionID = postPaidConsumeVoucherBean.getCsTransactionID();
        this.operationID = postPaidConsumeVoucherBean.getOperationID();
        this.operationIDReversed = postPaidConsumeVoucherBean.getOperationIDReversed();
        this.operationType = postPaidConsumeVoucherBean.getOperationType();
        this.requestTimestamp = postPaidConsumeVoucherBean.getRequestTimestamp();
        this.marketingMsg = postPaidConsumeVoucherBean.getMarketingMsg();
        this.warningMsg = postPaidConsumeVoucherBean.getWarningMsg();
        this.messageCode = postPaidConsumeVoucherBean.getMessageCode();
        this.statusCode = postPaidConsumeVoucherBean.getStatusCode();
        this.totalConsumed = postPaidConsumeVoucherBean.getTotalConsumed();
        this.amount = postPaidConsumeVoucherBean.getAmount();
        this.reconciled = postPaidConsumeVoucherBean.getReconciled();
        this.preAuthOperationID = postPaidConsumeVoucherBean.getPreAuthOperationID();

        for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {

            PostPaidConsumeVoucherDetailHistoryBean postPaidConsumeVoucherDetailHistoryBean = new PostPaidConsumeVoucherDetailHistoryBean(postPaidConsumeVoucherDetailBean);

            this.postPaidConsumeVoucherDetailHistoryBean.add(postPaidConsumeVoucherDetailHistoryBean);
        }
    }

    public PostPaidConsumeVoucher toPostPaidConsumeVoucher() {

        PostPaidConsumeVoucher postPaidConsumeVoucher = new PostPaidConsumeVoucher();
        postPaidConsumeVoucher.setCsTransactionID(this.csTransactionID);
        postPaidConsumeVoucher.setOperationID(this.operationID);
        postPaidConsumeVoucher.setOperationIDReversed(this.operationIDReversed);
        postPaidConsumeVoucher.setOperationType(this.operationType);
        postPaidConsumeVoucher.setRequestTimestamp(this.requestTimestamp);
        postPaidConsumeVoucher.setMarketingMsg(this.marketingMsg);
        postPaidConsumeVoucher.setWarningMsg(this.warningMsg);
        postPaidConsumeVoucher.setMessageCode(this.messageCode);
        postPaidConsumeVoucher.setStatusCode(this.statusCode);
        postPaidConsumeVoucher.setTotalConsumed(this.totalConsumed);
        postPaidConsumeVoucher.setAmount(this.amount);
        postPaidConsumeVoucher.setReconciled(this.reconciled);
        postPaidConsumeVoucher.setPreAuthOperationID(this.preAuthOperationID);

        if (!this.postPaidConsumeVoucherDetailHistoryBean.isEmpty()) {

            for (PostPaidConsumeVoucherDetailHistoryBean postPaidConsumeVoucherDetailHistoryBean : this.postPaidConsumeVoucherDetailHistoryBean) {
                PostPaidConsumeVoucherDetail PostPaidConsumeVoucherDetail = postPaidConsumeVoucherDetailHistoryBean.toPostPaidConsumeVoucherDetail();
                postPaidConsumeVoucher.getPostPaidConsumeVoucherDetail().add(PostPaidConsumeVoucherDetail);
            }
        }

        return postPaidConsumeVoucher;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public void setOperationIDReversed(String operationIDReversed) {
        this.operationIDReversed = operationIDReversed;
    }

    public String getOperationIDReversed() {
        return operationIDReversed;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotalConsumed() {
        return totalConsumed;
    }

    public void setTotalConsumed(Double totalConsumed) {
        this.totalConsumed = totalConsumed;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Boolean getReconciled() {
        return reconciled;
    }

    public void setReconciled(Boolean reconciled) {
        this.reconciled = reconciled;
    }

    public String getPreAuthOperationID() {
        return preAuthOperationID;
    }

    public void setPreAuthOperationID(String preAuthOperationID) {
        this.preAuthOperationID = preAuthOperationID;
    }

    public PostPaidTransactionHistoryBean getPostPaidTransactionHistoryBean() {
        return postPaidTransactionHistoryBean;
    }

    public void setPostPaidTransactionHistoryBean(PostPaidTransactionHistoryBean postPaidTransactionHistoryBean) {
        this.postPaidTransactionHistoryBean = postPaidTransactionHistoryBean;
    }

    public Set<PostPaidConsumeVoucherDetailHistoryBean> getPostPaidConsumeVoucherDetailHistoryBean() {
        return postPaidConsumeVoucherDetailHistoryBean;
    }

    public void setPostPaidConsumeVoucherDetailHistoryBean(Set<PostPaidConsumeVoucherDetailHistoryBean> postPaidConsumeVoucherDetailHistoryBean) {
        this.postPaidConsumeVoucherDetailHistoryBean = postPaidConsumeVoucherDetailHistoryBean;
    }

}
