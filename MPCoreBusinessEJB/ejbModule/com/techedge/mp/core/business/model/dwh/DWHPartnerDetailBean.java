package com.techedge.mp.core.business.model.dwh;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PartnerDetailInfoData;

@Entity
@Table(name = "DWH_PARTNER_DETAIL")
@NamedQueries({ 
    @NamedQuery(name = "DWHPartnerDetailBean.findById", query = "select pd from DWHPartnerDetailBean pd where pd.id = :id"),
    @NamedQuery(name = "DWHPartnerDetailBean.findAll", query = "select pd from DWHPartnerDetailBean pd")
})
public class DWHPartnerDetailBean{
    
    public static final String FIND_By_ID = "DWHPartnerDetailBean.findById";
    public static final String FIND_ALL = "DWHPartnerDetailBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                id;

    @Column(name="creation_timestamp")
    private Date creationTimestamp;
    
    @Column(name = "partner_id")
    private Integer              partnerId;

    @Column(name = "category_id")
    private Integer              categoryId;
    
    @Column(name = "category")
    private String             category;

    @Column(name = "name")
    private String              name;
    
    @Lob @Basic(fetch=FetchType.LAZY)
    @Column(name = "description")
    private String              description;

    @Lob @Basic(fetch=FetchType.LAZY)
    @Column(name = "title")
    private String              title;
    
    @Lob @Basic(fetch=FetchType.LAZY)
    @Column(name = "logic")
    private String              logic;
    
    @Lob @Basic(fetch=FetchType.LAZY)
    @Column(name = "logo_url")
    private String              logoUrl;
    
    @Lob @Basic(fetch=FetchType.LAZY)
    @Column(name = "logo_small_url")
    private String              logoSmallURL;
    
    
    public DWHPartnerDetailBean(){}
    
    public DWHPartnerDetailBean(PartnerDetailInfoData partnerDetailInfoData){
        this.categoryId=partnerDetailInfoData.getCategoryId();
        this.category=partnerDetailInfoData.getCategory();
        this.description=partnerDetailInfoData.getDescription();
        this.logic=partnerDetailInfoData.getLogic();
        this.logoSmallURL=partnerDetailInfoData.getLogoSmallUrl();
        this.logoUrl = partnerDetailInfoData.getLogoUrl();
        this.name = partnerDetailInfoData.getName();
        this.partnerId=partnerDetailInfoData.getPartnerId();
        this.title=partnerDetailInfoData.getTitle();
    }
    
    public PartnerDetailInfoData toPartnerDetailInfoData(){
        PartnerDetailInfoData partnerDetailInfoData = new PartnerDetailInfoData();
        partnerDetailInfoData.setCategoryId(this.categoryId);
        partnerDetailInfoData.setCategory(this.category);
        partnerDetailInfoData.setDescription(this.description);
        partnerDetailInfoData.setLogic(this.logic);
        partnerDetailInfoData.setLogoSmallUrl(this.logoSmallURL);
        partnerDetailInfoData.setLogoUrl(this.logoUrl);
        partnerDetailInfoData.setName(this.name);
        partnerDetailInfoData.setPartnerId(this.partnerId);
        partnerDetailInfoData.setTitle(this.title);
        
        return partnerDetailInfoData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogic() {
        return logic;
    }

    public void setLogic(String logic) {
        this.logic = logic;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getLogoSmallURL() {
        return logoSmallURL;
    }

    public void setLogoSmallURL(String logoSmallURL) {
        this.logoSmallURL = logoSmallURL;
    }
    
    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }
    
}
