package com.techedge.mp.core.business.model.dwh;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.user.DWHOperationType;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;

@Entity
@Table(name = "DWH_EVENT")
@NamedQueries({ 
    @NamedQuery(name = "DWHEventBean.findById", query = "select upt from DWHEventBean upt where upt.id = :id"),
    @NamedQuery(name = "DWHEventBean.findToReconcilie", query = "select upt from DWHEventBean upt where upt.toReconcilie = 1"),
    @NamedQuery(name = "DWHEventBean.findByUserAndOperation", query = "select upt from DWHEventBean upt where upt.userBean = :userBean and upt.operation = :operation"),
    @NamedQuery(name = "DWHEventBean.findByUser", query = "select upt from DWHEventBean upt where upt.userBean = :userBean")
})
public class DWHEventBean implements UserReconciliationInterfaceBean {
    
    public static final String FIND_By_ID = "DWHEventBean.findById";
    public static final String FIND_TO_RECONCILIE = "DWHEventBean.findToReconcilie";
    public static final String FIND_BY_USER_AND_OPERATION = "DWHEventBean.findByUserAndOperation";
    public static final String FIND_BY_USER = "DWHEventBean.findByUser";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @OneToOne()
    @JoinColumn(name = "user_id", nullable = false)
    private UserBean           userBean;

    @Column(name = "operation", nullable = false)
    private String                         operation;

    @Column(name = "event_status", nullable = false)
    private String                         eventStatus;
    
    @Column(name = "to_reconcilie")
    private Boolean            toReconcilie;

    @Column(name = "reconciliation_attempts_left")
    private Integer            reconciliationAttemptsLeft;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "dwhEventBean")
    private Set<DWHEventOperationBean> eventOperationList = new HashSet<DWHEventOperationBean>(0);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public String getEventStatus() {
        return eventStatus;
    }
    
    public void setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }
    
    public Boolean getToReconcilie() {
        return toReconcilie;
    }

    public void setToReconcilie(Boolean toReconcilie) {
        this.toReconcilie = toReconcilie;
    }

    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }
    
    public DWHOperationType getOperation() {
        return DWHOperationType.valueOf(operation);
    }
    
    public String getOperationName() {
        return DWHOperationType.valueOf(operation).name(); 
    }
    
    public void setOperation(DWHOperationType operation) {
        this.operation = operation.name();
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public Set<DWHEventOperationBean> getEventOperationListList() {
        return eventOperationList;
    }

    public void setEventOperationList(Set<DWHEventOperationBean> eventOperationList) {
        this.eventOperationList = eventOperationList;
    }
    
    public DWHEventOperationBean getLastEventOperationBean() {
        DWHEventOperationBean dWHEventOperationBean = null;
        
        for (DWHEventOperationBean tmpEventOperationBean : getEventOperationListList()) {
            if (dWHEventOperationBean != null) {
                if (tmpEventOperationBean.getRequestTimestamp().after(dWHEventOperationBean.getRequestTimestamp())) {
                    dWHEventOperationBean = tmpEventOperationBean;
                }
            }
            else {
                dWHEventOperationBean = tmpEventOperationBean;
            }
        }
        
        return dWHEventOperationBean;
    }

    @Override
    public String getFinalStatus() {
        return getEventStatus();
    }

    @Override
    public void setFinalStatus(String status) {
        setEventStatus(status);
    }
    
    @Override
    public Date getOperationTimestamp() {
        if (getLastEventOperationBean() != null)
            return getLastEventOperationBean().getRequestTimestamp();
        
        return null;
    }
    
    @Override
    public String getFinalStatusMessage() {
        String message = null;
        DWHEventOperationBean dwhEventOperationBean = getLastEventOperationBean();
        
        if (dwhEventOperationBean != null) {
            message = dwhEventOperationBean.getMessageCode();
        }
        
        return message;
    }
    
    @Override
    public String getServerEndpoint() {
        return "DWH Serijakala";
    }
}
