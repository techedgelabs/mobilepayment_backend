package com.techedge.mp.core.business.model.postpaid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShoppingCartDetail;

@Entity
@Table(name = "POSTPAIDCART_HISTORY")
//@NamedQueries({
//	@NamedQuery(name="TransactionBean.findTransactionByID", query="select t from TransactionBean t where t.transactionID = :transactionID"),
//	@NamedQuery(name="TransactionBean.findTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean"),
//	@NamedQuery(name="TransactionBean.findTransactionByUserAndDate", query="select t from TransactionBean t where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),
//	@NamedQuery(name="TransactionBean.findTransactionSuccessfulByUserAndDate", query="select t from TransactionBean t where t.userBean = :userBean and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) and t.finalStatusType = 'SUCCESSFUL' order by t.creationTimestamp desc"),
//	//@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and (( t.finalStatusType is null ) or ( t.finalStatusType <> 'FAILED' and t.finalStatusType <> 'SUCCESSFUL' and t.finalStatusType <> 'MISSING_NOTIFICATION' ))")
//	@NamedQuery(name="TransactionBean.findActiveTransactionByUser", query="select t from TransactionBean t where t.userBean = :userBean and t.confirmed = false"),
//	@NamedQuery(name="TransactionBean.findActiveTransaction", query="select t from TransactionBean t where t.confirmed = false"),
//	@NamedQuery(name="TransactionBean.findActiveTransactionNew", query="select t from TransactionBean t where t.finalStatusType = null"),
//	@NamedQuery(name="TransactionBean.findTransactionByFinalStatusType", query="select t from TransactionBean t where t.finalStatusType = :finalStatusType"),
//	@NamedQuery(name="TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus", query="select t from TransactionBean t where t.finalStatusType = :finalStatusType and t.GFGNotification = :gfgNotification"),
//})
public class PostPaidCartHistoryBean {

    //	public static final String FIND_BY_ID = "TransactionBean.findTransactionByID";
    //	public static final String FIND_BY_USER = "TransactionBean.findTransactionByUser";
    //	public static final String FIND_ACTIVE_BY_USER = "TransactionBean.findActiveTransactionByUser";
    //	public static final String FIND_ACTIVE = "TransactionBean.findActiveTransaction";
    //	public static final String FIND_ACTIVE_NEW = "TransactionBean.findActiveTransactionNew";
    //	public static final String FIND_BY_FINAL_STATUS_TYPE = "TransactionBean.findTransactionByFinalStatusType";
    //	public static final String FIND_BY_FINAL_STATUS_TYPE_AND_NOTIFICATION = "TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus";
    //	public static final String FIND_BY_USER_AND_DATE = "TransactionBean.findTransactionByUserAndDate";
    //	public static final String FIND_SUCCESSFUL_BY_USER_AND_DATE = "TransactionBean.findTransactionSuccessfulByUserAndDate";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                           id;

    @Column(name = "productId", nullable = true)
    private String                         productId;

    @Column(name = "productDescription", nullable = true)
    private String                         productDescription;

    @Column(name = "amount", nullable = true)
    private Double                         amount;

    @Column(name = "quantity", nullable = true)
    private Integer                        quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactionID", nullable = false)
    private PostPaidTransactionHistoryBean postPaidTransactionHistoryBean;

    public PostPaidCartHistoryBean() {}

    public PostPaidCartHistoryBean(PostPaidCartBean postPaidCartBean) {
        this.quantity = postPaidCartBean.getQuantity();
        this.amount = postPaidCartBean.getAmount();
        this.productDescription = postPaidCartBean.getProductDescription();
        this.productId = postPaidCartBean.getProductId();
    }

    public PostPaidCart toPostPaidCart() {

        PostPaidCart postPaidCart = new PostPaidCart();
        postPaidCart.setQuantity(this.quantity);
        postPaidCart.setAmount(this.amount);
        postPaidCart.setProductDescription(this.productDescription);
        postPaidCart.setProductId(this.productId);
        return postPaidCart;
    }

    public PostPaidCartHistoryBean(ShoppingCartDetail shoppingCart, PostPaidTransactionHistoryBean postPaidTransactionHistoryBean) {

        this.quantity = shoppingCart.getQuantity();
        this.amount = shoppingCart.getAmount();
        this.productDescription = shoppingCart.getProductDescription();
        this.productId = shoppingCart.getProductID();
        this.postPaidTransactionHistoryBean = postPaidTransactionHistoryBean;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public PostPaidTransactionHistoryBean getPostPaidTransactionHistoryBean() {
        return postPaidTransactionHistoryBean;
    }

    public void setPostPaidTransactionHistoryBean(PostPaidTransactionHistoryBean postPaidTransactionHistoryBean) {
        this.postPaidTransactionHistoryBean = postPaidTransactionHistoryBean;
    }

}
