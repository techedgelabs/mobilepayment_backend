package com.techedge.mp.core.business.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.LastLoginData;

@Entity
@Table(name = "LASTLOGINDATA")
/*@NamedQueries({ 
    @NamedQuery(name = "LastLoginDataBean.findByDeviceToken", query = "select l from LastLoginDataBean l where l.deviceToken = :deviceToken"),
    @NamedQuery(name = "LoyaltyCardBean.findLoyaltyCardByPanCode", query = "select c from LoyaltyCardBean c where c.panCode = :panCode"),
    @NamedQuery(name = "LoyaltyCardBean.findLoyaltyCardByEanCode", query = "select c from LoyaltyCardBean c where c.eanCode = :eanCode"),
    @NamedQuery(name = "LoyaltyCardBean.findLoyaltyCardByUser", query = "select c from LoyaltyCardBean c where c.userBean = :userBean")
    })
*/
public class LastLoginDataBean {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long      id;

    @Column(name = "time", nullable = true)
    private Timestamp time;

    @Column(name = "device_id", nullable = true)
    private String    deviceId;

    @Column(name = "device_name", nullable = true)
    private String    deviceName;

    @Column(name = "device_token", nullable = true)
    private String    deviceToken;

    @Column(name = "device_family", nullable = true)
    private String    deviceFamily;

    @Column(name = "device_endpoint", nullable = true)
    private String    deviceEndpoint;

    @Column(name = "latitude", nullable = true)
    private Double    latitude;

    @Column(name = "longitude", nullable = true)
    private Double    longitude;

    public LastLoginDataBean() {}

    public LastLoginDataBean(LastLoginData lastLoginData) {

        this.id = lastLoginData.getId();
        this.time = lastLoginData.getTime();
        this.deviceId = lastLoginData.getDeviceId();
        this.deviceName = lastLoginData.getDeviceName();
        this.deviceToken = lastLoginData.getDeviceToken();
        this.deviceFamily = lastLoginData.getDeviceFamily();
        this.deviceEndpoint = lastLoginData.getDeviceEndpoint();
        this.latitude = lastLoginData.getLatitude();
        this.longitude = lastLoginData.getLongitude();
    }

    public LastLoginData toLastLoginData() {

        LastLoginData lastLoginData = new LastLoginData();

        lastLoginData.setId(this.id);
        lastLoginData.setTime(this.time);
        lastLoginData.setDeviceId(this.deviceId);
        lastLoginData.setDeviceName(this.deviceName);
        lastLoginData.setDeviceToken(this.deviceToken);
        lastLoginData.setDeviceFamily(this.deviceFamily);
        lastLoginData.setDeviceEndpoint(this.deviceEndpoint);
        lastLoginData.setLatitude(this.latitude);
        lastLoginData.setLongitude(this.longitude);

        return lastLoginData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceFamily() {
        return deviceFamily;
    }

    public void setDeviceFamily(String deviceFamily) {
        this.deviceFamily = deviceFamily;
    }

    public String getDeviceEndpoint() {
        return deviceEndpoint;
    }

    public void setDeviceEndpoint(String deviceEndpoint) {
        this.deviceEndpoint = deviceEndpoint;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
