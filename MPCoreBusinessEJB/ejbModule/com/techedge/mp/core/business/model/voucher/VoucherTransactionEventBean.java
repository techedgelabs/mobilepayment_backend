package com.techedge.mp.core.business.model.voucher;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.voucher.VoucherTransactionEvent;

@Entity
@Table(name = "VOUCHER_TRANSACTION_EVENT")
@NamedQueries({ 
    @NamedQuery(name = "VoucherTransactionEventBean.findByTransaction", 
            query = "select e from VoucherTransactionEventBean e where e.voucherTransactionBean = :voucherTransactionBean")

})
public class VoucherTransactionEventBean {

    public static final String FIND_BY_TRANSACTION   = "VoucherTransactionEventBean.findByTransaction";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "voucher_transaction_id", nullable = false)
    private VoucherTransactionBean    voucherTransactionBean;

    @Column(name = "sequence_id", nullable = false)
    private Integer            sequenceID;

    @Column(name = "event_type", nullable = false)
    private String             eventType;

    @Column(name = "event_amount", nullable = true)
    private Double             eventAmount;

    @Column(name = "transaction_result", nullable = false)
    private String             transactionResult;

    @Column(name = "error_code", nullable = true)
    private String             errorCode;

    @Column(name = "error_description", nullable = true)
    private String             errorDescription;

    public VoucherTransactionEventBean() {}

    public VoucherTransactionEventBean(VoucherTransactionEvent voucherTransactionEvent) {

        this.id = 0;
        this.sequenceID = voucherTransactionEvent.getSequenceID();
        this.eventType = voucherTransactionEvent.getEventType();
        this.eventAmount = voucherTransactionEvent.getEventAmount();
        this.transactionResult = voucherTransactionEvent.getTransactionResult();
        this.errorCode = voucherTransactionEvent.getErrorCode();
        this.errorDescription = voucherTransactionEvent.getErrorDescription();
    }

    public VoucherTransactionEvent toVoucherTransactionEvent() {

        VoucherTransactionEvent voucherTransactionEvent = new VoucherTransactionEvent();
        voucherTransactionEvent.setId(this.id);
        voucherTransactionEvent.setSequenceID(this.sequenceID);
        voucherTransactionEvent.setEventType(this.eventType);
        voucherTransactionEvent.setEventAmount(this.eventAmount);
        voucherTransactionEvent.setTransactionResult(this.transactionResult);
        voucherTransactionEvent.setErrorCode(this.errorCode);
        voucherTransactionEvent.setErrorDescription(this.errorDescription);
        return voucherTransactionEvent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public VoucherTransactionBean getVoucherTransactionBean() {
        return voucherTransactionBean;
    }

    public void setVoucherTransactionBean(VoucherTransactionBean voucherTransactionBean) {
        this.voucherTransactionBean = voucherTransactionBean;
    }

    public Integer getSequenceID() {
        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {
        this.sequenceID = sequenceID;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Double getEventAmount() {
        return eventAmount;
    }

    public void setEventAmount(Double eventAmount) {
        this.eventAmount = eventAmount;
    }

    public String getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {
        this.transactionResult = transactionResult;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

}
