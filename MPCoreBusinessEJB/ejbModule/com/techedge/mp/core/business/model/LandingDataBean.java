package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.LandingData;
    
@Entity
@Table(name = "LANDINGDATA")
@NamedQueries({
    @NamedQuery(name = "LandingDataBean.findBysectionID", query = "select l from LandingDataBean l where l.sectionID = :sectionID"),
    @NamedQuery(name = "LandingDataBean.findAll", query = "select l from LandingDataBean l")
})
public class LandingDataBean {

    public static final String FIND_BY_SECTIONID = "LandingDataBean.findBysectionID";
    public static final String FIND_ALL          = "LandingDataBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "title", nullable = true)
    private String             title;

    @Column(name = "url", nullable = true)
    private String             url;

    @Column(name = "showAlways", nullable = true)
    private Boolean            showAlways;

    @Column(name = "status", nullable = true)
    private String             status;

    @Column(name = "sectionID", nullable = true)
    private String             sectionID;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usercategory_id", nullable = true)
    private UserCategoryBean   userCategoryBean;

    public LandingDataBean() {}

    public LandingDataBean(LandingData landingData) {

        this.title = landingData.getTitle();
        this.url = landingData.getUrl();
        this.showAlways = landingData.getShowAlways();
        this.status = landingData.getStatus();
        this.sectionID = landingData.getSectionID();
    }

    public LandingData toLandingData() {
        LandingData landingData = new LandingData();
        landingData.setId(this.id);
        landingData.setSectionID(this.sectionID);
        landingData.setShowAlways(this.showAlways);
        landingData.setStatus(this.status);
        landingData.setTitle(this.title);
        landingData.setUrl(this.url);
        return landingData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getShowAlways() {
        return showAlways;
    }

    public void setShowAlways(Boolean showAlways) {
        this.showAlways = showAlways;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSectionID() {
        return sectionID;
    }

    public void setSectionID(String sectionID) {
        this.sectionID = sectionID;
    }

    public UserCategoryBean getUserCategoryBean() {
        return userCategoryBean;
    }

    public void setUserCategoryBean(UserCategoryBean userCategory) {
        this.userCategoryBean = userCategory;
    }

}
