package com.techedge.mp.core.business.model;

import java.util.Date;

import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.TransactionInterfaceType;

public interface TransactionInterfaceBean {


    public long getId();
    
    public String getTransactionID();

    public UserBean getUserBean();

    public StationBean getStationBean();

    public Boolean getUseVoucher();

    public Date getCreationTimestamp();

    public Date getEndRefuelTimestamp();

    public Double getInitialAmount();
    
    public Double getAmount();

    public void setAmount(Double amount);

    public String getPumpID();

    public Integer getPumpNumber();

    public String getProductID();

    public String getProductDescription();

    public String getCurrency();

    public String getShopLogin();

    public String getAcquirerID();

    public String getBankTansactionID();
    
    public String getAuthorizationCode();

    public String getToken();

    public String getPaymentType();

    public String getTransactionStatus();
    
    public void setTransactionStatus(String transactionStatus);

    public Boolean getGFGNotification();

    public Long getPaymentMethodId();

    public String getPaymentMethodType();

    public String getRefuelMode();

    public Integer getReconciliationAttemptsLeft();
    
    public void setReconciliationAttemptsLeft(Integer attemptsLeft);
    
    public String getLastStatusEvent();
    
    public String getLastStatusEventCode();
    
    public String getLastStatusEventResult();
    
    public TransactionInterfaceType getTransactionType();

    public String getEncodedSecretKey();
    
    public String getGroupAcquirer();
    
    public TransactionCategoryType getTransactionCategory();
    
    public Double getUnitPrice();
    
    public String getServerName();
}
