package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.ProvinceInfo;

@Entity
@Table(name = "PROVINCES")
@NamedQuery(name = "ProvinceInfo.findByName", query = "select p from ProvinceInfoBean p where p.name = :name")
public class ProvinceInfoBean {

    public static final String FIND_BY_NAME = "ProvinceInfo.findByName";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "name", nullable = false)
    private String             name;

    @Column(name = "region", nullable = false)
    private String             region;
    
    @Column(name = "area")
    private String             area;
    
    public ProvinceInfoBean() {}

    public ProvinceInfoBean(ProvinceInfo provinceInfo) {

        this.name = provinceInfo.getName();
        this.region = provinceInfo.getRegion();
        this.area = provinceInfo.getArea();
    }

    public ProvinceInfo toProvinceInfo() {

        ProvinceInfo provinceInfo = new ProvinceInfo();
        provinceInfo.setName(this.name);
        provinceInfo.setRegion(region);
        provinceInfo.setArea(this.area);
        return provinceInfo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getArea() {
        return area;
    }
    
    public void setArea(String area) {
        this.area = area;
    }
}
