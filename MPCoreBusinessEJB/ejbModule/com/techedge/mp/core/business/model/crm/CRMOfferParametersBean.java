package com.techedge.mp.core.business.model.crm;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_OFFER_PARAMETERS")

public class CRMOfferParametersBean {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                  id;

    @Column(name = "parameter_name", nullable = false)
    private String                parameterName;

    @Column(name = "parameter_value")
    private String                parameterValue;
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "offer_id", nullable = false)
    private CRMOfferBean crmOfferBean;

    public CRMOfferParametersBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = (parameterName != null) ? parameterName.trim() : parameterName;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = (parameterValue != null) ? parameterValue.trim() : parameterValue;
    }

    public CRMOfferBean getCRMOfferBean() {
        return crmOfferBean;
    }

    public void setCRMOfferBean(CRMOfferBean crmOfferBean) {
        this.crmOfferBean = crmOfferBean;
    }

}
