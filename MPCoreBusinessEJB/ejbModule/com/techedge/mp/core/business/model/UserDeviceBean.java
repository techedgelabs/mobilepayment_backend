package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.user.UserDevice;

@Entity
@Table(name = "USER_DEVICE")
@NamedQueries({ @NamedQuery(name = "UserDeviceBean.findDeviceByUserID", query = "select p from UserDeviceBean p where p.userBean = :userid"),
        @NamedQuery(name = "UserDeviceBean.findDeviceByID", query = "select p from UserDeviceBean p where p.id = :id"),
        @NamedQuery(name = "UserDeviceBean.findDeviceByDeviceID", query = "select p from UserDeviceBean p where p.deviceId = :deviceID and p.status != 2") })
public class UserDeviceBean {
    public static final String FIND_DEVICE_BY_DEVICEID = "UserDeviceBean.findDeviceByDeviceID";
    public static final String FIND_DEVICE_BY_ID       = "UserDeviceBean.findDeviceByID";
    public static final String FIND_DEVICE_BY_USERID   = "UserDeviceBean.findDeviceByUserID";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "creation_timestamp")
    private Date               creationTimestamp;

    @Column(name = "association_timestamp", nullable = true)
    private Date               associationTimestamp;

    @Column(name = "device_id")
    private String             deviceId;

    @Column(name = "device_name", nullable = true)
    private String             deviceName;

    @Column(name = "device_family", nullable = true)
    private String             deviceFamily;

    @Column(name = "device_token", nullable = true)
    private String             deviceToken;

    @Column(name = "device_endpoint", nullable = true)
    private String             deviceEndpoint;

    @Column(name = "status", nullable = true)
    private Integer            status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = true)
    private UserBean           userBean;

    @Column(name = "verification_code", nullable = true)
    private String             verificationCode;
    
    @Column(name = "lastused_timestamp", nullable = true)
    private Date               lastUsedTimestamp;

    public UserDeviceBean() {}

    public UserDeviceBean(UserDevice userDevice) {
        this.deviceId = userDevice.getDeviceId();
        this.deviceName = userDevice.getDeviceName();
        this.deviceToken = userDevice.getDeviceToken();
        this.deviceFamily = userDevice.getDeviceFamily();
        this.deviceEndpoint = userDevice.getDeviceFamily();
        this.status = userDevice.getStatus();
        this.verificationCode = userDevice.getVerificationCode();
        this.creationTimestamp = userDevice.getCreationTimestamp();
        this.associationTimestamp = userDevice.getAssociationTimestamp();
        this.lastUsedTimestamp = userDevice.getLastUsedTimestamp();
    }

    public UserDevice toUserDevice() {
        UserDevice userDevice = new UserDevice();
        userDevice.setDeviceEndpoint(deviceEndpoint);
        userDevice.setDeviceFamily(deviceFamily);
        userDevice.setDeviceId(deviceId);
        userDevice.setDeviceName(deviceName);
        userDevice.setStatus(status);
        userDevice.setVerificationCode(verificationCode);
        userDevice.setAssociationTimestamp(associationTimestamp);
        userDevice.setCreationTimestamp(creationTimestamp);
        userDevice.setLastUsedTimestamp(lastUsedTimestamp);
        return userDevice;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceFamily() {
        return deviceFamily;
    }

    public void setDeviceFamily(String deviceFamily) {
        this.deviceFamily = deviceFamily;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceEndpoint() {
        return deviceEndpoint;
    }

    public void setDeviceEndpoint(String deviceEndpoint) {
        this.deviceEndpoint = deviceEndpoint;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getAssociationTimestamp() {
        if (associationTimestamp == null) {
            return creationTimestamp;
        }
        return associationTimestamp;
    }

    public void setAssociationTimestamp(Date associationTimestamp) {
        this.associationTimestamp = associationTimestamp;
    }

    public Date getLastUsedTimestamp() {
        if (lastUsedTimestamp == null) {
            return creationTimestamp;
        }
        return lastUsedTimestamp;
    }

    public void setLastUsedTimestamp(Date lastUsedTimestamp) {
        this.lastUsedTimestamp = lastUsedTimestamp;
    }

}
