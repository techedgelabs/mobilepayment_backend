package com.techedge.mp.core.business.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PrePaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionAdditionalData;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.TransactionEvent;
import com.techedge.mp.core.business.interfaces.TransactionInterfaceType;
import com.techedge.mp.core.business.interfaces.TransactionOperation;
import com.techedge.mp.core.business.interfaces.TransactionStatus;
import com.techedge.mp.core.business.model.voucher.PaymentTokenPackageBean;

@Entity
@Table(name = "TRANSACTIONS")
@NamedQueries({
        @NamedQuery(name = "TransactionBean.findTransactionByID", query = "select t from TransactionBean t where t.transactionID = :transactionID"),

        @NamedQuery(name = "TransactionBean.findTransactionByUser", query = "select t from TransactionBean t where t.userBean = :userBean"),

        @NamedQuery(name = "TransactionBean.findTransactionByUserAndDate", query = "select t from TransactionBean t where t.userBean = :userBean "
                + "and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionBean.findTransactionForReportByDate", query = "select t from TransactionBean t where t.finalStatusType != null "
                + "and t.stationBean.stationStatus = 2 and (t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionBean.findTransactionSuccessfulByUserAndDate", query = "select t from TransactionBean t where t.userBean = :userBean "
                + "and ( t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) and t.finalStatusType = 'SUCCESSFUL' order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionBean.findTransactionByDate", query = "select t from TransactionBean t where t.creationTimestamp >= :startDate "
                + "and t.creationTimestamp < :endDate order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionBean.findTransactionByDateAndSrcTransaction", query = "select t from TransactionBean t where t.creationTimestamp >= :startDate "
                + "and t.creationTimestamp < :endDate and t.srcTransactionID != null order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesis", query = "select "
                + "(select count(t1.id) from TransactionBean t1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and t1.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL
                + "' and t1.finalAmount > 0 "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionBean t2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and t2.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL
                + "' and t2.finalAmount > 0 "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionBean t3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportAllSynthesis", query = "select "
                + "count(t3.id) from TransactionBean t3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisBusiness", query = "select "
                + "(select count(t1.id) from TransactionBean t1 where t1.transactionCategory = 'BUSINESS' and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t1.finalAmount > 0 "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionBean t2 where t2.transactionCategory = 'BUSINESS' and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t2.finalAmount > 0 "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionBean t3 where t3.transactionCategory = 'BUSINESS' and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllBusiness", query = "select "
                + "count(t3.id) from TransactionBean t3 where t3.transactionCategory = 'BUSINESS' and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisArea", query = "select "
                + "(select count(t1.id) from TransactionBean t1, ProvinceInfoBean p1 where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and "
                + "t1.stationBean.province = p1.name and p1.area = :area and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t1.finalAmount > 0 "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionBean t2, ProvinceInfoBean p2 where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and "
                + "t2.stationBean.province = p2.name and p2.area = :area and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t2.finalAmount > 0 "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionBean t3, ProvinceInfoBean p3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllArea", query = "select "
                + "count(t3.id) from TransactionBean t3, ProvinceInfoBean p3 where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAreaBusiness", query = "select "
                + "(select count(t1.id) from TransactionBean t1, ProvinceInfoBean p1 where t1.transactionCategory = 'BUSINESS' and "
                + "t1.stationBean.province = p1.name and p1.area = :area and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t1.finalAmount > 0 "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionBean t2, ProvinceInfoBean p2 where t2.transactionCategory = 'BUSINESS' and "
                + "t2.stationBean.province = p2.name and p2.area = :area and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t2.finalAmount > 0 "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionBean t3, ProvinceInfoBean p3 where t3.transactionCategory = 'BUSINESS' and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllAreaBusiness", query = "select "
                + "count(t3.id) from TransactionBean t3, ProvinceInfoBean p3 where t3.transactionCategory = 'BUSINESS' and "
                + "t3.stationBean.province = p3.name and p3.area = :area and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.finalAmount > 0 "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisProvince", query = "select " + "(select count(t1.id) from TransactionBean t1 where t1.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 2 and t1.stationBean.province = :stationProvince) "
                + "and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionBean t2 where t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 2 and t2.stationBean.province = :stationProvince) "
                + "and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionBean t3 where t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 2 and t3.stationBean.province = :stationProvince) "
                + "and t3.creationTimestamp <= :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisVoucherConsumed", query = "select "
                + "(select count(t1.id) from TransactionBean t1 join t1.prePaidConsumeVoucherBeanList cv1 join cv1.prePaidConsumeVoucherDetailBean cvd1 "
                + "where cvd1.voucherValue = cvd1.consumedValue and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionBean t2 join t2.prePaidConsumeVoucherBeanList cv2 join cv2.prePaidConsumeVoucherDetailBean cvd2 "
                + "where cvd2.voucherValue = cvd2.consumedValue and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionBean t3 join t3.prePaidConsumeVoucherBeanList cv3 join cv3.prePaidConsumeVoucherDetailBean cvd3 "
                + "where cvd3.voucherValue = cvd3.consumedValue and t3.creationTimestamp <= :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisPromoVoucherConsumed", query = "select "
                + "(select count(t1.id) from TransactionBean t1 join t1.prePaidConsumeVoucherBeanList cv1 join cv1.prePaidConsumeVoucherDetailBean cvd1 "
                + "where cvd1.voucherValue = cvd1.consumedValue and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate) "
                + "and cvd1.voucherCode IN (select pv1.code from PromoVoucherBean pv1 where pv1.code = cvd1.voucherCode and pv1.promotionBean = :promotionBean)), "
                + "(select count(t2.id) from TransactionBean t2 join t2.prePaidConsumeVoucherBeanList cv2 join cv2.prePaidConsumeVoucherDetailBean cvd2 "
                + "where cvd2.voucherValue = cvd2.consumedValue and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate) "
                + "and cvd2.voucherCode IN (select pv2.code from PromoVoucherBean pv2 where pv2.code = cvd2.voucherCode and pv2.promotionBean = :promotionBean)), "
                + "(select count(t3.id) from TransactionBean t3 join t3.prePaidConsumeVoucherBeanList cv3 join cv3.prePaidConsumeVoucherDetailBean cvd3 "
                + "where cvd3.voucherValue = cvd3.consumedValue and t3.creationTimestamp <= :dailyEndDate "
                + "and cvd3.voucherCode IN (select pv3.code from PromoVoucherBean pv3 where pv3.code = cvd3.voucherCode and pv3.promotionBean = :promotionBean)) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalAmount", query = "select "
                + "(select case when sum(t1.finalAmount) is null then 0.00 else sum(t1.finalAmount) end from TransactionBean t1 "
                + "where t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.finalAmount) is null then 0.00 else sum(t2.finalAmount) end from TransactionBean t2 "
                + "where t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.finalAmount) is null then 0.00 else sum(t3.finalAmount) end from TransactionBean t3 " + "where t3.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalAmount", query = "select "
                + "case when sum(t3.finalAmount) is null then 0.00 else sum(t3.finalAmount) end from TransactionBean t3 " + "where t3.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalAmountBusiness", query = "select "
                + "(select case when sum(t1.finalAmount) is null then 0.00 else sum(t1.finalAmount) end from TransactionBean t1 "
                + "where t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t1.transactionCategory = 'BUSINESS' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.finalAmount) is null then 0.00 else sum(t2.finalAmount) end from TransactionBean t2 "
                + "where t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t2.transactionCategory = 'BUSINESS' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.finalAmount) is null then 0.00 else sum(t3.finalAmount) end from TransactionBean t3 " + "where t3.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.transactionCategory = 'BUSINESS' and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalAmountBusiness", query = "select "
                + "case when sum(t3.finalAmount) is null then 0.00 else sum(t3.finalAmount) end from TransactionBean t3 " + "where t3.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t3.transactionCategory = 'BUSINESS' and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalFuelQuantity", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionBean t1 "
                + "where (t1.transactionCategory is null or t1.transactionCategory = 'CUSTOMER') and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionBean t2 "
                + "where (t2.transactionCategory is null or t2.transactionCategory = 'CUSTOMER') and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalFuelQuantity", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 "
                + "where (t3.transactionCategory is null or t3.transactionCategory = 'CUSTOMER') and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalFuelQuantityBusiness", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionBean t1 "
                + "where t1.transactionCategory = 'BUSINESS' and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionBean t2 "
                + "where t2.transactionCategory = 'BUSINESS' and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalFuelQuantityBusiness", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalFuelQuantityByProduct", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionBean t1 "
                + "where (t1.transactionCategory = 'CUSTOMER' or t1.transactionCategory is null) and t1.productDescription = :productDescription and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionBean t2 "
                + "where (t2.transactionCategory = 'CUSTOMER' or t2.transactionCategory is null) and t2.productDescription = :productDescription and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 "
                + "where (t3.transactionCategory = 'CUSTOMER' or t3.transactionCategory is null) and t3.productDescription = :productDescription and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"), 
                
        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalFuelQuantityByProduct", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 "
                + "where (t3.transactionCategory = 'CUSTOMER' or t3.transactionCategory is null) "
                + "and t3.productDescription = :productDescription "
                + "and t3.finalAmount > 0 "
                + "and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"), 

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalFuelQuantityByProductBusiness", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionBean t1 "
                + "where t1.transactionCategory = 'BUSINESS' and t1.productDescription = :productDescription and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionBean t2 "
                + "where t2.transactionCategory = 'BUSINESS' and t2.productDescription = :productDescription and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.productDescription = :productDescription and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"), 
                
        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalFuelQuantityByProductBusiness", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 "
                + "where t3.transactionCategory = 'BUSINESS' and t3.productDescription = :productDescription and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) "
                + "and t3.creationTimestamp >= :totalStartDate and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "TransactionBean.statisticsReportDetail", query = "select t from TransactionBean t "
                + "where (t.transactionCategory is null or t.transactionCategory = 'CUSTOMER') and t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL
                + "' and t.finalAmount > 0 and t.stationBean.stationStatus = 2 " + "and t.creationTimestamp <= :endDate order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionBean.statisticsReportDetailBusiness", query = "select t from TransactionBean t "
                + "where t.transactionCategory = 'BUSINESS' and t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL
                + "' and t.finalAmount > 0 and t.stationBean.stationStatus = 2 " + "and t.creationTimestamp <= :endDate order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionBean.statisticsReportDetailByDate", query = "select t from TransactionBean t "
                + "where (t.transactionCategory is null or t.transactionCategory = 'CUSTOMER') and t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL
                + "' and t.finalAmount > 0 and t.stationBean.stationStatus = 2 "
                + "and (t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionBean.statisticsReportDetailByDateBusiness", query = "select t from TransactionBean t "
                + "where t.transactionCategory = 'BUSINESS' and t.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL
                + "' and t.finalAmount > 0 and t.stationBean.stationStatus = 2 "
                + "and (t.creationTimestamp >= :startDate and t.creationTimestamp < :endDate) order by t.creationTimestamp desc"),

        @NamedQuery(name = "TransactionBean.findActiveTransactionByUser", query = "select t from TransactionBean t where t.userBean = :userBean and t.confirmed = false"),

        @NamedQuery(name = "TransactionBean.findActiveTransactionByFinalStatusNull", query = "select t from TransactionBean t where t.finalStatusType = null"),

        @NamedQuery(name = "TransactionBean.findActiveTransaction", query = "select t from TransactionBean t where t.confirmed = false"),

        @NamedQuery(name = "TransactionBean.findActiveTransactionNew", query = "select t from TransactionBean t where t.finalStatusType = null"),

        @NamedQuery(name = "TransactionBean.countActiveTransactionNew", query = "select count(t) from TransactionBean t where t.finalStatusType = null"),

        @NamedQuery(name = "TransactionBean.findTransactionByFinalStatusType", query = "select t from TransactionBean t where t.finalStatusType = :finalStatusType"),

        @NamedQuery(name = "TransactionBean.findTransactionByFinalStatusTypeDate", query = "select t from TransactionBean t where t.finalStatusType = :finalStatusType and t.creationTimestamp < :time"),

        @NamedQuery(name = "TransactionBean.findTransactionByFinalStatusTypeAndVoucherReconciliation", query = "select t from TransactionBean t "
                + "where t.finalStatusType = :finalStatusType and t.voucherReconciliation = :voucherReconciliation"),

        @NamedQuery(name = "TransactionBean.findTransactionByFinalStatusTypeAndLoyaltyReconciliation", query = "select t from TransactionBean t "
                + "where t.finalStatusType = :finalStatusType and t.loyaltyReconcile = :loyaltyReconcile"),

        @NamedQuery(name = "TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus", query = "select t from TransactionBean t "
                + "where t.finalStatusType = :finalStatusType and t.GFGNotification = :gfgNotification and t.creationTimestamp <= :startTime"),

        @NamedQuery(name = "TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatusDate", query = "select t from TransactionBean t where t.finalStatusType = :finalStatusType and t.voucherReconciliation is not null and t.voucherReconciliation = false and t.creationTimestamp < :time and t.GFGNotification = :gfgNotification"),

        @NamedQuery(name = "TransactionBean.findTransactionsByPaymentMethodId", query = "select t from TransactionBean t where t.paymentMethodId = :paymentMethodId"),

        @NamedQuery(name = "TransactionBean.findTransactionBySrcTransactionID", query = "select t from TransactionBean t where t.srcTransactionID = :srcTransactionID"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalLoyalty", query = "select "
                + "(select count(t1.id) from TransactionBean t1 join t1.prePaidLoadLoyaltyCreditsBeanList tl1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), " + "(select count(t2.id) from TransactionBean t2 join t2.prePaidLoadLoyaltyCreditsBeanList tl2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), " + "(select count(t3.id) from TransactionBean t3 join t3.prePaidLoadLoyaltyCreditsBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalLoyalty", query = "select "
                + "count(t3.id) from TransactionBean t3 join t3.prePaidLoadLoyaltyCreditsBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalLoyaltyArea", query = "select "
                + "(select count(t1.id) from TransactionBean t1, ProvinceInfoBean p1 join t1.prePaidLoadLoyaltyCreditsBeanList tl1 "
                + "where t1.stationBean.province = p1.name and p1.area = :area and tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 "
                + "and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select count(t2.id) from TransactionBean t2, ProvinceInfoBean p2 join t2.prePaidLoadLoyaltyCreditsBeanList tl2 "
                + "where t2.stationBean.province = p2.name and p2.area = :area and tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 "
                + "and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select count(t3.id) from TransactionBean t3, ProvinceInfoBean p3 join t3.prePaidLoadLoyaltyCreditsBeanList tl3 "
                + "where t3.stationBean.province = p3.name and p3.area = :area and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 "
                + "and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalLoyaltyArea", query = "select "
                + "count(t3.id) from TransactionBean t3, ProvinceInfoBean p3 join t3.prePaidLoadLoyaltyCreditsBeanList tl3 "
                + "where t3.stationBean.province = p3.name and p3.area = :area and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 "
                + "and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalLoyaltyCredits", query = "select "
                + "(select case when sum(tl1.credits) is null then 0 else sum(tl1.credits) end from TransactionBean t1 join t1.prePaidLoadLoyaltyCreditsBeanList tl1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(tl2.credits) is null then 0 else sum(tl2.credits) end from TransactionBean t2 join t2.prePaidLoadLoyaltyCreditsBeanList tl2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(tl3.credits) is null then 0 else sum(tl3.credits) end from TransactionBean t3 join t3.prePaidLoadLoyaltyCreditsBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalLoyaltyCredits", query = "select "
                + "case when sum(tl3.credits) is null then 0 else sum(tl3.credits) end from TransactionBean t3 join t3.prePaidLoadLoyaltyCreditsBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),

        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalLoyaltyFuelQuantity", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionBean t1 join t1.prePaidLoadLoyaltyCreditsBeanList tl1 "
                + "where tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionBean t2 join t2.prePaidLoadLoyaltyCreditsBeanList tl2 "
                + "where tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 join t3.prePaidLoadLoyaltyCreditsBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),
                
        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantity", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 join t3.prePaidLoadLoyaltyCreditsBeanList tl3 "
                + "where tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
                
        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisTotalLoyaltyFuelQuantityByProduct", query = "select "
                + "(select case when sum(t1.fuelQuantity) is null then 0.00 else sum(t1.fuelQuantity) end from TransactionBean t1 join t1.prePaidLoadLoyaltyCreditsBeanList tl1 "
                + "where t1.productDescription = :productDescription and tl1.statusCode = '00' and tl1.operationType = 'LOAD' and tl1.credits > 0 and t1.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t1.finalAmount > 0 and (t1.stationBean.stationStatus = 1 or t1.stationBean.stationStatus = 2) and (t1.creationTimestamp >= :dailyStartDate "
                + "and t1.creationTimestamp < :dailyEndDate)), "
                + "(select case when sum(t2.fuelQuantity) is null then 0.00 else sum(t2.fuelQuantity) end from TransactionBean t2 join t2.prePaidLoadLoyaltyCreditsBeanList tl2 "
                + "where t2.productDescription = :productDescription and tl2.statusCode = '00' and tl2.operationType = 'LOAD' and tl2.credits > 0 and t2.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t2.finalAmount > 0 and (t2.stationBean.stationStatus = 1 or t2.stationBean.stationStatus = 2) and (t2.creationTimestamp >= :weeklyStartDate "
                + "and t2.creationTimestamp < :weeklyEndDate)), "
                + "(select case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 join t3.prePaidLoadLoyaltyCreditsBeanList tl3 "
                + "where t3.productDescription = :productDescription and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate) from TransactionBean t"),
                
        @NamedQuery(name = "TransactionBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantityByProduct", query = "select "
                + "case when sum(t3.fuelQuantity) is null then 0.00 else sum(t3.fuelQuantity) end from TransactionBean t3 join t3.prePaidLoadLoyaltyCreditsBeanList tl3 "
                + "where t3.productDescription = :productDescription and tl3.statusCode = '00' and tl3.operationType = 'LOAD' and tl3.credits > 0 and t3.finalStatusType = '" + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' "
                + "and t3.finalAmount > 0 and (t3.stationBean.stationStatus = 1 or t3.stationBean.stationStatus = 2) and t3.creationTimestamp >= :totalStartDate "
                + "and t3.creationTimestamp < :dailyEndDate"),
        
        @NamedQuery(name = "TransactionBean.amountRefuelForPeriod", query = "select "
                + "case when sum(t.finalAmount) is null then 0 else sum(t.finalAmount) end from TransactionBean t where t.userBean.id=:userId and t.finalStatusType = '"
                + StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "' and t.finalAmount > 0 "
                + "and t.creationTimestamp >= :periodStartDate "
                + "and t.creationTimestamp < :periodEndDate "
                + "and t.acquirerID <> 'MULTICARD'"),
                
        @NamedQuery(name = "TransactionBean.countPendingTransactions", query = "select t from TransactionBean t where t.serverName = :serverName and (t.finalStatusType is null or t.finalStatusType = '') ")
 
        })
@SqlResultSetMapping(name = "updateResult", columns = { @ColumnResult(name = "count") })
@NamedNativeQueries({ @NamedNativeQuery(name = "confirmTransactionBean", query = "UPDATE TRANSACTIONS SET confirmed = 1 WHERE id = ?", resultSetMapping = "updateResult") })
public class TransactionBean implements TransactionInterfaceBean {

    public static final String                 FIND_BY_ID                                              = "TransactionBean.findTransactionByID";
    public static final String                 FIND_BY_USER                                            = "TransactionBean.findTransactionByUser";
    public static final String                 FIND_ACTIVE_BY_USER                                     = "TransactionBean.findActiveTransactionByUser";
    public static final String                 FIND_ACTIVE                                             = "TransactionBean.findActiveTransaction";
    public static final String                 FIND_ACTIVE_NEW                                         = "TransactionBean.findActiveTransactionNew";
    public static final String                 COUNT_ACTIVE_NEW                                        = "TransactionBean.countActiveTransactionNew";
    public static final String                 FIND_BY_FINAL_STATUS_TYPE                               = "TransactionBean.findTransactionByFinalStatusType";
    public static final String                 FIND_BY_FINAL_STATUS_TYPE_DATE                          = "TransactionBean.findTransactionByFinalStatusTypeDate";
    public static final String                 FIND_BY_FINAL_STATUS_TYPE_AND_NOTIFICATION_DATE         = "TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatusDate";
    public static final String                 FIND_BY_FINAL_STATUS_TYPE_AND_NOTIFICATION              = "TransactionBean.findTransactionByFinalStatusTypeAndNotificationStatus";
    public static final String                 FIND_BY_FINAL_STATUS_TYPE_AND_VOUCHER_RECONCILIATION    = "TransactionBean.findTransactionByFinalStatusTypeAndVoucherReconciliation";
    public static final String                 FIND_BY_FINAL_STATUS_TYPE_AND_LOYALTY_RECONCILIATION    = "TransactionBean.findTransactionByFinalStatusTypeAndLoyaltyReconciliation";
    public static final String                 FIND_BY_USER_AND_DATE                                   = "TransactionBean.findTransactionByUserAndDate";
    public static final String                 FIND_SUCCESSFUL_BY_USER_AND_DATE                        = "TransactionBean.findTransactionSuccessfulByUserAndDate";
    public static final String                 FIND_BY_DATE                                            = "TransactionBean.findTransactionByDate";
    public static final String                 FIND_BY_DATE_SRC_TRANSACTION                            = "TransactionBean.findTransactionByDateAndSrcTransaction";
    public static final String                 FIND_ACTIVE_BY_FINAL_STATUS_NULL                        = "TransactionBean.findActiveTransactionByFinalStatusNull";
    public static final String                 STATISTICS_REPORT_DETAIL                                = "TransactionBean.statisticsReportDetail";
    public static final String                 STATISTICS_REPORT_DETAIL_BUSINESS                       = "TransactionBean.statisticsReportDetailBusiness";
    public static final String                 STATISTICS_REPORT_DETAIL_BY_DATE                        = "TransactionBean.statisticsReportDetailByDate";
    public static final String                 STATISTICS_REPORT_DETAIL_BY_DATE_BUSINESS               = "TransactionBean.statisticsReportDetailByDateBusiness";
    public static final String                 STATISTICS_REPORT_SYNTHESIS                             = "TransactionBean.statisticsReportSynthesis";
    public static final String                 STATISTICS_REPORT_ALL_SYNTHESIS                         = "TransactionBean.statisticsReportAllSynthesis";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_BUSINESS                    = "TransactionBean.statisticsReportSynthesisBusiness";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_BUSINESS                = "TransactionBean.statisticsReportSynthesisAllBusiness";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_AREA                        = "TransactionBean.statisticsReportSynthesisArea";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_AREA                    = "TransactionBean.statisticsReportSynthesisAllArea";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_AREA_BUSINESS               = "TransactionBean.statisticsReportSynthesisAreaBusiness";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_AREA_BUSINESS           = "TransactionBean.statisticsReportSynthesisAllAreaBusiness";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_PROVINCE                    = "TransactionBean.statisticsReportSynthesisProvince";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_VOUCHER_CONSUMED            = "TransactionBean.statisticsReportSynthesisVoucherConsumed";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_PROMO_VOUCHER_CONSUMED      = "TransactionBean.statisticsReportSynthesisPromoVoucherConsumed";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT                = "TransactionBean.statisticsReportSynthesisTotalAmount";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT            = "TransactionBean.statisticsReportSynthesisAllTotalAmount";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_AMOUNT_BUSINESS       = "TransactionBean.statisticsReportSynthesisTotalAmountBusiness";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_AMOUNT_BUSINESS   = "TransactionBean.statisticsReportSynthesisAllTotalAmountBusiness";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY         = "TransactionBean.statisticsReportSynthesisTotalFuelQuantity";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY     = "TransactionBean.statisticsReportSynthesisAllTotalFuelQuantity";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BUSINESS = "TransactionBean.statisticsReportSynthesisTotalFuelQuantityBusiness";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BUSINESS = "TransactionBean.statisticsReportSynthesisAllTotalFuelQuantityBusiness";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT = "TransactionBean.statisticsReportSynthesisTotalFuelQuantityByProduct";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT = "TransactionBean.statisticsReportSynthesisAllTotalFuelQuantityByProduct";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS = "TransactionBean.statisticsReportSynthesisTotalFuelQuantityByProductBusiness";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_FUEL_QUANTITY_BY_PRODUCT_BUSINESS = "TransactionBean.statisticsReportSynthesisAllTotalFuelQuantityByProductBusiness";
    public static final String                 FIND_BY_PAYMENT_METHOD_ID                               = "TransactionBean.findTransactionsByPaymentMethodId";
    public static final String                 FIND_BY_SRC_TRANSACTION_ID                              = "TransactionBean.findTransactionBySrcTransactionID";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY               = "TransactionBean.statisticsReportSynthesisTotalLoyalty";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY           = "TransactionBean.statisticsReportSynthesisAllTotalLoyalty";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_AREA          = "TransactionBean.statisticsReportSynthesisTotalLoyaltyArea";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_AREA      = "TransactionBean.statisticsReportSynthesisAllTotalLoyaltyArea";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_CREDITS       = "TransactionBean.statisticsReportSynthesisTotalLoyaltyCredits";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_CREDITS   = "TransactionBean.statisticsReportSynthesisAllTotalLoyaltyCredits";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY = "TransactionBean.statisticsReportSynthesisTotalLoyaltyFuelQuantity";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY = "TransactionBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantity";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT = "TransactionBean.statisticsReportSynthesisTotalLoyaltyFuelQuantityByProduct";
    public static final String                 STATISTICS_REPORT_SYNTHESIS_ALL_TOTAL_LOYALTY_FUEL_QUANTITY_BY_PRODUCT = "TransactionBean.statisticsReportSynthesisAllTotalLoyaltyFuelQuantityByProduct";
    public static final String                 AMOUNT_REFUEL_FOR_PERIOD						     		= "TransactionBean.amountRefuelForPeriod";
    public static final String                 COUNT_PENDING_TRANSACTION  = "TransactionBean.countPendingTransactions";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                               id;

    @Column(name = "transactionID", nullable = true)
    private String                             transactionID;

    @OneToOne()
    @JoinColumn(name = "user_id")
    private UserBean                           userBean;

    @OneToOne()
    @JoinColumn(name = "station_id")
    private StationBean                        stationBean;

    @Column(name = "use_voucher", nullable = true)
    private Boolean                            useVoucher;

    @Column(name = "creation_timestamp", nullable = false)
    private Date                               creationTimestamp;

    @Column(name = "end_refuel_timestamp", nullable = true)
    private Date                               endRefuelTimestamp;

    @Column(name = "initial_amount", nullable = true)
    private Double                             initialAmount;

    @Column(name = "final_amount", nullable = true)
    private Double                             finalAmount;

    @Column(name = "fuel_quantity", nullable = true)
    private Double                             fuelQuantity;

    @Column(name = "fuel_amount", nullable = true)
    private Double                             fuelAmount;
    
    @Column(name = "unit_price", nullable = true)
    private Double                             unitPrice;

    @Column(name = "pump_id", nullable = true)
    private String                             pumpID;

    @Column(name = "pump_number", nullable = true)
    private Integer                            pumpNumber;

    @Column(name = "product_id", nullable = true)
    private String                             productID;

    @Column(name = "product_description", nullable = true)
    private String                             productDescription;

    @Column(name = "currency", nullable = true)
    private String                             currency;

    @Column(name = "shop_login", nullable = true)
    private String                             shopLogin;

    @Column(name = "acquirer_id", nullable = true)
    private String                             acquirerID;

    @Column(name = "bank_transaction_id", nullable = true)
    private String                             bankTansactionID;

    @Column(name = "authorization_code", nullable = true)
    private String                             authorizationCode;

    @Column(name = "token", nullable = true)
    private String                             token;

    @Column(name = "payment_type", nullable = true)
    private String                             paymentType;

    @Column(name = "final_status_type", nullable = true)
    private String                             finalStatusType;

    @Column(name = "gfg_notification", nullable = false)
    private Boolean                            GFGNotification;

    @Column(name = "confirmed", nullable = false)
    private Boolean                            confirmed;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionBean")
    private Set<TransactionStatusBean>         transactionStatusBeanData                               = new HashSet<TransactionStatusBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionBean")
    private Set<TransactionEventBean>          transactionEventBeanData                                = new HashSet<TransactionEventBean>(0);
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionBean")
    private Set<TransactionOperationBean>      transactionOperationBeanData                                = new HashSet<TransactionOperationBean>(0);

    @Column(name = "payment_method_id", nullable = true)
    private Long                               paymentMethodId;

    @Column(name = "payment_method_type", nullable = true)
    private String                             paymentMethodType;

    @Column(name = "server_name", nullable = true)
    private String                             serverName;

    @Column(name = "status_attempts_left", nullable = true)
    private Integer                            statusAttemptsLeft;

    @Column(name = "out_of_range", nullable = true)
    private String                             outOfRange;

    @Column(name = "refuel_mode", nullable = true)
    private String                             refuelMode;

    @Column(name = "voucher_reconciliation", nullable = true)
    private Boolean                            voucherReconciliation;

    @Column(name = "reconciliation_attempts_left", nullable = true)
    private Integer                            reconciliationAttemptsLeft;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionBean")
    private Set<PrePaidConsumeVoucherBean>     prePaidConsumeVoucherBeanList                           = new HashSet<PrePaidConsumeVoucherBean>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionBean", cascade = CascadeType.ALL)
    private Set<PrePaidLoadLoyaltyCreditsBean> prePaidLoadLoyaltyCreditsBeanList                       = new HashSet<PrePaidLoadLoyaltyCreditsBean>(0);

    @Column(name = "voucherStatus", nullable = true)
    private String                             voucherStatus;

    @Column(name = "src_transaction_id", nullable = true)
    private String                             srcTransactionID;

    @Column(name = "encoded_secret_key", nullable = true)
    private String                             encodedSecretKey;

    @Column(name = "group_acquirer", nullable = true)
    private String                             groupAcquirer;

    @Column(name = "loyalty_reconcile", nullable = true)
    private Boolean                            loyaltyReconcile;

    @Column(name = "gfg_electronic_Invoice_id", nullable = true)
    private String                             gfgElectronicInvoiceID;

    @Column(name = "transaction_category", nullable = true)
    private String                            transactionCategory;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "payment_token_package_id")
    private PaymentTokenPackageBean paymentTokenPackageBean;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionBean", cascade = CascadeType.ALL)
    private Set<TransactionAdditionalDataBean> transactionAdditionalDataBeanList = new HashSet<TransactionAdditionalDataBean>(0);

    public TransactionBean() {}

    public TransactionBean(Transaction transaction) {

        this.id = transaction.getId();
        this.transactionID = transaction.getTransactionID();
        this.userBean = new UserBean(transaction.getUser());
        this.stationBean = new StationBean(transaction.getStation());
        this.useVoucher = transaction.getUseVoucher();
        this.creationTimestamp = transaction.getCreationTimestamp();
        this.endRefuelTimestamp = transaction.getEndRefuelTimestamp();
        this.initialAmount = transaction.getInitialAmount();
        this.finalAmount = transaction.getFinalAmount();
        this.fuelAmount = transaction.getFuelAmount();
        this.fuelQuantity = transaction.getFuelQuantity();
        this.unitPrice = transaction.getUnitPrice();
        this.pumpID = transaction.getPumpID();
        this.pumpNumber = transaction.getPumpNumber();
        this.productID = transaction.getProductID();
        this.productDescription = transaction.getProductDescription();
        this.currency = transaction.getCurrency();
        this.shopLogin = transaction.getShopLogin();
        this.acquirerID = transaction.getAcquirerID();
        this.bankTansactionID = transaction.getBankTansactionID();
        this.authorizationCode = transaction.getAuthorizationCode();
        this.token = transaction.getToken();
        this.productID = transaction.getProductID();
        this.paymentType = transaction.getPaymentType();
        this.finalStatusType = transaction.getFinalStatusType();
        this.GFGNotification = transaction.getGFGNotification();
        this.confirmed = transaction.getConfirmed();
        this.paymentMethodId = transaction.getPaymentMethodId();
        this.paymentMethodType = transaction.getPaymentMethodType();
        this.serverName = transaction.getServerName();
        this.statusAttemptsLeft = transaction.getStatusAttemptsLeft();
        this.outOfRange = transaction.getOutOfRange();
        this.refuelMode = transaction.getRefuelMode();
        this.voucherReconciliation = transaction.getVoucherReconciliation();
        this.srcTransactionID = transaction.getSrcTransactionID();
        this.encodedSecretKey = transaction.getEncodedSecretKey();
        this.groupAcquirer = transaction.getGroupAcquirer();
        this.loyaltyReconcile = transaction.getLoyaltyReconcile();
        this.gfgElectronicInvoiceID = transaction.getGFGElectronicInvoiceID();
        this.transactionCategory = transaction.getTransactionCategory().getValue();

        if (transaction.getTransactionStatusData() != null) {
            Set<TransactionStatus> transactionStatusData = transaction.getTransactionStatusData();
            for (TransactionStatus transactionStatus : transactionStatusData) {
                TransactionStatusBean transactionStatusBean = new TransactionStatusBean(transactionStatus);
                transactionStatusBean.setTransactionBean(this);
                this.transactionStatusBeanData.add(transactionStatusBean);
            }
        }

        if (transaction.getTransactionEventData() != null) {
            Set<TransactionEvent> transactionEventData = transaction.getTransactionEventData();
            for (TransactionEvent transactionEvent : transactionEventData) {
                TransactionEventBean transactionEventBean = new TransactionEventBean(transactionEvent);
                transactionEventBean.setTransactionBean(this);
                this.transactionEventBeanData.add(transactionEventBean);
            }
        }

        if (transaction.getPrePaidConsumeVoucherList() != null) {
            for (PrePaidConsumeVoucher prePaidConsumeVoucher : transaction.getPrePaidConsumeVoucherList()) {
                PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean(prePaidConsumeVoucher);
                prePaidConsumeVoucherBean.setTransactionBean(this);
                this.prePaidConsumeVoucherBeanList.add(prePaidConsumeVoucherBean);
            }
        }

        if (transaction.getPrePaidLoadLoyaltyCreditsBeanList() != null) {
            for (PrePaidLoadLoyaltyCredits prePaidLoadLoyaltyCredits : transaction.getPrePaidLoadLoyaltyCreditsBeanList()) {
                PrePaidLoadLoyaltyCreditsBean prePaidLoadLoyaltyCreditsBean = new PrePaidLoadLoyaltyCreditsBean(prePaidLoadLoyaltyCredits);
                prePaidLoadLoyaltyCreditsBean.setTransactionBean(this);
                this.prePaidLoadLoyaltyCreditsBeanList.add(prePaidLoadLoyaltyCreditsBean);
            }
        }
        
        if (transaction.getTransactionAdditionalDataList() != null) {
            for (TransactionAdditionalData transactionAdditionalData : transaction.getTransactionAdditionalDataList()) {
                TransactionAdditionalDataBean transactionAdditionalDataBean = new TransactionAdditionalDataBean(transactionAdditionalData);
                transactionAdditionalDataBean.setTransactionBean(this);
                this.transactionAdditionalDataBeanList.add(transactionAdditionalDataBean);
            }
        }

    }

    public Boolean getLoyaltyReconcile() {
        return loyaltyReconcile;
    }

    public void setLoyaltyReconcile(Boolean loyaltyReconcile) {
        this.loyaltyReconcile = loyaltyReconcile;
    }

    public TransactionBean(TransactionHistoryBean transaction) {

        //this.id = transaction.getId();
        this.transactionID = transaction.getTransactionID();
        this.userBean = transaction.getUserBean();
        this.stationBean = transaction.getStationBean();
        this.useVoucher = transaction.getUseVoucher();
        this.creationTimestamp = transaction.getCreationTimestamp();
        this.endRefuelTimestamp = transaction.getEndRefuelTimestamp();
        this.initialAmount = transaction.getInitialAmount();
        this.finalAmount = transaction.getFinalAmount();
        this.fuelAmount = transaction.getFuelAmount();
        this.fuelQuantity = transaction.getFuelQuantity();
        this.pumpID = transaction.getPumpID();
        this.pumpNumber = transaction.getPumpNumber();
        this.productID = transaction.getProductID();
        this.productDescription = transaction.getProductDescription();
        this.currency = transaction.getCurrency();
        this.shopLogin = transaction.getShopLogin();
        this.acquirerID = transaction.getAcquirerID();
        this.bankTansactionID = transaction.getBankTansactionID();
        this.authorizationCode = transaction.getAuthorizationCode();
        this.token = transaction.getToken();
        this.productID = transaction.getProductID();
        this.paymentType = transaction.getPaymentType();
        this.finalStatusType = transaction.getFinalStatusType();
        this.GFGNotification = transaction.getGFGNotification();
        this.confirmed = transaction.getConfirmed();
        this.paymentMethodId = transaction.getPaymentMethodId();
        this.paymentMethodType = transaction.getPaymentMethodType();
        this.serverName = transaction.getServerName();
        this.statusAttemptsLeft = transaction.getStatusAttemptsLeft();
        this.outOfRange = transaction.getOutOfRange();
        this.refuelMode = transaction.getRefuelMode();
        this.voucherReconciliation = transaction.getVoucherReconciliation();
        this.srcTransactionID = transaction.getSrcTransactionID();
        this.encodedSecretKey = transaction.getEncodedSecretKey();
        this.groupAcquirer = transaction.getGroupAcquirer();
        this.gfgElectronicInvoiceID = transaction.getGFGElectronicInvoiceID();
        this.transactionCategory = transaction.getTransactionCategory().getValue();

        if (transaction.getTransactionStatusHistoryBeanData() != null) {
            Set<TransactionStatusHistoryBean> transactionStatusBeanData = transaction.getTransactionStatusHistoryBeanData();
            for (TransactionStatusHistoryBean transactionStatusBean : transactionStatusBeanData) {
                TransactionStatusBean transactionStatusHistoryBean = new TransactionStatusBean(transactionStatusBean);
                transactionStatusHistoryBean.setTransactionBean(this);
                this.transactionStatusBeanData.add(transactionStatusHistoryBean);
            }
        }

        if (transaction.getTransactionEventHistoryBeanData() != null) {
            Set<TransactionEventHistoryBean> transactionEventBeanData = transaction.getTransactionEventHistoryBeanData();
            for (TransactionEventHistoryBean transactionEventBean : transactionEventBeanData) {
                TransactionEventBean transactionEventHistoryBean = new TransactionEventBean(transactionEventBean);
                transactionEventHistoryBean.setTransactionBean(this);
                this.transactionEventBeanData.add(transactionEventHistoryBean);
            }
        }

        if (transaction.getPrePaidConsumeVoucherHistoryBeanList() != null) {
            for (PrePaidConsumeVoucherHistoryBean prePaidConsumeVoucherBean : transaction.getPrePaidConsumeVoucherHistoryBeanList()) {
                PrePaidConsumeVoucherBean prePaidConsumeVoucherHistoryBean = new PrePaidConsumeVoucherBean(prePaidConsumeVoucherBean);
                prePaidConsumeVoucherHistoryBean.setTransactionBean(this);
                this.prePaidConsumeVoucherBeanList.add(prePaidConsumeVoucherHistoryBean);
            }
        }

        if (transaction.getPrePaidLoadLoyaltyCreditsHistoryBeanList() != null) {
            for (PrePaidLoadLoyaltyCreditsHistoryBean prePaidLoadLoyaltyCreditsHistoryBean : transaction.getPrePaidLoadLoyaltyCreditsHistoryBeanList()) {
                PrePaidLoadLoyaltyCreditsBean prePaidLoadLoyaltyCreditsBean = new PrePaidLoadLoyaltyCreditsBean(prePaidLoadLoyaltyCreditsHistoryBean);
                prePaidLoadLoyaltyCreditsBean.setTransactionBean(this);
                this.prePaidLoadLoyaltyCreditsBeanList.add(prePaidLoadLoyaltyCreditsBean);
            }
        }
    }

    public Transaction toTransaction() {

        Transaction transaction = new Transaction();
        transaction.setId(this.id);
        transaction.setTransactionID(this.transactionID);
        transaction.setUser(this.userBean.toUser());
        transaction.setStation(this.stationBean.toStation());
        transaction.setUseVoucher(this.useVoucher);
        transaction.setCreationTimestamp(this.creationTimestamp);
        transaction.setEndRefuelTimestamp(this.endRefuelTimestamp);
        transaction.setInitialAmount(this.initialAmount);
        transaction.setFinalAmount(this.finalAmount);
        transaction.setFuelAmount(this.fuelAmount);
        transaction.setFuelQuantity(this.fuelQuantity);
        transaction.setUnitPrice(this.unitPrice);
        transaction.setPumpID(this.pumpID);
        transaction.setPumpNumber(this.pumpNumber);
        transaction.setProductID(this.productID);
        transaction.setProductDescription(this.productDescription);
        transaction.setCurrency(this.currency);
        transaction.setShopLogin(this.shopLogin);
        transaction.setAcquirerID(this.acquirerID);
        transaction.setBankTansactionID(this.bankTansactionID);
        transaction.setAuthorizationCode(this.authorizationCode);
        transaction.setToken(this.token);
        transaction.setProductID(this.productID);
        transaction.setPaymentType(this.paymentType);
        transaction.setFinalStatusType(this.finalStatusType);
        transaction.setGFGNotification(this.GFGNotification);
        transaction.setConfirmed(this.confirmed);
        transaction.setPaymentMethodId(this.paymentMethodId);
        transaction.setPaymentMethodType(this.paymentMethodType);
        transaction.setServerName(this.serverName);
        transaction.setStatusAttemptsLeft(this.statusAttemptsLeft);
        transaction.setOutOfRange(this.outOfRange);
        transaction.setRefuelMode(this.refuelMode);
        transaction.setVoucherReconciliation(this.voucherReconciliation);
        transaction.setSrcTransactionID(this.srcTransactionID);
        transaction.setEncodedSecretKey(this.encodedSecretKey);
        transaction.setGroupAcquirer(this.groupAcquirer);
        transaction.setLoyaltyReconcile(this.loyaltyReconcile);
        transaction.setGFGElectronicInvoiceID(this.gfgElectronicInvoiceID);
        transaction.setTransactionCategory(this.getTransactionCategory());

        if (!this.transactionStatusBeanData.isEmpty()) {

            Set<TransactionStatusBean> transactionStatusBeanData = this.transactionStatusBeanData;
            for (TransactionStatusBean transactionStatusBean : transactionStatusBeanData) {
                TransactionStatus transactionStatus = transactionStatusBean.toTransactionStatus();
                transaction.getTransactionStatusData().add(transactionStatus);
            }
        }

        if (!this.transactionEventBeanData.isEmpty()) {

            Set<TransactionEventBean> transactionEventBeanData = this.transactionEventBeanData;
            for (TransactionEventBean transactionEventBean : transactionEventBeanData) {
                TransactionEvent transactionEvent = transactionEventBean.toTransactionEvent();
                transaction.getTransactionEventData().add(transactionEvent);
            }
        }

        if (!this.prePaidConsumeVoucherBeanList.isEmpty()) {

            for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : this.prePaidConsumeVoucherBeanList) {
                PrePaidConsumeVoucher prePaidConsumeVoucher = prePaidConsumeVoucherBean.toPrePaidConsumeVoucher();
                transaction.getPrePaidConsumeVoucherList().add(prePaidConsumeVoucher);
            }
        }

        if (!this.prePaidLoadLoyaltyCreditsBeanList.isEmpty()) {

            for (PrePaidLoadLoyaltyCreditsBean prePaidLoadLoyaltyCreditsBean : this.prePaidLoadLoyaltyCreditsBeanList) {
                PrePaidLoadLoyaltyCredits prePaidLoadLoyaltyCredits = prePaidLoadLoyaltyCreditsBean.toPrePaidLoadLoyaltyCredits();
                transaction.getPrePaidLoadLoyaltyCreditsBeanList().add(prePaidLoadLoyaltyCredits);
            }
        }
        
        if (!this.transactionAdditionalDataBeanList.isEmpty()) {

            for (TransactionAdditionalDataBean transactionAdditionalDataBean : this.transactionAdditionalDataBeanList) {
                TransactionAdditionalData transactionAdditionalData = transactionAdditionalDataBean.toTransactionAdditionalData();
                transaction.getTransactionAdditionalDataList().add(transactionAdditionalData);
            }
        }
        
        if (!this.transactionOperationBeanData.isEmpty()) {

            for (TransactionOperationBean transactionOperationBean : this.transactionOperationBeanData) {
                TransactionOperation transactionOperation = transactionOperationBean.toTransactionOperation();
                transaction.getTransactionOperationList().add(transactionOperation);
            }
        }

        return transaction;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public StationBean getStationBean() {
        return stationBean;
    }

    public void setStationBean(StationBean stationBean) {
        this.stationBean = stationBean;
    }

    public Boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getEndRefuelTimestamp() {
        return endRefuelTimestamp;
    }

    public void setEndRefuelTimestamp(Date endRefuelTimestamp) {
        this.endRefuelTimestamp = endRefuelTimestamp;
    }

    public Double getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(Double initialAmount) {
        this.initialAmount = initialAmount;
    }

    public Double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(Double finalAmount) {
        this.finalAmount = finalAmount;
    }

    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public Double getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(Double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public String getPumpID() {
        return pumpID;
    }

    public void setPumpID(String pumpID) {
        this.pumpID = pumpID;
    }

    public Integer getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(Integer pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
        this.acquirerID = acquirerID;
    }

    public String getBankTansactionID() {
        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {
        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getFinalStatusType() {
        return finalStatusType;
    }

    public void setFinalStatusType(String finalStatusType) {
        this.finalStatusType = finalStatusType;
    }

    public Boolean getGFGNotification() {
        return GFGNotification;
    }

    public void setGFGNotification(Boolean gFGNotification) {
        GFGNotification = gFGNotification;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Set<TransactionStatusBean> getTransactionStatusBeanData() {
        return transactionStatusBeanData;
    }

    public void setTransactionStatusBeanData(Set<TransactionStatusBean> transactionStatusBeanData) {
        this.transactionStatusBeanData = transactionStatusBeanData;
    }

    public Set<TransactionEventBean> getTransactionEventBeanData() {
        return transactionEventBeanData;
    }

    public void setTransactionEventBeanData(Set<TransactionEventBean> transactionEventBeanData) {
        this.transactionEventBeanData = transactionEventBeanData;
    }

    public TransactionStatusBean getLastTransactionStatus() {
        TransactionStatusBean transactionStatus = null;
        int sequence = 0;

        for (TransactionStatusBean transactionStatusBean : getTransactionStatusBeanData()) {
            //System.out.println("sequenceID:" + transactionStatusBean.getSequenceID());
            if (transactionStatusBean.getSequenceID() != null && transactionStatusBean.getSequenceID().intValue() > sequence) {
                transactionStatus = transactionStatusBean;
                sequence = transactionStatusBean.getSequenceID().intValue();
                //System.out.println("sequence selected:" + sequence);
            }
        }

        //System.out.println("transactionStatus sequenceID:" + (transactionStatus != null ? transactionStatus.getSequenceID().intValue() : 0));
        return transactionStatus;
    }

    public TransactionEventBean getLastTransactionEvent() {
        TransactionEventBean transactionEvent = null;
        int sequence = 0;

        for (TransactionEventBean transactionEventBean : getTransactionEventBeanData()) {
            //System.out.println("sequenceID:" + transactionEventBean.getSequenceID());
            if (transactionEventBean.getSequenceID() != null && transactionEventBean.getSequenceID().intValue() > sequence) {
                transactionEvent = transactionEventBean;
                sequence = transactionEventBean.getSequenceID().intValue();
                //System.out.println("sequence selected:" + sequence);
            }
        }

        //System.out.println("transactionEvent sequenceID:" + (transactionEvent != null ? transactionEvent.getSequenceID().intValue() : 0));
        return transactionEvent;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public Integer getStatusAttemptsLeft() {
        return statusAttemptsLeft;
    }

    public void setStatusAttemptsLeft(Integer statusAttemptsLeft) {
        this.statusAttemptsLeft = statusAttemptsLeft;
    }

    public String getOutOfRange() {
        return outOfRange;
    }

    public void setOutOfRange(String outOfRange) {
        this.outOfRange = outOfRange;
    }

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    /**
     * @return the voucherReconciliation
     */
    public Boolean getVoucherReconciliation() {
        return voucherReconciliation;
    }

    /**
     * @param voucherReconciliation
     *            the voucherReconciliation to set
     */
    public void setVoucherReconciliation(Boolean voucherReconciliation) {
        this.voucherReconciliation = voucherReconciliation;
    }

    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public Set<PrePaidConsumeVoucherBean> getPrePaidConsumeVoucherBeanList() {
        return prePaidConsumeVoucherBeanList;
    }

    public void setPrePaidConsumeVoucherBeanList(Set<PrePaidConsumeVoucherBean> prePaidConsumeVoucherBeanList) {
        this.prePaidConsumeVoucherBeanList = prePaidConsumeVoucherBeanList;
    }

    public PrePaidConsumeVoucherBean getLastPrePaidConsumeVoucherBean() {

        PrePaidConsumeVoucherBean lastPrePaidConsumeVoucherBean = null;

        Long maxRequestTimestamp = 0l;

        for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : this.getPrePaidConsumeVoucherBeanList()) {

            if (prePaidConsumeVoucherBean.getRequestTimestamp() > maxRequestTimestamp) {

                lastPrePaidConsumeVoucherBean = prePaidConsumeVoucherBean;
                maxRequestTimestamp = prePaidConsumeVoucherBean.getRequestTimestamp();
            }
        }

        return lastPrePaidConsumeVoucherBean;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getSrcTransactionID() {

        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {

        this.srcTransactionID = srcTransactionID;
    }

    @Override
    public String getTransactionStatus() {
        return getFinalStatusType();
    }

    @Override
    public void setTransactionStatus(String transactionStatus) {
        setFinalStatusType(transactionStatus);
    }

    @Override
    public Double getAmount() {
        return getFinalAmount();
    }

    @Override
    public void setAmount(Double amount) {
        setFinalAmount(amount);
    }

    @Override
    public String getLastStatusEvent() {
        TransactionStatusBean transactionStatusBean = getLastTransactionStatus();

        if (transactionStatusBean != null) {
            return transactionStatusBean.getStatus();
        }

        return null;
    }

    @Override
    public String getLastStatusEventCode() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getLastStatusEventResult() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionInterfaceType getTransactionType() {
        return TransactionInterfaceType.TRANSACTION_PREPAID;
    }

    @Override
    public String getEncodedSecretKey() {
        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {
        this.encodedSecretKey = encodedSecretKey;
    }

    @Override
    public String getGroupAcquirer() {
        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {
        this.groupAcquirer = groupAcquirer;
    }

    public Set<PrePaidLoadLoyaltyCreditsBean> getPrePaidLoadLoyaltyCreditsBeanList() {
        return prePaidLoadLoyaltyCreditsBeanList;
    }

    public void setPrePaidLoadLoyaltyCreditsBeanList(Set<PrePaidLoadLoyaltyCreditsBean> prePaidLoadLoyaltyCreditsBeanList) {
        this.prePaidLoadLoyaltyCreditsBeanList = prePaidLoadLoyaltyCreditsBeanList;
    }

    public String getGFGElectronicInvoiceID() {
        return gfgElectronicInvoiceID;
    }

    public void setGFGElectronicInvoiceID(String gfgElectronicInvoiceID) {
        this.gfgElectronicInvoiceID = gfgElectronicInvoiceID;
    }
    
    public TransactionCategoryType getTransactionCategory() {
        if (transactionCategory == null) {
            return TransactionCategoryType.TRANSACTION_CUSTOMER;
        }
        
        return TransactionCategoryType.getValueOf(transactionCategory);
    }
    
    public void setTransactionCategory(TransactionCategoryType transactionCategory) {
        this.transactionCategory = transactionCategory.getValue();
    }

    public PaymentTokenPackageBean getPaymentTokenPackageBean() {
        return paymentTokenPackageBean;
    }

    public void setPaymentTokenPackageBean(PaymentTokenPackageBean paymentTokenPackageBean) {
        this.paymentTokenPackageBean = paymentTokenPackageBean;
    }

    public Set<TransactionAdditionalDataBean> getTransactionAdditionalDataBeanList() {
        return transactionAdditionalDataBeanList;
    }

    public void setTransactionAdditionalDataBeanList(Set<TransactionAdditionalDataBean> transactionAdditionalDataBeanList) {
        this.transactionAdditionalDataBeanList = transactionAdditionalDataBeanList;
    }

    @Override
    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Set<TransactionOperationBean> getTransactionOperationBeanData() {
        return transactionOperationBeanData;
    }

    public void setTransactionOperationBeanData(Set<TransactionOperationBean> transactionOperationBeanData) {
        this.transactionOperationBeanData = transactionOperationBeanData;
    }

}
