package com.techedge.mp.core.business.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.AdminRole;

@Entity
@Table(name = "ADMIN_ROLE")
@NamedQueries({ @NamedQuery(name = "AdminRoleBean.findAdminRoleByName", query = "select r from AdminRoleBean r where r.name = :name"),
        @NamedQuery(name = "AdminRoleBean.findAllAdminRoles", query = "select r from AdminRoleBean r") })
public class AdminRoleBean {
    public static final String FIND_BY_NAME = "AdminRoleBean.findAdminRoleByName";
    public static final String FIND_ALL     = "AdminRoleBean.findAllAdminRoles";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "name", nullable = true)
    private String             name;

    @ManyToMany(mappedBy = "roles")
    private Set<AdminBean>     admins       = new HashSet<AdminBean>(0);

    public AdminRoleBean() {}

    public AdminRoleBean(AdminRole adminRole) {
        this.name = adminRole.getName();
    }

    public AdminRole toAdminRole() {
        AdminRole adminRole = new AdminRole();
        adminRole.setName(this.name);
        return adminRole;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<AdminBean> getAdmins() {
        return admins;
    }

    public void setAdmins(Set<AdminBean> admins) {
        this.admins = admins;
    }

}
