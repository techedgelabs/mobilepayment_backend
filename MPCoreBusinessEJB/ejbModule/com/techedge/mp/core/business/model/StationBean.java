package com.techedge.mp.core.business.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.Station;

@Entity
@Table(name = "STATIONS")
@NamedQueries({ @NamedQuery(name = "StationBean.findByID", query = "select s from StationBean s where s.stationID = :stationID"),
        @NamedQuery(name = "StationBean.findActiveByID", query = "select s from StationBean s where s.stationID = :stationID and stationStatus != 0"),
        @NamedQuery(name = "StationBean.findByPrimaryKey", query = "select s from StationBean s where s.id = :id"),
        @NamedQuery(name = "StationBean.findByBeaconCode", query = "select s from StationBean s where s.beaconCode = :beaconCode"),
        @NamedQuery(name = "StationBean.findActiveByBeaconCode", query = "select s from StationBean s where s.beaconCode = :beaconCode and stationStatus != 0"),
        @NamedQuery(name = "StationBean.findAll", query = "select s from StationBean s"),
        @NamedQuery(name = "StationBean.countAllAppActive", query = "select count(s.id) from StationBean s where s.stationStatus = 2 and s.newAcquirerActive = 1"),
        @NamedQuery(name = "StationBean.countAllAppActiveBusiness", query = "select count(s.id) from StationBean s where s.stationStatus = 2 and s.newAcquirerActive = 1 and s.businessActive = 1"),
        @NamedQuery(name = "StationBean.countAllLoyaltyActive", query = "select count(s.id) from StationBean s where s.stationStatus = 2 and s.loyaltyActive = 1"),
        @NamedQuery(name = "StationBean.findAllAppActive", query = "select s from StationBean s where s.stationStatus = 2 and s.newAcquirerActive = 1"),
        @NamedQuery(name = "StationBean.findAllActive", query = "select s from StationBean s where stationStatus = 2") })
public class StationBean {

    public static final String FIND_BY_ID                = "StationBean.findByID";
    public static final String FIND_ACTIVE_BY_ID         = "StationBean.findActiveByID";
    public static final String FIND_BY_PRIMARY_KEY       = "StationBean.findByPrimaryKey";
    public static final String FIND_BY_BEACONCODE        = "StationBean.findByBeaconCode";
    public static final String FIND_ACTIVE_BY_BEACONCODE = "StationBean.findActiveByBeaconCode";
    public static final String FIND_ALL                  = "StationBean.findAll";
    public static final String FIND_ALL_ACTIVE           = "StationBean.findAllActive";
    public static final String FIND_ALL_APP_ACTIVE       = "StationBean.findAllAppActive";
    public static final String COUNT_ALL_APP_ACTIVE      = "StationBean.countAllAppActive";
    public static final String COUNT_ALL_APP_ACTIVE_BUSINESS = "StationBean.countAllAppActiveBusiness";
    public static final String COUNT_ALL_LOYALTY_ACTIVE  = "StationBean.countAllLoyaltyActive";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "station_id", nullable = true)
    private String             stationID;

    @Column(name = "beacon_code", nullable = true)
    private String             beaconCode;

    @Column(name = "address", nullable = true)
    private String             address;

    @Column(name = "city", nullable = true)
    private String             city;

    @Column(name = "country", nullable = true)
    private String             country;

    @Column(name = "province", nullable = true)
    private String             province;

    @Column(name = "latitude", nullable = true)
    private Double             latitude;

    @Column(name = "longitude", nullable = true)
    private Double             longitude;

    @Column(name = "oil_shop_login", nullable = true)
    private String             oilShopLogin;

    @Column(name = "oil_acquirer_id", nullable = true)
    private String             oilAcquirerID;

    @Column(name = "no_oil_shop_login", nullable = true)
    private String             noOilShopLogin;

    @Column(name = "no_oil_acquirer_id", nullable = true)
    private String             noOilAcquirerID;

    @Column(name = "status", nullable = true)
    private Integer            stationStatus;

    @ManyToMany(mappedBy = "stations")
    private Set<ManagerBean>   managers                  = new HashSet<ManagerBean>(0);

    @Column(name = "prepaid_active", nullable = true)
    private Boolean            prepaidActive;

    @Column(name = "postpaid_active", nullable = true)
    private Boolean            postpaidActive;

    @Column(name = "shop_active", nullable = true)
    private Boolean            shopActive;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "data_acquirer_id")
    private DataAcquirerBean   dataAcquirer;
    
    @Column(name = "new_acquirer_active", nullable = true)
    private Boolean            newAcquirerActive;
    
    @Column(name = "refueling_active", nullable = true)
    private Boolean            refuelingActive;
    
    @Column(name = "loyalty_active", nullable = true)
    private Boolean            loyaltyActive;
    
    @Column(name = "end_refuel_offset", nullable = true)
    private Long            endRefuelOffset;
    
    @Column(name = "end_refuel_timestamp", nullable = true)
    private Date            endRefuelTimestamp;

    @Column(name = "voucher_active", nullable = true)
    private Boolean            voucherActive;
    
    @Column(name = "business_active", nullable = true)
    private Boolean            businessActive;
    
    public StationBean() {}

    public StationBean(Station station) {

        this.stationID = station.getStationID();
        this.beaconCode = station.getBeaconCode();
        this.address = station.getAddress();
        this.city = station.getCity();
        this.country = station.getCountry();
        this.province = station.getProvince();
        this.latitude = station.getLatitude();
        this.longitude = station.getLongitude();
        this.oilShopLogin = station.getOilShopLogin();
        this.oilAcquirerID = station.getOilAcquirerID();
        this.noOilShopLogin = station.getNoOilShopLogin();
        this.noOilAcquirerID = station.getNoOilAcquirerID();
        this.stationStatus = station.getStationStatus();
        this.postpaidActive = station.getPostpaidActive();
        this.prepaidActive = station.getPrepaidActive();
        this.shopActive = station.getShopActive();
        if (station.getDataAcquirer() != null) {
            this.dataAcquirer = new DataAcquirerBean(station.getDataAcquirer());
        }
        this.newAcquirerActive = station.getNewAcquirerActive();
        this.refuelingActive = station.getRefuelingActive();
        this.loyaltyActive = station.getLoyaltyActive();
        this.voucherActive = station.getVoucherActive();
        this.businessActive = station.getBusinessActive();
    }

    public Station toStation() {

        Station station = new Station();
        station.setId(this.id);
        station.setStationID(this.stationID);
        station.setBeaconCode(this.beaconCode);
        station.setAddress(this.address);
        station.setCity(this.city);
        station.setCountry(this.country);
        station.setProvince(this.province);
        station.setLatitude(this.latitude);
        station.setLongitude(this.longitude);
        station.setOilShopLogin(this.oilShopLogin);
        station.setOilAcquirerID(this.oilAcquirerID);
        station.setNoOilShopLogin(this.noOilShopLogin);
        station.setNoOilAcquirerID(this.noOilAcquirerID);
        station.setStationStatus(this.stationStatus);
        station.setPostpaidActive(this.postpaidActive);
        station.setPrepaidActive(this.prepaidActive);
        station.setShopActive(this.shopActive);
        if (this.dataAcquirer != null) {
            station.setDataAcquirer(this.dataAcquirer.toDataAcquired());
        }
        station.setNewAcquirerActive(this.newAcquirerActive);
        station.setRefuelingActive(this.refuelingActive);
        station.setLoyaltyActive(this.loyaltyActive);
        station.setVoucherActive(this.voucherActive);
        station.setBusinessActive(this.businessActive);
        
        return station;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getBeaconCode() {
        return beaconCode;
    }

    public void setBeaconCode(String beaconCode) {
        this.beaconCode = beaconCode;
    }

    public String getAddress() {
        return address;
    }

    public String getFullAddress() {
        String fullAddress = null;

        if (address != null) {
            fullAddress = address;

            if (city != null) {
                fullAddress += ", " + city;
            }

            if (province != null) {
                fullAddress += " (" + province + ")";
            }

            if (country != null) {
                fullAddress += ", " + country;
            }
        }

        return fullAddress;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getOilShopLogin() {
        return oilShopLogin;
    }

    public void setOilShopLogin(String oilShopLogin) {
        this.oilShopLogin = oilShopLogin;
    }

    public String getOilAcquirerID() {
        return oilAcquirerID;
    }

    public void setOilAcquirerID(String oilAcquirerID) {
        this.oilAcquirerID = oilAcquirerID;
    }

    public String getNoOilShopLogin() {
        return noOilShopLogin;
    }

    public void setNoOilShopLogin(String noOilShopLogin) {
        this.noOilShopLogin = noOilShopLogin;
    }

    public String getNoOilAcquirerID() {
        return noOilAcquirerID;
    }

    public void setNoOilAcquirerID(String noOilAcquirerID) {
        this.noOilAcquirerID = noOilAcquirerID;
    }

    public Integer getStationStatus() {
        return stationStatus;
    }

    public void setStationStatus(Integer stationStatus) {
        this.stationStatus = stationStatus;
    }

    public Set<ManagerBean> getManagers() {
        return managers;
    }

    public void setManagers(Set<ManagerBean> managers) {
        this.managers = managers;
    }

    public Boolean getPrepaidActive() {
        return prepaidActive;
    }

    public void setPrepaidActive(Boolean prepaidActive) {
        this.prepaidActive = prepaidActive;
    }

    public Boolean getPostpaidActive() {
        return postpaidActive;
    }

    public void setPostpaidActive(Boolean postpaidActive) {
        this.postpaidActive = postpaidActive;
    }

    public Boolean getShopActive() {
        return shopActive;
    }

    public void setShopActive(Boolean shopActive) {
        this.shopActive = shopActive;
    }

    public DataAcquirerBean getDataAcquirer() {
        return dataAcquirer;
    }

    public void setDataAcquirer(DataAcquirerBean dataAcquire) {
        this.dataAcquirer = dataAcquire;
    }

    public Boolean getNewAcquirerActive() {
        if (newAcquirerActive == null) {
            return Boolean.TRUE;
        }
        return newAcquirerActive;
    }

    public void setNewAcquirerActive(Boolean newAcquirerActive) {
        this.newAcquirerActive = newAcquirerActive;
    }

    public Boolean getRefuelingActive() {
        if (refuelingActive == null) {
            return Boolean.TRUE;
        }
        return refuelingActive;
    }

    public void setRefuelingActive(Boolean refuelingActive) {
        this.refuelingActive = refuelingActive;
    }

    public Boolean getLoyaltyActive() {
        if (loyaltyActive == null) {
            return Boolean.TRUE;
        }
        return loyaltyActive;
    }

    public void setLoyaltyActive(Boolean loyaltyActive) {
        this.loyaltyActive = loyaltyActive;
    }
    
    public Long getEndRefuelOffset() {
        return endRefuelOffset;
    }

    public void setEndRefuelOffset(Long endRefuelOffset) {
        this.endRefuelOffset = endRefuelOffset;
    }

    public Date getEndRefuelTimestamp() {
        return endRefuelTimestamp;
    }

    public void setEndRefuelTimestamp(Date endRefuelTimestamp) {
        this.endRefuelTimestamp = endRefuelTimestamp;
    }
    
    public Boolean getVoucherActive() {
        if (voucherActive == null) {
            return Boolean.FALSE;
        }
        return voucherActive;
    }
    
    public void setVoucherActive(Boolean voucherActive) {
        this.voucherActive = voucherActive;
    }

    public Boolean getBusinessActive() {
        if (businessActive == null) {
            return Boolean.FALSE;
        }
        return businessActive;
    }
    
    public void setBusinessActive(Boolean businessActive) {
        this.businessActive = businessActive;
    }
}
