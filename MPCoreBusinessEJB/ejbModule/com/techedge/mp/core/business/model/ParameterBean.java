package com.techedge.mp.core.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="PARAMS")
@NamedQueries({
	@NamedQuery(name="ParameterBean.findByName", query="select p from ParameterBean p where p.name = :name"),
	@NamedQuery(name="ParameterBean.findAll", query="select p from ParameterBean p")
})
public class ParameterBean {

	public static final String FIND_BY_NAME = "ParameterBean.findByName";
	public static final String FIND_ALL     = "ParameterBean.findAll";
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	
	@Column(name="name", nullable=true)
	private String name;
	
	@Column(name="value", nullable=true)
	private String value;
		
	public ParameterBean() {
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
