package com.techedge.mp.core.business.model.dwh;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.CategoryEarnDataDetail;
import com.techedge.mp.core.business.interfaces.PartnerDetailInfoData;

@Entity
@Table(name = "DWH_CATEGORY_EARN")
@NamedQueries({ @NamedQuery(name = "DWHCategoryEarnDetailBean.findById", query = "select cd from DWHCategoryEarnDetailBean cd where cd.id = :id"),
        @NamedQuery(name = "DWHCategoryEarnDetailBean.findAll", query = "select cd from DWHCategoryEarnDetailBean cd") })
public class DWHCategoryEarnDetailBean {

    public static final String FIND_By_ID = "DWHCategoryEarnDetailBean.findById";
    public static final String FIND_ALL   = "DWHCategoryEarnDetailBean.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "creation_timestamp")
    private Date               creationTimestamp;

    @Column(name = "category_id")
    private Integer             categoryId;

    @Column(name = "category")
    private String             category;

    @Lob @Basic(fetch=FetchType.LAZY)
    @Column(name = "category_image_url")
    private String             categoryImageUrl;

    public DWHCategoryEarnDetailBean() {}

    public DWHCategoryEarnDetailBean(CategoryEarnDataDetail categoryDataDetail) {
        this.categoryId = categoryDataDetail.getCategoryId();
        this.category = categoryDataDetail.getCategory();
        this.categoryImageUrl = categoryDataDetail.getCategoryImageUrl();
    }

    public CategoryEarnDataDetail toCategoryDataDetail() {
        CategoryEarnDataDetail categoryDataDetail = new CategoryEarnDataDetail();
        categoryDataDetail.setCategoryId(this.categoryId);
        categoryDataDetail.setCategory(this.category);
        categoryDataDetail.setCategoryImageUrl(this.categoryImageUrl);

        return categoryDataDetail;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryImageUrl() {
        return categoryImageUrl;
    }

    public void setCategoryImageUrl(String categoryImageUrl) {
        this.categoryImageUrl = categoryImageUrl;
    }

    

}
