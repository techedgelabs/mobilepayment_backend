package com.techedge.mp.core.business.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucherDetail;

@Entity
@Table(name = "PREPAIDCONSUMEVOUCHERDETAIL")
@NamedQueries({
    @NamedQuery(name = "PrePaidConsumeVoucherDetailBean.findPrePaidConsumeVoucherDetailBeanById", query = "select v from PrePaidConsumeVoucherDetailBean v where v.id = :id ")
})
public class PrePaidConsumeVoucherDetailBean {
    
    public static final String FIND_BY_ID = "PrePaidConsumeVoucherDetailBean.findPrePaidConsumeVoucherDetailBeanById";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                       id;

    @Column(name = "voucherCode", nullable = true)
    private String                     voucherCode;

    @Column(name = "voucherStatus", nullable = true)
    private String                     voucherStatus;

    @Column(name = "voucherType", nullable = true)
    private String                     voucherType;

    @Column(name = "voucherValue", nullable = true)
    private Double                     voucherValue;

    @Column(name = "initialValue", nullable = true)
    private Double                     initialValue;

    @Column(name = "consumedValue", nullable = true)
    private Double                     consumedValue;

    @Column(name = "voucherBalanceDue", nullable = true)
    private Double                     voucherBalanceDue;

    @Column(name = "expirationDate", nullable = true)
    private Date                       expirationDate;

    @Column(name = "promoCode", nullable = true)
    private String                     promoCode;

    @Column(name = "promoDescription", nullable = true)
    private String                     promoDescription;

    @Column(name = "promoDoc", nullable = true)
    private String                     promoDoc;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "prePaidConsumeVoucherBean", nullable = false)
    private PrePaidConsumeVoucherBean prePaidConsumeVoucherBean;

    public PrePaidConsumeVoucherDetailBean() {}

    public PrePaidConsumeVoucherDetailBean(PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail) {

        this.id = prePaidConsumeVoucherDetail.getId();
        this.voucherCode = prePaidConsumeVoucherDetail.getVoucherCode();
        this.voucherStatus = prePaidConsumeVoucherDetail.getVoucherStatus();
        this.voucherType = prePaidConsumeVoucherDetail.getVoucherType();
        this.voucherValue = prePaidConsumeVoucherDetail.getVoucherValue();
        this.initialValue = prePaidConsumeVoucherDetail.getInitialValue();
        this.consumedValue = prePaidConsumeVoucherDetail.getConsumedValue();
        this.voucherBalanceDue = prePaidConsumeVoucherDetail.getVoucherBalanceDue();
        this.expirationDate = prePaidConsumeVoucherDetail.getExpirationDate();
        this.promoCode = prePaidConsumeVoucherDetail.getPromoCode();
        this.promoDescription = prePaidConsumeVoucherDetail.getPromoDescription();
        this.promoDoc = prePaidConsumeVoucherDetail.getPromoDoc();
    }

    public PrePaidConsumeVoucherDetailBean(PrePaidConsumeVoucherDetailHistoryBean prePaidConsumeVoucherDetailBean) {

        this.voucherCode = prePaidConsumeVoucherDetailBean.getVoucherCode();
        this.voucherStatus = prePaidConsumeVoucherDetailBean.getVoucherStatus();
        this.voucherType = prePaidConsumeVoucherDetailBean.getVoucherType();
        this.voucherValue = prePaidConsumeVoucherDetailBean.getVoucherValue();
        this.initialValue = prePaidConsumeVoucherDetailBean.getInitialValue();
        this.consumedValue = prePaidConsumeVoucherDetailBean.getConsumedValue();
        this.voucherBalanceDue = prePaidConsumeVoucherDetailBean.getVoucherBalanceDue();
        this.expirationDate = prePaidConsumeVoucherDetailBean.getExpirationDate();
        this.promoCode = prePaidConsumeVoucherDetailBean.getPromoCode();
        this.promoDescription = prePaidConsumeVoucherDetailBean.getPromoDescription();
        this.promoDoc = prePaidConsumeVoucherDetailBean.getPromoDoc();
    }
    
    public PrePaidConsumeVoucherDetail toPrePaidConsumeVoucherDetail() {

        PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail = new PrePaidConsumeVoucherDetail();
        prePaidConsumeVoucherDetail.setId(this.id);
        prePaidConsumeVoucherDetail.setVoucherCode(this.voucherCode);
        prePaidConsumeVoucherDetail.setVoucherStatus(this.voucherStatus);
        prePaidConsumeVoucherDetail.setVoucherType(this.voucherType);
        prePaidConsumeVoucherDetail.setVoucherValue(this.voucherValue);
        prePaidConsumeVoucherDetail.setInitialValue(this.initialValue);
        prePaidConsumeVoucherDetail.setConsumedValue(this.consumedValue);
        prePaidConsumeVoucherDetail.setVoucherBalanceDue(this.voucherBalanceDue);
        prePaidConsumeVoucherDetail.setExpirationDate(this.expirationDate);
        prePaidConsumeVoucherDetail.setPromoCode(this.promoCode);
        prePaidConsumeVoucherDetail.setPromoDescription(this.promoDescription);
        prePaidConsumeVoucherDetail.setPromoDoc(this.promoDoc);
        return prePaidConsumeVoucherDetail;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public Double getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(Double voucherValue) {
        this.voucherValue = voucherValue;
    }

    public Double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Double initialValue) {
        this.initialValue = initialValue;
    }

    public Double getConsumedValue() {
        return consumedValue;
    }

    public void setConsumedValue(Double consumedValue) {
        this.consumedValue = consumedValue;
    }

    public Double getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Double voucherBalanceDue) {
        this.voucherBalanceDue = voucherBalanceDue;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {
        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {
        this.promoDoc = promoDoc;
    }

    public PrePaidConsumeVoucherBean getPrePaidConsumeVoucherBean() {
        return prePaidConsumeVoucherBean;
    }

    public void setPrePaidConsumeVoucherBean(PrePaidConsumeVoucherBean prePaidConsumeVoucherBean) {
        this.prePaidConsumeVoucherBean = prePaidConsumeVoucherBean;
    }

}
