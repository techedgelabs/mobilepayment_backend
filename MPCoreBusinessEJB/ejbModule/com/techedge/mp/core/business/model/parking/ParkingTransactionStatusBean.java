package com.techedge.mp.core.business.model.parking;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.TransactionStatus;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransactionStatus;

@Entity
@Table(name = "PARKING_TRANSACTION_STATUS")
//@NamedQueries({ @NamedQuery(name = "TransactionStatusBean.findTransactionStatusByTransaction", query = "select t from TransactionStatusBean t where t.transactionBean = :transactionBean") })
public class ParkingTransactionStatusBean {

    //    public static final String FIND_BY_TRANSACTION               = "TransactionStatusBean.findTransactionStatusByTransaction";
    //    public static final String FIND_BY_TRANSACTION_AND_STATUS_ID = "TransactionStatusBean.findTransactionStatusByTransactionAndStatusID";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                   id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parking_transaction_id", nullable = false)
    private ParkingTransactionBean parkingTransactionBean;

    @Column(name = "sequence_id", nullable = true)
    private Integer                sequenceID;

    @Column(name = "timestamp", nullable = true)
    private Timestamp              timestamp;

    @Column(name = "status", nullable = true)
    private String                 status;

    @Column(name = "sub_status", nullable = true)
    private String                 subStatus;

    @Column(name = "sub_status_description", nullable = true)
    private String                 subStatusDescription;

    @Column(name = "request_id", nullable = true)
    private String                 requestID;

    @Column(name = "level", nullable = true)
    private String                 level;

    public ParkingTransactionStatusBean() {}

    public ParkingTransactionStatusBean(TransactionStatus transactionStatus) {

        this.id = transactionStatus.getId();
        this.sequenceID = transactionStatus.getSequenceID();
        this.timestamp = transactionStatus.getTimestamp();
        this.status = transactionStatus.getStatus();
        this.subStatus = transactionStatus.getStatus();
        this.subStatusDescription = transactionStatus.getSubStatusDescription();
        this.requestID = transactionStatus.getRequestID();
        this.level = transactionStatus.getLevel();

    }

    public ParkingTransactionStatus toParkingTransactionStatus() {

        ParkingTransactionStatus parkingTransactionStatus = new ParkingTransactionStatus();
        parkingTransactionStatus.setId(this.id);
        parkingTransactionStatus.setSequenceID(this.sequenceID);
        parkingTransactionStatus.setTimestamp(this.timestamp);
        parkingTransactionStatus.setStatus(this.status);
        parkingTransactionStatus.setSubStatus(this.subStatus);
        parkingTransactionStatus.setSubStatusDescription(this.subStatusDescription);
        parkingTransactionStatus.setRequestID(this.requestID);
        parkingTransactionStatus.setLevel(this.level);
        return parkingTransactionStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ParkingTransactionBean getParkingTransactionBean() {
        return parkingTransactionBean;
    }

    public void setParkingTransactionBean(ParkingTransactionBean parkingTransactionBean) {
        this.parkingTransactionBean = parkingTransactionBean;
    }

    public Integer getSequenceID() {
        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {
        this.sequenceID = sequenceID;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubStatus() {
        return subStatus;
    }

    public String getSubStatusDescription() {
        return subStatusDescription;
    }

    public void setSubStatusDescription(String subStatusDescription) {
        this.subStatusDescription = subStatusDescription;
    }

    public void setSubStatus(String subStatus) {
        this.subStatus = subStatus;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

}
