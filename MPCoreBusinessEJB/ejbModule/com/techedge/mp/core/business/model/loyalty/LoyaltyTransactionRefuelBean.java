package com.techedge.mp.core.business.model.loyalty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.loyalty.ProductIdType;
import com.techedge.mp.core.business.interfaces.loyalty.RefuelModeType;

@Entity
@Table(name = "LOYALTY_TRANSACTION_REFUEL")
@NamedQueries({ @NamedQuery(name = "LoyaltyTransactionRefuelBean.findByLoyaltyTransaction", query = "select r from LoyaltyTransactionRefuelBean r "
        + "where r.loyaltyTransactionBean = :loyaltyTransactionBean") })
public class LoyaltyTransactionRefuelBean {

    public static final String     FIND_BY_LOYALTY_TRANSACTION = "LoyaltyTransactionRefuelBean.findByLoyaltyTransaction";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long                   id;

    @Column(name = "refuel_mode", nullable = false)
    private String                 refuelMode;

    @Column(name = "pump_number")
    private String                 pumpNumber;

    @Column(name = "product_id", nullable = false)
    private String                 productID;

    @Column(name = "product_description")
    private String                 productDescription;

    @Column(name = "fuel_quantity", nullable = false)
    private Double                 fuelQuantity;

    @Column(name = "amount", nullable = false)
    private Double                 amount;

    @Column(name = "credits", nullable = false)
    private Integer                credits;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loyalty_transaction_id", nullable = false)
    private LoyaltyTransactionBean loyaltyTransactionBean;

    public LoyaltyTransactionRefuelBean() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RefuelModeType getRefuelMode() {
        return RefuelModeType.getRefuelMode(refuelMode);
    }

    public void setRefuelMode(RefuelModeType refuelMode) {
        this.refuelMode = refuelMode.getCode();
    }

    public String getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(String pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public ProductIdType getProductID() {
        return ProductIdType.getProduct(productID);
    }

    public void setProductID(ProductIdType productID) {
        this.productID = productID.getCode();
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public LoyaltyTransactionBean getLoyaltyTransactionBean() {
        return loyaltyTransactionBean;
    }

    public void setLoyaltyTransactionBean(LoyaltyTransactionBean loyaltyTransactionBean) {
        this.loyaltyTransactionBean = loyaltyTransactionBean;
    }

}
