package com.techedge.mp.core.business.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.techedge.mp.core.business.interfaces.UserSocialData;

@Entity
@Table(name = "USER_SOCIAL_DATA")
@NamedQueries({ @NamedQuery(name = "UserSocialDataBean.findSocialUserByUUIDandStatus", query = "select usdb from UserSocialDataBean usdb where usdb.uuid = :uuid and usdb.provider = :provider and usdb.userBean.userStatus != :userStatus") })
//,
// @NamedQuery(name = "VoucherBean.findVoucherByVoucherCode", query =
// "select v from VoucherBean v where v.code = :code"),
// @NamedQuery(name = "VoucherBean.findActiveVoucherByUser", query =
// "select v from VoucherBean v where v.userBean = :userBean and status = 'V'")
// })
public class UserSocialDataBean {

    public static final String FIND_SOCIAL_USER_BY_UUID_AND_STATUS = "UserSocialDataBean.findSocialUserByUUIDandStatus";
    // public static final String FIND_BY_CODE =
    // "VoucherBean.findVoucherByVoucherCode";
    // public static final String FIND_ACTIVE_BY_USER =
    // "VoucherBean.findActiveVoucherByUser";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long               id;

    @Column(name = "provider", nullable = false, length = 40)
    private String             provider;

    @Column(name = "uuid", nullable = false, length = 50)
    private String             uuid;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "user_id")
    private UserBean           userBean;

    public UserSocialDataBean() {}

    public UserSocialDataBean(UserSocialData userSocialData) {
        this.id = userSocialData.getId();
        this.provider = userSocialData.getProvider();

    }

    public UserSocialData toUserSocialData() {
        UserSocialData userSocialData = new UserSocialData();
        userSocialData.setId(this.id);
        userSocialData.setProvider(this.provider);
        userSocialData.setUuid(this.uuid);

        return userSocialData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

}
