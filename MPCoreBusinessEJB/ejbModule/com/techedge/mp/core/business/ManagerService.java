package com.techedge.mp.core.business;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.techedge.mp.core.actions.manager.ManagerAuthenticationAction;
import com.techedge.mp.core.actions.manager.ManagerGetTransactionDetailAction;
import com.techedge.mp.core.actions.manager.ManagerLogoutAction;
import com.techedge.mp.core.actions.manager.ManagerRescuePasswordAction;
import com.techedge.mp.core.actions.manager.ManagerRetrieveTransactionsAction;
import com.techedge.mp.core.actions.manager.ManagerRetrieveVouchersAction;
import com.techedge.mp.core.actions.manager.ManagerStationDetailsAction;
import com.techedge.mp.core.actions.manager.ManagerUpdatePasswordAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ManagerAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ManagerRetrieveTransactionsResponse;
import com.techedge.mp.core.business.interfaces.ManagerRetrieveVouchersResponse;
import com.techedge.mp.core.business.interfaces.ManagerStationDetailsResponse;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;

/**
 * Session Bean implementation class ManagerService
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ManagerService implements ManagerServiceRemote, ManagerServiceLocal {

    private static final String               PARAM_EMAIL_SENDER_ADDRESS             = "EMAIL_SENDER_ADDRESS";
    private static final String               PARAM_SMTP_HOST                        = "SMTP_HOST";
    private static final String               PARAM_SMTP_PORT                        = "SMTP_PORT";
    //private static final String PARAM_ACTIVATION_LINK = "ACTIVATION_LINK";
    private static final String               PARAM_PASSWORD_RECOVERY_LINK           = "PASSWORD_RECOVERY_LINK";
    private static final String               PARAM_TICKET_EXPIRY_TIME               = "TICKET_EXPIRY_TIME";              // minutes
    //private static final String PARAM_PASSWORD_HISTORY_LENGTH = "PASSWORD_HISTORY_LENGTH";
    private static final String               PARAM_RESCUE_PASSWORD_EXPIRATION_DELAY = "RESCUE_PASSWORD_EXPIRATION_DELAY"; // minutes
    private static final String               PARAM_MAX_PENDING_INTERVAL             = "MAX_PENDING_INTERVAL";
    private static final String               PARAM_LOGIN_ATTEMPTS_THRESHOLD         = "LOGIN_ATTEMPTS_THRESHOLD";
    private static final String               PARAM_LOGIN_LOCK_EXPIRY_TIME           = "LOGIN_LOCK_EXPIRY_TIME";          // minutes
    private final static String               PARAM_TRANSACTION_MANAGER_INTERVAL     = "TRANSACTION_MANAGER_INTERVAL";
    private final static String               PARAM_PROXY_HOST                       = "PROXY_HOST";
    private final static String               PARAM_PROXY_PORT                       = "PROXY_PORT";
    private final static String               PARAM_PROXY_NO_HOSTS                   = "PROXY_NO_HOSTS";

    @EJB
    private ManagerAuthenticationAction       managerAuthenticationAction;

    @EJB
    private ManagerLogoutAction               managerLogoutAction;

    @EJB
    private ManagerUpdatePasswordAction       managerUpdatePasswordAction;

    @EJB
    private ManagerRescuePasswordAction       managerRescuePasswordAction;

    @EJB
    private ManagerStationDetailsAction       managerStationDetailsAction;

    @EJB
    private ManagerRetrieveTransactionsAction managerRetrieveTransactionsAction;

    @EJB
    private ManagerGetTransactionDetailAction managerGetTransactionDetailAction;

    @EJB
    private ManagerRetrieveVouchersAction     managerRetrieveVouchersAction;

    private ParametersService                 parametersService                      = null;
    private LoggerService                     loggerService                          = null;
    private EmailSenderRemote                 emailSender                            = null;
    private ForecourtInfoServiceRemote        forecourtInfoService                   = null;

    /**
     * Default constructor.
     */
    public ManagerService() throws EJBException {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get EmailSender object");

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ParametersService object");

            throw new EJBException(e);
        }

        try {
            this.forecourtInfoService = EJBHomeCache.getInstance().getForecourtInfoService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ForecourtService object");

            throw new EJBException(e);
        }
    }

    @Override
    public ManagerAuthenticationResponse authentication(String username, String password, String requestId, String deviceId, String deviceName) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("username", username));
        inputParameters.add(new Pair<String, String>("password", password));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deviceId", deviceId));
        inputParameters.add(new Pair<String, String>("deviceName", deviceName));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "ManagerAuthentication", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        ManagerAuthenticationResponse authenticationResponse = null;
        try {
            Integer maxPendingInterval = Integer.valueOf(this.parametersService.getParamValue(ManagerService.PARAM_MAX_PENDING_INTERVAL));
            Integer ticketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(ManagerService.PARAM_TICKET_EXPIRY_TIME));
            Integer loginAttemptsLimit = Integer.valueOf(this.parametersService.getParamValue(ManagerService.PARAM_LOGIN_ATTEMPTS_THRESHOLD));
            Integer loginLockExpiryTime = Integer.valueOf(this.parametersService.getParamValue(ManagerService.PARAM_LOGIN_LOCK_EXPIRY_TIME));

            authenticationResponse = managerAuthenticationAction.execute(username, password, requestId, deviceId, deviceName, maxPendingInterval, ticketExpiryTime,
                    loginAttemptsLimit, loginLockExpiryTime);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            authenticationResponse = new ManagerAuthenticationResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            authenticationResponse = new ManagerAuthenticationResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", authenticationResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "ManagerAuthentication", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return authenticationResponse;
    }

    @Override
    public String logout(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "logout", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = managerLogoutAction.execute(ticketId, requestId);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "logout", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String updatePassword(String ticketId, String requestId, String oldPassword, String newPassword) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("oldPassword", oldPassword));
        inputParameters.add(new Pair<String, String>("newPassword", newPassword));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePassword", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = managerUpdatePasswordAction.execute(ticketId, requestId, oldPassword, newPassword);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePassword", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String rescuePassword(String ticketId, String requestId, String email) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("mail", email));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "rescuePassword", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String passwordRecoveryLink = this.parametersService.getParamValue(ManagerService.PARAM_PASSWORD_RECOVERY_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(ManagerService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(ManagerService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(ManagerService.PARAM_SMTP_PORT);
            Integer rescuePasswordExpirationDelay = Integer.valueOf(this.parametersService.getParamValue(ManagerService.PARAM_RESCUE_PASSWORD_EXPIRATION_DELAY));
            String proxyHost = this.parametersService.getParamValue(ManagerService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(ManagerService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(ManagerService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = managerRescuePasswordAction.execute(ticketId, requestId, email, this.emailSender, rescuePasswordExpirationDelay, passwordRecoveryLink);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "rescuePassword", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public ManagerStationDetailsResponse stationDetails(String ticketId, String requestId, String stationId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("stationId", stationId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "stationDetails", ticketId, "opening", ActivityLog.createLogMessage(inputParameters));

        ManagerStationDetailsResponse stationDetailsResponse = null;
        try {
            stationDetailsResponse = managerStationDetailsAction.execute(ticketId, requestId, stationId, forecourtInfoService);
        }
        catch (EJBException ex) {
            stationDetailsResponse = new ManagerStationDetailsResponse();
            stationDetailsResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", stationDetailsResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "stationDetails", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return stationDetailsResponse;
    }

    @Override
    public ManagerRetrieveTransactionsResponse retrieveTransactions(String ticketId, String requestId, String stationID, Integer pumpMaxTransactions) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpMaxTransactions", (pumpMaxTransactions != null) ? pumpMaxTransactions.toString() : "null"));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTransactions", ticketId, "opening", ActivityLog.createLogMessage(inputParameters));

        ManagerRetrieveTransactionsResponse managerRetrieveTransactionsResponse = new ManagerRetrieveTransactionsResponse();

        String parameterTransactionsInterval = null;
        Integer transactionsInterval = new Integer(86400);

        try {
            try {
                parameterTransactionsInterval = parametersService.getParamValue(ManagerService.PARAM_TRANSACTION_MANAGER_INTERVAL);
                if (parameterTransactionsInterval != null && !parameterTransactionsInterval.equals("")) {
                    transactionsInterval = new Integer(parameterTransactionsInterval);
                }
            }
            catch (Exception e) {
                transactionsInterval = new Integer(86400);
            }

            managerRetrieveTransactionsResponse = managerRetrieveTransactionsAction.execute(ticketId, requestId, stationID, pumpMaxTransactions, transactionsInterval,
                    forecourtInfoService);
        }
        catch (EJBException ex) {

            managerRetrieveTransactionsResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", managerRetrieveTransactionsResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTransactions", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return managerRetrieveTransactionsResponse;
    }

    @Override
    public PostPaidGetTransactionDetailResponse getTransactionDetail(String requestID, String ticketID, String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", requestID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionDetail", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidGetTransactionDetailResponse popPaidGetTransactionDetailResponse = null;

        try {
            popPaidGetTransactionDetailResponse = managerGetTransactionDetailAction.execute(requestID, ticketID, mpTransactionID);
        }
        catch (EJBException ex) {
            popPaidGetTransactionDetailResponse = new PostPaidGetTransactionDetailResponse();
            popPaidGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", popPaidGetTransactionDetailResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionDetail", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return popPaidGetTransactionDetailResponse;
    }

    @Override
    public ManagerRetrieveVouchersResponse retrieveVouchers(String ticketId, String requestId, String stationID, Date startDate, Date endDate) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("startDate", (startDate != null) ? startDate.toString() : "null"));
        inputParameters.add(new Pair<String, String>("endDate", (endDate != null) ? endDate.toString() : "null"));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveVouchers", ticketId, "opening", ActivityLog.createLogMessage(inputParameters));

        ManagerRetrieveVouchersResponse managerRetrieveVouchersResponse = new ManagerRetrieveVouchersResponse();

        try {
            managerRetrieveVouchersResponse = managerRetrieveVouchersAction.execute(ticketId, requestId, stationID, startDate, endDate, forecourtInfoService);
        }
        catch (EJBException ex) {

            managerRetrieveVouchersResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", managerRetrieveVouchersResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveVouchers", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return managerRetrieveVouchersResponse;
    }

}
