package com.techedge.mp.core.business;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.HomePartner;
import com.techedge.mp.core.business.interfaces.ParamInfo;
import com.techedge.mp.core.business.model.HomePartnerBean;
import com.techedge.mp.core.business.model.ParameterBean;
import com.techedge.mp.core.business.utilities.QueryRepository;


@Singleton
@LocalBean
@TransactionManagement( TransactionManagementType.BEAN )
public class ParametersService implements ParametersServiceRemote, ParametersServiceLocal {

	private final static String UNIT_NAME = "CrudPU";
	
	@Resource
    private EJBContext context;
	
    @PersistenceContext(unitName = UNIT_NAME)
    private EntityManager em;
    
    
    private Hashtable<String, String> parameters = new Hashtable<String, String>(0);
    
    private List<HomePartner> homePartnerList = new ArrayList<HomePartner>(0);
    
	
    public ParametersService() {
    }
    
    
    @SuppressWarnings("unchecked")
    @Override
    public List<ParamInfo> refreshParameters() {
    	
    	System.out.println("refreshParameters");
    	
    	// Svuota l'hashmap
    	this.parameters.clear();
    	
    	ArrayList<ParamInfo> paramInfoList = new ArrayList<ParamInfo>();
    	
    	// Ricarica i parametri nell'hashmap leggendo dal database
    	Query query = em.createNamedQuery( "ParameterBean.findAll" );
		
		List<ParameterBean> resultList = query.getResultList();
		
		for( ParameterBean parameterBean : resultList) {
			
			String name  = parameterBean.getName();
			String value = parameterBean.getValue() != null ? parameterBean.getValue() : "";
			
			this.parameters.put(name, value);
			
			ParamInfo paramInfo = new ParamInfo();
			paramInfo.setKey(name);
			paramInfo.setValue(value);
			paramInfoList.add(paramInfo);
		}
		
		return paramInfoList;
    }
    
    
    @PostConstruct
    protected void initSingleton() {
       System.out.println("Init singleton");
       this.refreshParameters();
       this.refreshHomePartnerList();
    }
    

	@Override
	public String getParamValue(String paramName) throws ParameterNotFoundException {
		
		//System.out.println("getParamValue " + paramName);
		
		// Se il valore in cache del patametro TS_UPDATE � diverso da quello contenuto sul db
		// � necessario effettuare un refresh dei parametri della cache
		
		// TODO commentato per questioni di performances
		/*
		ParameterBean parameterBeanTSUpdate = QueryRepository.findSingleParameterBeanByName(em, "TS_UPDATE");
		String TSUpdateDB     = parameterBeanTSUpdate.getValue();
		String TSUpdateCached = this.parameters.get("TS_UPDATE");
		*/
		
		//System.out.println("TSUpdateDB:"     + TSUpdateDB);
		//System.out.println("TSUpdateCached:" + TSUpdateCached);
		
		/*
		if ( !TSUpdateDB.equals(TSUpdateCached) ) {
			System.out.println("Refreshing parameters cache");
			refreshParameters();
		}
		*/
		
		if ( paramName.equals("SERVER_NAME") ) {
			
			String serverNameType = this.parameters.get("SERVER_NAME_TYPE");
			
			if ( serverNameType == null ) {
				return System.getProperty("jboss.server.name");
			}
			else {
				
				if ( serverNameType.equals("SERVER_NAME") ) {
					return System.getProperty("jboss.server.name");
				}
				if ( serverNameType.equals("SERVER_IP") ) {
					return System.getProperty("jboss.bind.address");
				}
				if ( serverNameType.equals("LOCALHOST") ) {
					return "localhost";
				}
				
				return System.getProperty("jboss.server.name");
			}
		}
		else {
		
			String paramValue = this.parameters.get(paramName);
			
			if ( paramValue == null ) {
				throw new ParameterNotFoundException("Parameter " + paramName + " not found", paramName);
			}
			
			return paramValue;
		}
	}
	
	@Override
	public String getParamValueNoCache(String paramName) throws ParameterNotFoundException {
	    String paramValue = null;
	    List<ParameterBean> parameterBeanList = QueryRepository.findParameterBeanByName(em, paramName);
	    if (parameterBeanList != null &&  parameterBeanList.size() > 0) {
	        paramValue = parameterBeanList.get(0).getValue();
	    }
	    
	    return paramValue;
	}
	
	
	public List<HomePartner> refreshHomePartnerList() {
	    
	    System.out.println("refreshHomePartnerList");
        
        // Svuota la lista
        this.homePartnerList.clear();
        
        ArrayList<HomePartner> homePartnerList = new ArrayList<HomePartner>();
        
        // Ricarica i parametri nell'hashmap leggendo dal database
        Query query = em.createNamedQuery( "HomePartnerBean.findAll" );
        
        List<HomePartnerBean> resultList = query.getResultList();
        
        for( HomePartnerBean homePartnerBean : resultList) {
            
            Integer sequence = homePartnerBean.getSequence();
            String  name     = homePartnerBean.getName();
            
            HomePartner homePartner = new HomePartner();
            homePartner.setSequence(sequence);
            homePartner.setName(name);
            
            this.homePartnerList.add(homePartner);
        }
        
        return homePartnerList;
	}
	
	public List<HomePartner> getHomePartnerList() {
	    
	    return homePartnerList;
	}

}
