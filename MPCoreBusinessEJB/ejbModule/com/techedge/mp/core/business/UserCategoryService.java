package com.techedge.mp.core.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserTypeBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

/**
 * Session Bean implementation class UserCategoryService
 */
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserCategoryService implements UserCategoryServiceLocal {

    private final static String                 UNIT_NAME = "CrudPU";
    private Map<Pair<String, Integer>, Boolean> mappa     = new HashMap<Pair<String, Integer>, Boolean>(0);

    @Resource
    private EJBContext                          context;

    @PersistenceContext(unitName = UNIT_NAME)
    private EntityManager                       em;

    public UserCategoryService() {}

    @PostConstruct
    protected void initSingleton() {
        System.out.println("Init UserCategoryService");
        init();
    }

    @Override
    public void refreshData() {
        System.out.println("REFRESH DATA");
        init();
    }

    private void init() {
        UserTransaction userTransaction = context.getUserTransaction();
        try {
            userTransaction.begin();
            List<UserCategoryBean> list = QueryRepository.findUserCategoryAllName(em);
            for (UserCategoryBean item : list) {
                for (UserTypeBean element : item.getUserTypes()) {
                    Pair<String, Integer> pair = new Pair<String, Integer>(item.getName(), element.getCode());
                    mappa.put(pair, true);
                }
            }
            userTransaction.commit();
        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public Boolean isUserTypeInUserCategory(Integer userType, String userCategory) {
        Pair<String, Integer> pair = new Pair<String, Integer>(userCategory, userType);
        Boolean result = mappa.containsKey(pair);
        return result;

    }

    @Override
    public List<Integer> getUserTypeByCategory(String userCategory) {
        List<Integer> result = new ArrayList<Integer>(0);
        for (Map.Entry<Pair<String, Integer>, Boolean> item : mappa.entrySet()) {
            if (item.getKey().getLeft().equals(userCategory) && item.getValue() == true) {
                result.add(item.getKey().getRight());
            }
        }
        return result;
    }
}
