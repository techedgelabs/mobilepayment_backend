package com.techedge.mp.core.business.mail;

import java.io.FileInputStream;
import java.util.Properties;

public class MailStyle {
    Properties properties = new Properties();
    
    public MailStyle(String filePath) {
        try {
            properties.load(new FileInputStream(filePath));
        } catch (Exception ex) {
            System.err.println("Error in loading mail style properties! (" + ex.getMessage() + ")");
        }
    }
    
    public String getStyle(String name) {
        String value = "";
        
        if (!properties.containsKey(name) || properties.getProperty(name) == null) {
            return value;
        }
        
        value = properties.getProperty(name);
        return value;
    }

    public String[] getStyle4List(String name) {
        String[] value;
        
        if (!properties.containsKey(name) || properties.getProperty(name) == null) {
            value = new String[5];
            return value;
        }
        
        value = properties.getProperty(name).split(",");
        return value;
    }
    
    public int getStyle4Int(String name) {
        int value = 0;
        
        if (!properties.containsKey(name) || properties.getProperty(name) == null) {
            return value;
        }
        
        value = Integer.parseInt(properties.getProperty(name).trim());
        return value;
    }

    public int[] getStyle4ListInt(String name) {
        int[] value;
        
        if (!properties.containsKey(name) || properties.getProperty(name) == null) {
            value = new int[5];
            return value;
        }
        
        String[] tmpValue = properties.getProperty(name).split(",");
        value = new int[tmpValue.length];
        
        for (int i = 0; i < tmpValue.length; i++) {
            value[i] = Integer.parseInt(tmpValue[i].trim());
        }
        
        return value;
    }
}
