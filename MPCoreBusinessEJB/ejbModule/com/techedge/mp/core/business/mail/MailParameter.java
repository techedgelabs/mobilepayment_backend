package com.techedge.mp.core.business.mail;

import com.techedge.mp.email.sender.business.interfaces.Parameter;

public class MailParameter extends Parameter {

    /**
     * 
     */
    private static final long serialVersionUID = 7445410779594182665L;

    private int level;
    
    public MailParameter(String name, String value, int level) {
        super(name, value);
        setLevel(level);
    }

    public MailParameter(String name, String value) {
        this(name, value, 0);
    }

    public MailParameter(String name) {
        this(name, null, 0);
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
