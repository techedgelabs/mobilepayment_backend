package com.techedge.mp.core.business;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.actions.user.v2.SocialUserV2CreateAction;
import com.techedge.mp.core.actions.user.v2.UserBusinessAuthenticationAction;
import com.techedge.mp.core.actions.user.v2.UserBusinessClonePaymentMethodAction;
import com.techedge.mp.core.actions.user.v2.UserBusinessCreateAction;
import com.techedge.mp.core.actions.user.v2.UserBusinessRecoverUsernameAction;
import com.techedge.mp.core.actions.user.v2.UserBusinessRefreshUserDataAction;
import com.techedge.mp.core.actions.user.v2.UserBusinessRescuePasswordAction;
import com.techedge.mp.core.actions.user.v2.UserBusinessUpdateBusinessDataAction;
import com.techedge.mp.core.actions.user.v2.UserLoadVoucherEventCompaignAction;
import com.techedge.mp.core.actions.user.v2.UserV2AddPlateNumberAction;
import com.techedge.mp.core.actions.user.v2.UserV2AuthenticationAction;
import com.techedge.mp.core.actions.user.v2.UserV2CreateAction;
import com.techedge.mp.core.actions.user.v2.UserV2ExternalAuthenticationAction;
import com.techedge.mp.core.actions.user.v2.UserV2ExternalProviderAuthenticationAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetAwardListAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetBrandListAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetCategoryListAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetHomePartnerListAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetLandingAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetMissionListAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetMissionSfListAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetPartnerActionUrlAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetPartnerListAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetPromoPopupAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetReceiptAction;
import com.techedge.mp.core.actions.user.v2.UserV2GetRedemptionAwardAction;
import com.techedge.mp.core.actions.user.v2.UserV2GuestCreateAction;
import com.techedge.mp.core.actions.user.v2.UserV2InsertPaymentMethodAction;
import com.techedge.mp.core.actions.user.v2.UserV2NotificationVirtualizationAction;
import com.techedge.mp.core.actions.user.v2.UserV2RefreshUserDataAction;
import com.techedge.mp.core.actions.user.v2.UserV2RemovePlateNumberAction;
import com.techedge.mp.core.actions.user.v2.UserV2RequestResetPinAction;
import com.techedge.mp.core.actions.user.v2.UserV2RetrieveTermsOfServiceAction;
import com.techedge.mp.core.actions.user.v2.UserV2RetrieveTermsOfServiceByGroupAction;
import com.techedge.mp.core.actions.user.v2.UserV2SetActiveDpanAction;
import com.techedge.mp.core.actions.user.v2.UserV2SetDefaultPlateNumberAction;
import com.techedge.mp.core.actions.user.v2.UserV2SkipVirtualizationAction;
import com.techedge.mp.core.actions.user.v2.UserV2SocialAuthenticationAction;
import com.techedge.mp.core.actions.user.v2.UserV2UpdatePasswordAction;
import com.techedge.mp.core.actions.user.v2.UserV2UpdateUserPaymentDataAction;
import com.techedge.mp.core.actions.user.v2.UserV2UpdateUsersDataAction;
import com.techedge.mp.core.actions.user.v2.UserV2VirtualizationAttemptsLeftActionAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.AuthenticationBusinessResponse;
import com.techedge.mp.core.business.interfaces.AuthenticationResponse;
import com.techedge.mp.core.business.interfaces.AwardDetailDataResponse;
import com.techedge.mp.core.business.interfaces.BrandDataDetailResponse;
import com.techedge.mp.core.business.interfaces.CategoryEarnDataDetailResponse;
import com.techedge.mp.core.business.interfaces.CreateUserGuestResult;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ExternalAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ExternalProviderAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.GetHomePartnerListResult;
import com.techedge.mp.core.business.interfaces.GetPromoPopupDataResponse;
import com.techedge.mp.core.business.interfaces.GetReceiptDataResponse;
import com.techedge.mp.core.business.interfaces.LandingDataResponse;
import com.techedge.mp.core.business.interfaces.LoadVoucherEventCampaignResponse;
import com.techedge.mp.core.business.interfaces.MissionDetailDataResponse;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.PartnerActionUrlResponse;
import com.techedge.mp.core.business.interfaces.PartnerListInfoDataResponse;
import com.techedge.mp.core.business.interfaces.PaymentV2Response;
import com.techedge.mp.core.business.interfaces.RedemptionAwardResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveTermsOfServiceData;
import com.techedge.mp.core.business.interfaces.SocialAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.VirtualizationAttemptsLeftActionResponse;
import com.techedge.mp.core.business.interfaces.user.PersonalDataBusiness;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

/**
 * Session Bean implementation class UserManager
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2Service implements UserV2ServiceRemote, UserV2ServiceLocal {

    // Letti dai parametri di sistema

    private static final String                          PARAM_TICKET_EXPIRY_TIME                    = "TICKET_EXPIRY_TIME";                   // minutes
    private static final String                          PARAM_MAX_PENDING_INTERVAL                  = "MAX_PENDING_INTERVAL";                 // hour in millis
    private static final String                          PARAM_PASSWORD_HISTORY_LENGTH               = "PASSWORD_HISTORY_LENGTH";
    private static final String                          PARAM_LOGIN_ATTEMPTS_THRESHOLD              = "LOGIN_ATTEMPTS_THRESHOLD";
    private static final String                          PARAM_LOGIN_LOCK_EXPIRY_TIME                = "LOGIN_LOCK_EXPIRY_TIME";               //minutes
    private final static String                          PARAM_USER_BLOCK_EXCEPTION                  = "USER_BLOCK_EXCEPTION";
    private final static String                          PARAM_SECURITY_RSA_PRIVATE_PEM_PATH         = "SECURITY_RSA_PRIVATE_PEM_PATH";
    private final static String                          PARAM_LOYALTY_SESSION_EXPIRY_TIME           = "LOYALTY_SESSION_EXPIRY_TIME";          //minutes

    private static final String                          PARAM_EMAIL_SENDER_ADDRESS                  = "EMAIL_SENDER_ADDRESS";
    private static final String                          PARAM_SMTP_HOST                             = "SMTP_HOST";
    private static final String                          PARAM_SMTP_PORT                             = "SMTP_PORT";
    private final static String                          PARAM_PROXY_HOST                            = "PROXY_HOST";
    private final static String                          PARAM_PROXY_PORT                            = "PROXY_PORT";
    private final static String                          PARAM_PROXY_NO_HOSTS                        = "PROXY_NO_HOSTS";
    private final static String                          PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE = "VERIFICATION_CODE_MOBILE_SENDING_TYPE";
    private final static String                          PARAM_SMS_MAX_RETRY_ATTEMPS                 = "SMS_MAX_RETRY_ATTEMPS";
    private static final String                          PARAM_RECONCILIATION_MAX_ATTEMPTS           = "RECONCILIATION_MAX_ATTEMPTS";

    private static final String                          PARAM_ACQUIRER_ID_CARTASI                   = "ACQUIRER_ID_CARTASI";
    private static final String                          PARAM_APIKEY_CARTASI                        = "APIKEY_CARTASI";
    private static final String                          PARAM_UICCODE_REFUND_CARTASI                = "UICCODE_REFUND_CARTASI";
    private static final String                          PARAM_UICCODE_DEPOSIT_CARTASI               = "UICCODE_DEPOSIT_CARTASI";
    private static final String                          PARAM_GROUP_ACQUIRER_CARTASI                = "GROUP_ACQUIRER_CARTASI";
    private static final String                          PARAM_ENCODED_SECRET_KEY_CARTASI            = "ENCODED_SECRET_KEY_CARTASI";
    private static final String                          PARAM_PIN_CHECK_MAX_ATTEMPTS                = "PIN_CHECK_MAX_ATTEMPTS";
    private static final String                          PARAM_CHECK_AMOUNT_VALUE                    = "CHECK_AMOUNT_VALUE";
    private static final String                          PARAM_MAX_CHECK_AMOUNT                      = "MAX_CHECK_AMOUNT";
    private final static String                          PARAM_CHECK_AMOUNT_MAX_ATTEMPTS             = "CHECK_AMOUNT_MAX_ATTEMPTS";
    private final static String                          PARAM_CHECK_CREDIT_CARD_UNIQUE              = "CHECK_CREDIT_CARD_UNIQUE";

    private final static String                          PARAM_CARTASI_VAULT_BLOCK                   = "CARTASI_VAULT_BLOCK";
    private final static String                          PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY         = "CARTASI_VAULT_BLOCK_ATTRIBUTE_KEY";
    private static final String                          PARAM_ENIPAY_MIGRATION_TIMESTAMP            = "ENIPAY_MIGRATION_TIMESTAMP";
    private static final String                          PARAM_GUEST_LANDING_ACTIVATION_TIMESTAMP    = "GUEST_LANDING_ACTIVATION_TIMESTAMP";

    private static final String                          PARAM_ACTIVATION_LINK                       = "ACTIVATION_LINK";
    private static final String                          PARAM_VERIFICATION_CODE_EXPIRY_TIME         = "VERIFICATION_CODE_EXPIRY_TIME";        // minutes
    private static final String                          PARAM_INITIAL_CAP                           = "INITIAL_CAP";
    private static final String                          PARAM_CHECK_FISCAL_CODE                     = "CHECK_FISCAL_CODE";
    private final static String                          PARAM_CREATE_USER_USERTYPE                  = "CREATE_USER_USERTYPE";
    private final static String                          PARAM_CREATE_USER_MOBILE_PHONE_MANDATORY    = "CREATE_USER_MOBILE_PHONE_MANDATORY";
    private final static String                          PARAM_CHECK_MOBILE_PHONE_UNIQUE             = "CHECK_MOBILE_PHONE_UNIQUE";
    private final static String                          PARAM_VERIFICATION_NUMBER_DEVICE            = "VERIFICATION_NUMBER_DEVICE";
    private final static String                          PARAM_VIRTUALIZATION_ATTEMPTS_LEFT          = "VIRTUALIZATION_ATTEMPTS_LEFT";
    private final static String                          PARAM_CHECK_BLACKLIST_MAIL                  = "CHECK_BLACKLIST_MAIL";
    private final static String                          PARAM_STRING_SUBSTITUTION_PATTERN           = "STRING_SUBSTITUTION_PATTERN";
    private static final String                          PARAM_CHECK_BIRTH_PLACE                     = "CHECK_BIRTH_PLACE";
    private static final String                          PARAM_PROMO_VOUCHER_VALID_INTERVAL          = "PROMO_VOUCHER_VALID_INTERVAL";
    private static final String                          PARAM_CHECK_DEVICE_ID_UNIQUE                = "CHECK_DEVICE_ID_UNIQUE";

    // Parametri per la gestione del flusso di autenticazione tramite social
    // login
    private static final String                          PARAM_GOOGLE_CLIENT_ID_IOS                  = "GOOGLE_CLIENT_ID_IOS";
    private static final String                          PARAM_GOOGLE_CLIENT_ID_AND                  = "GOOGLE_CLIENT_ID_AND";
    private static final String                          PARAM_FB_APP_ID                             = "FB_APP_ID";
    private static final String                          PARAM_FB_APP_SECRET                         = "FB_APP_SECRET";
    private static final String                          PARAM_GOOGLE_PROFILE_URI                    = "GOOGLE_PROFILE_URI";
    private static final String                          PARAM_FACEBOOK_PROFILE_URI                  = "FACEBOOK_PROFILE_URI";
    private static final String                          PARAM_FACEBOOK_CHECK_TOKEN_URI_PREFIX       = "FACEBOOK_CHECK_TOKEN_URI_PREFIX";
    private static final String                          PARAM_GOOGLE_CHECK_TOKEN_URI_PREFIX         = "GOOGLE_CHECK_TOKEN_URI_PREFIX";
    private static final String                          PARAM_CAPTCHA_SECRET_ANDROID                = "CAPTCHA_SECRET_ANDROID";
    private static final String                          PARAM_CAPTCHA_SECRET_IOS                    = "CAPTCHA_SECRET_IOS";
    private static final String                          PARAM_URL_CAPTCHA_VERIFICATION              = "URL_CAPTCHA_VERIFICATION";
    private static final String                          PARAM_RESCUE_PASSWORD_EXPIRATION_DELAY      = "RESCUE_PASSWORD_EXPIRATION_DELAY";     // minutes
    private static final String                          PARAM_PASSWORD_RECOVERY_LINK                = "PASSWORD_RECOVERY_LINK";
    
    private static final String                          PARAM_REFUELING_OAUTH2_ACTIVE               = "REFUELING_OAUTH2_ACTIVE";

    public static final String                           SOCIAL_USER_DEFAULT_PASSWORD                = "XXXXXXXXXXXXXXXXXXXX";
    
    private final String 								 PARAM_CRM_SF_ACTIVE						 ="CRM_SF_ACTIVE";


    public static String                                 googleClientIdIOS;
    public static String                                 googleClientIdAND;
    public static String                                 fbAppId;
    public static String                                 fbAppSecret;
    public static String                                 googleProfileUri;
    public static String                                 facebookProfileUri;
    public static String                                 facebookCheckTokenUriPrefix;
    public static String                                 googleCheckTokenUriPrefix;

    @EJB
    private UserV2AuthenticationAction                   userV2AuthenticationAction;

    @EJB
    private UserV2SocialAuthenticationAction             userV2SocialAuthenticationAction;
    
    @EJB
    private UserV2ExternalProviderAuthenticationAction   userV2ExternalProviderAuthenticationAction;

    @EJB
    private UserV2UpdatePasswordAction                   userV2UpdatePasswordAction;

    @EJB
    private UserV2UpdateUsersDataAction                  userV2UpdateUsersDataAction;

    @EJB
    private UserV2NotificationVirtualizationAction       userV2NotificationVirtualizationAction;

    @EJB
    private UserV2VirtualizationAttemptsLeftActionAction userV2VirtualizationAttemptsLeftActionAction;

    @EJB
    private UserV2InsertPaymentMethodAction              userV2InsertPaymentMethodAction;

    @EJB
    private UserV2UpdateUserPaymentDataAction            userV2UpdateUserPaymentDataAction;

    @EJB
    private UserV2GetLandingAction                       userV2GetLandingAction;

    @EJB
    private UserV2GetPartnerListAction                   userV2GetPartnerListAction;

    @EJB
    private UserV2GetCategoryListAction                  userV2GetCategoryListAction;

    @EJB
    private UserV2GetBrandListAction                     userV2GetBrandListAction;

    @EJB
    private UserV2GetAwardListAction                     userV2GetAwardListAction;

    @EJB
    private UserV2GetRedemptionAwardAction               userV2GetRedemptionAwardAction;

    @EJB
    private UserV2GetPartnerActionUrlAction              userV2GetPartnerActionUrlAction;
    
    @EJB
    private UserV2GetMissionListAction                   userV2GetMissionListAction;
    
    @EJB
    private UserV2GetMissionSfListAction                 userV2GetMissionSfListAction;

    @EJB
    private UserV2RetrieveTermsOfServiceAction           userV2RetrieveTermsOfServiceAction;

    @EJB
    private UserV2RetrieveTermsOfServiceByGroupAction    userV2RetrieveTermsOfServiceByGroupAction;

    @EJB
    private UserV2CreateAction                           userV2CreateAction;

    @EJB
    private SocialUserV2CreateAction                     socialUserV2CreateAction;

    @EJB
    private UserV2SkipVirtualizationAction               userV2SkipVirtualizationAction;

    @EJB
    private UserLoadVoucherEventCompaignAction           userLoadVoucherEventCompaignAction;

    @EJB
    private UserV2GetPromoPopupAction                    userV2GetPromoPopupAction;
	
	@EJB
    private UserV2GuestCreateAction                      userV2GuestCreateAction;
	
	@EJB
	private UserV2GetReceiptAction                       userV2GetReceiptAction;
	
    @EJB
    private UserV2RefreshUserDataAction                  userV2RefreshUserDataAction;
    
    @EJB
    private UserV2AddPlateNumberAction                   userV2AddPlateNumberAction;
    
    @EJB
    private UserV2RemovePlateNumberAction                userV2RemovePlateNumberAction;
    
    @EJB
    private UserV2SetDefaultPlateNumberAction            userV2SetDefaultPlateNumberAction;
    
    @EJB
    private UserV2GetHomePartnerListAction               userV2GetHomePartnerListAction;

    @EJB
    private UserBusinessAuthenticationAction             userBusinessAuthenticationAction;

    @EJB
    private UserBusinessCreateAction                     userBusinessCreateAction;

    @EJB
    private UserBusinessUpdateBusinessDataAction         userBusinessUpdateBusinessDataAction;

    @EJB
    private UserBusinessRefreshUserDataAction            userBusinessRefreshUserDataAction;
    
    @EJB
    private UserBusinessRecoverUsernameAction            userBusinessRecoverUsernameAction;

    @EJB
    private UserBusinessRescuePasswordAction             userBusinessRescuePasswordAction;
    
    @EJB
    private UserV2RequestResetPinAction                  userV2RequestResetPinAction;

    @EJB
    private UserBusinessClonePaymentMethodAction         userBusinessClonePaymentMethodAction;
    
    @EJB
    private UserV2ExternalAuthenticationAction           userV2ExternalAuthenticationAction;
    
    @EJB
    private UserV2SetActiveDpanAction                    userV2SetActiveDpanAction;

    private ParametersService                            parametersService                           = null;
    private LoggerService                                loggerService                               = null;
    private UserCategoryService                          userCategoryService                         = null;
    private UnavailabilityPeriodService                  unavailabilityPeriodService                 = null;
    private DWHAdapterServiceRemote                      dwhAdapterService                           = null;
    private PushNotificationServiceRemote                pushNotificationService                     = null;
    private EmailSenderRemote                            emailSender                                 = null;
    private FidelityServiceRemote                        fidelityService                             = null;
    private SmsServiceRemote                             smsService                                  = null; 
    private CRMServiceRemote                             crmService                                  = null;
    private ParkingServiceRemote                         parkingService                              = null;
    private RefuelingNotificationServiceRemote           refuelingNotificationService                = null;
    private RefuelingNotificationServiceOAuth2Remote     refuelingNotificationServiceOAuth2          = null;
    private CardInfoServiceRemote                        cardInfoService                             = null;

    private String                                       privateKeyPEM                               = null;
    private String                                       secretKey                                   = null;
    private StringSubstitution                           stringSubstitution                          = null;

    private String                                       proxyHost                                   = null;
    private String                                       proxyPort                                   = null;
    private String                                       proxyNoHosts                                = null;

    //private String                                   publicKeyPEM                                   = null;

    /**
     * Default constructor.
     */
    public UserV2Service() throws EJBException {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ParametersService object");

            throw new EJBException(e);
        }

        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get UserCategoryService object");

            throw new EJBException(e);
        }
        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get FidelityService object");

            throw new EJBException(e);
        }
        try {
            this.unavailabilityPeriodService = EJBHomeCache.getInstance().getUnavailabilityPeriodService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        
        try {
            parkingService = EJBHomeCache.getInstance().getParkingService();
        }
        catch (InterfaceNotFoundException e1) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ParkingService object");

            throw new EJBException(e1);
        }
        
        try {
            refuelingNotificationService = EJBHomeCache.getInstance().getRefuelingNotificationService();
        }
        catch (InterfaceNotFoundException e1) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get RefuelingNotificationService object");

            throw new EJBException(e1);
        }
        
        try {
            refuelingNotificationServiceOAuth2 = EJBHomeCache.getInstance().getRefuelingNotificationServiceOAuth2();
        }
        catch (InterfaceNotFoundException e1) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get RefuelingNotificationServiceOAuth2 object");

            throw new EJBException(e1);
        }

        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get EmailSender object");

            throw new EJBException(e);
        }
        try {
            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        try {
            this.smsService = EJBHomeCache.getInstance().getSmsService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        try {
            this.crmService = EJBHomeCache.getInstance().getCRMService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        try {
            this.cardInfoService = EJBHomeCache.getInstance().getCardInfoService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get CardInfoService object");

            throw new EJBException(e);
        }
        
        try {
            this.proxyHost = parametersService.getParamValue(PARAM_PROXY_HOST);
            this.proxyPort = parametersService.getParamValue(PARAM_PROXY_PORT);
            this.proxyNoHosts = parametersService.getParamValue(PARAM_PROXY_NO_HOSTS);
        }
        catch (ParameterNotFoundException ex) {
            System.err.println("Parameter not found: " + ex.getMessage());
            throw new EJBException(ex);
        }
        
        try {
            //File filePrivatePEM = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "security" + File.separator + "frontend.private.pem");
            File filePrivatePEM = new File(parametersService.getParamValue(UserV2Service.PARAM_SECURITY_RSA_PRIVATE_PEM_PATH));
            FileInputStream input = new FileInputStream(filePrivatePEM);
            byte[] keyBytes = new byte[input.available()];
            input.read(keyBytes);
            input.close();
            privateKeyPEM = new String(keyBytes, "UTF-8");
        }
        catch (Exception ex) {
            System.err.println("Error reading private key file: " + ex.getMessage());
        }

        String vaultBlock = "";
        String vaultBlockAttributeCryptKey = "";
        String pattern = null;

        try {

            vaultBlock = parametersService.getParamValue(UserV2Service.PARAM_CARTASI_VAULT_BLOCK);
            vaultBlockAttributeCryptKey = parametersService.getParamValue(UserV2Service.PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY);
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeCryptKey + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.secretKey + "'");

        stringSubstitution = new StringSubstitution(pattern);

        try {
            googleClientIdIOS = this.parametersService.getParamValue(UserV2Service.PARAM_GOOGLE_CLIENT_ID_IOS);
            googleClientIdAND = this.parametersService.getParamValue(UserV2Service.PARAM_GOOGLE_CLIENT_ID_AND);
            fbAppId = this.parametersService.getParamValue(UserV2Service.PARAM_FB_APP_ID);
            fbAppSecret = this.parametersService.getParamValue(UserV2Service.PARAM_FB_APP_SECRET);
            googleProfileUri = this.parametersService.getParamValue(UserV2Service.PARAM_GOOGLE_PROFILE_URI);
            facebookProfileUri = this.parametersService.getParamValue(UserV2Service.PARAM_FACEBOOK_PROFILE_URI);
            facebookCheckTokenUriPrefix = this.parametersService.getParamValue(UserV2Service.PARAM_FACEBOOK_CHECK_TOKEN_URI_PREFIX);
            googleCheckTokenUriPrefix = this.parametersService.getParamValue(UserV2Service.PARAM_GOOGLE_CHECK_TOKEN_URI_PREFIX);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());

        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());

        }

    }

    @Override
    public AuthenticationResponse authentication(String email, String passwordHash, String passwordEncrypted, String requestId, String deviceId, String deviceName,
            String deviceToken) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("email", email));
        inputParameters.add(new Pair<String, String>("passwordHash", passwordHash));
        inputParameters.add(new Pair<String, String>("passwordEncrypted", passwordEncrypted));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deviceId", deviceId));
        inputParameters.add(new Pair<String, String>("deviceName", deviceName));
        inputParameters.add(new Pair<String, String>("deviceToken", deviceToken));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "authentication", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        SmsServiceRemote smsService = null;

        AuthenticationResponse authenticationResponse = null;
        try {

            smsService = EJBHomeCache.getInstance().getSmsService();
            String sendingType = this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE);
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_SMS_MAX_RETRY_ATTEMPS));
            Long deployDateLong = Long.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_ENIPAY_MIGRATION_TIMESTAMP));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
            String userBlockException = this.parametersService.getParamValue(UserV2Service.PARAM_USER_BLOCK_EXCEPTION);

            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();

            Integer maxPendingInterval = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_MAX_PENDING_INTERVAL));
            Integer ticketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_TICKET_EXPIRY_TIME));
            Integer loyaltySessionExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOYALTY_SESSION_EXPIRY_TIME));
            Integer loginAttemptsLimit = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOGIN_ATTEMPTS_THRESHOLD));
            Integer loginLockExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOGIN_LOCK_EXPIRY_TIME));

            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            authenticationResponse = userV2AuthenticationAction.execute(email, passwordHash, passwordEncrypted, privateKeyPEM, requestId, deviceId, deviceName, deviceToken,
                    maxPendingInterval, ticketExpiryTime, loyaltySessionExpiryTime, loginAttemptsLimit, loginLockExpiryTime, userBlockExceptionList, this.userCategoryService,
                    this.unavailabilityPeriodService, this.dwhAdapterService, this.pushNotificationService, maxRetryAttemps, sendingType, emailSender, smsService, deployDateLong,
                    stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            authenticationResponse = new AuthenticationResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            authenticationResponse = new AuthenticationResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", authenticationResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "authentication", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return authenticationResponse;
    }

    @Override
    public String updatePassword(String ticketId, String requestId, String oldPassword, String newPassword) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("oldPassword", oldPassword));
        inputParameters.add(new Pair<String, String>("newPassword", newPassword));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePassword", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            Integer passwordHistoryLenght = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_PASSWORD_HISTORY_LENGTH));
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_RECONCILIATION_MAX_ATTEMPTS));

            response = userV2UpdatePasswordAction.execute(ticketId, requestId, oldPassword, newPassword, passwordHistoryLenght, reconciliationMaxAttempts, userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePassword", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String updateUsersData(String ticketId, String requestId, HashMap<String, Object> usersData) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        String userFieldsString = "{ ";
        boolean first = false;

        for (String key : usersData.keySet()) {
            userFieldsString += (!first ? ", " : "") + key + ": " + usersData.get(key);

            if (first) {
                first = false;
            }
        }

        inputParameters.add(new Pair<String, String>("usersData", userFieldsString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateUsersData", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            response = userV2UpdateUsersDataAction.execute(ticketId, requestId, reconciliationMaxAttempts, usersData);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateUsersData", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String notificationVirtualization(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "userV2NotificationVirtualization", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_RECONCILIATION_MAX_ATTEMPTS));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userV2NotificationVirtualizationAction.execute(ticketId, requestId, reconciliationMaxAttempts, this.emailSender, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "userV2NotificationVirtualization", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftAction(String ticketId, String requestId, String action) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("action", action));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "virtualizationAttemptsLeftAction", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = null;

        try {
            Integer virtualizationAttemptsLeft = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_VIRTUALIZATION_ATTEMPTS_LEFT));
            virtualizationAttemptsLeftActionResponse = userV2VirtualizationAttemptsLeftActionAction.execute(ticketId, requestId, action, virtualizationAttemptsLeft);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
            virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
            virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", virtualizationAttemptsLeftActionResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "virtualizationAttemptsLeftAction", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return virtualizationAttemptsLeftActionResponse;
    }

    @Override
    public PaymentV2Response insertPaymentMethod(String ticketId, String requestId, String paymentMethodType, String newPin) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("newPin", newPin));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "insertPaymentMethod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        PaymentV2Response paymentV2Response = null;
        try {
            String apiKey = this.parametersService.getParamValue(UserV2Service.PARAM_APIKEY_CARTASI);
            String uicCode = this.parametersService.getParamValue(UserV2Service.PARAM_UICCODE_DEPOSIT_CARTASI);
            String groupAcquirer = this.parametersService.getParamValue(UserV2Service.PARAM_GROUP_ACQUIRER_CARTASI);
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            String checkAmountValue = this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_AMOUNT_VALUE);
            Double maxCheckAmount = Double.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_MAX_CHECK_AMOUNT));

            // Decodifica stringa acquirer
            String encodedSecretKey = this.parametersService.getParamValue(UserV2Service.PARAM_ENCODED_SECRET_KEY_CARTASI);
            String decodedSecretKey = "";

            if (encodedSecretKey != null && !encodedSecretKey.equals("")) {

                EncryptionAES encryptionAES = new EncryptionAES();
                encryptionAES.loadSecretKey(this.secretKey);
                decodedSecretKey = encryptionAES.decrypt(encodedSecretKey);
            }

            paymentV2Response = userV2InsertPaymentMethodAction.execute(ticketId, requestId, paymentMethodType, newPin, apiKey, pinCheckMaxAttempts, checkAmountValue,
                    maxCheckAmount, uicCode, groupAcquirer, decodedSecretKey, this.userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            paymentV2Response = new PaymentV2Response();
            paymentV2Response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            paymentV2Response = new PaymentV2Response();
            paymentV2Response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception e) {
            e.printStackTrace();

            paymentV2Response = new PaymentV2Response();
            paymentV2Response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", paymentV2Response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "insertPaymentMethod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return paymentV2Response;
    }

    @Override
    public String updateUserPaymentData(String transactionType, String transactionResult, String shopTransactionID, String bankTransactionID, String authorizationCode,
            String currency, String amount, String country, String buyerName, String buyerEmail, String errorCode, String errorDescription, String alertCode,
            String alertDescription, String TransactionKey, String token, String tokenExpiryMonth, String tokenExpiryYear, String cardBin, String TDLevel, String pan, 
            String brand, String hash) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionType", transactionType));
        inputParameters.add(new Pair<String, String>("transactionResult", transactionResult));
        inputParameters.add(new Pair<String, String>("shopTransactionID", shopTransactionID));
        inputParameters.add(new Pair<String, String>("bankTransactionID", bankTransactionID));
        inputParameters.add(new Pair<String, String>("authorizationCode", authorizationCode));
        inputParameters.add(new Pair<String, String>("currency", currency));
        inputParameters.add(new Pair<String, String>("amount", amount));
        inputParameters.add(new Pair<String, String>("buyerName", buyerName));
        inputParameters.add(new Pair<String, String>("buyerEmail", buyerEmail));
        inputParameters.add(new Pair<String, String>("errorCode", errorCode));
        inputParameters.add(new Pair<String, String>("errorDescription", errorDescription));
        inputParameters.add(new Pair<String, String>("alertCode", alertCode));
        inputParameters.add(new Pair<String, String>("alertDescription", alertDescription));
        inputParameters.add(new Pair<String, String>("TransactionKey", TransactionKey));
        inputParameters.add(new Pair<String, String>("token", token));
        inputParameters.add(new Pair<String, String>("tokenExpiryMonth", tokenExpiryMonth));
        inputParameters.add(new Pair<String, String>("tokenExpiryYear", tokenExpiryYear));
        inputParameters.add(new Pair<String, String>("cardBin", cardBin));
        inputParameters.add(new Pair<String, String>("TDLevel", TDLevel));
        inputParameters.add(new Pair<String, String>("brand", brand));
        inputParameters.add(new Pair<String, String>("hash", hash));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateUserPaymentData", shopTransactionID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String shopLoginCheckNewAcquirer = this.parametersService.getParamValue(UserV2Service.PARAM_APIKEY_CARTASI);
            Integer checkAmountMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_AMOUNT_MAX_ATTEMPTS));
            String acquirerIdCartaSi = this.parametersService.getParamValue(UserV2Service.PARAM_ACQUIRER_ID_CARTASI);
            String groupAcquirerCartaSi = this.parametersService.getParamValue(UserV2Service.PARAM_GROUP_ACQUIRER_CARTASI);
            String uicCodeRefundCartaSi = this.parametersService.getParamValue(UserV2Service.PARAM_UICCODE_REFUND_CARTASI);
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            boolean checkPaymentMethodExists = false;

            String encodedSecretKey = this.parametersService.getParamValue(UserV2Service.PARAM_ENCODED_SECRET_KEY_CARTASI);
            String decodedSecretKey = "";

            if (encodedSecretKey != null && !encodedSecretKey.equals("")) {

                EncryptionAES encryptionAES = new EncryptionAES();
                encryptionAES.loadSecretKey(this.secretKey);
                decodedSecretKey = encryptionAES.decrypt(encodedSecretKey);
            }

            if (this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_CREDIT_CARD_UNIQUE) != null) {
                checkPaymentMethodExists = Boolean.parseBoolean(this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_CREDIT_CARD_UNIQUE));
            }

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userV2UpdateUserPaymentDataAction.execute(transactionType, transactionResult, shopTransactionID, bankTransactionID, authorizationCode, currency, amount,
                    country, buyerName, buyerEmail, errorCode, errorDescription, alertCode, alertDescription, TransactionKey, token, tokenExpiryMonth, tokenExpiryYear, cardBin,
                    TDLevel, pan, brand, checkAmountMaxAttempts, shopLoginCheckNewAcquirer, uicCodeRefundCartaSi, checkPaymentMethodExists, acquirerIdCartaSi,
                    groupAcquirerCartaSi, decodedSecretKey, emailSender, userCategoryService, hash);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (InterfaceNotFoundException e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get CRMService object");
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateUserPaymentData", shopTransactionID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public LandingDataResponse getLandingData(String ticketId, String requestId, String sectionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("sectionID", sectionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLandingData", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        LandingDataResponse response = null;
        try {

            Long deployDateLong = Long.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_ENIPAY_MIGRATION_TIMESTAMP));
            Long guestLandingActivationTimestamp = Long.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_GUEST_LANDING_ACTIVATION_TIMESTAMP));

            response = userV2GetLandingAction.execute(ticketId, requestId, sectionID, deployDateLong, guestLandingActivationTimestamp, this.userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = new LandingDataResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            response = new LandingDataResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLandingData", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public RetrieveTermsOfServiceData retrieveTermsOfService(String ticketId, String requestId, Boolean isOptional) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTermsOfService", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveTermsOfServiceData retrieveTermsOfServiceData = null;
        try {

            //Integer userType = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_CREATE_USER_USERTYPE));

            retrieveTermsOfServiceData = userV2RetrieveTermsOfServiceAction.execute(ticketId, requestId, UserCategoryType.NEW_ACQUIRER_FLOW, userCategoryService, isOptional);
        }
        /*
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
            retrieveTermsOfServiceData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }*/
        catch (EJBException ex) {
            retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
            retrieveTermsOfServiceData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveTermsOfServiceData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTermsOfService", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveTermsOfServiceData;
    }

   
    @Override
    public RetrieveTermsOfServiceData retrieveTermsOfServiceByGroup(String ticketId, String requestId, String group) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("group", group));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTermsOfServiceByGroup", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        RetrieveTermsOfServiceData retrieveTermsOfServiceData = null;
        try {
            retrieveTermsOfServiceData = userV2RetrieveTermsOfServiceByGroupAction.execute(ticketId, requestId, UserCategoryType.NEW_ACQUIRER_FLOW, userCategoryService, group);
        }
        catch (EJBException ex) {
            retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
            retrieveTermsOfServiceData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveTermsOfServiceData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTermsOfServiceByGroup", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return retrieveTermsOfServiceData;
    }

    @Override
    public String createUser(String ticketId, String requestId, User user, Long loyaltyCardId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("user", user.toString()));
        inputParameters.add(new Pair<String, String>("user->source", user.getSource()));
        if (loyaltyCardId != null) {
            inputParameters.add(new Pair<String, String>("loyaltyCardId", loyaltyCardId.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("loyaltyCardId", ""));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createUser", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String activationLink = this.parametersService.getParamValue(UserV2Service.PARAM_ACTIVATION_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String checkFiscalCode = this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_FISCAL_CODE);
            Integer verificationCodeExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_CODE_EXPIRY_TIME));
            Double initialCap = Double.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_INITIAL_CAP));
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            Integer userType = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_CREATE_USER_USERTYPE));
            Boolean createUserMobilePhoneMandatory = Boolean.valueOf(parametersService.getParamValue(UserV2Service.PARAM_CREATE_USER_MOBILE_PHONE_MANDATORY));
            Boolean checkMobilePhoneUnique = Boolean.valueOf(parametersService.getParamValue(UserV2Service.PARAM_CHECK_MOBILE_PHONE_UNIQUE));
            String checkMailInBlacklist = this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_BLACKLIST_MAIL);
            Integer verificationNumberDevice = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_NUMBER_DEVICE));
            Integer virtualizationAttemptsLeft = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_VIRTUALIZATION_ATTEMPTS_LEFT));
            String checkBirthPlace = this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_BIRTH_PLACE);
            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
            String sendingType = this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_SMS_MAX_RETRY_ATTEMPS));

            response = userV2CreateAction.execute(ticketId, requestId, user, userType, loyaltyCardId, checkFiscalCode, verificationCodeExpiryTime, activationLink, initialCap,
                    createUserMobilePhoneMandatory, checkMobilePhoneUnique, emailSender, this.fidelityService, checkMailInBlacklist, verificationNumberDevice,
                    virtualizationAttemptsLeft, checkBirthPlace, stringSubstitution, sendingType, maxRetryAttemps, smsService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createUser", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
    
    @Override
    public String createSocialUser(String accessToken, String socialProvider, String ticketId, String requestId, User user, Long loyaltyCardId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("accessToken", accessToken));
        inputParameters.add(new Pair<String, String>("socialProvider", socialProvider));
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("user", user.toString()));
        if (loyaltyCardId != null) {
            inputParameters.add(new Pair<String, String>("loyaltyCardId", loyaltyCardId.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("loyaltyCardId", ""));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createSocialUser", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String activationLink = this.parametersService.getParamValue(UserV2Service.PARAM_ACTIVATION_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String checkFiscalCode = this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_FISCAL_CODE);
            Integer verificationCodeExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_CODE_EXPIRY_TIME));
            Double initialCap = Double.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_INITIAL_CAP));
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            Integer userType = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_CREATE_USER_USERTYPE));
            Boolean createUserMobilePhoneMandatory = Boolean.valueOf(parametersService.getParamValue(UserV2Service.PARAM_CREATE_USER_MOBILE_PHONE_MANDATORY));
            Boolean checkMobilePhoneUnique = Boolean.valueOf(parametersService.getParamValue(UserV2Service.PARAM_CHECK_MOBILE_PHONE_UNIQUE));
            String checkMailInBlacklist = this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_BLACKLIST_MAIL);
            Integer verificationNumberDevice = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_NUMBER_DEVICE));
            Integer virtualizationAttemptsLeft = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_VIRTUALIZATION_ATTEMPTS_LEFT));
            String checkBirthPlace = this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_BIRTH_PLACE);
            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
            String sendingType = this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_SMS_MAX_RETRY_ATTEMPS));

            response = socialUserV2CreateAction.execute(accessToken, socialProvider, ticketId, requestId, user, userType, loyaltyCardId, checkFiscalCode,
                    verificationCodeExpiryTime, activationLink, initialCap, createUserMobilePhoneMandatory, checkMobilePhoneUnique, emailSender, this.fidelityService,
                    checkMailInBlacklist, verificationNumberDevice, virtualizationAttemptsLeft, checkBirthPlace, stringSubstitution,this.proxyHost,
                    this.proxyPort, this.proxyNoHosts, sendingType, maxRetryAttemps, this.smsService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createSocialUser", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String skipVirtualization(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "skipVirtualization", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_RECONCILIATION_MAX_ATTEMPTS));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userV2SkipVirtualizationAction.execute(ticketId, requestId, reconciliationMaxAttempts, this, this.userCategoryService, this.emailSender, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "skipVirtualization", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public LoadVoucherEventCampaignResponse loadVoucherEventCampaign(String ticketId, String requestId, String voucherCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("voucherCode", voucherCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "loadVoucherEventCompaign", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        LoadVoucherEventCampaignResponse response = null;

        try {
            Long validationTimestamp = Long.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_PROMO_VOUCHER_VALID_INTERVAL));
            Integer maxVoucherCount = 200;

            response = userLoadVoucherEventCompaignAction.execute(ticketId, requestId, voucherCode, maxVoucherCount, this.fidelityService, this.userCategoryService,
                    validationTimestamp);
        }

        catch (EJBException ex) {
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (ParameterNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "loadVoucherEventCompaign", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public GetPromoPopupDataResponse getPromoPopupData(String ticketId, String requestId, String sectionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("sectionID", sectionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPromoPopupData", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        GetPromoPopupDataResponse response = null;
        try {

            response = userV2GetPromoPopupAction.execute(ticketId, requestId, sectionID);
        }
        catch (EJBException ex) {
            response = new GetPromoPopupDataResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPromoPopupData", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
	
	@Override
    public CreateUserGuestResult createUserGuest(String ticketId, String requestId, String deviceId, String captchaCode) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deviceId", deviceId));
        inputParameters.add(new Pair<String, String>("captchaCode", captchaCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createUserGuest", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateUserGuestResult createUserGuestResult = null;
        try {
            Double initialCap            = Double.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_INITIAL_CAP));
            Boolean checkDeviceIdUnique  = Boolean.valueOf(parametersService.getParamValue(UserV2Service.PARAM_CHECK_DEVICE_ID_UNIQUE));
            String captchaSecretAndroid  = this.parametersService.getParamValue(UserV2Service.PARAM_CAPTCHA_SECRET_ANDROID);
            String captchaSecretIOS      = this.parametersService.getParamValue(UserV2Service.PARAM_CAPTCHA_SECRET_IOS);
            String urlCapthaVerification = this.parametersService.getParamValue(UserV2Service.PARAM_URL_CAPTCHA_VERIFICATION);
            String proxyHost             = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort             = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts          = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            
            createUserGuestResult = userV2GuestCreateAction.execute(ticketId, requestId, deviceId, captchaCode, initialCap, checkDeviceIdUnique, stringSubstitution, captchaSecretAndroid, captchaSecretIOS, urlCapthaVerification, proxyHost, proxyPort, proxyNoHosts);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createUserGuestResult = new CreateUserGuestResult();
            createUserGuestResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            createUserGuestResult = new CreateUserGuestResult();
            createUserGuestResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", createUserGuestResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createUserGuest", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return createUserGuestResult;
    }
    
	@Override
    public SocialAuthenticationResponse socialAuthentication(String accessToken, String socialProvider, String requestId, String deviceId, String deviceName, String deviceToken) {

        SocialAuthenticationResponse socialAuthenticationResponse = null;

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("accessToken", accessToken));
        inputParameters.add(new Pair<String, String>("socialProvider", socialProvider));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deviceId", deviceId));
        inputParameters.add(new Pair<String, String>("deviceName", deviceName));
        inputParameters.add(new Pair<String, String>("deviceToken", deviceToken));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "socialAuthentication", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        SmsServiceRemote smsService = null;

        try {

            smsService = EJBHomeCache.getInstance().getSmsService();
            String sendingType = this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE);
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_SMS_MAX_RETRY_ATTEMPS));
            Long deployDateLong = Long.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_ENIPAY_MIGRATION_TIMESTAMP));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
            String userBlockException = this.parametersService.getParamValue(UserV2Service.PARAM_USER_BLOCK_EXCEPTION);

            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();

            Integer maxPendingInterval = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_MAX_PENDING_INTERVAL));
            Integer ticketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_TICKET_EXPIRY_TIME));
            Integer loyaltySessionExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOYALTY_SESSION_EXPIRY_TIME));
            Integer loginAttemptsLimit = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOGIN_ATTEMPTS_THRESHOLD));
            Integer loginLockExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOGIN_LOCK_EXPIRY_TIME));

            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            socialAuthenticationResponse = userV2SocialAuthenticationAction.execute(accessToken, socialProvider, requestId, deviceId, deviceName, deviceToken, this.proxyHost,
                    this.proxyPort, this.proxyNoHosts, maxPendingInterval, ticketExpiryTime, loyaltySessionExpiryTime, loginAttemptsLimit, loginLockExpiryTime,
                    userBlockExceptionList, this.userCategoryService, this.unavailabilityPeriodService, this.dwhAdapterService, this.pushNotificationService, maxRetryAttemps,
                    sendingType, emailSender, smsService, deployDateLong, stringSubstitution);

        }
        catch (ParameterNotFoundException ex) {
            ex.printStackTrace();
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            socialAuthenticationResponse = new SocialAuthenticationResponse();
            socialAuthenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            socialAuthenticationResponse = new SocialAuthenticationResponse();
            socialAuthenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", socialAuthenticationResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "socialAuthentication", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return socialAuthenticationResponse;
    }

    @Override
    public AuthenticationResponse refreshUserData(String ticketId, String loyaltySessionId, String requestId, String deviceId, String deviceName) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("loyaltySessionId", loyaltySessionId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deviceId", deviceId));
        inputParameters.add(new Pair<String, String>("deviceName", deviceName));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refreshUserData", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AuthenticationResponse authenticationResponse = null;
        try {

            Long deployDateLong = Long.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_ENIPAY_MIGRATION_TIMESTAMP));
            Integer maxPendingInterval = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_MAX_PENDING_INTERVAL));
            Integer ticketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_TICKET_EXPIRY_TIME));
            Integer loyaltySessionExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOYALTY_SESSION_EXPIRY_TIME));

            authenticationResponse = userV2RefreshUserDataAction.execute(ticketId, loyaltySessionId, requestId, deviceId, deviceName, maxPendingInterval, ticketExpiryTime,
                    loyaltySessionExpiryTime, deployDateLong, this.userCategoryService);

        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            authenticationResponse = new AuthenticationResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            authenticationResponse = new AuthenticationResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", authenticationResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refreshUserData", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return authenticationResponse;
    }
    
    @Override
    public PartnerListInfoDataResponse getPartnerListInfo(String requestId, String ticketId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        PartnerListInfoDataResponse partnerListInfoDataResponse = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPartnerListInfo", requestId, "opening", ActivityLog.createLogMessage(inputParameters));
        try {
            partnerListInfoDataResponse = userV2GetPartnerListAction.execute(ticketId, requestId);
        }

        catch (EJBException ex) {
            partnerListInfoDataResponse = new PartnerListInfoDataResponse();
            partnerListInfoDataResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", partnerListInfoDataResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPartnerListInfo", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return partnerListInfoDataResponse;
    }

    @Override
    public PartnerActionUrlResponse getPartnerActionUrl(String requestId, String ticketId, String partnerId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("partnerId", partnerId));

        PartnerActionUrlResponse partnerActionUrlResponse = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPartnerActionUrl", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        try {
            partnerActionUrlResponse = userV2GetPartnerActionUrlAction.execute(ticketId, requestId, partnerId);
        }

        catch (EJBException ex) {
            partnerActionUrlResponse = new PartnerActionUrlResponse();
            partnerActionUrlResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", partnerActionUrlResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPartnerActionUrl", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return partnerActionUrlResponse;
    }

    @Override
    public CategoryEarnDataDetailResponse getCategoryDetailList(String requestId, String ticketId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        CategoryEarnDataDetailResponse response = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCategoryDetailList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));
        try {
            response = userV2GetCategoryListAction.execute(ticketId, requestId);
        }

        catch (EJBException ex) {
            response = new CategoryEarnDataDetailResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCategoryDetailList", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        return response;

    }

    @Override
    public AwardDetailDataResponse getAwardList(String requestId, String ticketId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        AwardDetailDataResponse response = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getAwardList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));
        try {
            response = userV2GetAwardListAction.execute(ticketId, requestId, this.fidelityService, this.dwhAdapterService);
        }

        catch (EJBException ex) {
            response = new AwardDetailDataResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getAwardList", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        return response;

    }
    
    @Override
    public MissionDetailDataResponse getMissionList(String requestId, String ticketId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        MissionDetailDataResponse response = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMissionList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));
        try {
        	
        	String crmSfActive="0";
        	try {
        		crmSfActive = parametersService.getParamValue(PARAM_CRM_SF_ACTIVE);
    		} catch (ParameterNotFoundException e) {
    			e.printStackTrace();
    		}
        	
        	if(crmSfActive.equalsIgnoreCase("0"))
        		response = userV2GetMissionListAction.execute(ticketId, requestId,this.crmService);
        	else
        		response = userV2GetMissionSfListAction.execute(ticketId, requestId,this.crmService);
        }

        catch (EJBException ex) {
            response = new MissionDetailDataResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMissionList", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        return response;

    }

    @Override
    public RedemptionAwardResponse getRedemptionAward(String requestId, String ticketId, String awardId, String cardPartner, String pin) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("awardId", awardId));
        inputParameters.add(new Pair<String, String>("cardPartner", cardPartner));
        inputParameters.add(new Pair<String, String>("pin", pin));

        RedemptionAwardResponse response = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getRedemptionAward", requestId, "opening", ActivityLog.createLogMessage(inputParameters));
        try {

            response = userV2GetRedemptionAwardAction.execute(ticketId, requestId, awardId, cardPartner, pin);
        }

        catch (EJBException ex) {
            response = new RedemptionAwardResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getRedemptionAward", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public BrandDataDetailResponse getBrandDetailList(String requestId, String ticketId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        BrandDataDetailResponse response = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getBrandDetailList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        try {
            response = userV2GetBrandListAction.execute(ticketId, requestId);
        }

        catch (EJBException ex) {
            response = new BrandDataDetailResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getBrandDetailList", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        return response;

    }
    
    @Override
    public String addPlateNumber(String requestId, String ticketId, String plateNumber, String description) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("plateNumber", plateNumber));
        inputParameters.add(new Pair<String, String>("description", description));

        String response = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "addPlateNumber", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        try {
            response = userV2AddPlateNumberAction.execute(ticketId, requestId, plateNumber, description);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "addPlateNumber", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        return response;
    }

    @Override
    public String removePlateNumber(String requestId, String ticketId, Long id) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", String.valueOf(id)));

        String response = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "removePlateNumber", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        try {
            response = userV2RemovePlateNumberAction.execute(ticketId, requestId, id);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "removePlateNumber", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        return response;
    }
    
    @Override
    public String setDefaultPlateNumber(String requestId, String ticketId, Long id) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", String.valueOf(id)));

        String response = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setDefaultPlateNumber", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        try {
            response = userV2SetDefaultPlateNumberAction.execute(ticketId, requestId, id);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setDefaultPlateNumber", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        return response;
    }

    @Override
    public GetReceiptDataResponse getReceiptResponse(String requestId, String ticketId, String type, String transactionId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("type", type));
        inputParameters.add(new Pair<String, String>("transactionId", transactionId));
        
        GetReceiptDataResponse response = null;
        
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getReceiptResponse", requestId, "opening", ActivityLog.createLogMessage(inputParameters));
        
        try {
            response = userV2GetReceiptAction.execute(ticketId, requestId, type, transactionId, proxyHost, proxyPort, proxyNoHosts,this.userCategoryService,stringSubstitution);
        }

        catch (EJBException ex) {
            response = new GetReceiptDataResponse();
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        
        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getReceiptResponse", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        return response;
    }

    @Override
    public GetHomePartnerListResult getHomePartnerListInfo(String requestId, String ticketId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        GetHomePartnerListResult getHomePartnerListResult = null;

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getHomePartnerListInfo", requestId, "opening", ActivityLog.createLogMessage(inputParameters));
        try {
            getHomePartnerListResult = userV2GetHomePartnerListAction.execute(ticketId, requestId, this.parametersService);
        }

        catch (EJBException ex) {
            getHomePartnerListResult = new GetHomePartnerListResult();
            getHomePartnerListResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", getHomePartnerListResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getHomePartnerListInfo", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return getHomePartnerListResult;
    }
    
    @Override
    public ExternalProviderAuthenticationResponse externalProviderAuthentication(String username, String password, String accessToken, String externalProvider, String requestId, String deviceId, String deviceName, String deviceToken) {

        ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = null;

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("username", username));
        inputParameters.add(new Pair<String, String>("password", password));
        inputParameters.add(new Pair<String, String>("accessToken", accessToken));
        inputParameters.add(new Pair<String, String>("externalProvider", externalProvider));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deviceId", deviceId));
        inputParameters.add(new Pair<String, String>("deviceName", deviceName));
        inputParameters.add(new Pair<String, String>("deviceToken", deviceToken));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "externalProviderAuthentication", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        SmsServiceRemote smsService = null;

        try {

            smsService = EJBHomeCache.getInstance().getSmsService();
            String sendingType = this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE);
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_SMS_MAX_RETRY_ATTEMPS));
            Long deployDateLong = Long.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_ENIPAY_MIGRATION_TIMESTAMP));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
            String userBlockException = this.parametersService.getParamValue(UserV2Service.PARAM_USER_BLOCK_EXCEPTION);

            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();

            Integer maxPendingInterval = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_MAX_PENDING_INTERVAL));
            Integer ticketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_TICKET_EXPIRY_TIME));
            Integer loyaltySessionExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOYALTY_SESSION_EXPIRY_TIME));
            Integer loginAttemptsLimit = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOGIN_ATTEMPTS_THRESHOLD));
            Integer loginLockExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOGIN_LOCK_EXPIRY_TIME));
            Boolean refuelingOauth2Active = Boolean.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_REFUELING_OAUTH2_ACTIVE));

            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            externalProviderAuthenticationResponse = userV2ExternalProviderAuthenticationAction.execute(parkingService,refuelingNotificationService,refuelingNotificationServiceOAuth2,username,password,accessToken, externalProvider, requestId, deviceId, deviceName, deviceToken, this.proxyHost,
                    this.proxyPort, this.proxyNoHosts, maxPendingInterval, ticketExpiryTime, loyaltySessionExpiryTime, loginAttemptsLimit, loginLockExpiryTime,
                    userBlockExceptionList, this.userCategoryService, this.unavailabilityPeriodService, this.dwhAdapterService, this.pushNotificationService, maxRetryAttemps,
                    sendingType, emailSender, smsService, deployDateLong, refuelingOauth2Active, privateKeyPEM, stringSubstitution);

        }
        catch (ParameterNotFoundException ex) {
            ex.printStackTrace();
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
            externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
            externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", externalProviderAuthenticationResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "externalProviderAuthentication", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return externalProviderAuthenticationResponse;
    }

    @Override
    public AuthenticationBusinessResponse authenticationBusiness(String email, String passwordHash, String requestId, String deviceId, String deviceName, String source) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("email", email));
        inputParameters.add(new Pair<String, String>("passwordHash", passwordHash));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deviceId", deviceId));
        inputParameters.add(new Pair<String, String>("deviceName", deviceName));
        inputParameters.add(new Pair<String, String>("source", source));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "authenticationBusiness", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        SmsServiceRemote smsService = null;

        AuthenticationBusinessResponse authenticationResponse = null;
        try {

            smsService = EJBHomeCache.getInstance().getSmsService();
            String sendingType = this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE);
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_SMS_MAX_RETRY_ATTEMPS));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            Integer maxPendingInterval = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_MAX_PENDING_INTERVAL));
            Integer ticketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_TICKET_EXPIRY_TIME));
            Integer loginAttemptsLimit = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOGIN_ATTEMPTS_THRESHOLD));
            Integer loginLockExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOGIN_LOCK_EXPIRY_TIME));

            authenticationResponse = userBusinessAuthenticationAction.execute(email, passwordHash, privateKeyPEM, requestId, deviceId, deviceName,
                    maxPendingInterval, ticketExpiryTime, loginAttemptsLimit, loginLockExpiryTime, this.userCategoryService, 
                    maxRetryAttemps, sendingType, emailSender, smsService, source);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            authenticationResponse = new AuthenticationBusinessResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            authenticationResponse = new AuthenticationBusinessResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", authenticationResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "authenticationBusiness", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return authenticationResponse;
    }
    
    @Override
    public String createUserBusiness(String ticketId, String requestId, User user) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("user", user.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createUserBusiness", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String activationLink = this.parametersService.getParamValue(UserV2Service.PARAM_ACTIVATION_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String checkFiscalCode = this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_FISCAL_CODE);
            Integer verificationCodeExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_CODE_EXPIRY_TIME));
            Double initialCap = Double.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_INITIAL_CAP));
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);
            Boolean createUserMobilePhoneMandatory = Boolean.valueOf(parametersService.getParamValue(UserV2Service.PARAM_CREATE_USER_MOBILE_PHONE_MANDATORY));
            Boolean checkMobilePhoneUnique = Boolean.valueOf(parametersService.getParamValue(UserV2Service.PARAM_CHECK_MOBILE_PHONE_UNIQUE));
            String checkMailInBlacklist = this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_BLACKLIST_MAIL);
            Integer verificationNumberDevice = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_NUMBER_DEVICE));
            String checkBirthPlace = this.parametersService.getParamValue(UserV2Service.PARAM_CHECK_BIRTH_PLACE);
            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
            String sendingType = this.parametersService.getParamValue(UserV2Service.PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_SMS_MAX_RETRY_ATTEMPS));

            response = userBusinessCreateAction.execute(ticketId, requestId, user, checkFiscalCode, verificationCodeExpiryTime, activationLink, initialCap, createUserMobilePhoneMandatory, 
                    checkMobilePhoneUnique, emailSender, checkMailInBlacklist, verificationNumberDevice, checkBirthPlace, sendingType, maxRetryAttemps, smsService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createUserBusiness", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
    

    @Override
    public String updateBusinessData(String ticketId, String requestId, PersonalDataBusiness personalDataBusiness) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        
        String str = "{ ";
        if (personalDataBusiness != null) {
            Field[] fields = personalDataBusiness.getClass().getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                boolean accessible = field.isAccessible();
                field.setAccessible(true);
                
                if (field.getName().equals("serialVersionUID")) {
                    continue;
                }
                
                Object value = null;
                
                try {
                    value = field.get(personalDataBusiness);
                }
                catch (IllegalAccessException ex) {
                    System.err.println("Errore nella lettura del valore del campo " + field.getName() + ": " + ex.getMessage());    
                }
                
                str += field.getName() + ": " + (value != null ? value.toString() : "");
                if (i < (fields.length - 1)) {
                    str += "; ";
                }
                field.setAccessible(accessible);
            }
        }

        str += "}";
        inputParameters.add(new Pair<String, String>("personalDataBusiness", str));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateBusinessData", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = userBusinessUpdateBusinessDataAction.execute(ticketId, requestId, personalDataBusiness);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateBusinessData", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
        
    }
    
    @Override
    public RetrieveTermsOfServiceData retrieveTermsOfServiceBusiness(String ticketId, String requestId, Boolean isOptional) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTermsOfServiceBusiness", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveTermsOfServiceData retrieveTermsOfServiceData = null;
        try {

            //Integer userType = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_CREATE_USER_USERTYPE));

            retrieveTermsOfServiceData = userV2RetrieveTermsOfServiceAction.execute(ticketId, requestId, UserCategoryType.BUSINESS, userCategoryService, isOptional);
        }
        /*
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
            retrieveTermsOfServiceData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }*/
        catch (EJBException ex) {
            retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
            retrieveTermsOfServiceData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveTermsOfServiceData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTermsOfServiceBusiness", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveTermsOfServiceData;
    }
    
    @Override
    public AuthenticationBusinessResponse refreshUserDataBusiness(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refreshUserDataBusiness", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AuthenticationBusinessResponse authenticationResponse = null;
        try {
            Integer ticketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_TICKET_EXPIRY_TIME));
            authenticationResponse = userBusinessRefreshUserDataAction.execute(ticketId, requestId, ticketExpiryTime);

        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            authenticationResponse = new AuthenticationBusinessResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            authenticationResponse = new AuthenticationBusinessResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", authenticationResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refreshUserDataBusiness", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return authenticationResponse;
    }
    
    @Override
    public String recoverUsernameBusiness(String ticketId, String requestId, String fiscalcode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("fiscalcode", fiscalcode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "recoverUsernameBusiness", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            //String passwordRecoveryLink = this.parametersService.getParamValue(UserService.PARAM_PASSWORD_RECOVERY_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            //Integer rescuePasswordExpirationDelay = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_RESCUE_PASSWORD_EXPIRATION_DELAY));
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userBusinessRecoverUsernameAction.execute(ticketId, requestId, fiscalcode, this.emailSender, userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "recoverUsernameBusiness", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String rescuePasswordBusiness(String ticketId, String requestId, String i_mail) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("mail", i_mail));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "rescuePasswordBusiness", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String passwordRecoveryLink = this.parametersService.getParamValue(UserV2Service.PARAM_PASSWORD_RECOVERY_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            Integer rescuePasswordExpirationDelay = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_RESCUE_PASSWORD_EXPIRATION_DELAY));
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userBusinessRescuePasswordAction.execute(ticketId, requestId, i_mail, this.emailSender, rescuePasswordExpirationDelay, passwordRecoveryLink, 
                    userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "rescuePasswordBusiness", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
    
    @Override
    public String requestResetPin(String ticketId, String requestId) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "requestResetPin", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            Integer rescuePasswordExpirationDelay = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_RESCUE_PASSWORD_EXPIRATION_DELAY));
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userV2RequestResetPinAction.execute(ticketId, requestId, this.emailSender, rescuePasswordExpirationDelay, userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "requestResetPin", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
    
    @Override
    public String cloneUserPaymentMethod(String ticketId, String requestId, String encodedPin) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("encodedPin", encodedPin));
        
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "cloneUserPaymentMethod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String emailSenderAddress = this.parametersService.getParamValue(UserV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserV2Service.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userBusinessClonePaymentMethodAction.execute(ticketId, requestId, encodedPin, emailSender);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "cloneUserPaymentMethod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
 
    @Override
    public ExternalAuthenticationResponse externalAuthentication(String accessToken, String provider, String requestId, String deviceId, String deviceName, String deviceToken) {

        ExternalAuthenticationResponse externalAuthenticationResponse = null;

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("accessToken", accessToken));
        inputParameters.add(new Pair<String, String>("provider", provider));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deviceId", deviceId));
        inputParameters.add(new Pair<String, String>("deviceName", deviceName));
        inputParameters.add(new Pair<String, String>("deviceToken", deviceToken));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "externalAuthentication", requestId, "opening", ActivityLog.createLogMessage(inputParameters));


        try {
            Double initialCap = Double.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_INITIAL_CAP));
            Integer ticketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_TICKET_EXPIRY_TIME));
            Integer loyaltySessionExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserV2Service.PARAM_LOYALTY_SESSION_EXPIRY_TIME));

            externalAuthenticationResponse = userV2ExternalAuthenticationAction.execute(accessToken, provider, initialCap, requestId, deviceId, deviceName, deviceToken, ticketExpiryTime, loyaltySessionExpiryTime, userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            ex.printStackTrace();
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            externalAuthenticationResponse = new ExternalAuthenticationResponse();
            externalAuthenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            externalAuthenticationResponse = new ExternalAuthenticationResponse();
            externalAuthenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", externalAuthenticationResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "externalAuthentication", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return externalAuthenticationResponse;
    }

    @Override
    public String setActiveDpan(String requestId, String ticketId, String dpan, String status) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("dpan", dpan));
        inputParameters.add(new Pair<String, String>("status", status));
        
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setActiveDpan", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        
        try {
            response = userV2SetActiveDpanAction.execute(ticketId, requestId, dpan, status, this.userCategoryService, this.cardInfoService);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setActiveDpan", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
    
}
