package com.techedge.mp.core.business.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.UserBean;

@Stateless
public class TransactionDAO extends GenericDAO<TransactionBean> {

	public TransactionDAO() {

		super(TransactionBean.class);

	}
	
	public Boolean createTransaction(TransactionBean transactionBean){
		
		System.out.println("method createTransaction: transactionID: " + transactionBean.getTransactionID());
		
		super.save(transactionBean);
		
		return true;
	}
	
	public void save(TransactionBean transactionBean){
		
		System.out.println("method save: transactionID: " + transactionBean.getTransactionID());
		
		super.save(transactionBean);
	}
	
	public void delete(TransactionBean transactionBean){
		
		System.out.println("method delete: transactionID: " + transactionBean.getTransactionID());
		
		super.delete(transactionBean.getId(), TransactionBean.class);
	}
	
	public TransactionBean findTransactionByID(String transactionID){
    	
		System.out.println("method findTransactionByID transactionID: " + transactionID);
        
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("transactionID", transactionID);     
 
        return super.findOneResult(TransactionBean.FIND_BY_ID, parameters);
        
    }
	
	public List<TransactionBean> findTransactionsByUser(UserBean userBean){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userBean", userBean);     
 
        return super.find(TransactionBean.FIND_BY_USER, parameters);
        
    }
	
	public List<TransactionBean> findTransactionsByUserAndDate(UserBean userBean, Date startDate, Date endDate){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userBean", userBean);
        parameters.put("startDate", startDate);
        parameters.put("endDate", endDate);
 
        return super.find(TransactionBean.FIND_BY_USER_AND_DATE, parameters);
        
    }
	
	public List<TransactionBean> findActiveTransactionsByUser(UserBean userBean){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userBean", userBean);     
 
        return super.find(TransactionBean.FIND_ACTIVE_BY_USER, parameters);
        
    }
	
	public List<TransactionBean> findTransactionsByFinalStatusType(String finalStatusType){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("finalStatusType", finalStatusType);     
 
        return super.find(TransactionBean.FIND_BY_FINAL_STATUS_TYPE, parameters);
        
    }
	
	public List<TransactionBean> findTransactionsByFinalStatusTypeAndNotificationStatus(String finalStatusType, Boolean gfgNotification){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("finalStatusType", finalStatusType);
        parameters.put("gfgNotification", gfgNotification);
 
        return super.find(TransactionBean.FIND_BY_FINAL_STATUS_TYPE_AND_NOTIFICATION, parameters);
        
    }
	
//	public TransactionBean updateTransaction(String transactionID){
//    	
//        Map<String, Object> parameters = new HashMap<String, Object>();
//        parameters.put("transactionID", transactionID);     
// 
//        return super.update(TransactionBean.UPDATE_BY_ID, parameters);
//        
//    }
	
	

}
