package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.UserBean;

@Stateless
public class UserDAO extends GenericDAO<UserBean> {
	
	public UserDAO() {
		
        super(UserBean.class);
    }
	
	public Boolean createUser(UserBean userBean){
		
		super.save(userBean);
		
		return true;
	}
 
    public UserBean findUserByPersonalDataBean(PersonalDataBean personalDataBean){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("personalDataBean", personalDataBean);
 
        return super.findOneResult(UserBean.FIND_BY_PERSONALDATABEAN, parameters);
    }
}