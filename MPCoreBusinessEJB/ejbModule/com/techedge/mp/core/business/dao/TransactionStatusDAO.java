package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;

@Stateless
public class TransactionStatusDAO extends GenericDAO<TransactionStatusBean> {

	
	public TransactionStatusDAO() {

		super(TransactionStatusBean.class);

	}
	
	
	public Boolean createTransactionStatus(TransactionStatusBean transactionStatusBean) {
		
		System.out.println("method createTransactionStatus: transactionID: " + transactionStatusBean.getTransactionBean().getTransactionID() + ", status: " + transactionStatusBean.getStatus() + ",subStatus: " + transactionStatusBean.getSubStatus());
		
		super.save(transactionStatusBean);
		
		return true;
	}
	
	
	public TransactionStatusBean findTransactionBeanByTransaction(TransactionBean transactionBean){
    	
		System.out.println("method findTransactionBeanByTransaction transactionID: " + transactionBean.getTransactionID());
        
		Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("transactionBean", transactionBean);     
 
        return super.findOneResult(TransactionStatusBean.FIND_BY_TRANSACTION, parameters);
        
    }
	
	
	public void delete(TransactionStatusBean transactionStatusBean){
		
		System.out.println("method delete: transactionStatusID: " + transactionStatusBean.getId());
		
		super.delete(transactionStatusBean.getId(), TransactionStatusBean.class);
	}
	
}
