package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.TransactionEventHistoryBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;

@Stateless
public class TransactionEventHistoryDAO extends GenericDAO<TransactionEventHistoryBean> {

	
	public TransactionEventHistoryDAO() {

		super(TransactionEventHistoryBean.class);

	}
	
	
	public Boolean createTransactionEventHistory(TransactionEventHistoryBean transactionEventBean) {
		
		System.out.println("method createTransactionEvent: transactionID: " + transactionEventBean.getTransactionHistoryBean().getTransactionID() + ", eventType: " + transactionEventBean.getEventType() + ",Status: " + transactionEventBean.getTransactionResult());
		
		super.save(transactionEventBean);
		
		return true;
	}
	
	
	public TransactionEventHistoryBean findTransactionBeanByTransaction(TransactionHistoryBean transactionBean){
    	
		System.out.println("method findTransactionBeanByTransaction transactionID: " + transactionBean.getTransactionID());
        
		Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("transactionBean", transactionBean);     
 
        return super.findOneResult(TransactionEventHistoryBean.FIND_BY_TRANSACTION, parameters);
        
    }
	
	
	public void delete(TransactionEventHistoryBean transactionEventBean){
		
		System.out.println("method delete: transactionStatusID: " + transactionEventBean.getId());
		
		super.delete(transactionEventBean.getId(), TransactionEventHistoryBean.class);
	}
	
}
