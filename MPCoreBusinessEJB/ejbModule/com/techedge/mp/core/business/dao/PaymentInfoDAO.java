package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.UserBean;

@Stateless
public class PaymentInfoDAO extends GenericDAO<PaymentInfoBean> {
	
	public PaymentInfoDAO() {
		
        super(PaymentInfoBean.class);
    }
	
	public Boolean createPaymentInfo(PaymentInfoBean paymentInfoBean){
		
		super.save(paymentInfoBean);
		
		return true;
	}
 
    public Boolean updatePaymentInfo(PaymentInfoBean paymentInfoBean){
    	
    	super.save(paymentInfoBean);
    	
    	return true;
    }
    
    public Boolean removePaymentInfo(PaymentInfoBean paymentInfoBean){
    	
    	super.delete(paymentInfoBean.getId(), PaymentInfoBean.class);
    	
    	return true;
    }
    
    public PaymentInfoBean findByUser(UserBean userBean){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userBean", userBean);     
                
        return super.findOneResult(PaymentInfoBean.FIND_BY_USERBEAN, parameters);
    }
    
    public List<PaymentInfoBean> findAllByUser(UserBean userBean){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userBean", userBean);     
 
        return super.find(PaymentInfoBean.FIND_BY_USERBEAN, parameters);
    }
}
