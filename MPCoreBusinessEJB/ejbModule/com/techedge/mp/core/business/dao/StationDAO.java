package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.StationBean;

@Stateless
public class StationDAO extends GenericDAO<StationBean> {

	public StationDAO() {
		
		super(StationBean.class);
		
	}
	
	public Boolean createStation(StationBean stationBean){
		
		super.save(stationBean);
		
		return true;
	}
	
	public StationBean findByID(String stationID){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("stationID", stationID);     
 
        return super.findOneResult(StationBean.FIND_BY_ID, parameters);
        
    }


}
