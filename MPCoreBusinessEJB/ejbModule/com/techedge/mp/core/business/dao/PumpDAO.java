package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.PumpBean;

@Stateless
public class PumpDAO extends GenericDAO<PumpBean> {
	
	public PumpDAO() {

		super(PumpBean.class);

	}
	
	public Boolean createPump(PumpBean pumpBean){
		
		super.save(pumpBean);
		
		return true;
	}
	
	public PumpBean findTransactionByID(String pumpID){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("pumpID", pumpID);     
 
        return super.findOneResult(PumpBean.FIND_BY_ID, parameters);
        
    }

}
