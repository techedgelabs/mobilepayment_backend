package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.PersonalDataBean;

@Stateless
public class PersonalDataDAO extends GenericDAO<PersonalDataBean> {
	
	public PersonalDataDAO() {
		
        super(PersonalDataBean.class);
    }
	
	public Boolean createPersonalData(PersonalDataBean personalDataBean){
		
		super.save(personalDataBean);
		
		return true;
	}
 
    public PersonalDataBean findPersonalDataByEmail(String email){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("email", email);
 
        return super.findOneResult(PersonalDataBean.FIND_BY_EMAIL, parameters);
    }
}