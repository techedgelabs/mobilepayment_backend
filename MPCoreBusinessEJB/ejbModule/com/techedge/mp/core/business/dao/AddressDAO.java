package com.techedge.mp.core.business.dao;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.AddressBean;

@Stateless
public class AddressDAO extends GenericDAO<AddressBean> {
	
	public AddressDAO() {
		
        super(AddressBean.class);
    }
	
	public Boolean createAddress(AddressBean addressBean){
		
		super.save(addressBean);
		
		return true;
	}
	
	/*
    public AddressBean findAddressByAddressId(long addressId){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("ticketId", ticketId);     
 
        return super.findOneResult(TicketBean.FIND_BY_TICKETID, parameters);
    }
    */
}
