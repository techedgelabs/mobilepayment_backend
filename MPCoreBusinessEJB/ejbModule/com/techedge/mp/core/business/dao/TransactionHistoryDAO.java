package com.techedge.mp.core.business.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.UserBean;

@Stateless
public class TransactionHistoryDAO extends GenericDAO<TransactionHistoryBean> {

	public TransactionHistoryDAO() {

		super(TransactionHistoryBean.class);

	}
	
	public Boolean createTransaction(TransactionHistoryBean transactionHistoryBean){
		
		System.out.println("method createTransaction: transactionID: " + transactionHistoryBean.getTransactionID());
		
		super.save(transactionHistoryBean);
		
		return true;
	}
	
	public void save(TransactionHistoryBean transactionHistoryBean){
		
		System.out.println("method save: transactionID: " + transactionHistoryBean.getTransactionID());
		
		super.save(transactionHistoryBean);
	}
	
	public TransactionHistoryBean findTransactionByID(String transactionID){
    	
		System.out.println("method findTransactionByID transactionID: " + transactionID);
        
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("transactionID", transactionID);     
 
        return super.findOneResult(TransactionHistoryBean.FIND_BY_ID, parameters);
        
    }
	
	public List<TransactionHistoryBean> findTransactionsByUser(UserBean userBean){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userBean", userBean);     
 
        return super.find(TransactionHistoryBean.FIND_BY_USER, parameters);
        
    }
	
	public List<TransactionHistoryBean> findActiveTransactionsByUser(UserBean userBean){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userBean", userBean);     
 
        return super.find(TransactionHistoryBean.FIND_ACTIVE_BY_USER, parameters);
        
    }
	
	public List<TransactionHistoryBean> findTransactionsByUserAndDate(UserBean userBean, Date startDate, Date endDate){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userBean", userBean);
        parameters.put("startDate", startDate);
        parameters.put("endDate", endDate);
 
        return super.find(TransactionHistoryBean.FIND_BY_USER_AND_DATE, parameters);
        
    }
	
//	public TransactionBean updateTransaction(String transactionID){
//    	
//        Map<String, Object> parameters = new HashMap<String, Object>();
//        parameters.put("transactionID", transactionID);     
// 
//        return super.update(TransactionBean.UPDATE_BY_ID, parameters);
//        
//    }
	
	

}
