package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.VerificationCodeBean;

@Stateless
public class VerificationCodeDAO extends GenericDAO<VerificationCodeBean> {
	
	public VerificationCodeDAO() {
		
        super(VerificationCodeBean.class);
    }
	
	public Boolean createTicket(VerificationCodeBean verificationCodeBean){
		
		super.save(verificationCodeBean);
		
		return true;
	}
 
    public VerificationCodeBean findVerificationCodeById(String verificationCodeId){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("verificationCodeId", verificationCodeId);     
 
        return super.findOneResult(VerificationCodeBean.FIND_BY_VERIFICATIONCODEID, parameters);
    }
}
