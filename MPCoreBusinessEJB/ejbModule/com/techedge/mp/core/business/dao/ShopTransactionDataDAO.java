package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.ShopTransactionDataBean;

@Stateless
public class ShopTransactionDataDAO extends GenericDAO<ShopTransactionDataBean> {
	
	public ShopTransactionDataDAO() {
		
        super(ShopTransactionDataBean.class);
    }
	
	public Boolean createShopTransactionData(ShopTransactionDataBean shopTransactionData){
		
		super.save(shopTransactionData);
		
		return true;
	}
	
	public Boolean updateShopTransactionData(ShopTransactionDataBean shopTransactionData){
		
		super.save(shopTransactionData);
		
		return true;
	}
 
    public ShopTransactionDataBean findShopTransactionDataByTransactionIdAndStatus(String transactionId, Integer status){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("transactionId", transactionId);
        parameters.put("status", status);
 
        return super.findOneResult(ShopTransactionDataBean.FIND_BY_TRANSACTIONIDANDSTATUS, parameters);
    }
    
    public ShopTransactionDataBean findShopTransactionDataByPaymentInfo(PaymentInfoBean paymentInfo){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("paymentInfo", paymentInfo);
 
        return super.findOneResult(ShopTransactionDataBean.FIND_BY_PAYMENTINFO, parameters);
    }
}
