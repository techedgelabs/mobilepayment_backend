package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.ParameterBean;

@Stateless
public class ParameterDAO extends GenericDAO<ParameterBean> {
	
	public ParameterDAO() {

		super(ParameterBean.class);

	}
	
	public Boolean createParameter(ParameterBean parameterBean){
		
		super.save(parameterBean);
		
		return true;
	}
	
	public ParameterBean findByName(String name){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("name", name);     
 
        return super.findOneResult(ParameterBean.FIND_BY_NAME, parameters);
        
    }

}
