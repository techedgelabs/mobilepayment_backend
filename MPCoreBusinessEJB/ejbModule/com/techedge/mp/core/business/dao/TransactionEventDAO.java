package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;

@Stateless
public class TransactionEventDAO extends GenericDAO<TransactionEventBean> {

	
	public TransactionEventDAO() {

		super(TransactionEventBean.class);

	}
	
	
	public Boolean createTransactionEvent(TransactionEventBean transactionEventBean) {
		
		System.out.println("method createTransactionEvent: transactionID: " + transactionEventBean.getTransactionBean().getTransactionID() + ", eventType: " + transactionEventBean.getEventType() + ",Status: " + transactionEventBean.getTransactionResult());
		
		super.save(transactionEventBean);
		
		return true;
	}
	
	
	public TransactionEventBean findTransactionBeanByTransaction(TransactionBean transactionBean){
    	
		System.out.println("method findTransactionBeanByTransaction transactionID: " + transactionBean.getTransactionID());
        
		Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("transactionBean", transactionBean);     
 
        return super.findOneResult(TransactionEventBean.FIND_BY_TRANSACTION, parameters);
        
    }
	
	
	public void delete(TransactionEventBean transactionEventBean){
		
		System.out.println("method delete: transactionStatusID: " + transactionEventBean.getId());
		
		super.delete(transactionEventBean.getId(), TransactionEventBean.class);
	}
	
}
