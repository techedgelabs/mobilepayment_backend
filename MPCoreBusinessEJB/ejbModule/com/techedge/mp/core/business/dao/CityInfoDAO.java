package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.CityInfoBean;

@Stateless
public class CityInfoDAO extends GenericDAO<CityInfoBean> {
	
	public CityInfoDAO() {

		super(CityInfoBean.class);

	}
	
	public Boolean createCityInfo(CityInfoBean cityInfoBean){
		
		super.save(cityInfoBean);
		
		return true;
	}
	
	public List<CityInfoBean> findCitiesByKeySearch(String key, Integer maxResults){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("key", key);     
 
        return super.findWithLimit(CityInfoBean.FIND_BY_KEY, parameters, maxResults);
    }

}
