package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.TransactionStatusHistoryBean;

@Stateless
public class TransactionStatusHistoryDAO extends GenericDAO<TransactionStatusHistoryBean> {

	
	public TransactionStatusHistoryDAO() {

		super(TransactionStatusHistoryBean.class);

	}
	
	
	public Boolean createTransactionHistoryStatus(TransactionStatusHistoryBean transactionStatusHistoryBean) {
		
		System.out.println("method createTransactionStatusHistory: transactionID: " + transactionStatusHistoryBean.getTransactionHistoryBean().getTransactionID() + ", status: " + transactionStatusHistoryBean.getStatus() + ",subStatus: " + transactionStatusHistoryBean.getSubStatus());
		
		super.save(transactionStatusHistoryBean);
		
		return true;
	}
	
	
	public TransactionStatusHistoryBean findTransactionBeanByTransaction(TransactionHistoryBean transactionHistoryBean){
    	
		System.out.println("method findTransactionBeanByTransaction transactionID: " + transactionHistoryBean.getTransactionID());
        
		Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("transactionBean", transactionHistoryBean);     
 
        return super.findOneResult(TransactionStatusHistoryBean.FIND_BY_TRANSACTION, parameters);
        
    }
	
}
