package com.techedge.mp.core.business.dao;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.CardDepositTransactionBean;

@Stateless
public class CardDepositTransactionDAO extends GenericDAO<CardDepositTransactionBean> {

	public CardDepositTransactionDAO() {
		
		super(CardDepositTransactionBean.class);
	}
	
	public Boolean createCardDepositTransaction(CardDepositTransactionBean cardDepositTransactionBean){
		
		super.save(cardDepositTransactionBean);
		
		return true;
	}
	/*
    public TicketBean findTicketByTicketId(String ticketId){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("ticketId", ticketId);     
 
        return super.findOneResult(TicketBean.FIND_BY_TICKETID, parameters);
    }
    */
}
