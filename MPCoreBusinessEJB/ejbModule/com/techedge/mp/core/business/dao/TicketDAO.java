package com.techedge.mp.core.business.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.techedge.mp.core.business.model.TicketBean;

@Stateless
public class TicketDAO extends GenericDAO<TicketBean> {
	
	public TicketDAO() {
		
        super(TicketBean.class);
    }
	
	public Boolean createTicket(TicketBean ticketBean){
		
		super.save(ticketBean);
		
		return true;
	}
 
    public TicketBean findTicketByTicketId(String ticketId){
    	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("ticketId", ticketId);     
 
        return super.findOneResult(TicketBean.FIND_BY_TICKETID, parameters);
    }
}
