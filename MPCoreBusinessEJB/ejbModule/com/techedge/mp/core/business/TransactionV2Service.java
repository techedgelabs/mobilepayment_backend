package com.techedge.mp.core.business;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.techedge.mp.core.actions.transaction.v2.TransactionBusinessCreateRefuelAction;
import com.techedge.mp.core.actions.transaction.v2.TransactionV2CreateMulticardRefuelAction;
import com.techedge.mp.core.actions.transaction.v2.TransactionV2CreateRefuelAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.CreateMulticardRefuelResponse;
import com.techedge.mp.core.business.interfaces.CreateRefuelResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

/**
 * Session Bean implementation class TransactionV2Service
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TransactionV2Service implements TransactionV2ServiceRemote, TransactionV2ServiceLocal {

    private static final String                   PARAM_UIC                               = "UIC";
    private static final String                   PARAM_SERVER_NAME                       = "SERVER_NAME";
    private static final String                   PARAM_PAYMENT_TYPE                      = "PAYMENT_TYPE";
    private static final String                   PARAM_PIN_CHECK_MAX_ATTEMPTS            = "PIN_CHECK_MAX_ATTEMPTS";
    private static final String                   PARAM_STATUS_MAX_ATTEMPTS               = "STATUS_MAX_ATTEMPTS";
    private static final String                   PARAM_RECONCILIATION_MAX_ATTEMPTS       = "RECONCILIATION_MAX_ATTEMPTS";
    private static final String                   PARAM_EMAIL_SENDER_ADDRESS              = "EMAIL_SENDER_ADDRESS";
    private static final String                   PARAM_SMTP_HOST                         = "SMTP_HOST";
    private static final String                   PARAM_SMTP_PORT                         = "SMTP_PORT";
    private final static String                   PARAM_PROXY_HOST                        = "PROXY_HOST";
    private final static String                   PARAM_PROXY_PORT                        = "PROXY_PORT";
    private final static String                   PARAM_PROXY_NO_HOSTS                    = "PROXY_NO_HOSTS";
    private final static String                   PARAM_ACQUIRER_ID                       = "ACQUIRER_ID";
    private final static String                   PARAM_SHOPLOGIN_NEW_FLOW                = "SHOPLOGIN_NEW_FLOW";
    private final static String                   PARAM_USER_BLOCK_EXCEPTION              = "USER_BLOCK_EXCEPTION";
    private final static String                   PARAM_STRING_SUBSTITUTION_PATTERN       = "STRING_SUBSTITUTION_PATTERN";
    private final static String                   PARAM_MULTICARD_CURRENCY                = "MULTICARD_CURRENCY";
    private final static String                   PARAM_TRANSACTION_AMOUNT_FULL           = "TRANSACTION_AMOUNT_FULL";
    private final static String                   PARAM_TRANSACTION_AMOUNT_FULL_MULTICARD = "TRANSACTION_AMOUNT_FULL_MULTICARD";
    

    @EJB
    private TransactionV2CreateRefuelAction              transactionV2CreateRefuelAction;

    @EJB
    private TransactionBusinessCreateRefuelAction        transactionBusinessCreateRefuelAction;
    
    @EJB
    private TransactionV2CreateMulticardRefuelAction     transactionV2CreateMulticardRefuelAction;
    

    private LoggerService                         loggerService                     = null;
    private ParametersService                     parametersService                 = null;
    private EmailSenderRemote                     emailSender                       = null;
    private FidelityServiceRemote                 fidelityService                   = null;
    private TransactionService                    transactionService                = null;
    private UserCategoryService                   userCategoryService               = null;
    private UnavailabilityPeriodService           unavailabilityPeriodService       = null;
    private PaymentServiceRemote                  paymentService                    = null;
    private StringSubstitution                    stringSubstitution                = null;

    /**
     * Default constructor.
     */
    public TransactionV2Service() {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get EmailSender object");

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.transactionService = EJBHomeCache.getInstance().getTransactionService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.unavailabilityPeriodService = EJBHomeCache.getInstance().getUnavailabilityPeriodService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        
        try {
            this.paymentService = EJBHomeCache.getInstance().getPaymentService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        String pattern = null;

        try {
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        stringSubstitution = new StringSubstitution(pattern);

    }

    @Override
    public CreateRefuelResponse createRefuel(String ticketID, String requestID, String encodedPin, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Long paymentMethodId, String paymentMethodType, String outOfRange, String refuelMode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("encodedPin", encodedPin));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("pumpNumber", pumpNumber.toString()));
        inputParameters.add(new Pair<String, String>("productID", productID));
        inputParameters.add(new Pair<String, String>("productDescription", productDescription));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        if (paymentMethodId != null) {
            inputParameters.add(new Pair<String, String>("paymentMethodId", paymentMethodId.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("paymentMethodId", "null"));
        }
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("outOfRange", outOfRange));
        inputParameters.add(new Pair<String, String>("refuelMode", refuelMode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createRefuel", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateRefuelResponse createRefuelResponse = null;
        try {

            String currency = this.parametersService.getParamValue(TransactionV2Service.PARAM_UIC);
            String serverName = this.parametersService.getParamValue(TransactionV2Service.PARAM_SERVER_NAME);
            String paymentType = this.parametersService.getParamValue(TransactionV2Service.PARAM_PAYMENT_TYPE);
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            Integer statusMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_STATUS_MAX_ATTEMPTS));
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            String emailSenderAddress = this.parametersService.getParamValue(TransactionV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(TransactionV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(TransactionV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(TransactionV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(TransactionV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(TransactionV2Service.PARAM_PROXY_NO_HOSTS);
            String shopLoginNewFlow = this.parametersService.getParamValue(TransactionV2Service.PARAM_SHOPLOGIN_NEW_FLOW);
            String acquirerID = this.parametersService.getParamValue(TransactionV2Service.PARAM_ACQUIRER_ID);
            String userBlockException = this.parametersService.getParamValue(TransactionV2Service.PARAM_USER_BLOCK_EXCEPTION);
            Integer transactionAmountFull = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_TRANSACTION_AMOUNT_FULL));
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            createRefuelResponse = transactionV2CreateRefuelAction.execute(ticketID, requestID, encodedPin, stationID, pumpID, pumpNumber, productID, productDescription, amount,
                    paymentType, paymentMethodId, paymentMethodType, outOfRange, refuelMode, shopLoginNewFlow, acquirerID, pinCheckMaxAttempts, statusMaxAttempts,
                    reconciliationMaxAttempts, transactionAmountFull, serverName, userBlockExceptionList, emailSender, userCategoryService, transactionService, gpService, fidelityService,
                    unavailabilityPeriodService, proxyHost, proxyPort, proxyNoHosts, currency, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createRefuelResponse = new CreateRefuelResponse();
            createRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get GPService object");
            createRefuelResponse = new CreateRefuelResponse();
            createRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            createRefuelResponse = new CreateRefuelResponse();
            createRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", createRefuelResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createRefuel", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createRefuelResponse;
    }

    @Override
    public CreateRefuelResponse createRefuelBusiness(String ticketID, String requestID, String encodedPin, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Long paymentMethodId, String paymentMethodType, String outOfRange, String refuelMode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("encodedPin", encodedPin));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("pumpNumber", pumpNumber.toString()));
        inputParameters.add(new Pair<String, String>("productID", productID));
        inputParameters.add(new Pair<String, String>("productDescription", productDescription));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        if (paymentMethodId != null) {
            inputParameters.add(new Pair<String, String>("paymentMethodId", paymentMethodId.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("paymentMethodId", "null"));
        }
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("outOfRange", outOfRange));
        inputParameters.add(new Pair<String, String>("refuelMode", refuelMode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createRefuelBusiness", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateRefuelResponse createRefuelResponse = null;
        try {

            String currency = this.parametersService.getParamValue(TransactionV2Service.PARAM_UIC);
            String serverName = this.parametersService.getParamValue(TransactionV2Service.PARAM_SERVER_NAME);
            String paymentType = this.parametersService.getParamValue(TransactionV2Service.PARAM_PAYMENT_TYPE);
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            Integer statusMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_STATUS_MAX_ATTEMPTS));
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            String emailSenderAddress = this.parametersService.getParamValue(TransactionV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(TransactionV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(TransactionV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(TransactionV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(TransactionV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(TransactionV2Service.PARAM_PROXY_NO_HOSTS);
            String shopLoginNewFlow = this.parametersService.getParamValue(TransactionV2Service.PARAM_SHOPLOGIN_NEW_FLOW);
            String acquirerID = this.parametersService.getParamValue(TransactionV2Service.PARAM_ACQUIRER_ID);
            String userBlockException = this.parametersService.getParamValue(TransactionV2Service.PARAM_USER_BLOCK_EXCEPTION);
            Integer transactionAmountFull = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_TRANSACTION_AMOUNT_FULL));
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            createRefuelResponse = transactionBusinessCreateRefuelAction.execute(ticketID, requestID, encodedPin, stationID, pumpID, pumpNumber, productID, productDescription, amount,
                    paymentType, paymentMethodId, paymentMethodType, outOfRange, refuelMode, shopLoginNewFlow, acquirerID, pinCheckMaxAttempts, statusMaxAttempts,
                    reconciliationMaxAttempts, transactionAmountFull, serverName, userBlockExceptionList, emailSender, userCategoryService, transactionService, gpService, unavailabilityPeriodService, currency);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createRefuelResponse = new CreateRefuelResponse();
            createRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get GPService object");
            createRefuelResponse = new CreateRefuelResponse();
            createRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            createRefuelResponse = new CreateRefuelResponse();
            createRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", createRefuelResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createRefuelBusiness", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createRefuelResponse;
    }
    
    @Override
    public CreateMulticardRefuelResponse createMulticardRefuel(String ticketID, String requestID, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Long paymentMethodId, String paymentCryptogram, String outOfRange, String refuelMode) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("pumpNumber", pumpNumber.toString()));
        inputParameters.add(new Pair<String, String>("productID", productID));
        inputParameters.add(new Pair<String, String>("productDescription", productDescription));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        inputParameters.add(new Pair<String, String>("paymentCryptogram", paymentCryptogram));
        inputParameters.add(new Pair<String, String>("outOfRange", outOfRange));
        inputParameters.add(new Pair<String, String>("refuelMode", refuelMode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMulticardRefuel", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateMulticardRefuelResponse createMulticardRefuelResponse = null;
        
        try {

            String serverName = this.parametersService.getParamValue(TransactionV2Service.PARAM_SERVER_NAME);
            String currency = this.parametersService.getParamValue(TransactionV2Service.PARAM_MULTICARD_CURRENCY);
            String paymentType = this.parametersService.getParamValue(TransactionV2Service.PARAM_PAYMENT_TYPE);
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            Integer statusMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_STATUS_MAX_ATTEMPTS));
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            String emailSenderAddress = this.parametersService.getParamValue(TransactionV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(TransactionV2Service.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(TransactionV2Service.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(TransactionV2Service.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(TransactionV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(TransactionV2Service.PARAM_PROXY_NO_HOSTS);
            String shopLoginNewFlow = this.parametersService.getParamValue(TransactionV2Service.PARAM_SHOPLOGIN_NEW_FLOW);
            String acquirerID = this.parametersService.getParamValue(TransactionV2Service.PARAM_ACQUIRER_ID);
            String userBlockException = this.parametersService.getParamValue(TransactionV2Service.PARAM_USER_BLOCK_EXCEPTION);
            Integer transactionAmountFullMulticard = Integer.valueOf(this.parametersService.getParamValue(TransactionV2Service.PARAM_TRANSACTION_AMOUNT_FULL_MULTICARD));
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            createMulticardRefuelResponse = transactionV2CreateMulticardRefuelAction.execute(ticketID, requestID, stationID, pumpID, pumpNumber, productID, productDescription, amount,
                    currency, paymentMethodId, paymentType, paymentCryptogram, outOfRange, refuelMode, shopLoginNewFlow, acquirerID, pinCheckMaxAttempts,
                    statusMaxAttempts, reconciliationMaxAttempts, transactionAmountFullMulticard, serverName, userBlockExceptionList, emailSender, userCategoryService, this,
                    gpService, paymentService, unavailabilityPeriodService, proxyHost, proxyPort, proxyNoHosts);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createMulticardRefuelResponse = new CreateMulticardRefuelResponse();
            createMulticardRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get service object");
            createMulticardRefuelResponse = new CreateMulticardRefuelResponse();
            createMulticardRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            createMulticardRefuelResponse = new CreateMulticardRefuelResponse();
            createMulticardRefuelResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", createMulticardRefuelResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMulticardRefuel", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createMulticardRefuelResponse;
    }
}
