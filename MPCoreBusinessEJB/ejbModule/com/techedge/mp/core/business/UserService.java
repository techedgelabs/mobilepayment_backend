package com.techedge.mp.core.business;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.techedge.mp.core.actions.multicard.InsertMulticardPaymentMethodAction;
import com.techedge.mp.core.actions.user.UserAuthenticationAction;
import com.techedge.mp.core.actions.user.UserCancelMobilePhoneUpdateAction;
import com.techedge.mp.core.actions.user.UserCheckAuthorizationAction;
import com.techedge.mp.core.actions.user.UserCheckAvailabilityAmountAction;
import com.techedge.mp.core.actions.user.UserCheckLoyaltySessionAction;
import com.techedge.mp.core.actions.user.UserCheckTransactionAuthorizationAction;
import com.techedge.mp.core.actions.user.UserCreateAction;
import com.techedge.mp.core.actions.user.UserCreateSystemUserAction;
import com.techedge.mp.core.actions.user.UserGetActiveVouchersAction;
import com.techedge.mp.core.actions.user.UserGetAvailableLoyaltyCardsAction;
import com.techedge.mp.core.actions.user.UserInfoRedemptionAction;
import com.techedge.mp.core.actions.user.UserInsertPaymentMethodAction;
import com.techedge.mp.core.actions.user.UserLoadPromoVoucherAction;
import com.techedge.mp.core.actions.user.UserLoadVoucherAction;
import com.techedge.mp.core.actions.user.UserLogoutAction;
import com.techedge.mp.core.actions.user.UserRecoverUsernameAction;
import com.techedge.mp.core.actions.user.UserRedemptionAction;
import com.techedge.mp.core.actions.user.UserRefundAvailableCapAction;
import com.techedge.mp.core.actions.user.UserRemoveActiveUserDevice;
import com.techedge.mp.core.actions.user.UserRemovePaymentMethodAction;
import com.techedge.mp.core.actions.user.UserRemoveVoucherAction;
import com.techedge.mp.core.actions.user.UserRescuePasswordAction;
import com.techedge.mp.core.actions.user.UserResendValidationAction;
import com.techedge.mp.core.actions.user.UserResetPinAction;
import com.techedge.mp.core.actions.user.UserRetrieveActiveDeviceAction;
import com.techedge.mp.core.actions.user.UserRetrieveAllPrefixAction;
import com.techedge.mp.core.actions.user.UserRetrieveCitiesDataAction;
import com.techedge.mp.core.actions.user.UserRetrieveDocumentAction;
import com.techedge.mp.core.actions.user.UserRetrievePaymentDataAction;
import com.techedge.mp.core.actions.user.UserRetrieveTermsOfServiceAction;
import com.techedge.mp.core.actions.user.UserSetDefaultLoyaltyCardAction;
import com.techedge.mp.core.actions.user.UserSetDefaultPaymentMethodAction;
import com.techedge.mp.core.actions.user.UserSetUseVoucherAction;
import com.techedge.mp.core.actions.user.UserSkipPaymentMethodConfigurationAction;
import com.techedge.mp.core.actions.user.UserUpdateAction;
import com.techedge.mp.core.actions.user.UserUpdateAvailableCapAction;
import com.techedge.mp.core.actions.user.UserUpdateEffectiveCapAction;
import com.techedge.mp.core.actions.user.UserUpdateMobilePhoneAction;
import com.techedge.mp.core.actions.user.UserUpdatePasswordAction;
import com.techedge.mp.core.actions.user.UserUpdatePinAction;
import com.techedge.mp.core.actions.user.UserUpdateSmsLogAction;
import com.techedge.mp.core.actions.user.UserUpdateTermsOfServiceAction;
import com.techedge.mp.core.actions.user.UserUpdateUserPaymentDataAction;
import com.techedge.mp.core.actions.user.UserValidateFieldAction;
import com.techedge.mp.core.actions.user.UserValidatePaymentMethodAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.AuthenticationResponse;
import com.techedge.mp.core.business.interfaces.CheckAvailabilityAmountData;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GetActiveVouchersData;
import com.techedge.mp.core.business.interfaces.GetAvailableLoyaltyCardsData;
import com.techedge.mp.core.business.interfaces.MulticardPaymentResponse;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;
import com.techedge.mp.core.business.interfaces.PaymentResponse;
import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveCitiesData;
import com.techedge.mp.core.business.interfaces.RetrieveDocumentData;
import com.techedge.mp.core.business.interfaces.RetrieveTermsOfServiceData;
import com.techedge.mp.core.business.interfaces.RetrieveUserDeviceData;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.ValidatePaymentMethodResponse;
import com.techedge.mp.core.business.interfaces.loyalty.InfoRedemptionResponse;
import com.techedge.mp.core.business.interfaces.loyalty.RedemptionResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.interfaces.user.UserUpdatePinResponse;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.document.encoder.business.DocumentServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

/**
 * Session Bean implementation class UserManager
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserService implements UserServiceRemote, UserServiceLocal {

    /*
     * private static final Integer TICKET_EXPIRY_TIME = 30; // minutes
     * private static final Integer VERIFICATION_CODE_EXPIRY_TIME = 172800; // minutes
     * private static final long MAX_PENDING_INTERVAL = 3600000; // hour in millis
     * private static final Integer CITIES_MAX_RESULTS = 10;
     * private static final Double INITIAL_CAP = 0.03;
     * private static final Double MAX_CHECK_AMOUNT = 2.00;
     * private static final Integer CHECK_AMOUNT_MAX_ATTEMPTS = 3;
     * private static final Integer PIN_CHECK_MAX_ATTEMPTS = 5;
     * public static final Integer PASSWORD_HISTORY_LENGTH = 6;
     * private static final Integer RESCUE_PASSWORD_EXPIRATION_DELAY = 5; // minutes
     */
    // Letti dai parametri di sistema
    //private static final String SHOP_LOGIN                     = "GESPAY61144";

    private static final String                      PARAM_SHOPLOGIN                             = "SHOPLOGIN";
    private static final String                      PARAM_SHOPLOGIN_CHECK                       = "SHOPLOGIN_CHECK";
    private static final String                      PARAM_SHOPLOGIN_CHECK_NEW_FLOW              = "SHOPLOGIN_CHECK_NEW_FLOW";
    private static final String                      PARAM_ACQUIRER_ID                           = "ACQUIRER_ID";
    private static final String                      PARAM_EMAIL_SENDER_ADDRESS                  = "EMAIL_SENDER_ADDRESS";
    private static final String                      PARAM_SMTP_HOST                             = "SMTP_HOST";
    private static final String                      PARAM_SMTP_PORT                             = "SMTP_PORT";
    private static final String                      PARAM_ACTIVATION_LINK                       = "ACTIVATION_LINK";
    private static final String                      PARAM_PASSWORD_RECOVERY_LINK                = "PASSWORD_RECOVERY_LINK";
    private static final String                      PARAM_TICKET_EXPIRY_TIME                    = "TICKET_EXPIRY_TIME";                   // minutes
    private static final String                      PARAM_VERIFICATION_CODE_EXPIRY_TIME         = "VERIFICATION_CODE_EXPIRY_TIME";        // minutes
    private static final String                      PARAM_MAX_PENDING_INTERVAL                  = "MAX_PENDING_INTERVAL";                 // hour in millis
    private static final String                      PARAM_CITIES_MAX_RESULTS                    = "CITIES_MAX_RESULTS";
    private static final String                      PARAM_INITIAL_CAP                           = "INITIAL_CAP";
    private static final String                      PARAM_MAX_CHECK_AMOUNT                      = "MAX_CHECK_AMOUNT";
    private static final String                      PARAM_CHECK_AMOUNT_MAX_ATTEMPTS             = "CHECK_AMOUNT_MAX_ATTEMPTS";
    private static final String                      PARAM_PIN_CHECK_MAX_ATTEMPTS                = "PIN_CHECK_MAX_ATTEMPTS";
    private static final String                      PARAM_PASSWORD_HISTORY_LENGTH               = "PASSWORD_HISTORY_LENGTH";
    private static final String                      PARAM_RESCUE_PASSWORD_EXPIRATION_DELAY      = "RESCUE_PASSWORD_EXPIRATION_DELAY";     // minutes
    private static final String                      PARAM_CHECK_AMOUNT_VALUE                    = "CHECK_AMOUNT_VALUE";                   // rand or double
    private static final String                      PARAM_LOGIN_ATTEMPTS_THRESHOLD              = "LOGIN_ATTEMPTS_THRESHOLD";
    private static final String                      PARAM_LOGIN_LOCK_EXPIRY_TIME                = "LOGIN_LOCK_EXPIRY_TIME";               //minutes
    private static final String                      PARAM_CHECK_FISCAL_CODE                     = "CHECK_FISCAL_CODE";
    private final static String                      PARAM_PROXY_HOST                            = "PROXY_HOST";
    private final static String                      PARAM_PROXY_PORT                            = "PROXY_PORT";
    private final static String                      PARAM_PROXY_NO_HOSTS                        = "PROXY_NO_HOSTS";
    private final static String                      PARAM_CHECK_CREDIT_CARD_UNIQUE              = "CHECK_CREDIT_CARD_UNIQUE";
    //private final static String                      PARAM_CREATE_USER_USERTYPE                  = "CREATE_USER_USERTYPE";
    private final static String                      PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE = "VERIFICATION_CODE_MOBILE_SENDING_TYPE";
    private final static String                      PARAM_CREATE_USER_MOBILE_PHONE_MANDATORY    = "CREATE_USER_MOBILE_PHONE_MANDATORY";
    private final static String                      PARAM_CHECK_MOBILE_PHONE_UNIQUE             = "CHECK_MOBILE_PHONE_UNIQUE";
    private final static String                      PARAM_RATIO_THRESHOLD                       = "RATIO_THRESHOLD";
    private final static String                      PARAM_VOUCHER_TRANSACTION_MIN_AMOUNT        = "VOUCHER_TRANSACTION_MIN_AMOUNT";
    private final static String                      PARAM_USER_BLOCK_EXCEPTION                  = "USER_BLOCK_EXCEPTION";
    private final static String                      PARAM_VOUCHER_DETAIL_ICON_URL               = "VOUCHER_DETAIL_ICON_URL";
    private final static String                      PARAM_VOUCHER_DETAIL_ICON_MASTERCARD_URL    = "VOUCHER_DETAIL_ICON_MASTERCARD_URL";
    private final static String                      PARAM_VOUCHER_DETAIL_ICON_MAKERFAIRE2017_URL = "VOUCHER_DETAIL_ICON_MAKERFAIRE2017_URL";
    private final static String                      PARAM_VOUCHER_DETAIL_ICON_MOTORSHOW2017_URL  = "VOUCHER_DETAIL_ICON_MOTORSHOW2017_URL";
    private final static String                      PARAM_SMS_MAX_RETRY_ATTEMPS                 = "SMS_MAX_RETRY_ATTEMPS";
    private final static String                      PARAM_CHECK_BLACKLIST_MAIL                  = "CHECK_BLACKLIST_MAIL";
    private final static String                      PARAM_LOYALTY_SESSION_EXPIRY_TIME           = "LOYALTY_SESSION_EXPIRY_TIME";          //minutes
    private static final String                      PARAM_RECONCILIATION_MAX_ATTEMPTS           = "RECONCILIATION_MAX_ATTEMPTS";
    private final static String                      PARAM_STRING_SUBSTITUTION_PATTERN           = "STRING_SUBSTITUTION_PATTERN";
    private final static String                      PARAM_VERIFICATION_NUMBER_DEVICE            = "VERIFICATION_NUMBER_DEVICE";

    @EJB
    private UserCreateSystemUserAction               userCreateSystemUserAction;

    @EJB
    private UserCreateAction                         userCreateAction;

    @EJB
    private UserUpdateAction                         userUpdateAction;

    @EJB
    private UserAuthenticationAction                 userAuthenticationAction;

    @EJB
    private UserLogoutAction                         userLogoutAction;

    @EJB
    private UserValidateFieldAction                  userValidateFieldAction;

    @EJB
    private UserValidatePaymentMethodAction          userValidatePaymentMethodAction;

    @EJB
    private UserUpdatePasswordAction                 userUpdatePasswordAction;

    @EJB
    private UserUpdatePinAction                      userUpdatePinAction;

    @EJB
    private UserInsertPaymentMethodAction            userInsertPaymentMethodAction;

    @EJB
    private UserRemovePaymentMethodAction            userRemovePaymentMethodAction;

    @EJB
    private UserSetDefaultPaymentMethodAction        userSetDefaultPaymentMethodAction;

    @EJB
    private UserRetrievePaymentDataAction            userRetrievePaymentDataAction;

    @EJB
    private UserUpdateUserPaymentDataAction          userUpdateUserPaymentDataAction;

    @EJB
    private UserCheckAuthorizationAction             userCheckAuthorizationAction;

    @EJB
    private UserRetrieveCitiesDataAction             userRetrieveCitiesDataAction;

    @EJB
    private UserCheckTransactionAuthorizationAction  userCheckTransactionAuthorizationAction;

    @EJB
    private UserUpdateAvailableCapAction             userUpdateAvailableCapAction;

    @EJB
    private UserUpdateEffectiveCapAction             userUpdateEffectiveCapAction;

    @EJB
    private UserRefundAvailableCapAction             userRefundAvailableCapAction;

    @EJB
    private UserRescuePasswordAction                 userRescuePasswordAction;

    @EJB
    private UserRecoverUsernameAction                userRecoverUsernameAction;

    @EJB
    private UserRetrieveTermsOfServiceAction         userRetrieveTermsOfServiceAction;

    @EJB
    private UserRetrieveDocumentAction               userRetrieveDocumentAction;

    @EJB
    private UserLoadVoucherAction                    userLoadVoucherAction;

    @EJB
    private UserLoadPromoVoucherAction               userLoadPromoVoucherAction;

    @EJB
    private UserGetActiveVouchersAction              userGetActiveVouchersAction;

    @EJB
    private UserRemoveVoucherAction                  userRemoveVoucherAction;

    @EJB
    private UserSetDefaultLoyaltyCardAction          userSetDefaultLoyaltyCardAction;

    @EJB
    private UserGetAvailableLoyaltyCardsAction       userGetAvailableLoyaltyCardsAction;

    @EJB
    private UserSetUseVoucherAction                  userSetUseVoucherAction;

    @EJB
    private UserResetPinAction                       userResetPinAction;

    @EJB
    private UserRetrieveAllPrefixAction              userRetrieveAllPrefixAction;

    @EJB
    private UserSkipPaymentMethodConfigurationAction userSkipPaymentMethodConfigurationAction;

    @EJB
    private UserCancelMobilePhoneUpdateAction        userCancelMobilePhoneUpdateAction;

    @EJB
    private UserUpdateMobilePhoneAction              userUpdateMobilePhoneAction;

    @EJB
    private UserResendValidationAction               userResendValidationAction;

    @EJB
    private UserCheckAvailabilityAmountAction        userCheckAvailabilityAmountAction;

    @EJB
    private UserUpdateTermsOfServiceAction           userUpdateTermsOfServiceAction;

    @EJB
    private UserUpdateSmsLogAction                   userUpdateSmsLogAction;

    @EJB
    private UserCheckLoyaltySessionAction            userCheckLoyaltySessionAction;

    @EJB
    private UserInfoRedemptionAction                 userInfoRedemptionAction;

    @EJB
    private UserRedemptionAction                     userRedemptionAction;

    @EJB
    private UserRetrieveActiveDeviceAction           userRetrieveActiveDevice;

    @EJB
    private UserRemoveActiveUserDevice               userRemoveActiveUserDevice;

    @EJB
    private InsertMulticardPaymentMethodAction   userInsertMulticardPaymentMethodAction;

    private ParametersService                        parametersService                           = null;
    private LoggerService                            loggerService                               = null;
    private EmailSenderRemote                        emailSender                                 = null;
    private DocumentServiceRemote                    documentService                             = null;
    private FidelityServiceRemote                    fidelityService                             = null;
    private UserCategoryService                      userCategoryService                         = null;
    private UnavailabilityPeriodService              unavailabilityPeriodService                 = null;
    private PushNotificationServiceRemote            pushNotificationService                     = null;
    private CardInfoServiceRemote                    cardInfoService                             = null;

    private StringSubstitution                       stringSubstitution                          = null;

    /*
     * private String shopLogin = null;
     * private String shopLoginCheck = null;
     * private String emailSenderAddress = null;
     * private String smtpHost = null;
     * private String smtpPort = null;
     * private String activationLink = null;
     * private String passwordRecoveryLink = null;
     */
    /**
     * Default constructor.
     */
    public UserService() throws EJBException {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get EmailSender object");

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ParametersService object");

            throw new EJBException(e);
        }

        try {
            this.documentService = EJBHomeCache.getInstance().getDocumentService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get DocumentService object");

            throw new EJBException(e);
        }

        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get FidelityService object");

            throw new EJBException(e);
        }

        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get UserCategoryService object");

            throw new EJBException(e);
        }

        try {
            this.unavailabilityPeriodService = EJBHomeCache.getInstance().getUnavailabilityPeriodService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        
        try {
            this.cardInfoService = EJBHomeCache.getInstance().getCardInfoService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get CardInfoService object");

            throw new EJBException(e);
        }
        
        String pattern = null;

        try {
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }
        
        stringSubstitution = new StringSubstitution(pattern);
    }

    @Override
    public String createSystemUser() {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createSystemUser", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = userCreateSystemUserAction.execute();
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createSystemUser", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String createUser(String ticketId, String requestId, User user, Long loyaltyCardId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("user", user.toString()));
        if (loyaltyCardId != null) {
            inputParameters.add(new Pair<String, String>("loyaltyCardId", loyaltyCardId.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("loyaltyCardId", ""));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createUser", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String activationLink = this.parametersService.getParamValue(UserService.PARAM_ACTIVATION_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(UserService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserService.PARAM_SMTP_PORT);
            String checkFiscalCode = this.parametersService.getParamValue(UserService.PARAM_CHECK_FISCAL_CODE);
            Integer verificationCodeExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_VERIFICATION_CODE_EXPIRY_TIME));
            Double initialCap = Double.valueOf(this.parametersService.getParamValue(UserService.PARAM_INITIAL_CAP));
            String proxyHost = this.parametersService.getParamValue(UserService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserService.PARAM_PROXY_NO_HOSTS);
            Integer userType = User.USER_TYPE_CUSTOMER;
            Boolean createUserMobilePhoneMandatory = Boolean.valueOf(parametersService.getParamValue(UserService.PARAM_CREATE_USER_MOBILE_PHONE_MANDATORY));
            Boolean checkMobilePhoneUnique = Boolean.valueOf(parametersService.getParamValue(UserService.PARAM_CHECK_MOBILE_PHONE_UNIQUE));
            String checkMailInBlacklist = this.parametersService.getParamValue(UserService.PARAM_CHECK_BLACKLIST_MAIL);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userCreateAction.execute(ticketId, requestId, user, userType, loyaltyCardId, checkFiscalCode, verificationCodeExpiryTime, activationLink, initialCap,
                    createUserMobilePhoneMandatory, checkMobilePhoneUnique, emailSender, fidelityService, checkMailInBlacklist);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createUser", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String updateUser(String ticketId, String requestId, User user) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("user", user.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateUser", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = userUpdateAction.execute(ticketId, requestId, user);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateUser", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public AuthenticationResponse authentication(String email, String password, String requestId, String deviceId, String deviceName, String deviceToken) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("email", email));
        inputParameters.add(new Pair<String, String>("password", password));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deviceId", deviceId));
        inputParameters.add(new Pair<String, String>("deviceName", deviceName));
        inputParameters.add(new Pair<String, String>("deviceToken", deviceToken));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "authentication", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        AuthenticationResponse authenticationResponse = null;
        try {
            String emailSenderAddress = this.parametersService.getParamValue(UserService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();

            Integer maxPendingInterval = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_MAX_PENDING_INTERVAL));
            Integer ticketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_TICKET_EXPIRY_TIME));
            Integer loginAttemptsLimit = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_LOGIN_ATTEMPTS_THRESHOLD));
            Integer loginLockExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_LOGIN_LOCK_EXPIRY_TIME));

            String userBlockException = this.parametersService.getParamValue(UserService.PARAM_USER_BLOCK_EXCEPTION);
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            authenticationResponse = userAuthenticationAction.execute(email, password, requestId, deviceId, deviceName, maxPendingInterval, ticketExpiryTime, loginAttemptsLimit,
                    loginLockExpiryTime, userBlockExceptionList, userCategoryService, unavailabilityPeriodService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            authenticationResponse = new AuthenticationResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            authenticationResponse = new AuthenticationResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (InterfaceNotFoundException e) {
            authenticationResponse = new AuthenticationResponse();
            authenticationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", authenticationResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "authentication", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return authenticationResponse;
    }

    @Override
    public String logout(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "logout", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = userLogoutAction.execute(ticketId, requestId);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "logout", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String validateField(String ticketId, String requestId, String deviceId, String verificationType, String verificationField, String verificationCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("deviceId", deviceId));
        inputParameters.add(new Pair<String, String>("verificationType", verificationType));
        inputParameters.add(new Pair<String, String>("verificationField", verificationField));
        inputParameters.add(new Pair<String, String>("verificationCode", verificationCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "validateField", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        SmsServiceRemote smsService = null;

        try {
            smsService = EJBHomeCache.getInstance().getSmsService();
            pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();

            String sendingType = this.parametersService.getParamValue(UserService.PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE);
            String emailSenderAddress = this.parametersService.getParamValue(UserService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserService.PARAM_PROXY_NO_HOSTS);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_SMS_MAX_RETRY_ATTEMPS));
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            Integer verificationNumberDevice = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_VERIFICATION_NUMBER_DEVICE));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userValidateFieldAction.execute(ticketId, requestId, deviceId, verificationType, sendingType, verificationField, verificationCode, maxRetryAttemps,
                    reconciliationMaxAttempts, verificationNumberDevice, this, emailSender, smsService, pushNotificationService, userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "validateField", requestId, "closing", "Error creating sms service: " + ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "validateField", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public ValidatePaymentMethodResponse validatePaymentMethod(String ticketId, String requestId, Long cardId, String cardType, Double verificationAmount) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("cardID", cardId.toString()));
        inputParameters.add(new Pair<String, String>("cardType", cardType));
        inputParameters.add(new Pair<String, String>("verificationAmount", verificationAmount.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "validateField", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        ValidatePaymentMethodResponse validatePaymentMethodResponse = null;

        try {
            validatePaymentMethodResponse = userValidatePaymentMethodAction.execute(ticketId, requestId, cardId, cardType, verificationAmount);
        }
        catch (EJBException ex) {
            validatePaymentMethodResponse = new ValidatePaymentMethodResponse();
            validatePaymentMethodResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", validatePaymentMethodResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "validateField", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return validatePaymentMethodResponse;
    }

    @Override
    public String updatePassword(String ticketId, String requestId, String oldPassword, String newPassword) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("oldPassword", oldPassword));
        inputParameters.add(new Pair<String, String>("newPassword", newPassword));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePassword", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            Integer passwordHistoryLenght = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_PASSWORD_HISTORY_LENGTH));

            response = userUpdatePasswordAction.execute(ticketId, requestId, oldPassword, newPassword, passwordHistoryLenght);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePassword", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public UserUpdatePinResponse updatePin(String ticketId, String requestId, Long cardId, String cardType, String oldPin, String newPin) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        if (cardId != null) {
            inputParameters.add(new Pair<String, String>("cardId", cardId.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("cardId", "null"));
        }
        inputParameters.add(new Pair<String, String>("cardType", cardType));
        inputParameters.add(new Pair<String, String>("oldPin", oldPin));
        inputParameters.add(new Pair<String, String>("newPin", newPin));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePin", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        UserUpdatePinResponse userUpdatePinResponse = new UserUpdatePinResponse();
        try {
            //          Double maxCheckAmount          = Double.valueOf(this.parametersService.getParamValue(UserService.PARAM_MAX_CHECK_AMOUNT));
            //          Integer checkAmountMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_CHECK_AMOUNT_MAX_ATTEMPTS));
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_PIN_CHECK_MAX_ATTEMPTS));

            userUpdatePinResponse = userUpdatePinAction.execute(ticketId, requestId, cardId, cardType, oldPin, newPin,
            //                  maxCheckAmount,
            //                  checkAmountMaxAttempts,
                    pinCheckMaxAttempts, this.userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            userUpdatePinResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            userUpdatePinResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", userUpdatePinResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePin", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return userUpdatePinResponse;
    }

    @Override
    public PaymentResponse insertPaymentMethod(String ticketId, String requestId, String paymentMethodType, String newPin) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("newPin", newPin));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "insertPaymentMethod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        PaymentResponse paymentResponse = null;
        try {
            String shopLogin = this.parametersService.getParamValue(UserService.PARAM_SHOPLOGIN);
            String shopLoginCheckNewFlow = this.parametersService.getParamValue(UserService.PARAM_SHOPLOGIN_CHECK_NEW_FLOW);
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            String checkAmountValue = this.parametersService.getParamValue(UserService.PARAM_CHECK_AMOUNT_VALUE);
            Double maxCheckAmount = Double.valueOf(this.parametersService.getParamValue(UserService.PARAM_MAX_CHECK_AMOUNT));

            paymentResponse = userInsertPaymentMethodAction.execute(ticketId, requestId, paymentMethodType, newPin, shopLogin, shopLoginCheckNewFlow, pinCheckMaxAttempts,
                    checkAmountValue, maxCheckAmount, this.userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            paymentResponse = new PaymentResponse();
            paymentResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            paymentResponse = new PaymentResponse();
            paymentResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", paymentResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "insertPaymentMethod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return paymentResponse;
    }
    
    @Override
    public String removePaymentMethod(String ticketId, String requestId, Long paymentMethodId, String paymentMethodType) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("paymentMethodId", paymentMethodId.toString()));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "removePaymentMethod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        
        try {
            String shopLogin = this.parametersService.getParamValue(UserService.PARAM_SHOPLOGIN);

            response = userRemovePaymentMethodAction.execute(ticketId, requestId, paymentMethodId, paymentMethodType, shopLogin, this.userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "removePaymentMethod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String setDefaultPaymentMethod(String ticketId, String requestId, Long paymentMethodId, String paymentMethodType) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("paymentMethodId", paymentMethodId.toString()));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setDefaultPaymentMethod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String shopLogin = this.parametersService.getParamValue(UserService.PARAM_SHOPLOGIN);

            response = userSetDefaultPaymentMethodAction.execute(ticketId, requestId, paymentMethodId, paymentMethodType, shopLogin);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setDefaultPaymentMethod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public PaymentInfoResponse retrievePaymentData(String ticketId, String requestId, Long cardId, String cardType) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrievePaymentData", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        PaymentInfoResponse paymentInfoResponse = null;
        try {
            Long maxPendingInterval = Long.valueOf(this.parametersService.getParamValue(UserService.PARAM_MAX_PENDING_INTERVAL));

            paymentInfoResponse = userRetrievePaymentDataAction.execute(ticketId, requestId, cardId, cardType, maxPendingInterval, this.userCategoryService, this.cardInfoService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            paymentInfoResponse = new PaymentInfoResponse();
            paymentInfoResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            paymentInfoResponse = new PaymentInfoResponse();
            paymentInfoResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", paymentInfoResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrievePaymentData", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return paymentInfoResponse;
    }

    @Override
    public String updateUserPaymentData(String transactionType, String transactionResult, String shopTransactionID, String bankTransactionID, String authorizationCode,
            String currency, String amount, String country, String buyerName, String buyerEmail, String errorCode, String errorDescription, String alertCode,
            String alertDescription, String TransactionKey, String token, String tokenExpiryMonth, String tokenExpiryYear, String cardBin, String TDLevel) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionType", transactionType));
        inputParameters.add(new Pair<String, String>("transactionResult", transactionResult));
        inputParameters.add(new Pair<String, String>("shopTransactionID", shopTransactionID));
        inputParameters.add(new Pair<String, String>("bankTransactionID", bankTransactionID));
        inputParameters.add(new Pair<String, String>("authorizationCode", authorizationCode));
        inputParameters.add(new Pair<String, String>("currency", currency));
        inputParameters.add(new Pair<String, String>("amount", amount));
        inputParameters.add(new Pair<String, String>("buyerName", buyerName));
        inputParameters.add(new Pair<String, String>("buyerEmail", buyerEmail));
        inputParameters.add(new Pair<String, String>("errorCode", errorCode));
        inputParameters.add(new Pair<String, String>("errorDescription", errorDescription));
        inputParameters.add(new Pair<String, String>("alertCode", alertCode));
        inputParameters.add(new Pair<String, String>("alertDescription", alertDescription));
        inputParameters.add(new Pair<String, String>("TransactionKey", TransactionKey));
        inputParameters.add(new Pair<String, String>("token", token));
        inputParameters.add(new Pair<String, String>("tokenExpiryMonth", tokenExpiryMonth));
        inputParameters.add(new Pair<String, String>("tokenExpiryYear", tokenExpiryYear));
        inputParameters.add(new Pair<String, String>("cardBin", cardBin));
        inputParameters.add(new Pair<String, String>("TDLevel", TDLevel));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateUserPaymentData", shopTransactionID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String shopLoginCheck = this.parametersService.getParamValue(UserService.PARAM_SHOPLOGIN_CHECK);
            String shopLoginCheckNewFlow = this.parametersService.getParamValue(UserService.PARAM_SHOPLOGIN_CHECK_NEW_FLOW);
            Integer checkAmountMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_CHECK_AMOUNT_MAX_ATTEMPTS));
            String acquirerId = this.parametersService.getParamValue(UserService.PARAM_ACQUIRER_ID);
            String emailSenderAddress = this.parametersService.getParamValue(UserService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserService.PARAM_PROXY_NO_HOSTS);
            boolean checkPaymentMethodExists = false;

            if (this.parametersService.getParamValue(UserService.PARAM_CHECK_CREDIT_CARD_UNIQUE) != null) {
                checkPaymentMethodExists = Boolean.parseBoolean(this.parametersService.getParamValue(UserService.PARAM_CHECK_CREDIT_CARD_UNIQUE));
            }

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            String groupAcquirer = "";
            String encodedSecretKey = "";

            response = userUpdateUserPaymentDataAction.execute(transactionType, transactionResult, shopTransactionID, bankTransactionID, authorizationCode, currency, amount,
                    country, buyerName, buyerEmail, errorCode, errorDescription, alertCode, alertDescription, TransactionKey, token, tokenExpiryMonth, tokenExpiryYear, cardBin,
                    TDLevel, checkAmountMaxAttempts, shopLoginCheck, shopLoginCheckNewFlow, checkPaymentMethodExists, acquirerId, groupAcquirer, encodedSecretKey, emailSender,
                    userCategoryService);

        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateUserPaymentData", shopTransactionID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String checkAuthorization(String ticketId, Integer operationType) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("operationType", operationType.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkAuthorization", ticketId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = userCheckAuthorizationAction.execute(ticketId, operationType);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkAuthorization", ticketId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public RetrieveCitiesData retrieveCities(String ticketId, String requestId, String searchKey) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("searchKey", searchKey));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveCities", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveCitiesData retrieveCitiesData = null;
        try {
            Integer citiesMaxResults = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_CITIES_MAX_RESULTS));

            retrieveCitiesData = userRetrieveCitiesDataAction.execute(ticketId, requestId, searchKey, citiesMaxResults);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrieveCitiesData = new RetrieveCitiesData();
            retrieveCitiesData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            retrieveCitiesData = new RetrieveCitiesData();
            retrieveCitiesData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveCitiesData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveCities", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveCitiesData;
    }

    @Override
    public String checkTransactionAuthorization(Double amount, String shopTransactionID, String valuta, String token, String operation) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        inputParameters.add(new Pair<String, String>("shopTransactionID", shopTransactionID));
        inputParameters.add(new Pair<String, String>("valuta", valuta));
        inputParameters.add(new Pair<String, String>("token", token));
        inputParameters.add(new Pair<String, String>("operation", operation));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkTransactionAuthorization", shopTransactionID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = userCheckTransactionAuthorizationAction.execute(amount, shopTransactionID, valuta, token, operation, this.userCategoryService);
        }
        catch (EJBException ex) {
            ex.printStackTrace();
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkTransactionAuthorization", shopTransactionID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public String updateAvailableCap(Double amount, String shopTransactionID, String valuta, String token) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        inputParameters.add(new Pair<String, String>("shopTransactionID", shopTransactionID));
        inputParameters.add(new Pair<String, String>("valuta", valuta));
        inputParameters.add(new Pair<String, String>("token", token));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateAvailableCap", shopTransactionID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = userUpdateAvailableCapAction.execute(amount, shopTransactionID, valuta, token);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateAvailableCap", shopTransactionID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String updateEffectiveCap(Double amount, String shopTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        inputParameters.add(new Pair<String, String>("shopTransactionID", shopTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateEffectiveCap", shopTransactionID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = userUpdateEffectiveCapAction.execute(amount, shopTransactionID);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateEffectiveCap", shopTransactionID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String refundAvailableCap(String shopTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("shopTransactionID", shopTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refundAvailableCap", shopTransactionID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = userRefundAvailableCapAction.execute(shopTransactionID);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refundAvailableCap", shopTransactionID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String rescuePassword(String ticketId, String requestId, String i_mail) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("mail", i_mail));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "rescuePassword", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            String passwordRecoveryLink = this.parametersService.getParamValue(UserService.PARAM_PASSWORD_RECOVERY_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(UserService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserService.PARAM_SMTP_PORT);
            Integer rescuePasswordExpirationDelay = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_RESCUE_PASSWORD_EXPIRATION_DELAY));
            String proxyHost = this.parametersService.getParamValue(UserService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userRescuePasswordAction.execute(ticketId, requestId, i_mail, this.emailSender, rescuePasswordExpirationDelay, passwordRecoveryLink, 
                    userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "rescuePassword", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String recoverUsername(String ticketId, String requestId, String fiscalcode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("fiscalcode", fiscalcode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "recoverUsername", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            //String passwordRecoveryLink = this.parametersService.getParamValue(UserService.PARAM_PASSWORD_RECOVERY_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(UserService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserService.PARAM_SMTP_PORT);
            //Integer rescuePasswordExpirationDelay = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_RESCUE_PASSWORD_EXPIRATION_DELAY));
            String proxyHost = this.parametersService.getParamValue(UserService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userRecoverUsernameAction.execute(ticketId, requestId, fiscalcode, this.emailSender, userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "recoverUsername", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public RetrieveDocumentData retrieveDocument(String ticketId, String requestId, String documentID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("documentID", documentID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveDocument", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveDocumentData retrieveDocumentData = null;
        try {
            retrieveDocumentData = userRetrieveDocumentAction.execute(ticketId, requestId, documentID, this.documentService);
        }

        catch (EJBException ex) {
            retrieveDocumentData = new RetrieveDocumentData();
            retrieveDocumentData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveDocumentData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveDocument", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveDocumentData;
    }

    @Override
    public RetrieveTermsOfServiceData retrieveTermsOfService(String ticketId, String requestId, Boolean isOptional) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTermsOfService", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveTermsOfServiceData retrieveTermsOfServiceData = null;
        try {

            //Integer userType = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_CREATE_USER_USERTYPE));

            retrieveTermsOfServiceData = userRetrieveTermsOfServiceAction.execute(ticketId, requestId, UserCategoryType.NEW_PAYMENT_FLOW, userCategoryService, isOptional);
        }
        /*
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
            retrieveTermsOfServiceData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }*/
        catch (EJBException ex) {
            retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
            retrieveTermsOfServiceData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveTermsOfServiceData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTermsOfService", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveTermsOfServiceData;
    }

    @Override
    public String loadVoucher(String ticketId, String requestId, String voucherCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("voucherCode", voucherCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "loadVoucher", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            // TODO - rendere parametrico questo valore
            Integer maxVoucherCount = 200;

            response = userLoadVoucherAction.execute(ticketId, requestId, voucherCode, maxVoucherCount, this.fidelityService, this.userCategoryService);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "loadVoucher", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String loadPromoVoucher(String voucherCode, Long userId, String verificationValue) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("voucherCode", voucherCode));
        inputParameters.add(new Pair<String, String>("userId", userId.toString()));
        inputParameters.add(new Pair<String, String>("verificationValue", verificationValue));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "loadPromoVoucher", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {

            response = userLoadPromoVoucherAction.execute(voucherCode, verificationValue, userId, this.fidelityService);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "loadPromoVoucher", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public GetActiveVouchersData getActiveVouchers(String ticketId, String requestId, Boolean refresh, String loyaltySessionID) {

        String refreshString = "";
        if (refresh == true) {
            refreshString = "true";
        }
        else {
            refreshString = "false";
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("refresh", refreshString));
        inputParameters.add(new Pair<String, String>("loyaltySessionID", (loyaltySessionID != null) ? loyaltySessionID : ""));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getActiveVouchers", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        GetActiveVouchersData getActiveVouchersData = null;

        try {
            Integer loyaltySessionExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_LOYALTY_SESSION_EXPIRY_TIME));
            String voucher_detail_icon                = parametersService.getParamValue(UserService.PARAM_VOUCHER_DETAIL_ICON_URL);
            String voucher_detail_icon_mastercard     = parametersService.getParamValue(UserService.PARAM_VOUCHER_DETAIL_ICON_MASTERCARD_URL);
            String voucher_detail_icon_makerfaire2017 = parametersService.getParamValue(UserService.PARAM_VOUCHER_DETAIL_ICON_MAKERFAIRE2017_URL);
            String voucher_detail_icon_motorshow2017  = parametersService.getParamValue(UserService.PARAM_VOUCHER_DETAIL_ICON_MOTORSHOW2017_URL);

            getActiveVouchersData = userGetActiveVouchersAction.execute(ticketId, requestId, refresh, this.fidelityService, voucher_detail_icon, voucher_detail_icon_mastercard, 
                    voucher_detail_icon_makerfaire2017, voucher_detail_icon_motorshow2017, loyaltySessionID, loyaltySessionExpiryTime);
        }

        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getActiveVouchersData = new GetActiveVouchersData();
            getActiveVouchersData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getActiveVouchersData = new GetActiveVouchersData();
            getActiveVouchersData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getActiveVouchersData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getActiveVouchers", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return getActiveVouchersData;
    }

    @Override
    public String removeVoucher(String ticketId, String requestId, String voucherCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("voucherCode", voucherCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "removeVoucher", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            // TODO - rendere parametrico questo valore
            Integer maxVoucherCount = 200;

            response = userRemoveVoucherAction.execute(ticketId, requestId, voucherCode, maxVoucherCount, this.fidelityService);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "removeVoucher", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String setDefaultLoyaltyCard(String ticketId, String requestId, String panCode, String eanCode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("panCode", panCode));
        inputParameters.add(new Pair<String, String>("eanCode", eanCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setDefaultLoyaltyCard", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {

            response = userSetDefaultLoyaltyCardAction.execute(ticketId, requestId, panCode, eanCode, this.fidelityService);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setDefaultLoyaltyCard", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public GetAvailableLoyaltyCardsData getAvailableLoyaltyCards(String ticketId, String requestId, String fiscalcode) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("fiscalcode", fiscalcode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getAvailableLoyaltyCards", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();

        try {
            getAvailableLoyaltyCardsData = userGetAvailableLoyaltyCardsAction.execute(ticketId, requestId, fiscalcode, this.fidelityService);
        }
        catch (Exception ex) {
            getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", getAvailableLoyaltyCardsData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getAvailableLoyaltyCard", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return getAvailableLoyaltyCardsData;
    }

    @Override
    public String setUseVoucher(String ticketId, String requestId, boolean useVoucher) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("useVoucher", String.valueOf(useVoucher)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setUseVoucher", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            response = userSetUseVoucherAction.execute(ticketId, requestId, useVoucher);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setUseVoucher", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String resetPin(String ticketId, String requestId, String password, String newPin) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("password", password));
        inputParameters.add(new Pair<String, String>("newPin", newPin));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "resetPin", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_PIN_CHECK_MAX_ATTEMPTS));

            response = userResetPinAction.execute(ticketId, requestId, password, newPin, pinCheckMaxAttempts, this.userCategoryService);
        }

        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "resetPin", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String skipPaymentMethodConfiguration(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "skipPaymentMethodConfiguration", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        String response = null;

        try {
            //String passwordRecoveryLink = this.parametersService.getParamValue(UserService.PARAM_PASSWORD_RECOVERY_LINK);
            String emailSenderAddress = this.parametersService.getParamValue(UserService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserService.PARAM_SMTP_PORT);
            //Integer rescuePasswordExpirationDelay = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_RESCUE_PASSWORD_EXPIRATION_DELAY));
            String proxyHost = this.parametersService.getParamValue(UserService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userSkipPaymentMethodConfigurationAction.execute(ticketId, requestId, this, this.userCategoryService, this.emailSender);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "skipPaymentMethodConfiguration", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public PrefixNumberResult retrieveAllPrefixNumber(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveAllPrefixNumber", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        PrefixNumberResult prefixNumberResult = new PrefixNumberResult();

        try {
            prefixNumberResult = userRetrieveAllPrefixAction.execute(ticketId, requestId);
        }
        catch (Exception ex) {
            prefixNumberResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", prefixNumberResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveAllPrefixNumber", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return prefixNumberResult;
    }

    @Override
    public String cancelMobilePhoneUpdate(String ticketId, String requestId, Long id) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("mobilePhoneId", id.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "cancelMobilePhoneUpdate", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String cellphone = "";

        try {
            cellphone = userCancelMobilePhoneUpdateAction.execute(ticketId, requestId, id);
        }
        catch (Exception ex) {
            cellphone = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", cellphone));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "cancelMobilePhoneUpdate", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return cellphone;
    }

    @Override
    public String updateMobilePhone(String ticketId, String requestId, String mobilePhonePrefix, String mobilePhoneNumber) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("mobilePhonePrefix", mobilePhonePrefix));
        inputParameters.add(new Pair<String, String>("mobilePhoneNumber", mobilePhoneNumber));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateMobilePhone", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        SmsServiceRemote smsService = null;

        try {
            smsService = EJBHomeCache.getInstance().getSmsService();
            String sendingType = this.parametersService.getParamValue(UserService.PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE);
            String emailSenderAddress = this.parametersService.getParamValue(UserService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserService.PARAM_PROXY_NO_HOSTS);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_SMS_MAX_RETRY_ATTEMPS));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            Boolean checkMobilePhoneUnique = Boolean.valueOf(parametersService.getParamValue(UserService.PARAM_CHECK_MOBILE_PHONE_UNIQUE));
            Integer verificationCodeExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_VERIFICATION_CODE_EXPIRY_TIME));

            response = userUpdateMobilePhoneAction.execute(ticketId, requestId, mobilePhonePrefix, mobilePhoneNumber, checkMobilePhoneUnique, verificationCodeExpiryTime,
                    maxRetryAttemps, sendingType, this, emailSender, smsService, userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "updateMobilePhone", requestId, "closing", "Error creating sms service: " + ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (Exception ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateMobilePhone", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String userResendValidation(String ticketId, String requestId, String id, String validitationType) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("id", (id != null) ? id : ""));
        inputParameters.add(new Pair<String, String>("validitationType", (validitationType != null) ? validitationType : ""));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "resendValidation", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        SmsServiceRemote smsService = null;

        try {
            smsService = EJBHomeCache.getInstance().getSmsService();
            String sendingType = this.parametersService.getParamValue(UserService.PARAM_VERIFICATION_CODE_MOBILE_SENDING_TYPE);
            String emailSenderAddress = this.parametersService.getParamValue(UserService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(UserService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(UserService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(UserService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(UserService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(UserService.PARAM_PROXY_NO_HOSTS);
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_SMS_MAX_RETRY_ATTEMPS));

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            response = userResendValidationAction.execute(ticketId, requestId, id, validitationType, sendingType, maxRetryAttemps, this.emailSender, smsService,
                    userCategoryService, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (InterfaceNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "resendValidation", requestId, "closing", "Error creating sms service: " + ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "resendValidation", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public CheckAvailabilityAmountData userCheckAvailabilityAmount(String ticketId, String requestId, Double amount, String loyaltySessionID) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("amount", (amount != null) ? amount.toString() : ""));
        inputParameters.add(new Pair<String, String>("loyaltySessionID", (loyaltySessionID != null) ? loyaltySessionID : ""));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "userCheckAvailabilityAmount", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        CheckAvailabilityAmountData checkAvailabilityAmountData = new CheckAvailabilityAmountData();

        try {
            Double voucherTransactionMinAmount = Double.valueOf(this.parametersService.getParamValue(UserService.PARAM_VOUCHER_TRANSACTION_MIN_AMOUNT));
            Double ratioThreshold = Double.valueOf(this.parametersService.getParamValue(UserService.PARAM_RATIO_THRESHOLD));
            checkAvailabilityAmountData = userCheckAvailabilityAmountAction.execute(ticketId, requestId, amount, ratioThreshold, voucherTransactionMinAmount, this,
                    loyaltySessionID);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            checkAvailabilityAmountData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            checkAvailabilityAmountData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", checkAvailabilityAmountData.getStatusCode()));
        if (checkAvailabilityAmountData.getMaxAmount() != null) {
            outputParameters.add(new Pair<String, String>("maxAmount", checkAvailabilityAmountData.getMaxAmount().toString()));
        }
        else {
            outputParameters.add(new Pair<String, String>("maxAmount", "null"));
        }
        if (checkAvailabilityAmountData.getThresholdAmount() != null) {
            outputParameters.add(new Pair<String, String>("thresholdAmount", checkAvailabilityAmountData.getThresholdAmount().toString()));
        }
        else {
            outputParameters.add(new Pair<String, String>("thresholdAmount", "null"));
        }
        if (checkAvailabilityAmountData.getAllowResize() != null) {
            outputParameters.add(new Pair<String, String>("allowResize", checkAvailabilityAmountData.getAllowResize().toString()));
        }
        else {
            outputParameters.add(new Pair<String, String>("allowResize", "null"));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "userCheckAvailabilityAmount", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return checkAvailabilityAmountData;
    }

    @Override
    public String updateTermsOfService(String ticketId, String requestId, List<TermsOfService> termsOfServiceList) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        String termsOfServiceListString = "";
        int count = 0;
        for (TermsOfService termsOfService : termsOfServiceList) {
            if (count != 0) {
                termsOfServiceListString = termsOfServiceListString + ",";
            }
            termsOfServiceListString = termsOfServiceListString + "{ \"id\": " + termsOfService.getId() + ", \"\"" + termsOfService.getAccepted() + " }";
            count++;
        }
        inputParameters.add(new Pair<String, String>("termsOfServiceList", termsOfServiceListString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateTermsOfService", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;

        Integer reconciliationMaxAttempts = 5;

        try {
            reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "updateTermsOfService", requestId, null,
                    "Errore nella lettura dei parametri: " + ex.getMessage());
        }

        try {
            response = userUpdateTermsOfServiceAction.execute(ticketId, requestId, termsOfServiceList, reconciliationMaxAttempts, userCategoryService);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateTermsOfService", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String updateSmsLog(String correlationID, String destinationAddress, String message, String mtMessageID, Integer statusCode, Integer responseCode, Integer reasonCode,
            String responseMessage, Date operatorTimestamp, Date providerTimestamp, String operator, boolean firstUpdate) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("correlationID", correlationID));
        inputParameters.add(new Pair<String, String>("destinationAddress", destinationAddress));
        inputParameters.add(new Pair<String, String>("message", message));
        inputParameters.add(new Pair<String, String>("mtMessageID", mtMessageID));
        inputParameters.add(new Pair<String, String>("statusCode", (statusCode != null ? statusCode.toString() : "")));
        inputParameters.add(new Pair<String, String>("responseCode", (responseCode != null ? responseCode.toString() : "")));
        inputParameters.add(new Pair<String, String>("reasonCode", (reasonCode != null ? reasonCode.toString() : "")));
        inputParameters.add(new Pair<String, String>("responseMessage", (responseMessage != null ? responseMessage : "")));
        inputParameters.add(new Pair<String, String>("operatorTimestamp", (operatorTimestamp != null ? operatorTimestamp.toString() : "")));
        inputParameters.add(new Pair<String, String>("providerTimestamp", (providerTimestamp != null ? providerTimestamp.toString() : "")));
        inputParameters.add(new Pair<String, String>("operator", operator));
        inputParameters.add(new Pair<String, String>("firstUpdate", String.valueOf(firstUpdate)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateSmsLog", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;

        try {
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_SMS_MAX_RETRY_ATTEMPS));
            response = userUpdateSmsLogAction.execute(correlationID, destinationAddress, maxRetryAttemps, message, mtMessageID, statusCode, responseCode, reasonCode,
                    responseMessage, operatorTimestamp, providerTimestamp, operator, firstUpdate);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updateSmsLog", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String checkLoyaltySession(String sessionId, String fiscalCode) {
        /*
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("sessionId", sessionId));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkLoyaltySession", null, "opening", ActivityLog.createLogMessage(inputParameters));
        */

        String response;

        try {
            Integer loyaltySessionExpiryTime = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_LOYALTY_SESSION_EXPIRY_TIME));

            response = userCheckLoyaltySessionAction.execute(sessionId, fiscalCode, loyaltySessionExpiryTime);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }

        /*
        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkLoyaltySession", null, "closing", ActivityLog.createLogMessage(outputParameters));
        */

        return response;
    }

    @Override
    public InfoRedemptionResponse infoRedemption(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "infoRedemption", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        InfoRedemptionResponse infoRedemptionResponse = new InfoRedemptionResponse();

        try {
            infoRedemptionResponse = userInfoRedemptionAction.execute(ticketId, requestId, fidelityService);
        }

        catch (EJBException ex) {
            infoRedemptionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", infoRedemptionResponse.getStatusCode()));

        String redemptionsString = "";

        for (RedemptionDetail redemptionDetail : infoRedemptionResponse.getRedemptions()) {
            redemptionsString += " { name: " + redemptionDetail.getName() + ", amount: " + redemptionDetail.getAmount() + ", code: " + redemptionDetail.getCode() + ", points: "
                    + redemptionDetail.getPoints() + "} ";
        }

        outputParameters.add(new Pair<String, String>("redemptions", redemptionsString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "infoRedemption", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return infoRedemptionResponse;
    }

    @Override
    public RedemptionResponse redemption(String ticketId, String requestId, Integer redemptionCode, String pin) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("redemptionCode", redemptionCode.toString()));
        inputParameters.add(new Pair<String, String>("pin", pin));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "redemption", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RedemptionResponse response = null;
        Integer reconciliationAttemptsLeft = 5;

        try {
            reconciliationAttemptsLeft = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
        }

        try {
            response = userRedemptionAction.execute(ticketId, requestId, redemptionCode, fidelityService, pin, reconciliationAttemptsLeft);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response.getStatusCode()));

        String voucherString = "";

        if (response.getVoucher() != null) {
            VoucherDetail voucherDetail = response.getVoucher();
            voucherString = "{ " + " code: " + voucherDetail.getVoucherCode() + " status: " + voucherDetail.getVoucherStatus() + " type: " + voucherDetail.getVoucherType()
                    + " balanceDue: " + voucherDetail.getVoucherBalanceDue() + " value: " + voucherDetail.getVoucherValue() + " consumedValue: " + voucherDetail.getConsumedValue()
                    + " initialValue: " + voucherDetail.getInitialValue() + " combinable: " + voucherDetail.getIsCombinable() + " promoCode: " + voucherDetail.getPromoCode()
                    + " promoDescription: " + voucherDetail.getPromoDescription() + " promoDoc: " + voucherDetail.getPromoDoc() + " promoPartner: "
                    + voucherDetail.getPromoPartner() + " validPV: " + voucherDetail.getValidPV() + " minAmount: " + voucherDetail.getMinAmount() + " minQuantity: "
                    + voucherDetail.getMinQuantity() + " icon: " + voucherDetail.getIcon() + "}";
        }
        outputParameters.add(new Pair<String, String>("statusCode", response.getStatusCode()));
        outputParameters.add(new Pair<String, String>("voucher", voucherString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "redemption", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public RetrieveUserDeviceData retrieveActiveDevice(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveActiveDevice", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveUserDeviceData retrieveUserDeviceData = null;
        try {

            retrieveUserDeviceData = userRetrieveActiveDevice.execute(ticketId, requestId);
        }
        catch (EJBException ex) {
            retrieveUserDeviceData = new RetrieveUserDeviceData();
            retrieveUserDeviceData.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrieveUserDeviceData.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveActiveDevice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveUserDeviceData;
    }

    @Override
    public String removeActiveUserDevice(String ticketId, String requestId, String userDeviceID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("userDeviceID", userDeviceID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "removeActiveUserDevice", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {

            response = userRemoveActiveUserDevice.execute(ticketId, requestId, userDeviceID);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "removeActiveUserDevice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
    
    @Override
    public MulticardPaymentResponse insertMulticardPaymentMethod(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("paymentMethodType", PaymentInfo.PAYMENTINFO_TYPE_MULTICARD));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "insertMulticardPaymentMethod", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        MulticardPaymentResponse paymentResponse = null;
        try {
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(UserService.PARAM_PIN_CHECK_MAX_ATTEMPTS));

            paymentResponse = userInsertMulticardPaymentMethodAction.execute(ticketId, requestId, PaymentInfo.PAYMENTINFO_TYPE_MULTICARD, pinCheckMaxAttempts, this.userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            paymentResponse = new MulticardPaymentResponse();
            paymentResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            paymentResponse = new MulticardPaymentResponse();
            paymentResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", paymentResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "insertMulticardPaymentMethod", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return paymentResponse;
    }

}
