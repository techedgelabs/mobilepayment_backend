package com.techedge.mp.core.business.exceptions;

public class PromotionException extends BaseException {

    /**
     * 
     */
    private static final long serialVersionUID = -4002676890424517744L;

    
    public PromotionException(String message) {
        super(message);
    }

}
