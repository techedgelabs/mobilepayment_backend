package com.techedge.mp.core.business.exceptions;

public class CampaignGenericException extends BaseException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 187743597629605643L;

	public CampaignGenericException(String message) {
        super(message);
    }

}
