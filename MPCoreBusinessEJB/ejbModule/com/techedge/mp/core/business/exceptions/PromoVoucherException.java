package com.techedge.mp.core.business.exceptions;

public class PromoVoucherException extends BaseException {

    
    /**
     * 
     */
    private static final long serialVersionUID = -3869479732727907098L;

    public PromoVoucherException(String message) {
        super(message);
    }

}
