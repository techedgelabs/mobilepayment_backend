package com.techedge.mp.core.business.exceptions;

public class SocialAuthenticationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String errorCode;
	String errorMessage;
	int errCode; 


	public int getErrCode() {
		return errCode;
	}
	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public SocialAuthenticationException(int errorCode, String errorMessage) {
		super();
		this.errCode = errorCode;
		this.errorMessage = errorMessage;
	}
	public SocialAuthenticationException(String errorCode, String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	public SocialAuthenticationException() {
		super();
	}
	
	

}
