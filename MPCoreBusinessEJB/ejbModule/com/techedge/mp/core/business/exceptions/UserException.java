package com.techedge.mp.core.business.exceptions;

public class UserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9097523797146372926L;
	
	private String message;
	
	public UserException(String message) {
		
		this.message = message;
	}
	
	public String toString()
	{
		return this.getClass().getName() + "message: " + message;
	}
}
