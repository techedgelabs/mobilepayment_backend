package com.techedge.mp.core.business.exceptions;

public class ManagerException extends Exception {

	
	/**
   * 
   */
  private static final long serialVersionUID = -1397016660275979336L;
  private String message;
	
	public ManagerException(String message) {
		
		this.message = message;
	}
	
	public String toString()
	{
		return this.getClass().getName() + "message: " + message;
	}
}
