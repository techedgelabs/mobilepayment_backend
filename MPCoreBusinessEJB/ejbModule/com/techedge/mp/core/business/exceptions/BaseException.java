package com.techedge.mp.core.business.exceptions;

public abstract class BaseException extends Exception
{

    /**
     * 
     */
    private static final long serialVersionUID = -7049820993170436232L;

    protected String message;
    
    public BaseException(String message) {
        
        this.message = message;
    }
    
    
    public String getMessage() {
        return message;
    }


    public String toString()
    {
        return this.getClass().getName() + "message: " + message;
    }
    
    
}
