package com.techedge.mp.core.business.exceptions;

public class AdminException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7648576633669393953L;
	
	private String message;
	
	public AdminException(String message) {
		
		this.message = message;
	}
	
	public String toString()
	{
		return this.getClass().getName() + "message: " + message;
	}
}
