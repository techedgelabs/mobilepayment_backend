package com.techedge.mp.core.business.exceptions;

public class PaymentInfoNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6932864830860036556L;
	
	private String message;
	
	public PaymentInfoNotFoundException(String message) {
		
		this.message = message;
	}
	
	public String toString()
	{
		return this.getClass().getName() + "message: " + message;
	}
}
