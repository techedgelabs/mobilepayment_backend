package com.techedge.mp.core.business.exceptions;

public class PinException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2533294433141126423L;
	
	private String statusCode;
	private String statusMessage;
	private Integer pinCheckAttemptsLeft;
	
	
	public PinException(String statusCode, String statusMessage) {
		
		this.statusCode    = statusCode;
		this.statusMessage = statusMessage;
	}
	
	public PinException(String statusCode, String statusMessage, Integer pinCheckAttemptsLeft ) {
		
		this.statusCode    = statusCode;
		this.statusMessage = statusMessage;
		this.pinCheckAttemptsLeft = pinCheckAttemptsLeft;
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	

	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	

	public Integer getPinCheckAttemptsLeft() {
		return pinCheckAttemptsLeft;
	}


	public void setPinCheckAttemptsLeft(Integer pinCheckAttemptsLeft) {
		this.pinCheckAttemptsLeft = pinCheckAttemptsLeft;
	}


	public String toString()
	{
		return this.getClass().getSimpleName() + " statusCode: " + this.statusCode + " statusMessage: " + this.statusMessage;
	}
}
