package com.techedge.mp.core.business;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.UnavailabilityPeriod;
import com.techedge.mp.core.business.model.UnavailabilityPeriodBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

/**
 * Session Bean implementation class UnavailabilityPeriodService
 */
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UnavailabilityPeriodService implements UnavailabilityPeriodServiceLocal {

    private final static String                     UNIT_NAME = "CrudPU";
    private Map<String, List<UnavailabilityPeriod>> map       = new HashMap<String, List<UnavailabilityPeriod>>(0);

    @Resource
    private EJBContext                              context;

    @PersistenceContext(unitName = UNIT_NAME)
    private EntityManager                           em;

    public UnavailabilityPeriodService() {}

    @PostConstruct
    protected void initSingleton() {
        System.out.println("Init UnavailabilityPeriodService");
        init();
    }

    @Override
    public void refreshData() {
        System.out.println("REFRESH DATA");
        init();
    }

    private void init() {
        UserTransaction userTransaction = context.getUserTransaction();
        try {
            userTransaction.begin();
            List<UnavailabilityPeriodBean> unavailabilityPeriodBeanList = QueryRepository.findAllActiveUnavailabilityPeriods(em);
            for (UnavailabilityPeriodBean unavailabilityPeriodBean : unavailabilityPeriodBeanList) {

                UnavailabilityPeriod unavailabilityPeriod = unavailabilityPeriodBean.toUnavailabilityPeriod();

                String operation = unavailabilityPeriod.getOperation();
                
                System.out.println("UnavailabilityPeriodService.init: trovato elemento " + unavailabilityPeriod.getCode() + " per operation " + operation);
                
                List<UnavailabilityPeriod> unavailabilityPeriodList = map.get(operation);
                if (unavailabilityPeriodList != null) {
                    System.out.println("Operation " + operation + " found in map");
                    unavailabilityPeriodList.add(unavailabilityPeriod);
                }
                else {
                    System.out.println("Operation " + operation + " not found in map -> creating");
                    unavailabilityPeriodList = new ArrayList<UnavailabilityPeriod>(0);
                    unavailabilityPeriodList.add(unavailabilityPeriod);
                    map.put(operation, unavailabilityPeriodList);
                }
            }
            userTransaction.commit();
        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public ServiceAvailabilityData retrieveServiceAvailability(String operation, Date time) {
        
        DateFormat format     = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ITALIAN);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        List<UnavailabilityPeriod> unavailabilityPeriodList = map.get(operation);
        if ( unavailabilityPeriodList != null && !unavailabilityPeriodList.isEmpty() ) {
        
            System.out.println("UnavailabilityPeriodList found for operation " + operation);
            
            for(UnavailabilityPeriod unavailabilityPeriod : unavailabilityPeriodList) {
            
                String startString = "";
                String endString   = "";
                
                if ( unavailabilityPeriod.getStartDate() != null && !unavailabilityPeriod.getStartDate().trim().equals("") 
                  && unavailabilityPeriod.getEndDate() != null && !unavailabilityPeriod.getEndDate().trim().equals("") ) {
                    
                    // Effettua il controllo utilizzando anche le date
                    startString = unavailabilityPeriod.getStartDate() + " " + unavailabilityPeriod.getStartTime();
                    endString   = unavailabilityPeriod.getEndDate()   + " " + unavailabilityPeriod.getEndTime();
                }
                else {
                    
                    // Effettua il controllo senza date (si considera il periodo come valido per tutti i giorni)
                    String dateString  = dateFormat.format(time);
                    startString = dateString + " " + unavailabilityPeriod.getStartTime();
                    endString   = dateString + " " + unavailabilityPeriod.getEndTime();
                }
                
                Date startDate;
                Date endDate;
                
                try {
                    startDate = (Date) format.parse(startString);
                    endDate   = (Date) format.parse(endString);
                }
                catch (ParseException e) {
                    
                    System.out.println("Error parsing date: " + e.getLocalizedMessage());
                    e.printStackTrace();
                    return null;
                }
                
                Long dateLong = time.getTime();
                Long startLong = startDate.getTime();
                Long endLong   = endDate.getTime();
                
                System.out.println("StartLong: " + startLong);
                System.out.println("DateLong:  " + dateLong);
                System.out.println("EndLong:   " + endLong);
                
                if ( dateLong >= startLong && dateLong <= endLong ) {
                    
                    ServiceAvailabilityData serviceAvailabilityData = new ServiceAvailabilityData();
                    serviceAvailabilityData.setStatusCode(unavailabilityPeriod.getStatusCode());
                    serviceAvailabilityData.setStatusMessage(unavailabilityPeriod.getStatusMessage());
                    return serviceAvailabilityData;
                }
            }
        }
        
        return null;
    }
}
