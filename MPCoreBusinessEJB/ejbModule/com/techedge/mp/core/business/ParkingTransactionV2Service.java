package com.techedge.mp.core.business;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.actions.parking.v2.ParkingV2ApproveExtendParkingAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2ApproveParkingTransactionAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2CheckAuthTokenStateAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2EndParkingAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2EstimateEndParkingPriceAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2EstimateExtendedParkingPriceAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2EstimateParkingPriceAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2GetCitiesAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2GetParkingTransactionDetailAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2PersistAuthTokenAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2RetrieveParkingZonesAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2RetrieveParkingZonesByCityAction;
import com.techedge.mp.core.actions.parking.v2.ParkingV2RetrievePendingParkingTransactionAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.ApproveExtendParkingTransactionResult;
import com.techedge.mp.core.business.interfaces.parking.ApproveParkingTransactionResult;
import com.techedge.mp.core.business.interfaces.parking.Authentication;
import com.techedge.mp.core.business.interfaces.parking.EndParkingResult;
import com.techedge.mp.core.business.interfaces.parking.EstimateEndParkingPriceResult;
import com.techedge.mp.core.business.interfaces.parking.EstimateExtendedParkingPriceResult;
import com.techedge.mp.core.business.interfaces.parking.EstimateParkingPriceResult;
import com.techedge.mp.core.business.interfaces.parking.GetCitiesResult;
import com.techedge.mp.core.business.interfaces.parking.GetParkingTransactionDetailResult;
import com.techedge.mp.core.business.interfaces.parking.RetrieveParkingZonesByCityResult;
import com.techedge.mp.core.business.interfaces.parking.RetrieveParkingZonesResult;
import com.techedge.mp.core.business.interfaces.parking.RetrievePendingParkingTransactionResult;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.StringUtils;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

/**
 * Session Bean implementation class ParkingTransactionV2Service
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ParkingTransactionV2Service implements ParkingTransactionV2ServiceRemote, ParkingTransactionV2ServiceLocal {

    @EJB
    private ParkingV2RetrieveParkingZonesAction         parkingV2RetrieveParkingZonesAction;

    @EJB
    private ParkingV2RetrieveParkingZonesByCityAction   parkingV2RetrieveParkingZonesByCityAction;

    @EJB
    private ParkingV2GetCitiesAction                    parkingV2GetCitiesAction;

    @EJB
    private ParkingV2EstimateParkingPriceAction         parkingV2EstimateParkingPriceAction;

    @EJB
    private ParkingV2ApproveParkingTransactionAction    parkingV2ApproveParkingTransactionAction;

    @EJB
    private ParkingV2EndParkingAction                   parkingV2EndParkingAction;

    @EJB
    private ParkingV2EstimateEndParkingPriceAction      parkingV2EstimateEndParkingPriceAction;

    @EJB
    private ParkingV2EstimateExtendedParkingPriceAction parkingV2EstimateExtendedParkingPriceAction;

    @EJB
    private ParkingV2ApproveExtendParkingAction         parkingV2ExtendParkingAction;
    
    @EJB
    private ParkingV2RetrievePendingParkingTransactionAction parkingV2RetrievePendingParkingTransactionAction;
    
    @EJB
    private ParkingV2GetParkingTransactionDetailAction  parkingV2GetParkingTransactionDetailAction;

	@EJB
    private ParkingV2PersistAuthTokenAction                  parkingV2PersistAuthTokenAction;

    @EJB
    private ParkingV2CheckAuthTokenStateAction               parkingV2CheckAuthTokenStateAction;

    private ParametersService                           parametersService                     = null;
    private LoggerService                               loggerService                         = null;
    private ParkingServiceRemote                        parkingService                        = null;
    private GPServiceRemote                             gpService                             = null;

    private final static String                         PARAM_PARKING_LANG                    = "PARKING_LANG";
    private final static String                         PARAM_PARKING_ACCURACY                = "PARKING_ACCURACY";
    private static final String                         PARAM_PIN_CHECK_MAX_ATTEMPTS          = "PIN_CHECK_MAX_ATTEMPTS";
    private final static String                         PARAM_CARTASI_VAULT_BLOCK             = "CARTASI_VAULT_BLOCK";
    private final static String                         PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY   = "CARTASI_VAULT_BLOCK_ATTRIBUTE_KEY";
    private static final String                         PARAM_ACQUIRER_ID_CARTASI             = "ACQUIRER_ID_CARTASI";
    private static final String                         PARAM_PARKING_ENCODED_SECRET_KEY      = "PARKING_ENCODED_SECRET_KEY";
    private static final String                         PARAM_PARKING_DEFAULT_AMOUNT          = "PARKING_DEFAULT_AMOUNT";
    private static final String                         PARAM_UICCODE_REFUND_CARTASI          = "UICCODE_REFUND_CARTASI";
    private static final String                         PARAM_SERVER_NAME                     = "SERVER_NAME";
    private static final String                         PARAM_GROUP_ACQUIRER_CARTASI          = "GROUP_ACQUIRER_CARTASI";
    private static final String                         PARAM_PARKING_API_KEY                 = "PARKING_API_KEY";
    private static final String                         PARAM_PARKING_MAX_AMOUNT              = "PARKING_MAX_PARKING_AMOUNT";
    private final static String                         PARAM_PARKING_PREAUTHORIZATION_AMOUNT = "PARKING_PREAUTHORIZATION_AMOUNT";
    private final static String                         PARAM_STRING_SUBSTITUTION_PATTERN     = "STRING_SUBSTITUTION_PATTERN";
    private static final String                         PARAM_EMAIL_SENDER_ADDRESS            = "EMAIL_SENDER_ADDRESS";
    private static final String                         PARAM_SMTP_HOST                       = "SMTP_HOST";
    private static final String                         PARAM_SMTP_PORT                       = "SMTP_PORT";
    private static final String                         PARAM_RECEIPT_EMAIL_ACTIVE            = "RECEIPT_EMAIL_ACTIVE";
    private final static String                         PARAM_PROXY_HOST                      = "PROXY_HOST";
    private final static String                         PARAM_PROXY_PORT                      = "PROXY_PORT";
    private final static String                         PARAM_PROXY_NO_HOSTS                  = "PROXY_NO_HOSTS";
    private final static String                         PARAM_RECONCILIATION_MAX_ATTEMPTS     = "RECONCILIATION_MAX_ATTEMPTS";

    private EmailSenderRemote                           emailSender                           = null;
    
    private String                                      LANG;
    private BigDecimal                                  ACCURACY;
    private String                                      secretKey                             = null;
    private String                                      miCiceryDecodedSecretKey              = null;
    
    private StringSubstitution                          stringSubstitution                    = null;

    /**
     * Default constructor.
     */
    public ParkingTransactionV2Service() {
        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get LoggerService object");
            throw new EJBException(e);
        }
        
        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get EmailSender object");

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ParametersService object");

            throw new EJBException(e);
        }
        try {
            parkingService = EJBHomeCache.getInstance().getParkingService();
        }
        catch (InterfaceNotFoundException e1) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ParkingService object");

            throw new EJBException(e1);
        }

        try {
            gpService = EJBHomeCache.getInstance().getGpService();
        }
        catch (InterfaceNotFoundException e2) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get gpService object");

            throw new EJBException(e2);
        }

        try {
            LANG = this.parametersService.getParamValue(PARAM_PARKING_LANG);
        }
        catch (ParameterNotFoundException e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "PARKING_LANG value not found or not set, check database values");
        }

        try {
            ACCURACY = new BigDecimal(this.parametersService.getParamValue(PARAM_PARKING_ACCURACY));
        }
        catch (ParameterNotFoundException e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "PARKING_ACCURACY not found or not set, check database values");
        }

        String vaultBlock                  = "";
        String vaultBlockAttributeCryptKey = "";
        String pattern                     = "";

        try {
            vaultBlock = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK);
            vaultBlockAttributeCryptKey = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY);
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeCryptKey + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.secretKey + "'");

        try {
            String myCiceroEncodedSecretKey = this.parametersService.getParamValue(PARAM_PARKING_ENCODED_SECRET_KEY);

            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(this.secretKey);

            miCiceryDecodedSecretKey = encryptionAES.decrypt(myCiceroEncodedSecretKey);
        }
        catch (ParameterNotFoundException e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null,
                    "MY_CICERO_ENCODED_SECRET_KEY value not found or not set, check database values");
        }
        catch (Exception e) {
            e.printStackTrace();
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Error decodin myCiceroEncodedSecretKey");
        }

        stringSubstitution = new StringSubstitution(pattern);
    }

    public RetrieveParkingZonesResult retrieveParkingZones(String ticketId, String requestId, double latitude, double longitude) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("latitude", String.valueOf(latitude)));
        inputParameters.add(new Pair<String, String>("longitude", String.valueOf(longitude)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveParkingZones", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveParkingZonesResult retrieveParkingZonesResult = null;

        try {
            retrieveParkingZonesResult = parkingV2RetrieveParkingZonesAction.execute(parkingService, ticketId, requestId, latitude, longitude, LANG, ACCURACY);
        }
        catch (Exception e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, e.getMessage());
            retrieveParkingZonesResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", retrieveParkingZonesResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveParkingZones", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveParkingZonesResult;
    }

    @Override
    public EstimateParkingPriceResult estimateParkingPrice(String ticketId, String requestId, String plateNumber, Date requestedEndTime, String parkingZoneId, String stallCode, String cityId) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        DecimalFormat decimalFormat = new DecimalFormat("#0.##");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("plateNumber", plateNumber));
        inputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(requestedEndTime)));
        inputParameters.add(new Pair<String, String>("parkingZoneId", parkingZoneId));
        inputParameters.add(new Pair<String, String>("stallCode", stallCode));
        inputParameters.add(new Pair<String, String>("cityId", cityId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateParkingPrice", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        EstimateParkingPriceResult estimateParkingPriceResult = null;

        try {
            String acquirerID = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_ACQUIRER_ID_CARTASI);
            String currency = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_UICCODE_REFUND_CARTASI);
            String groupAcquirer = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_GROUP_ACQUIRER_CARTASI);
            String serverName = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_SERVER_NAME);
            String shopLogin = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PARKING_API_KEY);
            Double parkingMaxAmount = Double.valueOf(this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PARKING_MAX_AMOUNT));
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_RECONCILIATION_MAX_ATTEMPTS));

            estimateParkingPriceResult = parkingV2EstimateParkingPriceAction.execute(parkingService, ticketId, requestId, plateNumber, requestedEndTime, parkingZoneId, stallCode,
                    cityId, LANG, acquirerID, currency, groupAcquirer, serverName, shopLogin, parkingMaxAmount, reconciliationMaxAttempts);
        }
        catch (Exception e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, e.getMessage());
            estimateParkingPriceResult = new EstimateParkingPriceResult();
            estimateParkingPriceResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return estimateParkingPriceResult;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", estimateParkingPriceResult.getStatusCode()));

        if (estimateParkingPriceResult!= null && estimateParkingPriceResult.getStatusCode().equals(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_SUCCESS)) {
            
            outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(estimateParkingPriceResult.getNotice(), "|")));
            outputParameters.add(new Pair<String, String>("parkingEndTime", dateFormat.format(estimateParkingPriceResult.getParkingEndTime())));
            outputParameters.add(new Pair<String, String>("parkingStartTime", dateFormat.format(estimateParkingPriceResult.getParkingStartTime())));
            outputParameters.add(new Pair<String, String>("parkingTimeCorrection", estimateParkingPriceResult.getParkingTimeCorrection()));
            outputParameters.add(new Pair<String, String>("parkingZoneId", estimateParkingPriceResult.getParkingZoneId()));
            outputParameters.add(new Pair<String, String>("plateNumber", estimateParkingPriceResult.getPlateNumber()));
            outputParameters.add(new Pair<String, String>("price", decimalFormat.format(estimateParkingPriceResult.getPrice())));
            outputParameters.add(new Pair<String, String>("requestedEndTime", dateFormat.format(estimateParkingPriceResult.getRequestedEndTime())));
            outputParameters.add(new Pair<String, String>("stallCode", estimateParkingPriceResult.getStallCode()));
            outputParameters.add(new Pair<String, String>("transactionId", estimateParkingPriceResult.getTransactionId()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateParkingPrice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return estimateParkingPriceResult;
    }

    @Override
    public ApproveParkingTransactionResult approveParkingTransaction(String ticketId, String requestId, String transactionId, Long paymentMethodId, String paymentMethodType,
            String encodedPin) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        DecimalFormat decimalFormat = new DecimalFormat("#0.##");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        //        inputParameters.add(new Pair<String, String>("plateNumber", plateNumber));
        //        inputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(requestedEndTime)));
        //        inputParameters.add(new Pair<String, String>("parkingZoneId", parkingZoneId));
        //        inputParameters.add(new Pair<String, String>("stallCode", stallCode));
        inputParameters.add(new Pair<String, String>("transactionId", transactionId));
        inputParameters.add(new Pair<String, String>("paymentMethodId", String.valueOf(paymentMethodId)));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("encodedPin", encodedPin));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approveParkingTransaction", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        ApproveParkingTransactionResult approveParkingTransactionResult = null;

        try {
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            Double defaultAmount = Double.valueOf(this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PARKING_DEFAULT_AMOUNT));
            Double parkingMaxAmount = Double.valueOf(this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PARKING_MAX_AMOUNT));

            approveParkingTransactionResult = parkingV2ApproveParkingTransactionAction.execute(parkingService, gpService, ticketId, requestId, encodedPin, pinCheckMaxAttempts,
                    transactionId, paymentMethodId, paymentMethodType, defaultAmount, LANG, miCiceryDecodedSecretKey, parkingMaxAmount);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            approveParkingTransactionResult = new ApproveParkingTransactionResult();
            approveParkingTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return approveParkingTransactionResult;
        }
        catch (Exception e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, e.getMessage());
            approveParkingTransactionResult = new ApproveParkingTransactionResult();
            approveParkingTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return approveParkingTransactionResult;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", approveParkingTransactionResult.getStatusCode()));
        
        if (approveParkingTransactionResult != null && approveParkingTransactionResult.getStatusCode().equals(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_SUCCESS)) {
            
            outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(approveParkingTransactionResult.getNotice(), "|")));
            outputParameters.add(new Pair<String, String>("parkingEndTime", dateFormat.format(approveParkingTransactionResult.getParkingEndTime())));
            outputParameters.add(new Pair<String, String>("parkingStartTime", dateFormat.format(approveParkingTransactionResult.getParkingStartTime())));
            outputParameters.add(new Pair<String, String>("parkingTimeCorrection", approveParkingTransactionResult.getParkingTimeCorrection().toString()));
            outputParameters.add(new Pair<String, String>("parkingZoneId", approveParkingTransactionResult.getParkingZoneId()));
            outputParameters.add(new Pair<String, String>("plateNumber", approveParkingTransactionResult.getPlateNumber()));
            outputParameters.add(new Pair<String, String>("price", decimalFormat.format(approveParkingTransactionResult.getPrice())));
            outputParameters.add(new Pair<String, String>("requestedEndTime", dateFormat.format(approveParkingTransactionResult.getRequestedEndTime())));
            outputParameters.add(new Pair<String, String>("stallCode", approveParkingTransactionResult.getStallCode()));
            outputParameters.add(new Pair<String, String>("transactionId", approveParkingTransactionResult.getTransactionId()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approveParkingTransaction", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return approveParkingTransactionResult;
    }

    @Override
    public EndParkingResult endParking(String ticketId, String requestId, String transactionId) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        DecimalFormat decimalFormat = new DecimalFormat("#0.##");
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionId", transactionId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "endParking", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        EndParkingResult endParkingResult = null;

        try {
            String emailSenderAddress       = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost                 = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_SMTP_HOST);
            String smtpPort                 = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_SMTP_PORT);
            String receiptEmailActiveString = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_RECEIPT_EMAIL_ACTIVE);
            String proxyHost                = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PROXY_HOST);
            String proxyPort                = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PROXY_PORT);
            String proxyNoHosts             = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PROXY_NO_HOSTS);
            
            Boolean receiptEmailActive = false;
            if (receiptEmailActiveString.equals("true")) {
                receiptEmailActive = true;
            }
            
            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);
            
            endParkingResult = parkingV2EndParkingAction.execute(this.parkingService, this.gpService, receiptEmailActive, this.emailSender, ticketId, requestId,
                    transactionId, LANG, miCiceryDecodedSecretKey, proxyHost, proxyPort, proxyNoHosts, stringSubstitution);
            if (endParkingResult == null) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "startParkingResult not found");
                endParkingResult = new EndParkingResult();
                endParkingResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
                return endParkingResult;
            }
        }
        catch (Exception e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, e.getMessage());
            endParkingResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return endParkingResult;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", endParkingResult.getStatusCode()));
        
        if (endParkingResult.getStatusCode().equals(ResponseHelper.PARKING_END_PARKING_SUCCESS)) {
            
            outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(endParkingResult.getNotice(), "|")));
            outputParameters.add(new Pair<String, String>("parkingEndTime", dateFormat.format(endParkingResult.getParkingEndTime())));
            outputParameters.add(new Pair<String, String>("parkingStartTime", dateFormat.format(endParkingResult.getParkingStartTime())));
            // outputParameters.add(new Pair<String, String>("parkingTimeCorrection", endParkingResult.getParkingTimeCorrection().toString()));
            outputParameters.add(new Pair<String, String>("parkingZoneId", endParkingResult.getParkingZoneId()));
            outputParameters.add(new Pair<String, String>("plateNumber", endParkingResult.getPlateNumber()));
            outputParameters.add(new Pair<String, String>("price", decimalFormat.format(endParkingResult.getPrice())));
            //outputParameters.add(new Pair<String, String>("requestedEndTime", dateFormat.format(endParkingResult.getRequestedEndTime())));
            outputParameters.add(new Pair<String, String>("stallCode", endParkingResult.getStallCode()));
            outputParameters.add(new Pair<String, String>("transactionId", endParkingResult.getTransactionId()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "endParking", requestId, "closing", ActivityLog.createLogMessage(outputParameters));
        
        return endParkingResult;
    }

    @Override
    public EstimateEndParkingPriceResult estimateEndParkingPrice(String ticketId, String requestId, String transactionId) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        DecimalFormat decimalFormat = new DecimalFormat("#0.##");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionId", transactionId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateEndParkingPrice", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        EstimateEndParkingPriceResult estimateEndParkingPriceResult = null;

        try {
            estimateEndParkingPriceResult = parkingV2EstimateEndParkingPriceAction.execute(parkingService, ticketId, requestId, transactionId, LANG);
        }
        catch (Exception e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, e.getMessage());
            estimateEndParkingPriceResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return estimateEndParkingPriceResult;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", estimateEndParkingPriceResult.getStatusCode()));
        
        if (estimateEndParkingPriceResult.getStatusCode().equals(ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_SUCCESS)) {
            outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(estimateEndParkingPriceResult.getNotice(), "|")));
            outputParameters.add(new Pair<String, String>("parkingEndTime", dateFormat.format(estimateEndParkingPriceResult.getParkingEndTime())));
            outputParameters.add(new Pair<String, String>("parkingStartTime", dateFormat.format(estimateEndParkingPriceResult.getParkingStartTime())));
            if (estimateEndParkingPriceResult.getRequestedEndTime() != null) {
                outputParameters.add(new Pair<String, String>("requestedEndTime", dateFormat.format(estimateEndParkingPriceResult.getRequestedEndTime())));
            }
            else {
                outputParameters.add(new Pair<String, String>("requestedEndTime", "null"));
            }
            if (estimateEndParkingPriceResult.getParkingTimeCorrection() != null) {
                outputParameters.add(new Pair<String, String>("parkingTimeCorrection", estimateEndParkingPriceResult.getParkingTimeCorrection().toString()));
            }
            else {
                outputParameters.add(new Pair<String, String>("parkingTimeCorrection", "null"));
            }
            outputParameters.add(new Pair<String, String>("parkingId", estimateEndParkingPriceResult.getParkingId()));
            outputParameters.add(new Pair<String, String>("parkingZoneId", estimateEndParkingPriceResult.getParkingZoneId()));
            outputParameters.add(new Pair<String, String>("plateNumber", estimateEndParkingPriceResult.getPlateNumber()));
            outputParameters.add(new Pair<String, String>("price", decimalFormat.format(estimateEndParkingPriceResult.getPrice())));
            if (estimateEndParkingPriceResult.getCurrentPrice() != null) {
                outputParameters.add(new Pair<String, String>("currentPrice", decimalFormat.format(estimateEndParkingPriceResult.getCurrentPrice())));
            }
            else {
                outputParameters.add(new Pair<String, String>("currentPrice", "null"));
            }
            if (estimateEndParkingPriceResult.getCurrentPrice() != null) {
                outputParameters.add(new Pair<String, String>("priceDifference", decimalFormat.format(estimateEndParkingPriceResult.getPriceDifference())));
            }
            else {
                outputParameters.add(new Pair<String, String>("priceDifference", "null"));
            }
            outputParameters.add(new Pair<String, String>("stallCode", estimateEndParkingPriceResult.getStallCode()));
            outputParameters.add(new Pair<String, String>("transactionId", estimateEndParkingPriceResult.getTransactionId()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateEndParkingPrice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return estimateEndParkingPriceResult;
    }

    @Override
    public EstimateExtendedParkingPriceResult estimateExtendedParkingPrice(String ticketId, String requestId, String transactionId, Date requestedEndTime) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        DecimalFormat decimalFormat = new DecimalFormat("#0.##");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionId", transactionId));
        inputParameters.add(new Pair<String, String>("requestedEndTime", dateFormat.format(requestedEndTime)));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateExtendedParkingPrice", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        EstimateExtendedParkingPriceResult estimateExtendedParkingPriceResult = null;

        try {
            Double parkingMaxAmount = Double.valueOf(this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PARKING_MAX_AMOUNT));
            String acquirerID = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_ACQUIRER_ID_CARTASI);
            String currency = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_UICCODE_REFUND_CARTASI);
            String groupAcquirer = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_GROUP_ACQUIRER_CARTASI);
            String serverName = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_SERVER_NAME);
            String shopLogin = this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PARKING_API_KEY);
            
            estimateExtendedParkingPriceResult = parkingV2EstimateExtendedParkingPriceAction.execute(parkingService, ticketId, requestId, transactionId, requestedEndTime, LANG,
                    acquirerID, currency, groupAcquirer, serverName, shopLogin, parkingMaxAmount);
        }
        catch (Exception e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, e.getMessage());
            estimateExtendedParkingPriceResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return estimateExtendedParkingPriceResult;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", estimateExtendedParkingPriceResult.getStatusCode()));

        if (estimateExtendedParkingPriceResult != null && estimateExtendedParkingPriceResult.getStatusCode().equals(ResponseHelper.PARKING_ESTIMATE_EXTEND_PARKING_PRICE_SUCCESS)) {
            
            outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(estimateExtendedParkingPriceResult.getNotice(), "|")));
            outputParameters.add(new Pair<String, String>("parkingEndTime", dateFormat.format(estimateExtendedParkingPriceResult.getParkingEndTime())));
            outputParameters.add(new Pair<String, String>("parkingStartTime", dateFormat.format(estimateExtendedParkingPriceResult.getParkingStartTime())));
            outputParameters.add(new Pair<String, String>("requestedEndTime", dateFormat.format(estimateExtendedParkingPriceResult.getRequestedEndTime())));
            outputParameters.add(new Pair<String, String>("parkingTimeCorrection", estimateExtendedParkingPriceResult.getParkingTimeCorrection().toString()));
            outputParameters.add(new Pair<String, String>("parkingId", estimateExtendedParkingPriceResult.getParkingId()));
            outputParameters.add(new Pair<String, String>("parkingZoneId", estimateExtendedParkingPriceResult.getParkingZoneId()));
            outputParameters.add(new Pair<String, String>("plateNumber", estimateExtendedParkingPriceResult.getPlateNumber()));
            outputParameters.add(new Pair<String, String>("price", decimalFormat.format(estimateExtendedParkingPriceResult.getPrice())));
            outputParameters.add(new Pair<String, String>("requestedEndTime", dateFormat.format(estimateExtendedParkingPriceResult.getRequestedEndTime())));
            outputParameters.add(new Pair<String, String>("stallCode", estimateExtendedParkingPriceResult.getStallCode()));
            outputParameters.add(new Pair<String, String>("transactionId", estimateExtendedParkingPriceResult.getTransactionId()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateExtendedParkingPrice", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return estimateExtendedParkingPriceResult;
    }

    @Override
    public ApproveExtendParkingTransactionResult approveExtendParkingTransaction(String ticketId, String requestId, String transactionId, Long paymentMethodId,
            String paymentMethodType, String encodedPin) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        DecimalFormat decimalFormat = new DecimalFormat("#0.##");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("transactionId", transactionId));
        //inputParameters.add(new Pair<String, String>("requestedEndTime", dateFormat.format(requestedEndTime)));
        inputParameters.add(new Pair<String, String>("paymentMethodId", String.valueOf(paymentMethodId)));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("encodedPin", encodedPin));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approveExtendParkingTransaction", requestId, "opening",
                ActivityLog.createLogMessage(inputParameters));

        ApproveExtendParkingTransactionResult approveExtendParkingTransactionResult = null;

        try {

            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            Double parkingMaxAmount = Double.valueOf(this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PARKING_MAX_AMOUNT));
            Double parkingPreauthAmount = Double.valueOf(this.parametersService.getParamValue(ParkingTransactionV2Service.PARAM_PARKING_PREAUTHORIZATION_AMOUNT));

            approveExtendParkingTransactionResult = parkingV2ExtendParkingAction.execute(parkingService, gpService, ticketId, requestId, encodedPin, pinCheckMaxAttempts,
                    transactionId, paymentMethodId, paymentMethodType, LANG, miCiceryDecodedSecretKey, parkingMaxAmount, parkingPreauthAmount);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            approveExtendParkingTransactionResult = new ApproveExtendParkingTransactionResult();
            approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return approveExtendParkingTransactionResult;
        }
        catch (Exception e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, e.getMessage());
            approveExtendParkingTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            return approveExtendParkingTransactionResult;
        }

        if (approveExtendParkingTransactionResult != null
                && approveExtendParkingTransactionResult.getStatusCode().equals(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_SUCCESS)) {

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", approveExtendParkingTransactionResult.getStatusCode()));

        outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(approveExtendParkingTransactionResult.getNotice(), "|")));
        outputParameters.add(new Pair<String, String>("parkingEndTime", dateFormat.format(approveExtendParkingTransactionResult.getParkingEndTime())));
        outputParameters.add(new Pair<String, String>("parkingStartTime", dateFormat.format(approveExtendParkingTransactionResult.getParkingStartTime())));
        outputParameters.add(new Pair<String, String>("requestedEndTime", dateFormat.format(approveExtendParkingTransactionResult.getRequestedEndTime())));
        outputParameters.add(new Pair<String, String>("parkingTimeCorrection", approveExtendParkingTransactionResult.getParkingTimeCorrection().toString()));
        outputParameters.add(new Pair<String, String>("parkingId", approveExtendParkingTransactionResult.getParkingId()));
        outputParameters.add(new Pair<String, String>("parkingZoneId", approveExtendParkingTransactionResult.getParkingZoneId()));
        outputParameters.add(new Pair<String, String>("plateNumber", approveExtendParkingTransactionResult.getPlateNumber()));
        outputParameters.add(new Pair<String, String>("price", decimalFormat.format(approveExtendParkingTransactionResult.getPrice())));
        outputParameters.add(new Pair<String, String>("requestedEndTime", dateFormat.format(approveExtendParkingTransactionResult.getRequestedEndTime())));
        outputParameters.add(new Pair<String, String>("stallCode", approveExtendParkingTransactionResult.getStallCode()));
        outputParameters.add(new Pair<String, String>("transactionId", approveExtendParkingTransactionResult.getTransactionId()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approveExtendParkingTransaction", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));
        }

        return approveExtendParkingTransactionResult;
    }

    @Override
    public RetrievePendingParkingTransactionResult retrievePendingParkingTransaction(String ticketId, String requestId) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketId));
        inputParameters.add(new Pair<String, String>("requestID", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrievePendingParkingTransaction", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrievePendingParkingTransactionResult retrievePendingParkingTransactionResult = null;
        try {
            retrievePendingParkingTransactionResult = parkingV2RetrievePendingParkingTransactionAction.execute(ticketId, requestId, this);
        }
        catch (EJBException ex) {
            retrievePendingParkingTransactionResult = new RetrievePendingParkingTransactionResult();
            retrievePendingParkingTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", retrievePendingParkingTransactionResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrievePendingParkingTransaction", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrievePendingParkingTransactionResult;
    }
    
    public RetrieveParkingZonesByCityResult retrieveParkingZonesByCity(String ticketId, String requestId, String cityId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("cityID", cityId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveParkingZonesByCity", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        RetrieveParkingZonesByCityResult retrieveParkingZonesByCityResult = null;

        try {
            retrieveParkingZonesByCityResult = parkingV2RetrieveParkingZonesByCityAction.execute(parkingService, ticketId, requestId, cityId, LANG);
        }
        catch (Exception e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, e.getMessage());
            retrieveParkingZonesByCityResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", retrieveParkingZonesByCityResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveParkingZonesByCity", requestId, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return retrieveParkingZonesByCityResult;
    }

    public GetCitiesResult getCities(String ticketId, String requestId) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCities", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        GetCitiesResult getCitiesResult = null;

        try {
            getCitiesResult = parkingV2GetCitiesAction.execute(parkingService, ticketId, requestId, LANG);
        }
        catch (Exception e) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, e.getMessage());
            getCitiesResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", getCitiesResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCities", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return getCitiesResult;
    }
    
    @Override
    public Authentication persistAuthToken(Authentication auth) {

        Authentication resfreshedAuthentication = parkingV2PersistAuthTokenAction.execute(auth);
        return resfreshedAuthentication;

    }

    @Override
    public Authentication checkAccessTokenState() {
        Authentication auth = parkingV2CheckAuthTokenStateAction.execute();
        return auth;
    }

    @Override
    public GetParkingTransactionDetailResult getParkingTransactionDetail(String ticketId, String requestId, String parkingTransactionId) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("parkingTransactionId", parkingTransactionId));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getParkingTransactionDetail", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        GetParkingTransactionDetailResult getParkingTransactionDetailResult = null;
        try {
            getParkingTransactionDetailResult = parkingV2GetParkingTransactionDetailAction.execute(ticketId, requestId, parkingTransactionId);
        }
        catch (EJBException ex) {
            getParkingTransactionDetailResult = new GetParkingTransactionDetailResult();
            getParkingTransactionDetailResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getParkingTransactionDetailResult.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getParkingTransactionDetail", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return getParkingTransactionDetailResult;
    }
}
