package com.techedge.mp.core.business;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.actions.poptransaction.PoPRetrievePendingTransactionAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionApproveShopAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionArchiveAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionConfirmTransactionAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionCreateShopAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionEnableShopAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionGetShopTransactionAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionGetSourceDetailAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionGetStationAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionGetStationListAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionGetTransactionDetailAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionRejectShopAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionRetrieveTransactionHistoryAction;
import com.techedge.mp.core.actions.poptransaction.PoPTransactionReverseShopAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GetStationListResponse;
import com.techedge.mp.core.business.interfaces.GetStationResponse;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.GetSourceDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCreateShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidEnableShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetPendingTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRejectShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidReverseShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.RetrieveTransactionHistoryResponse;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShopRefuelDetail;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShoppingCartDetail;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@LocalBean
public class PostPaidTransactionService implements PostPaidTransactionServiceRemote, PostPaidTransactionServiceLocal {

    /*
     * private static final String CURRENCY = "242"; private static final String
     * PAYMENT_TYPE = "MobilePayment"; private static final Integer
     * PIN_CHECK_MAX_ATTEMPTS = 5; private static final Integer
     * STATUS_MAX_ATTEMPTS = 3;
     */

    private static final String                            PARAM_UIC                                 = "UIC";
    //private static final String                            PARAM_SERVER_NAME                 = "SERVER_NAME";
    //private static final String                            PARAM_PAYMENT_TYPE                = "PAYMENT_TYPE";
    private static final String                            PARAM_PIN_CHECK_MAX_ATTEMPTS              = "PIN_CHECK_MAX_ATTEMPTS";
    //private static final String                            PARAM_STATUS_MAX_ATTEMPTS         = "STATUS_MAX_ATTEMPTS";
    private static final String                            PARAM_RECONCILIATION_MAX_ATTEMPTS         = "RECONCILIATION_MAX_ATTEMPTS";
    private static final String                            PARAM_RANGE_THRESHOLD                     = "RANGE_THRESHOLD";
    private static final String                            PARAM_RANGE_THRESHOLD_BLOCKING            = "RANGE_THRESHOLD_BLOCKING";
    private static final String                            PARAM_EMAIL_SENDER_ADDRESS                = "EMAIL_SENDER_ADDRESS";
    private static final String                            PARAM_SMTP_HOST                           = "SMTP_HOST";
    private static final String                            PARAM_SMTP_PORT                           = "SMTP_PORT";
    private static final String                            PARAM_RECEIPT_EMAIL_ACTIVE                = "RECEIPT_EMAIL_ACTIVE";
    private final static String                            PARAM_PROXY_HOST                          = "PROXY_HOST";
    private final static String                            PARAM_PROXY_PORT                          = "PROXY_PORT";
    private final static String                            PARAM_PROXY_NO_HOSTS                      = "PROXY_NO_HOSTS";
    private final static String                            PARAM_POST_PAID_TRANSACTION_TIMEOUT       = "POST_PAID_TRANSACTION_TIMEOUT";
    private static final String                            PARAM_STATION_LIST_MAX_RANGE              = "STATION_LIST_MAX_RANGE";
    private static final String                            PARAM_VOUCHER_PURCHASE_MIN_INTERVAL       = "VOUCHER_PURCHASE_MIN_INTERVAL";
    private final static String                            PARAM_USER_BLOCK_EXCEPTION                = "USER_BLOCK_EXCEPTION";
    private static final String                            PARAM_STATION_LIST_CORRECTION_COEFFICIENT = "STATION_LIST_CORRECTION_COEFFICIENT";
    private final static String                            PARAM_LOYALTY_SESSION_EXPIRY_TIME         = "LOYALTY_SESSION_EXPIRY_TIME";        //minutes
    private final static String                            PARAM_DEFAULT_NEXT_POLLING_INTERVAL       = "DEFAULT_NEXT_POLLING_INTERVAL";      //millis
    private final static String                            PARAM_STRING_SUBSTITUTION_PATTERN         = "STRING_SUBSTITUTION_PATTERN";
    private final static String                            PARAM_CARTASI_VAULT_BLOCK                 = "CARTASI_VAULT_BLOCK";
    private final static String                            PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY       = "CARTASI_VAULT_BLOCK_ATTRIBUTE_KEY";

    @EJB
    private PoPTransactionCreateShopAction                 popTransactionCreateShopAction;

    @EJB
    private PoPTransactionEnableShopAction                 popTransactionEnableShopAction;

    @EJB
    private PoPTransactionApproveShopAction                popTransactionApproveShopAction;

    @EJB
    private PoPTransactionRejectShopAction                 popTransactionRejectShopAction;

    @EJB
    private PoPTransactionGetShopTransactionAction         popTransactionGetShopTransactionAction;

    @EJB
    private PoPTransactionGetTransactionDetailAction       popTransactionGetTransactionDetailAction;

    @EJB
    private PoPTransactionReverseShopAction                popTransactionReverseShopAction;

    @EJB
    private PoPTransactionGetSourceDetailAction            popTransactionGetSourceDetailAction;

    @EJB
    private PoPRetrievePendingTransactionAction            poPRetrievePendingTransactionAction;

    @EJB
    private PoPTransactionGetStationAction                 popTransactionGetStations;

    @EJB
    private PoPTransactionGetStationListAction             popTransactionGetStationList;

    @EJB
    private PoPTransactionArchiveAction                    popArchive;

    @EJB
    private PoPTransactionConfirmTransactionAction         popTransactionConfirmTransactionAction;

    @EJB
    private PoPTransactionRetrieveTransactionHistoryAction popTransactionRetrieveTransactionHistoryAction;

    private LoggerService                                  loggerService                             = null;
    private ParametersService                              parametersService                         = null;
    private GPServiceRemote                                gpService                                 = null;
    private TransactionService                             transactionService                        = null;
    private ForecourtPostPaidServiceRemote                 forecourtPostPaidServiceRemote            = null;
    private ForecourtInfoServiceRemote                     forecourtInfoServiceRemote                = null;
    private FidelityServiceRemote                          fidelityService                           = null;
    private EmailSenderRemote                              emailSender                               = null;
    private UserCategoryService                            userCategoryService                       = null;
    private UnavailabilityPeriodService                    unavailabilityPeriodService               = null;

    private String                                         secretKey                                 = null;
    private StringSubstitution                             stringSubstitution                        = null;

    /**
     * Default constructor.
     */
    public PostPaidTransactionService() {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.gpService = EJBHomeCache.getInstance().getGpService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.transactionService = EJBHomeCache.getInstance().getTransactionService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.forecourtPostPaidServiceRemote = EJBHomeCache.getInstance().getForecourtPostPaidService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.forecourtInfoServiceRemote = EJBHomeCache.getInstance().getForecourtInfoService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.fidelityService = EJBHomeCache.getInstance().getFidelityService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.unavailabilityPeriodService = EJBHomeCache.getInstance().getUnavailabilityPeriodService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }
        
        String pattern = null;
        
        try {
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
        }
        catch (ParameterNotFoundException ex) {
            System.err.println("Parameter not found: " + ex.getMessage());
        }
        
        String vaultBlock = "";
        String vaultBlockAttributeCryptKey = "";

        try {

            vaultBlock = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK);
            vaultBlockAttributeCryptKey = parametersService.getParamValue(PARAM_CARTASI_VAULT_BLOCK_CRYPT_KEY);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeCryptKey + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.secretKey + "'");
        
        stringSubstitution = new StringSubstitution(pattern);
    }

    @Override
    public PostPaidApproveShopTransactionResponse approveShopTransaction(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentMethodType,
            String encodedPin, Boolean useVoucher) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("paymentMethodId", paymentMethodId.toString()));
        inputParameters.add(new Pair<String, String>("paymentMethodType", paymentMethodType));
        inputParameters.add(new Pair<String, String>("encodedPin", encodedPin));

        if (useVoucher != null) {
            inputParameters.add(new Pair<String, String>("useVoucher", useVoucher.toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("useVoucher", null));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approveShopTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidApproveShopTransactionResponse postPaidApproveShopTransactionResponse = null;
        try {

            String emailSenderAddress = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_SMTP_PORT);
            String receiptEmailActiveString = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_RECEIPT_EMAIL_ACTIVE);
            String proxyHost = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_PROXY_NO_HOSTS);

            Boolean receiptEmailActive = false;
            if (receiptEmailActiveString.equals("true")) {
                receiptEmailActive = true;
            }

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            //String serverName = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_SERVER_NAME);
            //String currency = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_UIC);
            //String paymentType = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_PAYMENT_TYPE);
            Integer pinCheckMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            //Integer statusMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_STATUS_MAX_ATTEMPTS));
            Integer voucherPurchaseMinInterval = Integer.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_VOUCHER_PURCHASE_MIN_INTERVAL));
            String userBlockException = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_USER_BLOCK_EXCEPTION);
            List<String> userBlockExceptionList = Arrays.asList(userBlockException.split(";"));

            postPaidApproveShopTransactionResponse = popTransactionApproveShopAction.execute(requestID, ticketID, mpTransactionID, paymentMethodId, paymentMethodType, encodedPin,
                    useVoucher, pinCheckMaxAttempts, voucherPurchaseMinInterval, userBlockExceptionList, gpService, forecourtPostPaidServiceRemote, fidelityService,
                    userCategoryService, receiptEmailActive, emailSender, unavailabilityPeriodService, proxyHost, proxyPort, proxyNoHosts, null);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidApproveShopTransactionResponse = new PostPaidApproveShopTransactionResponse();
            postPaidApproveShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            postPaidApproveShopTransactionResponse = new PostPaidApproveShopTransactionResponse();
            postPaidApproveShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", postPaidApproveShopTransactionResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "approveShopTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return postPaidApproveShopTransactionResponse;
    }

    @Override
    public PostPaidEnableShopTransactionResponse enableShopTransaction(String requestID, String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enableShopTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidEnableShopTransactionResponse postPaidEnableShopTransactionResponse = null;
        try {

            // String serverName =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_SERVER_NAME);
            // String currency =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_UIC);
            // String paymentType =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_PAYMENT_TYPE);
            // Integer pinCheckMaxAttempts =
            // Integer.valueOf(this.parametersService.getParamValue(PostPayedTransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            // Integer statusMaxAttempts =
            // Integer.valueOf(this.parametersService.getParamValue(PostPayedTransactionService.PARAM_STATUS_MAX_ATTEMPTS));

            postPaidEnableShopTransactionResponse = popTransactionEnableShopAction.execute(requestID, mpTransactionID);
        }
        // catch (ParameterNotFoundException ex) {
        // this.loggerService.log(ErrorLevel.ERROR,
        // this.getClass().getSimpleName(),
        // null, null, null, ex.getMessage() );
        // postPayedEnableShopTransactionResponse = new
        // PostPayedEnableShopTransactionResponse();
        // postPayedEnableShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE);
        // }
        catch (EJBException ex) {
            postPaidEnableShopTransactionResponse = new PostPaidEnableShopTransactionResponse();
            postPaidEnableShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", postPaidEnableShopTransactionResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enableShopTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return postPaidEnableShopTransactionResponse;
    }

    @Override
    public PostPaidCreateShopTransactionResponse createShopTransaction(String requestID, String source, // "CASH-REGISTER"
            String sourceID, String sourceNumber, String stationID, String srcTransactionID, Double amount, String productType, // OIL
                                                                                                                                // NON_OIL
                                                                                                                                // MIXED
            // String currency,
            List<ShoppingCartDetail> shoppingCartList, List<ShopRefuelDetail> shopRefuelDetail) {

        // String paymentMethodIdString = "";

        // if ( paymentMethodId != null ) {
        // paymentMethodIdString = paymentMethodId.toString();
        // }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("source", source));
        inputParameters.add(new Pair<String, String>("sourceID", sourceID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("srcTransactionID", srcTransactionID));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        inputParameters.add(new Pair<String, String>("productType", productType));
        if (shoppingCartList != null) {
            inputParameters.add(new Pair<String, String>("shoppingCartList", shoppingCartList.toString()));
        }
        if (shopRefuelDetail != null) {
            inputParameters.add(new Pair<String, String>("shopRefuelDetail", shopRefuelDetail.toString()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createShopTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidCreateShopTransactionResponse postPaidCreateShopTransactionResponse = null;
        try {

            // String serverName =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_SERVER_NAME);
            String currency = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_UIC);
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            // String paymentType =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_PAYMENT_TYPE);
            // Integer pinCheckMaxAttempts =
            // Integer.valueOf(this.parametersService.getParamValue(PostPayedTransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            // Integer statusMaxAttempts =
            // Integer.valueOf(this.parametersService.getParamValue(PostPayedTransactionService.PARAM_STATUS_MAX_ATTEMPTS));

            postPaidCreateShopTransactionResponse = popTransactionCreateShopAction.execute(requestID, source, // "CASH-REGISTER"
                    sourceID, sourceNumber, stationID, srcTransactionID, amount, productType, // OIL
                                                                                              // NON_OIL
                                                                                              // MIXED
                    currency, reconciliationMaxAttempts, shoppingCartList, shopRefuelDetail, forecourtPostPaidServiceRemote);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidCreateShopTransactionResponse = new PostPaidCreateShopTransactionResponse();
            postPaidCreateShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            postPaidCreateShopTransactionResponse = new PostPaidCreateShopTransactionResponse();
            postPaidCreateShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", postPaidCreateShopTransactionResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createShopTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return postPaidCreateShopTransactionResponse;
    }

    @Override
    public PostPaidRejectShopTransactionResponse rejectShopTransaction(String requestID, String ticketID, String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "rejectShopTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidRejectShopTransactionResponse postPaidRejectShopTransactionResponse = null;
        try {

            // String serverName =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_SERVER_NAME);
            // String currency =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_UIC);
            // String paymentType =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_PAYMENT_TYPE);
            // Integer pinCheckMaxAttempts =
            // Integer.valueOf(this.parametersService.getParamValue(PostPayedTransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            // Integer statusMaxAttempts =
            // Integer.valueOf(this.parametersService.getParamValue(PostPayedTransactionService.PARAM_STATUS_MAX_ATTEMPTS));

            postPaidRejectShopTransactionResponse = popTransactionRejectShopAction.execute(requestID, ticketID, mpTransactionID, forecourtPostPaidServiceRemote);
        }
        // catch (ParameterNotFoundException ex) {
        // this.loggerService.log(ErrorLevel.ERROR,
        // this.getClass().getSimpleName(),
        // null, null, null, ex.getMessage() );
        // postPayedRejectShopTransactionResponse = new
        // PostPayedRejectShopTransactionResponse();
        // postPayedRejectShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        // }
        catch (EJBException ex) {
            postPaidRejectShopTransactionResponse = new PostPaidRejectShopTransactionResponse();
            postPaidRejectShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", postPaidRejectShopTransactionResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "rejectShopTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return postPaidRejectShopTransactionResponse;
    }

    @Override
    public PostPaidGetShopTransactionResponse getShopTransaction(String requestID, String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getShopTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidGetShopTransactionResponse postPaidGetShopTransactionResponse = null;

        try {
            postPaidGetShopTransactionResponse = popTransactionGetShopTransactionAction.execute(requestID, mpTransactionID);
        }
        catch (EJBException ex) {
            postPaidGetShopTransactionResponse = new PostPaidGetShopTransactionResponse();
            postPaidGetShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", postPaidGetShopTransactionResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getShopTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return postPaidGetShopTransactionResponse;
    }

    @Override
    public PostPaidGetTransactionDetailResponse getTransactionDetail(String requestID, String ticketID, String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionDetail", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidGetTransactionDetailResponse popPaidGetTransactionDetailResponse = null;

        try {
            popPaidGetTransactionDetailResponse = popTransactionGetTransactionDetailAction.execute(requestID, ticketID, mpTransactionID);
        }
        catch (EJBException ex) {
            popPaidGetTransactionDetailResponse = new PostPaidGetTransactionDetailResponse();
            popPaidGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", popPaidGetTransactionDetailResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionDetail", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return popPaidGetTransactionDetailResponse;
    }

    /*
     * @Override public PostPaidGetShopTransactionJsonResponse
     * getShopTransactionByJson( String requestID, String ticketID, String
     * mpTransactionID) {
     * 
     * 
     * Set<Pair<String, String>> inputParameters = new HashSet<Pair<String,
     * String>>();
     * 
     * inputParameters.add(new Pair<String,String>("requestID", requestID));
     * inputParameters.add(new Pair<String,String>("ticketID", ticketID));
     * inputParameters.add(new Pair<String,String>("mpTransactionID",
     * mpTransactionID));
     * 
     * 
     * this.loggerService.log( ErrorLevel.DEBUG,
     * this.getClass().getSimpleName(), "getShopTransaction", requestID,
     * "opening", ActivityLog.createLogMessage(inputParameters));
     * 
     * PostPaidGetShopTransactionJsonResponse
     * postPaidGetShopTransactionJsonResponse = null; try {
     * 
     * // String serverName =
     * this.parametersService.getParamValue(PostPayedTransactionService
     * .PARAM_SERVER_NAME); // String currency =
     * this.parametersService.getParamValue
     * (PostPayedTransactionService.PARAM_UIC); // String paymentType =
     * this.parametersService
     * .getParamValue(PostPayedTransactionService.PARAM_PAYMENT_TYPE); //
     * Integer pinCheckMaxAttempts =
     * Integer.valueOf(this.parametersService.getParamValue(
     * PostPayedTransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS)); // Integer
     * statusMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(
     * PostPayedTransactionService.PARAM_STATUS_MAX_ATTEMPTS));
     * 
     * postPaidGetShopTransactionJsonResponse =
     * popTransactionGetShopByJsonAction.execute( requestID, ticketID,
     * mpTransactionID); } // catch (ParameterNotFoundException ex) { //
     * this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(),
     * null, null, null, ex.getMessage() ); //
     * postPayedEnableShopTransactionResponse = new
     * PostPayedEnableShopTransactionResponse(); //
     * postPayedEnableShopTransactionResponse
     * .setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE); // } catch
     * (EJBException ex) { postPaidGetShopTransactionJsonResponse = new
     * PostPaidGetShopTransactionJsonResponse();
     * postPaidGetShopTransactionJsonResponse
     * .setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE); }
     * 
     * Set<Pair<String, String>> outputParameters = new HashSet<Pair<String,
     * String>>(); outputParameters.add(new Pair<String,String>("statusCode",
     * postPaidGetShopTransactionJsonResponse.getStatusCode()));
     * 
     * this.loggerService.log( ErrorLevel.DEBUG,
     * this.getClass().getSimpleName(), "getShopTransaction", requestID,
     * "closing", ActivityLog.createLogMessage(outputParameters));
     * 
     * return postPaidGetShopTransactionJsonResponse; }
     */

    /*
     * @Override public PostPaidDetailTransactionJsonResponse
     * getPoPTransactionDetailByJson( String requestID, String ticketID, String
     * mpTransactionID) {
     * 
     * 
     * Set<Pair<String, String>> inputParameters = new HashSet<Pair<String,
     * String>>();
     * 
     * inputParameters.add(new Pair<String,String>("requestID", requestID));
     * inputParameters.add(new Pair<String,String>("ticketID", ticketID));
     * inputParameters.add(new Pair<String,String>("mpTransactionID",
     * mpTransactionID));
     * 
     * 
     * this.loggerService.log( ErrorLevel.DEBUG,
     * this.getClass().getSimpleName(), "getPoPTransactionDetailByJson",
     * requestID, "opening", ActivityLog.createLogMessage(inputParameters));
     * 
     * PostPaidDetailTransactionJsonResponse
     * postPaidDetailTransactionJsonResponse = null; try {
     * 
     * 
     * postPaidDetailTransactionJsonResponse =
     * popTransactionDetailByJsonAction.execute( requestID, ticketID,
     * mpTransactionID); }
     * 
     * catch (EJBException ex) { postPaidDetailTransactionJsonResponse = new
     * PostPaidDetailTransactionJsonResponse();
     * postPaidDetailTransactionJsonResponse
     * .setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE); }
     * 
     * Set<Pair<String, String>> outputParameters = new HashSet<Pair<String,
     * String>>(); outputParameters.add(new Pair<String,String>("statusCode",
     * postPaidDetailTransactionJsonResponse.getStatusCode()));
     * 
     * this.loggerService.log( ErrorLevel.DEBUG,
     * this.getClass().getSimpleName(), "getPoPTransactionDetailByJson",
     * requestID, "closing", ActivityLog.createLogMessage(outputParameters));
     * 
     * return postPaidDetailTransactionJsonResponse; }
     */

    @Override
    public PostPaidReverseShopTransactionResponse reverseShopTransaction(String requestID, String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reverseShopTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PostPaidReverseShopTransactionResponse postPaidReverseShopTransactionResponse = null;
        try {

            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(this.secretKey);
            
            // String serverName =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_SERVER_NAME);
            // String currency =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_UIC);
            // String paymentType =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_PAYMENT_TYPE);
            // Integer pinCheckMaxAttempts =
            // Integer.valueOf(this.parametersService.getParamValue(PostPayedTransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            // Integer statusMaxAttempts =
            // Integer.valueOf(this.parametersService.getParamValue(PostPayedTransactionService.PARAM_STATUS_MAX_ATTEMPTS));

            String emailSenderAddress = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_EMAIL_SENDER_ADDRESS);
            String smtpHost = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_SMTP_HOST);
            String smtpPort = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_SMTP_PORT);
            String proxyHost = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_PROXY_HOST);
            String proxyPort = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_PROXY_PORT);
            String proxyNoHosts = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_PROXY_NO_HOSTS);

            this.emailSender.init(smtpHost, smtpPort, proxyHost, proxyPort, proxyNoHosts, emailSenderAddress);

            postPaidReverseShopTransactionResponse = popTransactionReverseShopAction.execute(requestID, mpTransactionID, gpService, forecourtPostPaidServiceRemote,
                    fidelityService, emailSender, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution, encryptionAES);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidReverseShopTransactionResponse = new PostPaidReverseShopTransactionResponse();
            postPaidReverseShopTransactionResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        // catch (ParameterNotFoundException ex) {
        // this.loggerService.log(ErrorLevel.ERROR,
        // this.getClass().getSimpleName(),
        // null, null, null, ex.getMessage() );
        // postPayedEnableShopTransactionResponse = new
        // PostPayedEnableShopTransactionResponse();
        // postPayedEnableShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE);
        // }
        catch (EJBException ex) {
            postPaidReverseShopTransactionResponse = new PostPaidReverseShopTransactionResponse();
            postPaidReverseShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_FAILURE);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidReverseShopTransactionResponse = new PostPaidReverseShopTransactionResponse();
            postPaidReverseShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", postPaidReverseShopTransactionResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reverseShopTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return postPaidReverseShopTransactionResponse;
    }

    /*
     * @Override public PostPaidGetTransactionByQRCodeResponse
     * getTransactionByQRCode( String requestID, String ticketID, String qrCode)
     * {
     * 
     * 
     * Set<Pair<String, String>> inputParameters = new HashSet<Pair<String,
     * String>>();
     * 
     * inputParameters.add(new Pair<String,String>("requestID", requestID));
     * inputParameters.add(new Pair<String,String>("ticketID", ticketID));
     * inputParameters.add(new Pair<String,String>("qrCode", qrCode));
     * 
     * 
     * 
     * this.loggerService.log( ErrorLevel.DEBUG,
     * this.getClass().getSimpleName(), "getByQRCode", requestID, "opening",
     * ActivityLog.createLogMessage(inputParameters));
     * 
     * PostPaidGetTransactionByQRCodeResponse
     * postPaidGetTransactionByQRCodeResponse = null; try {
     * 
     * // String serverName =
     * this.parametersService.getParamValue(PostPayedTransactionService
     * .PARAM_SERVER_NAME);
     * 
     * 
     * // String paymentType =
     * this.parametersService.getParamValue(PostPayedTransactionService
     * .PARAM_PAYMENT_TYPE); // Integer pinCheckMaxAttempts =
     * Integer.valueOf(this.
     * parametersService.getParamValue(PostPayedTransactionService
     * .PARAM_PIN_CHECK_MAX_ATTEMPTS)); // Integer statusMaxAttempts =
     * Integer.valueOf
     * (this.parametersService.getParamValue(PostPayedTransactionService
     * .PARAM_STATUS_MAX_ATTEMPTS)); String currency =
     * this.parametersService.getParamValue
     * (PostPaidTransactionService.PARAM_UIC);
     * postPaidGetTransactionByQRCodeResponse =
     * popTransactionGetByQRCode.execute( requestID, ticketID, qrCode, currency,
     * forecourtPostPaidServiceRemote ); } catch (ParameterNotFoundException ex)
     * { this.loggerService.log(ErrorLevel.ERROR,
     * this.getClass().getSimpleName(), null, null, null, ex.getMessage() );
     * postPaidGetTransactionByQRCodeResponse = new
     * PostPaidGetTransactionByQRCodeResponse();
     * postPaidGetTransactionByQRCodeResponse
     * .setStatusCode(ResponseHelper.PP_TRANSACTION_GET_FAILURE); } catch
     * (EJBException ex) { postPaidGetTransactionByQRCodeResponse = new
     * PostPaidGetTransactionByQRCodeResponse();
     * postPaidGetTransactionByQRCodeResponse
     * .setStatusCode(ResponseHelper.PP_TRANSACTION_GET_FAILURE); }
     * 
     * Set<Pair<String, String>> outputParameters = new HashSet<Pair<String,
     * String>>(); outputParameters.add(new Pair<String,String>("statusCode",
     * postPaidGetTransactionByQRCodeResponse.getStatusCode()));
     * 
     * this.loggerService.log( ErrorLevel.DEBUG,
     * this.getClass().getSimpleName(), "getShopTransaction", requestID,
     * "closing", ActivityLog.createLogMessage(outputParameters));
     * 
     * return postPaidGetTransactionByQRCodeResponse; }
     */

    @Override
    public GetSourceDetailResponse getSourceDetail(String requestID, String ticketID, String codeType, String sourceID, String beaconCode, Double userPositionLatitude,
            Double userPositionLongitude) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        String userPositionLatitudeString = null;
        if (userPositionLatitude != null) {
            userPositionLatitudeString = userPositionLatitude.toString();
        }

        String userPositionLongitudeString = null;
        if (userPositionLongitude != null) {
            userPositionLongitudeString = userPositionLongitude.toString();
        }

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("codeType", codeType));
        inputParameters.add(new Pair<String, String>("sourceID", sourceID));
        inputParameters.add(new Pair<String, String>("beaconCode", beaconCode));
        inputParameters.add(new Pair<String, String>("userPositionLatitudeString", userPositionLatitudeString));
        inputParameters.add(new Pair<String, String>("userPositionLongitudeString", userPositionLongitudeString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getSourceDetail", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetSourceDetailResponse getSourceDetailResponse = null;

        try {

            String currency = this.parametersService.getParamValue(PostPaidTransactionService.PARAM_UIC);
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_RECONCILIATION_MAX_ATTEMPTS));
            Double rangeThreshold = new Double(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_RANGE_THRESHOLD));
            Boolean rangeThresholdBlocking = new Boolean(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_RANGE_THRESHOLD_BLOCKING));
            Integer transactionTimeout = Integer.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_POST_PAID_TRANSACTION_TIMEOUT));

            System.out.println("param range threshold: '" + this.parametersService.getParamValue(PostPaidTransactionService.PARAM_RANGE_THRESHOLD)
                    + "' - param range threshold blocking: '" + this.parametersService.getParamValue(PostPaidTransactionService.PARAM_RANGE_THRESHOLD_BLOCKING) + "'");

            getSourceDetailResponse = popTransactionGetSourceDetailAction.execute(requestID, ticketID, codeType, sourceID, beaconCode, userPositionLatitude, userPositionLongitude,
                    currency, reconciliationMaxAttempts, rangeThreshold, rangeThresholdBlocking, transactionTimeout, forecourtInfoServiceRemote, forecourtPostPaidServiceRemote,
                    userCategoryService);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getSourceDetailResponse = new GetSourceDetailResponse();
            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getSourceDetailResponse = new GetSourceDetailResponse();
            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getSourceDetailResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getSourceDetail", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getSourceDetailResponse;
    }

    @Override
    public PostPaidGetPendingTransactionResponse retrievePoPPendingTransaction(String requestID, String ticketID, String loyaltySessionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("loyaltySessionID", loyaltySessionID != null ? loyaltySessionID : ""));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrievePoPPendingTransaction", requestID, "opening",
                ActivityLog.createLogMessage(inputParameters));

        PostPaidGetPendingTransactionResponse postPaidGetPendingRefuelResponse = null;
        try {
            Integer loyaltySessionExpiryTime = Integer.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_LOYALTY_SESSION_EXPIRY_TIME));
            Integer defaultNextPollingInterval = Integer.parseInt(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_DEFAULT_NEXT_POLLING_INTERVAL));
            // String serverName =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_SERVER_NAME);

            // String paymentType =
            // this.parametersService.getParamValue(PostPayedTransactionService.PARAM_PAYMENT_TYPE);
            // Integer pinCheckMaxAttempts =
            // Integer.valueOf(this.parametersService.getParamValue(PostPayedTransactionService.PARAM_PIN_CHECK_MAX_ATTEMPTS));
            // Integer statusMaxAttempts =
            // Integer.valueOf(this.parametersService.getParamValue(PostPayedTransactionService.PARAM_STATUS_MAX_ATTEMPTS));
            // String currency =
            // this.parametersService.getParamValue(PostPaidTransactionService.PARAM_UIC);
            postPaidGetPendingRefuelResponse = poPRetrievePendingTransactionAction.execute(requestID, ticketID, loyaltySessionID, loyaltySessionExpiryTime,
                    defaultNextPollingInterval);
        }
        // catch (ParameterNotFoundException ex) {
        // this.loggerService.log(ErrorLevel.ERROR,
        // this.getClass().getSimpleName(),
        // null, null, null, ex.getMessage() );
        // postPaidGetPendingRefuelResponse = new
        // PostPaidGetPendingRefuelResponse();
        // postPaidGetPendingRefuelResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GET_FAILURE);
        // }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidGetPendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
            postPaidGetPendingRefuelResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            postPaidGetPendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
            postPaidGetPendingRefuelResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", postPaidGetPendingRefuelResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrievePoPPendingTransaction", requestID, "closing",
                ActivityLog.createLogMessage(outputParameters));

        return postPaidGetPendingRefuelResponse;
    }

    @Override
    public GetStationResponse getStation(String requestID, String ticketID, String codeType, String beaconCode, Double userPositionLatitude, Double userPositionLongitude) {

        String userPositionLatitudeString = "";
        if (userPositionLatitude != null) {
            userPositionLatitudeString = userPositionLatitude.toString();
        }

        String userPositionLongitudeString = "";
        if (userPositionLatitude != null) {
            userPositionLongitudeString = userPositionLongitude.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("codeType", codeType));
        inputParameters.add(new Pair<String, String>("beaconCode", beaconCode));
        inputParameters.add(new Pair<String, String>("userPositionLatitude", userPositionLatitudeString));
        inputParameters.add(new Pair<String, String>("userPositionLongitude", userPositionLongitudeString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "GetStation", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetStationResponse getStationsResponse = null;

        try {

            Double outRangeThreshold = Double.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_RANGE_THRESHOLD));
            Boolean rangeThresholdBlocking = Boolean.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_RANGE_THRESHOLD_BLOCKING));

            getStationsResponse = popTransactionGetStations.execute(requestID, ticketID, codeType, beaconCode, userPositionLatitude, userPositionLongitude, outRangeThreshold,
                    rangeThresholdBlocking, this.transactionService, this.forecourtInfoServiceRemote);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getStationsResponse = new GetStationResponse();
            getStationsResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            getStationsResponse = new GetStationResponse();
            getStationsResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getStationsResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "GetStation", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getStationsResponse;
    }

    @Override
    public GetStationListResponse getStationList(String requestID, String ticketID, Double userPositionLatitude, Double userPositionLongitude) {

        String userPositionLatitudeString = "";
        if (userPositionLatitude != null) {
            userPositionLatitudeString = userPositionLatitude.toString();
        }

        String userPositionLongitudeString = "";
        if (userPositionLatitude != null) {
            userPositionLongitudeString = userPositionLongitude.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("userPositionLatitude", userPositionLatitudeString));
        inputParameters.add(new Pair<String, String>("userPositionLongitude", userPositionLongitudeString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getStationList", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetStationListResponse getStationsListResponse = null;

        try {

            Integer stationListMaxRange = Integer.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_STATION_LIST_MAX_RANGE));
            Double stationListCorrectionCoefficient = Double.valueOf(this.parametersService.getParamValue(PostPaidTransactionService.PARAM_STATION_LIST_CORRECTION_COEFFICIENT));

            getStationsListResponse = popTransactionGetStationList.execute(requestID, ticketID, userPositionLatitude, userPositionLongitude, stationListMaxRange,
                    stationListCorrectionCoefficient, this.transactionService, this.forecourtInfoServiceRemote);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            getStationsListResponse = new GetStationListResponse();
            getStationsListResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (EJBException ex) {
            getStationsListResponse = new GetStationListResponse();
            getStationsListResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getStationsListResponse.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getStationList", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getStationsListResponse;
    }

    @Override
    public String archiveTransactions() {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "archivePoPTransactions", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = popArchive.execute();
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "archivePoPTransactions", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public RetrieveTransactionHistoryResponse retrieveTransactionHistory(String requestID, String ticketID, Date startDate, Date endDate, int itemsLimit, int pageOffset) {
        // TODO Auto-generated method stub
        RetrieveTransactionHistoryResponse response = null;

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("startDate", sdf.format(startDate)));
        inputParameters.add(new Pair<String, String>("endDate", sdf.format(endDate)));
        inputParameters.add(new Pair<String, String>("itemsLimit", String.valueOf(itemsLimit)));
        inputParameters.add(new Pair<String, String>("pageOffset", String.valueOf(pageOffset)));
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTransactionHistory", null, "opening", ActivityLog.createLogMessage(inputParameters));

        try {
            response = popTransactionRetrieveTransactionHistoryAction.execute(requestID, ticketID, startDate, endDate, itemsLimit, pageOffset, userCategoryService);
        }
        catch (EJBException ex) {
            response = new RetrieveTransactionHistoryResponse();
            response.setStatusCode(ResponseHelper.RETRIEVE_TRANSACTION_HISTORY_FAILURE);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response.getStatusCode()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveTransactionHistory", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public String confirmTransaction(String ticketID, String requestID, String transactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketID", ticketID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "confirmTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        String response = null;
        try {
            response = popTransactionConfirmTransactionAction.execute(ticketID, requestID, transactionID);
        }
        catch (EJBException ex) {
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "confirmTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

}
