package com.techedge.mp.core.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.techedge.mp.core.actions.eventnotification.CheckTransactionAction;
import com.techedge.mp.core.actions.eventnotification.GetTransactionDetailAction;
import com.techedge.mp.core.actions.eventnotification.SetEventNotificationAction;
import com.techedge.mp.core.actions.multicard.MulticardRefuelingStatusNotificationAction;
import com.techedge.mp.core.actions.multicard.MulticardStatusNotificationAction;
import com.techedge.mp.core.actions.pushnotification.RetrieveNotificationAction;
import com.techedge.mp.core.actions.pushnotification.UpdatePushNotificationAction;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.CheckTransactionResponse;
import com.techedge.mp.core.business.interfaces.DpanStatus;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MulticardStatusNotificationResponse;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ParameterNotification;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.loyalty.EventNotification;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransaction;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionNonOil;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionRefuel;
import com.techedge.mp.core.business.interfaces.loyalty.RewardTransaction;
import com.techedge.mp.core.business.interfaces.loyalty.RewardTransactionParameter;
import com.techedge.mp.core.business.interfaces.pushnotification.NotificationResponse;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;

/**
 * Session Bean implementation class SurveyService
 */
@Startup
@Singleton
//@Stateless
//@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class EventNotificationService implements EventNotificationServiceLocal, EventNotificationServiceRemote {

    @Resource
    private EJBContext                        context;

    @Resource
    private SessionContext                    sessionContext;

    @EJB
    private GetTransactionDetailAction        getTransactionDetailAction;

    @EJB
    private UpdatePushNotificationAction      updatePushNotificationAction;

    @EJB
    private RetrieveNotificationAction        retrieveNotificationAction;

    @EJB
    private SetEventNotificationAction        setEventNotificationAction;

    @EJB
    private CheckTransactionAction            checkTransactionAction;

    @EJB
    private MulticardStatusNotificationAction multicardStatusNotificationAction;

    @EJB
    private MulticardRefuelingStatusNotificationAction multicardRefuelingStatusNotificationAction;
    
    private final static String               PARAM_RECONCILIATION_MAX_ATTEMPTS        = "RECONCILIATION_MAX_ATTEMPTS";
    private final static String               PARAM_PUSH_NOTIFICATION_EXPIRY_TIME      = "PUSH_NOTIFICATION_MAX_EXPIRY_TIME";
    private final static String               PARAM_PUSH_NOTIFICATION_SENDING_DELAY    = "PUSH_NOTIFICATION_SENDING_DELAY";
    private final static String               PARAM_VOUCHER_REWARD_ALERT_RECIPIENT     = "VOUCHER_REWARD_ALERT_RECIPIENT";
    private final static String               PARAM_CHECK_PAYMENT_TRANSACTION_INTERVAL = "CHECK_PAYMENT_TRANSACTION_INTERVAL";
    private final static String               PARAM_STRING_SUBSTITUTION_PATTERN        = "STRING_SUBSTITUTION_PATTERN";

    private LoggerService                     loggerService                            = null;
    private ParametersService                 parametersService                        = null;
    private PushNotificationServiceRemote     pushNotificationService                  = null;
    private EmailSenderRemote                 emailSender                              = null;
    private UserCategoryService               userCategoryService                      = null;
    private CardInfoServiceRemote             cardInfoService                          = null;

    private StringSubstitution                stringSubstitution                       = null;

    public EventNotificationService() {
        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get LoggerService object");
            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ParametersService object");

            throw new EJBException(e);
        }

        try {
            this.userCategoryService = EJBHomeCache.getInstance().getUserCategoryService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get UserCategoryService object");

            throw new EJBException(e);
        }
        
        try {
            this.cardInfoService = EJBHomeCache.getInstance().getCardInfoService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get CardInfoService object");

            throw new EJBException(e);
        }
        
        String pattern = null;

        try {
            pattern = parametersService.getParamValue(PARAM_STRING_SUBSTITUTION_PATTERN);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        stringSubstitution = new StringSubstitution(pattern);
    }

    @PostConstruct
    public void initialize() {}

    @Override
    public EventNotification getTransactionDetail(String ticketId, String requestId, Long eventNotificationId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("eventNotificationId", eventNotificationId.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionDetail", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        EventNotification eventNotificationResponse = new EventNotification();

        try {

            eventNotificationResponse = getTransactionDetailAction.execute(ticketId, requestId, eventNotificationId);
        }

        catch (EJBException ex) {
            eventNotificationResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        String message = "{ eventType: " + eventNotificationResponse.getEventType() + ", fiscalCode: " + eventNotificationResponse.getFiscalCode() + ", srcTransactionID: "
                + eventNotificationResponse.getSrcTransactionID() + ", requestTimestamp: "
                + (eventNotificationResponse.getRequestTimestamp() != null ? eventNotificationResponse.getRequestTimestamp().toString() : "") + ", sessionID: "
                + (eventNotificationResponse.getSessionID() != null ? eventNotificationResponse.getSessionID() : "") + ", panCode: " + eventNotificationResponse.getPanCode()
                + ", stationID: " + eventNotificationResponse.getStationID() + ", terminalID: "
                + ((eventNotificationResponse.getTerminalID() != null) ? eventNotificationResponse.getTerminalID() : "") + ", transactionResult: "
                + eventNotificationResponse.getTransactionResult() + ", transactionDate: "
                + (eventNotificationResponse.getTransactionDate() != null ? eventNotificationResponse.getTransactionDate() : "") + ", credits: "
                + (eventNotificationResponse.getCredits() != null ? eventNotificationResponse.getCredits() : 0) + ", balance: " + eventNotificationResponse.getBalance()
                + ", marketingMsg: " + (eventNotificationResponse.getMarketingMsg() != null ? eventNotificationResponse.getMarketingMsg() : "") + ", warningMsg: "
                + (eventNotificationResponse.getWarningMsg() != null ? eventNotificationResponse.getWarningMsg() : "") + ", operationType: "
                + (eventNotificationResponse.getOperationType() != null ? eventNotificationResponse.getOperationType() : "") + ", subtype: "
                + (eventNotificationResponse.getSubtype() != null ? eventNotificationResponse.getSubtype() : "") + ", loyaltyTransactionDetail: ";

        if (eventNotificationResponse.getLoyaltyTransactionDetail() != null) {
            LoyaltyTransaction loyaltyTransactionDetail = eventNotificationResponse.getLoyaltyTransactionDetail();
            message += "{ " + "paymentMode: " + loyaltyTransactionDetail.getPaymentMode() + ", refuelDetail: ";

            if (loyaltyTransactionDetail.getRefuelDetails() != null) {
                for (LoyaltyTransactionRefuel refuelDetail : loyaltyTransactionDetail.getRefuelDetails()) {
                    String messageTmp = "{ " + "refuelMode: " + refuelDetail.getRefuelMode() + ", pumpNumber: "
                            + (refuelDetail.getPumpNumber() != null ? refuelDetail.getPumpNumber() : "") + ", productID: " + refuelDetail.getProductID() + ", productDescription: "
                            + (refuelDetail.getProductDescription() != null ? refuelDetail.getProductDescription() : "") + ", fuelQuantity: " + refuelDetail.getFuelQuantity()
                            + ", amount: " + refuelDetail.getAmount() + ", credits: " + refuelDetail.getCredits() + " }";

                    message += messageTmp;
                }
            }
            else {
                message += "{ null }";
            }

            message += ", nonOilDetail: ";

            if (loyaltyTransactionDetail.getNonOilDetails() != null) {
                for (LoyaltyTransactionNonOil nonOilDetail : loyaltyTransactionDetail.getNonOilDetails()) {
                    String messageTmp = "{ " + "productID: " + (nonOilDetail.getProductID() != null ? nonOilDetail.getProductID() : "") + ", productDescription: "
                            + (nonOilDetail.getProductDescription() != null ? nonOilDetail.getProductDescription() : "") + ", amount: " + nonOilDetail.getAmount() + ", credits: "
                            + nonOilDetail.getCredits() + " }";

                    message += messageTmp;
                }
            }
            else {
                message += "{ null }";
            }
        }
        else {
            message += "{ null }";
        }

        message += ", rewardDetail: ";

        if (eventNotificationResponse.getRewardTransactionDetail() != null) {
            RewardTransaction rewardDetail = eventNotificationResponse.getRewardTransactionDetail();
            message += "{ " + "parameterDetails: ";

            if (rewardDetail.getParameterDetails() != null) {
                for (RewardTransactionParameter parameterDetails : rewardDetail.getParameterDetails()) {
                    String messageTmp = "{ parameterID: " + parameterDetails.getParameterID() + ", parameterValue: " + parameterDetails.getParameterValue() + " }";

                    message += messageTmp;
                }
            }
            else {
                message += "{ null }";
            }
        }
        else {
            message += "{ null }";
        }

        message += " }";

        outputParameters.add(new Pair<String, String>("eventNotification", message));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionDetail", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return eventNotificationResponse;

    }

    @Override
    public String updatePushNotication(Long pushNotificationID, String publishMessageID, String endpoint, String title, String message, Date sendingTimestamp,
            PushNotificationSourceType source, PushNotificationContentType contentType, PushNotificationStatusType statusCode, String statusMessage, Long userID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("pushNotificationID", pushNotificationID.toString()));
        inputParameters.add(new Pair<String, String>("publishMessageID", publishMessageID != null ? publishMessageID : ""));
        inputParameters.add(new Pair<String, String>("endpoint", endpoint != null ? endpoint : ""));
        inputParameters.add(new Pair<String, String>("title", title));
        inputParameters.add(new Pair<String, String>("message", message));
        inputParameters.add(new Pair<String, String>("sendingTimestamp", sendingTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("source", source.name()));
        if (contentType != null) {
            inputParameters.add(new Pair<String, String>("contentType", contentType.name()));
        }
        else {
            inputParameters.add(new Pair<String, String>("contentType", ""));
        }
        inputParameters.add(new Pair<String, String>("statusCode", statusCode.name()));
        inputParameters.add(new Pair<String, String>("statusMessage", (statusMessage != null ? statusMessage : "")));
        inputParameters.add(new Pair<String, String>("userID", userID.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePushNotication", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;

        try {
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
            Integer expiryTime = Integer.valueOf(this.parametersService.getParamValue(PARAM_PUSH_NOTIFICATION_EXPIRY_TIME));

            response = updatePushNotificationAction.execute(pushNotificationID, publishMessageID, endpoint, source, title, message, contentType, sendingTimestamp, statusCode,
                    statusMessage, userID, expiryTime, maxRetryAttemps);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (EJBException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "updatePushNotication", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;

    }

    @Override
    public NotificationResponse retrieveNotification(String ticketId, String requestId, Long notificationId) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("notificationId", notificationId.toString()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveNotification", null, "opening", ActivityLog.createLogMessage(inputParameters));

        NotificationResponse notificationResponse = new NotificationResponse();
        notificationResponse = retrieveNotificationAction.execute(ticketId, requestId, notificationId);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("id", notificationResponse.getId() != null ? notificationResponse.getId().toString() : ""));
        outputParameters.add(new Pair<String, String>("expireDate", notificationResponse.getExpireDate() != null ? notificationResponse.getExpireDate().toString() : ""));
        outputParameters.add(new Pair<String, String>("sendDate", notificationResponse.getSendDate() != null ? notificationResponse.getSendDate().toString() : ""));
        outputParameters.add(new Pair<String, String>("statusCode", notificationResponse.getStatusCode() != null ? notificationResponse.getStatusCode() : ""));
        outputParameters.add(new Pair<String, String>("text", notificationResponse.getText() != null ? notificationResponse.getText() : ""));
        outputParameters.add(new Pair<String, String>("title", notificationResponse.getTitle() != null ? notificationResponse.getTitle() : ""));
        outputParameters.add(new Pair<String, String>("source", notificationResponse.getSource() != null ? notificationResponse.getSource().name() : ""));

        String parametersString = " {";

        if (notificationResponse.getAppLink() != null) {
            for (ParameterNotification parameterNotification : notificationResponse.getAppLink().getParameters()) {
                parametersString += "[name: " + parameterNotification.getName() + "  - value: " + parameterNotification.getValue() + "]";
            }
        }

        parametersString += " }";

        outputParameters.add(new Pair<String, String>("appLink.parameterNotification", parametersString));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveNotification", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return notificationResponse;
    }

    @Override
    public String setEventNotification(EventNotification eventNotification) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("eventType", eventNotification.getEventType().getValue()));
        inputParameters.add(new Pair<String, String>("fiscalCode", eventNotification.getFiscalCode()));
        inputParameters.add(new Pair<String, String>("srcTransactionID", eventNotification.getSrcTransactionID()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", eventNotification.getRequestTimestamp().toString()));
        inputParameters.add(new Pair<String, String>("srcTransactionID", eventNotification.getSrcTransactionID()));
        inputParameters.add(new Pair<String, String>("sessionID", eventNotification.getSessionID()));
        if (eventNotification.getCardStatus() != null) {
            inputParameters.add(new Pair<String, String>("cardStatus", eventNotification.getCardStatus()));
        }
        else {
            inputParameters.add(new Pair<String, String>("cardStatus", ""));
        }
        inputParameters.add(new Pair<String, String>("panCode", eventNotification.getPanCode()));
        inputParameters.add(new Pair<String, String>("stationID", eventNotification.getStationID()));
        inputParameters.add(new Pair<String, String>("terminalID", eventNotification.getTerminalID()));
        inputParameters.add(new Pair<String, String>("transactionResult", eventNotification.getTransactionResult()));
        if (eventNotification.getTransactionDate() != null) {
            inputParameters.add(new Pair<String, String>("transactionDate", eventNotification.getTransactionDate().toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("transactionDate", ""));
        }
        inputParameters.add(new Pair<String, String>("transactionResult", eventNotification.getTransactionResult()));
        if (eventNotification.getCredits() != null) {
            inputParameters.add(new Pair<String, String>("credits", eventNotification.getCredits().toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("credits", ""));
        }

        if (eventNotification.getMarketingMsg() != null) {
            inputParameters.add(new Pair<String, String>("marketingMsg", eventNotification.getMarketingMsg()));
        }
        else {
            inputParameters.add(new Pair<String, String>("marketingMsg", ""));
        }

        if (eventNotification.getWarningMsg() != null) {
            inputParameters.add(new Pair<String, String>("warningMsg", eventNotification.getWarningMsg()));
        }
        else {
            inputParameters.add(new Pair<String, String>("warningMsg", ""));
        }

        if (eventNotification.getWarningMsg() != null) {
            inputParameters.add(new Pair<String, String>("operationType", eventNotification.getOperationType()));
        }
        else {
            inputParameters.add(new Pair<String, String>("operationType", ""));
        }

        if (eventNotification.getWarningMsg() != null) {
            inputParameters.add(new Pair<String, String>("subtype", eventNotification.getSubtype()));
        }
        else {
            inputParameters.add(new Pair<String, String>("subtype", ""));
        }

        if (eventNotification.getLoyaltyTransactionDetail() != null) {
            LoyaltyTransaction loyaltyTransactionDetail = eventNotification.getLoyaltyTransactionDetail();
            String message = "{ " + "paymentMode: " + loyaltyTransactionDetail.getPaymentMode() + ", refuelDetail: ";

            if (loyaltyTransactionDetail.getRefuelDetails() != null) {
                message += " { ";

                for (LoyaltyTransactionRefuel refuelDetail : loyaltyTransactionDetail.getRefuelDetails()) {
                    String messageTmp = "{ " + "refuelMode: " + refuelDetail.getRefuelMode() + ", pumpNumber: "
                            + (refuelDetail.getPumpNumber() != null ? refuelDetail.getPumpNumber() : "") + ", productID: " + refuelDetail.getProductID() + ", productDescription: "
                            + (refuelDetail.getProductDescription() != null ? refuelDetail.getProductDescription() : "") + ", fuelQuantity: " + refuelDetail.getFuelQuantity()
                            + ", amount: " + refuelDetail.getAmount() + ", credits: " + refuelDetail.getCredits() + " }";

                    message += messageTmp;
                }
            }
            else {
                message += "null";
            }

            message += ", nonOilDetail: ";

            if (loyaltyTransactionDetail.getNonOilDetails() != null) {
                message += " { ";

                for (LoyaltyTransactionNonOil nonOilDetail : loyaltyTransactionDetail.getNonOilDetails()) {
                    String messageTmp = "{ " + "productID: " + (nonOilDetail.getProductID() != null ? nonOilDetail.getProductID() : "") + ", productDescription: "
                            + (nonOilDetail.getProductDescription() != null ? nonOilDetail.getProductDescription() : "") + ", amount: " + nonOilDetail.getAmount() + ", credits: "
                            + nonOilDetail.getCredits() + " }";

                    message += messageTmp;
                }
            }
            else {
                message += "null";
            }

            inputParameters.add(new Pair<String, String>("loyaltyTransactionDetail", message));
        }
        else {
            inputParameters.add(new Pair<String, String>("loyaltyTransactionDetail", "null"));
        }

        if (eventNotification.getRewardTransactionDetail() != null) {
            RewardTransaction rewardDetail = eventNotification.getRewardTransactionDetail();
            String message = "{ " + "parameterDetails: ";

            if (rewardDetail.getParameterDetails() != null) {
                for (RewardTransactionParameter parameterDetails : rewardDetail.getParameterDetails()) {
                    String messageTmp = "{ parameterID: " + parameterDetails.getParameterID() + ", parameterValue: " + parameterDetails.getParameterValue() + " }";

                    message += messageTmp;
                }
            }
            else {
                message += "null";
            }

            inputParameters.add(new Pair<String, String>("rewardDetail", message));
        }
        else {
            inputParameters.add(new Pair<String, String>("rewardDetail", "null"));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setEventNotification", null, "opening", ActivityLog.createLogMessage(inputParameters));

        String response;

        try {
            Integer maxRetryAttemps = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
            Integer expiryTime = Integer.valueOf(this.parametersService.getParamValue(PARAM_PUSH_NOTIFICATION_EXPIRY_TIME));
            Integer sendingDelay = Integer.valueOf(this.parametersService.getParamValue(PARAM_PUSH_NOTIFICATION_SENDING_DELAY));
            String voucherRewardAlertRecipient = this.parametersService.getParamValue(PARAM_VOUCHER_REWARD_ALERT_RECIPIENT);

            this.pushNotificationService = EJBHomeCache.getInstance().getPushNotificationService();
            this.emailSender = EJBHomeCache.getInstance().getEmailSender();
            response = setEventNotificationAction.execute(eventNotification, expiryTime, maxRetryAttemps, sendingDelay, voucherRewardAlertRecipient, pushNotificationService,
                    emailSender, stringSubstitution);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response = ResponseHelper.SYSTEM_ERROR;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("response", response));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setEventNotification", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public CheckTransactionResponse checkTransaction(String operationID, Long requestTimestamp, String stationID, String pumpNumber) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpNumber", pumpNumber));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkTransaction", null, "opening", ActivityLog.createLogMessage(inputParameters));

        CheckTransactionResponse response = new CheckTransactionResponse();

        try {
            Long interval = Long.valueOf(this.parametersService.getParamValue(PARAM_CHECK_PAYMENT_TRANSACTION_INTERVAL));

            response = checkTransactionAction.execute(operationID, requestTimestamp, stationID, pumpNumber, interval);
        }
        catch (ParameterNotFoundException ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response.getStatusCode()));
        if (response.getTransationDetail() != null) {
            outputParameters.add(new Pair<String, String>("stationID", response.getTransationDetail().getStationID()));
            outputParameters.add(new Pair<String, String>("pumpNumber", response.getTransationDetail().getPumpNumber()));
            outputParameters.add(new Pair<String, String>("amount", response.getTransationDetail().getAmount().toString()));
            outputParameters.add(new Pair<String, String>("fuelQuantity", response.getTransationDetail().getFuelQuantity().toString()));
            outputParameters.add(new Pair<String, String>("productCode", response.getTransationDetail().getProductCode()));
            outputParameters.add(new Pair<String, String>("authorizationCode", response.getTransationDetail().getAuthorizationCode()));
            outputParameters.add(new Pair<String, String>("bankTransactionID", response.getTransationDetail().getBankTransactionID()));
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkTransaction", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }

    @Override
    public MulticardStatusNotificationResponse multicardStatusNotification(String operationID, Long requestTimestamp, String userID, ArrayList<DpanStatus> dpanStatus) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("userID", userID));
        if (dpanStatus != null && !dpanStatus.isEmpty()) {
            for (DpanStatus item : dpanStatus) {
                inputParameters.add(new Pair<String, String>("dpanStatus - dpan:", item.getDPAN() + ", status: " + item.getStatus()));
            }
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "multicardStatusNotification", null, "opening", ActivityLog.createLogMessage(inputParameters));

        MulticardStatusNotificationResponse response = new MulticardStatusNotificationResponse();

        try {

            response = multicardStatusNotificationAction.execute(operationID, requestTimestamp, userID, dpanStatus, this.userCategoryService, this.cardInfoService);
        }

        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response.getStatusCode()));
        outputParameters.add(new Pair<String, String>("operationID", response.getOperationID()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "multicardStatusNotification", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
    
    @Override
    public MulticardStatusNotificationResponse multicardRefuelingStatusNotification(String operationID, Long requestTimestamp, String userID, String serverSerialNumber, ArrayList<DpanStatus> dpanStatus) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("userID", userID));
        inputParameters.add(new Pair<String, String>("serverSerialNumber", serverSerialNumber));
        if (dpanStatus != null && !dpanStatus.isEmpty()) {
            for (DpanStatus item : dpanStatus) {
                inputParameters.add(new Pair<String, String>("dpanStatus - dpan:", item.getDPAN() + ", status: " + item.getStatus()));
            }
        }

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "multicardRefuelingStatusNotification", null, "opening", ActivityLog.createLogMessage(inputParameters));

        MulticardStatusNotificationResponse response = new MulticardStatusNotificationResponse();

        try {

            response = multicardRefuelingStatusNotificationAction.execute(operationID, requestTimestamp, userID, serverSerialNumber, dpanStatus, this.userCategoryService, this.cardInfoService);
        }

        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", response.getStatusCode()));
        outputParameters.add(new Pair<String, String>("operationID", response.getOperationID()));

        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "multicardRefuelingStatusNotification", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return response;
    }
}
