CHANGELOG for 2.0.x
===================

This changelog references the relevant changes (bug and security fixes) done
in 2.0 minor versions.

* 2.0.17a (2016-07-29)

 * Gestione errore elettrovalvola (importo movimentato superiore all'importo preautorizzato)

* 2.0.17 (2016-07-25)

 * Ottimizzazione report giornaliero
 * Implementazione servizi di backoffice per archiviazione transazioni
 * Fix procedura di storno manuale per transazione pagata parzialmente o interamente con voucher

* 2.0.16a (2016-07-20)

 * Fix nome allegato email scontrino (enipay al posto di eni_pay)
 
* 2.0.16 (2016-07-18)

 * Fix eccezione eventi asincroni start e end refuel
 * Fix bug per impianto non funzionante in caso di stazione non trovata in prossimitą dell'utente
 * Creazione servizi di backoffice per aggiornamento promozioni
 * Aggiornamento servizio di backoffice update utente per campo userType
 * Ottimizzazione job report giornaliero

* 2.0.15a (2016-07-04)

 * Fix bug chiamata doppia alla callSettle del gestpay service

* 2.0.15 (2016-06-23)

 * Fix procedura di storno caricamento punti in caso di storno manuale di una transazione shop con rifornimenti multipli
 * Fix bug su invio notifica postpaid in caso di funziona di caricamento con esito positivo ma 0 punti caricati
 * Fix visualizzazione scontrino da storico in caso di transazione shop con rifornimento multiplo
 * Correzione messaggi per transazioni postpaid stornate

* 2.0.14 (2016-06-09)

 * Revisione flusso postpaid e relative procedure di riconciliazione
 * Centralizzazione gestione proxy

* 2.0.13 (2016-05-19)

 * Creazione job per cancellazione metodi di pagamento non verificati entro 60 giorni dalla creazione
 * Creazione job per cancellazione transazioni post paid in stato ONHOLD
 * Esclusione dalla risposta della getStationDetails degli erogatori con stato non valido
 * Correzione bug per invio valutazione per transazione inesistente
 
* 2.0.12a (2016-05-13)

 * Creazione servizio per popolamento tabella CardBin

* 2.0.12 (2016-05-06)

 * Inserimento voce "Utenti che hanno effettuato la loro prima transazione" sul report giornaliero
 * Creazione servizio di BO testRetrieveStation
 * Spostamento deleteToken da GSService a GPService

* 2.0.11 (2016-05-03)

 * Questionario di gradimento

* 2.0.10 (2016-04-27)

 * Invio pdf allegato a email scontrino
 * Centralizzazione impostazione proxy su adapter
 * Aggiornamento report giornaliero transazioni
 * Gestione status code PUMP_NOT_AVAILABLE_501
 * Bugfix minori

* 2.0.9b (2016-04-06)

 * Gestione voucher promozionali
 * Creazione report statistiche giornaliero
 * Soluzione bug in gestione utenti in attesa di voucher promozionali
 * Gestione della transazione in elaborazione nei servizi post paid
 * Modifica nella mappatura degli stati delle transazione nei report

* 2.0.9a (2016-03-11)

 * Flag utilizzo voucher attivato di default
 * Possibilitą di gestire documenti pdf accessibili via url senza che siano visibili nell'ultimo step di iscrizione
 * Bugfix minori

* 2.0.9 (2016-03-08)

 * Redirect su pagina esito attivazione su sito web
 * setNotificationUser a true in caso di errore nella preautorizzazione del pagamento di una transazione post-paid

* 2.0.8 (2016-03-03)

 * Implementazione voucher promozionali: promozione di benvenuto e promozione mastercard
 * Gestione dinamica wsdl servizi di pagamento
 * Inserimento importo voucher consumato in report transazioni prepaid
 * Formattazione importi e quantitą in report transazioni prepaid
 * Visualizzazione id e numero cassa su scontrino transazioni shop
 * Creazione parametri landingHomeTitle e landingRegistrationTitle
 * Visualizzazione totale importo shop al posto delle singole voci
 * Visualizzazione messaggi di marketing dei voucher nella sezione promo dello scontrino
 * Bugfix minori

* 2.0.7 (2016-02-16)

 * Utilizzo voucher in rifornimenti pre-paid
 * Adeguamento flusso di riconciliazione transazioni pre-paid per consumo voucher
 * Modifica template email per nuova grafica
 * Eliminazione annotation per gestione concorrenza processi schedulati
 * Bugfix minori

* 2.0.6 (2016-02-15)

 * Servizi mappa dinamica; gestione errori per cassa e/o erogatore inesistente

* 2.0.5 (2016-02-12)

 * Creazione nuovi parametri su SystemParameters per gestione id app
 * Creazione json per visualizzazione tutorial app
 * Correzione bug per messaggio errato accesso a pagina voucher con voucher cancellati
 * Possibilią di riassegnare un voucher precedentemente cancellato

* 2.0.4 (2016-02-05)

 * Correzione bug invio email automatiche con cc e bcc

* 2.0.3 (2016-02-04)

 * Correzione bug servizio retrieveStationList per inversione campi prepaid/postpaid

* 2.0.2 (2016-02-03)

 * Attivazione flag per recupero dati reali impianto in getStation
 * Lettura BIN carta rossa da database
 * Servizio che restituisce la mappa delle stazioni
 * Filtro funzionalitą PV
 * Modifica gestione dei messaggi restituiti all'app nella schermata finale della transazione, nello scontrino e nell'email
 * Bugfix vari
 
* 2.0.1 (2016-01-27)

 * Verifica esistenza transazione postpaid con stesso srcTransactionId prima di crearne una nuova
 * Controllo codice fiscale non bloccante per utenti in stato new

* 2.0.0 (2016-01-26)

 * Start changelog versioning
