CHANGELOG for 2.1.x
===================

This changelog references the relevant changes (bug and security fixes) done
in 2.1 minor versions.

* 2.1.0 (2016-01-26)

 * Refuelling implementation with Enjoy integration
