package com.techedge.mp.pushnotification.adapter.business;

import java.util.List;

import javax.ejb.Local;

import com.techedge.mp.pushnotification.adapter.interfaces.Platform;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;

@Local
public interface PushNotificationServiceLocal {

    public PushNotificationResult createEndpoint(String deviceToken, String deviceData, Platform platform);
    
    public PushNotificationResult removeEndpoint(String arnEndpoint);

    public PushNotificationResult publishMessage(String arnEndpoint, PushNotificationMessage notificationMessage, boolean publishToTopic, boolean showJSON);
    
    public PushNotificationResult createTopic(String topicName);
    
    public PushNotificationResult subscribeToTopic(String arnTopic, List<String> arnEndpointList, boolean clearAllSubscription);
    
    public PushNotificationResult subscribeToTopic(String arnTopic, String arnEndpoint);
    
    public boolean topicIsEmpty(String arnTopic);
    
}

