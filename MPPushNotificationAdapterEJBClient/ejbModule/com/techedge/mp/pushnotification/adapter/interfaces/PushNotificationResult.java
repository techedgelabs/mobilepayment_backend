package com.techedge.mp.pushnotification.adapter.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PushNotificationResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7028080830397741945L;
    private String            arnEndpoint;
    private String            messageId;
    private String            statusCode;
    private Integer           httpStatusCode;
    private String            errorCode;
    private String            errorType;
    private String            message;
    private String            requestId;
    private Date              requestTimestamp;
    private List<String>      ErrorSubscriptionsList = new ArrayList<String>();  

    public String getArnEndpoint() {
        return arnEndpoint;
    }

    public void setArnEndpoint(String arnEndpoint) {
        this.arnEndpoint = arnEndpoint;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(Integer httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }
    
    public List<String> getErrorSubscriptionsList() {
        return ErrorSubscriptionsList;
    }
    
    public void setErrorSubscriptionsList(List<String> errorSubscriptionsList) {
        ErrorSubscriptionsList = errorSubscriptionsList;
    }
}
