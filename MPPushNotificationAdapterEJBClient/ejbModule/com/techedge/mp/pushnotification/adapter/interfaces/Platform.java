package com.techedge.mp.pushnotification.adapter.interfaces;

public enum Platform {
    // Apple Push Notification Service
    APNS,
    // Sandbox version of Apple Push Notification Service
    APNS_SANDBOX,
    // Amazon Device Messaging
    ADM,
    // Google Cloud Messaging
    GCM,
    // Windows Phone 7+
    MPNS,
    //Windows Phone 8+ and Windows Phone 8.1+
    WNS

}
