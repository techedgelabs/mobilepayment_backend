package com.techedge.mp.pushnotification.adapter.interfaces;

import java.io.Serializable;

public class PushNotificationMessage implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3918150157557368174L;
    private Message notification;
    
    public Message getMessage() {
        return notification;
    }

    public void setMessage(Message notification) {
        this.notification = notification;
    }

    public void setMessage(Long id, String text, String title) {
        this.notification = new Message();
        this.notification.setId(id);
        this.notification.setText(text);
        this.notification.setTitle(title);
        
    }
    
    @Override
    public String toString() {
        return "id=" + notification.getId() + "@title=" + notification.getTitle() + "@text=" + notification.getText();
    }
    
    public class Message implements Serializable {

        /**
         * 
         */
        private static final long serialVersionUID = -6892188313639218382L;
        String text;
        Long   id;
        String title;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
        
        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }
}
