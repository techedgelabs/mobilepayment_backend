package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;

public class EnablePumpResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3056711919329845594L;
	
	private String statusCode;
	private String messageCode;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
}
