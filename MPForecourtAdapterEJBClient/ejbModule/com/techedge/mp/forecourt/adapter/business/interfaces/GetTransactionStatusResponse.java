package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;

public class GetTransactionStatusResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1145361571663347080L;
	
	private String statusCode;
	private String messageCode;
	private RefuelDetail refuelDetail;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public RefuelDetail getRefuelDetail() {
		return refuelDetail;
	}
	public void setRefuelDetail(RefuelDetail refuelDetail) {
		this.refuelDetail = refuelDetail;
	}
}
