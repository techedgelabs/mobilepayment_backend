package com.techedge.mp.forecourt.adapter.business;

import java.util.List;

import javax.ejb.Remote;

import com.techedge.mp.forecourt.adapter.business.interfaces.EnablePumpResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetPumpStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetTransactionStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.PaymentAuthorizationResult;
import com.techedge.mp.forecourt.adapter.business.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendPaymentTransactionResultResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendStationListPaymentResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.StationPayment;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionReconciliationResponse;

@Remote
public interface ForecourtInfoServiceRemote {

    public GetPumpStatusResponse getPumpStatus(String requestID, String stationID, String pumpID, String transactionID);

    public GetStationDetailsResponse getStationDetails(String requestID, String stationID, String pumpID, Boolean pumpDetailsReq);

    public EnablePumpResponse enablePump(String requestID, String transactionID, String stationID, String pumpID, String paymentMode, String productID, Double amount,
            PaymentAuthorizationResult paymentAuthorizationResult);

    public GetTransactionStatusResponse getTransactionStatus(String requestID, String transactionID);

    public TransactionReconciliationResponse transactionReconciliation(String requestID, List<String> transactionIDList);

    public SendPaymentTransactionResultResponse sendPaymentTransactionResult(String requestID, String transactionID, Double amount,
            PaymentTransactionResult paymentTransactionResult);
    
    public SendStationListPaymentResponse sendStationListPayment(String requestID, List<StationPayment> stationPaymentList);
    
}
