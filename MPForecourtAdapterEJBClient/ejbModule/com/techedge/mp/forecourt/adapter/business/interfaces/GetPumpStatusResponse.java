package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;

public class GetPumpStatusResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3906193112370304754L;
	
	private String statusCode;
	private String messageCode;
	private String refuelMode;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getRefuelMode() {
		return refuelMode;
	}
	public void setRefuelMode(String refuelMode) {
		this.refuelMode = refuelMode;
	}
}
