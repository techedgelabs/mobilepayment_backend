package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TransactionReconciliationResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 861857875764980559L;
	
	private List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>(0);

	public List<TransactionDetail> getTransactionDetails() {
		return transactionDetails;
	}

	public void setTransactionDetails(List<TransactionDetail> transactionDetails) {
		this.transactionDetails = transactionDetails;
	}
}
