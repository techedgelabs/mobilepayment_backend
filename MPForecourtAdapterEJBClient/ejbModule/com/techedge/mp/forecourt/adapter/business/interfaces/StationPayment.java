
package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;


public class StationPayment implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6741804192652189248L;
    private String stationID;
    private boolean payment;

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String value) {
        this.stationID = value;
    }

    public boolean getPayment() {
        return payment;
    }

    public void setPayment(boolean value) {
        this.payment = value;
    }
}
