package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TransactionDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1327303050655678366L;
	
	private String statusCode;
	private String messageCode;
	private String transactionID;
	private String timestamp;
	private String lastTransactionStatus;
	private List<TransactionStatus> transactionStatusHistory = new ArrayList<TransactionStatus>(0);
	private RefuelDetail refuelDetail;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getLastTransactionStatus() {
		return lastTransactionStatus;
	}
	public void setLastTransactionStatus(String lastTransactionStatus) {
		this.lastTransactionStatus = lastTransactionStatus;
	}
	
	public List<TransactionStatus> getTransactionStatusHistory() {
		return transactionStatusHistory;
	}
	public void setTransactionStatusHistory(
			List<TransactionStatus> transactionStatusHistory) {
		this.transactionStatusHistory = transactionStatusHistory;
	}
	
	public RefuelDetail getRefuelDetail() {
		return refuelDetail;
	}
	public void setRefuelDetail(RefuelDetail refuelDetail) {
		this.refuelDetail = refuelDetail;
	}
}
