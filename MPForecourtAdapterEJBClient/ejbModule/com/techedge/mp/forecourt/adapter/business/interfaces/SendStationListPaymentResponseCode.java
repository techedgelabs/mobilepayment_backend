package com.techedge.mp.forecourt.adapter.business.interfaces;

public enum SendStationListPaymentResponseCode {
    MESSAGE_RECEIVED_200,
    STATION_NOT_FOUND_404,
    SYSTEM_ERROR_500;

    public String value() {
        return name();
    }

    public static SendStationListPaymentResponseCode fromValue(String v) {
        return valueOf(v);
    }
    
}
