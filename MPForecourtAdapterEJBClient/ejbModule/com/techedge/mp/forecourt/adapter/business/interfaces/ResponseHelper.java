package com.techedge.mp.forecourt.adapter.business.interfaces;

public class ResponseHelper {
	
	public final static String SYSTEM_ERROR = "SYSTEM_ERROR_500";
	
	public final static String GET_PUMP_STATUS_PUMP_NOT_FOUND         = "PUMP_NOT_FOUND_404";
	public final static String GET_PUMP_STATUS_PUMP_NOT_AVAILABLE_501 = "PUMP_NOT_AVAILABLE_501";
	
	public final static String GET_STATION_DETAILS_PARAMETER_NOT_FOUND = "PARAMETER_NOT_FOUND_400";
	
	public final static String ENABLE_PUMP_PARAMETER_NOT_FOUND = "PARAMETER_NOT_FOUND_400";
	
	public final static String GET_TRANSACTION_STATUS_PARAMETER_NOT_FOUND = "PARAMETER_NOT_FOUND_400";
	
	public final static String TRANSACTION_RECONCILIATION_MESSAGE_RECEIVED = "MESSAGE_RECEIVED_200";
	
	public final static String SEND_PAYMENT_TRANSACTION_RESULT_MESSAGE_RECEIVED           = "MESSAGE_RECEIVED_200";
	public final static String SEND_PAYMENT_TRANSACTION_RESULT_PARAMETER_NOT_FOUND        = "PARAMETER_NOT_FOUND_400";
	public final static String SEND_PAYMENT_TRANSACTION_RESULT_TRANSACTION_NOT_RECOGNIZED = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String SEND_STATION_LIST_MESSAGE_RECEIVED           = "MESSAGE_RECEIVED_200";
}