package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;

public class TransactionStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2186323878194170645L;
	
	private String timestamp;
	private String statusCode;
	private String messageCode;
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
}
