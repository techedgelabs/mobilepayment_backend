package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;

public class RefuelDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2130977492835831456L;
	
	private String timestampEndRefuel;
	private Double amount;
	private String fuelType;
	private Double fuelQuantity;
	private String productID;
	private String productDescription;
	private Double unitPrice;
	
	public String getTimestampEndRefuel() {
		return timestampEndRefuel;
	}
	public void setTimestampEndRefuel(String timestampEndRefuel) {
		this.timestampEndRefuel = timestampEndRefuel;
	}
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	
	public Double getFuelQuantity() {
		return fuelQuantity;
	}
	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}
	
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	
    public Double getUnitPrice() {
        return unitPrice;
    }
    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }
}
