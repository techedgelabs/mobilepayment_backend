package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;

public class SourceDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8111455679372485814L;
	
	private String sourceID;
	private String sourceNumber;
	
	public String getSourceID() {
		return sourceID;
	}
	public void setSourceID(String sourceID) {
		this.sourceID = sourceID;
	}
	
	public String getSourceNumber() {
		return sourceNumber;
	}
	public void setSourceNumber(String sourceNumber) {
		this.sourceNumber = sourceNumber;
	}
}
