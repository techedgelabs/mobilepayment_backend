package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StationDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6277470855246835786L;
	
	private String stationID;
	private String validityDateDetails;
	private String address;
	private String city;
	private String country;
	private String province;
	private String latitude;
	private String longitude;
	private List<PumpDetail> pumpDetails = new ArrayList<PumpDetail>(0);
	private List<SourceDetail> sourceDetails = new ArrayList<SourceDetail>(0);
	
	public String getStationID() {
		return stationID;
	}
	public void setStationID(String stationID) {
		this.stationID = stationID;
	}
	
	public String getValidityDateDetails() {
		return validityDateDetails;
	}
	public void setValidityDateDetails(String validityDateDetails) {
		this.validityDateDetails = validityDateDetails;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public List<PumpDetail> getPumpDetails() {
		return pumpDetails;
	}
	public void setPumpDetails(List<PumpDetail> pumpDetails) {
		this.pumpDetails = pumpDetails;
	}
	
	public List<SourceDetail> getSourceDetails() {
		return sourceDetails;
	}
	public void setSourceDetails(List<SourceDetail> sourceDetails) {
		this.sourceDetails = sourceDetails;
	}
}
