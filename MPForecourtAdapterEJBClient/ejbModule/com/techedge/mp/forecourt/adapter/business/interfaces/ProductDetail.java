package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;

public class ProductDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8958406253755697724L;
	
	private String productID;
	private String productDescription;
	private String fuelType;
	private Double productPrice;
	
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	
	public Double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}
}
