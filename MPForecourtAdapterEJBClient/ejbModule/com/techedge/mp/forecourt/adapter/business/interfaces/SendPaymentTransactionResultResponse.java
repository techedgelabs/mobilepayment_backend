package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;

public class SendPaymentTransactionResultResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4392733759902737981L;
	
	private String statusCode;
	private String messageCode;
	private String electronicInvoiceID;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getElectronicInvoiceID() {
        return electronicInvoiceID;
    }
	
	public void setElectronicInvoiceID(String electronicInvoiceID) {
        this.electronicInvoiceID = electronicInvoiceID;
    }
}
