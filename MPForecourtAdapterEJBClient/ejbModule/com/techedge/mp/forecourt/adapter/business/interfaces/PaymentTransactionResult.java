package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;

public class PaymentTransactionResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8201291893588138952L;
	
	private String eventType;
	private String transactionResult;
	private String shopTransactionID;
	private String bankTransactionID;
	private String authorizationCode;
	private String errorCode;
	private String errorDescription;
	
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	public String getTransactionResult() {
		return transactionResult;
	}
	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}
	
	public String getShopTransactionID() {
		return shopTransactionID;
	}
	public void setShopTransactionID(String shopTransactionID) {
		this.shopTransactionID = shopTransactionID;
	}
	
	public String getBankTransactionID() {
		return bankTransactionID;
	}
	public void setBankTransactionID(String bankTransactionID) {
		this.bankTransactionID = bankTransactionID;
	}
	
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
}
