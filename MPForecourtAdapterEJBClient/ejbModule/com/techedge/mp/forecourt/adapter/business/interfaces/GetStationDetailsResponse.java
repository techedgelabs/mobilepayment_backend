package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;

public class GetStationDetailsResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2645349211393335128L;
	
	private String statusCode;
	private String messageCode;
	private StationDetail stationDetail;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public StationDetail getStationDetail() {
		return stationDetail;
	}
	public void setStationDetail(StationDetail stationDetail) {
		this.stationDetail = stationDetail;
	}
}
