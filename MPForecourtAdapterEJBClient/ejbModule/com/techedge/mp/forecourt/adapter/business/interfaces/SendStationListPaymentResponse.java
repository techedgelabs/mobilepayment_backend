package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;

public class SendStationListPaymentResponse implements Serializable {

    /**
     * 
     */
    private static final long                  serialVersionUID = 125189955711293326L;
    private SendStationListPaymentResponseCode statusCode;
    private String                             messageCode;
    private ArrayList<String>                  stationNotFound = new ArrayList<String>();

    public SendStationListPaymentResponseCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = SendStationListPaymentResponseCode.fromValue(statusCode);
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }
    
    public ArrayList<String> getStationNotFound() {
        return stationNotFound;
    }
    
    public void setStationNotFound(ArrayList<String> stationNotFound) {
        this.stationNotFound = stationNotFound;
    }

}
