package com.techedge.mp.forecourt.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PumpDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5875233270971957596L;
	
	private String pumpID;
	private String pumpNumber;
	private String pumpStatus;
	private String refuelMode;
	private List<ProductDetail> productDetails = new ArrayList<ProductDetail>(0);
	
	public String getPumpID() {
		return pumpID;
	}
	public void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}
	
	public String getPumpNumber() {
		return pumpNumber;
	}
	public void setPumpNumber(String pumpNumber) {
		this.pumpNumber = pumpNumber;
	}
	
	public String getPumpStatus() {
		return pumpStatus;
	}
	public void setPumpStatus(String pumpStatus) {
		this.pumpStatus = pumpStatus;
	}
	
	public String getRefuelMode() {
		return refuelMode;
	}
	public void setRefuelMode(String refuelMode) {
		this.refuelMode = refuelMode;
	}
	
	public List<ProductDetail> getProductDetails() {
		return productDetails;
	}
	public void setProductDetails(List<ProductDetail> productDetails) {
		this.productDetails = productDetails;
	}
}
