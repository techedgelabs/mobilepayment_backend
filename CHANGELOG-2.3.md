CHANGELOG for 2.3.x
===================

This changelog references the relevant changes (bug and security fixes) done
in 2.3 minor versions.

* 2.3.14 (2017-03-26)
 
 * Verifica automatica email in iscrizione

* 2.3.13 (2017-02-22)
 
 * Implementazione controllo di prossimità sulla procedura di caricamento punti da POS
 * Modifica mapping descrizione prodotti su flusso di notifica al CRM per le operazioni di caricamento punti da POS

* 2.3.12 (2017-02-15)
 
 * Implementazione servizio per visualizzazione popup promozionale sulla homepage dell'app

* 2.3.11 (2017-01-18)
 
 * Modifica chiamate servizi CRM e DWH con gestione tramite thread asincroni
 * Implementazione codici di errore ai servizi CheckPayment e EventNotification di Quenit

* 2.3.10 (2017-12-08)
 
 * Modifica alla struttura dati ed al layout del file excel del report statistiche giornaliero
 * Bugfix vari

* 2.3.9 (2017-12-12)
 
 * Modifica alla struttura dati ed al layout del file excel del report statistiche giornaliero
 * Bugfix di risposta vuota alle richieste sul servizio caricamenti punti da pos/nfc

* 2.3.8 (2017-12-09)
 
 * Gestione servizio di restituizione documenti da profilo utente
 * Bugfix vari

* 2.3.7 (2017-11-29)
 
 * Implementazione servizio per generazione voucher promozionali per eventi e fiere
 * Memorizzazione log servizio checkPayment anche per PV non esistenti o non attivi
 * Differenziazione icone voucher i base alla promozione
 * Aggiornamento template email
 * Bugfix minori

* 2.3.6 (2017-11-22)
 
 * Estensione servizio di checkPayment per gestire l'errore per PV non attivo; memorizzazione log esito chiamata su db
 * Fix controllo su numero massimo device associabili per utente; invalidazione ticket dopo cancellazione manuale/automatica device
 * Implementazione flag per attivazione/disattivazione servizi su PV (newAcquirer, refueling, loyalty)
 * Adeguamento servizio di backoffice per il caricamento dei PV
 * Controllo validità transazione postpaid restituita dal servizio getLastRefuel; attivazione controllo parametricamente e salvataggio esito su db
 * Eliminazione riferimenti a codice erogatore negli allegati delle email e negli scontrini
 * Bugfix vari sulle procedure di riconciliazione
 
* 2.3.5 (2017-10-26)
 
 * Associazione utente a transazione postpaid al momento della generazione e chiusura automatica transazione al momento del passaggio al pagamento in cassa
 * Gestione utente Eni Pay e Eni Station che accede ad Enistation+ con credenziali Enistation; restituzione stesso messaggio per utente Eni Station + 
 * Differenziazione testi sms per verifica device

* 2.3.4 (2017-10-24)
 
 * Differenziazione landing page migrazione da Eni Pay per utenti vecchio flusso (senza numero di telefono)
 * Modifica pagina per accettazione termini e condizioni (flag facoltativi accettati automaticamente)
 * Gestione esautimento tentativi inserimento pin
 * Fix servizio di modifica pin per ricavare automaticamente le informazioni relative al pin dell'utente
 * Bugfix vari su backend e backoffice

* 2.3.3 (2017-10-21)
 
 * Gestione brand carta di credito diversi da Visa e MasterCart (Maestro e VPay)
 * Modifica lunghezza parametro message tabella PAYMENTINFO per gestire messaggi di errori lunghi fino a 255 caratteri

* 2.3.2 (2017-10-18)

 * Modifica flusso di iscrizione per rendere facoltativa la virtualizzazione della carta; creazione del servizio skipVirtualization per eseguire lo skip della virtualizzazione
 * Creazione servizio per esecuzione riconciliazione transazione singola di tipo user
 * Ripristino del vecchio funzionamento del servizio getSourceDetail per le transazioni postpaid (eliminazione servizio createPopTransaction)
 * Gestione dei termsAndConditions per categorie; possibilità di restituire gli elementi appartenenti solo a una determinata categoria
 * Estensione della risposta del servizio retrievePopTransaction per resitutire anche la modalità operativa degli erogatori
 * Rilassamento del vincolo di obbligatorietà nella getSourceDetail per consentire l'esecuzione anche ad utenti che non hanno una carta ma che hanno almeno un voucher

* 2.3.1 (2017-09-18)

 * Bugfix vari
 
 * 2.3.0 (2017-09-06)

 * Nuove funzionalità di backend con integrazione con CRM, sistemi Serijakala e push notification
 * Nuovo acquirer CartaSì
