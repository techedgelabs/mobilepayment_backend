package com.techedge.mp.pushnotification.adapter.interfaces;

import java.util.HashMap;

public class PlatformApplications {

    private HashMap<Platform, String> applications = new HashMap<Platform, String>();
    
    public void addApplication(Platform platform, String arnApplication) {
        applications.put(platform, arnApplication);
    }
    
    public String getArnApplication(Platform platform) {
        return applications.get(platform);
    }
}
