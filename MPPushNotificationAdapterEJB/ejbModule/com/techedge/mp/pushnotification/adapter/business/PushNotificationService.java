package com.techedge.mp.pushnotification.adapter.business;

import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.http.conn.ConnectTimeoutException;
import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.PredefinedClientConfigurations;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.AmazonSNSException;
import com.amazonaws.services.sns.model.AuthorizationErrorException;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.DeleteEndpointRequest;
import com.amazonaws.services.sns.model.DeleteEndpointResult;
import com.amazonaws.services.sns.model.Endpoint;
import com.amazonaws.services.sns.model.GetEndpointAttributesRequest;
import com.amazonaws.services.sns.model.GetEndpointAttributesResult;
import com.amazonaws.services.sns.model.InternalErrorException;
import com.amazonaws.services.sns.model.InvalidParameterException;
import com.amazonaws.services.sns.model.ListEndpointsByPlatformApplicationRequest;
import com.amazonaws.services.sns.model.ListEndpointsByPlatformApplicationResult;
import com.amazonaws.services.sns.model.ListSubscriptionsByTopicRequest;
import com.amazonaws.services.sns.model.ListSubscriptionsByTopicResult;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.NotFoundException;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.SetEndpointAttributesRequest;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.Subscription;
import com.amazonaws.services.sns.model.SubscriptionLimitExceededException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.pushnotification.adapter.interfaces.Platform;
import com.techedge.mp.pushnotification.adapter.interfaces.PlatformApplications;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.pushnotification.adapter.interfaces.StatusCode;

/**
 * Session Bean implementation class SmsService
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class PushNotificationService implements PushNotificationServiceRemote, PushNotificationServiceLocal {

    private final static String     PARAM_PROXY_HOST                               = "PROXY_HOST";
    private final static String     PARAM_PROXY_PORT                               = "PROXY_PORT";
    private final static String     PARAM_PROXY_NO_HOSTS                           = "PROXY_NO_HOSTS";
    private final static String     PARAM_AWS_SNS_ARN_APPLICATION_GCM              = "AWS_SNS_ARN_APPLICATION_GCM";
    private final static String     PARAM_AWS_SNS_ARN_APPLICATION_APNS             = "AWS_SNS_ARN_APPLICATION_APNS";
    private final static String     PARAM_AWS_SNS_ARN_APPLICATION_WNS              = "AWS_SNS_ARN_APPLICATION_WNS";
    private final static String     PARAM_AWS_SNS_ARN_APPLICATION_MPNS             = "AWS_SNS_ARN_APPLICATION_MPNS";
    private final static String     PARAM_AWS_SNS_CLIENT_REGION                    = "AWS_SNS_CLIENT_REGION";
    private final static String     PARAM_AWS_SNS_CLIENT_CONNECTION_TIMEOUT        = "AWS_SNS_CLIENT_CONNECTION_TIMEOUT";

    private final static String     PARAM_AWS_SNS_VAULT_BLOCK                      = "AWS_SNS_VAULT_BLOCK";
    private final static String     PARAM_AWS_SNS_VAULT_BLOCK_ATTRIBUTE_ACCESS_KEY = "AWS_SNS_VAULT_BLOCK_ATTRIBUTE_ACCESS_KEY";
    private final static String     PARAM_AWS_SNS_VAULT_BLOCK_ATTRIBUTE_SECRET_KEY = "AWS_SNS_VAULT_BLOCK_ATTRIBUTE_SECRET_KEY";

    private ParametersServiceRemote parametersService                              = null;
    private LoggerServiceRemote     loggerService                                  = null;

    // Valori di default
    private String                  proxyHost                                      = "mpsquid.enimp.pri";
    private String                  proxyPort                                      = "3128";
    private String                  proxyNoHosts                                   = "localhost|127.0.0.1|*.enimp.pri";

    private String                  accessKey                                      = null;                                      //"AKIAJRKC5RFJ7C7SDEEQ";
    private String                  secretKey                                      = null;                                      //"cKuyy8iZzlQNtCnEIk002b0PyqqDIHopBYKRiJ0H";
    private Regions                 clientRegion                                   = Regions.DEFAULT_REGION;
    private PlatformApplications    platformApplications                           = new PlatformApplications();

    private int                     connectionTimeout                              = -1;

    /**
     * Default constructor.
     */
    public PushNotificationService() {
        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Errore nella inizializzazione del servizio Parameters: " + e.getMessage());
        }

        //String wsdlString = "";
        String vaultBlock = "";
        String vaultBlockAttributeAccekey = "";
        String vaultBlockAttributeSecretKey = "";

        try {

            this.proxyHost = parametersService.getParamValue(PushNotificationService.PARAM_PROXY_HOST);
            this.proxyPort = parametersService.getParamValue(PushNotificationService.PARAM_PROXY_PORT);
            this.proxyNoHosts = parametersService.getParamValue(PushNotificationService.PARAM_PROXY_NO_HOSTS);
            vaultBlock = parametersService.getParamValue(PushNotificationService.PARAM_AWS_SNS_VAULT_BLOCK);
            vaultBlockAttributeAccekey = parametersService.getParamValue(PushNotificationService.PARAM_AWS_SNS_VAULT_BLOCK_ATTRIBUTE_ACCESS_KEY);
            vaultBlockAttributeSecretKey = parametersService.getParamValue(PushNotificationService.PARAM_AWS_SNS_VAULT_BLOCK_ATTRIBUTE_SECRET_KEY);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Errore nella inizializzazione dei parametri: " + e.getMessage());
        }

        try {
            String region = parametersService.getParamValue(PushNotificationService.PARAM_AWS_SNS_CLIENT_REGION);

            if (region != null && !region.isEmpty()) {
                this.clientRegion = Regions.fromName(region);
            }

            String arnApplicationGCM = parametersService.getParamValue(PushNotificationService.PARAM_AWS_SNS_ARN_APPLICATION_GCM);

            if (arnApplicationGCM != null && !arnApplicationGCM.isEmpty()) {
                platformApplications.addApplication(Platform.GCM, arnApplicationGCM);
            }

            String arnApplicationAPNS = parametersService.getParamValue(PushNotificationService.PARAM_AWS_SNS_ARN_APPLICATION_APNS);

            if (arnApplicationAPNS != null && !arnApplicationAPNS.isEmpty()) {
                platformApplications.addApplication(Platform.APNS, arnApplicationAPNS);
            }

            String arnApplicationMPNS = parametersService.getParamValue(PushNotificationService.PARAM_AWS_SNS_ARN_APPLICATION_MPNS);

            if (arnApplicationMPNS != null && !arnApplicationMPNS.isEmpty()) {
                platformApplications.addApplication(Platform.MPNS, arnApplicationMPNS);
            }

            String arnApplicationWNS = parametersService.getParamValue(PushNotificationService.PARAM_AWS_SNS_ARN_APPLICATION_WNS);

            if (arnApplicationWNS != null && !arnApplicationWNS.isEmpty()) {
                platformApplications.addApplication(Platform.WNS, arnApplicationWNS);
            }

            if (parametersService.getParamValue(PushNotificationService.PARAM_AWS_SNS_CLIENT_CONNECTION_TIMEOUT) != null) {
                connectionTimeout = Integer.parseInt(parametersService.getParamValue(PushNotificationService.PARAM_AWS_SNS_CLIENT_CONNECTION_TIMEOUT));
            }
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Errore nella inizializzazione dei parametri: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.accessKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeAccekey, new byte[] { 1 })).trim();
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeSecretKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeAccekey + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 2 NAME: '" + vaultBlockAttributeSecretKey + "'");

        System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.accessKey + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 2 VALUE: '" + this.secretKey + "'");
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {
        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    @Override
    public PushNotificationResult createEndpoint(String deviceToken, String deviceData, Platform platform) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("deviceToken", (deviceToken != null ? deviceToken : "")));
        inputParameters.add(new Pair<String, String>("deviceData", (deviceData != null ? deviceData : "")));
        inputParameters.add(new Pair<String, String>("platform", (platform != null ? platform.name() : "")));

        log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "createEndpoint", "opening", ActivityLog.createLogMessage(inputParameters));

        //System.out.println("Invoking AWS SNS service...");

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        //System.setProperty("javax.net.debug", "ssl");
        proxy.setHttp();

        String arnEndpoint = null;
        boolean updateNeeded = false;
        boolean createNeeded = true;
        PushNotificationResult pushNotificationResult = new PushNotificationResult();
        pushNotificationResult.setRequestTimestamp(new Date());

        try {
            AmazonSNS amazonSNS = createSNSClient();

            Endpoint endpoint = getPlatformEndpoint(amazonSNS, deviceToken, platformApplications.getArnApplication(platform));

            if (endpoint != null) {
                arnEndpoint = endpoint.getEndpointArn();
                createNeeded = false;
            }

            //System.out.println("Endpoint createNeeded = " + createNeeded);

            if (createNeeded) {
                // No platform endpoint ARN is stored; need to call createEndpoint.
                CreatePlatformEndpointResult createEndpointResult = createPlatformEndpoint(amazonSNS, deviceToken, deviceData, platformApplications.getArnApplication(platform));
                arnEndpoint = createEndpointResult.getEndpointArn();
                createNeeded = false;
            }

            //System.out.println("Retrieving platform endpoint data...");
            // Look up the platform endpoint and make sure the data in it is current, even if
            // it was just created.
            try {
                GetEndpointAttributesRequest geaReq = new GetEndpointAttributesRequest().withEndpointArn(arnEndpoint);
                GetEndpointAttributesResult geaRes = amazonSNS.getEndpointAttributes(geaReq);

                updateNeeded = !geaRes.getAttributes().get("Token").equals(deviceToken) || !geaRes.getAttributes().get("Enabled").equalsIgnoreCase("true");

            }
            catch (NotFoundException nfe) {
                // We had a stored ARN, but the platform endpoint associated with it
                // disappeared. Recreate it.
                createNeeded = true;
            }

            //System.out.println("Endpoint updateNeeded = " + updateNeeded);

            if (updateNeeded) {
                // The platform endpoint is out of sync with the current data;
                // update the token and enable it.
                System.out.println("Updating platform endpoint " + arnEndpoint);
                Map<String, String> attribs = new HashMap<String, String>();
                attribs.put("Token", deviceToken);
                attribs.put("Enabled", "true");
                SetEndpointAttributesRequest saeReq = new SetEndpointAttributesRequest().withEndpointArn(arnEndpoint).withAttributes(attribs);
                amazonSNS.setEndpointAttributes(saeReq);
            }

            pushNotificationResult.setHttpStatusCode(200);
            pushNotificationResult.setArnEndpoint(arnEndpoint);
            pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_CREATE_ENDPOINT_SUCCESS);
        }
        catch (AmazonSNSException ex) {
            System.err.println("CreateEndpoint ERROR");
            System.err.println("   |-> Device Token:     " + deviceToken);
            System.err.println("   |-> Error Message:    " + ex.getMessage());
            System.err.println("   |-> HTTP Status Code: " + ex.getStatusCode());
            System.err.println("   |-> AWS Error Code:   " + ex.getErrorCode());
            System.err.println("   |-> Error Type:       " + ex.getErrorType());
            System.err.println("   |-> Request ID:       " + ex.getRequestId());

            pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_CREATE_ENDPOINT_FAILURE);
            pushNotificationResult.setMessage(ex.getMessage());
            pushNotificationResult.setHttpStatusCode(ex.getStatusCode());
            pushNotificationResult.setErrorCode(ex.getErrorCode());
            pushNotificationResult.setErrorType(ex.getErrorType().toString());
            pushNotificationResult.setRequestId(ex.getRequestId());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", pushNotificationResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("arnEndpoint", arnEndpoint));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "createEndpoint", "closing", ActivityLog.createLogMessage(outputParameters));

        return pushNotificationResult;
    }

    @Override
    public PushNotificationResult publishMessage(String arnEndpoint, PushNotificationMessage notificationMessage, boolean publishToTopic, boolean showJSON) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("arnEndpoint", arnEndpoint));

        String messageString = "{ " + "id: " + notificationMessage.getMessage().getId() + ", title: " + notificationMessage.getMessage().getTitle() + ", text: "
                + notificationMessage.getMessage().getText() + " }";

        inputParameters.add(new Pair<String, String>("notificationMessage", messageString));
        inputParameters.add(new Pair<String, String>("publishToTopic", String.valueOf(publishToTopic)));

        log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "publishMessage", "opening", ActivityLog.createLogMessage(inputParameters));

        //System.out.println("Invoking AWS SNS service...");

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        //System.setProperty("javax.net.debug", "ssl");
        proxy.setHttp();

        PushNotificationResult pushNotificationResult = new PushNotificationResult();
        pushNotificationResult.setRequestTimestamp(new Date());
        String messageId = null;

        try {

            if (arnEndpoint != null) {
                AmazonSNS amazonSNS = createSNSClient();
                PublishResult publishResult = publish(amazonSNS, arnEndpoint, notificationMessage, publishToTopic, showJSON);
                messageId = publishResult.getMessageId();
                pushNotificationResult.setMessageId(messageId);
                pushNotificationResult.setArnEndpoint(arnEndpoint);
                pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS);
            }
            else {
                pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_ARN_NOT_FOUND);
                pushNotificationResult.setMessage("User arn endpoint not found");
            }
        }
        catch (AmazonSNSException ex) {
            System.err.println("PublishMessage ERROR" + ex.getMessage());
            System.err.println("   |-> Device Endpoint:  " + arnEndpoint);
            System.err.println("   |-> Error Message:    " + ex.getMessage());
            System.err.println("   |-> HTTP Status Code: " + ex.getStatusCode());
            System.err.println("   |-> AWS Error Code:   " + ex.getErrorCode());
            System.err.println("   |-> Error Type:       " + ex.getErrorType());
            System.err.println("   |-> Request ID:       " + ex.getRequestId());

            pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_ERROR);
            pushNotificationResult.setMessage(ex.getMessage());
            pushNotificationResult.setHttpStatusCode(ex.getStatusCode());
            pushNotificationResult.setErrorCode(ex.getErrorCode());
            pushNotificationResult.setErrorType(ex.getErrorType().toString());
            pushNotificationResult.setRequestId(ex.getRequestId());
        }
        catch (Exception ex) {
            System.err.println("PublishMessage ERROR: " + ex.getMessage());
            System.err.println("   |-> Device Endpoint:  " + arnEndpoint);

            pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_ERROR);
            pushNotificationResult.setMessage(ex.getMessage());
        }
        //deleteAllPlatformEndpoints(amazonSNS);

        proxy.unsetHttp();

        //System.out.println("Published messageID: " + messageId);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", pushNotificationResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", pushNotificationResult.getMessage() != null ? pushNotificationResult.getMessage() : ""));
        outputParameters.add(new Pair<String, String>("messageId", messageId != null ? messageId : ""));
        outputParameters.add(new Pair<String, String>("arnEndpoint", arnEndpoint != null ? arnEndpoint : ""));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "publishMessage", "closing", ActivityLog.createLogMessage(outputParameters));

        return pushNotificationResult;
    }

    @Override
    public PushNotificationResult removeEndpoint(String arnEndpoint) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("arnEndpoint", (arnEndpoint != null ? arnEndpoint : "")));

        log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "removeEndpoint", "opening", ActivityLog.createLogMessage(inputParameters));

        //System.out.println("Invoking AWS SNS service...");

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        //System.setProperty("javax.net.debug", "ssl");
        proxy.setHttp();
        PushNotificationResult pushNotificationResult = new PushNotificationResult();

        try {
            AmazonSNS amazonSNS = createSNSClient();

            if (arnEndpoint != null) {
                deletePlatformEndpoint(amazonSNS, arnEndpoint);
                pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_REMOVE_ENDPOINT_SUCCESS);
            }
        }
        catch (AmazonSNSException ex) {
            System.err.println("RemoveEndpoint ERROR");
            System.err.println("   |-> Device Endpoint:  " + arnEndpoint);
            System.err.println("   |-> Error Message:    " + ex.getMessage());
            System.err.println("   |-> HTTP Status Code: " + ex.getStatusCode());
            System.err.println("   |-> AWS Error Code:   " + ex.getErrorCode());
            System.err.println("   |-> Error Type:       " + ex.getErrorType());
            System.err.println("   |-> Request ID:       " + ex.getRequestId());

            pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_REMOVE_ENDPOINT_FAILURE);
            pushNotificationResult.setMessage(ex.getMessage());
            pushNotificationResult.setHttpStatusCode(ex.getStatusCode());
            pushNotificationResult.setErrorCode(ex.getErrorCode());
            pushNotificationResult.setErrorType(ex.getErrorType().toString());
            pushNotificationResult.setRequestId(ex.getRequestId());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", pushNotificationResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "removeEndpoint", "closing", ActivityLog.createLogMessage(outputParameters));

        return pushNotificationResult;
    }

    @Override
    public PushNotificationResult createTopic(String topicName) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("topicName", (topicName != null ? topicName : "")));

        log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "createTopic", "opening", ActivityLog.createLogMessage(inputParameters));

        //System.out.println("Invoking AWS SNS service...");

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        //System.setProperty("javax.net.debug", "ssl");
        proxy.setHttp();

        String arnEndpoint = null;
        PushNotificationResult pushNotificationResult = new PushNotificationResult();
        pushNotificationResult.setRequestTimestamp(new Date());

        try {
            AmazonSNS amazonSNS = createSNSClient();

            //create a new SNS topic
            CreateTopicRequest createTopicRequest = new CreateTopicRequest(topicName);
            CreateTopicResult createTopicResult = amazonSNS.createTopic(createTopicRequest);
            arnEndpoint = createTopicResult.getTopicArn();
            //print TopicArn
            System.out.println("Topic ARN:" + arnEndpoint);
            //get request id for CreateTopicRequest from SNS metadata       
            System.out.println("CreateTopicRequest - " + amazonSNS.getCachedResponseMetadata(createTopicRequest));

            pushNotificationResult.setHttpStatusCode(200);
            pushNotificationResult.setArnEndpoint(arnEndpoint);
            pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_CREATE_TOPIC_SUCCESS);
        }
        catch (AmazonSNSException ex) {
            System.err.println("createTopic ERROR");
            System.err.println("   |-> Topic Name:     " + topicName);
            System.err.println("   |-> Error Message:    " + ex.getMessage());
            System.err.println("   |-> HTTP Status Code: " + ex.getStatusCode());
            System.err.println("   |-> AWS Error Code:   " + ex.getErrorCode());
            System.err.println("   |-> Error Type:       " + ex.getErrorType());
            System.err.println("   |-> Request ID:       " + ex.getRequestId());

            pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_CREATE_TOPIC_FAILURE);
            pushNotificationResult.setMessage(ex.getMessage());
            pushNotificationResult.setHttpStatusCode(ex.getStatusCode());
            pushNotificationResult.setErrorCode(ex.getErrorCode());
            pushNotificationResult.setErrorType(ex.getErrorType().toString());
            pushNotificationResult.setRequestId(ex.getRequestId());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", pushNotificationResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("arnEndpoint", arnEndpoint));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "createEndpoint", "closing", ActivityLog.createLogMessage(outputParameters));

        return pushNotificationResult;
    }

    @Override
    public PushNotificationResult subscribeToTopic(String arnTopic, List<String> arnEndpointList, boolean clearAllSubscription) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("arnTopic", (arnTopic != null ? arnTopic : "")));
        inputParameters.add(new Pair<String, String>("arnEndpointList", (arnEndpointList != null && !arnEndpointList.isEmpty() ? String.valueOf(arnEndpointList.size()) : "0")));
        inputParameters.add(new Pair<String, String>("clearAllSubscription", String.valueOf(clearAllSubscription)));

        log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "subscribeToTopic", "opening", ActivityLog.createLogMessage(inputParameters));

        //System.out.println("Invoking AWS SNS service...");

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        //System.setProperty("javax.net.debug", "ssl");
        proxy.setHttp();
        PushNotificationResult pushNotificationResult = new PushNotificationResult();

        try {
            AmazonSNS amazonSNS = createSNSClient();

            if (clearAllSubscription) {
                ListSubscriptionsByTopicRequest listSubscriptionsByTopicRequest = new ListSubscriptionsByTopicRequest(arnTopic);
                ListSubscriptionsByTopicResult listSubscriptionsByTopicResult = amazonSNS.listSubscriptionsByTopic(listSubscriptionsByTopicRequest);

                for (Subscription subscription : listSubscriptionsByTopicResult.getSubscriptions()) {
                    if (subscription.getProtocol().equals("application")) {
                        amazonSNS.unsubscribe(subscription.getSubscriptionArn());
                    }
                }

                System.out.println("Removed all subscriptions from topic: " + arnTopic);
            }

            for (String arnEndpoint : arnEndpointList) {
                try {
                    //subscribe to an SNS topic
                    SubscribeRequest subRequest = new SubscribeRequest(arnTopic, "application", arnEndpoint);
                    amazonSNS.subscribe(subRequest);
                    //get request id for SubscribeRequest from SNS metadata
                    System.out.println("SubscribeRequest - " + amazonSNS.getCachedResponseMetadata(subRequest));
                }
                catch (SubscriptionLimitExceededException | InvalidParameterException | InternalErrorException | NotFoundException | AuthorizationErrorException ex) {
                    System.err.println("subscribeToTopic ERROR");
                    System.err.println("   |-> Error Message:    " + ex.getMessage());
                    System.err.println("   |-> HTTP Status Code: " + ex.getStatusCode());
                    System.err.println("   |-> AWS Error Code:   " + ex.getErrorCode());
                    pushNotificationResult.getErrorSubscriptionsList().add(arnEndpoint);
                }
            }

            pushNotificationResult.setHttpStatusCode(200);
            pushNotificationResult.setArnEndpoint(arnTopic);
            pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_SUBSCRIBE_TO_TOPIC_SUCCESS);
        }
        catch (AmazonSNSException ex) {
            System.err.println("subscribeToTopic ERROR");
            System.err.println("   |-> Topic ARN:     " + arnTopic);
            System.err.println("   |-> Error Message:    " + ex.getMessage());
            System.err.println("   |-> HTTP Status Code: " + ex.getStatusCode());
            System.err.println("   |-> AWS Error Code:   " + ex.getErrorCode());
            System.err.println("   |-> Error Type:       " + ex.getErrorType());
            System.err.println("   |-> Request ID:       " + ex.getRequestId());

            pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_SUBSCRIBE_TO_TOPIC_FAILURE);
            pushNotificationResult.setMessage(ex.getMessage());
            pushNotificationResult.setHttpStatusCode(ex.getStatusCode());
            pushNotificationResult.setErrorCode(ex.getErrorCode());
            pushNotificationResult.setErrorType(ex.getErrorType().toString());
            pushNotificationResult.setRequestId(ex.getRequestId());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", pushNotificationResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "subscribeToTopic", "closing", ActivityLog.createLogMessage(outputParameters));

        return pushNotificationResult;
    }

    @Override
    public PushNotificationResult subscribeToTopic(String arnTopic, String arnEndpoint) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("arnTopic", (arnTopic != null ? arnTopic : "")));
        inputParameters.add(new Pair<String, String>("arnEndpoint", (arnEndpoint != null ? arnEndpoint : "")));

        log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "subscribeToTopic", "opening", ActivityLog.createLogMessage(inputParameters));

        //System.out.println("Invoking AWS SNS service...");

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        //System.setProperty("javax.net.debug", "ssl");
        proxy.setHttp();
        PushNotificationResult pushNotificationResult = new PushNotificationResult();

        try {
            AmazonSNS amazonSNS = createSNSClient();

            try {
                //subscribe to an SNS topic
                SubscribeRequest subRequest = new SubscribeRequest(arnTopic, "application", arnEndpoint);
                amazonSNS.subscribe(subRequest);
                //get request id for SubscribeRequest from SNS metadata
                System.out.println("SubscribeRequest - " + amazonSNS.getCachedResponseMetadata(subRequest));
                
                pushNotificationResult.setHttpStatusCode(200);
                pushNotificationResult.setArnEndpoint(arnTopic);
                pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_SUBSCRIBE_TO_TOPIC_SUCCESS);
            }
            catch (SubscriptionLimitExceededException | InvalidParameterException | InternalErrorException | NotFoundException | AuthorizationErrorException ex) {
                System.err.println("subscribeToTopic ERROR");
                System.err.println("   |-> Error Message:    " + ex.getMessage());
                System.err.println("   |-> HTTP Status Code: " + ex.getStatusCode());
                System.err.println("   |-> AWS Error Code:   " + ex.getErrorCode());
                
                pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_SUBSCRIBE_TO_TOPIC_FAILURE);
                pushNotificationResult.getErrorSubscriptionsList().add(arnEndpoint);
                pushNotificationResult.setMessage(ex.getMessage());
                pushNotificationResult.setHttpStatusCode(ex.getStatusCode());
                pushNotificationResult.setErrorCode(ex.getErrorCode());
            }
        }
        catch (AmazonSNSException ex) {
            System.err.println("subscribeToTopic ERROR");
            System.err.println("   |-> Topic ARN:     " + arnTopic);
            System.err.println("   |-> Error Message:    " + ex.getMessage());
            System.err.println("   |-> HTTP Status Code: " + ex.getStatusCode());
            System.err.println("   |-> AWS Error Code:   " + ex.getErrorCode());
            System.err.println("   |-> Error Type:       " + ex.getErrorType());
            System.err.println("   |-> Request ID:       " + ex.getRequestId());

            pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_SUBSCRIBE_TO_TOPIC_FAILURE);
            pushNotificationResult.setMessage(ex.getMessage());
            pushNotificationResult.setHttpStatusCode(ex.getStatusCode());
            pushNotificationResult.setErrorCode(ex.getErrorCode());
            pushNotificationResult.setErrorType(ex.getErrorType().toString());
            pushNotificationResult.setRequestId(ex.getRequestId());
        }
        catch (SdkClientException ex) {
            
            System.err.println("subscribeToTopic ERROR");
            System.err.println("   |-> Topic ARN:     " + arnTopic);
            System.err.println("   |-> Error Message:    " + ex.getMessage());

            if (ex.getCause() != null && (ex.getCause().getClass().equals(ConnectTimeoutException.class) || ex.getCause().getClass().equals(SocketTimeoutException.class))) {
                pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_SUBSCRIBE_TO_TOPIC_TO_RETRY);
            }
            else {
                pushNotificationResult.setStatusCode(StatusCode.PUSH_NOTIFICATION_SUBSCRIBE_TO_TOPIC_FAILURE);
            }

            pushNotificationResult.setMessage(ex.getMessage());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", pushNotificationResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "subscribeToTopic", "closing", ActivityLog.createLogMessage(outputParameters));

        return pushNotificationResult;
    }
    
    @Override
    public boolean topicIsEmpty(String arnTopic) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("arnTopic", (arnTopic != null ? arnTopic : "")));

        log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "topicIsEmpty", "opening", ActivityLog.createLogMessage(inputParameters));

        //System.out.println("Invoking AWS SNS service...");

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        //System.setProperty("javax.net.debug", "ssl");
        proxy.setHttp();
        ListSubscriptionsByTopicResult result = new ListSubscriptionsByTopicResult();
        boolean isEmpty = false;

        try {
            AmazonSNS amazonSNS = createSNSClient();
            
            try {
                result = amazonSNS.listSubscriptionsByTopic(arnTopic);
                System.out.println("ListSubscriptionsByTopicResult: " + result.getSubscriptions().size());
            }
            catch (SubscriptionLimitExceededException | InvalidParameterException | InternalErrorException | NotFoundException | AuthorizationErrorException ex) {
                System.err.println("topicIsEmpty ERROR");
                System.err.println("   |-> Error Message:    " + ex.getMessage());
                System.err.println("   |-> HTTP Status Code: " + ex.getStatusCode());
                System.err.println("   |-> AWS Error Code:   " + ex.getErrorCode());
            }
            
            if (result.getSubscriptions() != null) {
                isEmpty = result.getSubscriptions().isEmpty();
            }
        }
        catch (AmazonSNSException ex) {
            System.err.println("topicIsEmpty ERROR");
            System.err.println("   |-> Topic ARN:     " + arnTopic);
            System.err.println("   |-> Error Message:    " + ex.getMessage());
            System.err.println("   |-> HTTP Status Code: " + ex.getStatusCode());
            System.err.println("   |-> AWS Error Code:   " + ex.getErrorCode());
            System.err.println("   |-> Error Type:       " + ex.getErrorType());
            System.err.println("   |-> Request ID:       " + ex.getRequestId());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("isEmpty", String.valueOf(isEmpty)));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "PushNotificationService", "topicIsEmpty", "closing", ActivityLog.createLogMessage(outputParameters));
        
        return isEmpty;
    }

    private AmazonSNS createSNSClient() {
        ClientConfiguration clientConfiguration = PredefinedClientConfigurations.defaultConfig();

        if (connectionTimeout != -1) {
            clientConfiguration.setConnectionTimeout(connectionTimeout);
        }

        AmazonSNSClientBuilder amazonSNSClientBuilder = AmazonSNSClientBuilder.standard().withRegion(clientRegion).withCredentials(new DefaultAWSCredentialsProviderChain() {
            @Override
            public AWSCredentials getCredentials() {
                return new BasicAWSCredentials(accessKey, secretKey);
            }
        }).withClientConfiguration(clientConfiguration);

        AmazonSNS amazonSNS = amazonSNSClientBuilder.build();

        return amazonSNS;
    }

    private CreatePlatformEndpointResult createPlatformEndpoint(AmazonSNS amazonSNS, String token, String customData, String arnApplication) throws AmazonSNSException {
        /*
         * CreatePlatformEndpointResult endpointResult = null;
         * String arnEndpoint = null;
         * try {
         * System.out.println("Creating platform endpoint with token " + token);
         * CreatePlatformEndpointRequest endpointRequest = new CreatePlatformEndpointRequest();
         * endpointRequest.setCustomUserData(customData);
         * endpointRequest.setToken(token);
         * endpointRequest.setPlatformApplicationArn(arnApplication);
         * endpointResult = amazonSNS.createPlatformEndpoint(endpointRequest);
         * arnEndpoint = endpointResult.getEndpointArn();
         * }
         * catch (InvalidParameterException ipe) {
         * String message = ipe.getErrorMessage();
         * System.out.println("Exception message: " + message);
         * Pattern p = Pattern.compile(".*Endpoint (arn:aws:sns[^ ]+) already exists " + "with the same token.*");
         * Matcher m = p.matcher(message);
         * if (m.matches()) {
         * // The platform endpoint already exists for this token, but with
         * // additional custom data that
         * // createEndpoint doesn't want to overwrite. Just use the
         * // existing platform endpoint.
         * arnEndpoint = m.group(1);
         * }
         * else {
         * // Rethrow the exception, the input is actually bad.
         * throw ipe;
         * }
         * }
         */
        //System.out.println("Creating platform endpoint with token " + token);
        CreatePlatformEndpointRequest endpointRequest = new CreatePlatformEndpointRequest();
        endpointRequest.setCustomUserData(customData);
        endpointRequest.setToken(token);
        endpointRequest.setPlatformApplicationArn(arnApplication);
        CreatePlatformEndpointResult endpointResult = amazonSNS.createPlatformEndpoint(endpointRequest);

        return endpointResult;
    }

    private Endpoint getPlatformEndpoint(AmazonSNS amazonSNS, String deviceToken, String arnApplication) throws AmazonSNSException {
        ListEndpointsByPlatformApplicationRequest request = new ListEndpointsByPlatformApplicationRequest();
        request.setPlatformApplicationArn(arnApplication);
        ListEndpointsByPlatformApplicationResult result = amazonSNS.listEndpointsByPlatformApplication(request);
        List<Endpoint> endpointList = result.getEndpoints();
        //System.out.println("Total platform endpoints: " + endpointList.size());
        Endpoint platformEndpoint = null;

        for (Endpoint endpoint : endpointList) {
            Map<String, String> attribs = endpoint.getAttributes();
            String token = attribs.get("Token");

            if (token != null && token.equals(deviceToken)) {
                platformEndpoint = endpoint;
                //System.out.println("Endpoint Token found: " + token);
                break;
            }
        }

        return platformEndpoint;
    }

    private String jsonify(Object message) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(message);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw (RuntimeException) e;
        }
    }

    private Map<String, Object> createMultiPlatformMessage(PushNotificationMessage notificationMessage) {
        Map<String, Object> multiPlatformMessage = new HashMap<String, Object>();
        Map<String, Object> apnsMessageMap = createAPNSMessage(notificationMessage);
        Map<String, Object> admMessageMap = createADMMessage(notificationMessage);
        Map<String, Object> gcmMessageMap = createGCMMessage(notificationMessage);
        String wnsMessageXML = createWNSMessage(notificationMessage);

        multiPlatformMessage.put("default", jsonify(notificationMessage));
        multiPlatformMessage.put("APNS", jsonify(apnsMessageMap));
        multiPlatformMessage.put("APNS_SANDBOX", jsonify(apnsMessageMap));
        multiPlatformMessage.put("ADM", jsonify(admMessageMap));
        multiPlatformMessage.put("GCM", jsonify(gcmMessageMap));
        //multiPlatformMessage.put("WNS", jsonify(wnsMessageXML));
        multiPlatformMessage.put("WNS", wnsMessageXML);

        return multiPlatformMessage;
    }

    private Map<String, Object> createAPNSMessage(PushNotificationMessage notificationMessage) {
        Map<String, Object> messageMap = new HashMap<String, Object>();
        Map<String, Object> appMessageMap = new HashMap<String, Object>();
        Map<String, Object> alertMessageMap = new HashMap<String, Object>();

        alertMessageMap.put("title", notificationMessage.getMessage().getTitle());
        alertMessageMap.put("body", notificationMessage.getMessage().getText());

        appMessageMap.put("alert", alertMessageMap);
        appMessageMap.put("badge", 0);
        appMessageMap.put("sound", "default");

        messageMap.put("aps", appMessageMap);

        messageMap.put("message", jsonify(notificationMessage));

        return messageMap;
    }

    private Map<String, Object> createADMMessage(PushNotificationMessage notificationMessage) {
        Map<String, Object> messageMap = new HashMap<String, Object>();
        Map<String, String> dataMessageMap = new HashMap<String, String>();

        dataMessageMap.put("title", notificationMessage.getMessage().getTitle());

        dataMessageMap.put("message", jsonify(notificationMessage));

        messageMap.put("data", dataMessageMap);
        messageMap.put("consolidationKey", "Welcome");
        //messageMap.put("expiresAfter", 1000);

        return messageMap;
    }

    private Map<String, Object> createGCMMessage(PushNotificationMessage notificationMessage) {
        Map<String, Object> messageMap = new HashMap<String, Object>();
        Map<String, String> notificationMessageMap = new HashMap<String, String>();
        /*
         * if (messageTitle != null) {
         * notificationMessageMap.put("title", messageTitle);
         * }
         */
        notificationMessageMap.put("text", jsonify(notificationMessage));
        //notificationMessageMap.put("sound", "default");
        //notificationMessageMap.put("message", message);

        messageMap.put("collapse_key", "Welcome");
        //messageMap.put("notification", notificationMessageMap);
        messageMap.put("data", notificationMessageMap);
        messageMap.put("delay_while_idle", true);
        //messageMap.put("time_to_live", 125);
        messageMap.put("dry_run", false);

        return messageMap;
    }

    private String createWNSMessage(PushNotificationMessage notificationMessage) {

        String messageXML = null;
        /*
         * try {
         * 
         * DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
         * DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
         * 
         * Document doc = docBuilder.newDocument();
         * doc.setXmlStandalone(true);
         * doc.setXmlVersion("1.0");
         * 
         * Element toastElement = doc.createElement("toast");
         * Attr launchAttr = doc.createAttribute("launch");
         * launchAttr.setValue(String.valueOf(notificationMessage.getMessage().getId()));
         * toastElement.setAttributeNode(launchAttr);
         * doc.appendChild(toastElement);
         * 
         * Element visualElement = doc.createElement("visual");
         * toastElement.appendChild(visualElement);
         * 
         * Element bindingElement = doc.createElement("binding");
         * Attr templateAttr = doc.createAttribute("template");
         * templateAttr.setValue("ToastText02");
         * bindingElement.setAttributeNode(templateAttr);
         * visualElement.appendChild(bindingElement);
         * 
         * Element text01Element = doc.createElement("text");
         * Attr id01Attr = doc.createAttribute("id");
         * id01Attr.setValue(notificationMessage.getMessage().getTitle());
         * text01Element.setAttributeNode(id01Attr);
         * bindingElement.appendChild(text01Element);
         * 
         * Element text02Element = doc.createElement("text");
         * Attr id02Attr = doc.createAttribute("id");
         * id02Attr.setValue(notificationMessage.getMessage().getText());
         * text02Element.setAttributeNode(id02Attr);
         * bindingElement.appendChild(text02Element);
         * 
         * TransformerFactory transformerFactory = TransformerFactory.newInstance();
         * Transformer transformer = transformerFactory.newTransformer();
         * DOMSource source = new DOMSource(doc);
         * StringWriter sw = new StringWriter();
         * StreamResult result = new StreamResult(sw);
         * transformer.transform(source, result);
         * messageXML = sw.toString();
         * 
         * } catch (ParserConfigurationException pce) {
         * System.err.println("Errore nella crezione del xml per Windows Phone: " + pce.getMessage());
         * 
         * messageXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
         * + "<toast launch=\"" + notificationMessage.getMessage().getId() + "\"><visual><binding template=\"ToastText02\">"
         * + "<text id=\"1\">" + notificationMessage.getMessage().getTitle() + "</text>"
         * + "<text id=\"2\"> " + notificationMessage.getMessage().getText() + "</text>"
         * + "</binding></visual></toast>";
         * } catch (TransformerException tfe) {
         * System.err.println("Errore nella crezione del xml per Windows Phone: " + tfe.getMessage());
         * 
         * messageXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
         * + "<toast launch=\"" + notificationMessage.getMessage().getId() + "\"><visual><binding template=\"ToastText02\">"
         * + "<text id=\"1\">" + notificationMessage.getMessage().getTitle() + "</text>"
         * + "<text id=\"2\"> " + notificationMessage.getMessage().getText() + "</text>"
         * + "</binding></visual></toast>";
         * }
         */
        messageXML = "<toast launch=\"" + notificationMessage.getMessage().getId() + "\"><visual><binding template=\"ToastText02\">" + "<text id=\"1\">"
                + notificationMessage.getMessage().getTitle() + "</text>" + "<text id=\"2\"> " + notificationMessage.getMessage().getText() + "</text>"
                + "</binding></visual></toast>";

        return messageXML;
    }

    private PublishResult publish(AmazonSNS amazonSNS, String arnEndpoint, PushNotificationMessage notificationMessage, boolean publishToTopic, boolean showJSON)
            throws AmazonSNSException {
        PublishRequest publishRequest = new PublishRequest();

        // For direct publish to mobile end points, topicArn is not relevant.
        if (publishToTopic) {
            publishRequest.setTopicArn(arnEndpoint);
        }
        else {
            publishRequest.setTargetArn(arnEndpoint);
        }

        Map<String, MessageAttributeValue> messageAttributes = new HashMap<String, MessageAttributeValue>();

        messageAttributes.put("AWS.SNS.MOBILE.WNS.Type", new MessageAttributeValue().withDataType("String").withStringValue("wns/toast"));
        // Insert your desired value (in seconds) of TTL here. For example, a TTL of 1 day would be 86,400 seconds. 
        messageAttributes.put("AWS.SNS.MOBILE.ADM.TTL", new MessageAttributeValue().withDataType("String").withStringValue("86400"));
        messageAttributes.put("AWS.SNS.MOBILE.GCM.TTL", new MessageAttributeValue().withDataType("String").withStringValue("86400"));
        messageAttributes.put("AWS.SNS.MOBILE.APNS.TTL", new MessageAttributeValue().withDataType("String").withStringValue("86400"));
        messageAttributes.put("AWS.SNS.MOBILE.APNS_SANDBOX.TTL", new MessageAttributeValue().withDataType("String").withStringValue("86400"));
        messageAttributes.put("AWS.SNS.MOBILE.WNS.TTL", new MessageAttributeValue().withDataType("String").withStringValue("86400"));

        publishRequest.setMessageStructure("json");
        Map<String, Object> messageMap = createMultiPlatformMessage(notificationMessage);
        String messageJSON = jsonify(messageMap);
        publishRequest.setMessage(messageJSON);

        if (showJSON) {
            System.out.println("Message JSNON: " + messageJSON);
        }

        publishRequest.setMessageAttributes(messageAttributes);

        // Display the message that will be sent to the endpoint/

        PublishResult publishResult = amazonSNS.publish(publishRequest);

        System.out.println("PublishRequest: " + amazonSNS.getCachedResponseMetadata(publishRequest) + " - HttpStatusCode: "
                + publishResult.getSdkHttpMetadata().getHttpStatusCode());

        return publishResult;
    }

    private DeleteEndpointResult deletePlatformEndpoint(AmazonSNS amazonSNS, String arnEndpoint) throws AmazonSNSException {
        DeleteEndpointRequest deleteRequest = new DeleteEndpointRequest();
        deleteRequest.setEndpointArn(arnEndpoint);
        DeleteEndpointResult deleteResult = amazonSNS.deleteEndpoint(deleteRequest);
        System.out.println("DeleteRequest: " + amazonSNS.getCachedResponseMetadata(deleteRequest) + " - HttpStatusCode: " + deleteResult.getSdkHttpMetadata().getHttpStatusCode());

        return deleteResult;
    }
    //FIXME: da eliminare?
    //    
    //    private void deleteAllPlatformEndpoints(AmazonSNS amazonSNS, String arnApplication) throws AmazonSNSException {
    //        ListEndpointsByPlatformApplicationRequest request = new ListEndpointsByPlatformApplicationRequest();
    //        request.setPlatformApplicationArn(arnApplication);
    //        ListEndpointsByPlatformApplicationResult result = amazonSNS.listEndpointsByPlatformApplication(request);
    //        List<Endpoint> endpointList = result.getEndpoints();
    //        System.out.println("Total platform endpoints to delete: " + endpointList.size());
    //        
    //        for (Endpoint endpoint : endpointList) {
    //            DeleteEndpointRequest deleteRequest = new DeleteEndpointRequest();
    //            deleteRequest.setEndpointArn(endpoint.getEndpointArn());
    //            DeleteEndpointResult deleteResult = amazonSNS.deleteEndpoint(deleteRequest);
    //            System.out.println("DeleteRequest: " + amazonSNS.getCachedResponseMetadata(deleteRequest)
    //                    + " - HttpStatusCode: " + deleteResult.getSdkHttpMetadata().getHttpStatusCode());
    //        }
    //    }

}
