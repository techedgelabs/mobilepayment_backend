CHANGELOG for 2.5.x
===================

This changelog references the relevant changes (bug and security fixes) done
in 2.5 minor versions.

* 2.5.1 (2019-05-27)

 * Integrazione nuovo CRM SF
 * Bugfix minori

* 2.5.0a (2019-05-23)

 * Promo cross Diesel+

* 2.5.0 (2019-04-11)

 * Promozione vodafone black
 * Bugfix minori
