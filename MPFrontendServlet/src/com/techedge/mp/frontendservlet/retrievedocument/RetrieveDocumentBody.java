package com.techedge.mp.frontendservlet.retrievedocument;

public class RetrieveDocumentBody {

    private String documentId;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

}
