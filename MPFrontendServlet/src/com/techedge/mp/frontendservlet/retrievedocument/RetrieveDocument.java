package com.techedge.mp.frontendservlet.retrievedocument;

public class RetrieveDocument {

    private RetrieveDocumentRequest retrieveDocument;

    public RetrieveDocumentRequest getRetrieveDocument() {
        return retrieveDocument;
    }

    public void setRetrieveDocument(RetrieveDocumentRequest retrieveDocument) {
        this.retrieveDocument = retrieveDocument;
    }

}
