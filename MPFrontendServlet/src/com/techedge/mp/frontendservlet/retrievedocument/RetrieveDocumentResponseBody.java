package com.techedge.mp.frontendservlet.retrievedocument;


public class RetrieveDocumentResponseBody {
	
	private String documentBase64 = new String();
	
	private String filename;
	private Long dimension;

	public String getDocumentBase64() {
		return documentBase64;
	}

	public void setDocumentBase64(String documentBase64) {
		this.documentBase64 = documentBase64;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Long getDimension() {
		return dimension;
	}

	public void setDimension(Long dimension) {
		this.dimension = dimension;
	}

	
}
