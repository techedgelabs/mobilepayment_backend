package com.techedge.mp.frontendservlet.retrievedocument;

import com.techedge.mp.frontendservlet.BaseResponse;

public class RetrieveDocumentResponse extends BaseResponse {

    private RetrieveDocumentResponseBody body;

    public RetrieveDocumentResponseBody getBody() {
        return body;
    }

    public void setBody(RetrieveDocumentResponseBody body) {
        this.body = body;
    }
}
