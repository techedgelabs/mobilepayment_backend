package com.techedge.mp.frontendservlet.retrievedocument;

import com.techedge.mp.frontendservlet.Credential;

public class RetrieveDocumentRequest {

    private Credential           credential;
    private RetrieveDocumentBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveDocumentBody getBody() {
        return body;
    }

    public void setBody(RetrieveDocumentBody body) {
        this.body = body;
    }

}
