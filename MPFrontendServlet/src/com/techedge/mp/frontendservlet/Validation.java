package com.techedge.mp.frontendservlet;

public class Validation {
  private String type;
  private String verificationField;
  private String verificationCode;
  
  public String getType() {
    return type;
  }
  
  public void setType(String type) {
    this.type = type;
  }
  
  public String getVerificationField() {
    return verificationField;
  }
  
  public void setVerificationField(String verificationField) {
    this.verificationField = verificationField;
  }
  
  public String getVerificationCode() {
    return verificationCode;
  }
  
  public void setVerificationCode(String verificationCode) {
    this.verificationCode = verificationCode;
  }
  
  

}
