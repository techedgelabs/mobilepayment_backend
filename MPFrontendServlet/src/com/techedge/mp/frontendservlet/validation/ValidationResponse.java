package com.techedge.mp.frontendservlet.validation;

import com.techedge.mp.frontendservlet.Status;

public class ValidationResponse {
  private Status status;
    
  public Status getStatus() {
    return status;
  }
  
  public void setStatus(Status status) {
    this.status = status;
  }
  
  
  
}
