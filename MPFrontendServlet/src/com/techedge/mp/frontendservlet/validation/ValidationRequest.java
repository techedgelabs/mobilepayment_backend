package com.techedge.mp.frontendservlet.validation;

public class ValidationRequest {
  private ValidationBodyRequest validateField;

  public ValidationBodyRequest getValidateField() {
    return validateField;
  }

  public void setValidateField(ValidationBodyRequest validateField) {
    this.validateField = validateField;
  }
  
  
}
