package com.techedge.mp.frontendservlet.validation;

import com.techedge.mp.frontendservlet.Credential;
import com.techedge.mp.frontendservlet.Validation;

public class ValidationBodyRequest {
  private Credential credential;
  private Validation body;

  public Validation getBody() {
    return body;
  }

  public void setBody(Validation body) {
    this.body = body;
  }

  public Credential getCredential() {
    return credential;
  }

  public void setCredential(Credential credential) {
    this.credential = credential;
  }
  
  
}
