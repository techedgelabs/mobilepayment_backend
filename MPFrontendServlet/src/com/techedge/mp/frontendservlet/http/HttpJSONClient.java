package com.techedge.mp.frontendservlet.http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpJSONClient {

  private final String DEFAULT_USER_AGENT = "Mozilla/5.0";
  private final String DEFAULT_CHARSET = "UTF-8";
  
  private String userAgent = null;
  private String charset = null;

  public HttpJSONClient(String userAgent, String charset) {
    if (userAgent != null) {
      this.userAgent = userAgent; 
    }
    else {
      this.userAgent = DEFAULT_USER_AGENT;
    }

    if (charset != null) {
      this.charset = charset; 
    }
    else {
      this.charset = DEFAULT_CHARSET;
    }
  }
  
  public HttpJSONClient() {
    this(null, null);
  }
  
  // HTTP POST request
  public HttpResponse sendPost(String url, String json) throws Exception {

    System.out.println("\nSending 'POST' request to URL : " + url);
    System.out.println("JSON data : " + json);

    URL obj = new URL(url);
    HttpURLConnection  con = (HttpURLConnection) obj.openConnection();
    //HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

    // add reuqest header
    con.setRequestMethod("POST");
    con.setRequestProperty("User-Agent", userAgent);
    con.setRequestProperty("Content-type", "application/json; charset=" + charset);

    // Send post request
    con.setDoOutput(true);
    con.setDoInput(true);
    
    DataOutputStream out = new DataOutputStream(con.getOutputStream());
    out.writeBytes(json);
    out.flush();
    out.close();

    HttpResponse httpResponse = new HttpResponse(con.getResponseCode(), con.getResponseMessage());
    
    // print result
    System.out.println("Response Code : " + httpResponse.getCode());
    System.out.println("Response Message : " + httpResponse.getMessage());

    if (isResponseOK(httpResponse)) {
      BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
      String inputLine;
      StringBuffer buf = new StringBuffer();

      while ((inputLine = in.readLine()) != null) {
        buf.append(inputLine);
      }
      
      in.close();
      httpResponse.setContent(buf.toString());
      System.out.println("Response Content : " + httpResponse.getContent());
    }
    
    con.disconnect();    
    return httpResponse;

  }
  
  public boolean isResponseOK(HttpResponse httpResponse)
  {
    return (httpResponse.getCode() == HttpURLConnection.HTTP_OK);
  }

}