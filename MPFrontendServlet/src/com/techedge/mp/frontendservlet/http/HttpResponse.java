package com.techedge.mp.frontendservlet.http;

public class HttpResponse {

  private int code;
  private String message;
  private String content;
  
  public HttpResponse(int code, String message) {
    this.code = code;
    this.message = message;
    this.content = null;
  }

  public HttpResponse() {
    this.code = -1;
    this.message = null;
    this.content = null;
  }
  
  public void setCode(int code) {
    this.code = code;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getCode() {
    return code;
  }
  
  public String getMessage() {
    return message;
  }

  public String getContent() {
    return content;
  }
}
