package com.techedge.mp.frontendservlet.smslog;

public class UpdateSmsLogRequest {
  private UpdateSmsLogBodyRequest updateSmsLog;

  public UpdateSmsLogBodyRequest getUpdateSmsLog() {
    return updateSmsLog;
  }

  public void setUpdateSmsLog(UpdateSmsLogBodyRequest updateSmsLog) {
    this.updateSmsLog = updateSmsLog;
  }
  
  
}
