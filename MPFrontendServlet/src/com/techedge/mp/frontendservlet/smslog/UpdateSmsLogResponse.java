package com.techedge.mp.frontendservlet.smslog;

import com.techedge.mp.frontendservlet.Status;


public class UpdateSmsLogResponse {
    private Status status;
    
    public Status getStatus() {
      return status;
    }
    
    public void setStatus(Status status) {
      this.status = status;
    }

}
