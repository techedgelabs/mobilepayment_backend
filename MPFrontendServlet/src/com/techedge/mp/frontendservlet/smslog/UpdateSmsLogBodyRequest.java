package com.techedge.mp.frontendservlet.smslog;

import com.techedge.mp.frontendservlet.Credential;
import com.techedge.mp.frontendservlet.SmsReport;

public class UpdateSmsLogBodyRequest {

    private Credential credential;
    private SmsReport  body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public SmsReport getBody() {
        return body;
    }

    public void setBody(SmsReport body) {
        this.body = body;
    }

}
