package com.techedge.mp.frontendservlet;

public class Credential {
  private String ticketID;
  private String requestID;
  
  public String getTicketID() {
    return ticketID;
  }
  
  public void setTicketID(String ticketID) {
    this.ticketID = ticketID;
  }
  
  public String getRequestID() {
    return requestID;
  }
  
  public void setRequestID(String requestID) {
    this.requestID = requestID;
  }
  
  

}
