package com.techedge.mp.frontendservlet.authentication;

public class AuthenticationBodyResponse {
  private String ticketID;
  private Object userData;
  
  public String getTicketID() {
    return ticketID;
  }
  public void setTicketID(String ticketID) {
    this.ticketID = ticketID;
  }
  public Object getUserData() {
    return userData;
  }
  public void setUserData(Object userData) {
    this.userData = userData;
  }
  
  
}
