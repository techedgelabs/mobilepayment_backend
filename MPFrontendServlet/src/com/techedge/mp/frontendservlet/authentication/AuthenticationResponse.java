package com.techedge.mp.frontendservlet.authentication;

import com.techedge.mp.frontendservlet.Status;

public class AuthenticationResponse {
  private Status status;
  private AuthenticationBodyResponse body;
  
  
  public Status getStatus() {
    return status;
  }
  public void setStatus(Status status) {
    this.status = status;
  }
  public AuthenticationBodyResponse getBody() {
    return body;
  }
  public void setBody(AuthenticationBodyResponse body) {
    this.body = body;
  }
  
  
  
}
