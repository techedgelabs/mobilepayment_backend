package com.techedge.mp.frontendservlet.authentication;

import com.techedge.mp.frontendservlet.Authentication;

public class AuthenticationBodyRequest {
  private Authentication body;

  public Authentication getBody() {
    return body;
  }

  public void setBody(Authentication body) {
    this.body = body;
  }
  
  
}
