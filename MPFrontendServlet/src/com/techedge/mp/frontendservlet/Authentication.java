package com.techedge.mp.frontendservlet;

public class Authentication {
  private String username;
  private String password;
  private String requestID;
  private String deviceID;
  private String deviceName;
  
  public String getUsername() {
    return username;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public String getRequestID() {
    return requestID;
  }
  public void setRequestID(String requestID) {
    this.requestID = requestID;
  }
  public String getDeviceID() {
    return deviceID;
  }
  public void setDeviceID(String deviceID) {
    this.deviceID = deviceID;
  }
  public String getDeviceName() {
    return deviceName;
  }
  public void setDeviceName(String deviceName) {
    this.deviceName = deviceName;
  }
  
  

}
