package com.techedge.mp.frontendservlet;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "smsreport")
public class SmsReport {

    private String  CorrelationId;
    private String  MessageId;
    private String  DestinationAddress;
    private Integer StatusCode;
    private Integer ReasonCode;
    private String  OperatorTimeStamp;
    private String  TimeStamp;
    private String  Operator;
    private String  StatusText;
    private Integer OperatorNetworkCode;
    private String  Message;

    @XmlElement(name = "correlationid")
    public String getCorrelationId() {
        return CorrelationId;
    }

    public void setCorrelationId(String correlationId) {
        CorrelationId = correlationId;
    }

    @XmlElement(name = "messageid")
    public String getMessageId() {
        return MessageId;
    }

    public void setMessageId(String messageId) {
        MessageId = messageId;
    }

    @XmlElement(name = "destinationaddress")
    public String getDestinationAddress() {
        return DestinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        DestinationAddress = destinationAddress;
    }

    @XmlElement(name = "statuscode")
    public Integer getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(Integer statusCode) {
        StatusCode = statusCode;
    }

    @XmlElement(name = "reasoncode")
    public Integer getReasonCode() {
        return ReasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        ReasonCode = reasonCode;
    }

    @XmlElement(name = "operatortimestamp")
    public String getOperatorTimeStamp() {
        return OperatorTimeStamp;
    }

    public void setOperatorTimeStamp(String operatorTimeStamp) {
        OperatorTimeStamp = operatorTimeStamp;
    }

    @XmlElement(name = "timestamp")
    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    @XmlElement(name = "operator")
    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    @XmlElement(name = "statustext")
    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String statusText) {
        StatusText = statusText;
    }

    @XmlElement(name = "operatornetworkcode")
    public Integer getOperatorNetworkCode() {
        return OperatorNetworkCode;
    }

    public void setOperatorNetworkCode(Integer operatorNetworkCode) {
        OperatorNetworkCode = operatorNetworkCode;
    }

    @XmlElement(name = "message")
    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

}
