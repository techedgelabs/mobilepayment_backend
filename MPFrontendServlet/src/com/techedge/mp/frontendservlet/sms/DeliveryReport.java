package com.techedge.mp.frontendservlet.sms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.frontendservlet.SmsReport;

public class DeliveryReport extends HttpServlet {

    /**
     * 
     */
    private static final long         serialVersionUID = 3054333725560434170L;

    private Hashtable<String, String> validUsers       = new Hashtable<String, String>();

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // ie this user has no password
        //validUsers.put("james:", "authorized");
        //validUsers.put("jswan:mypassword", "authorized");
        validUsers.put("netsize:n3ts1z3", "authorized");
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        out.println("So Long, and Thanks for All the Fish");
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        res.setContentType("text/xml");
        PrintWriter out = res.getWriter();
        // Get Authorization header
        String auth = req.getHeader("Authorization");
        // Do we allow that user?
        if (!allowUser(auth)) {
            // Not allowed, so report he's unauthorized
            res.setHeader("WWW-Authenticate", "BASIC realm=\"enipay\"");
            out.println("So Long, and Thanks for All the Fish");
            res.sendError(res.SC_UNAUTHORIZED);
            // Could offer to add him to the allowed user list
        }
        else {
            BufferedReader in = new BufferedReader(new InputStreamReader(req.getInputStream()));
            String inputLine;
            StringBuffer buf = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                buf.append(inputLine);
            }

            in.close();

            String xmlResponse = null;
            Document docXML = null;
            String content = buf.toString();

            try {
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

                // root elements
                docXML = docBuilder.newDocument();
                docXML.setXmlStandalone(true);
                Element rootElement = docXML.createElement("smsreport");
                docXML.appendChild(rootElement);

                String[] parameters = content.split("&");

                for (String parameter : parameters) {
                    String[] pairs = parameter.split("=");
                    // staff elements
                    Element element = docXML.createElement(pairs[0].toLowerCase());
                    element.setTextContent(URLDecoder.decode(pairs[1], "UTF-8"));
                    rootElement.appendChild(element);
                }

                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer();
                transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
                StringWriter writer = new StringWriter();
                transformer.transform(new DOMSource(docXML), new StreamResult(writer));
                String outputXML = writer.getBuffer().toString().replaceAll("\n|\r", "");
                System.out.println(outputXML);

                out.println("<DeliveryResponse ack=\"true\"/>");

                //writeReportLog(content, out);
                updateCoreSmsLog(req, out, xmlResponse, docXML);

            }
            catch (Exception ex) {

                xmlResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
                xmlResponse += "<smsreport>\n";

                out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
                out.println("<smsreport>");

                String[] parameters = content.split("&");

                for (String parameter : parameters) {
                    String[] pairs = parameter.split("=");
                    String xmlContent = "<" + pairs[0].toLowerCase() + ">" + URLDecoder.decode(pairs[1], "UTF-8") + "</" + pairs[0].toLowerCase() + ">";
                    xmlResponse += xmlContent + "\n";
                    out.println(xmlContent);
                }

                xmlResponse += "</smsreport>\n";

                out.println("</smsreport>");
                out.println("<DeliveryResponse ack=\"true\"/>");

                //writeReportLog(content, out);
                updateCoreSmsLog(req, out, xmlResponse, docXML);
            }
        }
    }

    // This method checks the user information sent in the Authorization
    // header against the database of users maintained in the users Hashtable.
    private boolean allowUser(String auth) throws IOException {

        return true;
        /*
         * if (auth == null) {
         * return false; // no auth
         * }
         * if (!auth.toUpperCase().startsWith("BASIC ")) {
         * return false; // we only do BASIC
         * }
         * // Get encoded user and password, comes after "BASIC "
         * String userpassEncoded = auth.substring(6);
         * // Decode it, using any base 64 decoder
         * sun.misc.BASE64Decoder dec = new sun.misc.BASE64Decoder();
         * String userpassDecoded = new String(dec.decodeBuffer(userpassEncoded));
         * 
         * // Check our user list to see if that user and password are "allowed"
         * if ("authorized".equals(validUsers.get(userpassDecoded))) {
         * return true;
         * }
         * else {
         * return false;
         * }
         */
    }

    private void updateCoreSmsLog(HttpServletRequest request, PrintWriter out, String content, Document docXML) {
        String scheme = request.getScheme();
        String serverName = request.getServerName();
        int serverPort = request.getServerPort();
        String servicePath = "/MPFrontendAdapter/UserJson";
        String url = scheme + "://" + serverName + ":" + serverPort + servicePath;

        try {
            // Ottieni il valore della secureString
            UserServiceRemote userService = null;

            final Hashtable jndiProperties = new Hashtable();

            jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

                final Context context = new InitialContext(jndiProperties);

                final String jndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserService!com.techedge.mp.core.business.UserServiceRemote";
                //final String jndi = "java:app/MPPaymentAdapterEJB/GSService!com.techedge.mp.payment.adapter.business.GSServiceRemote";
                Object o = context.lookup(jndi);
                userService = (UserServiceRemote) o;

                SmsReport smsReport = unmarshalResponse(content, docXML);

                String correlationID = smsReport.getCorrelationId();
                String mtMessageID = smsReport.getMessageId();
                String operator = smsReport.getOperator();
                Integer statusCode = smsReport.getStatusCode();
                Integer reasonCode = smsReport.getReasonCode();
                String responseMessage = smsReport.getMessage();
                Date operatorTimestamp = null;
                Date providerTimestamp = null;
                String timestamp;

                 try {
                    timestamp = smsReport.getOperatorTimeStamp();
                    if (timestamp != null && !timestamp.isEmpty()) {
                        timestamp = URLDecoder.decode(timestamp, "UTF-8");
                        System.out.println("operatorTimestamp: " + timestamp);
                        operatorTimestamp = new SimpleDateFormat("yyyyMMdd HH:mm:ss").parse(timestamp);
                    }   
                }
                catch (Exception e) {
                    System.err.println("Errore nel parsing della data operatorTimestamp: " + e.getMessage());
                }

                try {
                    timestamp = smsReport.getTimeStamp();
                    if (timestamp != null && !timestamp.isEmpty()) {
                        timestamp = URLDecoder.decode(timestamp, "UTF-8");
                        System.out.println("providerTimestamp: " + timestamp);
                        providerTimestamp = new SimpleDateFormat("yyyyMMdd HH:mm:ss").parse(timestamp);
                    }
                }
                catch (Exception e) {
                    System.err.println("Errore nel parsing della data providerTimestamp: " + e.getMessage());
                }

                String response = userService.updateSmsLog(correlationID, null, null, mtMessageID, statusCode, null, reasonCode, responseMessage, operatorTimestamp, providerTimestamp, operator, false);
                /*String response = userService.updateSmsLog(correlationID, mtMessageID, message, statusCode, reasonCode, operatorTimestamp,
                        providerTimestamp, operator);*/

                System.out.println("UpdateSmsLogResponse: " + response);

            }
            catch (Exception ex) {
                //ex.printStackTrace();
                System.err.println("Errore " + ex.getMessage());
            }
            /*
            Authentication authentication = new Authentication();
            AuthenticationBodyRequest authenticationBodyRequest = new AuthenticationBodyRequest();
            AuthenticationRequest authenticationRequest = new AuthenticationRequest();
            authentication.setUsername("MPSystemUser");
            authentication.setPassword("3zYGDSewI+6PCWLX8jfacA==");
            authentication.setRequestID("WEB-100000000000");
            authentication.setDeviceID("web");
            authentication.setDeviceName("WEB");
            authenticationBodyRequest.setBody(authentication);
            authenticationRequest.setAuthentication(authenticationBodyRequest);

            HttpJSONClient httpClient = new HttpJSONClient();
            HttpResponse authenticationHttpResponse = httpClient.sendPost(url, new Gson().toJson(authenticationRequest));

            if (httpClient.isResponseOK(authenticationHttpResponse)) {
                String jsonResponse = authenticationHttpResponse.getContent();
                AuthenticationResponse authenticationResponse = new AuthenticationResponse();
                authenticationResponse = new Gson().fromJson(jsonResponse, AuthenticationResponse.class);
                if (authenticationResponse.getStatus().getStatusCode().equals("USER_AUTH_200")) {
                    UpdateSmsLogRequest updateSmsLogRequest = new UpdateSmsLogRequest();
                    UpdateSmsLogBodyRequest updateSmsLogBodyRequest = new UpdateSmsLogBodyRequest();
                    Credential credential = new Credential();
                    credential.setTicketID(authenticationResponse.getBody().getTicketID());
                    credential.setRequestID(authentication.getRequestID());
                    SmsReport smsReport = unmarshalResponse(content, docXML);
                    updateSmsLogBodyRequest.setBody(smsReport);
                    updateSmsLogBodyRequest.setCredential(credential);
                    updateSmsLogRequest.setUpdateSmsLog(updateSmsLogBodyRequest);
                    HttpResponse smsLogHttpResponse = httpClient.sendPost(url, new Gson().toJson(updateSmsLogRequest));

                    if (httpClient.isResponseOK(smsLogHttpResponse)) {
                        jsonResponse = smsLogHttpResponse.getContent();
                        UpdateSmsLogResponse updateSmsLogResponse = new UpdateSmsLogResponse();
                        updateSmsLogResponse = new Gson().fromJson(jsonResponse, UpdateSmsLogResponse.class);

                        out.println("<UpdateSmsLogResponse>" + updateSmsLogResponse.getStatus().getStatusCode() + "</UpdateSmsLogResponse>");

                    }
                    else {
                        out.println("<SmsLogHttpResponse>" + smsLogHttpResponse.getMessage() + "</SmsLogHttpResponse>");
                    }
                }
                else {
                    out.println("<AuthenticationResponse>" + authenticationResponse.getStatus().getStatusCode() + "</AuthenticationResponse>");
                }
            }
            else {
                out.println("<AuthenticationHttpResponse>" + authenticationHttpResponse.getMessage() + "</AuthenticationHttpResponse>");
            }
        }
        catch (Exception e) {
            out.println("<Exception>" + e.getMessage() + "</Exception>");
        }
        */
    }

    private SmsReport unmarshalResponse(String xmlResponse, Document docXML) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(SmsReport.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        //jaxbUnmarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        //JAXBElement<UpdateSmsLogBodyRequest> jaxbElement = new JAXBElement<UpdateSmsLogBodyRequest>(new QName(null, "UpdateSmsLogBodyRequest"), UpdateSmsLogBodyRequest.class, customer);        
        if (docXML == null) {
            StringReader sr = new StringReader(xmlResponse);
            return (SmsReport) jaxbUnmarshaller.unmarshal(sr);
        }

        return (SmsReport) jaxbUnmarshaller.unmarshal(docXML);
    }

    private void writeReportLog(String content, PrintWriter out) throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String jbossHome = System.getProperty("jboss.home.dir");
        File fileSmsLog = new File(jbossHome + File.separator + "content" + File.separator + "sms_log" + File.separator + "report_" + timestamp + ".log");

        try {
            FileWriter writer;
            writer = new FileWriter(fileSmsLog);
            writer.write(content);
            writer.close();
        }
        catch (IOException e) {
            System.err.println("Errore nella scrittura del log (" + fileSmsLog.getAbsolutePath() + "): " + e.getMessage());
        }

    }

}