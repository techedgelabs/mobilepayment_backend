<%@page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html> 
    <head> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>Esito</title> 
    </head> 
    <body>
    	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
        <% 
			Enumeration en = request.getParameterNames();
 
        	String  esito       = "";
        	Integer codiceEsito = null;
        	String  messaggio   = "";
        	
            while (en.hasMoreElements()) {
                String parameterName = (String) en.nextElement();
                String parameterValue = request.getParameter(parameterName);
                
                if (parameterName.equals("esito")) {
                    esito = parameterValue;
                }
                
                if (parameterName.equals("codiceEsito")) {
                    codiceEsito = Integer.parseInt(parameterValue);
                }
                
                if (parameterName.equals("messaggio")) {
                    messaggio = parameterValue;
                }
            }
        %>
        <% if (codiceEsito == 0) { %>
        <script>
			$( document ).ready(function() {
				setTimeout(function() {

					$reditrectUri = '#deposit-success';
					$(location).attr('href', $reditrectUri);
					
					$reditrectUri = 'enipay://deposit-success';
					$(location).attr('href', $reditrectUri);
					
				}, 2000);
			});
		</script>
		<% }
           else { %>
        <script>
			$( document ).ready(function() {
				setTimeout(function() {

					$reditrectUri = '#deposit-error?message=' + encodeURIComponent('<%=messaggio%>');
					$(location).attr('href', $reditrectUri);
					
					$reditrectUri = 'enipay://deposit-error?message=' + encodeURIComponent('<%=messaggio%>');
					$(location).attr('href', $reditrectUri);
					
				}, 2000);
			});
		</script>
        <% } %>
    </body> 
</html> 