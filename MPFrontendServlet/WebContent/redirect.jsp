<%@page import="com.techedge.mp.frontendservlet.validation.ValidationResponse"%>
<%@page import="com.techedge.mp.frontendservlet.validation.ValidationBodyRequest"%>
<%@page import="com.techedge.mp.frontendservlet.Validation"%>
<%@page import="com.techedge.mp.frontendservlet.Credential"%>
<%@page import="com.techedge.mp.frontendservlet.validation.ValidationRequest"%>
<%@page import="com.techedge.mp.frontendservlet.authentication.AuthenticationResponse"%>
<%@page
	import="com.techedge.mp.frontendservlet.authentication.AuthenticationBodyResponse"%>
<%@page import="com.techedge.mp.frontendservlet.authentication.AuthenticationRequest"%>
<%@page import="com.techedge.mp.frontendservlet.Authentication"%>
<%@page
	import="com.techedge.mp.frontendservlet.authentication.AuthenticationBodyRequest"%>
<%@page import="com.techedge.mp.frontendservlet.http.HttpResponse"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.techedge.mp.frontendservlet.http.HttpJSONClient"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
 	
 	String type             = request.getParameter("type");
 	String email            = request.getParameter("email");
 	String verificationCode = request.getParameter("verificationCode");
 	String scheme 			= request.getScheme();
 	String serverName 		= request.getServerName();
 	int serverPort 			= request.getServerPort();
 	String servicePath 		= "/MPFrontendAdapter/UserJson";
 	String url 				= scheme + "://" +serverName + ":" + serverPort + servicePath;
 	String urlActivationOK	= "/MPFrontendServlet/activation_page/activation_ok.html";
 	String urlActivationKO 	= "/MPFrontendServlet/activation_page/activation_ko.html";
 	
	//System.clearProperty("http.proxyHost");
	//System.clearProperty("http.proxyPort");
	System.setProperty("http.proxyHost", "mpsquid.enimp.pri");
	System.setProperty("http.proxyPort", "3128");  	
 	
 	Authentication authentication = new Authentication();
 	AuthenticationBodyRequest authenticationBodyRequest = new AuthenticationBodyRequest();
 	AuthenticationRequest authenticationRequest = new AuthenticationRequest();
 	authentication.setUsername("MPSystemUser");
 	authentication.setPassword("3zYGDSewI+6PCWLX8jfacA==");
 	authentication.setRequestID("WEB-100000000000");
 	authentication.setDeviceID("web");
 	authentication.setDeviceName("WEB");
 	authenticationBodyRequest.setBody(authentication);
 	authenticationRequest.setAuthentication(authenticationBodyRequest);
	
 	HttpJSONClient httpClient = new HttpJSONClient();
 	HttpResponse authenticationHttpResponse = httpClient.sendPost(url, new Gson().toJson(authenticationRequest));
	
	if (httpClient.isResponseOK(authenticationHttpResponse)) {
	  String jsonResponse = authenticationHttpResponse.getContent();
	  AuthenticationResponse authenticationResponse = new AuthenticationResponse();
	  authenticationResponse = new Gson().fromJson(jsonResponse, AuthenticationResponse.class);
	  if (authenticationResponse.getStatus().getStatusCode().equals("USER_AUTH_200")) {
	    Credential credential = new Credential();
	    Validation validation = new Validation();
	    ValidationRequest validationRequest = new ValidationRequest();
	    ValidationBodyRequest validationBodyRequest = new ValidationBodyRequest();
	    credential.setTicketID(authenticationResponse.getBody().getTicketID());
	    credential.setRequestID(authentication.getRequestID());
	    validation.setType(type);
	    validation.setVerificationCode(verificationCode);
	    validation.setVerificationField(email);
	    validationBodyRequest.setBody(validation);
	    validationBodyRequest.setCredential(credential);
	    validationRequest.setValidateField(validationBodyRequest);
	    HttpResponse validationHttpResponse = httpClient.sendPost(url, new Gson().toJson(validationRequest));
	    
	    if (httpClient.isResponseOK(validationHttpResponse)) {
	   	  jsonResponse = validationHttpResponse.getContent();
	   	  ValidationResponse validationResponse = new ValidationResponse();
	   	  validationResponse = new Gson().fromJson(jsonResponse, ValidationResponse.class);
	   	  
	   	  if (validationResponse.getStatus().getStatusCode().equals("USER_CHECK_200")) {
	   	    response.sendRedirect(urlActivationOK);  
	   	  }
	   	  else {
	   	   response.sendRedirect(urlActivationKO);
	   	  }
	   	}
	   	else {
	   	 response.sendRedirect(urlActivationKO);
	   	}
	  }
	  else {
	      response.sendRedirect(urlActivationKO);
	  }
	}
	else {
	    response.sendRedirect(urlActivationKO); 
	}
%>		
		
		</script>
</body>
</html>
