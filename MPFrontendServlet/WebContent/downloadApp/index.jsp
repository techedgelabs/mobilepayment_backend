<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.xml.bind.DatatypeConverter" %>
<%@ page import="javax.servlet.http.HttpServletResponse.*" %>
<%
	final String USERNAME = "betatester";
	final String PASSWORD = "qxiE7goAd";
	
	Boolean authFailed = Boolean.TRUE;
	
	String authHeader = request.getHeader("authorization");

	if (authHeader != null && !authHeader.isEmpty()) {

	    //out.println("Authheader: <em>" + authHeader);
	    
		String encodedValue = authHeader.split(" ")[1];
		
		//out.println("Base64-encoded Authorization Value: <em>" + encodedValue);
		
		if (encodedValue != null && !encodedValue.isEmpty()) {
		
			// Decode data 
	        String decodedValue = new String(DatatypeConverter.parseBase64Binary(encodedValue));
			
			//out.println("</em><br/>Base64-decoded Authorization Value: <em>" + decodedValue);
			
			String[] credentials = decodedValue.split(":");
			
			if ( credentials != null && credentials.length == 2) {
			    
			    String username = credentials[0];
			    String password = credentials[1];
			    
			    //out.println("</em><br/>username: <em>" + username);
			    //out.println("</em><br/>password: <em>" + password);
				
			    if (username.equals(USERNAME) && password.equals(PASSWORD)) {
			        
			        authFailed = Boolean.FALSE;       
			    }
			}
		}
		
		//out.println("</em>");
	}
	
	if ( authFailed ) {
	    
	    response.setHeader("WWW-Authenticate", "Basic realm=\"Site Administration Area\"");
	    response.setHeader("Status", "401 Unauthorized");
	    response.setHeader("HTTP-Status", "401 Unauthorized");
	    //response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);      
	    //response.getWriter().println("401 Unauthorized");
	    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
	}
	
%>

<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="UTF-8">
	<title>App Download</title>
	<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
	<!-- Meta tags that set up the page as a mobile page   -->
	<!-- <meta name = "viewport" content = "user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width"> -->
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono" rel="stylesheet">

	<script type="text/javascript">

		function showWPDialog(){
			$('#wpdialog').toggleClass('visible');
		}
		(function($){

			$(document).ready(function(){
				$('.osrow').on('click', function(){
					$(this).find('a.link').click();
				});

				var app, android, ios, wp;
				var today = new Date();
				var url  = './appdata.json';

				$.ajax({ 
					type: 'GET', 
					url: url, 
					data: { get_param: today.getTime() }, 
					dataType: 'json',
					success: function (data) { 
						//console.log(JSON.stringify(data));
						app = data[0].app;
						android = data[0].android;
						ios = data[0].ios;
						wp = data[0].windowsphone;


						$('.mainColor_bkg').css({'background-color': app.maincolor});
						$('.mainColor_color').css({'color': app.maincolor});
						$('a').css({'color': app.maincolor});
						$('#mainTable tr').css({'border': '10px solid '+app.maincolor});

						$('#today').html(today);
						$('#time').html( today.getHours() + ':' + today.getMinutes());
						$('#date').html( today.toDateString() );

						$('#android .logo').append('<img src="'+android.icon+'" class="logoicon" width="50px" height="auto" />');
						$('#android .link').append('<a href="'+android.link+'" class="mainColor_color">Download android app</a>');
						$('#android .version').append(android.version);

						$('#ios .logo').append('<img src="'+ios.icon+'" class="logoicon" width="50px" height="auto" />');
						$('#ios .link').append('<a href="'+ios.link+'" class="mainColor_color">Download iOS app</a>');
						$('#ios .version').append(ios.version);

						$('#wp .logo').append('<img src="'+wp.icon+'" class="logoicon" width="50px" height="auto" />');
						$('#wp .link').append('<a href="#" onclick="showWPDialog()" class="mainColor_color">Download Windows Phone app</a>');
						$('#wp .version').append(wp.version);

						$('#wpdialog .qr').attr('src', wp.qr);
						$('#wpdialog .link').append('<p class="mainColor_color">'+wp.link+'</p>');
					},
					error: function(data){
						console.log('ERROR:');
						//console.log(JSON.stringify(data));
					}
				});
			});
		})(jQuery); 
	</script>
</head>
<body class="TMPdownload mainColor_bkg">
	<!--HEADER-->
	<header class="mainColor_bkg">
		<p>Download <b>Eni Station + Beta</b> app</p>
	</header>

	
	<table id="mainTable">
		<tr class="osrow" align="center" id="android">
			<td class="mainColor_color logo left"></td>
			<td class="mainColor_color link center"></td>
			<td class="mainColor_color version right"></td>
		</tr>
		<tr class="osrow" align="center" id="ios">
			<td class="mainColor_color logo left"></td>
			<td class="mainColor_color link center"></td>
			<td class="mainColor_color version right"></td>
		</tr>
		<tr class="osrow" align="center" id="wp">
			<td class="mainColor_color logo left"></td>
			<td class="mainColor_color link center"></td>
			<td class="mainColor_color version right"></td>
		</tr>
	</table>


	<!--FOOTER-->
	<footer class="col-xs-12 text-center dateFooter">
		<p id="time"></p>
		<p id="date"></p>
	</footer>
	<div id="wpdialog">
		<div class="container">
			<div class="inner">
				<img src="" alt="" class="qr">
				<br>
				<p class="link"></p>
				<a class="chiudi" href="#chiudi" onclick="showWPDialog()">Chiudi</a>
			</div>
		</div>
	</div>

</body>
<style>
html, body{
	min-height:100vh;
	font-family: 'Roboto';
	font-size:20px;
	line-height:150%;
	margin:0 auto;
}
a{
	/*color:#a8d44e;*/
	color: inherit !important;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
	text-decoration:none;
}
a:hover{
	text-decoration:underline;
}
header{
	padding:30px;
	text-align: center;
	color:#fff;
	background-color: #f8f8f8;
	font-size:48px;
	line-height:150%;
}
#mainTable{
	padding:40px;
	width:100%;
	min-height:60vh;
	max-width: 600px;
	margin: 20px auto;
    border-collapse: collapse;
}
#mainTable tr{
	background-color:#f8f8f8;
	border-radius: 5px !important;
   /* border-bottom:10px solid #fff;
    border-top:10px solid #fff;*/
}
#mainTable tr td{
	padding:20px;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
    text-align:left;
    border-collapse: collapse;
}
#mainTable tr td.left{width:25%;}
#mainTable tr td.center{width:50%;}
#mainTable tr td.right{width:25%;}

#wpdialog{
	position:fixed;
	top:0;
	left:0;
	opacity: 0;
	display: none;
	width:100vw;
	height:100vh;
	background-color:rgba(0,0,0, 0.5);
}
#wpdialog.visible{
	opacity: 1;
	display: block;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
#wpdialog .container{
	width: 80vw;
	height:80vh;
	top:5vh;
	left:10vw;
	position: absolute;
	background-color:#f8f8f8;
	border-radius:10px;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}

#wpdialog .container .inner{
	padding:50px;
	text-align:center;
}

#wpdialog .container .inner .qr{
	margin:0 auto;
	max-width:80%;
	height:auto;
}

#wpdialog .container .inner .chiudi{
	position:absolute;
	top:25px;
	right:25px;
	font-size:14px;
	text-transform: uppercase;
}
footer{
	background-color:#f8f8f8;
	color:#000;
	text-align: center;
	padding:50px;
}
@media(max-width:680px){
	header{
		padding:15px;
		font-size:36px;
	}

	#mainTable{
		padding:10px;
		margin: 20px auto;
		max-width: 90%;
	}
	#mainTable tr td{
		padding:15px;
		font-size:16px;
	}
}
</style>
</html>