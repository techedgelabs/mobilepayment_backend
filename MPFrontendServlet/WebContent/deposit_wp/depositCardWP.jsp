<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	String serverName = request.getServerName();
	String urlPrefix = "test";
	if ( serverName.equals("bemp-int.enimp.pri") ) {
		urlPrefix = "";
	}
	
    String PARes = request.getParameter("PaRes");
	
	String encString   = "";
	String transKey    = "";
	String shopLogin   = "";
	String mailAddress = "";
	
	Cookie cookie = null;
	Cookie[] cookies = null;
	
	if (PARes != null && PARes.length() > 0) {
		
		// Ottieni l'array dei cookies associati al dominio
		cookies = request.getCookies();
		
		if( cookies != null ) {
			
			for (int i = 0; i < cookies.length; i++) {
				
				// Leggi il cookie
				cookie = cookies[i];
				
				if ( cookie.getName().equals("encString") ) {
					
					// Assegna il valore del cookie alla variabile corrispondente
					encString = cookie.getValue();
					
					// Cancella il cookie
					cookie.setValue(null);
					cookie.setMaxAge(-1);
		            response.addCookie(cookie);
				}
				
				if ( cookie.getName().equals("transKey") ) {
					
					// Assegna il valore del cookie alla variabile corrispondente
					transKey = cookie.getValue();
					
					// Cancella il cookie
					cookie.setValue(null);
					cookie.setMaxAge(-1);
		            response.addCookie(cookie);
				}
				if ( cookie.getName().equals("shopLogin") ) {
					
					// Assegna il valore del cookie alla variabile corrispondente
					shopLogin = cookie.getValue();
					
					// Cancella il cookie
					cookie.setValue(null);
					cookie.setMaxAge(-1);
		            response.addCookie(cookie);
				}
				if ( cookie.getName().equals("mailAddress") ) {
					
					// Assegna il valore del cookie alla variabile corrispondente
					mailAddress = cookie.getValue();
					
					// Cancella il cookie
					cookie.setValue(null);
					cookie.setMaxAge(-1);
		            response.addCookie(cookie);
				}
			}
		}
	}
	else {
		PARes = "";
	}
 %>

<html lang="it">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Deposito carta di credito</title>
<!--script type="text/javascript" src="https://testecomm.sella.it/Pagam/JavaScript/js_GestPay.js"></script-->
<script type="text/javascript"
	src="https://<%=urlPrefix%>ecomm.sella.it/Pagam/JavaScript/js_GestPay.js"></script>
<link href="<%=request.getContextPath()%>/deposit_wp/css/style.css"
	rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">


<script type="text/javascript">
        //valorizzati solo dopo il redirect del 3dSecure
		var shopLogin = '<%= shopLogin %>';
		var encString = '<%= encString %>';
		var PARes = '<%= PARes %>';
		var transKey = '<%= transKey %>';
		var mailAddress = '<%= mailAddress %>';

		//declaring local object to handle asynchronous responses from the payment page 
        var LocalObj = {}
            //setting up a function to handle asynchronous security check result after creating the iFrame and loading the payment page
        	LocalObj.PaymentPageLoad = function (Result) {
                //check for errors, if the Result.ErroCode is 10 the iFrame is created correctly and the security check are passed 

                showLoading(false);
                if (Result.ErrorCode == 10) {
                    //iFrame created and and the security check passed 
                    //now we can show the form with the credit card fields
                    //Handle 3D authentication 2nd call

                    //Parametri provenienti dalla post  


                    if (PARes.length > 0) {
                        GestPay.SendPayment({PARes: PARes, TransKey: transKey}, LocalObj.PaymentCallBack);
                        doPolling(true);
                        showLoading(true);
                    } else {
                        CheckCC();
                    }
                } else {
                    //An error has occurred, check the Result.ErrorCode and Result.ErrorDescription 
                    //place error handle code HERE

                    onDepositError(Result.ErrorCode, Result.ErrorDescription);

                }
            }
            //setting up a function to handle payment result
        LocalObj.PaymentCallBack = function (Result) {

            showLoading(false);
            try {

                if (Result.ErrorCode == 0) {
                    //Transaction correctly processed
                    //Decrypt the string to read the Transaction Result
                    onDepositSuccess();
                } else {
                    //An error has occurred
                    //check for 3D authentication required
                    if (Result.ErrorCode == 8006) {
                        //The credit card is enrolled we must send the card holder to the authentication page on the issuer website
                        //Get the TransKey, IMPORTANT! this value must be stored for further use
                        var transKey = Result.TransKey
                            //put the TransKey in a cookie for later use, the cookies will expire in 20 minutes     
                        var date = new Date();
                        date.setTime(date.getTime() + (1200000));
                        document.cookie = 'transKey=' + transKey.toString() + '; expires=' + date.toGMTString() + ' ; path=/';
                        document.cookie = 'encString=' + encString + '; expires=' + date.toGMTString() + ' ; path=/';
                        document.cookie = 'shopLogin=' + shopLogin + '; expires=' + date.toGMTString() + ' ; path=/';
                        document.cookie = 'mailAddress=' + mailAddress + '; expires=' + date.toGMTString() + ' ; path=/';

                        //Get the VBVRisp encrypted string required to access the issuer authentication page
                        var VBVRisp = Result.VBVRisp
                            //redirect the user to the issuer authentication page
                        var a = shopLogin;
                        var b = VBVRisp;
                        var c = document.location.href; //this is the landing page where the user will be redirected after the issuer authentication must be ABSOLUTE
                        //var AuthUrl = 'https://testecomm.sella.it/pagam/pagam3d.aspx'; //TESTCODES
                        var AuthUrl = 'https://<%=urlPrefix%>ecomm.sella.it/pagam/pagam3d.aspx'; //PRODUCTION
                        document.location.replace(AuthUrl + '?a=' + a + '&b=' + b + '&c=' + c);
                    } else {
                        onDepositError(Result.ErrorCode, Result.ErrorDescription);

                    }
                }
            }catch(err){
                onDepositError(500,err);   
            }
        }

        //Send data to GestPay and process transaction
        function CheckCC() {

            try {

                showLoading(true);

                GestPay.SendPayment({
                    CC: document.CCForm.CC.value,
                    EXPMM: document.CCForm.EXPMM.value,
                    EXPYY: document.CCForm.EXPYY.value,
                    CVV2: document.CCForm.CVV2.value,
                    Name: document.CCForm.Name.value,
                    Email: document.CCForm.Email.value
                }, LocalObj.PaymentCallBack);

            } catch (err) {
                onDepositError(500,err);
            }

            return false;

        }

        function initPaymentPage(_shopLogin, _encString, _mailAddress) {

            shopLogin = _shopLogin;
            encString = _encString;
            document.CCForm.Email.value = _mailAddress;
            mailAddress = _mailAddress;
            //Browser enabled
            //Creating the iFrame
            GestPay.CreatePaymentPage(shopLogin, encString, LocalObj.PaymentPageLoad);
            showLoading(true);
        }


        function onDepositSuccess() {
            WPNotify("SUCCESS");
        }

        function onDepositError(errCode, errMessage) {
            WPNotify("DEPOSIT_METHOD_" + errCode + "|||" + errMessage);
        }
        
        function doPolling(bVal){
            if (bVal)
                WPNotify("START_POLLING");
            else
                WPNotify("STOP_POLLING");
        }

        function showLoading(show) {
            if (show)
                WPNotify("SHOW_LOADING");
            else
                WPNotify("DISMISS_LOADING");
        }


        function onRequestCardDeposit() {
            WPNotify("DEPOSIT_REQUEST");
            return false;
        }

        function initPage() {

            if (BrowserEnabled) {
                //Handle after 3D authentication second call
                if (PARes.length > 0) {
                    initPaymentPage(shopLogin, encString, mailAddress);
                }
            } else {
                //Browser not supported
                //Place error handle code here
                onDepositError(9991, "Browser not supported");
            }
        }

        function WPNotify(message) {
            window.external.notify(message);
        }
    </script>

</head>

<body onload="initPage()" style="margin: 0px;">
	<div class="container">
		<!-- Credit card form -->
		
		<div id="form-container">

			<br />
			
			<form name="CCForm" method="post" id="CCForm"
				OnSubmit="return onRequestCardDeposit();" class="On">
				<div id="Fields">
					<div id="CCField">
						<label for="CC" style="font-family: open_sansregular; font-size: 15px; max-width: 100%; margin-bottom: 5px; font-weight: 700;">Numero carta</label><br />
						<input type="number" name="CC" id="CC" autocomplete="off" maxlength="19" style="margin-top: 10px; margin-bottom: 10px; width: 100%; line-height: 19px; border: none; border-bottom: 1px solid #333333;" />
					</div>
					<div id="NameField" style="margin-top: 16px;">
						<label for="Name" style="font-family: open_sansregular; font-size: 15px; max-width: 100%; margin-bottom: 5px; font-weight: 700;">Nome titolare carta</label><br />
						<input type="text" name="Name" id="Name" value="" style="margin-top: 10px; margin-bottom: 10px; width: 100%; line-height: 19px; border: none; border-bottom: 1px solid #333333;" />
						<p class="eni-font-small" style="margin-top: -6px; font-size: 50%;">cos� come appare sulla carta</p>
					</div>
					<div id="ExpDate" style="margin-top: 16px;">
						<label style="font-family: open_sansregular; font-size: 15px; max-width: 100%; margin-bottom: 5px; font-weight: 700;">Data di scadenza</label><br />
						<select name="EXPMM" id="EXPMM">
							<!-- <option value="" selected>--</option>-->
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select>
						<select name="EXPYY" id="EXPYY">
							<option value="15">2015</option>
							<option value="16">2016</option>
							<option value="17">2017</option>
							<option value="18">2018</option>
							<option value="19">2019</option>
							<option value="20">2020</option>
							<option value="21">2021</option>
							<option value="22">2022</option>
							<option value="23">2023</option>
							<option value="24">2024</option>
							<option value="25">2025</option>
							<option value="26">2026</option>
							<option value="27">2027</option>
							<option value="28">2028</option>
							<option value="29">2029</option>
							<option value="30">2030</option>
							<option value="31">2031</option>
							<option value="32">2032</option>
							<option value="33">2033</option>
							<option value="34">2034</option>
							<option value="35">2035</option>
							<option value="36">2036</option>
							<option value="37">2037</option>
							<option value="38">2038</option>
							<option value="39">2039</option>
							<option value="40">2040</option>
						</select>
					</div>
					<div id="CCVField" style="margin-top: 16px;">
						<label style="font-family: open_sansregular; font-size: 15px; max-width: 100%; margin-bottom: 5px; font-weight: 700;">Codice di sicurezza</label><br />
						<input type="password" name="CVV2" id="CVV2" maxlength="4" style="margin-top: 10px; margin-bottom: 10px; width: 100%; line-height: 19px; border: none; border-bottom: 1px solid #333333;" />
						<p class="eni-font-small" style="margin-top: -6px; font-size: 50%;">Inserire il valore riportato sul retro della carta</p>
					</div>
					
					<div id="EmailField">
						<input type="hidden" name="Email" id="Email" value="" style="margin-top: 10px; margin-bottom: 10px; width: 100%; line-height: 19px; border: none; border-bottom: 1px solid #333333;" />
					</div>
	
					<input type="submit" class="btn btn-primary btn-block eni-font"	value="Deposita" id="submit" style="width: 100%; border: none; margin-top: 20px; font-size: 120%;" />
					
				</div>
			</form>
		</div>
	</div>

</body>

</html>