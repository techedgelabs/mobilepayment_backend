//CONFIGURA LO STILE DELLE STELLE
function setUpSelect() {
    
    $('div.stars select').barrating({
        theme: 'fontawesome-stars',
        //showValues: true,
        showSelectedRating: false,
        initialRating: null,
        hoverState: true,
        deselectable: false,
        allowEmpty: false
    });
}

var values = [];
$('div.stars select').each(function() {
    $this = $(this);
    values[$this.attr('id')] = $this.val();
});

$('div.stars select').change(function() {
    $this = $(this);
    values[$this.attr('id')] = $this.val();
    $enable_button = true;
    for (var i in values) {
        if(values[i] == '') {
            console.log('EMPTY');
            $enable_button = false;
        }
    }
    if($enable_button==true) {
        $('#output_uno').prop('disabled', false);
        $('#output_uno').css('background-color', '#2d2d28');
        $('#output_uno').css('color', '#ffffff');
        $('#output_uno').css('cursor', 'pointer');
    }else{
        $('#output_uno').prop('disabled', true);
        $('#output_uno').css('background-color', '#f1f1f1');
        $('#output_uno').css('color', '#ffffff');
        $('#output_uno').css('cursor', 'pointer');
    }
});

//creo json quando clicca su button Invia e scrivo in console
function sendSurvey() {

    var viewData = { 
        answers : [] 
    };
    
    var data_console = {};
    for(var i in values) {
        data_console[i] = values[i];
    }
    viewData.answers.push(data_console);

    var json = JSON.stringify(data_console);

    $path_survey = $('#survey_path').val();
    $key = $('#key').val();

    $.ajax({
        url: $path_survey,
        dataType: 'json',
        type: 'POST',
        data: { key: $key, answers: json },
        success: function (result) {

            if ( result != null && result['status']['statusCode'] == 'SURVEY_SUBMIT_SURVEY_200' ) {
                $('#star-content').hide();
                $('#header-star-content').hide();
                $('#submitted').show();
            } else {
                
                if(result != null && result['status']['statusCode'] == 'SURVEY_SUBMIT_SURVEY_302') {
                    $('.message_space_line').text('Questionario già compilato.');
                    
                } else {
                    $('.message_space_line').text('Si è verificato un errore durante l\'invio del questionario.<br>Ti invitiamo a riprovare.');
                                        
                }
                $('#star-content').hide();
                $('#header-star-content').hide();
                $('#submitted-ko').show();
            }
        },
        error: function (event, xhr, options, exc) {
            // $('#star-content').hide();
            // $('#header-star-content').hide();
            $('#submitted-ko').fadeIn("fast");
        },
        complete: function (reservations) {
        },
    });

    // commentato perchè apre una nuova scheda dove stampa json
    //var url = 'data:text/json;charset=utf8,' + encodeURIComponent(json);
    //window.open(url, '_blank');
    //window.focus();

    //$('#submitted').show(); //mostro sezione di ringraziamento
    //$('#star-content').hide(); //nascondo sezione index
}

// //funzione che aggiorna link del bottone salva - !!!! da eliminare se non serve il salva
function updateLink() {
    var data = {"Domanda 1": val_uno, "Domanda 2": val_due,"Domanda 3": val_tre}; //oggetto json

    var json = JSON.stringify(data); // stringify dell'oggetto json per il salvataggio
    var blob = new Blob([json], {type: "application/json"});
    var url  = URL.createObjectURL(blob);

    var a = document.createElement('a');
    a.download    = "backup.json";
    a.href        = url;
    a.textContent = "Salva Json";
    $('#output_due').children().remove(); //aggiorno l'url
    $('#output_due').append(a); //aggiorno l'url
};

function resetAll(){
    $('#domanda_uno').barrating('clear'); //Resetta le bar
    $('#domanda_due').barrating('clear'); //Resetta le bar
    $('#domanda_tre').barrating('clear'); //Resetta le bar
    $('#domanda_uno').barrating('set', 0);
    $('#domanda_due').barrating('set', 0);
    $('#domanda_tre').barrating('set', 0);

    $('#submitted').hide();	
    setUpSelect();
    val_uno= 0;
    val_due= 0;
    val_tre= 0;
    data = null;
}

//Controlla se è stato dato il punteggio a tutte e tre le domande
/*function checkRating(){
    if (val_uno && val_due && val_tre != 0){
        console.log('button enabled');

        $('#output_uno').prop("disabled",false); //riabilita il pulsante Invia
        $('#output_uno').css({
            'background-color': '#333'
        });
        $('#output_due').click(function(){
            resetAll();
        });
    }
}*/
//salvo file quando clicca su download
$(document).ready(function(){
    setUpSelect();
    $('#output_uno').prop("disabled",true); //disabilita il bottone Invia finche non è selezionata la valutazione

    resetAll();
});
