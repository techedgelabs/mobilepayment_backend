<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	String serverName = request.getServerName();
	String urlPrefix = "test";
	if ( serverName.equals("bemp-int.enimp.pri") ) {
		urlPrefix = "";
	}
	
    String PARes = request.getParameter("PaRes");
	
	String encString   = "";
	String transKey    = "";
	String shopLogin   = "";
	String mailAddress = "";
	
	Cookie cookie = null;
	Cookie[] cookies = null;
	
	if (PARes != null && PARes.length() > 0) {
		
		// Ottieni l'array dei cookies associati al dominio
		cookies = request.getCookies();
		
		if( cookies != null ) {
			
			for (int i = 0; i < cookies.length; i++) {
				
				// Leggi il cookie
				cookie = cookies[i];
				
				if ( cookie.getName().equals("encString") ) {
					
					// Assegna il valore del cookie alla variabile corrispondente
					encString = cookie.getValue();
					
					// Cancella il cookie
					cookie.setValue(null);
					cookie.setMaxAge(-1);
		            response.addCookie(cookie);
				}
				
				if ( cookie.getName().equals("transKey") ) {
					
					// Assegna il valore del cookie alla variabile corrispondente
					transKey = cookie.getValue();
					
					// Cancella il cookie
					cookie.setValue(null);
					cookie.setMaxAge(-1);
		            response.addCookie(cookie);
				}
				if ( cookie.getName().equals("shopLogin") ) {
					
					// Assegna il valore del cookie alla variabile corrispondente
					shopLogin = cookie.getValue();
					
					// Cancella il cookie
					cookie.setValue(null);
					cookie.setMaxAge(-1);
		            response.addCookie(cookie);
				}
				if ( cookie.getName().equals("mailAddress") ) {
					
					// Assegna il valore del cookie alla variabile corrispondente
					mailAddress = cookie.getValue();
					
					// Cancella il cookie
					cookie.setValue(null);
					cookie.setMaxAge(-1);
		            response.addCookie(cookie);
				}
			}
		}
	}
	else {
		PARes = "";
	}
 %>

<html lang="it">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Deposito carta di credito</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/deposit/css/ripples.min.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/deposit/css/style.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/deposit/css/material-wfont.min.css"
	rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="<%=request.getContextPath()%>/deposit/js/jquery.maskedinput.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script
	src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>


<script src="<%=request.getContextPath()%>/deposit/js/ripples.min.js"></script>
<script src="<%=request.getContextPath()%>/deposit/js/material.min.js"></script>
<!--script type="text/javascript" src="https://testecomm.sella.it/Pagam/JavaScript/js_GestPay.js"></script-->
<!--script type="text/javascript" src="https://ecomm.sella.it/Pagam/JavaScript/js_GestPay.js"></script-->
<script type="text/javascript"
	src="https://<%=urlPrefix%>ecomm.sella.it/Pagam/JavaScript/js_GestPay.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">


<script type="text/javascript">


//valorizzati solo dopo il redirect del 3dSecure
var shopLogin = '<%= shopLogin %>';
var encString = '<%= encString %>';
var PARes = '<%= PARes %>';
var transKey = '<%= transKey %>';
var mailAddress = '<%= mailAddress %>';

//declaring local object to handle asynchronous responses from the payment page 
var LocalObj = {}
//setting up a function to handle asynchronous security check result after creating the iFrame and loading the payment page
LocalObj.PaymentPageLoad = function(Result){
  //check for errors, if the Result.ErroCode is 10 the iFrame is created correctly and the security check are passed 
  
  showLoading(false);

  if(Result == null){
    onDepositError("unknown error");
  }

  if(Result.ErrorCode == 10 ){
    //iFrame created and and the security check passed 
    //now we can show the form with the credit card fields
    //Handle 3D authentication 2nd call
    
//Parametri provenienti dalla post  
    

    if (PARes.length > 0){
      GestPay.SendPayment({PARes:PARes,TransKey:transKey},LocalObj.PaymentCallBack);
      showLoading(true);     
    }else{
      CheckCC();
    }
  }else{
    //An error has occurred, check the Result.ErrorCode and Result.ErrorDescription 
    //place error handle code HERE
    
    onDepositError('Error:' + Result.ErrorCode + ' ' + Result.ErrorDescription);

  }             
}
//setting up a function to handle payment result
LocalObj.PaymentCallBack = function (Result){

  showLoading(false);

  if(Result == null){
    onDepositError("unknown error");
  }

  if(Result.ErrorCode == 0 ){
    //Transaction correctly processed
    //Decrypt the string to read the Transaction Result
    onDepositSuccess();
  }else{
    //An error has occurred
    //check for 3D authentication required
    if(Result.ErrorCode == 8006){
      //The credit card is enrolled we must send the card holder to the authentication page on the issuer website
      //Get the TransKey, IMPORTANT! this value must be stored for further use
      var transKey = Result.TransKey
      //put the TransKey in a cookie for later use, the cookies will expire in 20 minutes     
      var date = new Date();
      date.setTime(date.getTime()+(1200000));
      document.cookie = 'transKey='+transKey.toString()+'; expires='+ date.toGMTString() +' ; path=/';
      document.cookie = 'encString='+encString+'; expires='+ date.toGMTString() +' ; path=/';  
      document.cookie = 'shopLogin='+shopLogin+'; expires='+ date.toGMTString() +' ; path=/';  
      document.cookie = 'mailAddress='+mailAddress+'; expires='+ date.toGMTString() +' ; path=/';  

      //Get the VBVRisp encrypted string required to access the issuer authentication page
      var VBVRisp = Result.VBVRisp
      //redirect the user to the issuer authentication page
      var a = shopLogin; 
      var b = VBVRisp;
      var c= document.location.href; //this is the landing page where the user will be redirected after the issuer authentication must be ABSOLUTE
      //var AuthUrl = 'https://testecomm.sella.it/pagam/pagam3d.aspx'; //TESTCODES
      var AuthUrl = 'https://<%=urlPrefix%>ecomm.sella.it/pagam/pagam3d.aspx'; //PRODUCTION
      document.location.replace(AuthUrl+'?a='+a+'&b='+b+'&c='+c);
    }else{

       onDepositError('Error:' + Result.ErrorCode +' - ' + Result.ErrorDescription);
    
    }
  }
}

//Send data to GestPay and process transaction
function CheckCC(){

  try{

    showLoading(true);


    GestPay.SendPayment ({
     CC :    $('#CC').val(),
     EXPMM : $('#EXPMMYY').val().split("/")[0],
     EXPYY : $('#EXPMMYY').val().split("/")[1],
     CVV2:   $('#CVV2').val(),
     Name:   $('#Name').val(),
     Email:  $('#Email').val()
   },LocalObj.PaymentCallBack);

  }catch(err) {
   // alert(err);
  }

  return false;

}

function initPaymentPage(_shopLogin,_encString,_mailAddress){

  shopLogin = _shopLogin;
  encString = _encString;
  mailAddress = _mailAddress;

  $('#Email').val(_mailAddress);
  //Browser enabled
  //Creating the iFrame
  GestPay.CreatePaymentPage(shopLogin,encString,LocalObj.PaymentPageLoad);
  showLoading(true);
}


function onDepositSuccess(){
  try{
    Android.onDepositSuccess();
  }catch(err){
    //alert("DEPOSIT SUCCESS");
     window.location = "depositSuccess:";
  }

   


}

function onDepositError(message){
   try{
    Android.onDepositError(message);
  }catch(err){
     window.location = "depositError:" + message;
    //alert("DEPOSIT ERROR " + message);
  }
}

function showLoading(show){
  
  if(show){
      $('#loading-container').show();
      $('#form-container').hide();
    }else{
      $('#loading-container').hide();
      $('#form-container').show();
  }

  try{
    Android.showLoading(show);
  }catch(err){
     window.location = "showLoading:" + show;
  }

 

  
}


function onRequestCardDeposit(){
   
 if($("#CCForm").valid()){
 
   try{
     Android.onRequestCardDeposit();
    }catch(err){
       window.location = "depositRequest:";
  }

 }
  return false;

}



$( document ).ready(function() {

// $.material.init();
  
  if(BrowserEnabled){
    //Handle after 3D authentication second call
   if(PARes.length > 0){
      initPaymentPage(shopLogin,encString,mailAddress);
   } 
}else{
  //Browser not supported
  //Place error handle code here
  showError('Error: Browser not supported');
  
}

    $('#CCForm').validate({ // initialize the plugin
        rules: {
            CC: {
                required: true,
                maxlength: 19,
            },
            EXPMMYY: {
                required: true,
                minlength: 5,
                maxlength: 5
            },
            CVV2: {
              required: true,
              maxlength: 4,
              minlength: 2
            },
            Name: {
              required: true
            }
        },

      messages: {
        CC: "Numero carta non valido",
        EXPMMYY: "Data di scadenza non valida",
        CVV2: "Codice di sicurezza non valido",
        Name: "Nome titolare carta non valido",
      },
      
      submitHandler: function(form) {
       //form.submit();
       return true;
      }
        
    });



});




</script>

</head>
<body>
	<div class="container">
		<!-- Credit card form -->
		<div id="loading-container" style="display: none;">

			<h1 class="eni-font-title">Attendere prego...</h1>
			<p class="eni-font-title">Deposito della carta in corso</p>
			
		</div>
		<div id="form-container">

			<h1 class="eni-font-title">Deposito carta</h1>
			<br />

			<form name="CCForm" method="post" id="CCForm" class="form-horizontal"
				OnSubmit="return onRequestCardDeposit();">
				<div id="Fields">
					<div id="CCField" class="form-group">
						<label>Numero carta</label> <input class="form-control eni-font"
							type="text" name="CC" id="CC" autocomplete="off" maxlength="19" />
					</div>
					<div id="NameField" class="form-group">
						<label for="Name">Nome titolare carta</label> <input
							class="eni-font form-control" type="text" name="Name" id="Name"
							value="" autocomplete="off">
						<p class="eni-font-small">cosi come appare sulla carta</p>
					</div>
					<div id="CCField" class="form-group">
						<label>Data di scadenza</label> <input
							class="form-control eni-font" type="text" name="EXPMMYY"
							id="EXPMMYY" autocomplete="off" />
						<p class="eni-font-small">Inserire solo il mese e l'anno nel
							formato mm/aa</p>
					</div>

					<div id="CCVField" class="form-group">
						<label>Codice di sicurezza</label> <input
							class="eni-font form-control" type="password" name="CVV2"
							id="CVV2" maxlength="4" autocomplete="off" />
						<p class="eni-font-small">Inserire il valore riportato sul
							retro della carta</p>
					</div>

					<div id="EmailField" class="form-group">
						<input class="form-control" type="hidden" name="Email" id="Email"
							value="">
					</div>
				</div>

				<input type="submit" class="btn btn-primary btn-block eni-font"
					value="Deposita" id="submit" />

			</form>
		</div>
	</div>


</body>

<script type="text/javascript">
$(document).ready(function() {
	$("#EXPMMYY").mask("99/99",{placeholder:"mm/aa"});
});
</script>

</html>
