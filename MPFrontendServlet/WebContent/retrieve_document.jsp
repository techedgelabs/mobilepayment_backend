<%@page import="java.io.InputStream"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="javax.xml.bind.DatatypeConverter"%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="com.techedge.mp.frontendservlet.retrievedocument.RetrieveDocumentResponse"%>
<%@page import="com.techedge.mp.frontendservlet.retrievedocument.RetrieveDocumentBody"%>
<%@page import="com.techedge.mp.frontendservlet.retrievedocument.RetrieveDocumentResponseBody"%>
<%@page import="com.techedge.mp.frontendservlet.Credential"%>
<%@page import="com.techedge.mp.frontendservlet.retrievedocument.RetrieveDocumentRequest"%>
<%@page import="com.techedge.mp.frontendservlet.retrievedocument.RetrieveDocument"%>
<%@page import="com.techedge.mp.frontendservlet.authentication.AuthenticationResponse"%>
<%@page
	import="com.techedge.mp.frontendservlet.authentication.AuthenticationBodyResponse"%>
<%@page import="com.techedge.mp.frontendservlet.authentication.AuthenticationRequest"%>
<%@page import="com.techedge.mp.frontendservlet.Authentication"%>
<%@page
	import="com.techedge.mp.frontendservlet.authentication.AuthenticationBodyRequest"%>
<%@page import="com.techedge.mp.frontendservlet.http.HttpResponse"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.techedge.mp.frontendservlet.http.HttpJSONClient"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
 	
 	String documentId       = request.getParameter("documentId");
 	String scheme 			= request.getScheme();
 	String serverName 		= request.getServerName();
 	int serverPort 			= request.getServerPort();
 	String servicePath 		= "/MPFrontendAdapter/UserJson";
 	String url 				= scheme + "://" +serverName + ":" + serverPort + servicePath;
 	
	//System.clearProperty("http.proxyHost");
	//System.clearProperty("http.proxyPort");
	System.setProperty("http.proxyHost", "mpsquid.enimp.pri");
	System.setProperty("http.proxyPort", "3128");  	
 	
 	Authentication authentication = new Authentication();
 	AuthenticationBodyRequest authenticationBodyRequest = new AuthenticationBodyRequest();
 	AuthenticationRequest authenticationRequest = new AuthenticationRequest();
 	authentication.setUsername("MPSystemUser");
 	authentication.setPassword("3zYGDSewI+6PCWLX8jfacA==");
 	authentication.setRequestID("WEB-100000000000");
 	authentication.setDeviceID("web");
 	authentication.setDeviceName("WEB");
 	authenticationBodyRequest.setBody(authentication);
 	authenticationRequest.setAuthentication(authenticationBodyRequest);
	
 	HttpJSONClient httpClient = new HttpJSONClient();
 	HttpResponse authenticationHttpResponse = httpClient.sendPost(url, new Gson().toJson(authenticationRequest));
	
	if (httpClient.isResponseOK(authenticationHttpResponse)) {
	  	String jsonResponse = authenticationHttpResponse.getContent();
	  	AuthenticationResponse authenticationResponse = new AuthenticationResponse();
	  	authenticationResponse = new Gson().fromJson(jsonResponse, AuthenticationResponse.class);
	  	if (authenticationResponse.getStatus().getStatusCode().equals("USER_AUTH_200")) {
	    	Credential credential = new Credential();
	    
	    	RetrieveDocument retrieveDocument = new RetrieveDocument();
		    RetrieveDocumentRequest retrieveDocumentRequest = new RetrieveDocumentRequest();
		    RetrieveDocumentBody retrieveDocumentBody = new RetrieveDocumentBody();
		    retrieveDocumentBody.setDocumentId(documentId);
		    credential.setTicketID(authenticationResponse.getBody().getTicketID());
		    credential.setRequestID(authentication.getRequestID());
		    retrieveDocumentRequest.setBody(retrieveDocumentBody);
		    retrieveDocumentRequest.setCredential(credential);
		    retrieveDocument.setRetrieveDocument(retrieveDocumentRequest);
		    
		    HttpResponse retrieveDocumentHttpResponse = httpClient.sendPost(url, new Gson().toJson(retrieveDocument));
		    
		    if (httpClient.isResponseOK(retrieveDocumentHttpResponse)) {
		   	  	jsonResponse = retrieveDocumentHttpResponse.getContent();
		   		RetrieveDocumentResponse retrieveDocumentResponse = new RetrieveDocumentResponse();
		   		retrieveDocumentResponse = new Gson().fromJson(jsonResponse, RetrieveDocumentResponse.class);
		   	  
				if (retrieveDocumentResponse.getStatus().getStatusCode().equals("USER_RETRIEVE_DOCUMENT_200")) {
				    
				    String filename = retrieveDocumentResponse.getBody().getFilename();

                    String encodedString = retrieveDocumentResponse.getBody().getDocumentBase64();
                    
                    byte[] decodedBytes = DatatypeConverter.parseBase64Binary(encodedString);

                    //set headers
                    response.setHeader("Content-Length",            Integer.toString(decodedBytes.length));
                    response.setHeader("Content-Type",              "application/octet-stream");
                    response.setHeader("Content-Disposition",       "attachment;filename=\"" + filename + "\"");
                    response.setHeader("Content-Transfer-Encoding", "binary");

                    //response.
                    
                    //try {
                    	ServletOutputStream output = response.getOutputStream();
                    	InputStream input = new ByteArrayInputStream(decodedBytes);
                    
	                    //transfer input stream to output stream, via a buffer
	                    byte[] buffer = new byte[2048];
	                    int bytesRead;    
	                    while ((bytesRead = input.read(buffer)) != -1) {
                       		output.write(buffer, 0, bytesRead);
	                    }
                    //}
                    
                    
					//response.sendRedirect(urlActivationOK);  
				}
				else {
					
				}
			}
			else {
				
			}
		}
		else {
			
		}
	}
	else {
	}
%>		
</body>
</html>
