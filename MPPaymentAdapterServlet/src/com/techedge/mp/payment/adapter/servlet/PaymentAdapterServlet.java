package com.techedge.mp.payment.adapter.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.UserV2ServiceRemote;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.exceptions.ParameterNotFoundException;
import com.techedge.mp.payment.adapter.business.GSServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

/**
 * Servlet implementation class PaymentAdapterServlet
 */
@WebServlet("/PaymentAdapterServlet")
public class PaymentAdapterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PaymentAdapterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		
		System.out.println("Query string : " + request.getQueryString());
		
		response.setContentType("text/html");
 		response.setStatus(200);
		
		//String query = request.getQueryString();
		
		Map<String, String> query_pairs = new LinkedHashMap<String, String>();
	    String query = request.getQueryString();
	    
	    if ( query == null ) {
	    	out.println("Parametri obbligatori non presenti");
	    	out.close();
	    }
	    else {
		    String[] pairs = query.split("&");
		    for (String pair : pairs) {
		        int idx = pair.indexOf("=");
		        if ( idx != -1 ) {
		        	query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		        }
		        else {
		        	out.println("Query string non valida");
			 		out.close();
		        }
		    }
		    
		    String shopLogin     = null;
		    String encodedString = null;
		    
		    for ( String key : query_pairs.keySet() ) {
		    	
		    	if(key.equals("a")) {
		    	
		    		shopLogin = query_pairs.get(key);
		    		System.out.println("Shop Login : " + shopLogin);
		    		
		    	}
		    	
		    	if(key.equals("b")) {
			    	
		    		encodedString = query_pairs.get(key);
		    		System.out.println("EncodedString : " + encodedString);
		    		
		    	}
		        
		    }
		    
		    
		    if ( shopLogin != null && encodedString != null ) {
		    
			    // Ottieni il valore della secureString
		 		GSServiceRemote gsService = null;
		 		
		 		final Hashtable jndiProperties = new Hashtable();
		 		
		 		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		 		
		 		try {
		 			
		             final Context context = new InitialContext(jndiProperties);
		             
		             //final String jndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserService!com.techedge.mp.core.business.UserServiceRemote";
		             final String jndi = "java:app/MPPaymentAdapterEJB/GSService!com.techedge.mp.payment.adapter.business.GSServiceRemote";
		             Object o = context.lookup(jndi);
		             gsService = (GSServiceRemote)o;
		             
		         } catch (NamingException ex) {
		         	//ex.printStackTrace();
		         	//logger.log(Level.INFO, "Error in getRemoteObject()");
		         }
		 		
		 		GestPayData gestPayData = gsService.decryptString(shopLogin, encodedString);
		 		
		 		if ( gestPayData != null ) {
		 		
			 		String secureString = gestPayData.getTransactionKey();
			 		
			 		UserServiceRemote userService = null;
					
					try {
						
			            final Context context = new InitialContext(jndiProperties);
			            
			            final String jndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserService!com.techedge.mp.core.business.UserServiceRemote";
			            Object o = context.lookup(jndi);
			            userService = (UserServiceRemote)o;
			            
			        } catch (NamingException ex) {
			        	//ex.printStackTrace();
			        	//logger.log(Level.INFO, "Error in getRemoteObject()");
			        }
					
					System.out.println("TDLevel: " + gestPayData.getTdLevel());
					
					String result = userService.updateUserPaymentData(
							gestPayData.getTransactionType(),
							gestPayData.getTransactionResult(),
							gestPayData.getShopTransactionID(),
							gestPayData.getBankTransactionID(),
							gestPayData.getAuthorizationCode(),
							gestPayData.getCurrency(),
							gestPayData.getAmount(),
							gestPayData.getCountry(),
							gestPayData.getBuyerName(),
							gestPayData.getBuyerEmail(),
							gestPayData.getErrorCode(),
							gestPayData.getErrorDescription(),
						    gestPayData.getAlertCode(),
						    gestPayData.getAlertDescription(),
						    gestPayData.getTransactionKey(),
						    gestPayData.getToken(),
						    gestPayData.getTokenExpiryMonth(),
						    gestPayData.getTokenExpiryYear(),
						    gestPayData.getCardBIN(),
						    gestPayData.getTdLevel());
		 		}
		 		else {
		 			System.out.println("Errore di connessione al servizio remoto di mobile payment");
		 		}
		    }
		    else {
		    	System.out.println("Query string non valida");
		    }
	 		
	 		response.setContentType("text/html");
	 		response.setStatus(200);
	 		
	 		out.println("<html><head><title>Response</title></head><body><h1>OK</h1></body></html>");
	 		out.close();
	 		
	    }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	    System.out.println("Start");
	    
	    PrintWriter out = response.getWriter();
        response.setContentType("text/plain");
        
        String session_id = "";
        String regione = "";
        String tipoTransazione = ""; // VBV_FULL
        String data = ""; // 20170721
        String mac = ""; // 360a98ae1216c5015615f8b292f05c1e30e07245
        String codiceConvenzione = ""; // 00000318104
        String tipoProdotto = ""; // VISA CLASSIC - CREDIT - N
        String nazionalita = ""; // ITA
        String terminalId = ""; // 00000318
        String OPTION_CF = ""; //
        String scadenza_pan = ""; // 203012
        String esito = ""; // OK
        String messaggio = ""; // Message OK
        String mail = ""; // test001@dispostable.com
        String codAut = ""; // 000000
        String check = ""; // PGP
        String hash = ""; // fdIePLvfXiP/+GCYt1czrUGAHA4=
        String alias = ""; // ALIAS_WEB_00000318
        String codiceEsito = ""; // 0
        String orario = ""; // 105355
        String importo = ""; // 64
        String cognome = ""; // Rossi
        String languageId = "";// ITA
        String pan = ""; // 453997XXXXXX0006
        String num_contratto = ""; // NC_TEST_20170721105350
        String divisa = ""; // EUR
        String brand = ""; // VISA
        String nome = ""; // Mario
        String codTrans = ""; // Q0VGREIzRTNCQkIwNzQ4ODdCQTYyOE
        
        // Lettura dei parametri di input
        Map<String, String[]> params = request.getParameterMap();
        Iterator<String> i = params.keySet().iterator();
        while ( i.hasNext() )
        {
            String key = (String) i.next();
            String value = ((String[]) params.get( key ))[ 0 ];
            
            if (key.equals("session_id")) {
                session_id = value;
            }
            
            if (key.equals("regione")) {
                regione = value;
            }
            
            if (key.equals("tipoTransazione")) {
                tipoTransazione = value;
            }
            
            if (key.equals("data")) {
                data = value;
            }
            
            if (key.equals("mac")) {
                mac = value;
            }
            
            if (key.equals("codiceConvenzione")) {
                codiceConvenzione = value;
            }
            
            if (key.equals("tipoProdotto")) {
                tipoProdotto = value;
            }
            
            if (key.equals("nazionalita")) {
                nazionalita = value;
            }
            
            if (key.equals("terminalId")) {
                terminalId = value;
            }
            
            if (key.equals("OPTION_CF")) {
                OPTION_CF = value;
            }
            
            if (key.equals("scadenza_pan")) {
                scadenza_pan = value;
            }
            
            if (key.equals("esito")) {
                esito = value;
            }
            
            if (key.equals("messaggio")) {
                messaggio = value;
            }
            
            if (key.equals("mail")) {
                mail = value;
            }
            
            if (key.equals("codAut")) {
                codAut = value;
            }
            
            if (key.equals("check")) {
                check = value;
            }
            
            if (key.equals("hash")) {
                hash = value;
            }
            
            if (key.equals("alias")) {
                alias = value;
            }
            
            if (key.equals("codiceEsito")) {
                codiceEsito = value;
            }
            
            if (key.equals("orario")) {
                orario = value;
            }
            
            if (key.equals("importo")) {
                importo = value;
            }
            
            if (key.equals("cognome")) {
                cognome = value;
            }
            
            if (key.equals("languageId")) {
                languageId = value;
            }
            
            if (key.equals("pan")) {
                pan = value;
            }
            
            if (key.equals("num_contratto")) {
                num_contratto = value;
            }
            
            if (key.equals("divisa")) {
                divisa = value;
            }
            
            if (key.equals("brand")) {
                brand = value;
            }
            
            if (key.equals("nome")) {
                nome = value;
            }
            
            if (key.equals("codTrans")) {
                codTrans = value;
            }   
            
            System.out.println(key + ": " + value);
            out.write(key + ": " + value + "\r\n");
        }
        
        
        // Chiamata al servizio per verifica metodo di pagamento
        final Hashtable jndiProperties = new Hashtable();
        
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        
        UserV2ServiceRemote     userV2Service       = null;
        ParametersServiceRemote parametersService = null;
        
        try {
            
            final Context context = new InitialContext(jndiProperties);
            
            final String userV2ServiceRemoteJndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserV2Service!com.techedge.mp.core.business.UserV2ServiceRemote";
            Object objectUserV2ServiceRemoteJndi = context.lookup(userV2ServiceRemoteJndi);
            userV2Service = (UserV2ServiceRemote)objectUserV2ServiceRemoteJndi;
            
            final String parametersServiceRemoteJndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
            Object oParametersServiceRemoteJndi = context.lookup(parametersServiceRemoteJndi);
            parametersService = (ParametersServiceRemote)oParametersServiceRemoteJndi;
            
        } catch (NamingException ex) {
            //ex.printStackTrace();
            //logger.log(Level.INFO, "Error in getRemoteObject()");
        }
        
        // Recupero stringa di codifica
        String secretKey = "";
        
        String vaultBlock = "";
        String vaultBlockAttributeCryptKey = "";

        try {

            vaultBlock = parametersService.getParamValue("CARTASI_VAULT_BLOCK");
            vaultBlockAttributeCryptKey = parametersService.getParamValue("CARTASI_VAULT_BLOCK_ATTRIBUTE_KEY");
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            //System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        
        String decodedSecretKey = "";
        
        try {
            
            //System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
            //System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeCryptKey + "'");
            //System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + secretKey + "'");
            
            String encodedSecretKey = parametersService.getParamValue("ENCODED_SECRET_KEY_CARTASI");
            
            
            if ( encodedSecretKey != null && !encodedSecretKey.equals("") ) {
                
                EncryptionAES encryptionAES = new EncryptionAES();
                encryptionAES.loadSecretKey(secretKey);
                decodedSecretKey = encryptionAES.decrypt(encodedSecretKey);
            }
            
            // Verifica mac
            String stringaMac = "codTrans=" + codTrans +
                                "esito="    + esito +
                                "importo="  + importo +
                                "divisa="   + divisa +
                                "data="     + data +
                                "orario="   + orario +
                                "codAut="   + codAut +
                                decodedSecretKey;

            String macCalculated = hashMac(stringaMac);
            
            if (!macCalculated.equals(mac)) {
                
                System.out.println("End");
                
                response.setContentType("text/html");
                response.setStatus(401);
                
                out.println("<html><head><title>Response</title></head><body><h1>KO</h1></body></html>");
                out.close();
            }
        }
        catch (Exception e) {
            
            System.out.println("End");
            
            response.setContentType("text/html");
            response.setStatus(401);
            
            out.println("<html><head><title>Response</title></head><body><h1>KO</h1></body></html>");
            out.close();
        }
        
        // Chiamata servizio di aggiornamento metodo di pagamento
        Double convertedAmount = Double.valueOf(importo) / 100;
        String stringConvertedAmount = String.valueOf(convertedAmount);
        
        String tokenExpiryMonth = scadenza_pan.substring(4, 6);
        String tokenExpiryYear  = scadenza_pan.substring(2, 4);
        
        String cardBIN = pan.substring(0, 6);
        String tdLevel = "";
        if ( tipoTransazione.equals("NO_3DSECURE ")) {
            tdLevel = "HALF";
        }
        else {
            tdLevel = "FULL";
        }
        
        String result = userV2Service.updateUserPaymentData(
            "DECRYPT",
            esito,
            codTrans,
            "",
            codAut,
            divisa,
            stringConvertedAmount,
            nazionalita,
            nome + " " + cognome,
            mail,
            codiceEsito,
            messaggio,
            codiceEsito,
            messaggio,
            null,
            num_contratto,
            tokenExpiryMonth,
            tokenExpiryYear,
            cardBIN,
            tdLevel,
            pan,
            brand,
            hash);
        
        System.out.println("End");
        
        response.setContentType("text/html");
        response.setStatus(200);
        
        out.println("<html><head><title>Response</title></head><body><h1>OK</h1></body></html>");
        out.close();
        
	}
	
	
	private static String hashMac(String stringaMac) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        byte[] in = digest.digest(stringaMac.getBytes("UTF-8"));
        
        final StringBuilder builder = new StringBuilder();
        for(byte b : in) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

}
