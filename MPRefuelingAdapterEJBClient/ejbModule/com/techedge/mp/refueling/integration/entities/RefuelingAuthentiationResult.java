package com.techedge.mp.refueling.integration.entities;

import java.io.Serializable;
import java.util.Date;

public class RefuelingAuthentiationResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 9005791391924221276L;

    private String            Email;
    private String            Name;
    private String            Surname;
    private String            PrefixPhoneNumber;
    private String            PhoneNumber;
    private String            fiscalCode;
    private Date              dateOfBirth;
    private String            birthMunicipality;
    private String            birthProvince;
    private String            sex;
    private String            statusCode;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBirthMunicipality() {
        return birthMunicipality;
    }

    public void setBirthMunicipality(String birthMunicipality) {
        this.birthMunicipality = birthMunicipality;
    }

    public String getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(String birthProvince) {
        this.birthProvince = birthProvince;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getPrefixPhoneNumber() {
        return PrefixPhoneNumber;
    }

    public void setPrefixPhoneNumber(String prefixPhoneNumber) {
        PrefixPhoneNumber = prefixPhoneNumber;
    }

    
}
