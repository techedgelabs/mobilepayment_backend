package com.techedge.mp.refueling.integration.entities;

import java.io.Serializable;

public class RefuelDetail implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -5215988920908631948L;
    
    protected String timestampStartRefuel;
    protected String timestampEndRefuel;
    protected String authorizationCode;
    protected Double amount;
    protected Double fuelQuantity;
    protected String productID;
    protected String productDescription;
    protected String fuelType;

    public String getTimestampStartRefuel() {
        return timestampStartRefuel;
    }

    public void setTimestampStartRefuel(String timestampStartRefuel) {
        this.timestampStartRefuel = timestampStartRefuel;
    }

    public String getTimestampEndRefuel() {
        return timestampEndRefuel;
    }

    public void setTimestampEndRefuel(String timestampEndRefuel) {
        this.timestampEndRefuel = timestampEndRefuel;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String toString(){
        
        StringBuilder builder = new StringBuilder();
        builder.append("timestampStartRefuel: ").append(getTimestampStartRefuel());
        builder.append(" - timestampEndRefuel: ").append(getTimestampEndRefuel());
        builder.append(" - amount: ").append(String.valueOf(getAmount()));
        builder.append(" - authorizationCode: ").append(getAuthorizationCode());
        builder.append(" - fuelType: ").append(getFuelType());
        builder.append(" - fuelQuantity: ").append(String.valueOf(getFuelQuantity()));
        builder.append(" - productID: ").append(getProductID());
        builder.append(" - productDescription: ").append(getProductDescription());
        
        return builder.toString();
        
    }
}