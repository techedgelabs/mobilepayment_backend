package com.techedge.mp.refueling.integration.entities;

import java.io.Serializable;

public class MpTransactionDetail implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -1850119632650388123L;
    
    protected String       srcTransactionID;
    protected String       mpTransactionID;
    protected String       mpTransactionStatus;
    protected RefuelDetail refuelDetail;

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public String getMpTransactionStatus() {
        return mpTransactionStatus;
    }

    public void setMpTransactionStatus(String mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }

    public RefuelDetail getRefuelDetail() {
        return refuelDetail;
    }

    public void setRefuelDetail(RefuelDetail refuelDetail) {
        this.refuelDetail = refuelDetail;
    }

}