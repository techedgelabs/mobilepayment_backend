package com.techedge.mp.refueling.integration.entities;

import java.io.Serializable;

public class NotifySubscriptionResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8027708551286726995L;
    private String            statusCode;
    private String            statusMessage;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

}
