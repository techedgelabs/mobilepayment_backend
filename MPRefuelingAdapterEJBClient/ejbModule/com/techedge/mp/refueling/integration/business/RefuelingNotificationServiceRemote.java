package com.techedge.mp.refueling.integration.business;

import java.util.List;

import javax.ejb.Remote;

import com.techedge.mp.refueling.integration.entities.MpTransactionDetail;
import com.techedge.mp.refueling.integration.entities.NotifySubscriptionResult;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;
import com.techedge.mp.refueling.integration.entities.RefuelingAuthentiationResult;

@Remote
public interface RefuelingNotificationServiceRemote {

    public String sendMPTransactionNotification(String requestID, String srcTransactionID, String mpTransactionID, String mpTransactionStatus, RefuelDetail refuelDetail);

    public String sendMPTransactionReport(String requestID, String startDate, String endDate, List<MpTransactionDetail> mpTransactionList);
    
    public RefuelingAuthentiationResult refuelingAuthentication(String requestID,String username, String password);
    
    public NotifySubscriptionResult notifySubscription(String requestID,String fiscalCode);
    
}