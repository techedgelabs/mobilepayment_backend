package com.techedge.mp.refueling.integration.utility;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.beanutils.ConvertUtils;

public class UtilityCheck {

    
    
    public UtilityCheck() {

    }

    /***
     * Controlla i campi required dell'oggetto obj passato.
     * Se nella request manca qualche campo restituisce una RuntimeException indicando il primo campo mancante
     * 
     * @param fields
     *            array of object's field
     * @param obj
     *            Object to get field value
     * @return
     * @throws RuntimeException
     */
    public static boolean isRequired(Object obj) throws RuntimeException {

        Field[] fields = obj.getClass().getDeclaredFields();

        for (Field field : fields) {

            XmlElement element = field.getAnnotation(XmlElement.class);
            if (element.required()) {
                try {
                    field.setAccessible(true);
                    String str = ConvertUtils.convert(field.get(obj));
                    if (!isMandatory(str)) {
                        throw new RuntimeException("Error in SOAP request. " + field.getName() + " is missing");
                    }
                }
                catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }

        return true;
    }

    private static boolean isMandatory(String str) {

        boolean present = true;
        if (str == null || str.equals("")) {
            present = false;
        }
        return present;
    }

    
    /**
     * Usta nei JUnit per verificare la classCastExecption quando trovo required element che non possono essere castati a String
     * L'eccezione dovrebbe essere generata a questa istruzione if (!isMandatory((String) field.get(obj))).
     * 
     * @param obj
     * @return
     * @throws RuntimeException
     */
    public static boolean isRequiredCastException(Object obj) throws RuntimeException {

        Field[] fields = obj.getClass().getDeclaredFields();

        for (Field field : fields) {

            XmlElement element = field.getAnnotation(XmlElement.class);
            if (element.required()) {
                try {
                    field.setAccessible(true);
                    if (!isMandatory((String) field.get(obj))) {
                        throw new RuntimeException("Error in SOAP request. " + field.getName() + " is missing");
                    }
                }
                catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }

        return true;
    }
    
    /**
     * Fa il check sul formato della data passata come stringa 
     * @param String date
     * @return true se la data � di un formato valido, false altrimenti
     */
    public static boolean isValidDate(String date){
        
        boolean validData = true;
        
        Date javaData = null;
        
        try {
            
            javaData = convertStringToDate(date);

        }
        catch (ParseException e) {
            
            validData = false;
        }
        
        return validData;
    }

    /**
     * Converte la stringa in oggetto Date
     * @param date
     * @return La data in formato yyyy-MM-dd
     * @throws ParseException se il parametro date non � nel formato aspettato
     */
    public static Date convertStringToDate(String date) throws ParseException {

        Date javaData = new Date();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setLenient(false);

        javaData = format.parse(date);
        
        return javaData;
    }
    
    public static boolean isValidDateRange(Date startDate, Date endDate) {

        boolean validRange = false;

        if (startDate != null && endDate != null) {

            // true if they are equal
            if (startDate.equals(endDate)) {
                validRange = true;
            }

            // true if endDate after startDate
            if (endDate.after(startDate)) {
                validRange = true;
            }
        }
        return validRange;
    }
    

    /**
     * Controlla che la requestId inizi con un suffisso specificato
     * @param requestID ID univoco della richiesta
     * @param suffix pu� essere "ENJ-" oppure "MOP-"
     * @return true se requestID inizia con il suffisso 
     */
    public static boolean isValidRequestID(String requestID, String suffix){
        //TODO portrebbe essere modificato una volta capito come deve essere controllata la requestID
        boolean validRequestId = false;
        
        if(requestID.startsWith(suffix.toUpperCase()) && isIntegrity(requestID, suffix)){
            validRequestId = true;
        }

        return validRequestId;
    }
    
    public static boolean isIntegrity(String requestID, String suffix){
        
        boolean integrity = false;
        
        String[] str = requestID.split(suffix);
        
        if (str.length > 0 && str[1].matches("[0-9]+") && isEquals(str[1], 10)){
            
            integrity = true;
        }
        return integrity;
    }
    
    
    
    public static boolean isEquals(String str, int length){
        
        boolean isEquals = false;
        
        if(str.length()== length) {
            
            isEquals = true;
        }
        
        return isEquals;
    }
    /**
     * Controlla che l'attributo passato abbia una linghezza tra min e max
     * @param attribute attributo da controllare
     * @param min lunghezza minima
     * @param max lunghezza massima
     * @return true se l'attributo � nel range [min,max]
     */
    public static boolean isValidLength(String attribute, int min, int max){
        
        boolean validLength = false;
        
        String newString = attribute.replaceAll("\\s+","");
        
        if( newString.length() > min && newString.length() < max ){
            
            validLength = true;
        }
        
        return validLength;
    }
    
    public static boolean checkValidDate(String startDate, String endDate) {

        boolean validaData = false;
        
        if (UtilityCheck.isValidDate(startDate) && UtilityCheck.isValidDate(endDate)){
            try {
                
                Date start = UtilityCheck.convertStringToDate(startDate);
                Date end = UtilityCheck.convertStringToDate(endDate);
                
                if (UtilityCheck.isValidDateRange(start, end)){
                    validaData = true;
                    
                }
//                else{
//                    throw new RuntimeException("Invalid date range!");
//                }
            }
//            catch (RuntimeException e) {
//                // TODO Auto-generated catch block
//                throw e;
//            }
            catch (ParseException e) {
                
                e.printStackTrace();
            }
        }
//        else {
//            throw new RuntimeException("Invalid date!");
//        }
        
        return validaData;
    }
    
}
