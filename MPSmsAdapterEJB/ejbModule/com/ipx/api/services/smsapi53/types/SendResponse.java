
package com.ipx.api.services.smsapi53.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="correlationId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="messageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="responseCode" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="reasonCode" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="responseMessage" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="temporaryError" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="billingStatus" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="VAT" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "correlationId",
    "messageId",
    "responseCode",
    "reasonCode",
    "responseMessage",
    "temporaryError",
    "billingStatus",
    "vat"
})
@XmlRootElement(name = "SendResponse")
public class SendResponse {

    @XmlElement(required = true)
    protected String correlationId;
    @XmlElement(required = true)
    protected String messageId;
    protected int responseCode;
    protected int reasonCode;
    @XmlElement(required = true)
    protected String responseMessage;
    protected boolean temporaryError;
    protected int billingStatus;
    @XmlElement(name = "VAT")
    protected double vat;

    /**
     * Recupera il valore della proprietÓ correlationId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationId() {
        return correlationId;
    }

    /**
     * Imposta il valore della proprietÓ correlationId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationId(String value) {
        this.correlationId = value;
    }

    /**
     * Recupera il valore della proprietÓ messageId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Imposta il valore della proprietÓ messageId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Recupera il valore della proprietÓ responseCode.
     * 
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * Imposta il valore della proprietÓ responseCode.
     * 
     */
    public void setResponseCode(int value) {
        this.responseCode = value;
    }

    /**
     * Recupera il valore della proprietÓ reasonCode.
     * 
     */
    public int getReasonCode() {
        return reasonCode;
    }

    /**
     * Imposta il valore della proprietÓ reasonCode.
     * 
     */
    public void setReasonCode(int value) {
        this.reasonCode = value;
    }

    /**
     * Recupera il valore della proprietÓ responseMessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * Imposta il valore della proprietÓ responseMessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseMessage(String value) {
        this.responseMessage = value;
    }

    /**
     * Recupera il valore della proprietÓ temporaryError.
     * 
     */
    public boolean isTemporaryError() {
        return temporaryError;
    }

    /**
     * Imposta il valore della proprietÓ temporaryError.
     * 
     */
    public void setTemporaryError(boolean value) {
        this.temporaryError = value;
    }

    /**
     * Recupera il valore della proprietÓ billingStatus.
     * 
     */
    public int getBillingStatus() {
        return billingStatus;
    }

    /**
     * Imposta il valore della proprietÓ billingStatus.
     * 
     */
    public void setBillingStatus(int value) {
        this.billingStatus = value;
    }

    /**
     * Recupera il valore della proprietÓ vat.
     * 
     */
    public double getVAT() {
        return vat;
    }

    /**
     * Imposta il valore della proprietÓ vat.
     * 
     */
    public void setVAT(double value) {
        this.vat = value;
    }

}
