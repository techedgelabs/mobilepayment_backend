
package com.ipx.api.services.smsapi53.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="correlationId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="originatingAddress" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="originatorTON" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="destinationAddress" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="userData" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="userDataHeader" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DCS" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="relativeValidityTime" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="deliveryTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="statusReportFlags" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="accountName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tariffClass" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="VAT" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="referenceId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="serviceName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="serviceId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="serviceCategory" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="serviceMetaData" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="campaignName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "correlationId",
    "originatingAddress",
    "originatorTON",
    "destinationAddress",
    "userData",
    "userDataHeader",
    "dcs",
    "pid",
    "relativeValidityTime",
    "deliveryTime",
    "statusReportFlags",
    "accountName",
    "tariffClass",
    "vat",
    "referenceId",
    "serviceName",
    "serviceId",
    "serviceCategory",
    "serviceMetaData",
    "campaignName",
    "username",
    "password"
})
@XmlRootElement(name = "SendRequest")
public class SendRequest {

    @XmlElement(required = true)
    protected String correlationId;
    @XmlElement(required = true)
    protected String originatingAddress;
    protected int originatorTON;
    @XmlElement(required = true)
    protected String destinationAddress;
    @XmlElement(required = true)
    protected String userData;
    @XmlElement(required = true)
    protected String userDataHeader;
    @XmlElement(name = "DCS")
    protected int dcs;
    @XmlElement(name = "PID")
    protected int pid;
    protected int relativeValidityTime;
    @XmlElement(required = true)
    protected String deliveryTime;
    protected int statusReportFlags;
    @XmlElement(required = true)
    protected String accountName;
    @XmlElement(required = true)
    protected String tariffClass;
    @XmlElement(name = "VAT")
    protected double vat;
    @XmlElement(required = true)
    protected String referenceId;
    @XmlElement(required = true)
    protected String serviceName;
    @XmlElement(required = true)
    protected String serviceId;
    @XmlElement(required = true)
    protected String serviceCategory;
    @XmlElement(required = true)
    protected String serviceMetaData;
    @XmlElement(required = true)
    protected String campaignName;
    @XmlElement(required = true)
    protected String username;
    @XmlElement(required = true)
    protected String password;

    /**
     * Recupera il valore della proprietÓ correlationId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationId() {
        return correlationId;
    }

    /**
     * Imposta il valore della proprietÓ correlationId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationId(String value) {
        this.correlationId = value;
    }

    /**
     * Recupera il valore della proprietÓ originatingAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingAddress() {
        return originatingAddress;
    }

    /**
     * Imposta il valore della proprietÓ originatingAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingAddress(String value) {
        this.originatingAddress = value;
    }

    /**
     * Recupera il valore della proprietÓ originatorTON.
     * 
     */
    public int getOriginatorTON() {
        return originatorTON;
    }

    /**
     * Imposta il valore della proprietÓ originatorTON.
     * 
     */
    public void setOriginatorTON(int value) {
        this.originatorTON = value;
    }

    /**
     * Recupera il valore della proprietÓ destinationAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationAddress() {
        return destinationAddress;
    }

    /**
     * Imposta il valore della proprietÓ destinationAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationAddress(String value) {
        this.destinationAddress = value;
    }

    /**
     * Recupera il valore della proprietÓ userData.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserData() {
        return userData;
    }

    /**
     * Imposta il valore della proprietÓ userData.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserData(String value) {
        this.userData = value;
    }

    /**
     * Recupera il valore della proprietÓ userDataHeader.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserDataHeader() {
        return userDataHeader;
    }

    /**
     * Imposta il valore della proprietÓ userDataHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserDataHeader(String value) {
        this.userDataHeader = value;
    }

    /**
     * Recupera il valore della proprietÓ dcs.
     * 
     */
    public int getDCS() {
        return dcs;
    }

    /**
     * Imposta il valore della proprietÓ dcs.
     * 
     */
    public void setDCS(int value) {
        this.dcs = value;
    }

    /**
     * Recupera il valore della proprietÓ pid.
     * 
     */
    public int getPID() {
        return pid;
    }

    /**
     * Imposta il valore della proprietÓ pid.
     * 
     */
    public void setPID(int value) {
        this.pid = value;
    }

    /**
     * Recupera il valore della proprietÓ relativeValidityTime.
     * 
     */
    public int getRelativeValidityTime() {
        return relativeValidityTime;
    }

    /**
     * Imposta il valore della proprietÓ relativeValidityTime.
     * 
     */
    public void setRelativeValidityTime(int value) {
        this.relativeValidityTime = value;
    }

    /**
     * Recupera il valore della proprietÓ deliveryTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * Imposta il valore della proprietÓ deliveryTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryTime(String value) {
        this.deliveryTime = value;
    }

    /**
     * Recupera il valore della proprietÓ statusReportFlags.
     * 
     */
    public int getStatusReportFlags() {
        return statusReportFlags;
    }

    /**
     * Imposta il valore della proprietÓ statusReportFlags.
     * 
     */
    public void setStatusReportFlags(int value) {
        this.statusReportFlags = value;
    }

    /**
     * Recupera il valore della proprietÓ accountName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Imposta il valore della proprietÓ accountName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

    /**
     * Recupera il valore della proprietÓ tariffClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffClass() {
        return tariffClass;
    }

    /**
     * Imposta il valore della proprietÓ tariffClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffClass(String value) {
        this.tariffClass = value;
    }

    /**
     * Recupera il valore della proprietÓ vat.
     * 
     */
    public double getVAT() {
        return vat;
    }

    /**
     * Imposta il valore della proprietÓ vat.
     * 
     */
    public void setVAT(double value) {
        this.vat = value;
    }

    /**
     * Recupera il valore della proprietÓ referenceId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * Imposta il valore della proprietÓ referenceId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceId(String value) {
        this.referenceId = value;
    }

    /**
     * Recupera il valore della proprietÓ serviceName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Imposta il valore della proprietÓ serviceName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceName(String value) {
        this.serviceName = value;
    }

    /**
     * Recupera il valore della proprietÓ serviceId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * Imposta il valore della proprietÓ serviceId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceId(String value) {
        this.serviceId = value;
    }

    /**
     * Recupera il valore della proprietÓ serviceCategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceCategory() {
        return serviceCategory;
    }

    /**
     * Imposta il valore della proprietÓ serviceCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceCategory(String value) {
        this.serviceCategory = value;
    }

    /**
     * Recupera il valore della proprietÓ serviceMetaData.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceMetaData() {
        return serviceMetaData;
    }

    /**
     * Imposta il valore della proprietÓ serviceMetaData.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceMetaData(String value) {
        this.serviceMetaData = value;
    }

    /**
     * Recupera il valore della proprietÓ campaignName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampaignName() {
        return campaignName;
    }

    /**
     * Imposta il valore della proprietÓ campaignName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampaignName(String value) {
        this.campaignName = value;
    }

    /**
     * Recupera il valore della proprietÓ username.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Imposta il valore della proprietÓ username.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Recupera il valore della proprietÓ password.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Imposta il valore della proprietÓ password.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

}
