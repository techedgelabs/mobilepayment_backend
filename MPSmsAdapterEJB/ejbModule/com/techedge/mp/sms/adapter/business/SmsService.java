package com.techedge.mp.sms.adapter.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.ipx.api.services.smsapi53.SmsApiPort;
import com.ipx.api.services.smsapi53.SmsApiService;
import com.ipx.api.services.smsapi53.types.SendRequest;
import com.ipx.api.services.smsapi53.types.SendResponse;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.SmsStatusType;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.sms.adapter.business.interfaces.Parameter;
import com.techedge.mp.sms.adapter.business.interfaces.SendMessageResult;
import com.techedge.mp.sms.adapter.business.interfaces.SmsType;

/**
 * Session Bean implementation class SmsService
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class SmsService implements SmsServiceRemote, SmsServiceLocal {

    private final static String     PARAM_PROXY_HOST                         = "PROXY_HOST";
    private final static String     PARAM_PROXY_PORT                         = "PROXY_PORT";
    private final static String     PARAM_PROXY_NO_HOSTS                     = "PROXY_NO_HOSTS";

    //private final static String     PARAM_SMS_URL_WSDL                       = "SMS_URL_WSDL";
    private final static String     PARAM_SMS_URL_PREFIX_WSDL                = "SMS_URL_PREFIX_WSDL";

    private final static String     PARAM_SMS_VAULT_BLOCK                    = "SMS_VAULT_BLOCK";
    private final static String     PARAM_SMS_VAULT_BLOCK_ATTRIBUTE_USERNAME = "SMS_VAULT_BLOCK_ATTRIBUTE_USERNAME";
    private final static String     PARAM_SMS_VAULT_BLOCK_ATTRIBUTE_PASSWORD = "SMS_VAULT_BLOCK_ATTRIBUTE_PASSWORD";
    private final static String     PARAM_SMS_ADDRESS_ALIAS                  = "SMS_ADDRESS_ALIAS";    

    private ParametersServiceRemote parametersService                        = null;
    private LoggerServiceRemote     loggerService                            = null;
    private UserServiceRemote       userService                              = null;

    private URL                     url;

    // Valori di default
    private String                  proxyHost                                = "mpsquid.enimp.pri";
    private String                  proxyPort                                = "3128";
    private String                  proxyNoHosts                             = "localhost|127.0.0.1|*.enimp.pri";

    private String                  username                                 = "";
    private String                  password                                 = "";
    private String                  addressAlias                             = null;

    /**
     * Default constructor.
     */
    public SmsService() {
        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
            this.userService = EJBHomeCache.getInstance().getUserService();
        }
        catch (InterfaceNotFoundException e) {
            e.printStackTrace();
        }

        //String wsdlString = "";
        String smsUrlPrefixWsdl = "";
        String vaultBlock = "";
        String vaultBlockAttributeUsername = "";
        String vaultBlockAttributePassword = "";
        
        try {
            //wsdlString = parametersService.getParamValue(SmsService.PARAM_SMS_URL_WSDL);
            smsUrlPrefixWsdl = parametersService.getParamValue(SmsService.PARAM_SMS_URL_PREFIX_WSDL);
            this.proxyHost = parametersService.getParamValue(SmsService.PARAM_PROXY_HOST);
            this.proxyPort = parametersService.getParamValue(SmsService.PARAM_PROXY_PORT);
            this.proxyNoHosts = parametersService.getParamValue(SmsService.PARAM_PROXY_NO_HOSTS);
            vaultBlock = parametersService.getParamValue(SmsService.PARAM_SMS_VAULT_BLOCK);
            vaultBlockAttributeUsername = parametersService.getParamValue(SmsService.PARAM_SMS_VAULT_BLOCK_ATTRIBUTE_USERNAME);
            vaultBlockAttributePassword = parametersService.getParamValue(SmsService.PARAM_SMS_VAULT_BLOCK_ATTRIBUTE_PASSWORD);
            this.addressAlias = parametersService.getParamValue(SmsService.PARAM_SMS_ADDRESS_ALIAS);
        }
        catch (ParameterNotFoundException e) {
            e.printStackTrace();
        }

        try {
            InputStream fin = this.getClass().getResourceAsStream("/com/techedge/mp/sms/adapter/business/wsdl/SmsApi53.wsdl");

            InputStreamReader is = new InputStreamReader(fin);
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(is);
            String read;

            while ((read = br.readLine()) != null) {
                sb.append(read);
            }

            br.close();
            read = sb.toString().replaceAll("__SMS_URL_PREFIX_WSDL__", smsUrlPrefixWsdl);

            String prefix = System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "wsdl" + File.separator + "SmsApi53";
            String suffix = ".wsdl";
            File tempFile = File.createTempFile(prefix, suffix);
            FileOutputStream fout = new FileOutputStream(tempFile);
            tempFile.deleteOnExit();
            fout.write(read.getBytes());
            fout.close();

            if (tempFile.exists()) {
                System.out.println("Create temp sms wsdl: " + tempFile.getAbsolutePath());
            }
            else {
                System.err.println("Cannot to create temp sms wsdl: " + tempFile.getAbsolutePath());
            }

            this.url = tempFile.toURI().toURL();
            //this.url = new URL(wsdlString);
        }
        catch (MalformedURLException e) {

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "Constructor", null, null, "connection error - url: " + this.url);

            e.printStackTrace();
        }
        catch (IOException e) {

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "Constructor", null, null, "connection error - url: " + this.url);

            e.printStackTrace();
        }
        
        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {
                
                try {
                    this.username = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeUsername, new byte[] { 1 })).trim();
                    this.password = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributePassword, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeUsername + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 2 NAME: '" + vaultBlockAttributePassword + "'");
        
        System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.username + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 2 VALUE: '" + this.password + "'");
    }
    
    @Override
    public SendMessageResult sendShortMessage(String senderAlias, String destinationAddress, String message, String messageId) {

        System.out.println("Invoking SMS send service...");
        
        destinationAddress = destinationAddress.replaceAll("\\+", "");
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("destinationAddress", destinationAddress));
        inputParameters.add(new Pair<String, String>("message", message));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendShortMessage", null, "opening", ActivityLog.createLogMessage(inputParameters));

        SendMessageResult result = new SendMessageResult();

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        try {
            
            if (messageId == null) {
                messageId = "MP-SMS-" + new Date().getTime();
                userService.updateSmsLog(messageId, destinationAddress, message, null, null, null, null, null, null, null, null, true);
            }
            
            System.setProperty("javax.net.debug", "ssl");
            proxy.setHttp();

            SmsApiService smsService = new SmsApiService(this.url);
            System.out.println("Create SMS Web Service...");
            SmsApiPort smsPort = smsService.getSmsApi53();
            System.out.println("Call SMS Web Service Operation...");

            SendRequest request = new SendRequest();

            request.setCorrelationId(messageId);
            request.setOriginatingAddress((senderAlias != null) ? senderAlias : this.addressAlias);
            request.setOriginatorTON(1);
            request.setDestinationAddress(destinationAddress);
            request.setUserData(message);
            request.setUserDataHeader("#NULL#");
            request.setDCS(-1);
            request.setPID(-1);
            request.setRelativeValidityTime(-1);
            request.setDeliveryTime("#NULL#");
            request.setStatusReportFlags(1);
            request.setAccountName("#NULL#");
            request.setTariffClass("EUR0");
            request.setVAT(-1);
            request.setReferenceId("#NULL#");
            request.setServiceName("#NULL#");
            request.setServiceId("#NULL#");
            request.setServiceCategory("#NULL#");
            request.setServiceMetaData("#NULL#");
            request.setCampaignName("#NULL#");
            request.setUsername(this.username);
            request.setPassword(this.password);

            SendResponse response = smsPort.send(request);

            proxy.unsetHttp();

            result.setBillingStatus(response.getBillingStatus());
            result.setCorrelationId(response.getCorrelationId());
            result.setMessageId(response.getMessageId());
            result.setReasonCode(response.getReasonCode());
            result.setResponseCode(response.getResponseCode());
            result.setResponseMessage(response.getResponseMessage());
            result.setTemporaryError(response.isTemporaryError());
            result.setVAT(response.getVAT());
            
            if (result.getResponseCode() != 0) {
                userService.updateSmsLog(messageId, null, null, null, SmsStatusType.ERROR.getCode(), result.getResponseCode(), result.getReasonCode(), 
                        result.getResponseMessage(), null, null, null, false);
            }
        }
        /*catch (javax.xml.ws.soap.SOAPFaultException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendShortMessage", correlationId, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }
        catch (javax.xml.ws.ProtocolException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendShortMessage", correlationId, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }
        catch (javax.xml.ws.WebServiceException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendShortMessage", correlationId, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }
        catch (java.lang.SecurityException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendShortMessage", correlationId, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }*/
        catch (Exception ex) {
            //ex.printStackTrace();
            System.out.println("Eccezione in invio SMS");
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendShortMessage", messageId, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("BillingStatus", result.getBillingStatus().toString()));
        outputParameters.add(new Pair<String, String>("CorrelationId", result.getCorrelationId()));
        outputParameters.add(new Pair<String, String>("MessageId", result.getMessageId()));
        outputParameters.add(new Pair<String, String>("ReasonCode", result.getReasonCode().toString()));
        outputParameters.add(new Pair<String, String>("ResponseCode", result.getResponseCode().toString()));
        outputParameters.add(new Pair<String, String>("ResponseMessage", result.getResponseMessage()));
        outputParameters.add(new Pair<String, String>("TemporaryError", result.getTemporaryError().toString()));
        outputParameters.add(new Pair<String, String>("VAT", result.getVAT().toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendShortMessage", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return result;        
    }
    
    @Override
    public SendMessageResult sendShortMessage(String destinationAddress, String message, String messageId) {
        return sendShortMessage(null, destinationAddress, message, messageId);
    }    

    @Override
    public SendMessageResult sendShortMessage(SmsType smsType, String senderAlias, String destinationAddress, List<Parameter> parameters, String messageId) {
        String message = smsType.getTemplateContent();
        
        if (parameters != null) {

            for (Parameter parameter : parameters) {
    
                if (parameter.getName() != null && message.contains("%%" + parameter.getName() + "%%")) {
    
                    System.out.println( "Parameter name: " + parameter.getName() + ", vaule: " + parameter.getValue() );
                    
                    String value = "";
                    if (parameter.getValue() != null) {
                        value = parameter.getValue();
                    }
    
                    message = message.replaceAll("%%" + parameter.getName() + "%%", value);
                }
            }
        }
        
        return sendShortMessage(senderAlias, destinationAddress, message, messageId);
    }
    
    @Override
    public SendMessageResult sendShortMessage(SmsType smsType, String destinationAddress, List<Parameter> parameters, String messageId) {
        return sendShortMessage(smsType, null, destinationAddress, parameters, messageId);
    }
    
    public String getSender() {
        return this.addressAlias;
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {

            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

}
