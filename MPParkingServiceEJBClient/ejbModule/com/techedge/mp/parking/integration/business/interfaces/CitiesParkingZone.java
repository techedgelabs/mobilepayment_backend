package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CitiesParkingZone implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5527071852908111918L;

    private List<ParkingZone> parkingZoneList  = new ArrayList<ParkingZone>(0);
    private ParkingCity       city;

    public List<ParkingZone> getParkingZoneList() {
        return parkingZoneList;
    }

    public void setParkingZoneList(List<ParkingZone> parkingZoneList) {
        this.parkingZoneList = parkingZoneList;
    }

    public ParkingCity getCity() {
        return city;
    }

    public void setCity(ParkingCity city) {
        this.city = city;
    }

}
