package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExtendParkingResult extends FullParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6694975416474472398L;

    private BigDecimal        previousPrice;

    public BigDecimal getPreviousPrice() {
        return previousPrice;
    }

    public void setPreviousPrice(BigDecimal previousPrice) {
        this.previousPrice = previousPrice;
    }

}
