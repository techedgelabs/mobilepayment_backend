package com.techedge.mp.parking.integration.business;

import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.Remote;

import com.techedge.mp.parking.integration.business.interfaces.EndParkingResult;
import com.techedge.mp.parking.integration.business.interfaces.EstimateEndParkingPriceResult;
import com.techedge.mp.parking.integration.business.interfaces.EstimateExtendedParkingPriceResult;
import com.techedge.mp.parking.integration.business.interfaces.EstimateParkingPriceResult;
import com.techedge.mp.parking.integration.business.interfaces.ExtendParkingResult;
import com.techedge.mp.parking.integration.business.interfaces.GetCitiesResult;
import com.techedge.mp.parking.integration.business.interfaces.GetParkingZonesByCityResult;
import com.techedge.mp.parking.integration.business.interfaces.RetrieveParkingZonesResult;
import com.techedge.mp.parking.integration.business.interfaces.StartParkingResult;
import com.techedge.mp.parking.integration.business.interfaces.UserMyCiceroDataResult;

@Remote
public interface ParkingServiceRemote {

    public GetCitiesResult getCities(String lang);

    public GetParkingZonesByCityResult getParkingZonesByCityResult(String lang, String cityId);

    public RetrieveParkingZonesResult retrieveParkingZones(String lang, BigDecimal latitude, BigDecimal longitude, BigDecimal accuracy);

    public EstimateParkingPriceResult estimateParkingPrice(String lang, String plateNumber, Date requestedEndTime, String parkingZoneId, String stallCode);

    public StartParkingResult startParking(String lang, String plateNumber, Date requestedEndTime, String parkingZoneId, String stallCode, String clientOperationID);

    public EstimateExtendedParkingPriceResult estimateExtendedParkingPrice(String lang, String parkingId, Date requestedEndTime);

    public ExtendParkingResult extendParking(String lang, String parkingId, Date requestedEndTime, String clientOperationID);

    public EstimateEndParkingPriceResult estimateEndParkingPrice(String lang, String parkingId);

    public EndParkingResult endParking(String lang, String parkingId, String clientOperationID);

    public UserMyCiceroDataResult retrieveUserData(String logonToken);
}
