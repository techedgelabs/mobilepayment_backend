package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;

public class EndParkingResult extends BaseParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6631420076533119275L;
    
    private String            parkingId;

    public String getParkingId() {
        return parkingId;
    }

    public void setParkingId(String parkingId) {
        this.parkingId = parkingId;
    }

}
