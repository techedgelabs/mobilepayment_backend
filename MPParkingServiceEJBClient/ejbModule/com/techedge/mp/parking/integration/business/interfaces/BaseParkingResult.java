package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BaseParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -919460419132578598L;

    private String            statusCode;
    private String            parkingZoneId;
    private String            stallCode;
    private String            plateNumber;
    private Date              parkingStartTime;
    private Date              parkingEndTime;
    private BigDecimal        price;
    private List<String>      notice = new ArrayList<String>(0);

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getParkingZoneId() {
        return parkingZoneId;
    }

    public void setParkingZoneId(String parkingZoneId) {
        this.parkingZoneId = parkingZoneId;
    }

    public String getStallCode() {
        return stallCode;
    }

    public void setStallCode(String stallCode) {
        this.stallCode = stallCode;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Date getParkingStartTime() {
        return parkingStartTime;
    }

    public void setParkingStartTime(Date parkingStartTime) {
        this.parkingStartTime = parkingStartTime;
    }

    public Date getParkingEndTime() {
        return parkingEndTime;
    }

    public void setParkingEndTime(Date parkingEndTime) {
        this.parkingEndTime = parkingEndTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<String> getNotice() {
        return notice;
    }

    public void setNotice(List<String> notice) {
        
        if (notice != null && !notice.isEmpty()) {
            for(String noticeItem : notice) {
                if (noticeItem!= null) {
                    this.notice.add(noticeItem);
                }
            }
        }
        
        this.notice = new ArrayList<String>(0);
    }

}
