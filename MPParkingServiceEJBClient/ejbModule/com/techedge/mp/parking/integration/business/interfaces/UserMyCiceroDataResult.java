package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;

public class UserMyCiceroDataResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String            internalStatusCode;
    private String            MessaggioErrore;
    private String            StackTrace;
    private String            StatusCode;
    private Boolean           Successo;
    private String            AreaCode;
    private String            Email;
    private String            Name;
    private String            Surname;
    private String            PhoneNumber;
    private Boolean           VerifiedEmail;

    public String getInternalStatusCode() {
        return internalStatusCode;
    }

    public void setInternalStatusCode(String internalStatusCode) {
        this.internalStatusCode = internalStatusCode;
    }

    public String getMessaggioErrore() {
        return MessaggioErrore;
    }

    public void setMessaggioErrore(String messaggioErrore) {
        MessaggioErrore = messaggioErrore;
    }

    public String getStackTrace() {
        return StackTrace;
    }

    public void setStackTrace(String stackTrace) {
        StackTrace = stackTrace;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public Boolean getSuccesso() {
        return Successo;
    }

    public void setSuccesso(Boolean successo) {
        Successo = successo;
    }

    public String getAreaCode() {
        return AreaCode;
    }

    public void setAreaCode(String areaCode) {
        AreaCode = areaCode;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public Boolean getVerifiedEmail() {
        return VerifiedEmail;
    }

    public void setVerifiedEmail(Boolean verifiedEmail) {
        VerifiedEmail = verifiedEmail;
    }

}
