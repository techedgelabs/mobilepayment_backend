package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;
import java.math.BigDecimal;

public class EstimateExtendedParkingPriceResult extends FullParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -16421614295329874L;
    private BigDecimal        currentPrice;

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

}
