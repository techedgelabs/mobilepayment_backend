package com.techedge.mp.parking.integration.business.interfaces;

public class StatusCodeHelper {

    public final static String RETRIEVE_PARKING_ZONES_OK = "OK";
    public final static String RETRIEVE_PARKING_ZONES_KO = "KO";
    
    public final static String ESTIMATE_PARKING_PRICE_OK = "OK";
    public final static String ESTIMATE_PARKING_PRICE_KO = "KO";
    
    public final static String START_PARKING_OK = "OK";
    public final static String START_PARKING_KO = "KO";
    public final static String START_PARKING_INVALID_STALL_CODE = "INVALID_STALL_CODE";
    public final static String START_PARKING_PLATE_NUMBER_IN_USE = "PLATE_NUMBER_IN_USE";
    
    
    
    public final static String ESTIMATE_EXTEND_PARKING_PRICE_OK = "OK";
    public final static String ESTIMATE_EXTEND_PARKING_PRICE_KO = "KO";
    public final static String ESTIMATE_EXTEND_PARKING_PRICE_CLOSED = "CLOSED";
    
    public final static String EXTEND_PARKING_OK = "OK";
    public final static String EXTEND_PARKING_KO = "KO";
    public final static String PARKING_NOT_FOUND = "PARKING_NOT_FOUND";
    
    public final static String ESTIMATE_END_PARKING_PRICE_OK     = "OK";
    public final static String ESTIMATE_END_PARKING_PRICE_KO     = "KO";
    public final static String ESTIMATE_END_PARKING_PRICE_CLOSED = "CLOSED";
    
    public final static String END_PARKING_OK     = "OK";
    public final static String END_PARKING_KO     = "KO";
    public final static String END_PARKING_CLOSED = "CLOSED";
    
    public final static String RETRIEVE_USER_DATA_OK = "OK";
    public final static String RETRIEVE_USER_DATA_KO = "KO";
    
}
