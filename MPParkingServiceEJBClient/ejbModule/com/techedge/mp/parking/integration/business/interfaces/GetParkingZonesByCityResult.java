package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetParkingZonesByCityResult implements Serializable {

    /**
     * 
     */
    private static final long       serialVersionUID = 5527071852908111918L;

    private List<CitiesParkingZone> citiesParkingZones = new ArrayList<CitiesParkingZone>(0);
    private String                  statusCode;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<CitiesParkingZone> getCitiesParkingZones() {
        return citiesParkingZones;
    }

    public void setCitiesParkingZones(List<CitiesParkingZone> citiesParkingZones) {
        this.citiesParkingZones = citiesParkingZones;
    }

}
