package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class FullParkingResult extends BaseParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5882483253739906037L;

    private String            parkingId;
    private Date              requestedEndTime;
    private String            parkingTimeCorrection;
    private BigDecimal        priceDifference;

    public String getParkingId() {
        return parkingId;
    }

    public void setParkingId(String parkingId) {
        this.parkingId = parkingId;
    }

    public Date getRequestedEndTime() {
        return requestedEndTime;
    }

    public void setRequestedEndTime(Date requestedEndTime) {
        this.requestedEndTime = requestedEndTime;
    }

    public String getParkingTimeCorrection() {
        return parkingTimeCorrection;
    }

    public void setParkingTimeCorrection(String parkingTimeCorrection) {
        this.parkingTimeCorrection = parkingTimeCorrection;
    }

    public BigDecimal getPriceDifference() {
        return priceDifference;
    }

    public void setPriceDifference(BigDecimal priceDifference) {
        this.priceDifference = priceDifference;
    }

}
