package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;

public class StartParkingResult extends FullParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 242383489485148815L;
}
