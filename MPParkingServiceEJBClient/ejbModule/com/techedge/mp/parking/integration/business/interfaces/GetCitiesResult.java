package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetCitiesResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5477924351519597397L;
    
    private List<ParkingCity> parkingCityList  = new ArrayList<ParkingCity>(0);
    private String            statusCode;

    public List<ParkingCity> getParkingCityList() {
        return parkingCityList;
    }

    public void setParkingCityList(List<ParkingCity> parkingCityList) {
        this.parkingCityList = parkingCityList;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
