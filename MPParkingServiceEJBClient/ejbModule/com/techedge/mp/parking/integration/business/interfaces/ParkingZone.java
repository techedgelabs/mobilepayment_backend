package com.techedge.mp.parking.integration.business.interfaces;

import java.io.Serializable;
import java.util.List;

public class ParkingZone implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1828520203779260150L;

    private String            id;
    private String            cityId;
    private String            name;
    private String            description;
    private String            vendorId;
    private String            vendorName;
    private String            vendorUrl;
    private List<String>      notice;
    private Boolean           stallCodeRequired;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorUrl() {
        return vendorUrl;
    }

    public void setVendorUrl(String vendorUrl) {
        this.vendorUrl = vendorUrl;
    }

    public List<String> getNotice() {
        return notice;
    }

    public void setNotice(List<String> notice) {
        this.notice = notice;
    }

    public Boolean getStallCodeRequired() {
        return stallCodeRequired;
    }

    public void setStallCodeRequired(Boolean stallCodeRequired) {
        this.stallCodeRequired = stallCodeRequired;
    }

}
