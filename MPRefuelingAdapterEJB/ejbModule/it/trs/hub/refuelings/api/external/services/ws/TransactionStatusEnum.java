/**
 * TransactionStatusEnum.java
 * 
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.trs.hub.refuelings.api.external.services.ws;

public class TransactionStatusEnum implements java.io.Serializable {
    private java.lang.String         _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected TransactionStatusEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_, this);
    }

    public static final java.lang.String      _START_REFUELING       = "START_REFUELING";
    public static final java.lang.String      _END_REFUELING         = "END_REFUELING";
    public static final java.lang.String      _REFUELING_IN_PROGRESS = "REFUELING_IN_PROGRESS";
    public static final java.lang.String      _ERROR                 = "ERROR";
    public static final java.lang.String      _RECONCILIATION        = "RECONCILIATION";
    public static final java.lang.String      _AUTH_FAILED_451       = "AUTH_FAILED_451";
    public static final java.lang.String      _AUTH_FAILED_462       = "AUTH_FAILED_462";
    public static final java.lang.String      _AUTH_FAILED_500       = "AUTH_FAILED_500";

    public static final TransactionStatusEnum START_REFUELING        = new TransactionStatusEnum(_START_REFUELING);
    public static final TransactionStatusEnum END_REFUELING          = new TransactionStatusEnum(_END_REFUELING);
    public static final TransactionStatusEnum REFUELING_IN_PROGRESS  = new TransactionStatusEnum(_REFUELING_IN_PROGRESS);
    public static final TransactionStatusEnum ERROR                  = new TransactionStatusEnum(_ERROR);
    public static final TransactionStatusEnum RECONCILIATION         = new TransactionStatusEnum(_RECONCILIATION);
    public static final TransactionStatusEnum AUTH_FAILED_451        = new TransactionStatusEnum(_AUTH_FAILED_451);
    public static final TransactionStatusEnum AUTH_FAILED_462        = new TransactionStatusEnum(_AUTH_FAILED_462);
    public static final TransactionStatusEnum AUTH_FAILED_500        = new TransactionStatusEnum(_AUTH_FAILED_500);

    public java.lang.String getValue() {
        return _value_;
    }

    public static TransactionStatusEnum fromValue(java.lang.String value) throws java.lang.IllegalArgumentException {
        TransactionStatusEnum enumeration = (TransactionStatusEnum) _table_.get(value);
        if (enumeration == null)
            throw new java.lang.IllegalArgumentException();
        return enumeration;
    }

    public static TransactionStatusEnum fromString(java.lang.String value) throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }

    public boolean equals(java.lang.Object obj) {
        return (obj == this);
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public java.lang.String toString() {
        return _value_;
    }

    public java.lang.Object readResolve() throws java.io.ObjectStreamException {
        return fromValue(_value_);
    }

    public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType, java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
        return new org.apache.axis.encoding.ser.EnumSerializer(_javaType, _xmlType);
    }

    public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType, java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
        return new org.apache.axis.encoding.ser.EnumDeserializer(_javaType, _xmlType);
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(TransactionStatusEnum.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "transactionStatusEnum"));
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
