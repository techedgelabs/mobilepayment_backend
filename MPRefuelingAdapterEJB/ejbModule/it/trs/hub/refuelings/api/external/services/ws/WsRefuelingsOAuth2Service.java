/**
 * WsRefuelingsService.java
 * 
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.trs.hub.refuelings.api.external.services.ws;


public interface WsRefuelingsOAuth2Service extends java.rmi.Remote {
    public it.trs.hub.refuelings.api.external.services.ws.MpTransactionNotificationResponse sendMPTransactionNotification(
            it.trs.hub.refuelings.api.external.services.ws.MpTransactionNotificationRequest arg0) throws java.rmi.RemoteException;

    public it.trs.hub.refuelings.api.external.services.ws.MpTransactionReportResponse sendMPTransactionReport(
            it.trs.hub.refuelings.api.external.services.ws.MpTransactionReportRequest arg0) throws java.rmi.RemoteException;
}
