/**
 * WsRefuelingsServiceImplServiceLocator.java
 * 
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.trs.hub.refuelings.api.external.services.ws;

import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.HandlerRegistry;

import com.techedge.mp.refueling.integration.authorization.OAuth2AuthorizationHandler;

public class WsRefuelingsOAuth2ServiceImplServiceLocator extends org.apache.axis.client.Service implements it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsOAuth2ServiceImplService {

    public WsRefuelingsOAuth2ServiceImplServiceLocator() {}

    public WsRefuelingsOAuth2ServiceImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WsRefuelingsOAuth2ServiceImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    @Override
    public HandlerRegistry getHandlerRegistry() {
        HandlerRegistry registry = super.getHandlerRegistry();
        QName portName = new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "WsRefuelingsServiceImplPort");
        List handlerChain = registry.getHandlerChain(portName);

        HandlerInfo hi = new HandlerInfo();
        hi.setHandlerClass(OAuth2AuthorizationHandler.class);
        handlerChain.add(hi);

        return registry;
    }

    // Use to get a proxy class for WsRefuelingsServiceImplPort
    private java.lang.String WsRefuelingsServiceImplPort_address = "https://enjoy.mycarfleet.it/eni/mp/WsRefuelingService";

    public java.lang.String getWsRefuelingsServiceImplPortAddress() {
        return WsRefuelingsServiceImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WsRefuelingsServiceImplPortWSDDServiceName = "WsRefuelingsServiceImplPort";

    public java.lang.String getWsRefuelingsServiceImplPortWSDDServiceName() {
        return WsRefuelingsServiceImplPortWSDDServiceName;
    }

    public void setWsRefuelingsServiceImplPortWSDDServiceName(java.lang.String name) {
        WsRefuelingsServiceImplPortWSDDServiceName = name;
    }

    public it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsOAuth2Service getWsRefuelingsServiceImplPort() throws javax.xml.rpc.ServiceException {
        java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WsRefuelingsServiceImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWsRefuelingsServiceImplPort(endpoint);
    }

    public it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsOAuth2Service getWsRefuelingsServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsOAuth2ServiceImplServiceSoapBindingStub _stub = new it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsOAuth2ServiceImplServiceSoapBindingStub(
                    portAddress, this);
            _stub.setPortName(getWsRefuelingsServiceImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWsRefuelingsServiceImplPortEndpointAddress(java.lang.String address) {
        WsRefuelingsServiceImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsService.class.isAssignableFrom(serviceEndpointInterface)) {
                it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsServiceImplServiceSoapBindingStub _stub = new it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsServiceImplServiceSoapBindingStub(
                        new java.net.URL(WsRefuelingsServiceImplPort_address), this);
                _stub.setPortName(getWsRefuelingsServiceImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
                + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WsRefuelingsServiceImplPort".equals(inputPortName)) {
            return getWsRefuelingsServiceImplPort();
        }
        else {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "WsRefuelingsServiceImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "WsRefuelingsServiceImplPort"));
        }
        return ports.iterator();
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {

        if ("WsRefuelingsServiceImplPort".equals(portName)) {
            setWsRefuelingsServiceImplPortEndpointAddress(address);
        }
        else { // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
