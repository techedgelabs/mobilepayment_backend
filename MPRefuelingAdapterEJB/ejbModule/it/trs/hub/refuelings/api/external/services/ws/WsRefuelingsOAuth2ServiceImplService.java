/**
 * WsRefuelingsServiceImplService.java
 * 
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.trs.hub.refuelings.api.external.services.ws;

public interface WsRefuelingsOAuth2ServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getWsRefuelingsServiceImplPortAddress();

    public it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsOAuth2Service getWsRefuelingsServiceImplPort() throws javax.xml.rpc.ServiceException;

    public it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsOAuth2Service getWsRefuelingsServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
