/**
 * MpTransactionReportRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.trs.hub.refuelings.api.external.services.ws;

public class MpTransactionReportRequest  implements java.io.Serializable {
    private java.lang.String endDate;

    private it.trs.hub.refuelings.api.external.services.ws.TransactionDetail[] mpTransactionList;

    private java.lang.String requestID;

    private java.lang.String startDate;

    public MpTransactionReportRequest() {
    }

    public MpTransactionReportRequest(
           java.lang.String endDate,
           it.trs.hub.refuelings.api.external.services.ws.TransactionDetail[] mpTransactionList,
           java.lang.String requestID,
           java.lang.String startDate) {
           this.endDate = endDate;
           this.mpTransactionList = mpTransactionList;
           this.requestID = requestID;
           this.startDate = startDate;
    }


    /**
     * Gets the endDate value for this MpTransactionReportRequest.
     * 
     * @return endDate
     */
    public java.lang.String getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this MpTransactionReportRequest.
     * 
     * @param endDate
     */
    public void setEndDate(java.lang.String endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the mpTransactionList value for this MpTransactionReportRequest.
     * 
     * @return mpTransactionList
     */
    public it.trs.hub.refuelings.api.external.services.ws.TransactionDetail[] getMpTransactionList() {
        return mpTransactionList;
    }


    /**
     * Sets the mpTransactionList value for this MpTransactionReportRequest.
     * 
     * @param mpTransactionList
     */
    public void setMpTransactionList(it.trs.hub.refuelings.api.external.services.ws.TransactionDetail[] mpTransactionList) {
        this.mpTransactionList = mpTransactionList;
    }

    public it.trs.hub.refuelings.api.external.services.ws.TransactionDetail getMpTransactionList(int i) {
        return this.mpTransactionList[i];
    }

    public void setMpTransactionList(int i, it.trs.hub.refuelings.api.external.services.ws.TransactionDetail _value) {
        this.mpTransactionList[i] = _value;
    }


    /**
     * Gets the requestID value for this MpTransactionReportRequest.
     * 
     * @return requestID
     */
    public java.lang.String getRequestID() {
        return requestID;
    }


    /**
     * Sets the requestID value for this MpTransactionReportRequest.
     * 
     * @param requestID
     */
    public void setRequestID(java.lang.String requestID) {
        this.requestID = requestID;
    }


    /**
     * Gets the startDate value for this MpTransactionReportRequest.
     * 
     * @return startDate
     */
    public java.lang.String getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this MpTransactionReportRequest.
     * 
     * @param startDate
     */
    public void setStartDate(java.lang.String startDate) {
        this.startDate = startDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MpTransactionReportRequest)) return false;
        MpTransactionReportRequest other = (MpTransactionReportRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.mpTransactionList==null && other.getMpTransactionList()==null) || 
             (this.mpTransactionList!=null &&
              java.util.Arrays.equals(this.mpTransactionList, other.getMpTransactionList()))) &&
            ((this.requestID==null && other.getRequestID()==null) || 
             (this.requestID!=null &&
              this.requestID.equals(other.getRequestID()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getMpTransactionList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMpTransactionList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMpTransactionList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRequestID() != null) {
            _hashCode += getRequestID().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MpTransactionReportRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "mpTransactionReportRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mpTransactionList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mpTransactionList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "transactionDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "requestID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
