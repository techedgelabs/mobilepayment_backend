/**
 * TransactionDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.trs.hub.refuelings.api.external.services.ws;

public class TransactionDetail  implements java.io.Serializable {
    private java.lang.String mpTransactionID;

    private it.trs.hub.refuelings.api.external.services.ws.TransactionStatusEnum mpTransactionStatus;

    private it.trs.hub.refuelings.api.external.services.ws.RefuelDetail refuelDetail;

    private java.lang.String srcTransactionID;

    public TransactionDetail() {
    }

    public TransactionDetail(
           java.lang.String mpTransactionID,
           it.trs.hub.refuelings.api.external.services.ws.TransactionStatusEnum mpTransactionStatus,
           it.trs.hub.refuelings.api.external.services.ws.RefuelDetail refuelDetail,
           java.lang.String srcTransactionID) {
           this.mpTransactionID = mpTransactionID;
           this.mpTransactionStatus = mpTransactionStatus;
           this.refuelDetail = refuelDetail;
           this.srcTransactionID = srcTransactionID;
    }


    /**
     * Gets the mpTransactionID value for this TransactionDetail.
     * 
     * @return mpTransactionID
     */
    public java.lang.String getMpTransactionID() {
        return mpTransactionID;
    }


    /**
     * Sets the mpTransactionID value for this TransactionDetail.
     * 
     * @param mpTransactionID
     */
    public void setMpTransactionID(java.lang.String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }


    /**
     * Gets the mpTransactionStatus value for this TransactionDetail.
     * 
     * @return mpTransactionStatus
     */
    public it.trs.hub.refuelings.api.external.services.ws.TransactionStatusEnum getMpTransactionStatus() {
        return mpTransactionStatus;
    }


    /**
     * Sets the mpTransactionStatus value for this TransactionDetail.
     * 
     * @param mpTransactionStatus
     */
    public void setMpTransactionStatus(it.trs.hub.refuelings.api.external.services.ws.TransactionStatusEnum mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }


    /**
     * Gets the refuelDetail value for this TransactionDetail.
     * 
     * @return refuelDetail
     */
    public it.trs.hub.refuelings.api.external.services.ws.RefuelDetail getRefuelDetail() {
        return refuelDetail;
    }


    /**
     * Sets the refuelDetail value for this TransactionDetail.
     * 
     * @param refuelDetail
     */
    public void setRefuelDetail(it.trs.hub.refuelings.api.external.services.ws.RefuelDetail refuelDetail) {
        this.refuelDetail = refuelDetail;
    }


    /**
     * Gets the srcTransactionID value for this TransactionDetail.
     * 
     * @return srcTransactionID
     */
    public java.lang.String getSrcTransactionID() {
        return srcTransactionID;
    }


    /**
     * Sets the srcTransactionID value for this TransactionDetail.
     * 
     * @param srcTransactionID
     */
    public void setSrcTransactionID(java.lang.String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionDetail)) return false;
        TransactionDetail other = (TransactionDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.mpTransactionID==null && other.getMpTransactionID()==null) || 
             (this.mpTransactionID!=null &&
              this.mpTransactionID.equals(other.getMpTransactionID()))) &&
            ((this.mpTransactionStatus==null && other.getMpTransactionStatus()==null) || 
             (this.mpTransactionStatus!=null &&
              this.mpTransactionStatus.equals(other.getMpTransactionStatus()))) &&
            ((this.refuelDetail==null && other.getRefuelDetail()==null) || 
             (this.refuelDetail!=null &&
              this.refuelDetail.equals(other.getRefuelDetail()))) &&
            ((this.srcTransactionID==null && other.getSrcTransactionID()==null) || 
             (this.srcTransactionID!=null &&
              this.srcTransactionID.equals(other.getSrcTransactionID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMpTransactionID() != null) {
            _hashCode += getMpTransactionID().hashCode();
        }
        if (getMpTransactionStatus() != null) {
            _hashCode += getMpTransactionStatus().hashCode();
        }
        if (getRefuelDetail() != null) {
            _hashCode += getRefuelDetail().hashCode();
        }
        if (getSrcTransactionID() != null) {
            _hashCode += getSrcTransactionID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "transactionDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mpTransactionID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mpTransactionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mpTransactionStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mpTransactionStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "transactionStatusEnum"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refuelDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refuelDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "refuelDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("srcTransactionID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "srcTransactionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
