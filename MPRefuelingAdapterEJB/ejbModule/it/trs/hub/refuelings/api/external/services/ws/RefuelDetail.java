/**
 * RefuelDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.trs.hub.refuelings.api.external.services.ws;

public class RefuelDetail  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1099907202749412939L;

    private java.lang.Double amount;
    
    private java.lang.String authorizationCode;

    private java.lang.Double fuelQuantity;

    private java.lang.String fuelType;

    private java.lang.String productDescription;

    private it.trs.hub.refuelings.api.external.services.ws.ProductIdEnum productId;

    private java.lang.String timestampEndRefuel;

    private java.lang.String timestampStartRefuel;

    public RefuelDetail() {
    }

    public RefuelDetail(
           java.lang.Double amount,
           java.lang.String authorizationCode,
           java.lang.Double fuelQuantity,
           java.lang.String fuelType,
           java.lang.String productDescription,
           it.trs.hub.refuelings.api.external.services.ws.ProductIdEnum productId,
           java.lang.String timestampEndRefuel,
           java.lang.String timestampStartRefuel) {
           this.amount = amount;
           this.authorizationCode = authorizationCode;
           this.fuelQuantity = fuelQuantity;
           this.fuelType = fuelType;
           this.productDescription = productDescription;
           this.productId = productId;
           this.timestampEndRefuel = timestampEndRefuel;
           this.timestampStartRefuel = timestampStartRefuel;
    }


    /**
     * Gets the amount value for this RefuelDetail.
     * 
     * @return amount
     */
    public java.lang.Double getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this RefuelDetail.
     * 
     * @param amount
     */
    public void setAmount(java.lang.Double amount) {
        this.amount = amount;
    }
    
    
    /**
     * Gets the authorizationCode value for this RefuelDetail.
     * 
     * @return authorizationCode
     */
    public java.lang.String getAuthorizationCode() {
        return authorizationCode;
    }

    /**
     * Sets the authorizationCode value for this RefuelDetail.
     * 
     * @param authorizationCode
     */
    public void setAuthorizationCode(java.lang.String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    /**
     * Gets the fuelQuantity value for this RefuelDetail.
     * 
     * @return fuelQuantity
     */
    public java.lang.Double getFuelQuantity() {
        return fuelQuantity;
    }


    /**
     * Sets the fuelQuantity value for this RefuelDetail.
     * 
     * @param fuelQuantity
     */
    public void setFuelQuantity(java.lang.Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }


    /**
     * Gets the fuelType value for this RefuelDetail.
     * 
     * @return fuelType
     */
    public java.lang.String getFuelType() {
        return fuelType;
    }


    /**
     * Sets the fuelType value for this RefuelDetail.
     * 
     * @param fuelType
     */
    public void setFuelType(java.lang.String fuelType) {
        this.fuelType = fuelType;
    }


    /**
     * Gets the productDescription value for this RefuelDetail.
     * 
     * @return productDescription
     */
    public java.lang.String getProductDescription() {
        return productDescription;
    }


    /**
     * Sets the productDescription value for this RefuelDetail.
     * 
     * @param productDescription
     */
    public void setProductDescription(java.lang.String productDescription) {
        this.productDescription = productDescription;
    }


    /**
     * Gets the productId value for this RefuelDetail.
     * 
     * @return productId
     */
    public it.trs.hub.refuelings.api.external.services.ws.ProductIdEnum getProductId() {
        return productId;
    }


    /**
     * Sets the productId value for this RefuelDetail.
     * 
     * @param productId
     */
    public void setProductId(it.trs.hub.refuelings.api.external.services.ws.ProductIdEnum productId) {
        this.productId = productId;
    }


    /**
     * Gets the timestampEndRefuel value for this RefuelDetail.
     * 
     * @return timestampEndRefuel
     */
    public java.lang.String getTimestampEndRefuel() {
        return timestampEndRefuel;
    }


    /**
     * Sets the timestampEndRefuel value for this RefuelDetail.
     * 
     * @param timestampEndRefuel
     */
    public void setTimestampEndRefuel(java.lang.String timestampEndRefuel) {
        this.timestampEndRefuel = timestampEndRefuel;
    }


    /**
     * Gets the timestampStartRefuel value for this RefuelDetail.
     * 
     * @return timestampStartRefuel
     */
    public java.lang.String getTimestampStartRefuel() {
        return timestampStartRefuel;
    }


    /**
     * Sets the timestampStartRefuel value for this RefuelDetail.
     * 
     * @param timestampStartRefuel
     */
    public void setTimestampStartRefuel(java.lang.String timestampStartRefuel) {
        this.timestampStartRefuel = timestampStartRefuel;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RefuelDetail)) return false;
        RefuelDetail other = (RefuelDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.authorizationCode==null && other.getAuthorizationCode()==null) || 
             (this.authorizationCode!=null &&
              this.authorizationCode.equals(other.getAuthorizationCode()))) &&
            ((this.fuelQuantity==null && other.getFuelQuantity()==null) || 
             (this.fuelQuantity!=null &&
              this.fuelQuantity.equals(other.getFuelQuantity()))) &&
            ((this.fuelType==null && other.getFuelType()==null) || 
             (this.fuelType!=null &&
              this.fuelType.equals(other.getFuelType()))) &&
            ((this.productDescription==null && other.getProductDescription()==null) || 
             (this.productDescription!=null &&
              this.productDescription.equals(other.getProductDescription()))) &&
            ((this.productId==null && other.getProductId()==null) || 
             (this.productId!=null &&
              this.productId.equals(other.getProductId()))) &&
            ((this.timestampEndRefuel==null && other.getTimestampEndRefuel()==null) || 
             (this.timestampEndRefuel!=null &&
              this.timestampEndRefuel.equals(other.getTimestampEndRefuel()))) &&
            ((this.timestampStartRefuel==null && other.getTimestampStartRefuel()==null) || 
             (this.timestampStartRefuel!=null &&
              this.timestampStartRefuel.equals(other.getTimestampStartRefuel())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getAuthorizationCode() != null) {
            _hashCode += getAuthorizationCode().hashCode();
        }
        if (getFuelQuantity() != null) {
            _hashCode += getFuelQuantity().hashCode();
        }
        if (getFuelType() != null) {
            _hashCode += getFuelType().hashCode();
        }
        if (getProductDescription() != null) {
            _hashCode += getProductDescription().hashCode();
        }
        if (getProductId() != null) {
            _hashCode += getProductId().hashCode();
        }
        if (getTimestampEndRefuel() != null) {
            _hashCode += getTimestampEndRefuel().hashCode();
        }
        if (getTimestampStartRefuel() != null) {
            _hashCode += getTimestampStartRefuel().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RefuelDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "refuelDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "authorizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fuelQuantity");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fuelQuantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fuelType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fuelType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.services.external.api.refuelings.hub.trs.it/", "productIdEnum"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timestampEndRefuel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "timestampEndRefuel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timestampStartRefuel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "timestampStartRefuel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
