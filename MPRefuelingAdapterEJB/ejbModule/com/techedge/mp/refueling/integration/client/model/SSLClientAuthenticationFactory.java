package com.techedge.mp.refueling.integration.client.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Hashtable;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.axis.components.net.JSSESocketFactory;
import org.apache.axis.components.net.SecureSocketFactory;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.refueling.integration.business.EJBHomeCache;

public class SSLClientAuthenticationFactory extends JSSESocketFactory implements SecureSocketFactory {

    private final String            SSL_VERSION             = "TLSv1.2";
    private final String            KEY_MANAGER_ALGORITHM   = "SunX509";
    private final String            KEY_STORE_TYPE          = "JKS";
    private String                  keyStore                = null;                   //"/opt/enimp";
    private String                  keyStorePassword        = null;                   //"enimp2017";
    private String                  keyPassword             = null;                   //"enimp2017";

    private final static String     PARAM_KEYSTORE_PATH     = "REFUELING_KEYSTORE_PATH";
    private final static String     PARAM_KEYSTORE_PASSWORD = "REFUELING_KEYSTORE_PASSWORD";
    private final static String     PARAM_KEY_PASSWORD      = "REFUELING_KEY_PASSWORD";

    private ParametersServiceRemote parametersService;

    public SSLClientAuthenticationFactory(Hashtable attributes) {
        super(attributes);

        try {
            parametersService = EJBHomeCache.getInstance().getParametersService();
            this.keyStore = parametersService.getParamValue(PARAM_KEYSTORE_PATH);
            this.keyStorePassword = parametersService.getParamValue(PARAM_KEYSTORE_PASSWORD);
            this.keyPassword = parametersService.getParamValue(PARAM_KEY_PASSWORD);
        }
        catch (InterfaceNotFoundException ex) {
            System.err.println("ParametersService not found: " + ex.getMessage());
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        System.out.println("SSLClientAuthenticationFactory->keyStore: " + keyStore);
        System.out.println("SSLClientAuthenticationFactory->keyStorePassword: " + keyStorePassword);
        System.out.println("SSLClientAuthenticationFactory->keyPassword: " + keyPassword);
    }

    protected void initFactory() throws IOException {
        SSLContext context = getContext();
        if (context != null) {
            this.sslFactory = context.getSocketFactory();
        }
    }
    
    private HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, javax.net.ssl.SSLSession session) {
            return true;
        }
    };
    
    private static class TrustAllCertificates implements X509TrustManager {
        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }
     
        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
     
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    protected SSLContext getContext() {
        SSLContext sslcontext = null;
        try {
            sslcontext = SSLContext.getInstance(SSL_VERSION);
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KEY_MANAGER_ALGORITHM);
            KeyStore ks = KeyStore.getInstance(KEY_STORE_TYPE);
            ks.load(new FileInputStream(keyStore), keyStorePassword.toCharArray());
            kmf.init(ks, keyPassword.toCharArray());

            TrustManager[] tm = null;
            tm = new TrustManager[] {new TrustAllCertificates()};
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            
            sslcontext.init(kmf.getKeyManagers(), tm, null);
        }
        catch (Exception ex) {
            System.err.println("Errore nella costruzione del SSLContext: " + ex.getMessage());
        }

        return sslcontext;
    }

}
