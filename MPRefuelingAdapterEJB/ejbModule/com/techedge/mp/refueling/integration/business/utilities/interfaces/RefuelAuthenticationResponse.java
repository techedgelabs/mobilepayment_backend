package com.techedge.mp.refueling.integration.business.utilities.interfaces;

import java.io.Serializable;

public class RefuelAuthenticationResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1255892535437624019L;

    private UserData          userData;
    private String            statusCode;
    private String            statusMessage;

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

}
