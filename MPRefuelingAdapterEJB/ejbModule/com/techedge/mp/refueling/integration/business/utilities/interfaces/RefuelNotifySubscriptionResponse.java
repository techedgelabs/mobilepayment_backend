package com.techedge.mp.refueling.integration.business.utilities.interfaces;

import java.io.Serializable;

public class RefuelNotifySubscriptionResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2947843078894030937L;
    
    private String            statusCode;
    private String            statusMessage;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

}
