package com.techedge.mp.refueling.integration.business.utilities.interfaces;

import java.io.Serializable;

public class UserData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8888290951177879116L;

    private AddressData       addressData;
    private String            firstname;
    private String            lastname;
    private String            dateofbirth;
    private String            salutationid;
    private String            birthmunicipality;
    private String            birthprovince;
    private String            fiscalcode;

    public AddressData getAddressData() {
        return addressData;
    }

    public void setAddressData(AddressData addressData) {
        this.addressData = addressData;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getSalutationid() {
        return salutationid;
    }

    public void setSalutationid(String salutationid) {
        this.salutationid = salutationid;
    }

    public String getBirthmunicipality() {
        return birthmunicipality;
    }

    public void setBirthmunicipality(String birthmunicipality) {
        this.birthmunicipality = birthmunicipality;
    }

    public String getBirthprovince() {
        return birthprovince;
    }

    public void setBirthprovince(String birthprovince) {
        this.birthprovince = birthprovince;
    }

    public String getFiscalcode() {
        return fiscalcode;
    }

    public void setFiscalcode(String fiscalcode) {
        this.fiscalcode = fiscalcode;
    }

}
