package com.techedge.mp.refueling.integration.business;

import it.trs.hub.refuelings.api.external.services.ws.MpTransactionNotificationRequest;
import it.trs.hub.refuelings.api.external.services.ws.MpTransactionNotificationResponse;
import it.trs.hub.refuelings.api.external.services.ws.MpTransactionReportResponse;
import it.trs.hub.refuelings.api.external.services.ws.ProductIdEnum;
import it.trs.hub.refuelings.api.external.services.ws.TransactionDetail;
import it.trs.hub.refuelings.api.external.services.ws.TransactionStatusEnum;
import it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsService;
import it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsServiceProxy;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.refueling.integration.business.utilities.RefuelingJsonClient;
import com.techedge.mp.refueling.integration.business.utilities.interfaces.RefuelAuthenticationResult;
import com.techedge.mp.refueling.integration.business.utilities.interfaces.RefuelNotifySubscriptionResult;
import com.techedge.mp.refueling.integration.entities.MpTransactionDetail;
import com.techedge.mp.refueling.integration.entities.NotifySubscriptionResult;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;
import com.techedge.mp.refueling.integration.entities.RefuelingAuthentiationResult;
import com.techedge.mp.refueling.integration.utility.UtilityCheck;

/**
 * Session Bean implementation class RefuelingNotificationService
 */
@Stateless
@LocalBean
public class RefuelingNotificationService implements RefuelingNotificationServiceRemote, RefuelingNotificationServiceLocal {

    private final static String     PARAM_REFUELING_WSDL                  = "REFUELING_WSDL";
    private final static String     PARAM_REFUELING_JSON_BASE_URL         = "REFUELING_JSON_BASE_URL";
    private final static String     PARAM_REFUELING_JSON_USERNAME         = "REFUELING_JSON_USERNAME";
    private final static String     PARAM_REFUELING_JSON_PASSWORD         = "REFUELING_JSON_PASSWORD";

    private final static String     PARAM_PROXY_HOST                      = "PROXY_HOST";
    private final static String     PARAM_PROXY_PORT                      = "PROXY_PORT";
    private final static String     PARAM_PROXY_NO_HOSTS                  = "PROXY_NO_HOSTS";

    private String                  proxyHost                             = "mpsquid.enimp.pri";
    private Integer                 proxyPort                             = 3128;
    private String                  proxyNoHosts                          = "localhost|127.0.0.1|*.enimp.pri";
    
    private LoggerServiceRemote     loggerService                         = null;
    private ParametersServiceRemote parametersService                     = null;

    /**
     * Default constructor.
     */
    public RefuelingNotificationService() {
        
        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Service not found: " + e.getMessage());
        }
        
        try {
            this.proxyHost = parametersService.getParamValue(PARAM_PROXY_HOST);
            this.proxyPort = Integer.parseInt(parametersService.getParamValue(PARAM_PROXY_PORT));
            this.proxyNoHosts = parametersService.getParamValue(PARAM_PROXY_NO_HOSTS);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }
    }
    
    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    @Override
    public String sendMPTransactionNotification(String requestID, String srcTransactionID, String mpTransactionID, String mpTransactionStatus, RefuelDetail refuelDetail) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("srcTransactionID", srcTransactionID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("mpTransactionStatus", mpTransactionStatus));
        inputParameters.add(new Pair<String, String>("refuelDetail", refuelDetail.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionNotification", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        MpTransactionNotificationResponse transactionNotificationResponse = new MpTransactionNotificationResponse();


        WsRefuelingsServiceProxy proxy = new WsRefuelingsServiceProxy(getUrl());

        try {
            
            WsRefuelingsService refuelingsService = proxy.getWsRefuelingsService();

            it.trs.hub.refuelings.api.external.services.ws.MpTransactionNotificationRequest transactionNotificationRequest = new MpTransactionNotificationRequest();

            transactionNotificationRequest.setRequestID(requestID);
            transactionNotificationRequest.setMpTransactionID(mpTransactionID);
            transactionNotificationRequest.setSrcTransactionID(srcTransactionID);
            transactionNotificationRequest.setMpTransactionStatus(TransactionStatusEnum.fromValue(mpTransactionStatus));

            it.trs.hub.refuelings.api.external.services.ws.RefuelDetail refuelDetailWs = convertRefuelDetail(refuelDetail);
            transactionNotificationRequest.setRefuelDetail(refuelDetailWs);

            Proxy httpProxy = new Proxy(this.proxyHost, String.valueOf(this.proxyPort), this.proxyNoHosts);

            httpProxy.setHttp();
            
            transactionNotificationResponse = refuelingsService.sendMPTransactionNotification(transactionNotificationRequest);
            
            httpProxy.unsetHttp();
        }
        catch (RemoteException e) {
            e.printStackTrace();

            return "SYSTEM_ERROR_500";
        }
        
        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        String statusCode = "";
        if (transactionNotificationResponse.getStatusCode() != null) {
            statusCode = transactionNotificationResponse.getStatusCode().getValue();
        }
        else {
            System.err.println("sendMPTransactionNotification.statusCode null -> SYSTEM_ERROR_500");
            statusCode = "SYSTEM_ERROR_500";
        }
        outputParameters.add(new Pair<String, String>("statusCode", statusCode));
        outputParameters.add(new Pair<String, String>("messageCode", transactionNotificationResponse.getMessageCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionNotification", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return statusCode;
    }

    private String getUrl() {

        String wsdlString = "";
        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
            wsdlString = parametersService.getParamValue(PARAM_REFUELING_WSDL);
        }
        catch (InterfaceNotFoundException e) {

            e.printStackTrace();
        }
        catch (ParameterNotFoundException e) {

            e.printStackTrace();
        }

        return wsdlString;
    }

    @Override
    public String sendMPTransactionReport(String requestID, String startDate, String endDate, List<MpTransactionDetail> mpTransactionList) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("startDate", startDate));
        inputParameters.add(new Pair<String, String>("endDate", endDate));
        inputParameters.add(new Pair<String, String>("mpTransactionList size", String.valueOf(mpTransactionList.size())));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionReport", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        //FIXME usato per controllare che UtilityCheck sia visibile qualora dovessero servire dei check.
        Date date = new Date();
        try {

            date = UtilityCheck.convertStringToDate(startDate);
        }
        catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        MpTransactionReportResponse transactionReportResponse = new MpTransactionReportResponse();
        
        WsRefuelingsServiceProxy proxy = new WsRefuelingsServiceProxy(getUrl());

        try {
            
            WsRefuelingsService refuelingsService = proxy.getWsRefuelingsService();

            it.trs.hub.refuelings.api.external.services.ws.MpTransactionReportRequest transactionReportRequest = new it.trs.hub.refuelings.api.external.services.ws.MpTransactionReportRequest();

            transactionReportRequest.setRequestID(requestID);
            transactionReportRequest.setStartDate(startDate);
            transactionReportRequest.setEndDate(endDate);

            List<TransactionDetail> transactionDetailArray = new ArrayList<TransactionDetail>();

            for (MpTransactionDetail mpTransaction : mpTransactionList) {

                it.trs.hub.refuelings.api.external.services.ws.TransactionDetail transDetail = new it.trs.hub.refuelings.api.external.services.ws.TransactionDetail();

                transDetail.setSrcTransactionID(mpTransaction.getSrcTransactionID());
                transDetail.setMpTransactionID(mpTransaction.getMpTransactionID());
                transDetail.setMpTransactionStatus(TransactionStatusEnum.fromValue(mpTransaction.getMpTransactionStatus()));

                it.trs.hub.refuelings.api.external.services.ws.RefuelDetail refDetail = convertRefuelDetail(mpTransaction.getRefuelDetail());
                transDetail.setRefuelDetail(refDetail);

                transactionDetailArray.add(transDetail);

            }

            Object[] objectArray = transactionDetailArray.toArray();
            it.trs.hub.refuelings.api.external.services.ws.TransactionDetail[] transactionDetail = Arrays.copyOf(objectArray, objectArray.length, TransactionDetail[].class);
            transactionReportRequest.setMpTransactionList(transactionDetail);

            Proxy httpProxy = new Proxy(this.proxyHost, String.valueOf(this.proxyPort), this.proxyNoHosts);

            httpProxy.setHttp();
            
            transactionReportResponse = refuelingsService.sendMPTransactionReport(transactionReportRequest);
            
            httpProxy.unsetHttp();
        }
        catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            
            return "SYSTEM_ERROR_500";
        }
        catch (EJBTransactionRolledbackException e) {
            System.out.println("Exception caught");
            
            return "SYSTEM_ERROR_500";
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", transactionReportResponse.getStatusCode().getValue()));
        outputParameters.add(new Pair<String, String>("messageCode", transactionReportResponse.getMessageCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionReport", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return transactionReportResponse.getStatusCode().getValue();
    }

    private it.trs.hub.refuelings.api.external.services.ws.RefuelDetail convertRefuelDetail(RefuelDetail refuelDetail) {

        it.trs.hub.refuelings.api.external.services.ws.RefuelDetail returnedValue = new it.trs.hub.refuelings.api.external.services.ws.RefuelDetail();

        returnedValue.setAmount(refuelDetail.getAmount());
        returnedValue.setAuthorizationCode(refuelDetail.getAuthorizationCode());
        returnedValue.setFuelQuantity(refuelDetail.getFuelQuantity());
        returnedValue.setFuelType(refuelDetail.getFuelType());
        returnedValue.setProductDescription(refuelDetail.getProductDescription());
        returnedValue.setTimestampEndRefuel(refuelDetail.getTimestampEndRefuel());
        returnedValue.setTimestampStartRefuel(refuelDetail.getTimestampStartRefuel());
        if (refuelDetail.getProductID() != null && !refuelDetail.getProductID().isEmpty()) {
            returnedValue.setProductId(ProductIdEnum.fromValue(refuelDetail.getProductID()));
        }

        return returnedValue;
    }

    @Override
    public RefuelingAuthentiationResult refuelingAuthentication(String requestID, String username, String password) {
        // A partire da username e password recupero l'anagrafica utente da enjoy per poi ultimare la registrazione su enistation_plus con le info mancanti

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("username", username));
        inputParameters.add(new Pair<String, String>("password", password));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refuelingAuthentication", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        RefuelingAuthentiationResult refuelingAuthentiationResult = new RefuelingAuthentiationResult();

        try {

            if (this.parametersService == null) {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
            }

            String baseUrl = parametersService.getParamValue(RefuelingNotificationService.PARAM_REFUELING_JSON_BASE_URL);
            String basicAuthUsername = parametersService.getParamValue(RefuelingNotificationService.PARAM_REFUELING_JSON_USERNAME);
            String basicAuthPassword = parametersService.getParamValue(RefuelingNotificationService.PARAM_REFUELING_JSON_PASSWORD);

            Proxy httpProxy = new Proxy(this.proxyHost, String.valueOf(this.proxyPort), this.proxyNoHosts);

            httpProxy.setHttp();
            
            RefuelAuthenticationResult refuelAuthenticationResult = RefuelingJsonClient.authentication(requestID, username, password, baseUrl, basicAuthUsername, basicAuthPassword, null, null, null, null);
            
            httpProxy.unsetHttp();

            if (refuelAuthenticationResult == null) {

                refuelingAuthentiationResult.setStatusCode("SYSTEM_ERROR_500");
            }
            else {

                if (refuelAuthenticationResult.getStatusCode().equals("USR-AUTH-200")) {

                    refuelingAuthentiationResult.setName(refuelAuthenticationResult.getFirstname());
                    refuelingAuthentiationResult.setSurname(refuelAuthenticationResult.getLastname());
                    refuelingAuthentiationResult.setDateOfBirth(refuelAuthenticationResult.getBirthdate());
                    refuelingAuthentiationResult.setBirthMunicipality(refuelAuthenticationResult.getBirthmunicipality());
                    refuelingAuthentiationResult.setBirthProvince(refuelAuthenticationResult.getBirthprovince());
                    refuelingAuthentiationResult.setEmail(refuelAuthenticationResult.getEmail());
                    refuelingAuthentiationResult.setFiscalCode(refuelAuthenticationResult.getFiscalCode());
                    refuelingAuthentiationResult.setPhoneNumber(refuelAuthenticationResult.getMobilePhoneNumber());
                    refuelingAuthentiationResult.setPrefixPhoneNumber(refuelAuthenticationResult.getMobilePhoneNumberPrefix());
                    refuelingAuthentiationResult.setSex(refuelAuthenticationResult.getSex());

                    refuelingAuthentiationResult.setStatusCode(refuelAuthenticationResult.getStatusCode());
                }
                else {

                    refuelingAuthentiationResult.setStatusCode(refuelAuthenticationResult.getStatusCode());
                }
            }

        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            refuelingAuthentiationResult.setStatusCode("SYSTEM_ERROR_500");
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("name", refuelingAuthentiationResult.getName()));
        outputParameters.add(new Pair<String, String>("surname", refuelingAuthentiationResult.getSurname()));
        if (refuelingAuthentiationResult.getDateOfBirth() != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            outputParameters.add(new Pair<String, String>("dateOfBirth", formatter.format(refuelingAuthentiationResult.getDateOfBirth())));
        }
        else {
            outputParameters.add(new Pair<String, String>("dateOfBirth", "null"));
        }
        outputParameters.add(new Pair<String, String>("birthMunicipality", refuelingAuthentiationResult.getBirthMunicipality()));
        outputParameters.add(new Pair<String, String>("birthProvince", refuelingAuthentiationResult.getBirthProvince()));
        outputParameters.add(new Pair<String, String>("email", refuelingAuthentiationResult.getEmail()));
        outputParameters.add(new Pair<String, String>("fiscalCode", refuelingAuthentiationResult.getFiscalCode()));
        outputParameters.add(new Pair<String, String>("phoneNumber", refuelingAuthentiationResult.getPhoneNumber()));
        outputParameters.add(new Pair<String, String>("prefixPhoneNumber", refuelingAuthentiationResult.getPrefixPhoneNumber()));
        outputParameters.add(new Pair<String, String>("sex", refuelingAuthentiationResult.getSex()));
        outputParameters.add(new Pair<String, String>("statusCode", refuelingAuthentiationResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "refuelingAuthentication", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return refuelingAuthentiationResult;
    }

    @Override
    public NotifySubscriptionResult notifySubscription(String requestID, String fiscalCode) {
        // Notifica al backend Enjoy che l'utente con codice fiscale fiscalCode ha completato la registrazione su Eni Station +

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "notifySubscription", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        NotifySubscriptionResult notifySubscriptionResult = new NotifySubscriptionResult();

        try {

            if (this.parametersService == null) {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
            }

            String baseUrl = parametersService.getParamValue(RefuelingNotificationService.PARAM_REFUELING_JSON_BASE_URL);
            String basicAuthUsername = parametersService.getParamValue(RefuelingNotificationService.PARAM_REFUELING_JSON_USERNAME);
            String basicAuthPassword = parametersService.getParamValue(RefuelingNotificationService.PARAM_REFUELING_JSON_PASSWORD);

            Proxy httpProxy = new Proxy(this.proxyHost, String.valueOf(this.proxyPort), this.proxyNoHosts);

            httpProxy.setHttp();
            
            RefuelNotifySubscriptionResult refuelNotifySubscriptionResult = RefuelingJsonClient.notifySubscription(requestID, fiscalCode, baseUrl, basicAuthUsername,
                    basicAuthPassword, null, null, null, null);

            httpProxy.unsetHttp();
            
            if (refuelNotifySubscriptionResult == null) {

                notifySubscriptionResult.setStatusCode(StatusHelper.REFUELING_SUBSCRIPTION_ERROR);
                notifySubscriptionResult.setStatusMessage("System error");
            }
            else {

                notifySubscriptionResult.setStatusCode(refuelNotifySubscriptionResult.getStatusCode());
                notifySubscriptionResult.setStatusMessage(refuelNotifySubscriptionResult.getStatusMessage());
            }

        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            notifySubscriptionResult.setStatusCode(StatusHelper.REFUELING_SUBSCRIPTION_ERROR);
            notifySubscriptionResult.setStatusMessage("System error");
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", notifySubscriptionResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", notifySubscriptionResult.getStatusMessage()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "notifySubscription", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return notifySubscriptionResult;
    }

}
