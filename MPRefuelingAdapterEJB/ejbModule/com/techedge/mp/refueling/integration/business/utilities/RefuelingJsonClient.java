package com.techedge.mp.refueling.integration.business.utilities;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.DatatypeConverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.refueling.integration.business.utilities.interfaces.RefuelAuthenticationRequest;
import com.techedge.mp.refueling.integration.business.utilities.interfaces.RefuelAuthenticationResponse;
import com.techedge.mp.refueling.integration.business.utilities.interfaces.RefuelAuthenticationResult;
import com.techedge.mp.refueling.integration.business.utilities.interfaces.RefuelNotifySubscriptionRequest;
import com.techedge.mp.refueling.integration.business.utilities.interfaces.RefuelNotifySubscriptionResponse;
import com.techedge.mp.refueling.integration.business.utilities.interfaces.RefuelNotifySubscriptionResult;


public class RefuelingJsonClient {

    public static Gson       gson = new GsonBuilder().disableHtmlEscaping().create();
    
    private final static String OPERATOR_ID                     = "1";
    private final static String AUTHENTICATION_SERCVICE_URL     = "authentication";
    private final static String NOTIFY_SUBSCRIPTION_SERVICE_URL = "notifySubscription";
    
    private final static String SSL_VERSION                     = "TLSv1.2";
    private final static String KEY_MANAGER_ALGORITHM           = "SunX509";
    private final static String KEY_STORE_TYPE                  = "JKS";
    
    public static RefuelAuthenticationResult authentication(String requestID, String username, String password, String baseUrl, String basicAuthUsername, String basicAuthPassword, String oauth2AccessToken, String keyStore, String keyStorePassword, String keyPassword) throws IOException {

        RefuelAuthenticationResult refuelAuthenticationResult = null;
        
        TrustManager[] trustAllCerts = new X509TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
        };

        // Install the all-trusting trust manager
        if (keyStore != null && keyStorePassword != null && keyPassword != null) {
            
            System.out.println("Chiamata con mutua autenticazione");
            
            System.out.println("keyStore:         " + keyStore);
            System.out.println("keyStorePassword: " + keyStorePassword);
            System.out.println("keyPassword:      " + keyPassword);
            
            SSLContext sslcontext = null;
            try {
                sslcontext = SSLContext.getInstance(SSL_VERSION);
                KeyManagerFactory kmf = KeyManagerFactory.getInstance(KEY_MANAGER_ALGORITHM);
                KeyStore ks = KeyStore.getInstance(KEY_STORE_TYPE);
                ks.load(new FileInputStream(keyStore), keyStorePassword.toCharArray());
                kmf.init(ks, keyPassword.toCharArray());

                sslcontext.init(kmf.getKeyManagers(), trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
            }
            catch (Exception ex) {
                System.err.println("Errore nella costruzione del SSLContext: " + ex.getMessage());
            }
        }
        else {
            
            System.out.println("Chiamata senza mutua autenticazione");
            
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        
        String url = baseUrl + RefuelingJsonClient.AUTHENTICATION_SERCVICE_URL;
        URL obj = new URL(url);
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) obj.openConnection();
        
        httpsURLConnection.setRequestMethod("POST");
        
        RefuelAuthenticationRequest refuelAuthenticationRequest = new RefuelAuthenticationRequest();
        refuelAuthenticationRequest.setOperatorID(RefuelingJsonClient.OPERATOR_ID);
        refuelAuthenticationRequest.setRequestID(requestID);
        refuelAuthenticationRequest.setUsername(username);
        refuelAuthenticationRequest.setPassword(password);
        
        String jsonRequest = gson.toJson(refuelAuthenticationRequest);

        //add request header
        if (oauth2AccessToken != null && !oauth2AccessToken.isEmpty()) {
            String oauth2 = "Bearer " + oauth2AccessToken;
            httpsURLConnection.setRequestProperty ("Authorization", oauth2);
        }
        else {
            String userCredentials = basicAuthUsername + ":" + basicAuthPassword;
            String encodedUserCredentials = DatatypeConverter.printBase64Binary(userCredentials.getBytes("UTF-8"));
            String basicAuth = "Basic " + encodedUserCredentials;
            httpsURLConnection.setRequestProperty ("Authorization", basicAuth);
        }
        httpsURLConnection.setRequestProperty("Content-Type", "application/json");
        httpsURLConnection.setRequestProperty("Charset", "utf-8");
        //con.setUseCaches(false);
        //con.setDoInput(true);
        httpsURLConnection.setDoOutput(true);
        
        /*
        Map<String, List<String>> headerFields = httpsURLConnection.getHeaderFields();
        
        //Map<String, String> map = ...
        for (Map.Entry<String, List<String>> headerField : headerFields.entrySet())
        {
            System.out.println("key: " + headerField.getKey());
            for(String value : headerField.getValue()) {
                System.out.println(value);
            }
        }
        */
        
        // Send post request
        DataOutputStream wr = new DataOutputStream(httpsURLConnection.getOutputStream());
        wr.writeBytes(jsonRequest);
        wr.flush();
        wr.close();

        int responseCode = httpsURLConnection.getResponseCode();
        System.out.println("\nSending 'POST' request to URL: " + url);
        System.out.println("Request: " + jsonRequest);
        System.out.println("Response Code: " + responseCode);
        
        /*
        Map<String, List<String>> headerFields = httpsURLConnection.getHeaderFields();
        
        for (Map.Entry<String, List<String>> headerField : headerFields.entrySet())
        {
            System.out.println("key: " + headerField.getKey());
            for(String value : headerField.getValue()) {
                System.out.println(value);
            }
        }
        */

        BufferedReader in = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        String jsonResponse = response.toString();
        System.out.println("Response: " + jsonResponse);
        
        try {
            RefuelAuthenticationResponse refuelAuthenticationResponse = gson.fromJson(jsonResponse, RefuelAuthenticationResponse.class);
            
            if (refuelAuthenticationResponse.getStatusCode().equals("USR-AUTH-200")) {
                
                refuelAuthenticationResult = new RefuelAuthenticationResult();
                
                if (refuelAuthenticationResponse.getUserData() != null) {
                    
                    if (refuelAuthenticationResponse.getUserData().getAddressData() != null) {
                        
                        refuelAuthenticationResult.setEmail(refuelAuthenticationResponse.getUserData().getAddressData().getMail());
                        refuelAuthenticationResult.setMobilePhoneNumber(refuelAuthenticationResponse.getUserData().getAddressData().getMobilephonenumber());
                        refuelAuthenticationResult.setMobilePhoneNumberPrefix(refuelAuthenticationResponse.getUserData().getAddressData().getMobilephoneNumberprefix());
                    }
                    
                    refuelAuthenticationResult.setFirstname(refuelAuthenticationResponse.getUserData().getFirstname());
                    refuelAuthenticationResult.setLastname(refuelAuthenticationResponse.getUserData().getLastname());
                    refuelAuthenticationResult.setBirthdate(RefuelingJsonClient.convertLongtoDate(refuelAuthenticationResponse.getUserData().getDateofbirth()));
                    refuelAuthenticationResult.setBirthmunicipality(refuelAuthenticationResponse.getUserData().getBirthmunicipality());
                    refuelAuthenticationResult.setBirthprovince(refuelAuthenticationResponse.getUserData().getBirthprovince());
                    refuelAuthenticationResult.setFiscalCode(refuelAuthenticationResponse.getUserData().getFiscalcode());
                    refuelAuthenticationResult.setSex(RefuelingJsonClient.convertSalutationIdToSex(refuelAuthenticationResponse.getUserData().getSalutationid()));
                }
                
                refuelAuthenticationResult.setStatusCode(refuelAuthenticationResponse.getStatusCode());
            }
            else {
                
                refuelAuthenticationResult = new RefuelAuthenticationResult();
                refuelAuthenticationResult.setStatusCode(refuelAuthenticationResponse.getStatusCode());
            }
        }
        catch (JsonSyntaxException jsonEx) {
            
            refuelAuthenticationResult = new RefuelAuthenticationResult();
            refuelAuthenticationResult.setStatusCode("USR-AUTH-500");
        }

        return refuelAuthenticationResult;
    }
    
    
    public static RefuelNotifySubscriptionResult notifySubscription(String requestID, String fiscalcode, String baseUrl, String basicAuthUsername, String basicAuthPassword, String oauth2AccessToken, String keyStore, String keyStorePassword, String keyPassword) throws IOException {

        RefuelNotifySubscriptionResult refuelNotifySubscriptionResult = null;
        
        TrustManager[] trustAllCerts = new X509TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
        };

        // Install the all-trusting trust manager
        if (keyStore != null && keyStorePassword != null && keyPassword != null) {
            
            System.out.println("Chiamata con mutua autenticazione");
            
            System.out.println("keyStore:         " + keyStore);
            System.out.println("keyStorePassword: " + keyStorePassword);
            System.out.println("keyPassword:      " + keyPassword);
            
            SSLContext sslcontext = null;
            try {
                sslcontext = SSLContext.getInstance(SSL_VERSION);
                KeyManagerFactory kmf = KeyManagerFactory.getInstance(KEY_MANAGER_ALGORITHM);
                KeyStore ks = KeyStore.getInstance(KEY_STORE_TYPE);
                ks.load(new FileInputStream(keyStore), keyStorePassword.toCharArray());
                kmf.init(ks, keyPassword.toCharArray());

                sslcontext.init(kmf.getKeyManagers(), trustAllCerts, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
            }
            catch (Exception ex) {
                System.err.println("Errore nella costruzione del SSLContext: " + ex.getMessage());
            }
        }
        else {
            
            System.out.println("Chiamata senza mutua autenticazione");
            
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        
        String url = baseUrl + RefuelingJsonClient.NOTIFY_SUBSCRIPTION_SERVICE_URL;
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        
        RefuelNotifySubscriptionRequest refuelNotifySubscriptionRequest = new RefuelNotifySubscriptionRequest();
        refuelNotifySubscriptionRequest.setRequestID(requestID);
        refuelNotifySubscriptionRequest.setFiscalCode(fiscalcode);
        
        String jsonRequest = gson.toJson(refuelNotifySubscriptionRequest);

        //add request header
        String userCredentials = basicAuthUsername + ":" + basicAuthPassword;
        String encodedUserCredentials = DatatypeConverter.printBase64Binary(userCredentials.getBytes("UTF-8"));
        String basicAuth = "Basic " + encodedUserCredentials;
        con.setRequestProperty ("Authorization", basicAuth);
        if (oauth2AccessToken != null && !oauth2AccessToken.isEmpty()) {
            String oauth2 = "Bearer " + oauth2AccessToken;
            con.setRequestProperty ("Authorization", oauth2);
        }
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Content-Length", "" + jsonRequest.getBytes().length);
        con.setRequestMethod("POST");
        con.setUseCaches(false);
        con.setDoInput(true);
        con.setDoOutput(true);
        
        // Send post request
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(jsonRequest);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL: " + url);
        System.out.println("Request: " + jsonRequest);
        System.out.println("Response Code: " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        String jsonResponse = response.toString();
        System.out.println("Response: " + jsonResponse);
        
        try {
            RefuelNotifySubscriptionResponse refuelNotifySubscriptionResponse = gson.fromJson(jsonResponse, RefuelNotifySubscriptionResponse.class);
            
            if (refuelNotifySubscriptionResponse !=  null) {
                
                refuelNotifySubscriptionResult = new RefuelNotifySubscriptionResult();
                refuelNotifySubscriptionResult.setStatusCode(refuelNotifySubscriptionResponse.getStatusCode());
                refuelNotifySubscriptionResult.setStatusMessage(refuelNotifySubscriptionResponse.getStatusMessage());
            }
            else {
                
                refuelNotifySubscriptionResult = new RefuelNotifySubscriptionResult();
                refuelNotifySubscriptionResult.setStatusCode("USR-NTFY-500");
                refuelNotifySubscriptionResult.setStatusMessage("System error");
            }
        }
        catch (JsonSyntaxException jsonEx) {
            
            refuelNotifySubscriptionResult = new RefuelNotifySubscriptionResult();
            refuelNotifySubscriptionResult.setStatusCode("USR-NTFY-500");
            refuelNotifySubscriptionResult.setStatusMessage("System error");
        }

        return refuelNotifySubscriptionResult;
    }
    
    private static String convertSalutationIdToSex(String salutationId) {
        
        String sex = "";
        
        if (salutationId == null) {
            return sex;
        }
        if (salutationId.equals("10000")) {
            return "M";
        }
        if (salutationId.equals("10001")) {
            return "F";
        }
        
        return sex;
    }
    
    private static Date convertLongtoDate(String dateLong) {
        
        if (dateLong == null) {
            return null;
        }
        
        Long dateMillis = null;
        
        try {
            dateMillis = Long.parseLong(dateLong);
        }
        catch (NumberFormatException e) {
            System.out.println("Error parsing number: " + dateLong);
            return null;
        }
        
        Date date = new Date(dateMillis);
        
        return date;
    }
    
    private static Date convertStringtoDate(String dateString) {
        
        if (dateString == null) {
            return null;
        }
        
        Date date = null;
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {

            date = formatter.parse(dateString);
            System.out.println(date);
            System.out.println(formatter.format(date));

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        
        return date;
    }
}
