package com.techedge.mp.refueling.integration.business.utilities.interfaces;

import java.io.Serializable;

public class AddressData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1101122466463976425L;

    private String            mail;
    private String            mobilephonenumber;
    private String            mobilephoneNumberprefix;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMobilephonenumber() {
        return mobilephonenumber;
    }

    public void setMobilephonenumber(String mobilephonenumber) {
        this.mobilephonenumber = mobilephonenumber;
    }

    public String getMobilephoneNumberprefix() {
        return mobilephoneNumberprefix;
    }

    public void setMobilephoneNumberprefix(String mobilephoneNumberprefix) {
        this.mobilephoneNumberprefix = mobilephoneNumberprefix;
    }

}
