package com.techedge.mp.refueling.integration.business.utilities.interfaces;

import java.util.Date;

public class RefuelAuthenticationResult {

    private String firstname;
    private String lastname;
    private String email;
    private String sex;
    private Date   birthdate;
    private String birthmunicipality;
    private String birthprovince;
    private String fiscalCode;
    private String mobilePhoneNumber;
    private String mobilePhoneNumberPrefix;
    private String statusCode;

    
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthmunicipality() {
        return birthmunicipality;
    }

    public void setBirthmunicipality(String birthmunicipality) {
        this.birthmunicipality = birthmunicipality;
    }

    public String getBirthprovince() {
        return birthprovince;
    }

    public void setBirthprovince(String birthprovince) {
        this.birthprovince = birthprovince;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public String getMobilePhoneNumberPrefix() {
        return mobilePhoneNumberPrefix;
    }

    public void setMobilePhoneNumberPrefix(String mobilePhoneNumberPrefix) {
        this.mobilePhoneNumberPrefix = mobilePhoneNumberPrefix;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
