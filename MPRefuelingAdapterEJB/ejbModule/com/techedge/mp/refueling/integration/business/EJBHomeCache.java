package com.techedge.mp.refueling.integration.business;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class EJBHomeCache {

    final String loggerServiceRemoteJndi      = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
    final String parametersServiceRemoteJndi  = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
    
    private static EJBHomeCache instance;
    
    protected Context context = null;
    
    protected TransactionServiceRemote transactionService = null;
    protected ParametersServiceRemote parametersService   = null;
    protected LoggerServiceRemote loggerService           = null;
    
    private EJBHomeCache( ) throws InterfaceNotFoundException {
        
        final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();
        
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        
        try {
            context = new InitialContext(jndiProperties);
        } catch (NamingException e) {

            throw new InterfaceNotFoundException("Naming exception: " + e.getMessage());
        }
        try {
            loggerService = (LoggerServiceRemote)context.lookup(loggerServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(loggerServiceRemoteJndi);
        }
        try {
            parametersService = (ParametersServiceRemote)context.lookup(parametersServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(loggerServiceRemoteJndi);
        }
        
    }

    public static synchronized EJBHomeCache getInstance( ) throws InterfaceNotFoundException
    {
        if (instance == null)
            instance = new EJBHomeCache( );
        
        return instance;
    }
    
    public LoggerServiceRemote getLoggerService( )
    {
        return loggerService;
    }

    public ParametersServiceRemote getParametersService() {
    
        return parametersService;
    }
    
    
}
