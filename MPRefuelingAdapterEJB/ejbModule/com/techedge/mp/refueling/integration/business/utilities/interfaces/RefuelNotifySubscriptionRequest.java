package com.techedge.mp.refueling.integration.business.utilities.interfaces;

import java.io.Serializable;

public class RefuelNotifySubscriptionRequest implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6701341397529043249L;

    private String            requestID;
    private String            fiscalCode;

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

}
