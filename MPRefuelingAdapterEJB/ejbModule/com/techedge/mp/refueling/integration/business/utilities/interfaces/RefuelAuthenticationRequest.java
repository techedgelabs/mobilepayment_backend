package com.techedge.mp.refueling.integration.business.utilities.interfaces;

import java.io.Serializable;

public class RefuelAuthenticationRequest implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -896719173018751530L;

    private String            operatorID;
    private String            requestID;
    private String            username;
    private String            password;

    public String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
