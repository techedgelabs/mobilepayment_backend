package com.techedge.mp.refueling.integration.exception;

public class BPELException extends Exception {
	

	
	/**
     * 
     */
    private static final long serialVersionUID = -3178620315218873783L;

    public BPELException() {
		  
		  super(); 
		  
	  }
	  
	  public BPELException(String message) {
		  
		  super(message);
		  
	  }
	  
	  public BPELException(String message, Throwable cause) {
		  
		  super(message, cause); 
		  
	  }
	  
	  public BPELException(Throwable cause) {
		  
		  super(cause); 
		  
	  }
	  
	  
}