package com.techedge.mp.refueling.integration.exception;

public class InvalidTokenException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private int               statusCode;
    private String            errorDescription;

    public InvalidTokenException() {
        super();
    }

    public InvalidTokenException(int statusCode, String errorDescription) {
        super();
        this.statusCode = statusCode;
        this.errorDescription = errorDescription;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

}
