package it.trs.hub.refuelings.api.external.services.ws;

public class WsRefuelingsServiceProxy implements it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsService {
  private String _endpoint = null;
  private it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsService wsRefuelingsService = null;
  
  public WsRefuelingsServiceProxy() {
    _initWsRefuelingsServiceProxy();
  }
  
  public WsRefuelingsServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initWsRefuelingsServiceProxy();
  }
  
  private void _initWsRefuelingsServiceProxy() {
    try {
      wsRefuelingsService = (new it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsServiceImplServiceLocator()).getWsRefuelingsServiceImplPort();
      if (wsRefuelingsService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wsRefuelingsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wsRefuelingsService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wsRefuelingsService != null)
      ((javax.xml.rpc.Stub)wsRefuelingsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.trs.hub.refuelings.api.external.services.ws.WsRefuelingsService getWsRefuelingsService() {
    if (wsRefuelingsService == null)
      _initWsRefuelingsServiceProxy();
    return wsRefuelingsService;
  }
  
  public it.trs.hub.refuelings.api.external.services.ws.MpTransactionNotificationResponse sendMPTransactionNotification(it.trs.hub.refuelings.api.external.services.ws.MpTransactionNotificationRequest arg0) throws java.rmi.RemoteException{
    if (wsRefuelingsService == null)
      _initWsRefuelingsServiceProxy();
    return wsRefuelingsService.sendMPTransactionNotification(arg0);
  }
  
  public it.trs.hub.refuelings.api.external.services.ws.MpTransactionReportResponse sendMPTransactionReport(it.trs.hub.refuelings.api.external.services.ws.MpTransactionReportRequest arg0) throws java.rmi.RemoteException{
    if (wsRefuelingsService == null)
      _initWsRefuelingsServiceProxy();
    return wsRefuelingsService.sendMPTransactionReport(arg0);
  }
  
  
}