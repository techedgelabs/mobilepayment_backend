
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MESSAGE_RECEIVED_200"/>
 *     &lt;enumeration value="TRANSACTION_NOT_RECOGNIZED_400"/>
 *     &lt;enumeration value="PARAMETER_NOT_FOUND_400"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE")
@XmlEnum
public enum EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE {

    MESSAGE_RECEIVED_200,
    TRANSACTION_NOT_RECOGNIZED_400,
    PARAMETER_NOT_FOUND_400;

    public String value() {
        return name();
    }

    public static EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE fromValue(String v) {
        return valueOf(v);
    }

}
