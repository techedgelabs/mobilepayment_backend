
package com.techedge.mp.simulazioni;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transactionDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="lastTransactionStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://simulazioni.mp.techedge.com/}transactionStatusHistory" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://simulazioni.mp.techedge.com/}refuelDetail"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionDetails", propOrder = {
    "transactionID",
    "timestamp",
    "lastTransactionStatus",
    "transactionStatusHistory",
    "refuelDetail"
})
public class TransactionDetails {

	@XmlElement(required = true, nillable = true)
    protected String transactionID;
    protected long timestamp;
    @XmlElement(required = true, nillable = true)
    protected String lastTransactionStatus;
    @XmlElement(namespace = "http://simulazioni.mp.techedge.com/")
    protected List<TransactionStatusHistory> transactionStatusHistory;
    @XmlElement(namespace = "http://simulazioni.mp.techedge.com/", required = true)
    protected RefuelDetail refuelDetail;

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     */
    public void setTimestamp(long value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the lastTransactionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastTransactionStatus() {
        return lastTransactionStatus;
    }

    /**
     * Sets the value of the lastTransactionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastTransactionStatus(String value) {
        this.lastTransactionStatus = value;
    }

    /**
     * Gets the value of the transactionStatusHistory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionStatusHistory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionStatusHistory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionStatusHistory }
     * 
     * 
     */
    public List<TransactionStatusHistory> getTransactionStatusHistory() {
        if (transactionStatusHistory == null) {
            transactionStatusHistory = new ArrayList<TransactionStatusHistory>();
        }
        return this.transactionStatusHistory;
    }

    /**
     * Gets the value of the refuelDetail property.
     * 
     * @return
     *     possible object is
     *     {@link RefuelDetail }
     *     
     */
    public RefuelDetail getRefuelDetail() {
        return refuelDetail;
    }

    /**
     * Sets the value of the refuelDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefuelDetail }
     *     
     */
    public void setRefuelDetail(RefuelDetail value) {
        this.refuelDetail = value;
    }

}
