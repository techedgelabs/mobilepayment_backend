
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumPUMPSTAUSRESPONSE.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enumPUMPSTAUSRESPONSE">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PUMP_AVAILABLE_200"/>
 *     &lt;enumeration value="PUMP_BUSY_401"/>
 *     &lt;enumeration value="PUMP_NOT_AVAILABLE_501"/>
 *     &lt;enumeration value="PUMP_STATUS_NOT_AVAILABLE_502"/>
 *     &lt;enumeration value="PUMP_NOT_FOUND_404"/>
 *     &lt;enumeration value="STATION_NOT_FOUND_404"/>
 *     &lt;enumeration value="STATION_PUMP_NOT_FOUND_404"/>
 *     &lt;enumeration value="PARAMETER_NOT_FOUND_400"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enumPUMPSTAUSRESPONSE")
@XmlEnum
public enum EnumPUMPSTAUSRESPONSE {

    PUMP_AVAILABLE_200,
    PUMP_BUSY_401,
    PUMP_NOT_AVAILABLE_501,
    PUMP_STATUS_NOT_AVAILABLE_502,
    PUMP_NOT_FOUND_404,
    STATION_NOT_FOUND_404,
    STATION_PUMP_NOT_FOUND_404,
    PARAMETER_NOT_FOUND_400;

    public String value() {
        return name();
    }

    public static EnumPUMPSTAUSRESPONSE fromValue(String v) {
        return valueOf(v);
    }

}
