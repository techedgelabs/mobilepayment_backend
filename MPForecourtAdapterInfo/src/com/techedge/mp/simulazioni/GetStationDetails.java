
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStationDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStationDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stationStatusRequest" type="{http://simulazioni.mp.techedge.com/}stationStatusMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStationDetails", propOrder = {
    "stationStatusRequest"
})
public class GetStationDetails {

    @XmlElement(required = true)
    protected StationStatusMessageRequest stationStatusRequest;

    /**
     * Gets the value of the stationStatusRequest property.
     * 
     * @return
     *     possible object is
     *     {@link StationStatusMessageRequest }
     *     
     */
    public StationStatusMessageRequest getStationStatusRequest() {
        return stationStatusRequest;
    }

    /**
     * Sets the value of the stationStatusRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link StationStatusMessageRequest }
     *     
     */
    public void setStationStatusRequest(StationStatusMessageRequest value) {
        this.stationStatusRequest = value;
    }

}
