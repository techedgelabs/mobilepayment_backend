
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sendPaymentTransactionResultResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendPaymentTransactionResultResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sendPaymentTransactionResultResponse" type="{http://simulazioni.mp.techedge.com/}sendPaymentTransactionResultMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendPaymentTransactionResultResponse", propOrder = {
    "sendPaymentTransactionResultResponse"
})
public class SendPaymentTransactionResultResponse {

    @XmlElement(required = true)
    protected SendPaymentTransactionResultMessageResponse sendPaymentTransactionResultResponse;

    /**
     * Gets the value of the sendPaymentTransactionResultResponse property.
     * 
     * @return
     *     possible object is
     *     {@link SendPaymentTransactionResultMessageResponse }
     *     
     */
    public SendPaymentTransactionResultMessageResponse getSendPaymentTransactionResultResponse() {
        return sendPaymentTransactionResultResponse;
    }

    /**
     * Sets the value of the sendPaymentTransactionResultResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendPaymentTransactionResultMessageResponse }
     *     
     */
    public void setSendPaymentTransactionResultResponse(SendPaymentTransactionResultMessageResponse value) {
        this.sendPaymentTransactionResultResponse = value;
    }

}
