
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enablePumpResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enablePumpResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enablePumpResponse" type="{http://simulazioni.mp.techedge.com/}enablePumpMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enablePumpResponse", propOrder = {
    "enablePumpResponse"
})
public class EnablePumpResponse {

    @XmlElement(required = true)
    protected EnablePumpMessageResponse enablePumpResponse;

    /**
     * Gets the value of the enablePumpResponse property.
     * 
     * @return
     *     possible object is
     *     {@link EnablePumpMessageResponse }
     *     
     */
    public EnablePumpMessageResponse getEnablePumpResponse() {
        return enablePumpResponse;
    }

    /**
     * Sets the value of the enablePumpResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnablePumpMessageResponse }
     *     
     */
    public void setEnablePumpResponse(EnablePumpMessageResponse value) {
        this.enablePumpResponse = value;
    }

}
