
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enablePump complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enablePump">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enablePumpRequest" type="{http://simulazioni.mp.techedge.com/}enablePumpMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enablePump", propOrder = {
    "enablePumpRequest"
})
public class EnablePump {

    @XmlElement(required = true)
    protected EnablePumpMessageRequest enablePumpRequest;

    /**
     * Gets the value of the enablePumpRequest property.
     * 
     * @return
     *     possible object is
     *     {@link EnablePumpMessageRequest }
     *     
     */
    public EnablePumpMessageRequest getEnablePumpRequest() {
        return enablePumpRequest;
    }

    /**
     * Sets the value of the enablePumpRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnablePumpMessageRequest }
     *     
     */
    public void setEnablePumpRequest(EnablePumpMessageRequest value) {
        this.enablePumpRequest = value;
    }

}
