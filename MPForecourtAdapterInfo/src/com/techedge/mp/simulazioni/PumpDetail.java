
package com.techedge.mp.simulazioni;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pumpDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pumpDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pumpID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pumpNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pumpStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refuelMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productDetails" type="{http://simulazioni.mp.techedge.com/}productDetail" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pumpDetail", propOrder = {
    "pumpID",
    "pumpNumber",
    "pumpStatus",
    "refuelMode",
    "productDetails"
})
public class PumpDetail {

    @XmlElement(required = true, nillable = true)
    protected String pumpID;
    @XmlElement(required = true, nillable = true)
    protected String pumpNumber;
    @XmlElement(required = true, nillable = true)
    protected String pumpStatus;
    @XmlElement(required = true, nillable = true)
    protected String refuelMode;
    protected List<ProductDetail> productDetails;

    /**
     * Gets the value of the pumpID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpID() {
        return pumpID;
    }

    /**
     * Sets the value of the pumpID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpID(String value) {
        this.pumpID = value;
    }

    /**
     * Gets the value of the pumpNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpNumber() {
        return pumpNumber;
    }

    /**
     * Sets the value of the pumpNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpNumber(String value) {
        this.pumpNumber = value;
    }

    /**
     * Gets the value of the pumpStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpStatus() {
        return pumpStatus;
    }

    /**
     * Sets the value of the pumpStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpStatus(String value) {
        this.pumpStatus = value;
    }

    /**
     * Gets the value of the refuelMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefuelMode() {
        return refuelMode;
    }

    /**
     * Sets the value of the refuelMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefuelMode(String value) {
        this.refuelMode = value;
    }

    /**
     * Gets the value of the productDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductDetail }
     * 
     * 
     */
    public List<ProductDetail> getProductDetails() {
        if (productDetails == null) {
            productDetails = new ArrayList<ProductDetail>();
        }
        return this.productDetails;
    }

}
