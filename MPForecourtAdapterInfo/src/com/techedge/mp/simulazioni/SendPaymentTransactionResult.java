
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sendPaymentTransactionResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendPaymentTransactionResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sendPaymentTransactionResultRequest" type="{http://simulazioni.mp.techedge.com/}sendPaymentTransactionResultMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendPaymentTransactionResult", propOrder = {
    "sendPaymentTransactionResultRequest"
})
public class SendPaymentTransactionResult {

    @XmlElement(required = true)
    protected SendPaymentTransactionResultMessageRequest sendPaymentTransactionResultRequest;

    /**
     * Gets the value of the sendPaymentTransactionResultRequest property.
     * 
     * @return
     *     possible object is
     *     {@link SendPaymentTransactionResultMessageRequest }
     *     
     */
    public SendPaymentTransactionResultMessageRequest getSendPaymentTransactionResultRequest() {
        return sendPaymentTransactionResultRequest;
    }

    /**
     * Sets the value of the sendPaymentTransactionResultRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendPaymentTransactionResultMessageRequest }
     *     
     */
    public void setSendPaymentTransactionResultRequest(SendPaymentTransactionResultMessageRequest value) {
        this.sendPaymentTransactionResultRequest = value;
    }

}
