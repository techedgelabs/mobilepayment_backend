
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStationDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStationDetailsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stationStatusResponse" type="{http://simulazioni.mp.techedge.com/}stationStatusMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStationDetailsResponse", propOrder = {
    "stationStatusResponse"
})
public class GetStationDetailsResponse {

    @XmlElement(required = true)
    protected StationStatusMessageResponse stationStatusResponse;

    /**
     * Gets the value of the stationStatusResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StationStatusMessageResponse }
     *     
     */
    public StationStatusMessageResponse getStationStatusResponse() {
        return stationStatusResponse;
    }

    /**
     * Sets the value of the stationStatusResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StationStatusMessageResponse }
     *     
     */
    public void setStationStatusResponse(StationStatusMessageResponse value) {
        this.stationStatusResponse = value;
    }

}
