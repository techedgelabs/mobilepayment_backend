
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transactionReconciliationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionReconciliationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionReconciliationResponse" type="{http://simulazioni.mp.techedge.com/}transactionReconciliationMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionReconciliationResponse", propOrder = {
    "transactionReconciliationResponse"
})
public class TransactionReconciliationResponse {

    @XmlElement(required = true)
    protected TransactionReconciliationMessageResponse transactionReconciliationResponse;

    /**
     * Gets the value of the transactionReconciliationResponse property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionReconciliationMessageResponse }
     *     
     */
    public TransactionReconciliationMessageResponse getTransactionReconciliationResponse() {
        return transactionReconciliationResponse;
    }

    /**
     * Sets the value of the transactionReconciliationResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionReconciliationMessageResponse }
     *     
     */
    public void setTransactionReconciliationResponse(TransactionReconciliationMessageResponse value) {
        this.transactionReconciliationResponse = value;
    }

}
