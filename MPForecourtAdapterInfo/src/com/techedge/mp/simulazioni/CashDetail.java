
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlElement;


public class CashDetail {

    @XmlElement(required = true, nillable = true)
    protected String cashID;
    @XmlElement(required = true, nillable = true)
    protected String cashNumber;
	
    
    
    public String getCashID() {
		return cashID;
	}
	public void setCashID(String cashID) {
		this.cashID = cashID;
	}
	public String getCashNumber() {
		return cashNumber;
	}
	public void setCashNumber(String cashNumber) {
		this.cashNumber = cashNumber;
	}
    
    
    
    
    
    
    
}
