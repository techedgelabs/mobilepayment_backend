
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enablePumpMessageRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enablePumpMessageRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pumpID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element ref="{http://simulazioni.mp.techedge.com/}paymentAuthorizationResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enablePumpMessageRequest", propOrder = {
    "requestID",
    "transactionID",
    "stationID",
    "pumpID",
    "paymentMode",
    "productID",
    "amount",
    "paymentAuthorizationResult"
})
public class EnablePumpMessageRequest {

    @XmlElement(required = true)
    protected String requestID;
    @XmlElement(required = true)
    protected String transactionID;
    @XmlElement(required = true)
    protected String stationID;
    @XmlElement(required = true)
    protected String pumpID;
    @XmlElement(required = true)
    protected String paymentMode;
    protected String productID;
    @XmlElement(required = true)
    protected Double amount;
    @XmlElement(namespace = "http://simulazioni.mp.techedge.com/", required = true)
    protected PaymentAuthorizationResult paymentAuthorizationResult;

    /**
     * Gets the value of the requestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the stationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStationID() {
        return stationID;
    }

    /**
     * Sets the value of the stationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStationID(String value) {
        this.stationID = value;
    }

    /**
     * Gets the value of the pumpID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpID() {
        return pumpID;
    }

    /**
     * Sets the value of the pumpID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpID(String value) {
        this.pumpID = value;
    }

    /**
     * Gets the value of the paymentMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * Sets the value of the paymentMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMode(String value) {
        this.paymentMode = value;
    }

    /**
     * Gets the value of the productID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductID() {
        return productID;
    }

    /**
     * Sets the value of the productID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductID(String value) {
        this.productID = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAmount(Double value) {
        this.amount = value;
    }

    /**
     * Gets the value of the paymentAuthorizationResult property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentAuthorizationResult }
     *     
     */
    public PaymentAuthorizationResult getPaymentAuthorizationResult() {
        return paymentAuthorizationResult;
    }

    /**
     * Sets the value of the paymentAuthorizationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentAuthorizationResult }
     *     
     */
    public void setPaymentAuthorizationResult(PaymentAuthorizationResult value) {
        this.paymentAuthorizationResult = value;
    }

}
