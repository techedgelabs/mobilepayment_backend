
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPumpStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPumpStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pumpStatusResponse" type="{http://simulazioni.mp.techedge.com/}pumpStatusMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPumpStatusResponse", propOrder = {
    "pumpStatusResponse"
})
public class GetPumpStatusResponse {

    @XmlElement(required = true)
    protected PumpStatusMessageResponse pumpStatusResponse;

    /**
     * Gets the value of the pumpStatusResponse property.
     * 
     * @return
     *     possible object is
     *     {@link PumpStatusMessageResponse }
     *     
     */
    public PumpStatusMessageResponse getPumpStatusResponse() {
        return pumpStatusResponse;
    }

    /**
     * Sets the value of the pumpStatusResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link PumpStatusMessageResponse }
     *     
     */
    public void setPumpStatusResponse(PumpStatusMessageResponse value) {
        this.pumpStatusResponse = value;
    }

}
