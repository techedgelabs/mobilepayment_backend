
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for stationStatusMessageResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="stationStatusMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="statusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="messageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://simulazioni.mp.techedge.com/}stationDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "stationStatusMessageResponse", propOrder = {
    "statusCode",
    "messageCode",
    "stationDetail"
})
public class StationStatusMessageResponse {

    @XmlElement(required = true, nillable = true)
    protected String statusCode;
    @XmlElement(required = true, nillable = true)
    protected String messageCode;
    @XmlElement(namespace = "http://simulazioni.mp.techedge.com/")
    protected StationDetail stationDetail;

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the messageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Sets the value of the messageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    /**
     * Gets the value of the stationDetail property.
     * 
     * @return
     *     possible object is
     *     {@link StationDetail }
     *     
     */
    public StationDetail getStationDetail() {
        return stationDetail;
    }

    /**
     * Sets the value of the stationDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link StationDetail }
     *     
     */
    public void setStationDetail(StationDetail value) {
        this.stationDetail = value;
    }

}
