
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transactionReconciliation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionReconciliation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionReconciliationRequest" type="{http://simulazioni.mp.techedge.com/}transactionReconciliationMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionReconciliation", propOrder = {
    "transactionReconciliationRequest"
})
public class TransactionReconciliation {

    @XmlElement(required = true)
    protected TransactionReconciliationMessageRequest transactionReconciliationRequest;

    /**
     * Gets the value of the transactionReconciliationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionReconciliationMessageRequest }
     *     
     */
    public TransactionReconciliationMessageRequest getTransactionReconciliationRequest() {
        return transactionReconciliationRequest;
    }

    /**
     * Sets the value of the transactionReconciliationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionReconciliationMessageRequest }
     *     
     */
    public void setTransactionReconciliationRequest(TransactionReconciliationMessageRequest value) {
        this.transactionReconciliationRequest = value;
    }

}
