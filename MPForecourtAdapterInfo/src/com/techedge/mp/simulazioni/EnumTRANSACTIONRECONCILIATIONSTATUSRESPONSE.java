
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumTRANSACTIONRECONCILIATIONSTATUSRESPONSE.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enumTRANSACTIONRECONCILIATIONSTATUSRESPONSE">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REFUEL_ENABLED_200"/>
 *     &lt;enumeration value="REFUEL_STARTED_201"/>
 *     &lt;enumeration value="REFUEL_ENDED_202"/>
 *     &lt;enumeration value="TIMEOUT_AFTER_PUMP_ENABLE_400"/>
 *     &lt;enumeration value="TIMEOUT_AFTER_START_REFUEL_400"/>
 *     &lt;enumeration value="PARAMETER_NOT_FOUND_400"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enumTRANSACTIONRECONCILIATIONSTATUSRESPONSE")
@XmlEnum
public enum EnumTRANSACTIONRECONCILIATIONSTATUSRESPONSE {

    REFUEL_ENABLED_200,
    REFUEL_STARTED_201,
    REFUEL_ENDED_202,
    TIMEOUT_AFTER_PUMP_ENABLE_400,
    TIMEOUT_AFTER_START_REFUEL_400,
    PARAMETER_NOT_FOUND_400;

    public String value() {
        return name();
    }

    public static EnumTRANSACTIONRECONCILIATIONSTATUSRESPONSE fromValue(String v) {
        return valueOf(v);
    }

}
