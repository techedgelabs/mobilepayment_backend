
package com.techedge.mp.simulazioni;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.techedge.mp.simulazioni package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetStationDetails_QNAME = new QName("http://simulazioni.mp.techedge.com/", "getStationDetails");
    private final static QName _PumpDetail_QNAME = new QName("http://simulazioni.mp.techedge.com/", "pumpDetail");
    private final static QName _ProductDetail_QNAME = new QName("http://simulazioni.mp.techedge.com/", "productDetail");
    private final static QName _EnablePumpResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "enablePumpResponse");
    private final static QName _GetPumpStatusResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "getPumpStatusResponse");
    private final static QName _TransactionDetails_QNAME = new QName("http://simulazioni.mp.techedge.com/", "transactionDetails");
    private final static QName _StationStatusMessageRequest_QNAME = new QName("http://simulazioni.mp.techedge.com/", "stationStatusMessageRequest");
    private final static QName _EnablePumpMessageRequest_QNAME = new QName("http://simulazioni.mp.techedge.com/", "enablePumpMessageRequest");
    private final static QName _SendPaymentTransactionResultResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "sendPaymentTransactionResultResponse");
    private final static QName _TransactionID_QNAME = new QName("http://simulazioni.mp.techedge.com/", "transactionID");
    private final static QName _EnablePump_QNAME = new QName("http://simulazioni.mp.techedge.com/", "enablePump");
    private final static QName _EnablePumpMessageResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "enablePumpMessageResponse");
    private final static QName _PumpStatusMessageRequest_QNAME = new QName("http://simulazioni.mp.techedge.com/", "pumpStatusMessageRequest");
    private final static QName _GetStationDetailsResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "getStationDetailsResponse");
    private final static QName _TransactionStatusMessageRequest_QNAME = new QName("http://simulazioni.mp.techedge.com/", "transactionStatusMessageRequest");
    private final static QName _TransactionStatusHistory_QNAME = new QName("http://simulazioni.mp.techedge.com/", "transactionStatusHistory");
    private final static QName _GetTransactionStatusResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "getTransactionStatusResponse");
    private final static QName _TransactionReconciliationResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "transactionReconciliationResponse");
    private final static QName _PaymentAuthorizationResult_QNAME = new QName("http://simulazioni.mp.techedge.com/", "paymentAuthorizationResult");
    private final static QName _GetPumpStatus_QNAME = new QName("http://simulazioni.mp.techedge.com/", "getPumpStatus");
    private final static QName _GetTransactionStatus_QNAME = new QName("http://simulazioni.mp.techedge.com/", "getTransactionStatus");
    private final static QName _PumpStatusMessageResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "pumpStatusMessageResponse");
    private final static QName _TransactionReconciliationMessageResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "transactionReconciliationMessageResponse");
    private final static QName _TransactionReconciliation_QNAME = new QName("http://simulazioni.mp.techedge.com/", "transactionReconciliation");
    private final static QName _SendPaymentTransactionResult_QNAME = new QName("http://simulazioni.mp.techedge.com/", "sendPaymentTransactionResult");
    private final static QName _SendPaymentTransactionResultMessageResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "sendPaymentTransactionResultMessageResponse");
    private final static QName _SendPaymentTransactionResultMessageRequest_QNAME = new QName("http://simulazioni.mp.techedge.com/", "sendPaymentTransactionResultMessageRequest");
    private final static QName _StationStatusMessageResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "stationStatusMessageResponse");
    private final static QName _BankTransactionResult_QNAME = new QName("http://simulazioni.mp.techedge.com/", "bankTransactionResult");
    private final static QName _TransactionReconciliationMessageRequest_QNAME = new QName("http://simulazioni.mp.techedge.com/", "transactionReconciliationMessageRequest");
    private final static QName _RefuelDetail_QNAME = new QName("http://simulazioni.mp.techedge.com/", "refuelDetail");
    private final static QName _TransactionStatusMessageResponse_QNAME = new QName("http://simulazioni.mp.techedge.com/", "transactionStatusMessageResponse");
    private final static QName _StationDetail_QNAME = new QName("http://simulazioni.mp.techedge.com/", "stationDetail");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.techedge.mp.simulazioni
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RefuelDetail }
     * 
     */
    public RefuelDetail createRefuelDetail() {
        return new RefuelDetail();
    }

    /**
     * Create an instance of {@link StationDetail }
     * 
     */
    public StationDetail createStationDetail() {
        return new StationDetail();
    }

    /**
     * Create an instance of {@link TransactionStatusMessageResponse }
     * 
     */
    public TransactionStatusMessageResponse createTransactionStatusMessageResponse() {
        return new TransactionStatusMessageResponse();
    }

    /**
     * Create an instance of {@link SendPaymentTransactionResult }
     * 
     */
    public SendPaymentTransactionResult createSendPaymentTransactionResult() {
        return new SendPaymentTransactionResult();
    }

    /**
     * Create an instance of {@link TransactionReconciliation }
     * 
     */
    public TransactionReconciliation createTransactionReconciliation() {
        return new TransactionReconciliation();
    }

    /**
     * Create an instance of {@link SendPaymentTransactionResultMessageRequest }
     * 
     */
    public SendPaymentTransactionResultMessageRequest createSendPaymentTransactionResultMessageRequest() {
        return new SendPaymentTransactionResultMessageRequest();
    }

    /**
     * Create an instance of {@link SendPaymentTransactionResultMessageResponse }
     * 
     */
    public SendPaymentTransactionResultMessageResponse createSendPaymentTransactionResultMessageResponse() {
        return new SendPaymentTransactionResultMessageResponse();
    }

    /**
     * Create an instance of {@link TransactionReconciliationMessageRequest }
     * 
     */
    public TransactionReconciliationMessageRequest createTransactionReconciliationMessageRequest() {
        return new TransactionReconciliationMessageRequest();
    }

    /**
     * Create an instance of {@link BankTransactionResult }
     * 
     */
    public BankTransactionResult createBankTransactionResult() {
        return new BankTransactionResult();
    }

    /**
     * Create an instance of {@link StationStatusMessageResponse }
     * 
     */
    public StationStatusMessageResponse createStationStatusMessageResponse() {
        return new StationStatusMessageResponse();
    }

    /**
     * Create an instance of {@link TransactionReconciliationMessageResponse }
     * 
     */
    public TransactionReconciliationMessageResponse createTransactionReconciliationMessageResponse() {
        return new TransactionReconciliationMessageResponse();
    }

    /**
     * Create an instance of {@link GetTransactionStatus }
     * 
     */
    public GetTransactionStatus createGetTransactionStatus() {
        return new GetTransactionStatus();
    }

    /**
     * Create an instance of {@link PumpStatusMessageResponse }
     * 
     */
    public PumpStatusMessageResponse createPumpStatusMessageResponse() {
        return new PumpStatusMessageResponse();
    }

    /**
     * Create an instance of {@link TransactionStatusMessageRequest }
     * 
     */
    public TransactionStatusMessageRequest createTransactionStatusMessageRequest() {
        return new TransactionStatusMessageRequest();
    }

    /**
     * Create an instance of {@link TransactionStatusHistory }
     * 
     */
    public TransactionStatusHistory createTransactionStatusHistory() {
        return new TransactionStatusHistory();
    }

    /**
     * Create an instance of {@link TransactionReconciliationResponse }
     * 
     */
    public TransactionReconciliationResponse createTransactionReconciliationResponse() {
        return new TransactionReconciliationResponse();
    }

    /**
     * Create an instance of {@link PaymentAuthorizationResult }
     * 
     */
    public PaymentAuthorizationResult createPaymentAuthorizationResult() {
        return new PaymentAuthorizationResult();
    }

    /**
     * Create an instance of {@link GetPumpStatus }
     * 
     */
    public GetPumpStatus createGetPumpStatus() {
        return new GetPumpStatus();
    }

    /**
     * Create an instance of {@link GetTransactionStatusResponse }
     * 
     */
    public GetTransactionStatusResponse createGetTransactionStatusResponse() {
        return new GetTransactionStatusResponse();
    }

    /**
     * Create an instance of {@link EnablePumpMessageResponse }
     * 
     */
    public EnablePumpMessageResponse createEnablePumpMessageResponse() {
        return new EnablePumpMessageResponse();
    }

    /**
     * Create an instance of {@link GetStationDetailsResponse }
     * 
     */
    public GetStationDetailsResponse createGetStationDetailsResponse() {
        return new GetStationDetailsResponse();
    }

    /**
     * Create an instance of {@link PumpStatusMessageRequest }
     * 
     */
    public PumpStatusMessageRequest createPumpStatusMessageRequest() {
        return new PumpStatusMessageRequest();
    }

    /**
     * Create an instance of {@link StationStatusMessageRequest }
     * 
     */
    public StationStatusMessageRequest createStationStatusMessageRequest() {
        return new StationStatusMessageRequest();
    }

    /**
     * Create an instance of {@link TransactionDetails }
     * 
     */
    public TransactionDetails createTransactionDetails() {
        return new TransactionDetails();
    }

    /**
     * Create an instance of {@link TransactionID }
     * 
     */
    public TransactionID createTransactionID() {
        return new TransactionID();
    }

    /**
     * Create an instance of {@link SendPaymentTransactionResultResponse }
     * 
     */
    public SendPaymentTransactionResultResponse createSendPaymentTransactionResultResponse() {
        return new SendPaymentTransactionResultResponse();
    }

    /**
     * Create an instance of {@link EnablePump }
     * 
     */
    public EnablePump createEnablePump() {
        return new EnablePump();
    }

    /**
     * Create an instance of {@link EnablePumpMessageRequest }
     * 
     */
    public EnablePumpMessageRequest createEnablePumpMessageRequest() {
        return new EnablePumpMessageRequest();
    }

    /**
     * Create an instance of {@link ProductDetail }
     * 
     */
    public ProductDetail createProductDetail() {
        return new ProductDetail();
    }

    /**
     * Create an instance of {@link GetPumpStatusResponse }
     * 
     */
    public GetPumpStatusResponse createGetPumpStatusResponse() {
        return new GetPumpStatusResponse();
    }

    /**
     * Create an instance of {@link EnablePumpResponse }
     * 
     */
    public EnablePumpResponse createEnablePumpResponse() {
        return new EnablePumpResponse();
    }

    /**
     * Create an instance of {@link PumpDetail }
     * 
     */
    public PumpDetail createPumpDetail() {
        return new PumpDetail();
    }

    /**
     * Create an instance of {@link GetStationDetails }
     * 
     */
    public GetStationDetails createGetStationDetails() {
        return new GetStationDetails();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStationDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "getStationDetails")
    public JAXBElement<GetStationDetails> createGetStationDetails(GetStationDetails value) {
        return new JAXBElement<GetStationDetails>(_GetStationDetails_QNAME, GetStationDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PumpDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "pumpDetail")
    public JAXBElement<PumpDetail> createPumpDetail(PumpDetail value) {
        return new JAXBElement<PumpDetail>(_PumpDetail_QNAME, PumpDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "productDetail")
    public JAXBElement<ProductDetail> createProductDetail(ProductDetail value) {
        return new JAXBElement<ProductDetail>(_ProductDetail_QNAME, ProductDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnablePumpResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "enablePumpResponse")
    public JAXBElement<EnablePumpResponse> createEnablePumpResponse(EnablePumpResponse value) {
        return new JAXBElement<EnablePumpResponse>(_EnablePumpResponse_QNAME, EnablePumpResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPumpStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "getPumpStatusResponse")
    public JAXBElement<GetPumpStatusResponse> createGetPumpStatusResponse(GetPumpStatusResponse value) {
        return new JAXBElement<GetPumpStatusResponse>(_GetPumpStatusResponse_QNAME, GetPumpStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "transactionDetails")
    public JAXBElement<TransactionDetails> createTransactionDetails(TransactionDetails value) {
        return new JAXBElement<TransactionDetails>(_TransactionDetails_QNAME, TransactionDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StationStatusMessageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "stationStatusMessageRequest")
    public JAXBElement<StationStatusMessageRequest> createStationStatusMessageRequest(StationStatusMessageRequest value) {
        return new JAXBElement<StationStatusMessageRequest>(_StationStatusMessageRequest_QNAME, StationStatusMessageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnablePumpMessageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "enablePumpMessageRequest")
    public JAXBElement<EnablePumpMessageRequest> createEnablePumpMessageRequest(EnablePumpMessageRequest value) {
        return new JAXBElement<EnablePumpMessageRequest>(_EnablePumpMessageRequest_QNAME, EnablePumpMessageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendPaymentTransactionResultResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "sendPaymentTransactionResultResponse")
    public JAXBElement<SendPaymentTransactionResultResponse> createSendPaymentTransactionResultResponse(SendPaymentTransactionResultResponse value) {
        return new JAXBElement<SendPaymentTransactionResultResponse>(_SendPaymentTransactionResultResponse_QNAME, SendPaymentTransactionResultResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "transactionID")
    public JAXBElement<TransactionID> createTransactionID(TransactionID value) {
        return new JAXBElement<TransactionID>(_TransactionID_QNAME, TransactionID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnablePump }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "enablePump")
    public JAXBElement<EnablePump> createEnablePump(EnablePump value) {
        return new JAXBElement<EnablePump>(_EnablePump_QNAME, EnablePump.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnablePumpMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "enablePumpMessageResponse")
    public JAXBElement<EnablePumpMessageResponse> createEnablePumpMessageResponse(EnablePumpMessageResponse value) {
        return new JAXBElement<EnablePumpMessageResponse>(_EnablePumpMessageResponse_QNAME, EnablePumpMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PumpStatusMessageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "pumpStatusMessageRequest")
    public JAXBElement<PumpStatusMessageRequest> createPumpStatusMessageRequest(PumpStatusMessageRequest value) {
        return new JAXBElement<PumpStatusMessageRequest>(_PumpStatusMessageRequest_QNAME, PumpStatusMessageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStationDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "getStationDetailsResponse")
    public JAXBElement<GetStationDetailsResponse> createGetStationDetailsResponse(GetStationDetailsResponse value) {
        return new JAXBElement<GetStationDetailsResponse>(_GetStationDetailsResponse_QNAME, GetStationDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionStatusMessageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "transactionStatusMessageRequest")
    public JAXBElement<TransactionStatusMessageRequest> createTransactionStatusMessageRequest(TransactionStatusMessageRequest value) {
        return new JAXBElement<TransactionStatusMessageRequest>(_TransactionStatusMessageRequest_QNAME, TransactionStatusMessageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionStatusHistory }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "transactionStatusHistory")
    public JAXBElement<TransactionStatusHistory> createTransactionStatusHistory(TransactionStatusHistory value) {
        return new JAXBElement<TransactionStatusHistory>(_TransactionStatusHistory_QNAME, TransactionStatusHistory.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransactionStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "getTransactionStatusResponse")
    public JAXBElement<GetTransactionStatusResponse> createGetTransactionStatusResponse(GetTransactionStatusResponse value) {
        return new JAXBElement<GetTransactionStatusResponse>(_GetTransactionStatusResponse_QNAME, GetTransactionStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionReconciliationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "transactionReconciliationResponse")
    public JAXBElement<TransactionReconciliationResponse> createTransactionReconciliationResponse(TransactionReconciliationResponse value) {
        return new JAXBElement<TransactionReconciliationResponse>(_TransactionReconciliationResponse_QNAME, TransactionReconciliationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentAuthorizationResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "paymentAuthorizationResult")
    public JAXBElement<PaymentAuthorizationResult> createPaymentAuthorizationResult(PaymentAuthorizationResult value) {
        return new JAXBElement<PaymentAuthorizationResult>(_PaymentAuthorizationResult_QNAME, PaymentAuthorizationResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPumpStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "getPumpStatus")
    public JAXBElement<GetPumpStatus> createGetPumpStatus(GetPumpStatus value) {
        return new JAXBElement<GetPumpStatus>(_GetPumpStatus_QNAME, GetPumpStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransactionStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "getTransactionStatus")
    public JAXBElement<GetTransactionStatus> createGetTransactionStatus(GetTransactionStatus value) {
        return new JAXBElement<GetTransactionStatus>(_GetTransactionStatus_QNAME, GetTransactionStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PumpStatusMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "pumpStatusMessageResponse")
    public JAXBElement<PumpStatusMessageResponse> createPumpStatusMessageResponse(PumpStatusMessageResponse value) {
        return new JAXBElement<PumpStatusMessageResponse>(_PumpStatusMessageResponse_QNAME, PumpStatusMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionReconciliationMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "transactionReconciliationMessageResponse")
    public JAXBElement<TransactionReconciliationMessageResponse> createTransactionReconciliationMessageResponse(TransactionReconciliationMessageResponse value) {
        return new JAXBElement<TransactionReconciliationMessageResponse>(_TransactionReconciliationMessageResponse_QNAME, TransactionReconciliationMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionReconciliation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "transactionReconciliation")
    public JAXBElement<TransactionReconciliation> createTransactionReconciliation(TransactionReconciliation value) {
        return new JAXBElement<TransactionReconciliation>(_TransactionReconciliation_QNAME, TransactionReconciliation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendPaymentTransactionResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "sendPaymentTransactionResult")
    public JAXBElement<SendPaymentTransactionResult> createSendPaymentTransactionResult(SendPaymentTransactionResult value) {
        return new JAXBElement<SendPaymentTransactionResult>(_SendPaymentTransactionResult_QNAME, SendPaymentTransactionResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendPaymentTransactionResultMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "sendPaymentTransactionResultMessageResponse")
    public JAXBElement<SendPaymentTransactionResultMessageResponse> createSendPaymentTransactionResultMessageResponse(SendPaymentTransactionResultMessageResponse value) {
        return new JAXBElement<SendPaymentTransactionResultMessageResponse>(_SendPaymentTransactionResultMessageResponse_QNAME, SendPaymentTransactionResultMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendPaymentTransactionResultMessageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "sendPaymentTransactionResultMessageRequest")
    public JAXBElement<SendPaymentTransactionResultMessageRequest> createSendPaymentTransactionResultMessageRequest(SendPaymentTransactionResultMessageRequest value) {
        return new JAXBElement<SendPaymentTransactionResultMessageRequest>(_SendPaymentTransactionResultMessageRequest_QNAME, SendPaymentTransactionResultMessageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StationStatusMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "stationStatusMessageResponse")
    public JAXBElement<StationStatusMessageResponse> createStationStatusMessageResponse(StationStatusMessageResponse value) {
        return new JAXBElement<StationStatusMessageResponse>(_StationStatusMessageResponse_QNAME, StationStatusMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankTransactionResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "bankTransactionResult")
    public JAXBElement<BankTransactionResult> createBankTransactionResult(BankTransactionResult value) {
        return new JAXBElement<BankTransactionResult>(_BankTransactionResult_QNAME, BankTransactionResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionReconciliationMessageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "transactionReconciliationMessageRequest")
    public JAXBElement<TransactionReconciliationMessageRequest> createTransactionReconciliationMessageRequest(TransactionReconciliationMessageRequest value) {
        return new JAXBElement<TransactionReconciliationMessageRequest>(_TransactionReconciliationMessageRequest_QNAME, TransactionReconciliationMessageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RefuelDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "refuelDetail")
    public JAXBElement<RefuelDetail> createRefuelDetail(RefuelDetail value) {
        return new JAXBElement<RefuelDetail>(_RefuelDetail_QNAME, RefuelDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionStatusMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "transactionStatusMessageResponse")
    public JAXBElement<TransactionStatusMessageResponse> createTransactionStatusMessageResponse(TransactionStatusMessageResponse value) {
        return new JAXBElement<TransactionStatusMessageResponse>(_TransactionStatusMessageResponse_QNAME, TransactionStatusMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StationDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://simulazioni.mp.techedge.com/", name = "stationDetail")
    public JAXBElement<StationDetail> createStationDetail(StationDetail value) {
        return new JAXBElement<StationDetail>(_StationDetail_QNAME, StationDetail.class, null, value);
    }

}
