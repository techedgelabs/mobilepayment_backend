
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sendPaymentTransactionResultMessageRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendPaymentTransactionResultMessageRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="bankTransactionResults" type="{http://simulazioni.mp.techedge.com/}bankTransactionResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendPaymentTransactionResultMessageRequest", propOrder = {
    "requestID",
    "transactionID",
    "amount",
    "bankTransactionResults"
})
public class SendPaymentTransactionResultMessageRequest {

    @XmlElement(required = true)
    protected String requestID;
    @XmlElement(required = true)
    protected String transactionID;
    protected Double amount;
    @XmlElement(required = true)
    protected BankTransactionResult bankTransactionResults;

    /**
     * Gets the value of the requestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAmount(Double value) {
        this.amount = value;
    }

    /**
     * Gets the value of the bankTransactionResults property.
     * 
     * @return
     *     possible object is
     *     {@link BankTransactionResult }
     *     
     */
    public BankTransactionResult getBankTransactionResults() {
        return bankTransactionResults;
    }

    /**
     * Sets the value of the bankTransactionResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankTransactionResult }
     *     
     */
    public void setBankTransactionResults(BankTransactionResult value) {
        this.bankTransactionResults = value;
    }

}
