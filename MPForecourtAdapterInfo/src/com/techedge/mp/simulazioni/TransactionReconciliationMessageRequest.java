
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transactionReconciliationMessageRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionReconciliationMessageRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionIDList" type="{http://simulazioni.mp.techedge.com/}transactionID"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionReconciliationMessageRequest", propOrder = {
    "requestID",
    "transactionIDList"
})
public class TransactionReconciliationMessageRequest {

    @XmlElement(required = true)
    protected String requestID;
    @XmlElement(required = true)
    protected TransactionID transactionIDList;

    /**
     * Gets the value of the requestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Gets the value of the transactionIDList property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionID }
     *     
     */
    public TransactionID getTransactionIDList() {
        return transactionIDList;
    }

    /**
     * Sets the value of the transactionIDList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionID }
     *     
     */
    public void setTransactionIDList(TransactionID value) {
        this.transactionIDList = value;
    }

}
