
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTransactionStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTransactionStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionStatusRequest" type="{http://simulazioni.mp.techedge.com/}transactionStatusMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransactionStatus", propOrder = {
    "transactionStatusRequest"
})
public class GetTransactionStatus {

    @XmlElement(required = true)
    protected TransactionStatusMessageRequest transactionStatusRequest;

    /**
     * Gets the value of the transactionStatusRequest property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionStatusMessageRequest }
     *     
     */
    public TransactionStatusMessageRequest getTransactionStatusRequest() {
        return transactionStatusRequest;
    }

    /**
     * Sets the value of the transactionStatusRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionStatusMessageRequest }
     *     
     */
    public void setTransactionStatusRequest(TransactionStatusMessageRequest value) {
        this.transactionStatusRequest = value;
    }

}
