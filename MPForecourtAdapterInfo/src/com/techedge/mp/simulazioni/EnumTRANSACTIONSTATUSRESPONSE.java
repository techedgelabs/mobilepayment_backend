
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumTRANSACTIONSTATUSRESPONSE.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enumTRANSACTIONSTATUSRESPONSE">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REFUEL_TERMINATED_200"/>
 *     &lt;enumeration value="REFUEL_NOT_STARTED_400"/>
 *     &lt;enumeration value="REFUEL_INPROGRESS_401"/>
 *     &lt;enumeration value="TRANSACTION_NOT_RECOGNIZED_400"/>
 *     &lt;enumeration value="REFUEL_ABORTED_500"/>
 *     &lt;enumeration value="PARAMETER_NOT_FOUND_400"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enumTRANSACTIONSTATUSRESPONSE")
@XmlEnum
public enum EnumTRANSACTIONSTATUSRESPONSE {

    REFUEL_TERMINATED_200,
    REFUEL_NOT_STARTED_400,
    REFUEL_INPROGRESS_401,
    TRANSACTION_NOT_RECOGNIZED_400,
    REFUEL_ABORTED_500,
    PARAMETER_NOT_FOUND_400;

    public String value() {
        return name();
    }

    public static EnumTRANSACTIONSTATUSRESPONSE fromValue(String v) {
        return valueOf(v);
    }

}
