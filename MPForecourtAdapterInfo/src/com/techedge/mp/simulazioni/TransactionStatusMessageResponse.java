
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transactionStatusMessageResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionStatusMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="statusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="messageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://simulazioni.mp.techedge.com/}refuelDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionStatusMessageResponse", propOrder = {
    "statusCode",
    "messageCode",
    "refuelDetail"
})
public class TransactionStatusMessageResponse {

    @XmlElement(required = true, nillable = true)
    protected String statusCode;
    @XmlElement(required = true, nillable = true)
    protected String messageCode;
    @XmlElement(namespace = "http://simulazioni.mp.techedge.com/")
    protected RefuelDetail refuelDetail;

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the messageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Sets the value of the messageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    /**
     * Gets the value of the refuelDetail property.
     * 
     * @return
     *     possible object is
     *     {@link RefuelDetail }
     *     
     */
    public RefuelDetail getRefuelDetail() {
        return refuelDetail;
    }

    /**
     * Sets the value of the refuelDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefuelDetail }
     *     
     */
    public void setRefuelDetail(RefuelDetail value) {
        this.refuelDetail = value;
    }

}
