
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPumpStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPumpStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pumpStatusRequest" type="{http://simulazioni.mp.techedge.com/}pumpStatusMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPumpStatus", propOrder = {
    "pumpStatusRequest"
})
public class GetPumpStatus {

    @XmlElement(required = true)
    protected PumpStatusMessageRequest pumpStatusRequest;

    /**
     * Gets the value of the pumpStatusRequest property.
     * 
     * @return
     *     possible object is
     *     {@link PumpStatusMessageRequest }
     *     
     */
    public PumpStatusMessageRequest getPumpStatusRequest() {
        return pumpStatusRequest;
    }

    /**
     * Sets the value of the pumpStatusRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link PumpStatusMessageRequest }
     *     
     */
    public void setPumpStatusRequest(PumpStatusMessageRequest value) {
        this.pumpStatusRequest = value;
    }

}
