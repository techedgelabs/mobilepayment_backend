
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTransactionStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTransactionStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionStatusResponse" type="{http://simulazioni.mp.techedge.com/}transactionStatusMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransactionStatusResponse", propOrder = {
    "transactionStatusResponse"
})
public class GetTransactionStatusResponse {

    @XmlElement(required = true)
    protected TransactionStatusMessageResponse transactionStatusResponse;

    /**
     * Gets the value of the transactionStatusResponse property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionStatusMessageResponse }
     *     
     */
    public TransactionStatusMessageResponse getTransactionStatusResponse() {
        return transactionStatusResponse;
    }

    /**
     * Sets the value of the transactionStatusResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionStatusMessageResponse }
     *     
     */
    public void setTransactionStatusResponse(TransactionStatusMessageResponse value) {
        this.transactionStatusResponse = value;
    }

}
