package com.techedge.mp.simulazioni.clientsample;

import com.techedge.mp.simulazioni.*;

public class ClientSample {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        ForecourtEmulatorService service1 = new ForecourtEmulatorService();
	        System.out.println("Create Web Service...");
	        ForecourtEmulator port1 = service1.getForecourtEmulatorPort();
	        System.out.println("Call Web Service Operation...");
	        //System.out.println("Server said: " + port1.getPumpStatus(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        //System.out.println("Server said: " + port1.getTransactionStatus(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        //System.out.println("Server said: " + port1.getStationDetails(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        //System.out.println("Server said: " + port1.transactionReconciliation(null));
	        //Please input the parameters instead of 'null' for the upper method!
	        
	        EnablePumpMessageRequest enablePumpMessageRequest = new EnablePumpMessageRequest();
	        enablePumpMessageRequest.setAmount(1.0);
	        enablePumpMessageRequest.setPaymentMode("MobilePayment");
	        enablePumpMessageRequest.setProductID("1");
	        enablePumpMessageRequest.setPumpID("10001");
	        enablePumpMessageRequest.setRequestID("12345678");
	        enablePumpMessageRequest.setStationID("1");
	        enablePumpMessageRequest.setTransactionID("sdaiugcxaimm");
	        
	        port1.enablePump(enablePumpMessageRequest);
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.sendPaymentTransactionResult(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Create Web Service...");
	        ForecourtEmulator port2 = service1.getForecourtEmulatorPort();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port2.getPumpStatus(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.getTransactionStatus(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.getStationDetails(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.transactionReconciliation(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.enablePump(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.sendPaymentTransactionResult(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
