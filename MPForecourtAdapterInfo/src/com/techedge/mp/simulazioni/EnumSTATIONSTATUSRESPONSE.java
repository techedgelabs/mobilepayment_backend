
package com.techedge.mp.simulazioni;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumSTATIONSTATUSRESPONSE.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enumSTATIONSTATUSRESPONSE">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="STATION_PUMP_FOUND_200"/>
 *     &lt;enumeration value="STATION_NOT_FOUND_404"/>
 *     &lt;enumeration value="PUMP_NOT_FOUND_404"/>
 *     &lt;enumeration value="STATION_PUMP_NOT_FOUND_404"/>
 *     &lt;enumeration value="PUMP_STATUS_NOT_AVAILABLE_500"/>
 *     &lt;enumeration value="PARAMETER_NOT_FOUND_400"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enumSTATIONSTATUSRESPONSE")
@XmlEnum
public enum EnumSTATIONSTATUSRESPONSE {

    STATION_PUMP_FOUND_200,
    STATION_NOT_FOUND_404,
    PUMP_NOT_FOUND_404,
    STATION_PUMP_NOT_FOUND_404,
    PUMP_STATUS_NOT_AVAILABLE_500,
    PARAMETER_NOT_FOUND_400;

    public String value() {
        return name();
    }

    public static EnumSTATIONSTATUSRESPONSE fromValue(String v) {
        return valueOf(v);
    }

}
