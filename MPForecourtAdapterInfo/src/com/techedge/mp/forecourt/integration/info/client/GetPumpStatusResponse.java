
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPumpStatusMessageResponse" type="{http://gatewaymobilepayment.4ts.it/}getPumpStatusMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPumpStatusMessageResponse"
})
@XmlRootElement(name = "getPumpStatusResponse")
public class GetPumpStatusResponse {

    @XmlElement(required = true, nillable = true)
    protected GetPumpStatusMessageResponse getPumpStatusMessageResponse;

    /**
     * Recupera il valore della proprietÓ getPumpStatusMessageResponse.
     * 
     * @return
     *     possible object is
     *     {@link GetPumpStatusMessageResponse }
     *     
     */
    public GetPumpStatusMessageResponse getGetPumpStatusMessageResponse() {
        return getPumpStatusMessageResponse;
    }

    /**
     * Imposta il valore della proprietÓ getPumpStatusMessageResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPumpStatusMessageResponse }
     *     
     */
    public void setGetPumpStatusMessageResponse(GetPumpStatusMessageResponse value) {
        this.getPumpStatusMessageResponse = value;
    }

}
