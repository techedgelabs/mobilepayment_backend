
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stationStatusResponse" type="{http://gatewaymobilepayment.4ts.it/}stationStatusMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "stationStatusResponse"
})
@XmlRootElement(name = "getStationDetailsResponse")
public class GetStationDetailsResponse {

    @XmlElement(required = true, nillable = true)
    protected StationStatusMessageResponse stationStatusResponse;

    /**
     * Recupera il valore della proprietÓ stationStatusResponse.
     * 
     * @return
     *     possible object is
     *     {@link StationStatusMessageResponse }
     *     
     */
    public StationStatusMessageResponse getStationStatusResponse() {
        return stationStatusResponse;
    }

    /**
     * Imposta il valore della proprietÓ stationStatusResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link StationStatusMessageResponse }
     *     
     */
    public void setStationStatusResponse(StationStatusMessageResponse value) {
        this.stationStatusResponse = value;
    }

}
