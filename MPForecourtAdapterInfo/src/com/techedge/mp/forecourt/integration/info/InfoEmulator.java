package com.techedge.mp.forecourt.integration.info;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.techedge.mp.simulazioni.EnablePumpMessageRequest;
import com.techedge.mp.simulazioni.EnablePumpMessageResponse;
import com.techedge.mp.simulazioni.ProductDetail;
import com.techedge.mp.simulazioni.PumpDetail;
import com.techedge.mp.simulazioni.PumpStatusMessageRequest;
import com.techedge.mp.simulazioni.PumpStatusMessageResponse;
import com.techedge.mp.simulazioni.RefuelDetail;
import com.techedge.mp.simulazioni.SendPaymentTransactionResultMessageRequest;
import com.techedge.mp.simulazioni.SendPaymentTransactionResultMessageResponse;
import com.techedge.mp.simulazioni.StationDetail;
import com.techedge.mp.simulazioni.StationStatusMessageRequest;
import com.techedge.mp.simulazioni.StationStatusMessageResponse;
import com.techedge.mp.simulazioni.TransactionReconciliationMessageRequest;
import com.techedge.mp.simulazioni.TransactionReconciliationMessageResponse;
import com.techedge.mp.simulazioni.TransactionStatusMessageRequest;
import com.techedge.mp.simulazioni.TransactionStatusMessageResponse;

public class InfoEmulator {

	
	public static PumpStatusMessageResponse getPumpStatus(PumpStatusMessageRequest pumpStatusMessageRequest) {
		
		String pumpID = pumpStatusMessageRequest.getPumpID();
		String stationID = pumpStatusMessageRequest.getStationID();
		
		PumpStatusMessageResponse pumpStatusMessageResponse = new PumpStatusMessageResponse();
		
		pumpStatusMessageResponse.setStatusCode("PUMP_NOT_FOUND_404");
		
		if ( !stationID.equals("00001")) {
			pumpStatusMessageResponse.setStatusCode("STATION_NOT_FOUND_404");
			return pumpStatusMessageResponse;
		}
		
		if ( pumpID.equals("P0000101")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_BUSY_401");
		}
		if ( pumpID.equals("P0000102")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_NOT_AVAILABLE_501");
		}
		if ( pumpID.equals("P0000103")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_STATUS_NOT_AVAILABLE_502");
		}
		if ( pumpID.equals("P0000104")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000105")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000106")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000107")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000108")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000109")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000110")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000111")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000112")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000113")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000114")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		if ( pumpID.equals("P0000115")) {
			pumpStatusMessageResponse.setStatusCode("PUMP_AVAILABLE_200");
		}
		
		return pumpStatusMessageResponse;
	}
	
	
	public static StationStatusMessageResponse getStationDetails(StationStatusMessageRequest stationStatusMessageRequest) {
		
		StationStatusMessageResponse stationStatusMessageResponse = new StationStatusMessageResponse();
		
		if ( stationStatusMessageRequest.getPumpID().equals("P0000101") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000102") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000103") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000104") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000105") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000106") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000107") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000108") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000109") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000110") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000111") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000112") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000113") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000114") ||
			 stationStatusMessageRequest.getPumpID().equals("P0000115")
			) {
		
			stationStatusMessageResponse.setStatusCode("STATION_PUMP_FOUND_200");
			stationStatusMessageResponse.setMessageCode("Station/Pump found");
			
			
			StationDetail stationDetail = new StationDetail();
			stationDetail.setAddress("via Salvatore Quasimodo, 136, Roma");
			stationDetail.setCountry("Italia");
			stationDetail.setLatitude("12.345612");
			stationDetail.setLongitude("43.235898");
			stationDetail.setProvince("RM");
			stationDetail.setStationID("00001");
			
			XMLGregorianCalendar validityDate = null;
			try {
				GregorianCalendar cal = new GregorianCalendar();
				cal.set(2014, 11, 01);
				validityDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				
			}
			catch (Exception ex) {
				
			}
			stationDetail.setValidityDateDetails(validityDate);
			
			
			// Definizione pompe di test
			
			// Pompa 10001
			PumpDetail pumpDetail0001 = new PumpDetail();
			pumpDetail0001.setPumpID("P0000101");
			pumpDetail0001.setPumpNumber("1");
			pumpDetail0001.setPumpStatus("PUMP_BUSY_401");
			pumpDetail0001.setRefuelMode("Servito");
			
			ProductDetail productDetail = new ProductDetail();
			productDetail.setProductID("id01");
			productDetail.setProductDescription("diesel");
			productDetail.setProductPrice(1.000);
			productDetail.setFuelType("diesel");
			pumpDetail0001.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail0001);
	
			
			// Pompa 10002
			PumpDetail pumpDetail10002 = new PumpDetail();
			pumpDetail10002.setPumpID("P0000102");
			pumpDetail10002.setPumpNumber("2");
			pumpDetail10002.setPumpStatus("PUMP_NOT_AVAILABLE_501");
			pumpDetail10002.setRefuelMode("Servito");
			pumpDetail10002.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10002);
			
			
			// Pompa 10003
			PumpDetail pumpDetail10003 = new PumpDetail();
			pumpDetail10003.setPumpID("P0000103");
			pumpDetail10003.setPumpNumber("3");
			pumpDetail10003.setPumpStatus("PUMP_STATUS_NOT_AVAILABLE_502");
			pumpDetail10003.setRefuelMode("Servito");
			pumpDetail10003.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10003);
			
			// Pompa 10004
			PumpDetail pumpDetail10004 = new PumpDetail();
			pumpDetail10004.setPumpID("P0000104");
			pumpDetail10004.setPumpNumber("4");
			pumpDetail10004.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10004.setRefuelMode("Servito");
			pumpDetail10004.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10004);
			
			
			// Pompa 10005
			PumpDetail pumpDetail10005 = new PumpDetail();
			pumpDetail10005.setPumpID("P0000105");
			pumpDetail10005.setPumpNumber("5");
			pumpDetail10005.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10005.setRefuelMode("Servito");
			pumpDetail10005.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10005);
			
			
			// Pompa 10006
			PumpDetail pumpDetail10006 = new PumpDetail();
			pumpDetail10006.setPumpID("P0000106");
			pumpDetail10006.setPumpNumber("6");
			pumpDetail10006.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10006.setRefuelMode("Servito");
			pumpDetail10006.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10006);
			
			
			// Pompa 10007
			PumpDetail pumpDetail10007 = new PumpDetail();
			pumpDetail10007.setPumpID("P0000107");
			pumpDetail10007.setPumpNumber("7");
			pumpDetail10007.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10007.setRefuelMode("Servito");
			pumpDetail10007.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10007);
			
			
			// Pompa 10008
			PumpDetail pumpDetail10008 = new PumpDetail();
			pumpDetail10008.setPumpID("P0000108");
			pumpDetail10008.setPumpNumber("8");
			pumpDetail10008.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10008.setRefuelMode("Servito");
			pumpDetail10008.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10008);
			
			
			// Pompa 10009
			PumpDetail pumpDetail10009 = new PumpDetail();
			pumpDetail10009.setPumpID("P0000109");
			pumpDetail10009.setPumpNumber("9");
			pumpDetail10009.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10009.setRefuelMode("Servito");
			pumpDetail10009.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10009);

			
			// Pompa 10010
			PumpDetail pumpDetail10010 = new PumpDetail();
			pumpDetail10010.setPumpID("P0000110");
			pumpDetail10010.setPumpNumber("10");
			pumpDetail10010.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10010.setRefuelMode("Servito");
			pumpDetail10010.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10010);

			
			// Pompa 10011
			PumpDetail pumpDetail10011 = new PumpDetail();
			pumpDetail10011.setPumpID("P0000111");
			pumpDetail10011.setPumpNumber("11");
			pumpDetail10011.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10011.setRefuelMode("Servito");
			pumpDetail10011.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10011);

			
			// Pompa 10012
			PumpDetail pumpDetail10012 = new PumpDetail();
			pumpDetail10012.setPumpID("P0000112");
			pumpDetail10012.setPumpNumber("12");
			pumpDetail10012.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10012.setRefuelMode("Servito");
			pumpDetail10012.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10012);

			
			// Pompa 10013
			PumpDetail pumpDetail10013 = new PumpDetail();
			pumpDetail10013.setPumpID("P0000113");
			pumpDetail10013.setPumpNumber("13");
			pumpDetail10013.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10013.setRefuelMode("Servito");
			pumpDetail10013.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10013);

			
			// Pompa 10014
			PumpDetail pumpDetail10014 = new PumpDetail();
			pumpDetail10014.setPumpID("P0000114");
			pumpDetail10014.setPumpNumber("14");
			pumpDetail10014.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10014.setRefuelMode("Servito");
			pumpDetail10014.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10014);

			
			// Pompa 10015
			PumpDetail pumpDetail10015 = new PumpDetail();
			pumpDetail10015.setPumpID("P0000115");
			pumpDetail10015.setPumpNumber("15");
			pumpDetail10015.setPumpStatus("PUMP_AVAILABLE_200");
			pumpDetail10015.setRefuelMode("Servito");
			pumpDetail10015.getProductDetails().add(productDetail);
			
			stationDetail.getPumpDetails().add(pumpDetail10015);
			
			
			stationStatusMessageResponse.setStationDetail(stationDetail);
		}
		else {
			
			stationStatusMessageResponse.setStatusCode("STATION_PUMP_NOT_FOUND_404");
			stationStatusMessageResponse.setMessageCode("Station AND Pump Not Found");
		}
		
		return stationStatusMessageResponse;
	}
	
	
	public static EnablePumpMessageResponse enablePump(EnablePumpMessageRequest enablePumpMessageRequest) {
		
		String pumpID = enablePumpMessageRequest.getPumpID();
		String stationID = enablePumpMessageRequest.getStationID();
		
		EnablePumpMessageResponse enablePumpMessageResponse = new EnablePumpMessageResponse();
		
		enablePumpMessageResponse.setStatusCode("PUMP_NOT_FOUND_404");
		
		if ( !stationID.equals("00001")) {
			enablePumpMessageResponse.setStatusCode("STATION_NOT_FOUND_404");
			return enablePumpMessageResponse;
		}
		
		if ( pumpID.equals("P0000104")) {
			enablePumpMessageResponse.setStatusCode("PUMP_BUSY_401");
		}
		if ( pumpID.equals("P0000105")) {
			enablePumpMessageResponse.setStatusCode("PUMP_NOT_AVAILABLE_501");
		}
		if ( pumpID.equals("P0000106")) {
			enablePumpMessageResponse.setStatusCode("PUMP_STATUS_NOT_AVAILABLE_502");
		}
		if ( pumpID.equals("P0000107")) {
			enablePumpMessageResponse.setStatusCode("PUMP_ENABLED_200");
		}
		if ( pumpID.equals("P0000108")) {
			enablePumpMessageResponse.setStatusCode("PUMP_ENABLED_200");
		}
		if ( pumpID.equals("P0000109")) {
			enablePumpMessageResponse.setStatusCode("PUMP_ENABLED_200");
		}
		if ( pumpID.equals("P0000110")) {
			enablePumpMessageResponse.setStatusCode("PUMP_ENABLED_200");
		}
		if ( pumpID.equals("P0000111")) {
			enablePumpMessageResponse.setStatusCode("PUMP_ENABLED_200");
		}
		if ( pumpID.equals("P0000112")) {
			enablePumpMessageResponse.setStatusCode("PUMP_ENABLED_200");
		}
		if ( pumpID.equals("P0000113")) {
			enablePumpMessageResponse.setStatusCode("PUMP_ENABLED_200");
		}
		if ( pumpID.equals("P0000114")) {
			enablePumpMessageResponse.setStatusCode("PUMP_ENABLED_200");
		}
		if ( pumpID.equals("P0000115")) {
			enablePumpMessageResponse.setStatusCode("PUMP_ENABLED_200");
		}
		
		return enablePumpMessageResponse;
	}
	
	
	public static TransactionStatusMessageResponse getTransactionStatus(TransactionStatusMessageRequest transactionStatusMessageRequest) {
		
		TransactionStatusMessageResponse transactionStatusMessageResponse = new TransactionStatusMessageResponse();
		
		if(transactionStatusMessageRequest.getRequestID() == null || transactionStatusMessageRequest.getRequestID().trim().isEmpty()) {
			
			transactionStatusMessageResponse.setStatusCode("PARAMETER_NOT_FOUND_400");
			transactionStatusMessageResponse.setMessageCode("Required parameter not found");
			
		}
		
		if(transactionStatusMessageRequest.getTransactionID() == null || transactionStatusMessageRequest.getTransactionID().trim().isEmpty()) {
			
			transactionStatusMessageResponse.setStatusCode("PARAMETER_NOT_FOUND_400");
			transactionStatusMessageResponse.setMessageCode("Required parameter not found");
			
		}
		
		transactionStatusMessageResponse.setStatusCode("REFUEL_INPROGRESS_401");
		transactionStatusMessageResponse.setMessageCode("Refuel In Progress");
		
		RefuelDetail refueld = new RefuelDetail();
		refueld.setAmount(1.00);
		refueld.setFuelQuantity(1.50);
		refueld.setFuelType("Idrogeno");
		refueld.setProductDescription("rifornimento");
		refueld.setProductID("poi");
		refueld.setTimestampEndRefuel("654324567L");
		
		transactionStatusMessageResponse.setRefuelDetail(refueld);
		
		return transactionStatusMessageResponse;
	}
	
	
	public static TransactionReconciliationMessageResponse transactionReconciliation(TransactionReconciliationMessageRequest transactionReconciliationMessageRequest) {
		
		TransactionReconciliationMessageResponse transactionReconciliationMessageResponse = new TransactionReconciliationMessageResponse();
		
		return transactionReconciliationMessageResponse;
	}
	
	
	public static SendPaymentTransactionResultMessageResponse sendPaymentTransactionResult(SendPaymentTransactionResultMessageRequest sendPaymentTransactionResultMessageRequest) {
		
		SendPaymentTransactionResultMessageResponse sendPaymentTransactionResultMessageResponse = new SendPaymentTransactionResultMessageResponse();
		
		sendPaymentTransactionResultMessageResponse.setStatusCode("MESSAGE_RECEIVED_200");
		
		return sendPaymentTransactionResultMessageResponse;
	}
}
