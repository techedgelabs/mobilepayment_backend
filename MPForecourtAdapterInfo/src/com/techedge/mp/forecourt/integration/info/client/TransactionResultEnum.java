
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per transactionResultEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="transactionResultEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="KO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "transactionResultEnum")
@XmlEnum
public enum TransactionResultEnum {

    OK,
    KO;

    public String value() {
        return name();
    }

    public static TransactionResultEnum fromValue(String v) {
        return valueOf(v);
    }

}
