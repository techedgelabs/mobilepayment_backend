package com.techedge.mp.forecourt.integration.info.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "status", propOrder = {
	"statusCode",
	"messageCode"
})
public class Status {

	
	@XmlElement(required = true)
	protected ENUM_STATUS statusCode;
	@XmlElement(required = true)
	protected String messageCode;
	
	
	public ENUM_STATUS getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(ENUM_STATUS statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	
	
}
