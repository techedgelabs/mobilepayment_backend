
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPumpStatusMessageRequest" type="{http://gatewaymobilepayment.4ts.it/}getPumpStatusMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPumpStatusMessageRequest"
})
@XmlRootElement(name = "getPumpStatus")
public class GetPumpStatus {

    @XmlElement(required = true, nillable = true)
    protected GetPumpStatusMessageRequest getPumpStatusMessageRequest;

    /**
     * Recupera il valore della proprietÓ getPumpStatusMessageRequest.
     * 
     * @return
     *     possible object is
     *     {@link GetPumpStatusMessageRequest }
     *     
     */
    public GetPumpStatusMessageRequest getGetPumpStatusMessageRequest() {
        return getPumpStatusMessageRequest;
    }

    /**
     * Imposta il valore della proprietÓ getPumpStatusMessageRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPumpStatusMessageRequest }
     *     
     */
    public void setGetPumpStatusMessageRequest(GetPumpStatusMessageRequest value) {
        this.getPumpStatusMessageRequest = value;
    }

}
