
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enablePumpMessageResponse" type="{http://gatewaymobilepayment.4ts.it/}enablePumpMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enablePumpMessageResponse"
})
@XmlRootElement(name = "enablePumpResponse")
public class EnablePumpResponse {

    @XmlElement(required = true, nillable = true)
    protected EnablePumpMessageResponse enablePumpMessageResponse;

    /**
     * Recupera il valore della proprietÓ enablePumpMessageResponse.
     * 
     * @return
     *     possible object is
     *     {@link EnablePumpMessageResponse }
     *     
     */
    public EnablePumpMessageResponse getEnablePumpMessageResponse() {
        return enablePumpMessageResponse;
    }

    /**
     * Imposta il valore della proprietÓ enablePumpMessageResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link EnablePumpMessageResponse }
     *     
     */
    public void setEnablePumpMessageResponse(EnablePumpMessageResponse value) {
        this.enablePumpMessageResponse = value;
    }

}
