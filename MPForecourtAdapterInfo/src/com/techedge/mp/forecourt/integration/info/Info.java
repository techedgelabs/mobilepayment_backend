package com.techedge.mp.forecourt.integration.info;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.EventInfo;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PlateNumber;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.RetrieveTransactionEventsData;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionAdditionalData;
import com.techedge.mp.core.business.interfaces.TransactionEvent;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.PersonalDataBusiness;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.forecourt.integration.info.client.EventTypeEnum;
import com.techedge.mp.forecourt.integration.info.client.GatewayMobilePaymentImpl;
import com.techedge.mp.forecourt.integration.info.client.GetPumpStatusMessageRequest;
import com.techedge.mp.forecourt.integration.info.client.GetPumpStatusMessageResponse;
import com.techedge.mp.forecourt.integration.info.client.GetStationDetailsMessageRequest;
import com.techedge.mp.forecourt.integration.info.client.GetTransactionStatusResponseEnum;
import com.techedge.mp.forecourt.integration.info.client.IGatewayMobilePayment;
import com.techedge.mp.forecourt.integration.info.client.PaymentAuthorizationResult;
import com.techedge.mp.forecourt.integration.info.client.PaymentTransactionResult;
import com.techedge.mp.forecourt.integration.info.client.ProductDetail;
import com.techedge.mp.forecourt.integration.info.client.ProductIdEnum;
import com.techedge.mp.forecourt.integration.info.client.PumpDetail;
import com.techedge.mp.forecourt.integration.info.client.PumpStatusEnum;
import com.techedge.mp.forecourt.integration.info.client.SendPaymentTransactionResultResponseEnum;
import com.techedge.mp.forecourt.integration.info.client.StationStatusMessageResponseEnum;
import com.techedge.mp.forecourt.integration.info.client.TransactionDetails;
import com.techedge.mp.forecourt.integration.info.client.TransactionReconciliationMessageRequest;
import com.techedge.mp.forecourt.integration.info.client.TransactionReconciliationMessageResponse;
import com.techedge.mp.forecourt.integration.info.client.TransactionResultEnum;
import com.techedge.mp.forecourt.integration.info.client.TransactionStatusHistory;
import com.techedge.mp.forecourt.integration.info.client.Voucher;
import com.techedge.mp.forecourt.integration.info.entities.PumpStatusMessageRequest;
import com.techedge.mp.forecourt.integration.info.exception.BPELException;
import com.techedge.mp.simulazioni.EnablePumpMessageRequest;
import com.techedge.mp.simulazioni.EnablePumpMessageResponse;
import com.techedge.mp.simulazioni.EnumENABLEPUMPSTATUSRESPONSE;
import com.techedge.mp.simulazioni.EnumPUMPSTAUSRESPONSE;
import com.techedge.mp.simulazioni.EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE;
import com.techedge.mp.simulazioni.EnumSTATIONSTATUSRESPONSE;
import com.techedge.mp.simulazioni.EnumTRANSACTIONSTATUSRESPONSE;
import com.techedge.mp.simulazioni.PumpStatusMessageResponse;
import com.techedge.mp.simulazioni.RefuelDetail;
import com.techedge.mp.simulazioni.SendPaymentTransactionResultMessageRequest;
import com.techedge.mp.simulazioni.SendPaymentTransactionResultMessageResponse;
import com.techedge.mp.simulazioni.StationDetail;
import com.techedge.mp.simulazioni.StationStatusMessageRequest;
import com.techedge.mp.simulazioni.StationStatusMessageResponse;
import com.techedge.mp.simulazioni.TransactionStatusMessageRequest;
import com.techedge.mp.simulazioni.TransactionStatusMessageResponse;

@WebService()
public class Info {

    private final static String      PARAM_FORECOURT_WSDL                            = "FORECOURT_WSDL";
    private final static String      REFUEL_MODE_SELF                                = "Self";

    private Properties               prop                                            = new Properties();

    private TransactionServiceRemote transactionService                              = null;
    private ParametersServiceRemote  parametersService                               = null;
    private LoggerServiceRemote      loggerService                                   = null;

    private final static String      PARAM_SIMULATION_ACTIVE                         = "SIMULATION_ACTIVE";
    private final static String      PARAM_SIMULATION_GFG_ENABLEPUMP_ERROR           = "SIMULATION_GFG_ENABLEPUMP_ERROR";
    private final static String      PARAM_SIMULATION_GFG_GETTRANSACTIONSTATUS_ERROR = "SIMULATION_GFG_GETTRANSACTIONSTATUS_ERROR";
    private final static String      PARAM_SIMULATION_GFG_ENABLEPUMP_KO              = "SIMULATION_GFG_ENABLEPUMP_KO";
    private final static String      PARAM_SIMULATION_GFG_GETTRANSACTIONSTATUS_KO    = "SIMULATION_GFG_GETTRANSACTIONSTATUS_KO";
    private final static String      PARAM_SIMULATION_SENDPAYMENTTRANSACTIONRESULT_ERROR = "SIMULATION_GFG_SENDPAYMENTTRANSACTIONRESULT_ERROR";
    private final static String      PARAM_SIMULATION_SENDPAYMENTTRANSACTIONRESULT_KO    = "SIMULATION_GFG_SENDPAYMENTTRANSACTIONRESULT_KO";
    private final static String      PARAM_GFG_CLIENT_CONNECTION_TIMEOUT                 = "GFG_CLIENT_CONNECTION_TIMEOUT";

    private URL                      url;

    private boolean                  simulationActive                                = false;
    
    private Integer                  gfgClientConnectionTimeout                      = 0;

    public Info() throws BPELException {

        loadFileProperty();

    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }
    
    private static class TrustAllCertificates implements X509TrustManager {
        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }
     
        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
     
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
    
    private HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, javax.net.ssl.SSLSession session) {
            return true;
        }
    };

    private URL getUrl() throws BPELException {

        if (this.url == null) {
            
            TrustManager[] tm = null;
            tm = new TrustManager[] {new TrustAllCertificates()};
            
            SSLContext sslContext;
            try {
                sslContext = SSLContext.getInstance("TLSv1.2");
                sslContext.init(null, tm, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            }
            catch (NoSuchAlgorithmException e1) {
                
                e1.printStackTrace();
                
                throw new BPELException("NoSuchAlgorithmException" + e1.getMessage());
            }
            catch (KeyManagementException e) {
                
                e.printStackTrace();
                
                throw new BPELException("KeyManagementException" + e.getMessage());
            }

            String wsdlString = "";
            try {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
                wsdlString = parametersService.getParamValue(Info.PARAM_FORECOURT_WSDL);
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();

                throw new BPELException("Parameter " + Info.PARAM_FORECOURT_WSDL + " not found: " + e.getMessage());
            }
            catch (InterfaceNotFoundException e) {

                e.printStackTrace();

                throw new BPELException("InterfaceNotFoundException for jndi " + e.getJndiString());
            }

            try {
                this.url = new URL(wsdlString);
            }
            catch (MalformedURLException e) {

                e.printStackTrace();

                throw new BPELException("Malformed URL: " + e.getMessage());
            }

            try {
                this.simulationActive = Boolean.parseBoolean(parametersService.getParamValue(Info.PARAM_SIMULATION_ACTIVE));
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();

                throw new BPELException("Parameter for Eni Web Service not found: " + e.getMessage());
            }

            if (simulationActive) {
                System.out.println("Simulazione GFG attiva");
            }
            
            try {
                this.gfgClientConnectionTimeout = Integer.parseInt(parametersService.getParamValue(Info.PARAM_GFG_CLIENT_CONNECTION_TIMEOUT));
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();

                throw new BPELException("Parameter " + Info.PARAM_GFG_CLIENT_CONNECTION_TIMEOUT + " not found: " + e.getMessage());
            }
        }

        return this.url;
    }

    @WebMethod(operationName = "getPumpStatus")
    public @XmlElement(required = true, name = "pumpStatusMessageResponse")
    PumpStatusMessageResponse getPumpStatus(@WebParam(name = "pumpStatusRequest") @XmlElement(required = true) PumpStatusMessageRequest psmRequest) throws BPELException {

        Date now = new Date();
        String requestID = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("pumpID", psmRequest.getPumpID()));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("stationID", psmRequest.getStationID()));
        inputParameters.add(new Pair<String, String>("transactionID", psmRequest.getTransactionID()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPumpStatus", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        PumpStatusMessageResponse psmResponse = new PumpStatusMessageResponse();

        Boolean errorFound = false;

        if (psmRequest.getStationID() == null || psmRequest.getStationID().trim().isEmpty()) {

            psmResponse.setStatusCode(EnumPUMPSTAUSRESPONSE.PUMP_NOT_FOUND_404.name());
            psmResponse.setMessageCode(prop.getProperty(EnumPUMPSTAUSRESPONSE.PUMP_NOT_FOUND_404.name()));
            errorFound = true;

        }

        if (psmRequest.getPumpID() == null || psmRequest.getPumpID().trim().isEmpty()) {

            psmResponse.setStatusCode(EnumPUMPSTAUSRESPONSE.PUMP_NOT_FOUND_404.name());
            psmResponse.setMessageCode(prop.getProperty(EnumPUMPSTAUSRESPONSE.PUMP_NOT_FOUND_404.name()));
            errorFound = true;

        }

        if (!errorFound) {

            try {

                // Log timestamp chiamata
                Date currentTimestamp = new Date();
                String currentTimestampString = String.valueOf(currentTimestamp.getTime());

                Set<Pair<String, String>> remoteServiceInputParameters = new HashSet<Pair<String, String>>();
                remoteServiceInputParameters.add(new Pair<String, String>("timestamp", currentTimestampString));

                this.log(ErrorLevel.DEBUG, "WsEndpointGatewayMobilePayment", "getPumpStatus", requestID, "opening", ActivityLog.createLogMessage(remoteServiceInputParameters));

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                new Proxy().unsetHttp();

                GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());

                //Originale Forecourt
                IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
                //Chiamata Forecourt Emulator
                //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();
                
                ((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.connectionTimeout", this.gfgClientConnectionTimeout);

                GetPumpStatusMessageRequest requestForecourt = new GetPumpStatusMessageRequest();

                requestForecourt.setPumpID(psmRequest.getPumpID());
                requestForecourt.setRequestID(requestID);
                requestForecourt.setStationID(psmRequest.getStationID());

                GetPumpStatusMessageResponse forecourtResponse = port1.getPumpStatus(requestForecourt);

                // Log timestamp chiamata
                currentTimestamp = new Date();
                currentTimestampString = String.valueOf(currentTimestamp.getTime());

                Set<Pair<String, String>> remoteServiceOutputParameters = new HashSet<Pair<String, String>>();
                remoteServiceOutputParameters.add(new Pair<String, String>("timestamp", currentTimestampString));

                this.log(ErrorLevel.DEBUG, "WsEndpointGatewayMobilePayment", "getPumpStatus", requestID, "closing", ActivityLog.createLogMessage(remoteServiceOutputParameters));

                String refuelMode = forecourtResponse.getRefuelMode();

                // TODO rendere dinamico il controllo sul refuelMode

                if (!refuelMode.equals(Info.REFUEL_MODE_SELF)) {

                    // Erogatore non Self

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "refuelMode not Self: " + refuelMode);

                    psmResponse.setStatusCode(PumpStatusEnum.PUMP_NOT_AVAILABLE_501.toString());

                    psmResponse.setMessageCode(prop.getProperty(psmResponse.getStatusCode()));

                    String response = transactionService.persistPumpAvailabilityStatus(psmResponse.getStatusCode(), psmResponse.getMessageCode(), psmRequest.getTransactionID(),
                            requestID);

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "persistPumpAvailabilityStatus: " + response);
                }
                else {

                    psmResponse.setStatusCode(forecourtResponse.getStatusCode().toString());
                    psmResponse.setMessageCode(prop.getProperty(psmResponse.getStatusCode()));
                    psmResponse.setRefuelMode(refuelMode);

                    String response = transactionService.persistPumpAvailabilityStatus(psmResponse.getStatusCode(), psmResponse.getMessageCode(), psmRequest.getTransactionID(),
                            requestID);

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "persistPumpAvailabilityStatus: " + response);
                }

            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "Exception message: " + ex.getMessage());

                String statusCode = "SYSTEM_ERROR_500";
                String statusMessage = "System error";

                String resp = transactionService.persistPumpAvailabilityStatus(statusCode, statusMessage, psmRequest.getTransactionID(), requestID);

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "persistPumpAvailabilityStatus: " + resp);

                throw new BPELException("General exception");

            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", psmResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", psmResponse.getMessageCode()));
        outputParameters.add(new Pair<String, String>("refuelMode", psmResponse.getRefuelMode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPumpStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return psmResponse;

    }

    @WebMethod(operationName = "getStationDetails")
    public @XmlElement(required = true, name = "stationStatusMessageResponse")
    StationStatusMessageResponse getStationDetails(@WebParam(name = "stationStatusRequest") @XmlElement(required = true) StationStatusMessageRequest ssmRequest)
            throws BPELException {

        Date now = new Date();
        String requestID = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("pumpID", ssmRequest.getPumpID()));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("stationID", ssmRequest.getStationID()));
        inputParameters.add(new Pair<String, String>("pumpDetailsReq", ssmRequest.isPumpDetailsReq().toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getStationDetails", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        StationStatusMessageResponse ssmResponse = new StationStatusMessageResponse();

        Boolean errorFound = false;

        if ((ssmRequest.getPumpID() == null || ssmRequest.getPumpID().trim().isEmpty()) && (ssmRequest.getStationID() == null || ssmRequest.getStationID().trim().isEmpty())) {

            ssmResponse.setStatusCode(EnumSTATIONSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            ssmResponse.setMessageCode(prop.getProperty(EnumSTATIONSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (ssmRequest.isPumpDetailsReq() == null) {

            ssmResponse.setStatusCode(EnumSTATIONSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            ssmResponse.setMessageCode(prop.getProperty(EnumSTATIONSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (!errorFound) {

            try {

                // Log timestamp chiamata
                Date currentTimestamp = new Date();
                String currentTimestampString = String.valueOf(currentTimestamp.getTime());

                Set<Pair<String, String>> remoteServiceInputParameters = new HashSet<Pair<String, String>>();
                remoteServiceInputParameters.add(new Pair<String, String>("timestamp", currentTimestampString));

                this.log(ErrorLevel.DEBUG, "WsEndpointGatewayMobilePayment", "getStationDetails", requestID, "opening", ActivityLog.createLogMessage(remoteServiceInputParameters));

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                new Proxy().unsetHttp();

                GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());

                //Originale Forecourt
                IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
                //Chiamata Forecourt Emulator
                //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();

                ((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.connectionTimeout", this.gfgClientConnectionTimeout);

                GetStationDetailsMessageRequest forecourtRequest = new GetStationDetailsMessageRequest();
                com.techedge.mp.forecourt.integration.info.client.StationStatusMessageResponse forecourtResponse;

                forecourtRequest.setPumpID(ssmRequest.getPumpID());
                forecourtRequest.setRequestID(requestID);
                forecourtRequest.setStationID(ssmRequest.getStationID());
                forecourtRequest.setPumpDetailsReq(ssmRequest.isPumpDetailsReq());

                forecourtResponse = port1.getStationDetails(forecourtRequest);

                // Log timestamp chiamata
                currentTimestamp = new Date();
                currentTimestampString = String.valueOf(currentTimestamp.getTime());

                Set<Pair<String, String>> remoteServiceOutputParameters = new HashSet<Pair<String, String>>();
                remoteServiceOutputParameters.add(new Pair<String, String>("timestamp", currentTimestampString));

                this.log(ErrorLevel.DEBUG, "WsEndpointGatewayMobilePayment", "getStationDetails", requestID, "closing", ActivityLog.createLogMessage(remoteServiceOutputParameters));

                ssmResponse.setMessageCode(forecourtResponse.getMessageCode());

                if (forecourtResponse.getStationDetail() != null) {

                    StationDetail sd_local = new StationDetail();

                    sd_local.setAddress(forecourtResponse.getStationDetail().getAddress());
                    sd_local.setCity(forecourtResponse.getStationDetail().getCity());
                    sd_local.setCountry(forecourtResponse.getStationDetail().getCountry());
                    sd_local.setLatitude(forecourtResponse.getStationDetail().getLatitude());
                    sd_local.setLongitude(forecourtResponse.getStationDetail().getLongitude());
                    sd_local.setProvince(forecourtResponse.getStationDetail().getProvince());
                    sd_local.setStationID(forecourtResponse.getStationDetail().getStationID());

                    Date dob = null;
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:sss");

                    try {
                        dob = df.parse(forecourtResponse.getStationDetail().getValidityDateDetails());
                    }
                    catch (Exception ex) {

                        this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getStationDetails", requestID, null, "Error parsing validity date details: "
                                + forecourtResponse.getStationDetail().getValidityDateDetails());

                        dob = new Date();
                    }

                    GregorianCalendar cal = new GregorianCalendar();
                    cal.setTimeInMillis(dob.getTime());
                    XMLGregorianCalendar xmlDate2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);

                    /*
                    GregorianCalendar cal = new GregorianCalendar();
                    cal.setTime(dob);
                    XMLGregorianCalendar xmlDate2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), dob.getHours(),dob.getMinutes(),dob.getSeconds(),DatatypeConstants.FIELD_UNDEFINED, cal.getTimeZone().LONG).normalize();
                    */

                    sd_local.setValidityDateDetails(xmlDate2);

                    // Inserimento degli erogatori
                    List<PumpDetail> pumpList = forecourtResponse.getStationDetail().getPumpDetails();

                    for (PumpDetail pumpDetail : pumpList) {

                        com.techedge.mp.simulazioni.PumpDetail pumpDetailOut = new com.techedge.mp.simulazioni.PumpDetail();

                        pumpDetailOut.setPumpID(pumpDetail.getPumpID());
                        pumpDetailOut.setPumpNumber(pumpDetail.getPumpNumber());

                        if (pumpDetail.getRefuelMode() == null) {

                            pumpDetailOut.setPumpStatus("PUMP_AVAILABLE_200");
                            pumpDetailOut.setRefuelMode(null);
                        }
                        else {

                            if (!pumpDetail.getRefuelMode().equals(Info.REFUEL_MODE_SELF)) {

                                // Se l'erogatore ha refuelMode diverso da self si imposta lo stato a non disponibile
                                pumpDetailOut.setPumpStatus("PUMP_NOT_AVAILABLE_501");

                                this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getStationDetails", requestID, null,
                                        "refuelMode not valid: " + pumpDetail.getRefuelMode());
                            }
                            else {

                                if (pumpDetail.getPumpStatus() == PumpStatusEnum.PUMP_AVAILABLE_200)
                                    pumpDetailOut.setPumpStatus("PUMP_AVAILABLE_200");
                                if (pumpDetail.getPumpStatus() == PumpStatusEnum.PUMP_BUSY_401)
                                    pumpDetailOut.setPumpStatus("PUMP_BUSY_401");
                                if (pumpDetail.getPumpStatus() == PumpStatusEnum.PUMP_NOT_AVAILABLE_501)
                                    pumpDetailOut.setPumpStatus("PUMP_NOT_AVAILABLE_501");
                                if (pumpDetail.getPumpStatus() == PumpStatusEnum.PUMP_STATUS_NOT_AVAILABLE_500)
                                    pumpDetailOut.setPumpStatus("PUMP_STATUS_NOT_AVAILABLE_500");
                                if (pumpDetail.getPumpStatus() == PumpStatusEnum.PUMP_NOT_FOUND_404)
                                    pumpDetailOut.setPumpStatus("PUMP_NOT_FOUND_404");
                                if (pumpDetail.getPumpStatus() == PumpStatusEnum.STATION_NOT_FOUND_404)
                                    pumpDetailOut.setPumpStatus("STATION_NOT_FOUND_404");
                                if (pumpDetail.getPumpStatus() == PumpStatusEnum.STATION_PUMP_NOT_FOUND_404)
                                    pumpDetailOut.setPumpStatus("STATION_PUMP_NOT_FOUND_404");
                            }

                            pumpDetailOut.setRefuelMode(pumpDetail.getRefuelMode());
                        }

                        for (ProductDetail productDetail : pumpDetail.getProductDetails()) {

                            com.techedge.mp.simulazioni.ProductDetail productDetailOut = new com.techedge.mp.simulazioni.ProductDetail();
                            productDetailOut.setFuelType(productDetail.getFuelType());
                            productDetailOut.setProductDescription(productDetail.getProductDescription());

                            if (productDetail.getProductID() == ProductIdEnum.SP) {
                                productDetailOut.setProductID("SP");
                            }
                            if (productDetail.getProductID() == ProductIdEnum.GG) {
                                productDetailOut.setProductID("GG");
                            }
                            if (productDetail.getProductID() == ProductIdEnum.BS) {
                                productDetailOut.setProductID("BS");
                            }
                            if (productDetail.getProductID() == ProductIdEnum.BD) {
                                productDetailOut.setProductID("BD");
                            }
                            if (productDetail.getProductID() == ProductIdEnum.MT) {
                                productDetailOut.setProductID("MT");
                            }
                            if (productDetail.getProductID() == ProductIdEnum.GP) {
                                productDetailOut.setProductID("GP");
                            }
                            if (productDetail.getProductID() == ProductIdEnum.AD) {
                                productDetailOut.setProductID("AD");
                            }

                            productDetailOut.setProductPrice(productDetail.getProductPrice());

                            pumpDetailOut.getProductDetails().add(productDetailOut);
                        }

                        sd_local.getPumpDetails().add(pumpDetailOut);
                    }

                    ssmResponse.setStationDetail(sd_local);

                }
                else {

                    this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getStationDetails", requestID, null, "forecourtResponse.getStationDetail null");

                }

                if (forecourtResponse.getStatusCode() == StationStatusMessageResponseEnum.STATION_PUMP_FOUND_200) {
                    ssmResponse.setStatusCode("STATION_PUMP_FOUND_200");
                }
                if (forecourtResponse.getStatusCode() == StationStatusMessageResponseEnum.STATION_NOT_FOUND_404) {
                    ssmResponse.setStatusCode("STATION_NOT_FOUND_404");
                }
                if (forecourtResponse.getStatusCode() == StationStatusMessageResponseEnum.PUMP_NOT_FOUND_404) {
                    ssmResponse.setStatusCode("PUMP_NOT_FOUND_404");
                }
                if (forecourtResponse.getStatusCode() == StationStatusMessageResponseEnum.STATION_PUMP_NOT_FOUND_404) {
                    ssmResponse.setStatusCode("STATION_PUMP_NOT_FOUND_404");
                }
                if (forecourtResponse.getStatusCode() == StationStatusMessageResponseEnum.PUMP_STATUS_NOT_AVAILABLE_500) {
                    ssmResponse.setStatusCode("PUMP_STATUS_NOT_AVAILABLE_500");
                }

                ssmResponse.setMessageCode(prop.getProperty(ssmResponse.getStatusCode()));
            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getStationDetails", requestID, null, "Exception message: " + ex.getMessage());

                ssmResponse.setStatusCode("GENERIC_FAULT_500");
                ssmResponse.setMessageCode(ex.getMessage());

            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", ssmResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", ssmResponse.getMessageCode()));

        if (ssmResponse.getStationDetail() != null) {
            StationDetail stationDetail = ssmResponse.getStationDetail();

            String message = "{ stationID: " + stationDetail.getStationID() + ", address: " + stationDetail.getAddress() + ", city: " + stationDetail.getCity() + ", province: "
                    + stationDetail.getProvince() + " }";

            outputParameters.add(new Pair<String, String>("stationDetail", message));

            for (com.techedge.mp.simulazioni.PumpDetail pumpDetail : ssmResponse.getStationDetail().getPumpDetails()) {
                message = "{ pumpID: " + pumpDetail.getPumpID() + ", pumpNumber: " + pumpDetail.getPumpNumber() + ", pumpStatus: " + pumpDetail.getPumpStatus() + ", refuelMode: "
                        + pumpDetail.getRefuelMode() + " }";

                outputParameters.add(new Pair<String, String>("stationDetail.pumpDetail", message));
            }
        }

        /*
        outputParameters.add(new Pair<String, String>("stationDetail.stationID", ssmResponse.getStationDetail().getStationID()));
        outputParameters.add(new Pair<String, String>("stationDetail.address", ssmResponse.getStationDetail().getAddress()));
        outputParameters.add(new Pair<String, String>("stationDetail.city", ssmResponse.getStationDetail().getCity()));
        outputParameters.add(new Pair<String, String>("stationDetail.province", ssmResponse.getStationDetail().getProvince()));
                
        for (com.techedge.mp.simulazioni.PumpDetail pumpDetail : ssmResponse.getStationDetail().getPumpDetails()) {
            String message = "{ pumpID: " + pumpDetail.getPumpID() + ", pumpNumber: "
                    + pumpDetail.getPumpNumber() + ", pumpStatus: " + pumpDetail.getPumpStatus() + ", refuelMode: " + pumpDetail.getRefuelMode() + " }";
            
            outputParameters.add(new Pair<String, String>("stationDetail.pumpDetail", message));
        }
        */

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getStationDetails", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return ssmResponse;

    }

    @WebMethod(operationName = "enablePump")
    public @XmlElement(required = true, name = "enablePumpMessageResponse")
    EnablePumpMessageResponse enablePump(@WebParam(name = "enablePumpRequest") @XmlElement(required = true) EnablePumpMessageRequest epmRequest) throws BPELException {

        Date now = new Date();
        String requestID = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("pumpID", epmRequest.getPumpID()));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("stationID", epmRequest.getStationID()));
        inputParameters.add(new Pair<String, String>("transactionID", epmRequest.getTransactionID()));
        inputParameters.add(new Pair<String, String>("amount", epmRequest.getAmount().toString()));
        inputParameters.add(new Pair<String, String>("paymentMode", epmRequest.getPaymentMode()));
        inputParameters.add(new Pair<String, String>("productID", epmRequest.getProductID()));

        if (epmRequest.getPaymentAuthorizationResult() != null) {
            inputParameters.add(new Pair<String, String>("paymentAuthorizationResult", epmRequest.getPaymentAuthorizationResult().getTransactionResult()));
            inputParameters.add(new Pair<String, String>("paymentAuthorizationAuthorizationCode", epmRequest.getPaymentAuthorizationResult().getAuthorizationCode()));
            inputParameters.add(new Pair<String, String>("paymentAuthorizationBankTransactionID", epmRequest.getPaymentAuthorizationResult().getBankTransactionID()));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enablePump", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        EnablePumpMessageResponse epmResponse = new EnablePumpMessageResponse();

        Boolean errorFound = false;

        if (epmRequest.getTransactionID() == null || epmRequest.getTransactionID().trim().isEmpty()) {

            epmResponse.setStatusCode(EnumENABLEPUMPSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            epmResponse.setMessageCode(prop.getProperty(EnumENABLEPUMPSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (epmRequest.getPumpID() == null || epmRequest.getPumpID().trim().isEmpty()) {

            epmResponse.setStatusCode(EnumENABLEPUMPSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            epmResponse.setMessageCode(prop.getProperty(EnumENABLEPUMPSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (epmRequest.getPaymentMode() == null || epmRequest.getPaymentMode().trim().isEmpty()) {

            epmResponse.setStatusCode(EnumENABLEPUMPSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            epmResponse.setMessageCode(prop.getProperty(EnumENABLEPUMPSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (epmRequest.getPaymentMode() == null || epmRequest.getPaymentMode().trim().isEmpty()) {

            epmResponse.setStatusCode(EnumENABLEPUMPSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            epmResponse.setMessageCode(prop.getProperty(EnumENABLEPUMPSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (epmRequest.getAmount() == null) {

            epmResponse.setStatusCode(EnumENABLEPUMPSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            epmResponse.setMessageCode(prop.getProperty(EnumENABLEPUMPSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (!errorFound) {

            // TODO - Test
            /*
            String statusCode = "GENERIC_FAULT_500";
            String statusMessage = "Enable Pump Generic Fault";
            
            String resp = transactionService.persistPumpEnableStatus(statusCode, statusMessage, epmRequest.getTransactionID(), requestID);
            
            this.log( ErrorLevel.ERROR, this.getClass().getSimpleName(), "enablePump", requestID, null, "persistPumpEnableStatus: " + resp);
            
            String resp2 = transactionService.persistPumpGenericFaultStatus(epmRequest.getTransactionID(), requestID);
            
            this.log( ErrorLevel.ERROR, this.getClass().getSimpleName(), "enablePump", requestID, null, "persistPumpGenericFaultStatus: " + resp2);
            
            throw new BPELException("General exception");
            */

            String simulationEnablePumpError = null;
            boolean simulationEnablePumpKO = false;

            try {

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();
                
                //System.out.println("Sleep di 30 secondi");
                //Thread.sleep(30000);
                
                RetrieveTransactionEventsData events = transactionService.retrieveEventList(requestID, epmRequest.getTransactionID());
                boolean authFoundOK = false;
                boolean authFoundKO = false;
                boolean pumpIDOK = false;
                boolean invalidRequest = false;
                String checkTransactionID = null;
                String checkShopTransactionID = null;

                
                if (events.getAcquirerID() != null 
                        && (epmRequest.getPaymentAuthorizationResult() != null && epmRequest.getPaymentAuthorizationResult().getShopTransactionID() != null)) {
                    
                    checkShopTransactionID = epmRequest.getPaymentAuthorizationResult().getShopTransactionID();
                    
                    if (events.getAcquirerID().equals("CARTASI")) {
                        checkTransactionID = epmRequest.getTransactionID().substring(0, 30);
                        
                        if (!checkTransactionID.equals(checkShopTransactionID)) {
                            invalidRequest = true;
                        }
                    }
                    
                    if (events.getAcquirerID().equals("BANCASELLA")) {
                        checkTransactionID = epmRequest.getTransactionID();
                        
                        if (!checkTransactionID.equals(checkShopTransactionID)) {
                            invalidRequest = true;
                        }
                    }
                }
                System.out.println("checkTransactionID: " + (checkTransactionID != null ? checkTransactionID : ""));
                System.out.println("checkShopTransactionID: " + (checkShopTransactionID != null ? checkShopTransactionID : ""));
                
                if (invalidRequest) {
                    throw new Exception("Request not valid. TransactionID: " + checkTransactionID + ", ShopTransactionID: " + checkShopTransactionID);
                }
                
                System.out.println("epmRequest.getPumpID(): " + epmRequest.getPumpID());
                System.out.println("events.getPaymentRefuelDetailResponse().getSelectedPumpID(): " + events.getPaymentRefuelDetailResponse().getSelectedPumpID());

                if (events.getPaymentRefuelDetailResponse() != null && events.getPaymentRefuelDetailResponse().getSelectedPumpID() != null 
                        && events.getPaymentRefuelDetailResponse().getSelectedPumpID().equals(epmRequest.getPumpID())) {
                    pumpIDOK = true;
                }
                
                if (events.getTransactionEvents() != null && !events.getTransactionEvents().isEmpty()) {
                    for (EventInfo eventInfo : events.getTransactionEvents()) {
                        System.out.println("EventType: " + eventInfo.getEventType() + "  TransactionResult: " + eventInfo.getTransactionResult());
                        if (eventInfo.getEventType().equals("AUT")) {
                            if (eventInfo.getTransactionResult().equals("OK")) {
                                authFoundOK = true;
                            }

                            if (eventInfo.getTransactionResult().equals("KO")) {
                                authFoundKO = true;
                            }
                        }
                    }
                }
                else {
                    System.out.println("No records found in TransactionEvents");
                }
                
                if (!pumpIDOK) {
                    System.out.println("pumpIDOK: " + pumpIDOK);
                    throw new Exception("PumpID doesn't match. Database: " + events.getPaymentRefuelDetailResponse().getSelectedPumpID()
                            + ", BPEL: " + epmRequest.getPumpID());
                    
                }
                
                if (authFoundKO || !authFoundOK) {
                    System.out.println("authFoundOK: " + authFoundOK);
                    System.out.println("authFoundKO: " + authFoundKO);
                    throw new Exception("Authorization not found");
                }
                
                Transaction transaction = transactionService.getTransactionDetail(requestID, epmRequest.getTransactionID());
                
                //System.out.println("Transaction.productID: " + transaction.getProductID());
                
                //String productID = (transaction.getProductID() != null) ? transaction.getProductID() : "";
                
                new Proxy().unsetHttp();

                GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());

                //Originale Forecourt
                IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
                //Chiamata Forecourt Emulator
                //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();

                com.techedge.mp.forecourt.integration.info.client.EnablePumpMessageRequest requestForecourt = new com.techedge.mp.forecourt.integration.info.client.EnablePumpMessageRequest();

                requestForecourt.setAmount(epmRequest.getAmount());
                //requestForecourt.setPaymentAuthorizationResult(epmRequest.getPaymentAuthorizationResult());
                requestForecourt.setPaymentMode(epmRequest.getPaymentMode());

                
                //if (productID == null || productID.isEmpty()) {
                //    productID = epmRequest.getProductID();
                //}
                
                //System.out.println("epmRequest.productID: " + productID);
                
                // Abilitazione multiprodotto
                String productIDListString = null;
                for(TransactionAdditionalData transactionAdditionalData : transaction.getTransactionAdditionalDataList()) {
                    if (transactionAdditionalData.getDataKey().equals("productIDListString")) {
                        productIDListString = transactionAdditionalData.getDataValue();
                    }
                }
                
                if (productIDListString != null && !productIDListString.isEmpty()) {
                    String[] productIDList = productIDListString.split("\\|");
                    
                    for(String productID : productIDList) {
                        
                        if (productID.equals("SP")) {
                            requestForecourt.getProductIDList().add(ProductIdEnum.SP);
                        }
                        if (productID.equals("GG")) {
                            requestForecourt.getProductIDList().add(ProductIdEnum.GG);
                        }
                        if (productID.equals("BS")) {
                            requestForecourt.getProductIDList().add(ProductIdEnum.BS);
                        }
                        if (productID.equals("BD")) {
                            requestForecourt.getProductIDList().add(ProductIdEnum.BD);
                        }
                        if (productID.equals("MT")) {
                            requestForecourt.getProductIDList().add(ProductIdEnum.MT);
                        }
                        if (productID.equals("GP")) {
                            requestForecourt.getProductIDList().add(ProductIdEnum.GP);
                        }
                        if (productID.equals("AD")) {
                            requestForecourt.getProductIDList().add(ProductIdEnum.AD);
                        }
                    }
                }

                //requestForecourt.setProductID(ProductIdEnum.valueOf(epmRequest.getProductID()));
                requestForecourt.setPumpID(epmRequest.getPumpID());
                requestForecourt.setRequestID(requestID);
                requestForecourt.setStationID(epmRequest.getStationID());
                requestForecourt.setTransactionID(epmRequest.getTransactionID());

                PaymentAuthorizationResult paymentAuthorizationResult = new PaymentAuthorizationResult();
                paymentAuthorizationResult.setAcquirerId(epmRequest.getPaymentAuthorizationResult().getAcquirerID());
                paymentAuthorizationResult.setAuthorizationCode(epmRequest.getPaymentAuthorizationResult().getAuthorizationCode());
                paymentAuthorizationResult.setBankTransactionID(epmRequest.getPaymentAuthorizationResult().getBankTransactionID());
                paymentAuthorizationResult.setCurrency(epmRequest.getPaymentAuthorizationResult().getCurrency());
                paymentAuthorizationResult.setErrorCode(epmRequest.getPaymentAuthorizationResult().getErrorCode());
                paymentAuthorizationResult.setErrorDescription(epmRequest.getPaymentAuthorizationResult().getErrorDescription());
                paymentAuthorizationResult.setEventType(EventTypeEnum.AUT);
                paymentAuthorizationResult.setShopLogin(epmRequest.getPaymentAuthorizationResult().getShopLogin());
                paymentAuthorizationResult.setShopTransactionID(epmRequest.getPaymentAuthorizationResult().getShopTransactionID());

                if (epmRequest.getPaymentAuthorizationResult().getTransactionResult().equals("OK")) {
                    paymentAuthorizationResult.setTransactionResult(TransactionResultEnum.OK);
                }
                else {
                    paymentAuthorizationResult.setTransactionResult(TransactionResultEnum.KO);
                }

                requestForecourt.setPaymentAuthorizationResult(paymentAuthorizationResult);

                com.techedge.mp.forecourt.integration.info.client.EnablePumpMessageResponse forecourtResponse = new com.techedge.mp.forecourt.integration.info.client.EnablePumpMessageResponse();

                if (simulationActive) {

                    simulationEnablePumpError = parametersService.getParamValueNoCache(Info.PARAM_SIMULATION_GFG_ENABLEPUMP_ERROR);
                    simulationEnablePumpKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(Info.PARAM_SIMULATION_GFG_ENABLEPUMP_KO));

                    if (simulationEnablePumpError != null && simulationEnablePumpError.equalsIgnoreCase("before")) {
                        throw new Exception("Eccezione simulata di errore before");
                    }

                    if (simulationEnablePumpKO) {
                        epmResponse.setStatusCode("9999");
                        epmResponse.setMessageCode("Simulazione di KO");
                        return epmResponse;
                    }
                }
                
                if (requestForecourt.getProductIDList() != null && !requestForecourt.getProductIDList().isEmpty()) {
                    System.out.println("Lista prodotti abilitati");
                    for(ProductIdEnum product : requestForecourt.getProductIDList()) {
                        System.out.println("product: " + product.name());
                    }
                }
                
                ((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.connectionTimeout", this.gfgClientConnectionTimeout);

                forecourtResponse = port1.enablePump(requestForecourt);

                epmResponse.setStatusCode(forecourtResponse.getStatusCode().toString());
                epmResponse.setMessageCode(forecourtResponse.getMessageCode());

                String response = transactionService.persistPumpEnableStatus(epmResponse.getStatusCode(), epmResponse.getMessageCode(), epmRequest.getTransactionID(), requestID);

                this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "enablePump", requestID, null, "persistPumpEnableStatus: " + response);

                if (simulationActive) {
                    if (simulationEnablePumpError.equals("after")) {
                        throw new Exception("Eccezione simulata di errore after");
                    }
                }
            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "enablePump", requestID, null, "Exception message: " + ex.getMessage());

                String statusCode = "GENERIC_FAULT_500";
                String statusMessage = ex.getMessage();

                String resp = transactionService.persistPumpEnableStatus(statusCode, statusMessage, epmRequest.getTransactionID(), requestID);

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "enablePump", requestID, null, "persistPumpEnableStatus: " + resp);

                String resp2 = transactionService.persistPumpGenericFaultStatus(epmRequest.getTransactionID(), requestID);

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "enablePump", requestID, null, "persistPumpGenericFaultStatus: " + resp2);

                throw new BPELException("General exception");

            }

        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", epmResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", epmResponse.getMessageCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enablePump", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return epmResponse;

    }

    @WebMethod(operationName = "getTransactionStatus")
    public @XmlElement(required = true, name = "transactionStatusMessageResponse")
    TransactionStatusMessageResponse getTransactionStatus(@WebParam(name = "transactionStatusRequest") @XmlElement(required = true) TransactionStatusMessageRequest tsmRequest)
            throws BPELException {

        Date now = new Date();
        String requestID = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", tsmRequest.getRequestID()));
        inputParameters.add(new Pair<String, String>("transactionID", tsmRequest.getTransactionID()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", tsmRequest.getRequestID(), "opening", ActivityLog.createLogMessage(inputParameters));

        TransactionStatusMessageResponse tsmResponse = new TransactionStatusMessageResponse();

        Boolean errorFound = false;

        if (tsmRequest.getTransactionID() == null || tsmRequest.getTransactionID().trim().isEmpty()) {

            tsmResponse.setStatusCode(EnumTRANSACTIONSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            tsmResponse.setMessageCode(prop.getProperty(EnumTRANSACTIONSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (!errorFound) {

            try {
                String simulationGetTransactionStatusError = null;
                boolean simulationGetTransactionStatusKO = false;

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                new Proxy().unsetHttp();

                GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());

                //Originale Forecourt
                IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
                //Chiamata Forecourt Emulator
                //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();

                com.techedge.mp.forecourt.integration.info.client.TransactionStatusMessageRequest forecourtRequest = new com.techedge.mp.forecourt.integration.info.client.TransactionStatusMessageRequest();
                forecourtRequest.setRequestID(requestID);
                forecourtRequest.setTransactionID(tsmRequest.getTransactionID());

                com.techedge.mp.forecourt.integration.info.client.TransactionStatusMessageResponse forecourtResponse = new com.techedge.mp.forecourt.integration.info.client.TransactionStatusMessageResponse();

                if (simulationActive) {

                    simulationGetTransactionStatusError = parametersService.getParamValueNoCache(Info.PARAM_SIMULATION_GFG_GETTRANSACTIONSTATUS_ERROR);
                    simulationGetTransactionStatusKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(Info.PARAM_SIMULATION_GFG_GETTRANSACTIONSTATUS_KO));

                    if (simulationGetTransactionStatusError != null && simulationGetTransactionStatusError.equalsIgnoreCase("before")) {
                        throw new Exception("Eccezione simulata di errore before");
                    }

                    if (simulationGetTransactionStatusKO) {
                        tsmResponse.setStatusCode("9999");
                        tsmResponse.setMessageCode("Simulazione di KO");
                        return tsmResponse;
                    }
                }
                
                ((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.connectionTimeout", this.gfgClientConnectionTimeout);

                forecourtResponse = port1.getTransactionStatus(forecourtRequest);
                tsmResponse.setMessageCode(forecourtResponse.getMessageCode());

                if (forecourtResponse.getStatusCode() == GetTransactionStatusResponseEnum.REFUEL_INPROGRESS_401) {

                    tsmResponse.setStatusCode("REFUEL_INPROGRESS_401");

                    String transactionID = tsmRequest.getTransactionID();

                    String response = transactionService.persistStartRefuelReceivedStatus(requestID, transactionID, "getTransactionStatus");

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "persistStartRefuelReceivedStatus: " + response);

                }

                if (forecourtResponse.getStatusCode() == GetTransactionStatusResponseEnum.REFUEL_TERMINATED_200) {

                    tsmResponse.setStatusCode("REFUEL_TERMINATED_200");

                    String transactionID = tsmRequest.getTransactionID();
                    Double amount = forecourtResponse.getRefuelDetail().getAmount();
                    Double fuelQuantity = forecourtResponse.getRefuelDetail().getFuelQuantity();
                    String fuelType = forecourtResponse.getRefuelDetail().getFuelType();
                    String productDescription = forecourtResponse.getRefuelDetail().getProductDescription();
                    Double unitPrice = forecourtResponse.getRefuelDetail().getUnitPrice();

                    String productID = "";
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.SP) {
                        productID = "SP";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.GG) {
                        productID = "GG";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.BS) {
                        productID = "BS";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.BD) {
                        productID = "BD";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.MT) {
                        productID = "MT";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.GP) {
                        productID = "GP";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.AD) {
                        productID = "AD";
                    }

                    System.out.println("TimestampEndRefuel: " + forecourtResponse.getRefuelDetail().getTimestampEndRefuel());

                    String timestampEndRefuel = forecourtResponse.getRefuelDetail().getTimestampEndRefuel();

                    //String timestampEndRefuel = "";

                    String response = transactionService.persistEndRefuelReceivedStatus(requestID, transactionID, amount, fuelQuantity, fuelType, productDescription, productID,
                            timestampEndRefuel, unitPrice, "getTransactionStatus");

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "persistEndRefuelReceivedStatus: " + response);

                }

                if (forecourtResponse.getStatusCode() == GetTransactionStatusResponseEnum.REFUEL_NOT_STARTED_400) {

                    tsmResponse.setStatusCode("REFUEL_NOT_STARTED_400");
                }

                if (forecourtResponse.getStatusCode() == GetTransactionStatusResponseEnum.TRANSACTION_NOT_RECOGNIZED_400) {

                    tsmResponse.setStatusCode("TRANSACTION_NOT_RECOGNIZED_400");

                    String transactionID = tsmRequest.getTransactionID();

                    String response = transactionService.persistTransactionStatusRequestKoStatus(requestID, transactionID, "TRANSACTION_NOT_RECOGNIZED_400",
                            "Transaction not recognized");

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "persistTransactionStatusRequestKoStatus: " + response);

                }

                if (forecourtResponse.getStatusCode() == GetTransactionStatusResponseEnum.REFUEL_STATUS_NOT_AVAILABLE_500) {

                    tsmResponse.setStatusCode("REFUEL_STATUS_NOT_AVAILABLE_500");

                    String transactionID = tsmRequest.getTransactionID();

                    String response = transactionService.persistTransactionStatusRequestKoStatus(requestID, transactionID, "REFUEL_STATUS_NOT_AVAILABLE_500",
                            "Refuel status not available");

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "persistTransactionStatusRequestKoStatus: " + response);

                }

                this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "tsmResponse.statusCode: " + tsmResponse.getStatusCode());

                if (forecourtResponse.getRefuelDetail() != null) {
                    /*
                    String message = "forecourt response: { amount: "                  + forecourtResponse.getRefuelDetail().getAmount() +
                    									 ", fuel quantity: "           + forecourtResponse.getRefuelDetail().getFuelQuantity() +
                    									 ", fuel type: "               + forecourtResponse.getRefuelDetail().getFuelType() +
                    									 ", fuel description: "        + forecourtResponse.getRefuelDetail().getProductDescription() +
                    									 ", fuel timestampEndRefuel: " + forecourtResponse.getRefuelDetail().getTimestampEndRefuel() +
                    				 " }";
                    
                    this.log( ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, message);
                    */

                    RefuelDetail rd_local = new RefuelDetail();
                    rd_local.setAmount(forecourtResponse.getRefuelDetail().getAmount());
                    rd_local.setFuelQuantity(forecourtResponse.getRefuelDetail().getFuelQuantity());
                    rd_local.setFuelType(forecourtResponse.getRefuelDetail().getFuelType());
                    rd_local.setProductDescription(forecourtResponse.getRefuelDetail().getProductDescription());

                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.SP) {
                        rd_local.setProductID("SP");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.GG) {
                        rd_local.setProductID("GG");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.BS) {
                        rd_local.setProductID("BS");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.BD) {
                        rd_local.setProductID("BD");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.MT) {
                        rd_local.setProductID("MT");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.GP) {
                        rd_local.setProductID("GP");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.AD) {
                        rd_local.setProductID("AD");
                    }

                    try {

                        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

                        //Date parsedDate = dateFormat.parse(forecourtResponse.getRefuelDetail().getTimestampEndRefuel());

                        //rd_local.setTimestampEndRefuel(parsedDate.getTime());

                        rd_local.setTimestampEndRefuel(forecourtResponse.getRefuelDetail().getTimestampEndRefuel());

                    }
                    catch (Exception ex) {

                        this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "Error timestamp conversion: "
                                + forecourtResponse.getRefuelDetail().getTimestampEndRefuel());

                        rd_local.setTimestampEndRefuel((new Date()).toString());
                    }
                    
                    rd_local.setUnitPrice(forecourtResponse.getRefuelDetail().getUnitPrice());

                    tsmResponse.setRefuelDetail(rd_local);
                }
                else {

                    RefuelDetail rd_local = new RefuelDetail();

                    rd_local.setAmount(0.0);
                    rd_local.setFuelQuantity(0.0);
                    rd_local.setFuelType("");
                    rd_local.setProductDescription("");
                    rd_local.setTimestampEndRefuel("");
                    rd_local.setUnitPrice(0.0);

                    tsmResponse.setRefuelDetail(rd_local);
                }

                if (simulationActive) {
                    if (simulationGetTransactionStatusError != null && simulationGetTransactionStatusError.equalsIgnoreCase("after")) {
                        throw new Exception("Eccezione simulata di errore after");
                    }
                }
            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "Exception message: " + ex.getMessage());

                String transactionID = tsmRequest.getTransactionID();

                String response = transactionService.persistTransactionStatusRequestKoStatus(requestID, transactionID, "GENERIC_FAULT_500", ex.getMessage());

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "persistTransactionStatusRequestKoStatus: " + response);

                throw new BPELException("General exception");

            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", tsmResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", tsmResponse.getMessageCode()));

        if (tsmResponse.getRefuelDetail() != null) {
            RefuelDetail refuelDetail = tsmResponse.getRefuelDetail();
            String stringUnitPrice = "";
            if (refuelDetail.getUnitPrice() != null){
                stringUnitPrice = refuelDetail.getUnitPrice().toString();
            }
            String message = "{ amount: " + refuelDetail.getAmount() + ", fuelQuantity: " + refuelDetail.getFuelQuantity().toString() + ", fuelType: " + refuelDetail.getFuelType()
                    + ", unitPrice: " + stringUnitPrice + ", productID: " + refuelDetail.getProductID() + ", productDescription: " + refuelDetail.getProductDescription() + ", timestampEndRefuel: "
                    + refuelDetail.getTimestampEndRefuel() + " }";

            outputParameters.add(new Pair<String, String>("refuelDetail", message));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return tsmResponse;

    }

    @WebMethod(operationName = "transactionReconciliation")
    public @XmlElement(required = true, name = "transactionReconciliationMessageResponse")
    TransactionReconciliationMessageResponse transactionReconciliation(
            @WebParam(name = "transactionReconciliationRequest") @XmlElement(required = true) TransactionReconciliationMessageRequest trmRquest) throws BPELException {

        TransactionReconciliationMessageResponse trmResponse = new TransactionReconciliationMessageResponse();

        if (trmRquest.getRequestID() == null || trmRquest.getRequestID().trim().isEmpty()) {

//			trmResponse.setStatusCode(EnumTRANSACTIONSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
//			trmResponse.setMessageCode(prop.getProperty(EnumPUMPSTAUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            return trmResponse;

        }

        if (trmRquest.getTransactionIDList() == null) {

//			trmResponse.setStatusCode(EnumTRANSACTIONSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
//			trmResponse.setMessageCode(prop.getProperty(EnumPUMPSTAUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            return trmResponse;

        }

        System.out.println("***********************");
        System.out.println("Create Web Service Client...");
        GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());
        System.out.println("Create Web Service...");

        //Originale Forecourt
        IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
        //Chiamata Forecourt Emulator
        //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();
        System.out.println("Call Web Service Operation...");

        try {

            this.transactionService = EJBHomeCache.getInstance().getTransactionService();

            new Proxy().unsetHttp();

            //Date now = new Date();
            //String requestID = String.valueOf(now.getTime());

//			com.techedge.mp.forecourt.integration.info.client.TransactionReconciliationMessageRequest forecourtRequest = new com.techedge.mp.forecourt.integration.info.client.TransactionReconciliationMessageRequest();
//			
//			forecourtRequest.setRequestID(trmRquest.getRequestID());
//			forecourtRequest.setTransactionIDList(trmRquest.getTransactionIDList());
//			
            com.techedge.mp.forecourt.integration.info.client.TransactionReconciliationMessageResponse forecourtResponse = new com.techedge.mp.forecourt.integration.info.client.TransactionReconciliationMessageResponse();

            forecourtResponse = port1.transactionReconciliation(trmRquest);
            trmResponse = forecourtResponse;
            //trmResponse = port1.transactionReconciliation(trmRquest);

            //trmResponse = InfoEmulator.transactionReconciliation(trmRquest);
            //trmResponse.setMessageCode(prop.getProperty(trmResponse.getStatusCode()));

        }
        catch (javax.xml.ws.soap.SOAPFaultException ex) {

            throw new BPELException("SOAP fault exception");

        }
        catch (javax.xml.ws.ProtocolException ex) {

            throw new BPELException("Protocol exception");

        }
        catch (javax.xml.ws.WebServiceException ex) {

            throw new BPELException("Web service exception");

        }
        catch (java.lang.SecurityException ex) {

            throw new BPELException("Security exception");

        }
        catch (Exception ex) {

            throw new BPELException("General exception");

        }

//		trmResponse.setStatusCode(EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.MESSAGE_RECEIVED_200.name());
//		trmResponse.setMessageCode(prop.getProperty(EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.MESSAGE_RECEIVED_200.name()));
        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        for (TransactionDetails transactionDetail : trmResponse.getTransactionDetails()) {
            com.techedge.mp.forecourt.integration.info.client.RefuelDetail refuelDetail = transactionDetail.getRefuelDetail();

            outputParameters.add(new Pair<String, String>("statusCode", transactionDetail.getStatusCode().value()));
            outputParameters.add(new Pair<String, String>("messageCode", transactionDetail.getMessageCode()));
            outputParameters.add(new Pair<String, String>("transactionID", transactionDetail.getTransactionID()));
            outputParameters.add(new Pair<String, String>("lastTransactionStatus", transactionDetail.getLastTransactionStatus().value()));

            String stringUnitPrice = "";
            if (refuelDetail.getUnitPrice() != null) {
                stringUnitPrice = refuelDetail.getUnitPrice().toString();
            }
            String message = "{ amount: " + refuelDetail.getAmount() + ", fuelQuantity: " + refuelDetail.getFuelQuantity().toString() + ", fuelType: " + refuelDetail.getFuelType()
                    + ", unitPrice: " + stringUnitPrice + ", productID: " + refuelDetail.getProductID() + ", productDescription: " + refuelDetail.getProductDescription() + ", timestampEndRefuel: "
                    + refuelDetail.getTimestampEndRefuel() + " }";

            outputParameters.add(new Pair<String, String>("refuelDetail", message));

            for (TransactionStatusHistory transactionStatus : transactionDetail.getTransactionStatusHistory()) {
                message = "{ statusCode: " + transactionStatus.getStatusCode() + ", messageCode: " + transactionStatus.getMessageCode() + ", timestamp: "
                        + transactionStatus.getTimestamp() + " }";

                outputParameters.add(new Pair<String, String>("transactionStatus", message));
            }
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "transactionReconciliation", trmRquest.getRequestID(), "closing",
                ActivityLog.createLogMessage(outputParameters));

        return trmResponse;

    }

    @WebMethod(operationName = "sendPaymentTransactionResult")
    public @XmlElement(required = true, name = "sendPaymentTransactionResultResponse")
    SendPaymentTransactionResultMessageResponse sendPaymentTransactionResult(
            @WebParam(name = "sendPaymentTransactionResultRequest") @XmlElement(required = true) SendPaymentTransactionResultMessageRequest sptrRequest) throws BPELException {

        Date now = new Date();
        String requestID = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionID", sptrRequest.getTransactionID()));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("amount", sptrRequest.getAmount().toString()));
        inputParameters.add(new Pair<String, String>("bankAuthorizationCode", sptrRequest.getBankTransactionResults().getAuthorizationCode()));
        inputParameters.add(new Pair<String, String>("bankTransactionResult", sptrRequest.getBankTransactionResults().getTransactionResult()));
        inputParameters.add(new Pair<String, String>("bankTransactionType", sptrRequest.getBankTransactionResults().getTransactionType()));
        inputParameters.add(new Pair<String, String>("bankShopTransactionID", sptrRequest.getBankTransactionResults().getShopTransactionID()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        SendPaymentTransactionResultMessageResponse sptrmResponse = new SendPaymentTransactionResultMessageResponse();
        String electronicInvoiceID = null;
        

        Boolean errorFound = false;

        if (sptrRequest.getTransactionID() == null || sptrRequest.getTransactionID().isEmpty()) {

            sptrmResponse.setStatusCode(EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            sptrmResponse.setMessageCode(prop.getProperty(EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (sptrRequest.getAmount() == null) {

            sptrmResponse.setStatusCode(EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            sptrmResponse.setMessageCode(prop.getProperty(EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (sptrRequest.getBankTransactionResults() == null) {

            sptrmResponse.setStatusCode(EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            sptrmResponse.setMessageCode(prop.getProperty(EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (!errorFound) {

            try {

                StringBuffer debug = new StringBuffer("#### DEBUG GFG SEND-PAYMENT-TRANSACTION ####\n");
                
                String simulationSendPaymentTransactionResultError = null;
                boolean simulationSendPaymentTransactionResultKO = false;
                boolean invalidRequest = false;
                
                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                Transaction transactionResponse = transactionService.getTransactionDetail(requestID, sptrRequest.getTransactionID());

                String checkTransactionID = null;
                String checkShopTransactionID = null;

                if (transactionResponse.getAcquirerID() != null 
                        && (sptrRequest.getBankTransactionResults() != null && sptrRequest.getBankTransactionResults().getShopTransactionID() != null)) {
                    
                    checkShopTransactionID = sptrRequest.getBankTransactionResults().getShopTransactionID();
                    
                    if (transactionResponse.getAcquirerID().equals("CARTASI")) {
                        checkTransactionID = sptrRequest.getTransactionID().substring(0, 30);
                        
                        if (!checkTransactionID.equals(checkShopTransactionID)) {
                            invalidRequest = true;
                        }
                    }
                    
                    if (transactionResponse.getAcquirerID().equals("BANCASELLA")) {
                        checkTransactionID = sptrRequest.getTransactionID();
                        
                        if (!checkTransactionID.equals(checkShopTransactionID)) {
                            invalidRequest = true;
                        }
                    }
                }

                System.out.println("checkTransactionID: " + (checkTransactionID != null ? checkTransactionID : ""));
                System.out.println("checkShopTransactionID: " + (checkShopTransactionID != null ? checkShopTransactionID : ""));
            
                
                if (invalidRequest) {
                    
                    String transactionType   = "MOV";
                    String transactionResult = "OK";
                    int sequenceId = 0;
                    String notifyAmount = "0";
                    
                    if (transactionResponse.getFinalAmount() != null) {
                        notifyAmount = transactionResponse.getFinalAmount().toString();
                    }
                    
                    for (TransactionEvent transactionEvent : transactionResponse.getTransactionEventData()) {
                        if (transactionEvent.getSequenceID().intValue() > sequenceId) {
                            transactionType   = transactionEvent.getEventType();
                            transactionResult = transactionEvent.getTransactionResult();
                        }
                        sequenceId++;
                    }
                    
                    System.out.println("Amount request:                " + sptrRequest.getAmount());
                    System.out.println("Amount read:                   " + notifyAmount);
                    
                    String requestTransactionType = "";
                    if (transactionResponse.getNewPaymentFlow()) {
                        requestTransactionType = EventTypeEnum.CAN.name();
                    }
                    else {
                        if (transactionResponse.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {
                            requestTransactionType = EventTypeEnum.CAN.name();
                        }
                        else {
                            if (sptrRequest.getBankTransactionResults().getTransactionType().equals("PAGAM")) {
                                requestTransactionType = EventTypeEnum.AUT.name();
                            }
                            if (sptrRequest.getBankTransactionResults().getTransactionType().equals("SETTLE")) {
                                requestTransactionType = EventTypeEnum.MOV.name();
                            }
                            if (sptrRequest.getBankTransactionResults().getTransactionType().equals("DELETE")) {
                                requestTransactionType = EventTypeEnum.CAN.name();
                            }
                        }
                    }
                    
                    System.out.println("BankTransactionType request:   " + requestTransactionType);
                    System.out.println("BankTransactionType read:      " + transactionType);
                    
                    System.out.println("BankTransactionResult request: " + sptrRequest.getBankTransactionResults().getTransactionResult());
                    System.out.println("BankTransactionResult read:    " + transactionResult);
                    
                    throw new Exception("Request not valid. TransactionID: " + checkTransactionID + ", ShopTransactionID: " + checkShopTransactionID);
                    /*
                    Double notifyAmountDouble = Double.parseDouble(notifyAmount);
                    sptrRequest.setAmount(notifyAmountDouble);
                    
                    if (transactionType.equals("AUT")) {
                        sptrRequest.getBankTransactionResults().setTransactionType("PAGAM");
                    }
                    if (transactionType.equals("MOV")) {
                        sptrRequest.getBankTransactionResults().setTransactionType("SETTLE");
                    }
                    if (transactionType.equals("CAN")) {
                        sptrRequest.getBankTransactionResults().setTransactionType("DELETE");
                    }
                    
                    sptrRequest.getBankTransactionResults().setTransactionResult(transactionResult);
                    
                    System.out.println("Request not valid. TransactionID: " + checkTransactionID + ", ShopTransactionID: " + checkShopTransactionID);
                    */
                }
                
                
                new Proxy().unsetHttp();

                GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());

                //Originale Forecourt
                IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
                //Chiamata Forecourt Emulator
                //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();

                com.techedge.mp.forecourt.integration.info.client.SendPaymentTransactionResultMessageResponse forecourtResponse = new com.techedge.mp.forecourt.integration.info.client.SendPaymentTransactionResultMessageResponse();
                com.techedge.mp.forecourt.integration.info.client.SendPaymentTransactionResultMessageRequest forecourtRequest = new com.techedge.mp.forecourt.integration.info.client.SendPaymentTransactionResultMessageRequest();

                Double initialAmount = transactionResponse.getInitialAmount();
                Double amount = sptrRequest.getAmount();

                System.out.println("Amount iniziale: " + initialAmount + ", Amound finale: " + amount);

                if (amount > initialAmount) {

                    System.out.println("Errore elettrovalvola: final amount maggiore dell'initial amount");

                    amount = initialAmount;

                    System.out.println("Initial amount: " + initialAmount + ", final amount: " + amount);
                }

                forecourtRequest.setAmount(amount);
                forecourtRequest.setRequestID(requestID);
                forecourtRequest.setTransactionID(sptrRequest.getTransactionID());

                debug.append("RequestID: " + forecourtRequest.getRequestID() + "\n");
                debug.append("TransactionID: " + forecourtRequest.getTransactionID() + "\n");
                debug.append("Amount: " + forecourtRequest.getAmount() + "\n");

                PaymentTransactionResult ptr_local = new PaymentTransactionResult();
                ptr_local.setShopTransactionID(sptrRequest.getBankTransactionResults().getShopTransactionID());
                ptr_local.setAuthorizationCode(sptrRequest.getBankTransactionResults().getAuthorizationCode());
                ptr_local.setBankTransactionID(sptrRequest.getBankTransactionResults().getBankTransactionID());
                ptr_local.setErrorCode(sptrRequest.getBankTransactionResults().getErrorCode());
                ptr_local.setErrorDescription(sptrRequest.getBankTransactionResults().getErrorDescription());

                if (transactionResponse.getNewPaymentFlow()) {
                    ptr_local.setEventType(EventTypeEnum.CAN);
                }
                else {
                    if (transactionResponse.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {
                        ptr_local.setEventType(EventTypeEnum.CAN);
                    }
                    else {
                        if (sptrRequest.getBankTransactionResults().getTransactionType().equals("PAGAM")) {
                            ptr_local.setEventType(EventTypeEnum.AUT);
                        }
                        if (sptrRequest.getBankTransactionResults().getTransactionType().equals("SETTLE")) {
                            ptr_local.setEventType(EventTypeEnum.MOV);
                        }
                        if (sptrRequest.getBankTransactionResults().getTransactionType().equals("DELETE")) {
                            ptr_local.setEventType(EventTypeEnum.CAN);
                        }
                    }
                }

                if (sptrRequest.getBankTransactionResults().getTransactionResult().equals("OK")) {
                    ptr_local.setTransactionResult(TransactionResultEnum.OK);
                }
                else {
                    ptr_local.setTransactionResult(TransactionResultEnum.KO);
                }

                forecourtRequest.setPaymentTransactionResult(ptr_local);

                debug.append("PaymentTransactionResult: \n");
                debug.append("   ShopTransactionID:" + ptr_local.getShopTransactionID() + "\n");
                debug.append("   AuthorizationCode:" + ptr_local.getAuthorizationCode() + "\n");
                debug.append("   BankTransactionID:" + ptr_local.getBankTransactionID() + "\n");
                debug.append("   ErrorCode:" + ptr_local.getErrorCode() + "\n");
                debug.append("   ErrorDescription:" + ptr_local.getErrorDescription() + "\n");
                debug.append("   EventType:" + ptr_local.getEventType() + "\n");
                debug.append("   TransactionResult:" + ptr_local.getTransactionResult() + "\n");

                // Valorizzazione voucher
                if (transactionResponse != null) {

                    if (transactionResponse.getPrePaidConsumeVoucherList().size() > 0) {

                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "Consume voucher header found");

                        debug.append("Voucher: \n");

                        for (PrePaidConsumeVoucher prePaidVoucherConsume : transactionResponse.getPrePaidConsumeVoucherList()) {

                            if (prePaidVoucherConsume.getOperationType().equals("CONSUME") && prePaidVoucherConsume.getPrePaidConsumeVoucherDetail().size() > 0) {

                                this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "Consume voucher found");

                                for (PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail : prePaidVoucherConsume.getPrePaidConsumeVoucherDetail()) {

                                    Voucher voucher = new Voucher();
                                    voucher.setPromoCode(prePaidConsumeVoucherDetail.getPromoCode());
                                    voucher.setPromoDescription(prePaidConsumeVoucherDetail.getPromoDescription());
                                    voucher.setVoucherAmount(prePaidConsumeVoucherDetail.getConsumedValue());
                                    voucher.setVoucherCode(prePaidConsumeVoucherDetail.getVoucherCode());

                                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null,
                                            "Voucher: code: " + voucher.getVoucherCode() + ", amount: " + voucher.getVoucherAmount());

                                    forecourtRequest.getVoucher().add(voucher);

                                    debug.append("   Code:" + voucher.getVoucherCode() + "   Amount:" + voucher.getVoucherAmount() + "\n");
                                }
                            }
                        }
                    }
                    
                    if (transactionResponse.getBusiness()) {
                        
                        PersonalDataBusiness personalDataBusiness = transactionResponse.getUser().getPersonalDataBusinessList().get(0);
                        PersonalData personalData = transactionResponse.getUser().getPersonalData();
                        
                        if (personalDataBusiness == null) {
                            throw new Exception("No business data for user: " + personalData.getSecurityDataEmail());
                        }
                        
                        com.techedge.mp.forecourt.integration.info.client.ElectronicInvoice forecourtElectronicInvoice = new com.techedge.mp.forecourt.integration.info.client.ElectronicInvoice();
                        forecourtElectronicInvoice.setAddress(personalDataBusiness.getAddress());
                        forecourtElectronicInvoice.setBusinessName(personalDataBusiness.getBusinessName());
                        forecourtElectronicInvoice.setCity(personalDataBusiness.getCity());
                        forecourtElectronicInvoice.setCountry(personalDataBusiness.getCountry());
                        forecourtElectronicInvoice.setEmailAddress(personalData.getSecurityDataEmail());
                        
                        // Ricerca la targa preferita
                        String licensePlate = personalDataBusiness.getLicensePlate();
                        for(PlateNumber plateNumber : transactionResponse.getUser().getPlateNumberList()) {
                            
                            if (plateNumber.getDefaultPlateNumber()) {
                                
                                licensePlate = plateNumber.getPlateNumber();
                                break;
                            }
                        }
                        
                        forecourtElectronicInvoice.setLicensePlate(licensePlate);
                        forecourtElectronicInvoice.setName(personalDataBusiness.getFirstName());
                        forecourtElectronicInvoice.setPecEmailAddress(personalDataBusiness.getPecEmail());
                        forecourtElectronicInvoice.setProvince(personalDataBusiness.getProvince());
                        forecourtElectronicInvoice.setSdiCode(personalDataBusiness.getSdiCode());
                        forecourtElectronicInvoice.setStreetNumber(personalDataBusiness.getStreetNumber());
                        forecourtElectronicInvoice.setSurname(personalDataBusiness.getLastName());
                        forecourtElectronicInvoice.setVatNumber(personalDataBusiness.getVatNumber());
                        forecourtElectronicInvoice.setZipCode(personalDataBusiness.getZipCode());
                        forecourtElectronicInvoice.setFiscalCode(personalDataBusiness.getFiscalCode());
                        
                        forecourtRequest.setElectronicInvoice(forecourtElectronicInvoice);
                        
                        System.out.println("Aggiunta alla richiesta SOAP i dati della fattura elettronica");
                        System.out.println("      address: " + forecourtElectronicInvoice.getAddress());
                        System.out.println("      businnesName: " + forecourtElectronicInvoice.getBusinessName());
                        System.out.println("      city: " + forecourtElectronicInvoice.getCity());
                        System.out.println("      country: " + forecourtElectronicInvoice.getCountry());
                        System.out.println("      emailAddress: " + forecourtElectronicInvoice.getEmailAddress());
                        System.out.println("      fiscalCode: " + forecourtElectronicInvoice.getFiscalCode());
                        System.out.println("      licensePlate: " + forecourtElectronicInvoice.getLicensePlate());
                        System.out.println("      name: " + forecourtElectronicInvoice.getName());
                        System.out.println("      pecEmailAddress: " + forecourtElectronicInvoice.getPecEmailAddress());
                        System.out.println("      province: " + forecourtElectronicInvoice.getProvince());
                        System.out.println("      sdiCode: " + forecourtElectronicInvoice.getSdiCode());
                        System.out.println("      streetNumber: " + forecourtElectronicInvoice.getStreetNumber());
                        System.out.println("      surname: " + forecourtElectronicInvoice.getSurname());
                        System.out.println("      vatNumber: " + forecourtElectronicInvoice.getVatNumber());
                        System.out.println("      zipCode: " + forecourtElectronicInvoice.getZipCode());
                    }
                    
                }

                System.out.println(debug.toString());
                
                if (simulationActive) {

                    simulationSendPaymentTransactionResultError = parametersService.getParamValueNoCache(Info.PARAM_SIMULATION_SENDPAYMENTTRANSACTIONRESULT_ERROR);
                    simulationSendPaymentTransactionResultKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(Info.PARAM_SIMULATION_SENDPAYMENTTRANSACTIONRESULT_KO));

                    if (simulationSendPaymentTransactionResultError != null && simulationSendPaymentTransactionResultError.equalsIgnoreCase("before")) {
                        throw new Exception("Eccezione simulata di errore before");
                    }

                    if (simulationSendPaymentTransactionResultKO) {
                        sptrmResponse.setStatusCode("SOURCE_STATUS_NOT_AVAILABLE_500");
                        sptrmResponse.setMessageCode("Simulazione di KO");

                        return sptrmResponse;
                    }
                }
                
                ((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.connectionTimeout", this.gfgClientConnectionTimeout);

                forecourtResponse = port1.sendPaymentTransactionResult(forecourtRequest);
                
                if (forecourtResponse.getElectronicInvoiceID() != null && !forecourtResponse.getElectronicInvoiceID().trim().isEmpty()) {
                    electronicInvoiceID = forecourtResponse.getElectronicInvoiceID();
                }

                sptrmResponse.setMessageCode(forecourtResponse.getMessageCode());
                sptrmResponse.setStatusCode(forecourtResponse.getStatusCode().toString());
                sptrmResponse.setElectronicInvoiceID(electronicInvoiceID);

                String response = transactionService.setGFGNotification(sptrRequest.getTransactionID(), true, electronicInvoiceID);
                
                if (forecourtResponse.getStatusCode().equals(SendPaymentTransactionResultResponseEnum.MESSAGE_RECEIVED_200)) {
                    if (electronicInvoiceID != null) {
                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "setGFGNotification and setGFGElectronicInvoiceID response: " + response);
                    }
                    else {
                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "setGFGNotification response: " + response);
                    }
                }
                else {

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "setGFGNotification response: KO");
                }
            }
            catch (Exception ex) {

                ex.printStackTrace();
                transactionService.setGFGNotification(sptrRequest.getTransactionID(), false, electronicInvoiceID);

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "Exception message: " + ex.getMessage());

                throw new BPELException("General exception");
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", sptrmResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", sptrmResponse.getMessageCode()));
        outputParameters.add(new Pair<String, String>("electronicInvoiceID", sptrmResponse.getElectronicInvoiceID()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return sptrmResponse;

    }

    private void loadFileProperty() {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("messageStatus.properties");
        try {
            prop.load(inputStream);
            if (inputStream == null) {
                throw new FileNotFoundException("property file response_service.properties not found in the classpath");
            }
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
