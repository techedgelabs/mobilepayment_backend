package com.techedge.mp.forecourt.integration.info.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pumpStatusMessageRequest", propOrder = {
	"requestID",
	"stationID",
	"pumpID",
	"transactionID"
})
public class PumpStatusMessageRequest {

	@XmlElement(required=true)
	protected String requestID;
	@XmlElement(required=true)
	protected String stationID;
	@XmlElement(required=true)
	protected String pumpID;
	@XmlElement(required=true)
	protected String transactionID;
	
	
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	
	public String getStationID() {
		return stationID;
	}
	public void setStationID(String stationID) {
		this.stationID = stationID;
	}
	
	public String getPumpID() {
		return pumpID;
	}
	public void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	
	
}
