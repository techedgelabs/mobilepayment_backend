
package com.techedge.mp.forecourt.integration.info.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per pumpDetail complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="pumpDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pumpID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pumpNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pumpStatus" type="{http://gatewaymobilepayment.4ts.it/}pumpStatusEnum"/>
 *         &lt;element name="refuelMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productDetails" type="{http://gatewaymobilepayment.4ts.it/}productDetail" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pumpDetail", propOrder = {
    "pumpID",
    "pumpNumber",
    "pumpStatus",
    "refuelMode",
    "productDetails"
})
public class PumpDetail {

    @XmlElement(required = true, nillable = true)
    protected String pumpID;
    @XmlElement(required = true, nillable = true)
    protected String pumpNumber;
    @XmlElement(required = true)
    protected PumpStatusEnum pumpStatus;
    @XmlElement(required = true, nillable = true)
    protected String refuelMode;
    protected List<ProductDetail> productDetails;

    /**
     * Recupera il valore della proprietÓ pumpID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpID() {
        return pumpID;
    }

    /**
     * Imposta il valore della proprietÓ pumpID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpID(String value) {
        this.pumpID = value;
    }

    /**
     * Recupera il valore della proprietÓ pumpNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpNumber() {
        return pumpNumber;
    }

    /**
     * Imposta il valore della proprietÓ pumpNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpNumber(String value) {
        this.pumpNumber = value;
    }

    /**
     * Recupera il valore della proprietÓ pumpStatus.
     * 
     * @return
     *     possible object is
     *     {@link PumpStatusEnum }
     *     
     */
    public PumpStatusEnum getPumpStatus() {
        return pumpStatus;
    }

    /**
     * Imposta il valore della proprietÓ pumpStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link PumpStatusEnum }
     *     
     */
    public void setPumpStatus(PumpStatusEnum value) {
        this.pumpStatus = value;
    }

    /**
     * Recupera il valore della proprietÓ refuelMode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefuelMode() {
        return refuelMode;
    }

    /**
     * Imposta il valore della proprietÓ refuelMode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefuelMode(String value) {
        this.refuelMode = value;
    }

    /**
     * Gets the value of the productDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductDetail }
     * 
     * 
     */
    public List<ProductDetail> getProductDetails() {
        if (productDetails == null) {
            productDetails = new ArrayList<ProductDetail>();
        }
        return this.productDetails;
    }

}
