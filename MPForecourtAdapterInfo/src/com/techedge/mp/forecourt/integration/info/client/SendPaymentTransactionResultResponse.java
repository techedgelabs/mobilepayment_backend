
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sendPaymentTransactionResultMessageResponse" type="{http://gatewaymobilepayment.4ts.it/}sendPaymentTransactionResultMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendPaymentTransactionResultMessageResponse"
})
@XmlRootElement(name = "sendPaymentTransactionResultResponse")
public class SendPaymentTransactionResultResponse {

    @XmlElement(required = true, nillable = true)
    protected SendPaymentTransactionResultMessageResponse sendPaymentTransactionResultMessageResponse;

    /**
     * Recupera il valore della proprietÓ sendPaymentTransactionResultMessageResponse.
     * 
     * @return
     *     possible object is
     *     {@link SendPaymentTransactionResultMessageResponse }
     *     
     */
    public SendPaymentTransactionResultMessageResponse getSendPaymentTransactionResultMessageResponse() {
        return sendPaymentTransactionResultMessageResponse;
    }

    /**
     * Imposta il valore della proprietÓ sendPaymentTransactionResultMessageResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link SendPaymentTransactionResultMessageResponse }
     *     
     */
    public void setSendPaymentTransactionResultMessageResponse(SendPaymentTransactionResultMessageResponse value) {
        this.sendPaymentTransactionResultMessageResponse = value;
    }

}
