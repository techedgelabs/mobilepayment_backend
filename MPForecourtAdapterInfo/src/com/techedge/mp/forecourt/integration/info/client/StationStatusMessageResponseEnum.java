
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per stationStatusMessageResponseEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="stationStatusMessageResponseEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="STATION_PUMP_FOUND_200"/>
 *     &lt;enumeration value="STATION_NOT_FOUND_404"/>
 *     &lt;enumeration value="PUMP_NOT_FOUND_404"/>
 *     &lt;enumeration value="STATION_PUMP_NOT_FOUND_404"/>
 *     &lt;enumeration value="PUMP_STATUS_NOT_AVAILABLE_500"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "stationStatusMessageResponseEnum")
@XmlEnum
public enum StationStatusMessageResponseEnum {

    STATION_PUMP_FOUND_200,
    STATION_NOT_FOUND_404,
    PUMP_NOT_FOUND_404,
    STATION_PUMP_NOT_FOUND_404,
    PUMP_STATUS_NOT_AVAILABLE_500;

    public String value() {
        return name();
    }

    public static StationStatusMessageResponseEnum fromValue(String v) {
        return valueOf(v);
    }

}
