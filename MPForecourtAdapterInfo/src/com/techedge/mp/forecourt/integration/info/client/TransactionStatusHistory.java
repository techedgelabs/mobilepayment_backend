
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per transactionStatusHistory complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="transactionStatusHistory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusCode" type="{http://gatewaymobilepayment.4ts.it/}transactionStatusEnum"/>
 *         &lt;element name="messageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionStatusHistory", propOrder = {
    "timestamp",
    "statusCode",
    "messageCode"
})
public class TransactionStatusHistory {

    @XmlElement(required = true, nillable = true)
    protected String timestamp;
    @XmlElement(required = true)
    protected TransactionStatusEnum statusCode;
    @XmlElement(required = true, nillable = true)
    protected String messageCode;

    /**
     * Recupera il valore della proprietÓ timestamp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Imposta il valore della proprietÓ timestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimestamp(String value) {
        this.timestamp = value;
    }

    /**
     * Recupera il valore della proprietÓ statusCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionStatusEnum }
     *     
     */
    public TransactionStatusEnum getStatusCode() {
        return statusCode;
    }

    /**
     * Imposta il valore della proprietÓ statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionStatusEnum }
     *     
     */
    public void setStatusCode(TransactionStatusEnum value) {
        this.statusCode = value;
    }

    /**
     * Recupera il valore della proprietÓ messageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Imposta il valore della proprietÓ messageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

}
