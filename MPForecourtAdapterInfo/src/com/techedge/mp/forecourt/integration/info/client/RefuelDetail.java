
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per refuelDetail complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="refuelDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="timestampEndRefuel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="fuelType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fuelQuantity" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="productID" type="{http://gatewaymobilepayment.4ts.it/}productIdEnum" minOccurs="0"/>
 *         &lt;element name="productDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unitPrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "refuelDetail", propOrder = {
    "timestampEndRefuel",
    "amount",
    "fuelType",
    "fuelQuantity",
    "productID",
    "productDescription",
    "unitPrice"
})
public class RefuelDetail {

    @XmlElement(required = true, nillable = true)
    protected String timestampEndRefuel;
    protected double amount;
    protected String fuelType;
    protected Double fuelQuantity;
    protected ProductIdEnum productID;
    protected String productDescription;
    protected Double unitPrice;

    /**
     * Recupera il valore della proprietÓ timestampEndRefuel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimestampEndRefuel() {
        return timestampEndRefuel;
    }

    /**
     * Imposta il valore della proprietÓ timestampEndRefuel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimestampEndRefuel(String value) {
        this.timestampEndRefuel = value;
    }

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(double value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ fuelType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelType() {
        return fuelType;
    }

    /**
     * Imposta il valore della proprietÓ fuelType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelType(String value) {
        this.fuelType = value;
    }

    /**
     * Recupera il valore della proprietÓ fuelQuantity.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    /**
     * Imposta il valore della proprietÓ fuelQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setFuelQuantity(Double value) {
        this.fuelQuantity = value;
    }

    /**
     * Recupera il valore della proprietÓ productID.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdEnum }
     *     
     */
    public ProductIdEnum getProductID() {
        return productID;
    }

    /**
     * Imposta il valore della proprietÓ productID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdEnum }
     *     
     */
    public void setProductID(ProductIdEnum value) {
        this.productID = value;
    }

    /**
     * Recupera il valore della proprietÓ productDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * Imposta il valore della proprietÓ productDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductDescription(String value) {
        this.productDescription = value;
    }

    /**
     * Recupera il valore della proprietÓ unitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getUnitPrice() {
        return unitPrice;
    }

    /**
     * Imposta il valore della proprietÓ unitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

}
