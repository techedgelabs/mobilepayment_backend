package com.techedge.mp.forecourt.integration.info.client.clientsample;

import com.techedge.mp.forecourt.integration.info.client.*;

public class ClientSample {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl();
	        System.out.println("Create Web Service...");
	        IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port1.getTransactionStatus(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.sendPaymentTransactionResult(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.transactionReconciliation(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.enablePump(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.getPumpStatus(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.getStationDetails(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Create Web Service...");
	        IGatewayMobilePayment port2 = service1.getWsEndpointGatewayMobilePayment();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port2.getTransactionStatus(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.sendPaymentTransactionResult(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.transactionReconciliation(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.enablePump(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.getPumpStatus(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.getStationDetails(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
