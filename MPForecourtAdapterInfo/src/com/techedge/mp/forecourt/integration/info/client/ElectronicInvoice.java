package com.techedge.mp.forecourt.integration.info.client;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "electronicInvoice", propOrder = {
        "name",
        "surname",
        "businessName",
        "address",
        "province",
        "city",
        "zipCode",
        "streetNumber",
        "country",
        "fiscalCode",
        "vatNumber",
        "emailAddress",
        "pecEmailAddress",
        "sdiCode",
        "licensePlate"

})
public class ElectronicInvoice {

    @XmlElement(name = "Name")
    private String name;
    
    @XmlElement(name = "Surname")
    private String surname;
    
    @XmlElement(name = "BusinessName")
    private String businessName;
    
    @XmlElement(name = "Address", required = true, nillable = true)
    private String address;
    
    @XmlElement(name = "Province", required = true, nillable = true)
    private String province;
    
    @XmlElement(name = "City", required = true, nillable = true)
    private String city;
    
    @XmlElement(name = "ZipCode", required = true, nillable = true)
    private String zipCode;
    
    @XmlElement(name = "StreetNumber")
    private String streetNumber;
    
    @XmlElement(name = "Country")
    private String country;
    
    @XmlElement(name = "FiscalCode")
    private String fiscalCode;
    
    @XmlElement(name = "VatNumber")
    private String vatNumber;
    
    @XmlElement(name = "EmailAddress")
    private String emailAddress;
    
    @XmlElement(name = "PecEmailAddress")
    private String pecEmailAddress;
    
    @XmlElement(name = "SDICode")
    private String sdiCode;
    
    @XmlElement(name = "LicensePlate")
    private String licensePlate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPecEmailAddress() {
        return pecEmailAddress;
    }

    public void setPecEmailAddress(String pecEmailAddress) {
        this.pecEmailAddress = pecEmailAddress;
    }

    public String getSdiCode() {
        return sdiCode;
    }

    public void setSdiCode(String sdiCode) {
        this.sdiCode = sdiCode;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }


    
}
