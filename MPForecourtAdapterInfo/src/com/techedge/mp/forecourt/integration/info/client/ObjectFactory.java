
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.techedge.mp.forecourt.integration.info.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.techedge.mp.forecourt.integration.info.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTransactionStatus }
     * 
     */
    public GetTransactionStatus createGetTransactionStatus() {
        return new GetTransactionStatus();
    }

    /**
     * Create an instance of {@link TransactionStatusMessageRequest }
     * 
     */
    public TransactionStatusMessageRequest createTransactionStatusMessageRequest() {
        return new TransactionStatusMessageRequest();
    }

    /**
     * Create an instance of {@link GetPumpStatusResponse }
     * 
     */
    public GetPumpStatusResponse createGetPumpStatusResponse() {
        return new GetPumpStatusResponse();
    }

    /**
     * Create an instance of {@link GetPumpStatusMessageResponse }
     * 
     */
    public GetPumpStatusMessageResponse createGetPumpStatusMessageResponse() {
        return new GetPumpStatusMessageResponse();
    }

    /**
     * Create an instance of {@link GetStationDetailsResponse }
     * 
     */
    public GetStationDetailsResponse createGetStationDetailsResponse() {
        return new GetStationDetailsResponse();
    }

    /**
     * Create an instance of {@link StationStatusMessageResponse }
     * 
     */
    public StationStatusMessageResponse createStationStatusMessageResponse() {
        return new StationStatusMessageResponse();
    }

    /**
     * Create an instance of {@link EnablePumpResponse }
     * 
     */
    public EnablePumpResponse createEnablePumpResponse() {
        return new EnablePumpResponse();
    }

    /**
     * Create an instance of {@link EnablePumpMessageResponse }
     * 
     */
    public EnablePumpMessageResponse createEnablePumpMessageResponse() {
        return new EnablePumpMessageResponse();
    }

    /**
     * Create an instance of {@link SendPaymentTransactionResult }
     * 
     */
    public SendPaymentTransactionResult createSendPaymentTransactionResult() {
        return new SendPaymentTransactionResult();
    }

    /**
     * Create an instance of {@link SendPaymentTransactionResultMessageRequest }
     * 
     */
    public SendPaymentTransactionResultMessageRequest createSendPaymentTransactionResultMessageRequest() {
        return new SendPaymentTransactionResultMessageRequest();
    }

    /**
     * Create an instance of {@link TransactionReconciliation }
     * 
     */
    public TransactionReconciliation createTransactionReconciliation() {
        return new TransactionReconciliation();
    }

    /**
     * Create an instance of {@link TransactionReconciliationMessageRequest }
     * 
     */
    public TransactionReconciliationMessageRequest createTransactionReconciliationMessageRequest() {
        return new TransactionReconciliationMessageRequest();
    }

    /**
     * Create an instance of {@link SendPaymentTransactionResultResponse }
     * 
     */
    public SendPaymentTransactionResultResponse createSendPaymentTransactionResultResponse() {
        return new SendPaymentTransactionResultResponse();
    }

    /**
     * Create an instance of {@link SendPaymentTransactionResultMessageResponse }
     * 
     */
    public SendPaymentTransactionResultMessageResponse createSendPaymentTransactionResultMessageResponse() {
        return new SendPaymentTransactionResultMessageResponse();
    }

    /**
     * Create an instance of {@link TransactionReconciliationResponse }
     * 
     */
    public TransactionReconciliationResponse createTransactionReconciliationResponse() {
        return new TransactionReconciliationResponse();
    }

    /**
     * Create an instance of {@link TransactionReconciliationMessageResponse }
     * 
     */
    public TransactionReconciliationMessageResponse createTransactionReconciliationMessageResponse() {
        return new TransactionReconciliationMessageResponse();
    }

    /**
     * Create an instance of {@link EnablePump }
     * 
     */
    public EnablePump createEnablePump() {
        return new EnablePump();
    }

    /**
     * Create an instance of {@link EnablePumpMessageRequest }
     * 
     */
    public EnablePumpMessageRequest createEnablePumpMessageRequest() {
        return new EnablePumpMessageRequest();
    }

    /**
     * Create an instance of {@link GetPumpStatus }
     * 
     */
    public GetPumpStatus createGetPumpStatus() {
        return new GetPumpStatus();
    }

    /**
     * Create an instance of {@link GetPumpStatusMessageRequest }
     * 
     */
    public GetPumpStatusMessageRequest createGetPumpStatusMessageRequest() {
        return new GetPumpStatusMessageRequest();
    }

    /**
     * Create an instance of {@link GetStationDetails }
     * 
     */
    public GetStationDetails createGetStationDetails() {
        return new GetStationDetails();
    }

    /**
     * Create an instance of {@link GetStationDetailsMessageRequest }
     * 
     */
    public GetStationDetailsMessageRequest createGetStationDetailsMessageRequest() {
        return new GetStationDetailsMessageRequest();
    }

    /**
     * Create an instance of {@link GetTransactionStatusResponse }
     * 
     */
    public GetTransactionStatusResponse createGetTransactionStatusResponse() {
        return new GetTransactionStatusResponse();
    }

    /**
     * Create an instance of {@link TransactionStatusMessageResponse }
     * 
     */
    public TransactionStatusMessageResponse createTransactionStatusMessageResponse() {
        return new TransactionStatusMessageResponse();
    }

    /**
     * Create an instance of {@link RefuelDetail }
     * 
     */
    public RefuelDetail createRefuelDetail() {
        return new RefuelDetail();
    }

    /**
     * Create an instance of {@link StationDetail }
     * 
     */
    public StationDetail createStationDetail() {
        return new StationDetail();
    }

    /**
     * Create an instance of {@link TransactionStatusHistory }
     * 
     */
    public TransactionStatusHistory createTransactionStatusHistory() {
        return new TransactionStatusHistory();
    }

    /**
     * Create an instance of {@link PaymentTransactionResult }
     * 
     */
    public PaymentTransactionResult createPaymentTransactionResult() {
        return new PaymentTransactionResult();
    }

    /**
     * Create an instance of {@link PaymentAuthorizationResult }
     * 
     */
    public PaymentAuthorizationResult createPaymentAuthorizationResult() {
        return new PaymentAuthorizationResult();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link TransactionDetails }
     * 
     */
    public TransactionDetails createTransactionDetails() {
        return new TransactionDetails();
    }

    /**
     * Create an instance of {@link ProductDetail }
     * 
     */
    public ProductDetail createProductDetail() {
        return new ProductDetail();
    }

    /**
     * Create an instance of {@link PumpDetail }
     * 
     */
    public PumpDetail createPumpDetail() {
        return new PumpDetail();
    }

}
