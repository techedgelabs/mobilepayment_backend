
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per pumpStatusEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="pumpStatusEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PUMP_AVAILABLE_200"/>
 *     &lt;enumeration value="PUMP_BUSY_401"/>
 *     &lt;enumeration value="PUMP_NOT_AVAILABLE_501"/>
 *     &lt;enumeration value="PUMP_STATUS_NOT_AVAILABLE_500"/>
 *     &lt;enumeration value="PUMP_NOT_FOUND_404"/>
 *     &lt;enumeration value="STATION_NOT_FOUND_404"/>
 *     &lt;enumeration value="STATION_PUMP_NOT_FOUND_404"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "pumpStatusEnum")
@XmlEnum
public enum PumpStatusEnum {

    PUMP_AVAILABLE_200,
    PUMP_BUSY_401,
    PUMP_NOT_AVAILABLE_501,
    PUMP_STATUS_NOT_AVAILABLE_500,
    PUMP_NOT_FOUND_404,
    STATION_NOT_FOUND_404,
    STATION_PUMP_NOT_FOUND_404;

    public String value() {
        return name();
    }

    public static PumpStatusEnum fromValue(String v) {
        return valueOf(v);
    }

}
