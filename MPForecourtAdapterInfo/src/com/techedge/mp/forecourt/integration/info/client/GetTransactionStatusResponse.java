
package com.techedge.mp.forecourt.integration.info.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionStatusResponse" type="{http://gatewaymobilepayment.4ts.it/}transactionStatusMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionStatusResponse"
})
@XmlRootElement(name = "getTransactionStatusResponse")
public class GetTransactionStatusResponse {

    @XmlElement(required = true, nillable = true)
    protected TransactionStatusMessageResponse transactionStatusResponse;

    /**
     * Recupera il valore della proprietÓ transactionStatusResponse.
     * 
     * @return
     *     possible object is
     *     {@link TransactionStatusMessageResponse }
     *     
     */
    public TransactionStatusMessageResponse getTransactionStatusResponse() {
        return transactionStatusResponse;
    }

    /**
     * Imposta il valore della proprietÓ transactionStatusResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionStatusMessageResponse }
     *     
     */
    public void setTransactionStatusResponse(TransactionStatusMessageResponse value) {
        this.transactionStatusResponse = value;
    }

}
