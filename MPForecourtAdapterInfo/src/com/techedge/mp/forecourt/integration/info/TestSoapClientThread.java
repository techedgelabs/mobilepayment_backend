package com.techedge.mp.forecourt.integration.info;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class TestSoapClientThread implements Runnable {

    private String transactionID;

    public TestSoapClientThread(String transactionID) {

        this.transactionID = transactionID;
    }

    @Override
    public void run() {

        try {

            /**************************************/
            System.out.println("/*********************/");
            System.out.println("/*                   */");
            System.out.println("/* Evento end refuel */");
            System.out.println("/*                   */");
            System.out.println("/*********************/");
            String url = "http://10.231.241.15:8080/MPForecourtAdapterRefuel/Refuel";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            //HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            
            // add request header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)");
            con.setRequestProperty("Content-type", "application/text/xml;charset=UTF-8");

            String soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ref=\"http://refuel.integration.forecourt.mp.techedge.com/\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>"
                    + "<ref:endRefuel>"
                    + "<endRefuelRequest>"
                    + "<requestID>12345678</requestID>"
                    + "<transactionID>"
                    + this.transactionID
                    + "</transactionID>"
                    + "<refuelDetail>"
                    + "<timestampEndRefuel>2017-04-18 17:46:25</timestampEndRefuel>"
                    + "<amount>0</amount>"
                    + "<fuelQuantity>0</fuelQuantity>" + "</refuelDetail>" + "</endRefuelRequest>" + "</ref:endRefuel>" + "</soapenv:Body>" + "</soapenv:Envelope>";
            // Send post request
            con.setDoOutput(true);
            con.setDoInput(true);

            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.writeBytes(soapRequest);
            out.flush();
            out.close();
            
            // print result
            System.out.println("Response Code : " + con.getResponseCode());
            System.out.println("Response Message : " + con.getResponseMessage());
            
            String response = "";
            
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer buf = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    buf.append(inputLine);
                }

                in.close();

                response = buf.toString();

                System.out.println("Response Content : " + response);
            }
            
            con.disconnect();    

            System.out.println("/*********************/");
            System.out.println("/*                   */");
            System.out.println("/* Evento end refuel */");
            System.out.println("/*                   */");
            System.out.println("/*********************/");

            /***************************************/

        }
        catch (Exception ex) {

            ex.printStackTrace();

        }

    }

}
