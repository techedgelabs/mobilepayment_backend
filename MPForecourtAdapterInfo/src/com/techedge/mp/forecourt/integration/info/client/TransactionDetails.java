
package com.techedge.mp.forecourt.integration.info.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per transactionDetails complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="transactionDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="statusCode" type="{http://gatewaymobilepayment.4ts.it/}transactionDetailsStatusEnum"/>
 *         &lt;element name="messageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastTransactionStatus" type="{http://gatewaymobilepayment.4ts.it/}transactionStatusEnum" minOccurs="0"/>
 *         &lt;element name="transactionStatusHistory" type="{http://gatewaymobilepayment.4ts.it/}transactionStatusHistory" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="refuelDetail" type="{http://gatewaymobilepayment.4ts.it/}refuelDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionDetails", propOrder = {
    "statusCode",
    "messageCode",
    "transactionID",
    "timestamp",
    "lastTransactionStatus",
    "transactionStatusHistory",
    "refuelDetail"
})
public class TransactionDetails {

    @XmlElement(required = true)
    protected TransactionDetailsStatusEnum statusCode;
    @XmlElement(required = true, nillable = true)
    protected String messageCode;
    @XmlElement(required = true, nillable = true)
    protected String transactionID;
    protected String timestamp;
    protected TransactionStatusEnum lastTransactionStatus;
    protected List<TransactionStatusHistory> transactionStatusHistory;
    protected RefuelDetail refuelDetail;

    /**
     * Recupera il valore della proprietÓ statusCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDetailsStatusEnum }
     *     
     */
    public TransactionDetailsStatusEnum getStatusCode() {
        return statusCode;
    }

    /**
     * Imposta il valore della proprietÓ statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDetailsStatusEnum }
     *     
     */
    public void setStatusCode(TransactionDetailsStatusEnum value) {
        this.statusCode = value;
    }

    /**
     * Recupera il valore della proprietÓ messageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Imposta il valore della proprietÓ messageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    /**
     * Recupera il valore della proprietÓ transactionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Imposta il valore della proprietÓ transactionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Recupera il valore della proprietÓ timestamp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Imposta il valore della proprietÓ timestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimestamp(String value) {
        this.timestamp = value;
    }

    /**
     * Recupera il valore della proprietÓ lastTransactionStatus.
     * 
     * @return
     *     possible object is
     *     {@link TransactionStatusEnum }
     *     
     */
    public TransactionStatusEnum getLastTransactionStatus() {
        return lastTransactionStatus;
    }

    /**
     * Imposta il valore della proprietÓ lastTransactionStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionStatusEnum }
     *     
     */
    public void setLastTransactionStatus(TransactionStatusEnum value) {
        this.lastTransactionStatus = value;
    }

    /**
     * Gets the value of the transactionStatusHistory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionStatusHistory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionStatusHistory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionStatusHistory }
     * 
     * 
     */
    public List<TransactionStatusHistory> getTransactionStatusHistory() {
        if (transactionStatusHistory == null) {
            transactionStatusHistory = new ArrayList<TransactionStatusHistory>();
        }
        return this.transactionStatusHistory;
    }

    /**
     * Recupera il valore della proprietÓ refuelDetail.
     * 
     * @return
     *     possible object is
     *     {@link RefuelDetail }
     *     
     */
    public RefuelDetail getRefuelDetail() {
        return refuelDetail;
    }

    /**
     * Imposta il valore della proprietÓ refuelDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link RefuelDetail }
     *     
     */
    public void setRefuelDetail(RefuelDetail value) {
        this.refuelDetail = value;
    }

}
