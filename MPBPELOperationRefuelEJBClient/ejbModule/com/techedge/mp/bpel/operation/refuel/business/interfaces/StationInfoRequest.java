package com.techedge.mp.bpel.operation.refuel.business.interfaces;

import java.io.Serializable;

public class StationInfoRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4754574303516261530L;

	private String pumpID;
	private String requestID;
	private String stationID;
	private Boolean pumpDetailsReq;
	
	public String getPumpID() {
		return pumpID;
	}
	public void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}
	
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	
	public String getStationID() {
		return stationID;
	}
	public void setStationID(String stationID) {
		this.stationID = stationID;
	}
	
	public Boolean getPumpDetailsReq() {
		return pumpDetailsReq;
	}
	public void setPumpDetailsReq(Boolean pumpDetailsReq) {
		this.pumpDetailsReq = pumpDetailsReq;
	}
}
