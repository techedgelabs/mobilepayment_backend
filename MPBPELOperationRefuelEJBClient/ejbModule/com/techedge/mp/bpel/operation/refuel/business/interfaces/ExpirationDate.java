package com.techedge.mp.bpel.operation.refuel.business.interfaces;

import java.io.Serializable;

public class ExpirationDate implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7338840185242688105L;

    private Integer           year;
    private Integer           month;
    private Integer           day;
    private Integer           hour;
    private Integer           minute;
    private Integer           second;
    private String            timezone;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getSecond() {
        return second;
    }

    public void setSecond(Integer second) {
        this.second = second;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

}
