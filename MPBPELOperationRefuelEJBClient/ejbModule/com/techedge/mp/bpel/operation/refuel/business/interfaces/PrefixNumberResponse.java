package com.techedge.mp.bpel.operation.refuel.business.interfaces;

import java.io.Serializable;
import java.util.List;

public class PrefixNumberResponse implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -419839147090847061L;

    private String            statusCode;

    private List<String>      listPrefix;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<String> getListPrefix() {
        return listPrefix;
    }

    public void setListPrefix(List<String> listPrefix) {
        this.listPrefix = listPrefix;
    }

}
