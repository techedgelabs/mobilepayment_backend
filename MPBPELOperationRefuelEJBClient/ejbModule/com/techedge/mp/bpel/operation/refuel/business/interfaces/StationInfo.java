package com.techedge.mp.bpel.operation.refuel.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

public class StationInfo implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -22990364613040955L;
	
	private String stationID;
    private String address;
    private String city;
    private String name;
    private String country;
    private String latitude;
    private String longitude;
    private String province;
    private XMLGregorianCalendar validityDateDetails;
    private List<PumpInfo> pumpList = new ArrayList<PumpInfo>(0);
    private List<RegisterInfo> registerList = new ArrayList<RegisterInfo>(0);
    
	public String getStationID() {
		return stationID;
	}
	public void setStationID(String stationID) {
		this.stationID = stationID;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	
	public XMLGregorianCalendar getValidityDateDetails() {
		return validityDateDetails;
	}
	public void setValidityDateDetails(XMLGregorianCalendar validityDateDetails) {
		this.validityDateDetails = validityDateDetails;
	}
	
	public List<PumpInfo> getPumpList() {
		return pumpList;
	}
	public void setPumpList(List<PumpInfo> pumpList) {
		this.pumpList = pumpList;
	}
	public List<RegisterInfo> getRegisterList() {
		return registerList;
	}
	public void setRegisterList(List<RegisterInfo> registerList) {
		this.registerList = registerList;
	}
}
