package com.techedge.mp.bpel.operation.refuel.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PumpInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3237371879827603530L;
	
	private String pumpID;
	private String pumpNumber;
	private String pumpStatus;
	private String refuelMode;
	private List<ProductInfo> productList = new ArrayList<ProductInfo>(0);
	
	public String getPumpID() {
		return pumpID;
	}
	public void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}
	
	public String getPumpNumber() {
		return pumpNumber;
	}
	public void setPumpNumber(String pumpNumber) {
		this.pumpNumber = pumpNumber;
	}
	
	public String getPumpStatus() {
		return pumpStatus;
	}
	public void setPumpStatus(String pumpStatus) {
		this.pumpStatus = pumpStatus;
	}
	
	public String getRefuelMode() {
		return refuelMode;
	}
	public void setRefuelMode(String refuelMode) {
		this.refuelMode = refuelMode;
	}
	
	public List<ProductInfo> getProductList() {
		return productList;
	}
	public void setProductList(List<ProductInfo> productList) {
		this.productList = productList;
	}
}
