package com.techedge.mp.bpel.operation.refuel.business.interfaces;

import java.io.Serializable;

public class PumpStatusResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5745949550831315887L;
	
	private String statusCode;
    private String refuelMode;
    
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getRefuelMode() {
		return refuelMode;
	}
	public void setRefuelMode(String refuelMode) {
		this.refuelMode = refuelMode;
	}

}
