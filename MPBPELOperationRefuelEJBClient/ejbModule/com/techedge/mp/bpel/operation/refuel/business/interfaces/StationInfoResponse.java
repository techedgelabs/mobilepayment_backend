package com.techedge.mp.bpel.operation.refuel.business.interfaces;

import java.io.Serializable;

public class StationInfoResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8027761106908486902L;
	
	private String statusCode;
	private String statusMessage;
    private StationInfo stationInfo;
    
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	public StationInfo getStationInfo() {
		return stationInfo;
	}
	public void setStationInfo(StationInfo stationInfo) {
		this.stationInfo = stationInfo;
	}

}
