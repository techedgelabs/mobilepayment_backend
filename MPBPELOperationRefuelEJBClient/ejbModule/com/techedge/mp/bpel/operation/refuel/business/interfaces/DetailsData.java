package com.techedge.mp.bpel.operation.refuel.business.interfaces;

import java.io.Serializable;

public class DetailsData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7156384011545425633L;
    private String            label;
    private String            value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
