package com.techedge.mp.bpel.operation.refuel.business;

import javax.ejb.Remote;

@Remote
public interface BPELServiceRemote {

    public String startRefuelingProcess(String transactionID, String stationID, String pumpID, Double amount, String acquirerID, String shopLogin, String uicCode,
            String shopTransactionID, String tokenValue, String fuelType, String productDescription, String productID);
}
