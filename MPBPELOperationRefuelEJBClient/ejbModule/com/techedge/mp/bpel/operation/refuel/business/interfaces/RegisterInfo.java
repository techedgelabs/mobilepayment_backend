package com.techedge.mp.bpel.operation.refuel.business.interfaces;

import java.io.Serializable;

public class RegisterInfo implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -5626577965062182955L;
	
	private String registerID;
	private String registerNumber;
	private String registerStatus;
	public String getRegisterID() {
		return registerID;
	}
	public void setRegisterID(String registerID) {
		this.registerID = registerID;
	}
	public String getRegisterNumber() {
		return registerNumber;
	}
	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}
	public String getRegisterStatus() {
		return registerStatus;
	}
	public void setRegisterStatus(String registerStatus) {
		this.registerStatus = registerStatus;
	}
	
	
	
}
