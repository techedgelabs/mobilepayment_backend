package com.techedge.mp.forecourt.integration.shop.interfaces;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RefuelDetail", propOrder = {
		"fuelType",
		"fuelQuantity",
		"amount",
		"productId",
		"productDescription",
		"pumpID",
		"pumpNumber",
		"refuelMode",
		"unitPrice"
})

public class ShopRefuelDetail implements Serializable {
	//	TODO

	/**
	 * 
	 */
	private static final long serialVersionUID = 2772029049510769345L;
	@XmlElement(required = false, nillable = true)
	private String fuelType;
	private Double fuelQuantity;
	private Double amount;
	private String productId;
	private String productDescription;
	private String pumpID;
	private String pumpNumber;
	private String refuelMode;
	private Double unitPrice;
	
	
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	public Double getFuelQuantity() {
		return fuelQuantity;
	}
	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double fuelAmount) {
		this.amount = fuelAmount;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getPumpID() {
		return pumpID;
	}
	public void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}
	public String getPumpNumber() {
		return pumpNumber;
	}
	public void setPumpNumber(String pumpNumber) {
		this.pumpNumber = pumpNumber;
	}
	public String getRefuelMode() {
		return refuelMode;
	}
	public void setRefuelMode(String refuelMode) {
		this.refuelMode = refuelMode;
	}
    public Double getUnitPrice() {
        return unitPrice;
    }
    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }
	
}
