
package com.techedge.mp.forecourt.integration.shop.interfaces;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per productIdEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="productIdEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SP"/>
 *     &lt;enumeration value="GG"/>
 *     &lt;enumeration value="BS"/>
 *     &lt;enumeration value="BD"/>
 *     &lt;enumeration value="MT"/>
 *     &lt;enumeration value="GP"/>
 *     &lt;enumeration value="AD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "productIdEnum")
@XmlEnum
public enum ProductIdEnum {

    SP,
    GG,
    BS,
    BD,
    MT,
    GP,
    AD;

    public String value() {
        return name();
    }

    public static ProductIdEnum fromValue(String v) {
        return valueOf(v);
    }

}
