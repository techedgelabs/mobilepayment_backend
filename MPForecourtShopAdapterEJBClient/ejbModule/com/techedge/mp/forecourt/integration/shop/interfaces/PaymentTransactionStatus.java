package com.techedge.mp.forecourt.integration.shop.interfaces;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentTransactionStatus", propOrder = {
		"shopLogin",
		"acquirerID",
		"paymentMode",
		"shopTransactionID",
		"bankTransactionID",
		"authorizationCode",
		"currency",
		"eventList"
})

public class PaymentTransactionStatus {
	
	//TODO
	
	@XmlElement(required = true, nillable = true)
	private String shopLogin;
	@XmlElement(required = true, nillable = true)
	private String acquirerID;
	@XmlElement(required = true, nillable = true)
	private String paymentMode;
	@XmlElement(required = true, nillable = true)
	private String shopTransactionID;
	@XmlElement(required = true, nillable = true)
	private String bankTransactionID;
	@XmlElement(required = true, nillable = true)
	private String authorizationCode;
	//private String mpTransactionStatus;
	@XmlElement(required = false, nillable = true)
	private String currency;
	@XmlElement(required = true, nillable = true)
	private Set<PaymentTransactionEvent> eventList = new HashSet<PaymentTransactionEvent>(0);
	
	
	public String getShopLogin() {
		return shopLogin;
	}
	public void setShopLogin(String shopLogin) {
		this.shopLogin = shopLogin;
	}
	public String getAcquirerID() {
		return acquirerID;
	}
	public void setAcquirerID(String acquirerID) {
		this.acquirerID = acquirerID;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getShopTransactionID() {
		return shopTransactionID;
	}
	public void setShopTransactionID(String shopTransactionID) {
		this.shopTransactionID = shopTransactionID;
	}
	public String getBankTransactionID() {
		return bankTransactionID;
	}
	public void setBankTransactionID(String bankTransactionID) {
		this.bankTransactionID = bankTransactionID;
	}
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}
//	public String getMpTransactionStatus() {
//		return mpTransactionStatus;
//	}
//	public void setMpTransactionStatus(String mpTransactionStatus) {
//		this.mpTransactionStatus = mpTransactionStatus;
//	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Set<PaymentTransactionEvent> getEventList() {
		return eventList;
	}
	public void setEventList(Set<PaymentTransactionEvent> eventList) {
		this.eventList = eventList;
	}
	
	

	
	
	
	

}
