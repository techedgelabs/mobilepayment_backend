package com.techedge.mp.forecourt.integration.shop.interfaces;

import java.io.Serializable;

public class GetLastRefuelMessageResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2038371203297849699L;
	//TODO
	private String statusCode;
	private String messageCode;
	
	
	private String srcTransactionID;
	private String pumpID;
	private String pumpNumber;
	private String refuelMode;
	private StationDetail stationDetail;
	private RefuelDetails refuelDetail;
	
	
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public String getSrcTransactionID() {
		return srcTransactionID;
	}
	public void setSrcTransactionID(String srcTransactionID) {
		this.srcTransactionID = srcTransactionID;
	}
	public String getPumpID() {
		return pumpID;
	}
	public void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}
	public String getPumpNumber() {
		return pumpNumber;
	}
	public void setPumpNumber(String pumpNumber) {
		this.pumpNumber = pumpNumber;
	}
	public String getRefuelMode() {
		return refuelMode;
	}
	public void setRefuelMode(String refuelMode) {
		this.refuelMode = refuelMode;
	}
	public StationDetail getStationDetail() {
		return stationDetail;
	}
	public void setStationDetail(StationDetail stationDetail) {
		this.stationDetail = stationDetail;
	}
	public RefuelDetails getRefuelDetail() {
		return refuelDetail;
	}
	public void setRefuelDetail(RefuelDetails refuelDetail) {
		this.refuelDetail = refuelDetail;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	
}
