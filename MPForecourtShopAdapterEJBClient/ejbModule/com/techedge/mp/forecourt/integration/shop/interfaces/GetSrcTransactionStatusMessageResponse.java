package com.techedge.mp.forecourt.integration.shop.interfaces;

import java.io.Serializable;

public class GetSrcTransactionStatusMessageResponse implements Serializable {
	
	/**
     * 
     */
    private static final long serialVersionUID = -8058751662444538074L;
    //TODO
	private String statusCode;
	private String messageCode;
	private SrcTransactionDetail srcTransactionDetail;
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public SrcTransactionDetail getSrcTransactionDetail() {
		return srcTransactionDetail;
	}
	public void setSrcTransactionDetail(SrcTransactionDetail srcTransactionDetail) {
		this.srcTransactionDetail = srcTransactionDetail;
	}
	
	
}
