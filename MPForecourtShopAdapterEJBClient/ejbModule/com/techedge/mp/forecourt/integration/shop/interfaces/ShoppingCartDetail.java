package com.techedge.mp.forecourt.integration.shop.interfaces;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShoppingCart", propOrder = {
		"productID",
		"productDescription",
		"amount",
		"quantity"
})


public class ShoppingCartDetail implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8181404022825671257L;
	@XmlElement(required = true)
	private String productID;
	@XmlElement(required = true)
	private String productDescription;
	
	private Double amount;
	private Integer quantity;
	
	
	
	
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	
	
	

	
	
}
