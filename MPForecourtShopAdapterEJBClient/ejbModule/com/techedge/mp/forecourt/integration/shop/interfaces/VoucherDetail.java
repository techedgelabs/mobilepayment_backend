package com.techedge.mp.forecourt.integration.shop.interfaces;

import java.io.Serializable;

public class VoucherDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6279494229700564830L;

    protected double          voucherAmount;
    protected String          voucherCode;
    protected String          promoCode;
    protected String          promoDescription;

    public double getVoucherAmount() {
        return voucherAmount;
    }

    public void setVoucherAmount(double voucherAmount) {
        this.voucherAmount = voucherAmount;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }
}
