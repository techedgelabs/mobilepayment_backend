package com.techedge.mp.forecourt.integration.shop.interfaces;

import java.io.Serializable;

public class SrcTransactionDetail implements Serializable {


	/**
     * 
     */
    private static final long serialVersionUID = 7946815528211028323L;
    private String srcTransactionStatus;
	private Double amount;
	
	
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getSrcTransactionStatus() {
		return srcTransactionStatus;
	}
	public void setSrcTransactionStatus(String srcTransactionStatus) {
		this.srcTransactionStatus = srcTransactionStatus;
	}
	
	
	
	

	
	
}
