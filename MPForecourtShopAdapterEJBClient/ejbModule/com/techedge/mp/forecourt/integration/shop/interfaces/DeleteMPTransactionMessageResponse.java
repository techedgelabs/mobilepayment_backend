package com.techedge.mp.forecourt.integration.shop.interfaces;

public class DeleteMPTransactionMessageResponse {
//	TODO
	private String statusCode;
	private String messageCode;
	private PaymentTransactionStatus paymentTransactionStatus;
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public PaymentTransactionStatus getPaymentTransactionStatus() {
		return paymentTransactionStatus;
	}
	public void setPaymentTransactionStatus(PaymentTransactionStatus paymentTransactionStatus) {
		this.paymentTransactionStatus = paymentTransactionStatus;
	}
	
	
}
