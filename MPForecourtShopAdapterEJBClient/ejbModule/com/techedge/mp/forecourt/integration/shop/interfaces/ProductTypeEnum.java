package com.techedge.mp.forecourt.integration.shop.interfaces;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


@XmlType(name = "ProductTypeEnum")
@XmlEnum
public enum ProductTypeEnum  {

	OIL, NON_OIL, MIXED;
	
	 public String value() {
	        return name();
	    }

	    public static ProductTypeEnum fromValue(String v) {
	        return valueOf(v);
	    }

}
