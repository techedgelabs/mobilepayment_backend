package com.techedge.mp.forecourt.integration.shop.interfaces;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentTransactionEvent", propOrder = {
		"sequence",
		"type",
		"amount",
		"transactionResult",
		"errorCode",
		"errorDescription"
		
})

public class PaymentTransactionEvent {
	
	//TODO
	
	@XmlElement(required = true, nillable = true)
	private String sequence;
	@XmlElement(required = true, nillable = true)
	private String type;  				//eventType  MOV||STO
	@XmlElement(required = true, nillable = true)
	private Double amount;
	@XmlElement(required = true, nillable = true)
	private String transactionResult;
	@XmlElement(required = false, nillable = true)
	private String errorCode;
	@XmlElement(required = false, nillable = true)
	private String errorDescription;
	
	
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getTransactionResult() {
		return transactionResult;
	}
	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	

		

}
