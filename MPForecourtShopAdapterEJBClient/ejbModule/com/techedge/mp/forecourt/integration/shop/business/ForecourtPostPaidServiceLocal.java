package com.techedge.mp.forecourt.integration.shop.business;

import java.util.List;

import javax.ejb.Local;

import com.techedge.mp.forecourt.integration.shop.interfaces.ElectronicInvoice;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetLastRefuelMessageResponse;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse;
import com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.integration.shop.interfaces.SendMPTransactionResultMessageResponse;
import com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail;

@Local
public interface ForecourtPostPaidServiceLocal {

    public SendMPTransactionResultMessageResponse sendMPTransactionResult(String requestID, String stationID, String mpTransactionID, String srcTransactionID,
            String TransactionResult, PaymentTransactionResult paymentTransactionResult, Boolean loyaltyCredits, List<VoucherDetail> vouchers, ElectronicInvoice electronicInvoice);

    public GetSrcTransactionStatusMessageResponse getTransactionStatus(String requestID, String mpTransactionID, String srcTransactionID);

    public GetLastRefuelMessageResponse getLastRefuel(String requestID, String pumpID);

}
