package com.techedge.mp.forecourt.integration.shop.interfaces;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMPTransactionStatusMessageResponse", propOrder = {
		"statusCode",
		"messageCode",
		"transactionResult",
		"paymentTransactionStatus"
})


public class GetMPTransactionStatusMessageResponseOUT {
	
	//TODO
	@XmlElement(required = true, nillable = true)
	private String statusCode;
	@XmlElement(required = true, nillable = true)
	private String messageCode;
	@XmlElement(required = true, nillable = true)
	private String transactionResult;
	@XmlElement(required = true, nillable = true)
	private PaymentTransactionStatus paymentTransactionStatus;
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getTransactionResult() {
		return transactionResult;
	}
	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}
	public PaymentTransactionStatus getPaymentTransactionStatus() {
		return paymentTransactionStatus;
	}
	public void setPaymentTransactionStatus(PaymentTransactionStatus paymentTransactionStatus) {
		this.paymentTransactionStatus = paymentTransactionStatus;
	}
	
}
