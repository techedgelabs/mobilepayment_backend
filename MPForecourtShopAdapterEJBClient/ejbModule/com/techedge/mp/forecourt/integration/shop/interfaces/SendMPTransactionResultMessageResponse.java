package com.techedge.mp.forecourt.integration.shop.interfaces;

import java.io.Serializable;

public class SendMPTransactionResultMessageResponse implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 8381882450836990980L;
//	TODO
	
	
	private String statusCode;
	private String messageCode;
    private String electronicInvoiceID;
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
    public String getElectronicInvoiceID() {
        return electronicInvoiceID;
    }
    
    public void setElectronicInvoiceID(String electronicInvoiceID) {
        this.electronicInvoiceID = electronicInvoiceID;
    }

}
