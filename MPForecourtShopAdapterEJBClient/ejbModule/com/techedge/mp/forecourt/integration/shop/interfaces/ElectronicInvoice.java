package com.techedge.mp.forecourt.integration.shop.interfaces;

import java.io.Serializable;

public class ElectronicInvoice implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6259876294025773271L;

    private String name;

    private String surname;

    private String businessName;

    private String address;

    private String province;

    private String city;

    private String zipCode;

    private String streetNumber;

    private String country;

    private String fiscalCode;

    private String vatNumber;

    private String emailAdddress;

    private String pecEmailAdddress;

    private String sdiCode;

    private String licensePlate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getEmailAdddress() {
        return emailAdddress;
    }

    public void setEmailAdddress(String emailAdddress) {
        this.emailAdddress = emailAdddress;
    }

    public String getPecEmailAdddress() {
        return pecEmailAdddress;
    }

    public void setPecEmailAdddress(String pecEmailAdddress) {
        this.pecEmailAdddress = pecEmailAdddress;
    }

    public String getSdiCode() {
        return sdiCode;
    }

    public void setSdiCode(String sdiCode) {
        this.sdiCode = sdiCode;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

}
