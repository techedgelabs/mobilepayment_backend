package com.techedge.mp.forecourt.integration.shop.interfaces;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reverseMPTransactionMessageResponse", propOrder = {
		"statusCode",
		"messageCode",
		"transactionAuthorized"
})

public class ReverseMPTransactionMessageResponseOUT {
//	TODO
	@XmlElement(required = true, nillable = true)
	private String statusCode;
	@XmlElement(required = true, nillable = true)
	private String messageCode;
	@XmlElement(required = true, nillable = true)
	private String transactionAuthorized;
	//private PaymentTransactionResult paymentTransactionResult;
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
//	public PaymentTransactionResult getPaymentTransactionResult() {
//		return paymentTransactionResult;
//	}
//	public void setPaymentTransactionResult(PaymentTransactionResult paymentTransactionResult) {
//		this.paymentTransactionResult = paymentTransactionResult;
//	}
	public String getTransactionAuthorized() {
		return transactionAuthorized;
	}
	public void setTransactionAuthorized(String transactionResult) {
		this.transactionAuthorized = transactionResult;
	}
	
	
}
