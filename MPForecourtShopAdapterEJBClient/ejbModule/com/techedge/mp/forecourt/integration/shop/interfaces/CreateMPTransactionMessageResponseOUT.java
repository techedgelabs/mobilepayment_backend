package com.techedge.mp.forecourt.integration.shop.interfaces;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createMPTransactionMessageResponse", propOrder = {
		"mpTransactionID",
		"statusCode",
		"messageCode"
})

public class CreateMPTransactionMessageResponseOUT {
//	TODO
	@XmlElement(required = false, nillable = true)
	private String mpTransactionID;
	@XmlElement(required = true, nillable = true)
	private String statusCode;
	@XmlElement(required = true, nillable = true)
	private String messageCode;
	
	public String getMpTransactionID() {
		return mpTransactionID;
	}
	public void setMpTransactionID(String mpTransactionID) {
		this.mpTransactionID = mpTransactionID;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	


}
