package com.techedge.mp.forecourt.integration.shop.interfaces;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "refuelDetail", propOrder = {
	"timestampEndRefuel",
	"amount",
	"fuelType",
	"fuelQuantity",
	"productID",
	"productDescription",
	"unitPrice"
})
public class RefuelDetails implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5170935085244705938L;
	
	@XmlElement(required = true)
	protected String timestampEndRefuel;
	@XmlElement(required = true)
	protected Double amount;
	@XmlElement(required = false)
	protected String fuelType;
	@XmlElement(required = false)
	protected Double fuelQuantity;
	@XmlElement(required = false)
	protected String productID;
	@XmlElement(required = false)
	protected String productDescription;
	@XmlElement(required = false)
    protected Double unitPrice;
	
	
	public String getTimestampEndRefuel() {
		return timestampEndRefuel;
	}
	public void setTimestampEndRefuel(String timestampEndRefuel) {
		this.timestampEndRefuel = timestampEndRefuel;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	public Double getFuelQuantity() {
		return fuelQuantity;
	}
	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
    public Double getUnitPrice() {
        return unitPrice;
    }
    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }
	
}
