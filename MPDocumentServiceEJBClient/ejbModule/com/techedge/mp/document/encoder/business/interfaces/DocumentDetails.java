package com.techedge.mp.document.encoder.business.interfaces;

import java.io.Serializable;



public class DocumentDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8615350340374631433L;
	
	
	private String documentBase64;
	private Long dimension;
	private String filename;
	
	
	public String getDocumentBase64() {
		return documentBase64;
	}
	public void setDocumentBase64(String documentBase64) {
		this.documentBase64 = documentBase64;
	}
	public Long getDimension() {
		return dimension;
	}
	public void setDimension(Long dimension) {
		this.dimension = dimension;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	
	
}