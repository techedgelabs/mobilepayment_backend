
package com.techedge.mp.document.encoder.business;

import java.io.IOException;

import javax.ejb.Local;

import com.techedge.mp.document.encoder.business.interfaces.DocumentDetails;




@Local
public interface DocumentServiceLocal  {
	
	public DocumentDetails encodeFile(String filename) throws IOException;
	

}
