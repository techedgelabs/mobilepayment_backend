
package com.techedge.mp.document.encoder.business;

import java.io.IOException;

import javax.ejb.Remote;

import com.techedge.mp.document.encoder.business.interfaces.DocumentDetails;




@Remote
public interface DocumentServiceRemote  {
	
	public DocumentDetails encodeFile(String filename) throws IOException;
	

}
