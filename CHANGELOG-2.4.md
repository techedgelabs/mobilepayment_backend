CHANGELOG for 2.4.x
===================

This changelog references the relevant changes (bug and security fixes) done
in 2.4 minor versions.

* 2.4.12 (2018-08-07)
 
 * Fix controlli sulla città e il cap dell'indirizzo di fatturazione
 * Associazione manuale voucher da flusso CRM outbound
 * Definizione nuovi servizi per monitoraggio sistema da BO

* 2.4.11 (2018-06-14)
 
 * Login app Partita Iva con credenziali ES+
 * Fix controlli di validazione su indirizzo di fatturazione
 * Workaround per problema Gilbarco su transazioni postpaid
 * Flusso di riconciliazione per notifiche Enjoy

* 2.4.10 (2018-06-26)
 
 * Flusso di reset pin per utenti social
 * Report giornaliero per utenti business

* 2.4.9 (2018-06-18)
 
 * Bugfix minori

* 2.4.8 (2018-06-14)
 
 * Adeguamenti vari per app Partita Iva

* 2.4.7 (2018-06-11)
 
 * Rilascio servizi per app Partita Iva

* 2.4.6 (2018-06-04)
 
 * Implementazione flusso di aggiornamento automatico anagrafiche PV

* 2.4.5 (2018-05-30)
 
 * Iscrizione con Enjoy e invio notifica a backend Enjoy per primo rifornimento
 * Iscrizione con MyCicero
 * Report mensile transazioni Più Servito effettuate da utenti MyCicero
 * Adeguamento sezione +Punti per partner Alitalia con conversione punti->punti
 * Adeguamento servizio di conversione punti per accettare in input il codide della carta del partner, se richiesto
 * Bugfix vari

* 2.4.4 (2018-05-18)
 
 * Storico soste
 * Adeguamento scontrino
 * Inserimento manuale codice PV
 * Verifica numero targa
 * Adeguamento sezione +Premi

* 2.4.3 (2018-05-09)
 
 * Attivazione flussi social per login con Facebook e Google
 * Gestione utenza guest

* 2.4.2 (2018-05-07)

 * Bugfix vari

* 2.4.1 (2018-05-03)

 * Bugfix vari
 
* 2.4.0 (2018-04-30)

 * Attivazione nuova campagna Loyalty
 * Implementazione nuovo flusso per pagamento sosta con MyCicero
