package com.techedge.mp.gestpay;

import java.util.Hashtable;



import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

public class EJBHomeCache {

	final String userServiceRemoteJndi        = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserService!com.techedge.mp.core.business.UserServiceRemote";
	final String parametersServiceRemoteJndi  = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
	final String loggerServiceRemoteJndi      = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
	final String transactionServiceRemoteJndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionServiceRemote";
	final String gpServiceRemoteJndi = "java:global/MPPaymentAdapterEAR/MPPaymentAdapterEJB/GPService!com.techedge.mp.payment.adapter.business.GPServiceRemote";
	
	private static EJBHomeCache instance;
	
	protected Context context = null;
	
	protected UserServiceRemote       userService       = null;
	protected ParametersServiceRemote parametersService = null;
	protected LoggerServiceRemote     loggerService     = null;
	protected TransactionServiceRemote transactionService = null;
	protected GPServiceRemote gpService = null;
	
	private EJBHomeCache( ) throws InterfaceNotFoundException {
		
		final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();
		
		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		
		try {
			context = new InitialContext(jndiProperties);
		} catch (NamingException e) {

			throw new InterfaceNotFoundException("Naming exception: " + e.getMessage());
		}
	}

	public static synchronized EJBHomeCache getInstance( ) throws InterfaceNotFoundException
	{
		if (instance == null)
			instance = new EJBHomeCache( );
		
		return instance;
	}

	public UserServiceRemote getUserService() throws InterfaceNotFoundException {

        if (this.userService == null) {

            try {

                this.userService = (UserServiceRemote) context.lookup(userServiceRemoteJndi);
            }
            catch (Exception ex) {

                throw new InterfaceNotFoundException(userServiceRemoteJndi);
            }
        }

        return userService;
    }
	
	public ParametersServiceRemote getParametersService() throws InterfaceNotFoundException {

        if (this.parametersService == null) {

            try {

                this.parametersService = (ParametersServiceRemote) context.lookup(parametersServiceRemoteJndi);
            }
            catch (Exception ex) {

                throw new InterfaceNotFoundException(parametersServiceRemoteJndi);
            }
        }

        return parametersService;
    }
	
	public LoggerServiceRemote getLoggerService() throws InterfaceNotFoundException {

        if (this.loggerService == null) {

            try {

                this.loggerService = (LoggerServiceRemote) context.lookup(loggerServiceRemoteJndi);
            }
            catch (Exception ex) {

                throw new InterfaceNotFoundException(loggerServiceRemoteJndi);
            }
        }

        return loggerService;
    }
	
	public TransactionServiceRemote getTransactionService() throws InterfaceNotFoundException {

        if (this.transactionService == null) {

            try {

                this.transactionService = (TransactionServiceRemote) context.lookup(transactionServiceRemoteJndi);
            }
            catch (Exception ex) {

                throw new InterfaceNotFoundException(transactionServiceRemoteJndi);
            }
        }

        return transactionService;
    }
	
	public GPServiceRemote getGpService() throws InterfaceNotFoundException {

        if (this.gpService == null) {

            try {

                this.gpService = (GPServiceRemote) context.lookup(gpServiceRemoteJndi);
            }
            catch (Exception ex) {

                throw new InterfaceNotFoundException(gpServiceRemoteJndi);
            }
        }

        return gpService;
    }
}
