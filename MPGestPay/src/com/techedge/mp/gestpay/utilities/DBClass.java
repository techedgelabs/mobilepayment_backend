package com.techedge.mp.gestpay.utilities;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.core.business.TransactionServiceRemote;

public class DBClass {
	private TransactionServiceRemote transactionService;
	
	public DBClass(){
		
		this.transactionService = getRemoteObject();
	}
	
	public String getTokenByTransactionId(String transactionID){
		String token = "40FN2AW2GGI84007";
		//aggiungere codice per estrazione del token a partire dal transactionID
		//step1: recupero utenza da transactionid;
		//step2: recupero token da utenza;
		
		return token;
	}
	public String getShopLogin(){
		String shoplogin = "GESPAY61144";
		// aggiungere lettura dello shoplogin da file di configurazione o DB
		
		return shoplogin;
		
	}
	public String getValuta(){
		String uiccode = "242";
		// aggiungere lettura della valuta da file di configurazione o DB
		
		return uiccode;
		
	}
	public void setNegativeResponse() {
		// TODO Auto-generated method stub
		
	}
	public void setPositiveResponse() {
		// TODO Auto-generated method stub
		
		
	}
	
	/*
	public void setResponse(String statusCode, String subStatusCode,String error, String i_shopTransactionID, String bankTransactionID ){
		paymentService.updateStatus(statusCode, subStatusCode, error,  i_shopTransactionID, bankTransactionID);
	}
	*/
	
	public void setResponsePaymentAuthorization(String statusCode, String subStatusCode, String errorCode, String errorMessage, String i_shopTransactionID, String i_bankTransactionID, String i_authorizationCode ){
		transactionService.persistPaymentAuthorizationStatus(statusCode, subStatusCode, errorCode, errorMessage, i_shopTransactionID, i_bankTransactionID, i_authorizationCode);
	}
	
	
	public void setResponsePaymentDeletion(String statusCode, String subStatusCode, String errorCode, String errorMessage, String i_shopTransactionID, String bankTransactionID ){
		transactionService.persistPaymentDeletionStatus(statusCode, subStatusCode, errorCode, errorMessage, i_shopTransactionID, bankTransactionID);
	}
	
	
	public void setResponsePaymentCompletion(String statusCode, String subStatusCode, String errorCode, String errorMessage, String i_shopTransactionID, String bankTransactionID, Double effectiveAmount ){
		transactionService.persistPaymentCompletionStatus(statusCode, subStatusCode, errorCode, errorMessage, i_shopTransactionID, bankTransactionID, effectiveAmount);
	}
	
	public void setResponsePaymentRefundReconciliation(String statusCode, String subStatusCode, String error, String i_shopTransactionID, String bankTransactionID, Double effectiveAmount ){
		//transactionService.persistPaymentRefundReconciliationStatus(statusCode, subStatusCode, error,  i_shopTransactionID, bankTransactionID, effectiveAmount);
	}
	
	public void setResponsePaymentRefundCheckCarta(String statusCode, String subStatusCode, String error, String i_shopTransactionID, String bankTransactionID, Double effectiveAmount ){
		//transactionService.persistPaymentRefundCheckCartaStatus(statusCode, subStatusCode, error,  i_shopTransactionID, bankTransactionID, effectiveAmount);
	}
	
	private TransactionServiceRemote getRemoteObject() {
		
		TransactionServiceRemote transactionService = null;
		
		final Hashtable jndiProperties = new Hashtable();
		
		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		
		try {
			
            final Context context = new InitialContext(jndiProperties);

            final String jndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionServiceRemote";

            transactionService = (TransactionServiceRemote)context.lookup(jndi);
            
            return transactionService;
            
        } catch (NamingException ex) {
        	//ex.printStackTrace();
        	//logger.log(Level.INFO, "Error in getRemoteObject()");
			return null;
        }
	}
	
	

}
