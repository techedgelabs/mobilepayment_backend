package com.techedge.mp.gestpay.exception;

public class BPELException extends Exception {
	
	  /**
	 * 
	 */
	private static final long serialVersionUID = 2998741333040744329L;

	public BPELException() {
		  
		  super(); 
		  
	  }
	  
	  public BPELException(String message) {
		  
		  super(message); 
		  
	  }
	  
	  public BPELException(String message, Throwable cause) {
		  
		  super(message, cause); 
		  
	  }
	  
	  public BPELException(Throwable cause) {
		  
		  super(cause); 
		  
	  }
	  
}