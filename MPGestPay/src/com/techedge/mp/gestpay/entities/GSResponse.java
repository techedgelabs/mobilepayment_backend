package com.techedge.mp.gestpay.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.techedge.mp.payment.adapter.wss2s.xsd.GestPayS2S;

//import com.techedge.mp.gestpay.wss2s.xsd.GestPayS2S;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsresponse", propOrder = {
	"statusCode",
	"messageCode",
	"gps2s",
	
})

public class GSResponse {
	
	@XmlElement(required = true, nillable = true)
	protected String statusCode;
	@XmlElement(required = true, nillable = true)
	protected String messageCode;
	@XmlElement(required = true)
	protected GestPayS2S gps2s;
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public GestPayS2S getGPS2S() {
		return gps2s;
	}
	
	public void setGPS2S(GestPayS2S gps2s) {
		this.gps2s = gps2s;
	}
	
}
