package com.techedge.mp.gestpay;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.FidelityConsumeVoucherData;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionCancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.TransactionConsumeVoucherPreAuthResponse;
import com.techedge.mp.core.business.interfaces.TransactionExistsResponse;
import com.techedge.mp.core.business.interfaces.TransactionUseVoucherResponse;
//import com.techedge.mp.gestpay.entities.Extension;
import com.techedge.mp.gestpay.entities.GSResponse;
import com.techedge.mp.gestpay.exception.BPELException;
import com.techedge.mp.gestpay.utilities.DBClass;
//import com.techedge.mp.gestpay.wss2s.xsd.GestPayS2S;
//import com.techedge.mp.gestpay.wss2s.xsd.TransResult;
//import com.techedge.mp.gestpay.wss2s.xsd.TransType;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.payment.adapter.wss2s.xsd.GestPayS2S;
import com.techedge.mp.payment.adapter.wss2s.xsd.TransResult;
import com.techedge.mp.payment.adapter.wss2s.xsd.TransType;

@WebService(name = "gestPayService")
public class GestPayService {

    //private final static String      PARAM_PAYMENT_OUT_WSDL = "PAYMENT_OUT_WSDL";
    private final static String      PARAM_PROXY_HOST       = "PROXY_HOST";
    private final static String      PARAM_PROXY_PORT       = "PROXY_PORT";
    private final static String      PARAM_ACQUIRER_ID      = "ACQUIRER_ID";
    //private final static String       PARAM_SHOPLOGIN        = "SHOPLOGIN";

    private UserServiceRemote        userService            = null;
    private ParametersServiceRemote  parametersService      = null;
    //private LoggerServiceRemote      loggerService          = null;
    private TransactionServiceRemote transactionService     = null;
    private GPServiceRemote          gpService              = null;

    private URL                      url;

    // Valori di default
    private String                   proxyHost              = "mpsquid.enimp.pri";
    private String                   proxyPort              = "3128";

    private GSResponse               gsresponse;
    private DBClass                  dbobject               = new DBClass();
    private Boolean                  amount_zero            = false;
    private Boolean                  riconciliazione;
    private Boolean                  capLimitReached;

    private String                   acquirerId             = null;
    
    //private String                    defaultShopLogin  =   null;
    //private String                    defaultAcquirerID  =   null;

    //private PaymentServiceRemote paymentService;

    public GestPayService() {
    }
    
    private void initParams() {
        
        if ( this.acquirerId == null ) {
            
            try {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
                this.proxyHost = parametersService.getParamValue(GestPayService.PARAM_PROXY_HOST);
                this.proxyPort = parametersService.getParamValue(GestPayService.PARAM_PROXY_PORT);
                this.acquirerId = parametersService.getParamValue(GestPayService.PARAM_ACQUIRER_ID);
            }
            catch (ParameterNotFoundException e) {
    
                e.printStackTrace();
            }
            catch (InterfaceNotFoundException e) {
    
                e.printStackTrace();;
            }
        }
    }
    /*
    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }
    
    private URL getUrl() throws BPELException {

        if (this.url == null) {

            String wsdlString = "";
            try {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
                wsdlString = parametersService.getParamValue(GestPayService.PARAM_PAYMENT_OUT_WSDL);
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();

                //throw new BPELException("Parameter " + Info.PARAM_FORECOURT_WSDL + " not found: " + e.getMessage());
            }
            catch (InterfaceNotFoundException e) {

                e.printStackTrace();

                //throw new BPELException("InterfaceNotFoundException for jndi " + e.getJndiString());
            }

            try {
                this.url = new URL(wsdlString);
            }
            catch (MalformedURLException e) {

                e.printStackTrace();

                //throw new BPELException("Malformed URL: " + e.getMessage());
            }
        }

        return this.url;
    }
    */
    @WebMethod(operationName = "callPagam")
    public GSResponse requirePagam(@WebParam(name = "amount") @XmlElement(required = true) Double i_amount,
            @WebParam(name = "shopTransactionID") @XmlElement(required = true) String i_shopTransactionID,
            @WebParam(name = "shopLogin") @XmlElement(required = false) String i_shopLogin, @WebParam(name = "valuta") @XmlElement(required = false) String i_valuta,
            @WebParam(name = "token") @XmlElement(required = false) String i_token, @WebParam(name = "extensionList") @XmlElement(required = false) Extension[] i_extension)
            throws BPELException {

        String response = "";

        try {
            
            initParams();

            this.userService = EJBHomeCache.getInstance().getUserService();
            this.gpService = EJBHomeCache.getInstance().getGpService();

            // Verifica se l'utente � autorizzato all'operazione callPagam
            String operation = "callPagam";
            response = userService.checkTransactionAuthorization(i_amount, i_shopTransactionID, i_valuta, i_token, operation);

            System.out.println("checkTransactionAuthorization response: " + response);
            
            this.capLimitReached = false;

            if (response.equals(ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_TESTER)) {

                response = ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_SUCCESS;
                
                /*
                // Il servizio � stato richiamato da un utente tester,
                // perci� bisogna restituire una risposta positiva
                // senza effettuare nessuna operazione
                System.out.print("callPagam - tester");

                GestPayS2S gps2s = new GestPayS2S();

                gps2s.setAlertCode(null);
                gps2s.setAlertDescription(null);
                gps2s.setAuthorizationCode(null);
                gps2s.setBankTransactionID(null);
                gps2s.setBuyer(null);
                gps2s.setCompany(null);
                gps2s.setCountry(null);
                gps2s.setCurrency(i_valuta);
                gps2s.setCustomInfo(null);
                gps2s.setErrorCode("0");
                gps2s.setErrorDescription("OK");
                gps2s.setShopTransactionID(i_shopTransactionID);
                gps2s.setTransactionKey(null);
                gps2s.setTransactionResult(TransResult.OK);
                gps2s.setTransactionState(null);
                gps2s.setTransactionType(TransType.PAGAM);
                gps2s.setVbV(null);

                System.out.println("callPagam start sleep");
                try {
                    Thread.sleep(6000);
                }
                catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                System.out.println("callPagam end sleep");

                this.setPositiveResponse("P", i_shopTransactionID, gps2s, null);

                return gsresponse;
                */
            }

            if (!response.equals(ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_SUCCESS)) {

                if (response.equals(ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_UNAUTHORIZED)) {

                    System.out.print("User unauthorized");
                    throw new BPELException();
                }

                // TODO differenziare l'errore in base al valore restituito
                System.out.print("Available cap insufficient for operation");
                this.capLimitReached = true;
                //this.setNegativeResponse("P","", i_shopTransactionID );
                throw new BPELException();
                //return gsresponse;
            }

            String shopLogin = i_shopLogin;

            this.transactionService = EJBHomeCache.getInstance().getTransactionService();

            String requestID = String.valueOf(new Date().getTime());
            Transaction transaction = transactionService.getTransactionDetail(requestID, i_shopTransactionID);

            if (transaction.getNewPaymentFlow()
                    || (!transaction.getNewPaymentFlow() && transaction.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER))) {

                System.out.println("Utilizzo del nuovo flusso di pagamento");

                // Il nuovo flusso di pagamento prevede che il credito voucher sia stato gi� preautorizzato;
                // il codice identificativo della preautorizzazione � associato alla transazione ed � contenuto nel campo authorizationCode

                // Estrai la prima riga della tabella di log di consumo voucher e verifica che sia di tipo preautorizzazione
                PrePaidConsumeVoucher prePaidConsumeVoucherPreAuthorization = null;
                for (PrePaidConsumeVoucher prePaidConsumeVoucher : transaction.getPrePaidConsumeVoucherList()) {
                    prePaidConsumeVoucherPreAuthorization = prePaidConsumeVoucher;
                    break;
                }

                // Se non esiste una riga o se la riga non � di tipo preautorizzazione restituisci un errore
                if (prePaidConsumeVoucherPreAuthorization == null || !prePaidConsumeVoucherPreAuthorization.getOperationType().equals("PRE-AUTHORIZATION")) {

                    System.out.print("Preauthorization transaction not found for transaction " + transaction.getTransactionID());
                    this.capLimitReached = false;

                    throw new BPELException();
                }

                // Lo step di autorizzazione per il nuovo flusso deve solo inserire il nuovo stato e il nuovo evento sulle relative tabelle
                FidelityConsumeVoucherData fidelityConsumeVoucherData = new FidelityConsumeVoucherData();
                fidelityConsumeVoucherData.setAmount(prePaidConsumeVoucherPreAuthorization.getAmount());
                fidelityConsumeVoucherData.setCsTransactionID(prePaidConsumeVoucherPreAuthorization.getCsTransactionID());
                fidelityConsumeVoucherData.setMarketingMsg(prePaidConsumeVoucherPreAuthorization.getMarketingMsg());
                fidelityConsumeVoucherData.setMessageCode(prePaidConsumeVoucherPreAuthorization.getMessageCode());
                fidelityConsumeVoucherData.setPreAuthOperationID(prePaidConsumeVoucherPreAuthorization.getPreAuthOperationID());
                fidelityConsumeVoucherData.setStatusCode(prePaidConsumeVoucherPreAuthorization.getStatusCode());
                fidelityConsumeVoucherData.setWarningMsg(prePaidConsumeVoucherPreAuthorization.getWarningMsg());

                this.setPositiveResponseCreditVoucher("P", i_shopTransactionID, fidelityConsumeVoucherData, null);

                GestPayS2S gps2s = new GestPayS2S();

                String errorCode = prePaidConsumeVoucherPreAuthorization.getStatusCode();
                if (errorCode.equals("00")) {
                    errorCode = "0";
                }

                gps2s.setAlertCode(null);
                gps2s.setAlertDescription(null);
                gps2s.setAuthorizationCode(prePaidConsumeVoucherPreAuthorization.getPreAuthOperationID());
                gps2s.setBankTransactionID(prePaidConsumeVoucherPreAuthorization.getPreAuthOperationID());
                gps2s.setBuyer(null);
                gps2s.setCompany(null);
                gps2s.setCountry(null);
                gps2s.setCurrency(i_valuta);
                gps2s.setCustomInfo(null);
                gps2s.setErrorCode("0");
                gps2s.setErrorDescription(prePaidConsumeVoucherPreAuthorization.getMessageCode());
                gps2s.setShopTransactionID(i_shopTransactionID);
                gps2s.setTransactionKey(null);
                gps2s.setTransactionResult(TransResult.OK);
                gps2s.setTransactionState(null);
                gps2s.setTransactionType(TransType.PAGAM);
                gps2s.setVbV(null);

                gsresponse.setGPS2S(gps2s);

                System.out.println("PreAuthOperationID: " + prePaidConsumeVoucherPreAuthorization.getPreAuthOperationID());
                System.out.println("CsTransactionID: " + prePaidConsumeVoucherPreAuthorization.getCsTransactionID());

                return gsresponse;

            }
            else {
                
                System.out.println("preautorizzazione con carta di credito/multicard");

                String uicCode = i_valuta;// dbobject.getValuta();
                String amount = Double.toString(i_amount);
                String shopTransactionId = i_shopTransactionID;
                String tokenValue = i_token; //dbobject.getTokenByTransactionId(i_shopTransactionID); 

                System.out.println("requirePagam");

                System.out.println("SL: " + shopLogin + " _uicode: " + uicCode + " _amount: " + amount + " _stid: " + shopTransactionId + " _token: " + tokenValue);
                
                String acquirerID              = transaction.getAcquirerID();
                String groupAcquirer           = transaction.getGroupAcquirer();
                String encodedSecretKey        = transaction.getEncodedSecretKey();
                String paymentMethodExpiration = transaction.getPaymentMethodExpiration();
                String transactionCategory     = transaction.getTransactionCategory().getValue();

                GestPayData gestPayData = this.gpService.callPagam(i_amount, shopTransactionId, shopLogin, uicCode, tokenValue, null, acquirerID, groupAcquirer, encodedSecretKey, paymentMethodExpiration, i_extension, transactionCategory);

                GestPayS2S gps2s;
                if (gestPayData != null) {
                    gps2s = gestPayData.getGps2s();
                    this.setPositiveResponse("P", i_shopTransactionID, gps2s, null);
                }
                else {
                    throw new BPELException();
                }

                // Si sottrae al cap disponibile dell'utente l'importo autorizzato
                response = userService.updateAvailableCap(i_amount, i_shopTransactionID, i_valuta, i_token);

                // TODO gestire l'esito dell'operazione

                gsresponse.getGPS2S().setTransactionType(TransType.PAGAM);

                return gsresponse;
            }

        }
        catch (javax.xml.ws.soap.SOAPFaultException ex) {
            System.out.print("Eccezione soap1: " + ex);
            this.setNegativeResponse("P", "", i_shopTransactionID);
            throw new BPELException();

        }
        catch (javax.xml.ws.ProtocolException ex) {
            System.out.print("Eccezione soap1: " + ex);
            this.setNegativeResponse("P", "", i_shopTransactionID);
            throw new BPELException();

        }
        catch (javax.xml.ws.WebServiceException ex) {
            System.out.print("Eccezione ws1: " + ex);
            this.setNegativeResponse("P", "", i_shopTransactionID);
            throw new BPELException();

        }
        catch (java.lang.SecurityException ex) {
            System.out.print("Eccezione ex1: " + ex);
            this.setNegativeResponse("P", "", i_shopTransactionID);
            throw new BPELException();

        }
        catch (BPELException ex) {
            System.out.print("Eccezione exbpel: " + ex);
            this.setNegativeResponse("P", "", i_shopTransactionID);
            throw new BPELException();

        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.print("Eccezione ex2: " + ex);
            this.setNegativeResponse("P", "", i_shopTransactionID);
            throw new BPELException();

        }
    }

    @WebMethod(operationName = "callSettle")
    public GSResponse confirmPagam(@WebParam(name = "amount") @XmlElement(required = true) Double i_amount,
            @WebParam(name = "shopTransactionID") @XmlElement(required = true) String i_shopTransactionID,
            @WebParam(name = "extensionList") @XmlElement(required = false) Extension[] i_extension) throws BPELException {

        String response = "";

        try {
            
            initParams();

            this.userService = EJBHomeCache.getInstance().getUserService();

            // Verifica se l'utente � autorizzato all'operazione callSettle
            String operation = "callSettle";
            response = userService.checkTransactionAuthorization(i_amount, i_shopTransactionID, null, null, operation);

            this.capLimitReached = false;

            if (response.equals(ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_TESTER)) {

                response = ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_SUCCESS;
                /*
                // Il servizio � stato richiamato da un utente tester,
                // perci� bisogna restituire una risposta positiva
                // senza effettuare nessuna operazione
                System.out.print("callPagam - tester");

                GestPayS2S gps2s = new GestPayS2S();

                gps2s.setAlertCode(null);
                gps2s.setAlertDescription(null);
                gps2s.setAuthorizationCode(null);
                gps2s.setBankTransactionID(null);
                gps2s.setBuyer(null);
                gps2s.setCompany(null);
                gps2s.setCountry(null);
                gps2s.setCurrency("242");
                gps2s.setCustomInfo(null);
                gps2s.setErrorCode("0");
                gps2s.setErrorDescription("OK");
                gps2s.setShopTransactionID(i_shopTransactionID);
                gps2s.setTransactionKey(null);
                gps2s.setTransactionResult(TransResult.OK);
                gps2s.setTransactionState(null);
                gps2s.setTransactionType(TransType.SETTLE);
                gps2s.setVbV(null);

                this.setPositiveResponse("S", i_shopTransactionID, gps2s, i_amount);

                System.out.println("callSettle start sleep");
                try {
                    Thread.sleep(6000);
                }
                catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                System.out.println("callSettle end sleep");

                return gsresponse;
                */
            }

            //implementare codice per recuperare l'esercente da file config o database;
            this.transactionService = EJBHomeCache.getInstance().getTransactionService();

            TransactionExistsResponse transactionResponse = transactionService.transactionExists(i_shopTransactionID);

            String shopLogin = transactionResponse.getShopLogin();

            //implementare codice per recuperare la valuta da file config o database;
            String uicCode = transactionResponse.getUicCode();
            String amount = Double.toString(i_amount);
            String shopTransactionId = i_shopTransactionID;

            System.out.println("confirmPagam");

            System.out.println("shopLogin: " + shopLogin);
            System.out.println("uicCode: " + uicCode);
            System.out.println("amount: " + amount);
            System.out.println("shopTransactionId: " + shopTransactionId);
            System.out.println("Status: " + transactionResponse.getStatus());
            System.out.println("subStatus: " + transactionResponse.getSubStatus());

            //if (transactionResponse.getSubStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_CLOSED)
            //        || transactionResponse.getSubStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED)) {

            if (transactionResponse.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_CLOSED)
                    || transactionResponse.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED)) {

                System.out.println("Chiamata Anomala. Si sta cercando di pagare due volte la stessa transazione!");

                GestPayS2S gps2s = new GestPayS2S();

                gps2s.setAlertCode(null);
                gps2s.setAlertDescription(null);
                gps2s.setAuthorizationCode(null);
                gps2s.setBankTransactionID(null);
                gps2s.setBuyer(null);
                gps2s.setCompany(null);
                gps2s.setCountry(null);
                gps2s.setCurrency(uicCode);
                gps2s.setCustomInfo(null);
                gps2s.setErrorCode("0");
                gps2s.setErrorDescription("OK");
                gps2s.setShopTransactionID(i_shopTransactionID);
                gps2s.setTransactionKey(null);
                gps2s.setTransactionResult(TransResult.OK);
                gps2s.setTransactionState(null);
                gps2s.setTransactionType(TransType.SETTLE);
                gps2s.setVbV(null);

                return gsresponse;
            }

            Double effectiveAmount = i_amount;

            String requestID = String.valueOf(new Date().getTime());
            Transaction transaction = transactionService.getTransactionDetail(requestID, i_shopTransactionID);

            if (transaction.getNewPaymentFlow()) {

                System.out.println("callSettle: rilevato nuovo flusso voucher - chiamata utilizzo promo voucher disattivata.");
            }
            else {
                
                if (transaction.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {

                    //implementazione per uso voucher
                    System.out.println("callSettle: verifico se l'utente ha richiesto l'uso del voucher");
                    TransactionUseVoucherResponse transactionUseVoucherResponse = transactionService.transactionUseVoucher(i_shopTransactionID, i_amount);
        
                    if (transactionUseVoucherResponse.isVoucherUsed()) {
        
                        System.out.println("callSettle: richiesto uso voucher");
                        effectiveAmount = i_amount - transactionUseVoucherResponse.getVoucherTotalAmount();
                        BigDecimal bd = new BigDecimal(effectiveAmount).setScale(2, RoundingMode.HALF_EVEN);
                        effectiveAmount = bd.doubleValue();
        
                        System.out.println("Amount iniziale: " + i_amount + ", Amound residuo: " + effectiveAmount);
                    }
                    else {
        
                        System.out.println("callSettle: uso voucher non richiesto");
        
                        System.out.println("Amount iniziale: " + i_amount);
                    }
                }
            }

            if (effectiveAmount > 0) {

                System.out.println("Amount residuo > 0 --> chiama callSettle");

                Double initialAmount = transaction.getInitialAmount();

                System.out.println("Amount iniziale: " + initialAmount + ", Amound finale: " + effectiveAmount);

                if (effectiveAmount > initialAmount) {

                    System.out.println("Errore elettrovalvola: final amount maggiore dell'initial amount");

                    effectiveAmount = initialAmount;

                    System.out.println("Initial amount: " + initialAmount + ", final amount: " + effectiveAmount);
                }

                if (transaction.getNewPaymentFlow()
                        || (!transaction.getNewPaymentFlow() && transaction.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER))) {

                    System.out.println("Utilizzo del nuovo flusso di pagamento");

                    System.out.println("movimentazione credito voucher preautorizzato");

                    TransactionConsumeVoucherPreAuthResponse consumeVoucherPreAuthResponse = this.transactionService.consumeVoucherPreAuth(i_shopTransactionID, effectiveAmount);

                    if (consumeVoucherPreAuthResponse != null) {
                        FidelityConsumeVoucherData fidelityConsumeVoucherData = consumeVoucherPreAuthResponse.getFidelityConsumeVoucherData();
                        // TODO - Stub per far andare a buon fine l'operazione
                        fidelityConsumeVoucherData.setStatusCode(consumeVoucherPreAuthResponse.getFidelityStatusCode());
                        fidelityConsumeVoucherData.setMessageCode(consumeVoucherPreAuthResponse.getFidelityStatusMessage());
                        this.setPositiveResponseCreditVoucher("S", i_shopTransactionID, fidelityConsumeVoucherData, effectiveAmount);
                    }
                    else {
                        throw new BPELException();
                    }

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setAlertCode(null);
                    gps2s.setAlertDescription(null);
                    gps2s.setAuthorizationCode(null);
                    gps2s.setBankTransactionID(null);
                    gps2s.setBuyer(null);
                    gps2s.setCompany(null);
                    gps2s.setCountry(null);
                    gps2s.setCurrency(uicCode);
                    gps2s.setCustomInfo(null);

                    String errorCode = "0";
                    String errorDescription = "OK";
                    TransResult transactionResult = TransResult.OK;

                    if (!consumeVoucherPreAuthResponse.getFidelityStatusCode().equals("0")) {
                        System.out.println(consumeVoucherPreAuthResponse.getFidelityStatusCode());
                        errorCode = consumeVoucherPreAuthResponse.getFidelityStatusCode();
                        System.out.println(consumeVoucherPreAuthResponse.getFidelityStatusMessage());
                        errorDescription = consumeVoucherPreAuthResponse.getFidelityStatusMessage();
                        transactionResult = TransResult.KO;
                    }

                    gps2s.setErrorCode(errorCode);
                    gps2s.setErrorDescription(errorDescription);
                    gps2s.setTransactionResult(transactionResult);

                    gps2s.setShopTransactionID(i_shopTransactionID);
                    gps2s.setTransactionKey(null);
                    gps2s.setTransactionState(null);
                    gps2s.setTransactionType(TransType.SETTLE);
                    gps2s.setVbV(null);

                    gsresponse.setGPS2S(gps2s);

                    return gsresponse;

                }
                else {

                    System.out.println("movimentazione importo preautorizzato con carta di credito/multicard");

                    GestPayData gestPayData = this.gpService.callSettle(effectiveAmount, // i_amount,
                            shopTransactionId, shopLogin, uicCode, transaction.getAcquirerID(), transaction.getGroupAcquirer(), transaction.getEncodedSecretKey(),
                            transaction.getToken(), transaction.getAuthorizationCode(), transaction.getBankTansactionID(), transaction.getRefuelMode(),
                            transaction.getProductID(), transaction.getFuelQuantity(), transaction.getUnitPrice());

                    GestPayS2S gps2s;

                    if (gestPayData != null) {

                        System.out.println("gestPayData != null --> Invia notifica pagamento al GFG");

                        gps2s = gestPayData.getGps2s();
                        this.setPositiveResponse("S", i_shopTransactionID, gps2s, effectiveAmount); //i_amount);

                        gsresponse.getGPS2S().setTransactionType(TransType.SETTLE);
                    }
                    else {

                        System.out.println("gestPayData == null --> Gestire l'eccezione");

                        // TODO gestire l'eccezione
                        System.out.println("callSettle in eccezione");
                        //this.setNegativeResponse("S","", i_shopTransactionID );

                        throw new BPELException();
                    }
                }
            }
            else {

                System.out.println("Amount residuo == 0 --> chiama deletePagam");

                if (transaction.getNewPaymentFlow()
                        || (!transaction.getNewPaymentFlow() && transaction.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER))) {

                    System.out.println("cancellazione preautorizzazione consumo voucher");

                    TransactionCancelPreAuthorizationConsumeVoucherResponse cancelPreAuthorizationConsumeVoucher = this.transactionService.cancelPreAuthorizationConsumeVoucher(i_shopTransactionID);

                    this.amount_zero = true;

                    if (cancelPreAuthorizationConsumeVoucher != null) {
                        FidelityConsumeVoucherData fidelityConsumeVoucherData = cancelPreAuthorizationConsumeVoucher.getFidelityConsumeVoucherData();
                        fidelityConsumeVoucherData.setStatusCode(cancelPreAuthorizationConsumeVoucher.getFidelityStatusCode());
                        fidelityConsumeVoucherData.setMessageCode(cancelPreAuthorizationConsumeVoucher.getFidelityStatusMessage());
                        this.setPositiveResponseCreditVoucher("S", i_shopTransactionID, fidelityConsumeVoucherData, effectiveAmount);
                    }
                    else {
                        throw new BPELException();
                    }

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setAlertCode(null);
                    gps2s.setAlertDescription(null);
                    gps2s.setAuthorizationCode(null);
                    gps2s.setBankTransactionID(null);
                    gps2s.setBuyer(null);
                    gps2s.setCompany(null);
                    gps2s.setCountry(null);
                    gps2s.setCurrency("242");
                    gps2s.setCustomInfo(null);
                    gps2s.setErrorCode("0");
                    gps2s.setErrorDescription("OK");
                    gps2s.setShopTransactionID(i_shopTransactionID);
                    gps2s.setTransactionKey(null);
                    gps2s.setTransactionResult(TransResult.OK);
                    gps2s.setTransactionState(null);
                    gps2s.setTransactionType(TransType.PAGAM);
                    gps2s.setVbV(null);

                    gsresponse.setGPS2S(gps2s);

                    return gsresponse;

                }
                else {

                    System.out.println("cancellazione preautorizzazione uso carta");

                    Double amountToDelete = transaction.getInitialAmount();
                    
                    GestPayData gestPayData = this.gpService.deletePagam(amountToDelete, shopTransactionId, shopLogin, transaction.getCurrency(), null, null, transaction.getAcquirerID(), transaction.getGroupAcquirer(), transaction.getEncodedSecretKey());

                    this.amount_zero = true;

                    GestPayS2S gps2s;
                    if (gestPayData != null) {

                        System.out.println("gestPayData != null --> Invia notifica cancellazione autorizzazione al GFG");

                        gps2s = gestPayData.getGps2s();
                        this.setPositiveResponse("S", i_shopTransactionID, gps2s, effectiveAmount);

                        gsresponse.getGPS2S().setTransactionType(TransType.DELETE);
                    }
                    else {

                        System.out.println("gestPayData == null --> Gestire l'eccezione");

                        // TODO gestire l'eccezione
                        System.out.println("deletePagam in eccezione");
                        //this.setNegativeResponse("S","", i_shopTransactionID );

                        throw new BPELException();
                    }
                }
            }

            // Si sottrae al cap effettivo dell'utente l'importo movimentato e si allinea il cap disponibile se gli importi sono differenti
            response = userService.updateEffectiveCap(i_amount, i_shopTransactionID);

            // TODO gestire l'esito dell'operazione

            //gsresponse.getGPS2S().setTransactionType(TransType.SETTLE);

            return gsresponse;

        }

        catch (javax.xml.ws.soap.SOAPFaultException ex) {
            System.err.print("Eccezione soap1: " + ex);
            this.setNegativeResponse("S", ex.getMessage(), i_shopTransactionID);
            throw new BPELException();
        }
        catch (javax.xml.ws.ProtocolException ex) {
            System.err.print("Eccezione soap1: " + ex);
            this.setNegativeResponse("S", ex.getMessage(), i_shopTransactionID);
            throw new BPELException();
        }
        catch (javax.xml.ws.WebServiceException ex) {
            System.err.print("Eccezione ws1: " + ex);
            this.setNegativeResponse("S", ex.getMessage(), i_shopTransactionID);
            throw new BPELException();
        }
        catch (java.lang.SecurityException ex) {
            System.err.print("Eccezione ex1: " + ex);
            this.setNegativeResponse("S", ex.getMessage(), i_shopTransactionID);
            throw new BPELException();
        }
        catch (Exception ex) {
            System.out.print("Eccezione ex2: " + ex);
            System.out.println("catch 2");
            ex.printStackTrace();
            this.setNegativeResponse("S", ex.getMessage(), i_shopTransactionID);
            throw new BPELException();
        }

    }

    @WebMethod(operationName = "deletePagam")
    public GSResponse deletePagam(@WebParam(name = "shopTransactionID") @XmlElement(required = true) String i_shopTransactionID,
            @WebParam(name = "extensionList") @XmlElement(required = false) Extension[] i_extension) throws BPELException {

        String response = "";

        try {
            
            initParams();

            // Verifica se l'utente � autorizzato all'operazione deletePagam
            String operation = "deletePagam";
            response = userService.checkTransactionAuthorization(0.0, i_shopTransactionID, null, null, operation);
            
            if (response.equals(ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_TESTER)) {

                response = ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_SUCCESS;
                /*
                // Il servizio � stato richiamato da un utente tester,
                // perci� bisogna restituire una risposta positiva
                // senza effettuare nessuna operazione
                System.out.print("deletePagam - tester");

                GestPayS2S gps2s = new GestPayS2S();

                gps2s.setAlertCode(null);
                gps2s.setAlertDescription(null);
                gps2s.setAuthorizationCode(null);
                gps2s.setBankTransactionID(null);
                gps2s.setBuyer(null);
                gps2s.setCompany(null);
                gps2s.setCountry(null);
                gps2s.setCurrency("242");
                gps2s.setCustomInfo(null);
                gps2s.setErrorCode("0");
                gps2s.setErrorDescription("OK");
                gps2s.setShopTransactionID(i_shopTransactionID);
                gps2s.setTransactionKey(null);
                gps2s.setTransactionResult(TransResult.OK);
                gps2s.setTransactionState(null);
                gps2s.setTransactionType(TransType.DELETE);
                gps2s.setVbV(null);

                this.setPositiveResponse("D", i_shopTransactionID, gps2s, null);

                System.out.println("deletePagam start sleep");
                try {
                    Thread.sleep(6000);
                }
                catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                System.out.println("deletePagam end sleep");

                return gsresponse;
                */
            }

            this.userService = EJBHomeCache.getInstance().getUserService();
            this.transactionService = EJBHomeCache.getInstance().getTransactionService();

            //implementare codice per recuperare l'esercente da file config o database;

            String requestID = String.valueOf(new Date().getTime());
            Transaction transaction = transactionService.getTransactionDetail(requestID, i_shopTransactionID);

            TransactionExistsResponse transactionResponse = transactionService.transactionExists(i_shopTransactionID);
            
            if (transactionResponse.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_CLOSED)
                    || transactionResponse.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED)) {

                System.out.println("Chiamata Anomala. Si sta cercando di cancellare due volte la stessa transazione!");

                GestPayS2S gps2s = new GestPayS2S();

                gps2s.setAlertCode(null);
                gps2s.setAlertDescription(null);
                gps2s.setAuthorizationCode(null);
                gps2s.setBankTransactionID(null);
                gps2s.setBuyer(null);
                gps2s.setCompany(null);
                gps2s.setCountry(null);
                gps2s.setCurrency("242");
                gps2s.setCustomInfo(null);
                gps2s.setErrorCode("0");
                gps2s.setErrorDescription("OK");
                gps2s.setShopTransactionID(i_shopTransactionID);
                gps2s.setTransactionKey(null);
                gps2s.setTransactionResult(TransResult.OK);
                gps2s.setTransactionState(null);
                gps2s.setTransactionType(TransType.DELETE);
                gps2s.setVbV(null);

                return gsresponse;
            }

            if (transactionResponse.getStatus().equals(StatusHelper.STATUS_PUMP_GENERIC_FAULT)) {

                // Il servizio � stato richiamato dal bpel dopo un GENERIC_FAULT dell'enablePump
                // Non bisogna effettuare la cancellazione del pagamento, perch� la transazione deve essere riconciliata
                System.out.print("deletePagam - GENERIC_FAULT_500");

                GestPayS2S gps2s = new GestPayS2S();

                gps2s.setAlertCode(null);
                gps2s.setAlertDescription(null);
                gps2s.setAuthorizationCode(null);
                gps2s.setBankTransactionID(null);
                gps2s.setBuyer(null);
                gps2s.setCompany(null);
                gps2s.setCountry(null);
                gps2s.setCurrency("242");
                gps2s.setCustomInfo(null);
                gps2s.setErrorCode("0");
                gps2s.setErrorDescription("GENERIC_FAULT_500");
                gps2s.setShopTransactionID(i_shopTransactionID);
                gps2s.setTransactionKey(null);
                gps2s.setTransactionResult(TransResult.OK);
                gps2s.setTransactionState(null);
                gps2s.setTransactionType(TransType.DELETE);
                gps2s.setVbV(null);

                return gsresponse;
            }

            String shopLogin = transactionResponse.getShopLogin();

            String shopTransactionId = i_shopTransactionID;
            String currency          = transaction.getCurrency();

            System.out.println("deletePagam");
            System.out.println("shopLogin: " + shopLogin);
            System.out.println("shopTransactionId: " + shopTransactionId);
            System.out.println("currency: " + currency);
            this.setDeleteType(i_extension);

            if (transaction.getNewPaymentFlow()
                    || (!transaction.getNewPaymentFlow() && transaction.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER))) {

                System.out.println("Utilizzo del nuovo flusso di pagamento");

                System.out.println("cancellazione preautorizzazione consumo voucher");

                TransactionCancelPreAuthorizationConsumeVoucherResponse cancelPreAuthorizationConsumeVoucher = this.transactionService.cancelPreAuthorizationConsumeVoucher(i_shopTransactionID);
                if (cancelPreAuthorizationConsumeVoucher != null) {
                    FidelityConsumeVoucherData fidelityConsumeVoucherData = cancelPreAuthorizationConsumeVoucher.getFidelityConsumeVoucherData();
                    this.setPositiveResponseCreditVoucher("D", i_shopTransactionID, fidelityConsumeVoucherData, null);
                }
                else {
                    throw new BPELException();
                }

                GestPayS2S gps2s = new GestPayS2S();

                gps2s.setAlertCode(null);
                gps2s.setAlertDescription(null);
                gps2s.setAuthorizationCode(null);
                gps2s.setBankTransactionID(null);
                gps2s.setBuyer(null);
                gps2s.setCompany(null);
                gps2s.setCountry(null);
                gps2s.setCurrency("242");
                gps2s.setCustomInfo(null);
                gps2s.setErrorCode("0");
                gps2s.setErrorDescription("OK");
                gps2s.setShopTransactionID(i_shopTransactionID);
                gps2s.setTransactionKey(null);
                gps2s.setTransactionResult(TransResult.OK);
                gps2s.setTransactionState(null);
                gps2s.setTransactionType(TransType.PAGAM);
                gps2s.setVbV(null);

                gsresponse.setGPS2S(gps2s);

                return gsresponse;

            }
            else {

                System.out.println("cancellazione preautorizzazione uso carta");

                GestPayData gestPayData = this.gpService.deletePagam(transaction.getInitialAmount(), shopTransactionId, shopLogin, currency, null, null, transaction.getAcquirerID(), transaction.getGroupAcquirer(), transaction.getEncodedSecretKey());

                GestPayS2S gps2s;
                if (gestPayData != null) {
                    gps2s = gestPayData.getGps2s();
                    this.setPositiveResponse("D", i_shopTransactionID, gps2s, null); //delete amount zero;
                }
                else {

                    //this.setNegativeResponse("S","", i_shopTransactionID );
                    throw new BPELException();
                }

                // Si ripristina l'importo del cap disponibile precedentemente autorizzato
                response = userService.refundAvailableCap(i_shopTransactionID);

                // TODO gestire l'esito dell'operazione

                gsresponse.getGPS2S().setTransactionType(TransType.DELETE);

                return gsresponse;
            }

        }
        catch (Exception ex) {

            ex.printStackTrace();

            this.setNegativeResponse("D", "", i_shopTransactionID);
            throw new BPELException();

        }
    }

    @WebMethod(operationName = "callRefund")
    public GSResponse refundPagam(@WebParam(name = "amount") @XmlElement(required = true) Double i_amount,
            @WebParam(name = "shopTransactionID") @XmlElement(required = true) String i_shopTransactionID,
            @WebParam(name = "shopLogin") @XmlElement(required = false) String i_shopLogin, @WebParam(name = "valuta") @XmlElement(required = false) String i_valuta,
            @WebParam(name = "bankTransactionID") @XmlElement(required = true) String i_bankTransactionID,
            @WebParam(name = "extensionList") @XmlElement(required = false) Extension[] i_extension) throws BPELException {

        try {
            
            initParams();

            this.userService = EJBHomeCache.getInstance().getUserService();

            String shopLogin = i_shopLogin;// dbobject.getShopLogin(); 
            //implementare codice per recuperare la valuta da file config o database;
            String uicCode = i_valuta;// dbobject.getValuta();
            String amount = Double.toString(i_amount);
            String shopTransactionId = i_shopTransactionID;
            String authorizationCode = "";
            String bankTransactionId = i_bankTransactionID;
            // TODO Estrarre i dati
            String acquirerID       = "";
            String groupAcquirer    = "";
            String encodedSecretKey = "";
            String token            = "";
            String shopCode         = "";
            //String customInfo = this.setCustomInfo(i_extension);

            System.out.println("requireStorno");
            this.setRequestType(i_extension);

            System.out.println("SL: " + shopLogin + " _uicode: " + uicCode + " _amount: " + amount + " _stid: " + shopTransactionId);

            ////////////////
            GestPayData gestPayData = this.gpService.callRefund(i_amount, shopLogin, shopTransactionId, uicCode, token, authorizationCode, bankTransactionId, shopCode, acquirerID, groupAcquirer, encodedSecretKey);

            GestPayS2S gps2s;
            if (gestPayData != null) {
                gps2s = gestPayData.getGps2s();
                this.setPositiveResponse("R", i_shopTransactionID, gps2s, null);
            }
            else {

                //this.setNegativeResponse("S","", i_shopTransactionID );
                throw new BPELException();
            }

            ///////////////

            //			System.setProperty("http.proxyHost", this.proxyHost);
            //	    	System.setProperty("http.proxyPort", this.proxyPort);
            //
            //			WSs2S service1 = new WSs2S(this.getUrl());
            //			System.out.println("Create Web Service...");
            //			WSs2SSoap port1 = service1.getWSs2SSoap();
            //			System.out.println("Call Web Service Operation...");
            //			CallRefundS2SResult eresult = port1.callRefundS2S(
            //					shopLogin, 
            //					uicCode, 
            //					amount, 
            //					shopTransactionId, 
            //					bankTransactionId
            //			);
            //
            //			GestPayS2S gps2s;
            //			Object content = ((List)eresult.getContent()).get(0);
            //			JAXBContext context;
            //			try {
            //				System.out.println("OK");
            //				context = JAXBContext.newInstance(GestPayS2S.class);
            //				Unmarshaller um = context.createUnmarshaller();
            //				gps2s = (GestPayS2S)um.unmarshal((Node)content);
            //				this.setPositiveResponse("R", i_shopTransactionID ,gps2s, null);
            //			} catch (JAXBException e) {
            //				// TODO Auto-generated catch block
            //				e.printStackTrace();
            //				this.setNegativeResponse("R","", i_shopTransactionID );
            //				throw new BPELException();
            //			} 
        }

        catch (javax.xml.ws.soap.SOAPFaultException ex) {
            System.err.print("Eccezione soap1: " + ex);
            this.setNegativeResponse("R", "", i_shopTransactionID);
            throw new BPELException();

        }
        catch (javax.xml.ws.ProtocolException ex) {
            System.err.print("Eccezione soap1: " + ex);
            this.setNegativeResponse("R", "", i_shopTransactionID);
            throw new BPELException();

        }
        catch (javax.xml.ws.WebServiceException ex) {
            System.err.print("Eccezione ws1: " + ex);
            this.setNegativeResponse("R", "", i_shopTransactionID);
            throw new BPELException();

        }
        catch (java.lang.SecurityException ex) {
            System.err.print("Eccezione ex1: " + ex);
            this.setNegativeResponse("R", "", i_shopTransactionID);
            throw new BPELException();

        }
        catch (Exception ex) {
            System.err.print("Eccezione ex2: " + ex);
            this.setNegativeResponse("R", "", i_shopTransactionID);
            throw new BPELException();

        }

        //	paymentService.updateStatus(gsresponse.getStatusCode(), gsresponse.getMessageCode(), i_shopTransactionID);
        gsresponse.getGPS2S().setTransactionType(TransType.PAGAM);

        return gsresponse;
    }

    private void setPositiveResponse(String callmethodID, String i_shopTransactionID, GestPayS2S gps2s, Double amount) {

        gsresponse = new GSResponse();

        if (gps2s.getTransactionResult().equals(TransResult.OK)) {
            System.out.println("setPositiveResponse OK: " + callmethodID);
            gsresponse.setStatusCode("OK");
            gsresponse.setMessageCode(gps2s.getErrorDescription());
            gsresponse.setGPS2S(gps2s);
            if (callmethodID.equals("P")) {
                dbobject.setResponsePaymentAuthorization("ok", "", gps2s.getErrorCode(), gps2s.getErrorDescription(), i_shopTransactionID, gps2s.getBankTransactionID(),
                        gps2s.getAuthorizationCode());
            }
            else if (callmethodID.equals("S")) {
                dbobject.setResponsePaymentCompletion("ok", "", gps2s.getErrorCode(), gps2s.getErrorDescription(), i_shopTransactionID, gps2s.getBankTransactionID(), amount);
            }
            else if (callmethodID.equals("R")) {
                if (this.riconciliazione) {
                    dbobject.setResponsePaymentRefundReconciliation("ok", "", gps2s.getErrorDescription(), i_shopTransactionID, gps2s.getBankTransactionID(), amount);
                }
                else {
                    dbobject.setResponsePaymentRefundCheckCarta("ok", "", gps2s.getErrorDescription(), i_shopTransactionID, gps2s.getBankTransactionID(), amount);
                }
            }
            else if (callmethodID.equals("D")) {
                if (this.amount_zero) {
                    dbobject.setResponsePaymentDeletion("ok_za", "", gps2s.getErrorCode(), gps2s.getErrorDescription(), i_shopTransactionID, gps2s.getBankTransactionID());
                }
                else {
                    dbobject.setResponsePaymentDeletion("ok", "", gps2s.getErrorCode(), gps2s.getErrorDescription(), i_shopTransactionID, gps2s.getBankTransactionID());
                }
            }
            //dbobject.setPositiveResponse();
        }
        else {
            System.out.println("setPositiveResponse KO: " + callmethodID);
            gsresponse.setStatusCode("KO");
            gsresponse.setMessageCode(gps2s.getErrorDescription());
            gsresponse.setGPS2S(gps2s);
            if (callmethodID.equals("P")) {
                dbobject.setResponsePaymentAuthorization("ko", StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_REFUSED, gps2s.getErrorCode(), gps2s.getErrorDescription(),
                        i_shopTransactionID, gps2s.getBankTransactionID(), gps2s.getAuthorizationCode());
            }
            else if (callmethodID.equals("S")) {
                dbobject.setResponsePaymentCompletion("ko", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_NOT_CLOSED_REFUSED, gps2s.getErrorCode(), gps2s.getErrorDescription(),
                        i_shopTransactionID, gps2s.getBankTransactionID(), amount);
            }
            else if (callmethodID.equals("R")) {
                if (this.riconciliazione) {
                    dbobject.setResponsePaymentRefundReconciliation("ko", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_NOT_CLOSED_REFUSED, gps2s.getErrorDescription(),
                            i_shopTransactionID, gps2s.getBankTransactionID(), amount);
                }
                else {
                    dbobject.setResponsePaymentRefundCheckCarta("ko", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_NOT_CLOSED_FAULT, gps2s.getErrorDescription(),
                            i_shopTransactionID, gps2s.getBankTransactionID(), amount);
                }

            }
            else if (callmethodID.equals("D")) {
                if (this.amount_zero) {
                    dbobject.setResponsePaymentDeletion("ko_za", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_DELETED_REFUSED, gps2s.getErrorCode(), gps2s.getErrorDescription(),
                            i_shopTransactionID, gps2s.getBankTransactionID());
                }
                else {
                    dbobject.setResponsePaymentDeletion("ko", StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_REFUSED, gps2s.getErrorCode(), gps2s.getErrorDescription(),
                            i_shopTransactionID, gps2s.getBankTransactionID());
                }
            }
            //dbobject.setNegativeResponse();
        }

        System.out.println(gps2s.getErrorCode());
        System.out.println(gps2s.getErrorDescription());
    }

    private void setNegativeResponse(String callmethodID, String error, String i_shopTransactionID) {

        gsresponse = new GSResponse();
        //dbobject.setNegativeResponse();
        //dbobject.setResponse(statusCode,subStatusCode, i_shopTransactionID );

        System.out.println("setNegativeResponse: " + callmethodID);
        System.out.println("capLimitReached: " + this.capLimitReached);

        if (callmethodID.equals("P")) {
            if (this.capLimitReached) {
                System.out.println("CAP_LIMIT_REACH");
                dbobject.setResponsePaymentAuthorization("ko", StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_CAP_LIMIT_REACH, "", error, i_shopTransactionID, "", "");
            }
            else {
                System.out.println("FAULT");
                dbobject.setResponsePaymentAuthorization("ko", StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_FAULT, "", error, i_shopTransactionID, "", "");
            }
        }
        else if (callmethodID.equals("S")) {
            dbobject.setResponsePaymentCompletion("ko", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_CONSUME_VOUCHER_FAULT, "", error, i_shopTransactionID, "", 0.0);
        }
        else if (callmethodID.equals("R")) {
            if (riconciliazione) {
                dbobject.setResponsePaymentRefundReconciliation("ko", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_NOT_CLOSED_FAULT, error, i_shopTransactionID, "", 0.0);

            }
            else {
                dbobject.setResponsePaymentRefundCheckCarta("ko", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_NOT_CLOSED_FAULT, error, i_shopTransactionID, "", 0.0);
            }
        }
        else if (callmethodID.equals("D")) {
            if (this.amount_zero) {
                dbobject.setResponsePaymentDeletion("ko_za", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_DELETED_FAULT, "", error, i_shopTransactionID, "");
            }
            else {
                dbobject.setResponsePaymentDeletion("ko", StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_FAULT, "", error, i_shopTransactionID, "");
            }
        }
        gsresponse.setStatusCode("KO");
        gsresponse.setMessageCode("Errore");
    }
    /*
    private String setCustomInfo(Extension[] i_extension) {
        String customInfo = "";
        String key;
        String value;

        for (int i = 0; i < i_extension.length; i++) {
            key = i_extension[i].getKey();
            System.out.println("Key:" + key);
            value = i_extension[i].getValue();
            System.out.println("Value:" + value);
            if (key.startsWith("CST_") && value != "") {
                if (customInfo.equals("")) {
                    customInfo = key + "=" + value;
                }
                else {
                    customInfo = customInfo + "*P1*" + key + "=" + value;
                }
            }
        }

        System.out.println("CustomInfo: " + customInfo);
        return customInfo;
    }
    */
    private void setDeleteType(Extension[] i_extension) {
        Boolean amount_zero = false;
        String key;
        String value;

        for (int i = 0; i < i_extension.length; i++) {
            key = i_extension[i].getKey();
            System.out.println("Key:" + key);
            value = i_extension[i].getValue();
            System.out.println("Value:" + value);
            if (key.equals("PRIV_AMOUNT_NULL")) {
                if (value.toLowerCase().equals("true")) {
                    amount_zero = true;
                }
                else {
                    amount_zero = false;
                }
            }
        }

        System.out.println("Amount_zero: " + amount_zero);
        this.amount_zero = amount_zero;
    }

    private void setRequestType(Extension[] i_extension) {
        Boolean riconciliazione = false;
        String key;
        String value;

        for (int i = 0; i < i_extension.length; i++) {
            key = i_extension[i].getKey();
            System.out.println("Key:" + key);
            value = i_extension[i].getValue();
            System.out.println("Value:" + value);
            if (key.equals("PRIV_RICONCILIAZIONE")) {
                if (value.toLowerCase().equals("true")) {
                    riconciliazione = true;
                }
                else {
                    riconciliazione = false;
                }
            }
        }

        System.out.println("Amount_zero: " + amount_zero);
        this.riconciliazione = riconciliazione;
    }

    private void setPositiveResponseCreditVoucher(String callmethodID, String i_shopTransactionID, FidelityConsumeVoucherData fidelityConsumeVoucherData, Double amount) {

        gsresponse = new GSResponse();

        if (fidelityConsumeVoucherData.getStatusCode().equals("00") || fidelityConsumeVoucherData.getStatusCode().equals("0")) {
            System.out.println("setPositiveResponseCreditVoucher OK: " + callmethodID);
            gsresponse.setStatusCode("OK");
            gsresponse.setMessageCode(fidelityConsumeVoucherData.getMessageCode());
            //gsresponse.setGPS2S(gps2s);
            if (callmethodID.equals("P")) {
                dbobject.setResponsePaymentAuthorization("ok", "", fidelityConsumeVoucherData.getStatusCode(), fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID,
                        fidelityConsumeVoucherData.getCsTransactionID(), fidelityConsumeVoucherData.getPreAuthOperationID());
            }
            else if (callmethodID.equals("S")) {
                dbobject.setResponsePaymentCompletion("ok", "", fidelityConsumeVoucherData.getStatusCode(), fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID,
                        fidelityConsumeVoucherData.getCsTransactionID(), amount);
            }
            else if (callmethodID.equals("R")) {
                if (this.riconciliazione) {
                    dbobject.setResponsePaymentRefundReconciliation("ok", "", fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID,
                            fidelityConsumeVoucherData.getCsTransactionID(), amount);
                }
                else {
                    dbobject.setResponsePaymentRefundCheckCarta("ok", "", fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID,
                            fidelityConsumeVoucherData.getCsTransactionID(), amount);
                }
            }
            else if (callmethodID.equals("D")) {
                if (this.amount_zero) {
                    dbobject.setResponsePaymentDeletion("ok_za", "", fidelityConsumeVoucherData.getStatusCode(), fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID,
                            fidelityConsumeVoucherData.getCsTransactionID());
                }
                else {
                    dbobject.setResponsePaymentDeletion("ok", "", fidelityConsumeVoucherData.getStatusCode(), fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID,
                            fidelityConsumeVoucherData.getCsTransactionID());
                }
            }
            //dbobject.setPositiveResponse();
        }
        else {
            System.out.println("setPositiveResponseCreditVoucher KO: " + callmethodID);
            System.out.println("ststusCode: " + fidelityConsumeVoucherData.getStatusCode());
            gsresponse.setStatusCode("KO");
            gsresponse.setMessageCode(fidelityConsumeVoucherData.getMessageCode());
            //gsresponse.setGPS2S(gps2s);
            if (callmethodID.equals("P")) {
                dbobject.setResponsePaymentAuthorization("ko", StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_REFUSED, fidelityConsumeVoucherData.getStatusCode(),
                        fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID, fidelityConsumeVoucherData.getCsTransactionID(),
                        fidelityConsumeVoucherData.getPreAuthOperationID());
            }
            else if (callmethodID.equals("S")) {
                dbobject.setResponsePaymentCompletion("ko", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_NOT_CLOSED_REFUSED, fidelityConsumeVoucherData.getStatusCode(),
                        fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID, fidelityConsumeVoucherData.getCsTransactionID(), amount);
            }
            else if (callmethodID.equals("R")) {
                if (this.riconciliazione) {
                    dbobject.setResponsePaymentRefundReconciliation("ko", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_NOT_CLOSED_REFUSED,
                            fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID, fidelityConsumeVoucherData.getCsTransactionID(), amount);
                }
                else {
                    dbobject.setResponsePaymentRefundCheckCarta("ko", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_NOT_CLOSED_FAULT, fidelityConsumeVoucherData.getMessageCode(),
                            i_shopTransactionID, fidelityConsumeVoucherData.getCsTransactionID(), amount);
                }

            }
            else if (callmethodID.equals("D")) {
                if (this.amount_zero) {
                    dbobject.setResponsePaymentDeletion("ko_za", StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_DELETED_REFUSED, fidelityConsumeVoucherData.getStatusCode(),
                            fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID, fidelityConsumeVoucherData.getCsTransactionID());
                }
                else {
                    dbobject.setResponsePaymentDeletion("ko", StatusHelper.SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_REFUSED, fidelityConsumeVoucherData.getStatusCode(),
                            fidelityConsumeVoucherData.getMessageCode(), i_shopTransactionID, fidelityConsumeVoucherData.getCsTransactionID());
                }
            }
            //dbobject.setNegativeResponse();
        }

        System.out.println(fidelityConsumeVoucherData.getStatusCode());
        System.out.println(fidelityConsumeVoucherData.getMessageCode());
    }

}
