package com.techedge.mp.core.business;

import javax.ejb.Local;

@Local
public interface DWHServiceLocal {

    public String updatePassword(String operationID, Long requestTimestamp, String fiscalCode, String email, String oldPassword, String newPassword);
    public String activatePromotion(String promotionID, String fiscalCode);
    
}
