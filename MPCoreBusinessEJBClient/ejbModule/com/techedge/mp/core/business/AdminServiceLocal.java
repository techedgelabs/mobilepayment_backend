package com.techedge.mp.core.business;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminArchiveInvertResponse;
import com.techedge.mp.core.business.interfaces.AdminArchiveResponse;
import com.techedge.mp.core.business.interfaces.AdminAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.AdminDeleteTicketResponse;
import com.techedge.mp.core.business.interfaces.AdminErrorResponse;
import com.techedge.mp.core.business.interfaces.AdminPoPArchiveResponse;
import com.techedge.mp.core.business.interfaces.AdminPropagationUserDataResult;
import com.techedge.mp.core.business.interfaces.AdminRetrieveDataResponse;
import com.techedge.mp.core.business.interfaces.AdminRolesResponse;
import com.techedge.mp.core.business.interfaces.AdminUpdateParamsResponse;
import com.techedge.mp.core.business.interfaces.AdminUserNotVerifiedDeleteResult;
import com.techedge.mp.core.business.interfaces.CityInfo;
import com.techedge.mp.core.business.interfaces.CountPendingTransactionsResult;
import com.techedge.mp.core.business.interfaces.DeleteTestersData;
import com.techedge.mp.core.business.interfaces.DocumentAttribute;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GeneratePvResponse;
import com.techedge.mp.core.business.interfaces.GenerateTestersData;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.ParamInfo;
import com.techedge.mp.core.business.interfaces.PaymentDataResponse;
import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;
import com.techedge.mp.core.business.interfaces.PaymentMethodResponse;
import com.techedge.mp.core.business.interfaces.PollingIntervalData;
import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.core.business.interfaces.ProvinceData;
import com.techedge.mp.core.business.interfaces.RemoteSystem;
import com.techedge.mp.core.business.interfaces.AdminRemoteSystemCheckResult;
import com.techedge.mp.core.business.interfaces.RetrieveActivityLogData;
import com.techedge.mp.core.business.interfaces.RetrieveDocumentResponse;
import com.techedge.mp.core.business.interfaces.RetrieveLandingResponse;
import com.techedge.mp.core.business.interfaces.RetrieveManagerListData;
import com.techedge.mp.core.business.interfaces.RetrieveParamsData;
import com.techedge.mp.core.business.interfaces.RetrievePoPTransactionListData;
import com.techedge.mp.core.business.interfaces.RetrievePromotionsData;
import com.techedge.mp.core.business.interfaces.RetrieveStatisticsData;
import com.techedge.mp.core.business.interfaces.RetrieveTransactionListData;
import com.techedge.mp.core.business.interfaces.RetrieveVoucherTransactionListData;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.StationActivationData;
import com.techedge.mp.core.business.interfaces.StationData;
import com.techedge.mp.core.business.interfaces.StationsAdminData;
import com.techedge.mp.core.business.interfaces.SurveyList;
import com.techedge.mp.core.business.interfaces.SurveyQuestion;
import com.techedge.mp.core.business.interfaces.TypeData;
import com.techedge.mp.core.business.interfaces.UnavailabilityPeriodResponse;
import com.techedge.mp.core.business.interfaces.UpdatePvActivationFlagResult;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationDetail;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfoData;
import com.techedge.mp.core.business.interfaces.user.RetrieveUserListData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryData;
import com.techedge.mp.core.business.interfaces.user.UserTypeData;

@Local
public interface AdminServiceLocal {
    public AdminAuthenticationResponse adminAuthentication(String email, String password, String requestId);

    public String adminLogout(String ticketId, String requestId);

    public String adminCreate(String adminTicketId, String requestId, Admin admin);

    public RetrieveActivityLogData adminActivityLogRetrieve(String adminTicketId, String requestId, Timestamp start, Timestamp end, ErrorLevel minLevel, ErrorLevel maxLevel,
            String source, String groupId, String phaseId, String messagePattern);

    public RetrieveUserListData adminUserRetrieve(String adminTicketId, String requestId, Long id, String firstName,                    // LIKE
            // |OPT
            String lastName,                    // LIKE |OPT
            String fiscalCode,                  // LIKE |OPT
            String securityDataEmail,           // LIKE |OPT
            Integer userStatus,                 // EQUALS |OPT
            String externalUserId,              // LIKE |OPT
            Double capAvailableMin, Double capAvailableMax, Double capEffectiveMin, Double capEffectiveMax, Timestamp creationDateMin, Timestamp creationDateMax,  Integer userType);

    public RetrieveTransactionListData adminTransactionRetrieve(String adminTicketId, String requestId, String transactionId,       //
            String userId,                  //
            String stationId,//
            String pumpId,//
            String finalStatusType, //
            Boolean GFGNotification,//
            Boolean confirmed,//
            Date creationTimestampStart,//
            Date creationTimestampEnd,  //
            String productID, Boolean transactionHistoryFlag);

    public RetrievePoPTransactionListData adminPoPTransactionRetrieve(String adminTicketId, String requestId, String transactionId,       //
            String userId,                  //
            String stationId,//
            String sourceId,//
            String mpTransactionStatus, //
            Boolean notificationCreated,//
            Boolean notificationPaid,//
            Boolean notificationUser, Date creationTimestampStart,//
            Date creationTimestampEnd,  //
            Boolean transactionHistoryFlag);

    public PaymentInfoResponse adminPaymentMethodRetrieve(String adminTicketId, String requestId, Long id, String type);

    public String adminUserUpdate(String adminTicketId, String requestId, Long id, String firstName, String lastName, String email, Date birthDate, String birthMunicipality,
            String birthProvince, String sex, String fiscalCode, String language, Integer status, Boolean registrationCompleted, Double capAvailable, Double capEffective,
            String externalUserId, Integer userType, Boolean virtualizationCompleted, Integer virtualizationAttemptsLeft, Boolean eniStationUserType, Boolean depositCardStepCompleted);

    public String adminTransactionUpdate(String adminTicketId, String requestId, String transactionId, String finalStatusType, Boolean GFGNotification, Boolean confirmed,
            Integer reconciliationAttemptsLeft);

    public String adminPaymentMethodUpdate(String adminTicketId, String requestId, Long id, String type, Integer status, String defaultMethod, Integer attemptsLeft,
            Double checkAmount);

    public String adminCustomerUserCreate(String adminTicketId, String requestId, User user);

    public String adminUserDelete(String adminTicketId, String requestId, Long userId, Date creationStart, Date creationEnd);

    public GenerateTestersData adminTestersGenerate(String adminTicketId, String requestId, String emailPrefix, String emailsuffix, Integer startNumber, Integer userCount,
            String token, Integer userType);

    public DeleteTestersData adminTestersDelete(String adminTicketId, String requestId, String emailPrefix, String emailSuffix, Integer startNumber, Integer userCount);

    public RetrieveStatisticsData adminRetrieveStatistics(String adminTicketId, String requestId);

    public String adminAddStation(String adminTicketId, String requestId, Station station);

    public String adminUpdateStation(String adminTicketId, String requestId, Station station);

    public String adminDeleteStation(String adminTicketId, String requestId, Station station);

    public StationsAdminData adminAddStations(String adminTicketId, String requestId, StationsAdminData stationsAdminData);

    public StationsAdminData adminDeleteStations(String adminTicketId, String requestId, StationsAdminData stationsAdminData);

    public StationsAdminData adminSearchStations(String adminTicketId, String requestId, Station station);

    public RetrieveParamsData adminRetrieveParams(String adminTicketId, String requestId, String param);

    public String adminUpdateParam(String adminTicketId, String requestId, String param, String value, String operation);

    public AdminUpdateParamsResponse adminUpdateParams(String adminTicketId, String requestId, List<ParamInfo> params);

    public AdminArchiveResponse adminArchive(String adminTicketId, String requestId, String id);

    public AdminArchiveInvertResponse adminArchiveInvert(String adminTicketId, String requestId, String id);

    public AdminPoPArchiveResponse adminPoPArchive(String adminTicketId, String requestId, Date startDate, Date endDate);

    public String adminResendConfirmUserEmail(String adminTicketId, String requestId, String userEmail, String category);

    public ReconciliationInfoData adminReconcileTransactions(String adminTicketId, String requestId, List<String> transactionsID);

    public ReconciliationDetail adminReconcileDetail(String adminTicketId, String requestId, String transactionID);

    public String adminCreateManager(String adminTicketId, String requestId, Manager manager);

    public String adminDeleteManager(String adminTicketId, String requestId, Manager manager);

    public String adminUpdateManager(String adminTicketId, String requestId, Manager manager);

    public RetrieveManagerListData adminSearchManager(String adminTicketId, String requestId, Long id, String username, String email, Integer maxResults);

    public String adminAddStationManager(String adminTicketId, String requestId, Long managerId, Long stationId);

    public String adminCheckAdminAuthorization(String adminTicketId, String operation);

    public RetrievePoPTransactionListData adminRetrievePoPTransactionReconciliation(String adminTicketId, String requestId);

    public String adminAddVoucherToPromotion(String adminTicketId, String requestId, PromoVoucherInput listPromoVoucher, String promotionCode);

    public String adminSurveyCreate(String adminTicketId, String requestId, String code, String description, String note, Integer status, Date startDate, Date endDate,
            ArrayList<SurveyQuestion> questions);

    public String adminSurveyUpdate(String adminTicketId, String requestId, String code, String description, String note, Integer status, Date startDate, Date endDate);

    public String adminSurveyQuestionAdd(String adminTicketId, String requestId, String surveyCode, String questionCode, String text, String type, Integer sequence);

    public SurveyList adminSurveyGet(String adminTicketId, String requestId, String surveyCode, Date startSearch, Date endSearch, Integer status);

    public String adminSurveyReset(String adminTicketId, String requestId, String surveyCode);

    public String adminSurveyQuestionUpdate(String adminTicketId, String requestId, String questionCode, String text, String type, Integer sequence);

    public String adminSurveyQuestionDelete(String adminTicketId, String requestId, String questionCode);

    public String adminCardBinAdd(String adminTicketId, String requestId, String bin);

    public RetrievePromotionsData adminRetrievePromotions(String adminTicketId, String requestId);

    public String adminUpdatePromotion(String adminTicketId, String requestId, String code, String description, Integer status, Date startData, Date endData);

    public String adminEditMappingError(String adminTicketId, String requestID, String gpErrorCode, String mpStatusCode);

    public String adminCreateMappingError(String adminTicketId, String requestID, String gpErrorCode, String mpStatusCode);

    public List<AdminErrorResponse> adminGetMappingError(String adminTicketId, String requestID, String mpStatusCode);

    public String adminRemoveMappingError(String adminTicketId, String requestID, String mpStatusCode);

    public String adminCreateUserCategory(String adminTicketId, String requestId, String name);

    public String adminCreateUserType(String adminTicketId, String requestId, Integer code);

    public String adminAddTypeToUserCategory(String adminTicketId, String requestId, String name, Integer code);

    public String adminRemoveTypeFromUserCategory(String adminTicketId, String requestId, String name, Integer code);

    public UserCategoryData adminRetrieveUserCategory(String adminTicketId, String requestId, String name);

    public UserTypeData adminRetrieveUserType(String adminTicketId, String requestId, Integer code);

    public String adminCreatePrefixNumber(String adminTicketId, String requestId, String code);

    public String adminRemovePrefixNumber(String adminTicketId, String requestId, String code);

    public PrefixNumberResult adminRetrieveAllPrefixNumber(String adminTicketId, String requestId);

    public RetrieveVoucherTransactionListData adminRetrieveVoucherTransactions(String adminTicketId, String requestId, String voucherTransactionId,        //
            Long userId,                  //
            String voucherCode,//
            Date creationTimestampStart,//
            Date creationTimestampEnd,  //
            String finalStatusType);

    public String adminForceUpdateTermsOfService(String ticketId, String requestId, String keyval, Long personalDataId, Boolean valid);

    public String adminCreateBlockPeriod(String ticketId, String requestId, String code, String startDate, String endDate, String startTime, String endTime, String operation,
            String statusCode, String statusMessage, Boolean active);

    public String adminUpdateBlockPeriod(String ticketId, String requestId, String code, String startDate, String endDate, String startTime, String endTime, String operation,
            String statusCode, String statusMessage, Boolean active);

    public String adminDeleteBlockPeriod(String ticketId, String requestId, String code);

    public UnavailabilityPeriodResponse adminRetrieveBlockPeriod(String ticketId, String requestId, String code, Boolean active);

    public String adminCreateDocument(String adminTicketId, String requestID, String documentKey, Integer position, String templateFile, String title, String subtitle,
            List<DocumentAttribute> attributes, String userCategory, String groupCategory);

    public String adminUpdateDocument(String adminTicketId, String requestID, String documentKey, Integer position, String templateFile, String title, String subtitle, String userCategory, String groupCategory);

    public String adminDeleteDocument(String adminTicketId, String requestID, String documentKey);

    public RetrieveDocumentResponse adminRetrieveDocument(String adminTicketId, String requestID, String documentKey);

    public String adminCreateDocumentAttribute(String adminTicketId, String requestID, String checkKey, Boolean mandatory, Integer position, String conditionText,
            String extendedConditionText, String documentKey, String subtitle);

    public String adminDeleteDocumentAttribute(String adminTicketId, String requestID, String checkKey);

    public String adminUpdatePassword(String adminTicketId, String requestID, String oldPassword, String newPassword);

    public GeneratePvResponse adminGeneratePv(String ticketId, String requestID, Integer finalStatus, Boolean skipCheck, String noOilAcquirerId, String noOilShopLogin,
            String oilAcquirerId, String oilShopLogin, List<StationData> stationList);

    public String adminForceSmsNotificationStatus(String ticketId, String requestId, String correlationId, Integer status);

    public String adminGenerateMailingList(String ticketId, String requestId, String selectionType);

    public String adminUpdateMailingList(String ticketId, String requestId, Long id, String email, String name, Integer status, String template);

    public String adminAssignVoucher(String ticketId, String requestId, Long userId, String voucherCode);

    public String adminRemoveTransactionEvent(String ticketId, String requestId, Long transactionEventId);

    public String adminCreateEmailDomainInBlackList(String adminTicketId, String requestId, List<String> emails);

    public String adminEditEmailDomainInBlackList(String adminTicketId, String requestId, String id, String email);

    public String adminRemoveEmailDomainInBlackList(String adminTicketId, String requestId, String email);

    public List<String> adminGetEmailDomainInBlackList(String adminTicketId, String requestId, String email);

    public String adminInsertUserInPromoOnHold(String ticketId, String requestId, String promotionCode, Long userId);

    public String adminUpdatePopTransaction(String ticketId, String requestId, String transactionId, String mpTransactionStatus, Boolean toReconcile);

    public String adminUpdateVoucherTransaction(String ticketId, String requestId, String voucherTransactionId, String finalStatusType);

    public String adminUpdateVoucherData(String ticketId, String requestId, Long id, String status);

    public String adminDeleteVoucher(String ticketId, String requestId, Long id);

    public String adminCreateProvince(String ticketId, String requestId, List<ProvinceData> province);

    public String adminRemoveMobilePhone(String ticketId, String requestId, Long mobilePhoneId);

    public List<AdminPropagationUserDataResult> adminPropagationUserData(String adminTicketId, String requestId, Long userID, List<String> fiscalCode);

    public String adminGeneralCreate(String adminTicketId, String requestId, String type, List<TypeData> fields);

    public String adminGeneralUpdate(String adminTicketId, String requestId, String type, Long id, List<TypeData> fields);

    public String adminUpdatePrepaidConsumeVoucher(String ticketId, String requestId, Long id, String csTransactionID, String marketingMsg, String messageCode, String operationID,
            String operationIDReversed, String operationType, Boolean reconciled, String statusCode, Double totalConsumed, String warningMsg, Double amount,
            String preAuthOperationID);

    public String adminAddPrepaidConsumeVoucherDetail(String ticketId, String requestId, Double consumedValue, Date expirationDate, Double initialValue, String promoCode,
            String promoDescription, String promoDoc, Double voucherBalanceDue, String voucherCode, String voucherStatus, String voucherType, Double voucherValue,
            Long prePaidConsumeVoucherId);

    public String adminGeneralDelete(String ticketId, String requestId, String type, String id);

    public String adminCreateAdminRole(String adminTicketId, String requestID, String name);

    public String adminDeleteAdminRole(String adminTicketId, String requestID, String name);

    public String adminAddRoleToAdmin(String adminTicketId, String requestId, String role, String admin);

    public AdminRolesResponse retrieveAdminRole(String adminTicketId, String requestId);

    public String adminRemoveRoleToAdmin(String adminTicketId, String requestId, String role, String admin);

    public String adminPushNotificationSend(String adminTicketId, String requestID, Long userID, Long idMessage, String titleNotification, String messageNotification);

    public AdminRetrieveDataResponse adminRetrieve(String adminTicketId, String requestID, Long id, String email, Integer status);

    public String adminUpdate(String adminTicketId, String requestID, Admin admin);

    public String adminDelete(String adminTicketId, String requestID, Long adminID);

    public String adminCreateRefuelingUser(String ticketId, String requestId, String username, String password);
    
    public String adminUpdatePasswordRefuelingUser(String ticketId, String requestId, String username, String newPassword);
    
    public PaymentMethodResponse adminInsertPaymentMethodRefuelingUser(String ticketId, String requestId, String username);
    
    public String adminRemovePaymentMethodRefuelingUser(String ticketId, String requestId, String username, Long id);
    
    public PaymentDataResponse adminRetrievePaymentDataMethodRefuelingUser(String ticketId, String requestId, String username, Long id);
    
    public String adminGeneralMassiveDelete(String ticketId, String requestId, String type, String idFrom,String idTo);
    
    public AdminDeleteTicketResponse adminTicketDelete(String ticketId, String requestId, String ticketType, String idFrom,String idTo);
    
    public RetrieveLandingResponse adminRetrieveLanding(String ticketId, String requestId);
    
    public PollingIntervalData adminRetrievePollingInterval(String adminTicketId, String requestId);

    public String adminUpdateCities(String adminTicketId, String requestId, List<CityInfo> citiesList, boolean deleteAllRows);
    
    public AdminUserNotVerifiedDeleteResult adminUserNotVerifiedDelete(String adminTicketId, String requestId, Long userId, Date creationStart, Date creationEnd);
    
    public UpdatePvActivationFlagResult adminUpdatePvActivationFlag(String ticketId, String requestID, List<StationActivationData> stationActivationList);

    public List<AdminRemoteSystemCheckResult> systemCheck(String adminTicketId, String requestId, List<RemoteSystem> remoteSystemList);
    
    public PaymentMethodResponse adminInsertMulticardRefuelingUser(String adminTicketId, String requestId, String username);
    
    public CountPendingTransactionsResult adminCountPendingTransactions(String adminTicketId, String requestId, String serverName );
    
    public String adminPaymentMethodStatusUpdate(String adminTicketId, String requestId, Long id, String type, Integer status, String message);
}
