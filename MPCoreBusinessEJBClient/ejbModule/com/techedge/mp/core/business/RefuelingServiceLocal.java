package com.techedge.mp.core.business;

import javax.ejb.Local;

import com.techedge.mp.core.business.interfaces.refueling.RefuelingCreateMPTransactionResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTokenResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTransactionReportResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTransactionStatusResponse;

@Local
public interface RefuelingServiceLocal {

    public RefuelingGetMPTokenResponse getMPToken(String requestID, String username, String password);

    public RefuelingCreateMPTransactionResponse createMPTransaction(String requestID, String mpToken, String srcTransactionID, String stationID, String pumpID, Double amount,
            String currency, Integer pumpNumber, String refuelMode);

    public RefuelingGetMPTransactionStatusResponse getMPTransactionStatus(String requestID, String mpToken, String srcTransactionID, String mpTransactionID);

    public RefuelingGetMPTransactionReportResponse getMPTransactionReport(String requestID, String mpToken, String startDate, String endDate, String mpTransactionID);

    public String notifySubscription(String requestId, String fiscalCode);


}
