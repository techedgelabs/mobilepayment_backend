package com.techedge.mp.core.business;

import javax.ejb.Remote;

@Remote
public interface CRMSfTokenServiceRemote {
    
    public String manageToken(String newToken);
}
