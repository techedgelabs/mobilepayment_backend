package com.techedge.mp.core.business;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElement;
import com.techedge.mp.core.business.interfaces.crm.CrmSfDataElement;
import com.techedge.mp.core.business.interfaces.crm.GetMissionsResponse;
import com.techedge.mp.core.business.interfaces.crm.GetOffersResult;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.ProcessCrmOutboundInterfaceResponse;
import com.techedge.mp.core.business.interfaces.crm.ProcessEventResult;
import com.techedge.mp.core.business.interfaces.crm.Promo4MeResponse;
import com.techedge.mp.core.business.interfaces.crm.UpdateContactKeyBulkResult;
import com.techedge.mp.core.business.interfaces.crm.UpdateContactKeyResult;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;

@Remote
public interface CRMServiceRemote {
    
    public String sendNotifyEventOffer(String fiscalCode, InteractionPointType interactionPoint, UserProfile userProfile, Long sourceID, CRMEventSourceType sourceType);

    public ProcessCrmOutboundInterfaceResponse processCrmOutboundInterface(List<CrmDataElement> crmDataElementList);
    
    public Promo4MeResponse getPromo4Me(String ticketID, String requestID);
    
    public GetMissionsResponse getMissions(String ticketID, String requestID);
    
    public ProcessEventResult processEvent(String operationId, String requestId, String fiscalCode, String promoCode, Double voucherAmount);
    
    public NotifyEventResponse sendNotifySfEventOffer(String requestId, String fiscalCode, Date date, String stationId, String productId, Boolean paymentFlag, Integer credits, Double quantity, String refuelMode, String firstName, String lastName,
            String email, Date birthDate, Boolean notificationFlag, Boolean paymentCardFlag, String brand, String cluster, Double amount, Boolean privacyFlag1, Boolean privacyFlag2, String mobilePhone,
            String loyaltyCard, String eventType, String parameter1, String parameter2, String paymentMode);
    
    public GetOffersResult getMissionsSf(String requestId, String fiscalCode, Date date, Boolean notificationFlag, Boolean paymentCardFlag, String brand, String cluster);

    public ProcessCrmOutboundInterfaceResponse processCrmSfOutboundInterface(List<CrmSfDataElement> crmSfDataElementList);
    
    public UpdateContactKeyResult updateContactKey(String operationId, String requestId, String fiscalCode, String contactKey);
    
    public UpdateContactKeyBulkResult insertBulkContactKey(String operationId, String requestId, Map<String,String> usersRefresh);
}
