package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Admin implements Serializable {

    /**
	 * 
	 */
    private static final long   serialVersionUID      = 5046973407089851930L;

    public static final Integer ADMIN_STATUS_BLOCKED  = 0;

    public static final Integer ADMIN_STATUS_VERIFIED = 2;

    public static final Integer ADMIN_STATUS_NEW      = 5;

    private Long                id;

    private String              email;

    private String              password;

    private String              firstName;

    private String              lastName;

    private Integer             status;

    private Set<AdminRole>      roles                 = new HashSet<AdminRole>(0);

    public Admin() {

    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstname) {

        this.firstName = firstname;
    }

    public String getLastName() {

        return lastName;
    }

    public void setLastName(String lastname) {

        this.lastName = lastname;
    }

    public Integer getStatus() {

        return status;
    }

    public void setStatus(Integer status) {

        this.status = status;
    }

    public Set<AdminRole> getRoles() {

        return roles;
    }

    public void setRoles(Set<AdminRole> roles) {

        this.roles = roles;
    }

}
