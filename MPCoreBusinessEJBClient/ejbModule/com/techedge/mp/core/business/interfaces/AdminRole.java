package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class AdminRole implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6631573268530794247L;

    private String            name;

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

}
