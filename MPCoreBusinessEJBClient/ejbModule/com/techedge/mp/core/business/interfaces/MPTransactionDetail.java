package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.refueling.RefuelDetail;

public class MPTransactionDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3267003439492821549L;

    private String            srcTransactionID;

    private String            mpTransactionID;

    private String            mpTransactionStatus;

    private RefuelDetail      refuelDetail;

    public String getSrcTransactionID() {

        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {

        this.srcTransactionID = srcTransactionID;
    }

    public String getMpTransactionID() {

        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {

        this.mpTransactionID = mpTransactionID;
    }

    public String getMpTransactionStatus() {

        return mpTransactionStatus;
    }

    public void setMpTransactionStatus(String mpTransactionStatus) {

        this.mpTransactionStatus = mpTransactionStatus;
    }

    public RefuelDetail getRefuelDetail() {

        return refuelDetail;
    }

    public void setRefuelDetail(RefuelDetail refuelDetail) {

        this.refuelDetail = refuelDetail;
    }

}
