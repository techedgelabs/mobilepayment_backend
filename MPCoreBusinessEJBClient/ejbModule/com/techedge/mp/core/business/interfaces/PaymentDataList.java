package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class PaymentDataList implements Serializable {

    private static final long serialVersionUID = 180719082021491605L;

    private Long              id;

    private String            type;

    private String            brand;

    private Date              expirationDate;

    private Integer           status;

    private String            message;

    private String            defaultMethod;

    private Date              insertDate;

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public String getBrand() {

        return brand;
    }

    public void setBrand(String brand) {

        this.brand = brand;
    }

    public Date getExpirationDate() {

        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {

        this.expirationDate = expirationDate;
    }

    public Integer getStatus() {

        return status;
    }

    public void setStatus(Integer status) {

        this.status = status;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }

    public String getDefaultMethod() {

        return defaultMethod;
    }

    public void setDefaultMethod(String defaultMethod) {

        this.defaultMethod = defaultMethod;
    }

    public Date getInsertDate() {

        return insertDate;
    }

    public void setInsertDate(Date insertDate) {

        this.insertDate = insertDate;
    }
}
