package com.techedge.mp.core.business.interfaces.reconciliation;

import java.io.Serializable;



public class ReconciliationDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8065814682228627470L;
	private String transactionID;
	//private String finalStatusType;
	private String statusCode;
	protected String timestampEndRefuel;
	protected double amount;
    protected String fuelType;
    protected Double fuelQuantity;
    protected String productID;
    protected String productDescription;
	
	
    public String getTimestampEndRefuel() {
		return timestampEndRefuel;
	}
	public void setTimestampEndRefuel(String timestampEndRefuel) {
		this.timestampEndRefuel = timestampEndRefuel;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	public Double getFuelQuantity() {
		return fuelQuantity;
	}
	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
//	public String getFinalStatusType() {
//		return finalStatusType;
//	}
//	public void setFinalStatusType(String finalStatusType) {
//		this.finalStatusType = finalStatusType;
//	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
}
