package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PaymentRefuelHistoryResponse implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5765939485294240233L;
	
	private String statusCode;
	private List<PaymentRefuelDetailResponse> paymentRefuelDetailHistory = new ArrayList<PaymentRefuelDetailResponse>(0);
	private int pageOffset;
	private int pageTotal;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public List<PaymentRefuelDetailResponse> getPaymentRefuelDetailHistory() {
		return paymentRefuelDetailHistory;
	}
	public void setPaymentRefuelDetailHistory(List<PaymentRefuelDetailResponse> paymentRefuelDetailHistory) {
		this.paymentRefuelDetailHistory = paymentRefuelDetailHistory;
	}
	/**
	 * @return the pageOffset
	 */
	public int getPageOffset() {
		return pageOffset;
	}
	/**
	 * @param pageOffset the pageOffset to set
	 */
	public void setPageOffset(int pageOffset) {
		this.pageOffset = pageOffset;
	}
	/**
	 * @return the pageTotal
	 */
	public int getPageTotal() {
		return pageTotal;
	}
	/**
	 * @param pageTotal the pageTotal to set
	 */
	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}
	
	
	

}
