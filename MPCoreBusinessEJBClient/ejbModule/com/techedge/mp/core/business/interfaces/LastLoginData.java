package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.sql.Timestamp;

public class LastLoginData implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -7265442736103785009L;

    private long              id;

    private Timestamp         time;

    private String            deviceId;

    private String            deviceName;

    private String            deviceToken;

    private String            deviceFamily;

    private String            deviceEndpoint;

    private Double            latitude;

    private Double            longitude;

    public LastLoginData() {

    }

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public Timestamp getTime() {

        return time;
    }

    public void setTime(Timestamp time) {

        this.time = time;
    }

    public String getDeviceId() {

        return deviceId;
    }

    public void setDeviceId(String deviceId) {

        this.deviceId = deviceId;
    }

    public String getDeviceName() {

        return deviceName;
    }

    public void setDeviceName(String deviceName) {

        this.deviceName = deviceName;
    }

    public String getDeviceToken() {

        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {

        this.deviceToken = deviceToken;
    }

    public String getDeviceFamily() {

        return deviceFamily;
    }

    public void setDeviceFamily(String deviceFamily) {

        this.deviceFamily = deviceFamily;
    }

    public String getDeviceEndpoint() {

        return deviceEndpoint;
    }

    public void setDeviceEndpoint(String deviceEndpoint) {

        this.deviceEndpoint = deviceEndpoint;
    }

    public Double getLatitude() {

        return latitude;
    }

    public void setLatitude(Double latitude) {

        this.latitude = latitude;
    }

    public Double getLongitude() {

        return longitude;
    }

    public void setLongitude(Double longitude) {

        this.longitude = longitude;
    }
}
