package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

public class ActivityLog implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3668041415773601790L;

	private long id;
	
	private Date timestamp;
	private ErrorLevel level;
	private String source;
	private String groupId;
	private String phaseId;
	private String message;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	public ErrorLevel getLevel() {
		return level;
	}
	public void setLevel(ErrorLevel level) {
		this.level = level;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	public String getPhaseId() {
		return phaseId;
	}
	public void setPhaseId(String phaseId) {
		this.phaseId = phaseId;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public static String createLogMessage(Set<Pair<String, String>> parameters) {
		
		String message = "";
		Boolean firstElement = true;
		
		for( Pair<String, String> parameter: parameters ) {
			
			if ( firstElement ) {
				message = message + parameter;
				firstElement = false;
			}
			else {
				message = message + ", " + parameter;
			}
		}
		
		return message;
	}
}
