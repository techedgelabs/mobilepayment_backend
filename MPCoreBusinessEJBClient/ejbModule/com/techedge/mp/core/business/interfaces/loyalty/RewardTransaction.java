package com.techedge.mp.core.business.interfaces.loyalty;

import java.io.Serializable;
import java.util.HashSet;

public class RewardTransaction implements Serializable {

    /**
     * 
     */
    private static final long                   serialVersionUID = -6947342787701334203L;

    private HashSet<RewardTransactionParameter> parameterDetails = new HashSet<RewardTransactionParameter>(0);

    public RewardTransaction() {

    }

    public HashSet<RewardTransactionParameter> getParameterDetails() {

        return parameterDetails;
    }

    public void setParameterDetails(HashSet<RewardTransactionParameter> parameterDetails) {

        this.parameterDetails = parameterDetails;
    }

}
