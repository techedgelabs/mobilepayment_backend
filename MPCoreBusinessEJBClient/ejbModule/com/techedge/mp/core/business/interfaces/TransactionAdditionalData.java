package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class TransactionAdditionalData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6499754699250114644L;

    private long              id;
    private Transaction       transaction;
    private String            dataKey;
    private String            dataValue;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public Transaction getTransaction() {

        return transaction;
    }

    public void setTransaction(Transaction transaction) {

        this.transaction = transaction;
    }

    public String getDataKey() {
    
        return dataKey;
    }

    public void setDataKey(String dataKey) {
    
        this.dataKey = dataKey;
    }

    public String getDataValue() {
    
        return dataValue;
    }

    public void setDataValue(String dataValue) {
    
        this.dataValue = dataValue;
    }

}
