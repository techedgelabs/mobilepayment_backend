package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class LoadVoucherEventCampaignResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2476875498554550796L;

    private String            statusCode;

    private String            promoName;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getPromoName() {

        return promoName;
    }

    public void setPromoName(String promoName) {

        this.promoName = promoName;
    }

}
