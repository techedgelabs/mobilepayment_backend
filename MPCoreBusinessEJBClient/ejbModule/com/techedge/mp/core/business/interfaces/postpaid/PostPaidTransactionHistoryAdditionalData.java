package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.PostPaidTransactionHistory;

public class PostPaidTransactionHistoryAdditionalData implements Serializable {

    /**
     * 
     */
    private static final long          serialVersionUID = -1490772705196053175L;

    private long                       id;

    private PostPaidTransactionHistory postPaidTransactionHistory;

    private String                     dataKey;

    private String                     dataValue;

    public long getId() {

        return id;
    }
    
    public PostPaidTransactionHistoryAdditionalData() {}
    
    public PostPaidTransactionHistoryAdditionalData(PostPaidTransactionAdditionalData postPaidTransactionAdditionalData) {
        this.dataKey = postPaidTransactionAdditionalData.getDataKey();
        this.dataValue = postPaidTransactionAdditionalData.getDataValue();
    }

    public void setId(long id) {

        this.id = id;
    }

    public PostPaidTransactionHistory getPostPaidTransactionHistory() {

        return postPaidTransactionHistory;
    }

    public void setPostPaidTransactionHistory(PostPaidTransactionHistory postPaidTransactionHistory) {

        this.postPaidTransactionHistory = postPaidTransactionHistory;
    }

    public String getDataKey() {

        return dataKey;
    }

    public void setDataKey(String dataKey) {

        this.dataKey = dataKey;
    }

    public String getDataValue() {

        return dataValue;
    }

    public void setDataValue(String dataValue) {

        this.dataValue = dataValue;
    }

}
