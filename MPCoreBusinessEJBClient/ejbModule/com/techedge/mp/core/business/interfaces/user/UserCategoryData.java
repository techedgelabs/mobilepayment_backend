package com.techedge.mp.core.business.interfaces.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserCategoryData implements Serializable {
    /**
     * 
     */
    private static final long  serialVersionUID = 652312319550486724L;

    private String             statusCode;
    private List<UserCategory> userCategories   = new ArrayList<UserCategory>(0);

    public UserCategoryData() {}

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<UserCategory> getUserCategories() {
        return userCategories;
    }

    public void setUserCategories(List<UserCategory> userCategories) {
        this.userCategories = userCategories;
    }

}
