package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveTermsOfServiceData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9148909536876697169L;
	
	
	private String statusCode;
	private List<DocumentInfo> documents = new ArrayList<DocumentInfo>(0);
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public List<DocumentInfo> getDocuments() {
		return documents;
	}
	public void setDocuments(List<DocumentInfo> documents) {
		this.documents = documents;
	}
	
	
}
