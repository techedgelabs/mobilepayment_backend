package com.techedge.mp.core.business.interfaces.reconciliation;

public enum ReconciliationParkingEnum {
    PAYMENT_ERROR("PAYMENT_ERROR"),
    PAYMENT_DELETE_ERROR("PAYMENT_DELETE_ERROR");

    private String error;

    private ReconciliationParkingEnum(String error) {

        this.error = error;
    }

    public String getError() {

        return error;
    }

    public void setError(String error) {

        this.error = error;
    }
}
