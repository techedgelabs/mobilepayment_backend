package com.techedge.mp.core.business.interfaces.postpaid;

public enum PostPaidRefuelModeType {
    SERVITO("Servito"), FAI_DA_TE("Fai_da_te");

    private final String code;

    PostPaidRefuelModeType(final String code) {
        this.code = code;
    }

    public String getCode() {

        return code;
    }

}
