package com.techedge.mp.core.business.interfaces;

public enum MobilePhoneSendingValidationType {

    SMS("sms"),
    EMAIL("email");
    
    private final String value;

    MobilePhoneSendingValidationType(final String value) {
        this.value = value;
    }

    public String getValue() { 
    
        return value; 
    }

}
