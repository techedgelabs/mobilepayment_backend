package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class GetStationInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7325470663491780733L;
	
	private String stationId;
	private String name;
    private String code;
    private String address;
    private String city;
    private String province;
    private String country;
    private String latitude;
    private String longitude;
    private List<ExtendedPumpInfo> pumpList = new ArrayList<ExtendedPumpInfo>(0);
    private List<CashInfo> cashList = new ArrayList<CashInfo>(0);
    private Double distance;
    private Boolean prepaidActive;
    private Boolean postpaidActive;
    private Boolean shopActive; 
    private Boolean newAcquirerEnabled;
    private Boolean refuelingEnabled;
    private Boolean loyaltyEnabled;
    private Boolean businessEnabled;
    
    public GetStationInfo() {
        super();
    }
    
    public GetStationInfo(String stationId, String address, String city, String province, String country, String latitude, String longitude, Double distance, Boolean prepaidActive, 
            Boolean postpaidActive, Boolean shopActive, Boolean businessEnabled) {
        
        super();
        this.stationId = stationId;
        this.address = address;
        this.city = city;
        this.province = province;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
        this.prepaidActive = prepaidActive;
        this.postpaidActive = postpaidActive;
        this.shopActive = shopActive;
        this.businessEnabled = businessEnabled;
        
    }



    public String getStationId() {
		return stationId;
	}
	public void setStationId(String stationId) {
		this.stationId = stationId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public List<ExtendedPumpInfo> getPumpList() {
		return pumpList;
	}
	public void setPumpList(List<ExtendedPumpInfo> pumpList) {
		this.pumpList = pumpList;
	}
	
	public List<CashInfo> getCashList() {
		return cashList;
	}
	public void setCashList(List<CashInfo> cashList) {
		this.cashList = cashList;
	}
    /**
     * @return the distance
     */
    public Double getDistance() {
        return distance;
    }
    /**
     * @param distance the distance to set
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }
    /**
     * @return the prepaidActive
     */
    public Boolean getPrepaidActive() {
        return prepaidActive;
    }
    /**
     * @param prepaidActive the prepaidActive to set
     */
    public void setPrepaidActive(Boolean prepaidActive) {
        this.prepaidActive = prepaidActive;
    }
    /**
     * @return the postpaidActive
     */
    public Boolean getPostpaidActive() {
        return postpaidActive;
    }
    /**
     * @param postpaidActive the postpaidActive to set
     */
    public void setPostpaidActive(Boolean postpaidActive) {
        this.postpaidActive = postpaidActive;
    }
    /**
     * @return the shopActive
     */
    public Boolean getShopActive() {
        return shopActive;
    }
    /**
     * @param shopActive the shopActive to set
     */
    public void setShopActive(Boolean shopActive) {
        this.shopActive = shopActive;
    }

    public Boolean getNewAcquirerEnabled() {
        if(newAcquirerEnabled == null) {
            return Boolean.TRUE;
        }
        return newAcquirerEnabled;
    }

    public void setNewAcquirerEnabled(Boolean newAcquirerEnabled) {
    
        this.newAcquirerEnabled = newAcquirerEnabled;
    }

    public Boolean getRefuelingEnabled() {
        if(refuelingEnabled == null) {
            return Boolean.TRUE;
        }
        return refuelingEnabled;
    }

    public void setRefuelingEnabled(Boolean refuelingEnabled) {
    
        this.refuelingEnabled = refuelingEnabled;
    }

    public Boolean getLoyaltyEnabled() {
        if(loyaltyEnabled == null) {
            return Boolean.TRUE;
        }
        return loyaltyEnabled;
    }

    public void setLoyaltyEnabled(Boolean loyaltyEnabled) {
    
        this.loyaltyEnabled = loyaltyEnabled;
    }
    
    public Boolean getBusinessEnabled() {

        return businessEnabled;
    }
    
    public void setBusinessEnabled(Boolean businessEnabled) {

        this.businessEnabled = businessEnabled;
    }
}
