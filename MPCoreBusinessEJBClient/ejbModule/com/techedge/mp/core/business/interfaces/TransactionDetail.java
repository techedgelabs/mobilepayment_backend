package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.math.BigDecimal;

public class TransactionDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 836762353111775646L;

    private String            stationID;

    private String            pumpNumber;

    private BigDecimal            amount;

    private BigDecimal            fuelQuantity;

    private String            productCode;

    private String            authorizationCode;

    private String            bankTransactionID;

    public String getStationID() {

        return stationID;
    }

    public void setStationID(String stationID) {

        this.stationID = stationID;
    }

    public String getPumpNumber() {

        return pumpNumber;
    }

    public void setPumpNumber(String pumpNumber) {

        this.pumpNumber = pumpNumber;
    }

    public BigDecimal getAmount() {

        return amount;
    }

    public void setAmount(BigDecimal amount) {

        this.amount = amount;
    }

    public BigDecimal getFuelQuantity() {

        return fuelQuantity;
    }

    public void setFuelQuantity(BigDecimal fuelQuantity) {

        this.fuelQuantity = fuelQuantity;
    }

    public String getProductCode() {

        return productCode;
    }

    public void setProductCode(String productDescription) {

        this.productCode = productDescription;
    }

    public String getAuthorizationCode() {

        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {

        this.authorizationCode = authorizationCode;
    }

    public String getBankTransactionID() {

        return bankTransactionID;
    }

    public void setBankTransactionID(String bankTransactionID) {

        this.bankTransactionID = bankTransactionID;
    }

}
