package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class RemoteSystem implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 8595854638504380052L;
    private RemoteSystemType systemId;
    
    public RemoteSystem() {}
    
    public String getSystemId() {
        return (systemId != null) ? systemId.name() : null;
    }
    
    public RemoteSystemType getTypeSystemId() {
        return systemId;
    }
    
    public void setSystemId(String systemId) {
        this.systemId = RemoteSystemType.valueOf(systemId);
    }
}
