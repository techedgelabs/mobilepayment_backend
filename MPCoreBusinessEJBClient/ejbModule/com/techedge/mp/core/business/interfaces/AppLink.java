package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;

public class AppLink implements Serializable {

    /**
     * 
     */
    private static final long              serialVersionUID = -1856771732338298693L;

    private PushNotificationContentType    type;

    private String                         location;

    private Set<ParameterNotification> parameters = new HashSet<ParameterNotification>();

    public PushNotificationContentType getType() {

        return type;
    }

    public void setType(PushNotificationContentType type) {

        this.type = type;
    }

    public String getLocation() {

        return location;
    }

    public void setLocation(String location) {

        this.location = location;
    }

    public Set<ParameterNotification> getParameters() {

        return parameters;
    }

    public void setParameters(Set<ParameterNotification> parameterNotification) {

        this.parameters = parameterNotification;
    }

}
