package com.techedge.mp.core.business.interfaces.loyalty;

import java.io.Serializable;

import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;

public class RedemptionResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3600331263445417091L;

    private String            statusCode;
    private Integer     checkPinAttemptsLeft;
    private VoucherDetail     voucher;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public VoucherDetail getVoucher() {

        return voucher;
    }

    public void setVoucher(VoucherDetail voucher) {

        this.voucher = voucher;
    }

    public Integer getCheckPinAttemptsLeft() {
    
        return checkPinAttemptsLeft;
    }

    public void setCheckPinAttemptsLeft(Integer checkPinAttemptsLeft) {
    
        this.checkPinAttemptsLeft = checkPinAttemptsLeft;
    }

}
