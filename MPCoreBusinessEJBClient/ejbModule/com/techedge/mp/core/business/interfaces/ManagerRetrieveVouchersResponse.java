package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.postpaid.VoucherHistory;

public class ManagerRetrieveVouchersResponse implements Serializable {

    /**
     * 
     */
    private static final long    serialVersionUID = -2539625981103340368L;

    private String               statusCode;
    private List<VoucherHistory> voucherHistory   = new ArrayList<VoucherHistory>(0);

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<VoucherHistory> getVoucherHistory() {
        return voucherHistory;
    }

    public void setVoucherHistory(List<VoucherHistory> voucherHistory) {
        this.voucherHistory = voucherHistory;
    }

}
