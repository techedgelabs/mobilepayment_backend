package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PaymentMethodResponse implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -1044090420828645828L;

    private String            statusCode;
    private String            redirectURL;
    private PaymentMethod     paymentMethod = new PaymentMethod();

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public PaymentMethod getPaymentMethod() {

        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {

        this.paymentMethod = paymentMethod;
    }

    public String getRedirectURL() {

        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {

        this.redirectURL = redirectURL;
    }

}
