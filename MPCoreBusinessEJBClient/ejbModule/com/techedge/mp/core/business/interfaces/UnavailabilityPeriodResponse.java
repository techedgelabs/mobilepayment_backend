package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.List;

public class UnavailabilityPeriodResponse implements Serializable {

    /**
     * 
     */
    private static final long              serialVersionUID = -56758013374483350L;

    private String                         statusCode;

    private List<UnavailabilityPeriod> unavailabilityPeriodList;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<UnavailabilityPeriod> getUnavailabilityPeriodList() {

        return unavailabilityPeriodList;
    }

    public void setUnavailabilityPeriodList(List<UnavailabilityPeriod> unavailabilityPeriodList) {

        this.unavailabilityPeriodList = unavailabilityPeriodList;
    }

}
