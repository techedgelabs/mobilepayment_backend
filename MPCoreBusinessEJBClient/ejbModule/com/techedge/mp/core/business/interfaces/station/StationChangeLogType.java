package com.techedge.mp.core.business.interfaces.station;

public enum StationChangeLogType {
    Added,
    Changed,
    Deprecated,
    Removed,
    Fixed,
    Security;
}
