package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.List;

public class BrandDataDetailResponse implements Serializable {


    /**
     * 
     */
    private static final long serialVersionUID = -3300583309382867240L;

    private String                statusCode;

    private List<BrandDataDetail> brandDataDetailList;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<BrandDataDetail> getBrandDataDetailList() {

        return brandDataDetailList;
    }

    public void setBrandDataDetailList(List<BrandDataDetail> brandDataDetailList) {

        this.brandDataDetailList = brandDataDetailList;
    }

}
