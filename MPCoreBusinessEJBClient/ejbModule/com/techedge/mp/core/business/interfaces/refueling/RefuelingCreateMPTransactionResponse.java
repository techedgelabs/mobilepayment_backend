package com.techedge.mp.core.business.interfaces.refueling;


public class RefuelingCreateMPTransactionResponse extends StatusMessageCode{

    /**
     * 
     */
    private static final long serialVersionUID = 8130335651076770251L;

    private String            mpTransactionID;
    private String            mpCreationTimestamp;
    private String            mpShopLogin;
    private String            mpAcquirerID;
    private String            token;
    private String            currency;
    
    public String getMpTransactionID() {
    
        return mpTransactionID;
    }
    
    public void setMpTransactionID(String mpTransactionID) {
    
        this.mpTransactionID = mpTransactionID;
    }
    
    public String getMpCreationTimestamp() {
    
        return mpCreationTimestamp;
    }
    
    public void setMpCreationTimestamp(String mpCreationTimestamp) {
    
        this.mpCreationTimestamp = mpCreationTimestamp;
    }

    public String getMpShopLogin() {
    
        return mpShopLogin;
    }

    public void setMpShopLogin(String mpShopLogin) {
    
        this.mpShopLogin = mpShopLogin;
    }

    public String getMpAcquirerID() {
    
        return mpAcquirerID;
    }

    public void setMpAcquirerID(String mpAcquirerID) {
    
        this.mpAcquirerID = mpAcquirerID;
    }

    public String getToken() {
    
        return token;
    }

    public void setToken(String token) {
    
        this.token = token;
    }

    public String getCurrency() {
    
        return currency;
    }

    public void setCurrency(String currency) {
    
        this.currency = currency;
    }
    
}
