package com.techedge.mp.core.business.interfaces;

public class OperationType {

	public final static Integer RETRIEVE_STATION   = 1;
	public final static Integer CREATE_TRANSACTION = 2;
	
	public OperationType() {}
}
