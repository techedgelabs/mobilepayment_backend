package com.techedge.mp.core.business.interfaces;

public enum ErrorLevel {

	TRACE(0),
	DEBUG(1),
	INFO(2),
	WARN(3),
	ERROR(4),
	ADMIN(5),
	OFF(6);
	
	private int level;
	
	private ErrorLevel(int level) {
		this.level = level;
	}
	
	public Boolean isWorseThan(ErrorLevel level) {
		return this.level <= level.level;
	}
	/*
	public int getInt() {
		return this.level;
	}
	*/
	
	public static ErrorLevel buildErrorLevel(int level) {
		
		if(level == 0) {
			return ErrorLevel.TRACE;
		}
		
		if (level == 1) {
			return ErrorLevel.DEBUG;
		}
		
		if (level == 2) {
			return ErrorLevel.INFO;
		}
		
		if (level == 3) {
			return ErrorLevel.WARN;
		}
		
		if (level == 4) {
			return ErrorLevel.ERROR;
		}
		
		if (level == 5) {
			return ErrorLevel.ADMIN;
		}
		
		if (level == 6) {
			return ErrorLevel.OFF;
		}
		
		return null;
	}
	
	public String toString() {
		
		String output = "";;
		
		switch(this) {
		
			case TRACE:
				output = "TRACE";
				break;
				
			case DEBUG:
				output = "DEBUG";
				break;
				
			case INFO:
				output = "INFO";
				break;
			
			case WARN:
				output = "WARN";
				break;
			
			case ERROR:
				output = "ERROR";
				break;
				
			case ADMIN:
				output = "ADMIN";
				break;
				
			case OFF:
				output = "OFF";
				break;

			default:
				break;
		};
	
		return output;
	}
}   