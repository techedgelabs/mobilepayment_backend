package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class CityInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5566369320288679133L;
	
	private String name;
	private String province;
    private String code;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getCode() {
        return code;
    }
	
	public void setCode(String code) {
        this.code = code;
    }
}
