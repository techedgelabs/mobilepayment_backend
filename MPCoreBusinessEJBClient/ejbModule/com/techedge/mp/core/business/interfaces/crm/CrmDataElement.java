package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.regex.Pattern;

public class CrmDataElement implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7598677101265693152L;
    
    private static final String CD_CLIENTE_BROADCAST = "9999999";

    private final String   fieldSeparator = "|";

    private static String[] fieldList      = { "cdCliente", "cdCarta", "Nome", "Cognome", "telCellulare", "email", "sesso", "dataNascita", "msg", "canale", "codIniziativa",
            "codOfferta", "treatmentCode", "flagElab", "codiceFiscale", "c2", "c3", "c4", "c5", "tmsInserimento", "obiettivoPunti", "obiettivoLitri", "c6", "c7", "c8", "c9", "c10" };

    public CrmDataElement() {

    }
    
    public Boolean isBroadcastType() {
        
        if (this.cdCliente.equals(CrmDataElement.CD_CLIENTE_BROADCAST)) {
            return Boolean.TRUE;
        }
        else {
            return Boolean.FALSE;
        }
    }

    public Boolean process(String line) {

        String fields[] = line.split(Pattern.quote(fieldSeparator));
        for (int i = 0; i < fields.length; i++) {

            String fieldName = fieldList[i];
            Field f1;
            try {
                f1 = this.getClass().getField(fieldName);
                f1.set(this, fields[i]);
            }
            catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            catch (SecurityException e) {
                e.printStackTrace();
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return Boolean.TRUE;
    }

    public String cdCliente;

    public String cdCarta;

    public String Nome;

    public String Cognome;

    public String telCellulare;

    public String email;

    public String sesso;

    public String dataNascita;

    public String msg;

    public String canale;

    public String codIniziativa;

    public String codOfferta;

    public String treatmentCode;

    public String flagElab;

    public String codiceFiscale;

    public String c2;

    public String c3;

    public String c4;

    public String c5;

    public String tmsInserimento;

    public String obiettivoPunti;

    public String obiettivoLitri;

    public String c6;

    public String c7;

    public String c8;

    public String c9;

    public String c10;
}
