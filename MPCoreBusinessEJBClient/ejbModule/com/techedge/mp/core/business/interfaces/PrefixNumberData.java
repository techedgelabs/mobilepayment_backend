package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PrefixNumberData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6447166843874440622L;

    private long              id;

    private String            code;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

}
