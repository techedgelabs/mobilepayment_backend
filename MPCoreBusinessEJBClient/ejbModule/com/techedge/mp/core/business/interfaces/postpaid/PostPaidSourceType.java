package com.techedge.mp.core.business.interfaces.postpaid;

public enum PostPaidSourceType {
    SHOP("CASH REGISTER"), SELF("POSTPAID-SELF");

    private final String code;

    PostPaidSourceType(final String code) {
        this.code = code;
    }

    public String getCode() {

        return code;
    }

}
