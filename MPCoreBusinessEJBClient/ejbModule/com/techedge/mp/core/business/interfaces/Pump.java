package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class Pump implements Serializable {

	private static final long serialVersionUID = -2775714661736403620L;
	
	private long id;
	private String pumpID;
	private Station station;
	private String deviceID;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public final String getPumpID() {
		return pumpID;
	}
	public final void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}
	
	public Station getStation() {
		return station;
	}
	public void setStation(Station station) {
		this.station = station;
	}
	
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	
	
}
