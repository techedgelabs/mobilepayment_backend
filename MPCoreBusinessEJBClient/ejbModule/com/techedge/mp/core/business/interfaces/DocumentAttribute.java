package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class DocumentAttribute implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7887646221022732216L;

    private String            checkKey;
    private Boolean           mandatory;
    private Integer           position;
    private String            conditionText;
    private String            extendedConditionText;

    
    public String getCheckKey() {
        return checkKey;
    }

    public void setCheckKey(String checkKey) {
        this.checkKey = checkKey;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getConditionText() {
        return conditionText;
    }

    public void setConditionText(String conditionText) {
        this.conditionText = conditionText;
    }

    public String getExtendedConditionText() {
        return extendedConditionText;
    }

    public void setExtendedConditionText(String extendedConditionText) {
        this.extendedConditionText = extendedConditionText;
    }

}
