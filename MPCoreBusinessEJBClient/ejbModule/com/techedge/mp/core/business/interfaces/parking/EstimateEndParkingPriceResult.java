package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;
import java.math.BigDecimal;

public class EstimateEndParkingPriceResult extends FullParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -555749367763374269L;

    private BigDecimal        currentPrice;
    private BigDecimal        priceDifference;

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public BigDecimal getPriceDifference() {
        return priceDifference;
    }

    public void setPriceDifference(BigDecimal priceDifference) {
        this.priceDifference = priceDifference;
    }
}
