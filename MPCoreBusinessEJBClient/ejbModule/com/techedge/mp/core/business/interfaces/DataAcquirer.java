package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class DataAcquirer implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4728848954590644320L;

    private long              id;
    private String            acquirerID;
    private String            apiKey;
    private String            encodedSecretKey;
    private String            currency;
    private String            groupAcquirer;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getAcquirerID() {

        return acquirerID;
    }

    public void setAcquirerID(String aquiredID) {

        this.acquirerID = aquiredID;
    }

    public String getApiKey() {

        return apiKey;
    }

    public void setApiKey(String apiKey) {

        this.apiKey = apiKey;
    }

    public String getEncodedSecretKey() {

        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {

        this.encodedSecretKey = encodedSecretKey;
    }

    public String getCurrency() {

        return currency;
    }

    public void setCurrency(String currency) {

        this.currency = currency;
    }

    public String getGroupAcquirer() {

        return groupAcquirer;
    }

    public void setGroupAcquirer(String group) {

        this.groupAcquirer = group;
    }

}
