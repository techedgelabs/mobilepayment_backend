package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class PaymentInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6085097250741369596L;
	
	public static final Integer PAYMENTINFO_STATUS_PENDING      = 0;
	public static final Integer PAYMENTINFO_STATUS_NOT_VERIFIED = 1;
	public static final Integer PAYMENTINFO_STATUS_VERIFIED     = 2;
	public static final Integer PAYMENTINFO_STATUS_BLOCKED      = 3;
	public static final Integer PAYMENTINFO_STATUS_ERROR        = 4;
	public static final Integer PAYMENTINFO_STATUS_CANCELED     = 5;
	
	public static final String PAYMENTINFO_TD_LEVEL_FULL     = "FULL";
	public static final String PAYMENTINFO_TD_LEVEL_HALF     = "HALF";
	
	public static final String PAYMENTINFO_TYPE_CREDIT_CARD    = "credit_card";
	public static final String PAYMENTINFO_TYPE_CREDIT_VOUCHER = "credit_voucher";
	public static final String PAYMENTINFO_TYPE_MULTICARD      = "multicard";

	private long id;
	private String type;
	private String brand;
	private Date expirationDate;
	private String pan;
	private String cardBin;
	private Integer status;
	private String message;
	private String token;
	private String token3ds;
	private Boolean defaultMethod;
	private Integer attemptsLeft;
	private Double checkAmount;
	private Date insertTimestamp;
	private Date verifiedTimestamp;
	private String pin;
	private String checkShopLogin;
	private String checkShopTransactionId;
	private String checkBankTransactionID;
	private String checkCurrency;
	private Integer pinCheckAttemptsLeft;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	
	public String getCardBin() {
        return cardBin;
    }
    public void setCardBin(String cardBin) {
        this.cardBin = cardBin;
    }
    
    public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getToken3ds() {
		return token3ds;
	}
	public void setToken3ds(String token3ds) {
		this.token3ds = token3ds;
	}
	
	public Boolean getDefaultMethod() {
		return defaultMethod;
	}
	public void setDefaultMethod(Boolean defaultMethod) {
		this.defaultMethod = defaultMethod;
	}
	
	public Integer getAttemptsLeft() {
		return attemptsLeft;
	}
	public void setAttemptsLeft(Integer attemptsLeft) {
		this.attemptsLeft = attemptsLeft;
	}
	
	public Double getCheckAmount() {
		return checkAmount;
	}
	public void setCheckAmount(Double checkAmount) {
		this.checkAmount = checkAmount;
	}
	
	public Date getInsertTimestamp() {
		return insertTimestamp;
	}
	public void setInsertTimestamp(Date insertTimestamp) {
		this.insertTimestamp = insertTimestamp;
	}
	
	public Date getVerifiedTimestamp() {
		return verifiedTimestamp;
	}
	public void setVerifiedTimestamp(Date verifiedTimestamp) {
		this.verifiedTimestamp = verifiedTimestamp;
	}
	
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	
	public String getCheckShopLogin() {
		return checkShopLogin;
	}
	public void setCheckShopLogin(String checkShopLogin) {
		this.checkShopLogin = checkShopLogin;
	}
	
	public String getCheckShopTransactionId() {
		return checkShopTransactionId;
	}
	public void setCheckShopTransactionId(String checkShopTransactionId) {
		this.checkShopTransactionId = checkShopTransactionId;
	}
	
	public String getCheckBankTransactionID() {
		return checkBankTransactionID;
	}
	public void setCheckBankTransactionID(String checkBankTransactionID) {
		this.checkBankTransactionID = checkBankTransactionID;
	}
	
	public String getCheckCurrency() {
		return checkCurrency;
	}
	public void setCheckCurrency(String checkCurrency) {
		this.checkCurrency = checkCurrency;
	}
	
	public Integer getPinCheckAttemptsLeft() {
		return pinCheckAttemptsLeft;
	}
	public void setPinCheckAttemptsLeft(Integer pinCheckAttemptsLeft) {
		this.pinCheckAttemptsLeft = pinCheckAttemptsLeft;
	}
}
