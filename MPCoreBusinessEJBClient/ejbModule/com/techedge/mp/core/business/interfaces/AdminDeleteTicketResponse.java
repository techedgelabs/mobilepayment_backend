package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class AdminDeleteTicketResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1056828759032400688L;

    private String            statusCode;

    private Integer           deletedTickets;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public Integer getDeletedTickets() {

        return deletedTickets;
    }

    public void setDeletedTickets(Integer deletedTickets) {

        this.deletedTickets = deletedTickets;
    }

}
