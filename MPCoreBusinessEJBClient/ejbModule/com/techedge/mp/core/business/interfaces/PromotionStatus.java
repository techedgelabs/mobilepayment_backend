package com.techedge.mp.core.business.interfaces;

public enum PromotionStatus {
    
    //0 = not active, 1 = active
    INACTIVE(0),
    ACTIVE(1);
    
    private final Integer value;

    PromotionStatus(final Integer newValue) {
        value = newValue;
    }

    public int getValue() { 
    
        return value; 
    }
    
    
}
