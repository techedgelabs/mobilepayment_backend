package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class ManagerStationDetailsResponse  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5867734404241782241L;
    private String statusCode;
    private GetStationInfo stationInfo;

    
    public String getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
    
    public GetStationInfo getStationInfo() {
        return this.stationInfo;
    }
    public void setStationInfo(GetStationInfo stationInfo) {
        this.stationInfo = stationInfo;
    }

}
