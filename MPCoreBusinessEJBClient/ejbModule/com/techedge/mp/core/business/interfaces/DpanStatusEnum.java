package com.techedge.mp.core.business.interfaces;

public enum DpanStatusEnum {
	PRE_ENROLLED,
	ACTIVATED,
	CANCELLED,
	BLOCKED,
	REASSIGNED;
    
}
