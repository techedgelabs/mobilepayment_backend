package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class Device implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1904490510551912490L;

    private String            deviceName;

    private String            deviceID;

    private Date              lastLoginTime;

    private Date              creationTime;

    private Date              associationTime;

    public String getDeviceName() {

        return deviceName;
    }

    public void setDeviceName(String deviceName) {

        this.deviceName = deviceName;
    }

    public String getDeviceID() {

        return deviceID;
    }

    public void setDeviceID(String deviceID) {

        this.deviceID = deviceID;
    }

    public Date getLastLoginTime() {

        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {

        this.lastLoginTime = lastLoginTime;
    }

    public Date getCreationTime() {

        return creationTime;
    }

    public void setCreationTime(Date creationTime) {

        this.creationTime = creationTime;
    }

    public Date getAssociationTime() {

        return associationTime;
    }

    public void setAssociationTime(Date associationTime) {

        this.associationTime = associationTime;
    }

}
