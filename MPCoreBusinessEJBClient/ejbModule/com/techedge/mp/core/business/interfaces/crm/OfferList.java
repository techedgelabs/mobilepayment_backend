package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.util.ArrayList;

public class OfferList implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID  = -3848536964911862781L;

    private String            defaultString;

    private String            interactionPointName;

    private ArrayList<Offer>  recommendedOffers = new ArrayList<Offer>();

    public String getDefaultString() {
        return defaultString;
    }

    public void setDefaultString(String defaultString) {
        this.defaultString = defaultString;
    }

    public String getInteractionPointName() {
        return interactionPointName;
    }

    public void setInteractionPointName(String interactionPointName) {
        this.interactionPointName = interactionPointName;
    }

    public ArrayList<Offer> getRecommendedOffers() {
        return recommendedOffers;
    }

}
