package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class CreateVoucherResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7948123513327340830L;

    
    private String csTransactionID;
    private String statusCode;
    private String messageCode;
    private Voucher voucher;
    
    public String getCsTransactionID() {
    
        return csTransactionID;
    }
    
    public void setCsTransactionID(String csTransactionID) {
    
        this.csTransactionID = csTransactionID;
    }
    
    public String getStatusCode() {
    
        return statusCode;
    }
    
    public void setStatusCode(String statusCode) {
    
        this.statusCode = statusCode;
    }
    
    public String getMessageCode() {
    
        return messageCode;
    }
    
    public void setMessageCode(String messageCode) {
    
        this.messageCode = messageCode;
    }
    
    public Voucher getVoucher() {
    
        return voucher;
    }
    
    public void setVoucher(Voucher voucher) {
    
        this.voucher = voucher;
    }
    
    
    
}
