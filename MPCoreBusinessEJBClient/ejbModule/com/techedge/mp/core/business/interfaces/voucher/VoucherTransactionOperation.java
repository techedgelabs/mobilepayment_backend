package com.techedge.mp.core.business.interfaces.voucher;

import java.io.Serializable;
import java.util.Date;

public class VoucherTransactionOperation implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2338928846249149610L;

    private Long id;
    private Double amount;
    private String csTransactionID;
    private String marketingMsg;
    private String messageCode;
    private String operationID;
    private String operationIDReversed;
    private String operationType;
    private Date requestTimestamp;
    private String statusCode;
    private VoucherTransaction voucherTransaction;
    private String warningMsg;
    
    public Long getId() {
    
        return id;
    }
    
    public void setId(Long id) {
    
        this.id = id;
    }
    
    public Double getAmount() {
    
        return amount;
    }
    
    public void setAmount(Double amount) {
    
        this.amount = amount;
    }
    
    public String getCsTransactionID() {
    
        return csTransactionID;
    }
    
    public void setCsTransactionID(String csTransactionID) {
    
        this.csTransactionID = csTransactionID;
    }
    
    public String getMarketingMsg() {
    
        return marketingMsg;
    }
    
    public void setMarketingMsg(String marketingMsg) {
    
        this.marketingMsg = marketingMsg;
    }
    
    public String getMessageCode() {
    
        return messageCode;
    }
    
    public void setMessageCode(String messageCode) {
    
        this.messageCode = messageCode;
    }
    
    public String getOperationID() {
    
        return operationID;
    }
    
    public void setOperationID(String operationID) {
    
        this.operationID = operationID;
    }
    
    public String getOperationIDReversed() {
    
        return operationIDReversed;
    }
    
    public void setOperationIDReversed(String operationIDReversed) {
    
        this.operationIDReversed = operationIDReversed;
    }
    
    public String getOperationType() {
    
        return operationType;
    }
    
    public void setOperationType(String operationType) {
    
        this.operationType = operationType;
    }
    
    public Date getRequestTimestamp() {
    
        return requestTimestamp;
    }
    
    public void setRequestTimestamp(Date requestTimestamp) {
    
        this.requestTimestamp = requestTimestamp;
    }
    
    public String getStatusCode() {
    
        return statusCode;
    }
    
    public void setStatusCode(String statusCode) {
    
        this.statusCode = statusCode;
    }
    
    public VoucherTransaction getVoucherTransaction() {
    
        return voucherTransaction;
    }
    
    public void setVoucherTransaction(VoucherTransaction voucherTransaction) {
    
        this.voucherTransaction = voucherTransaction;
    }
    
    public String getWarningMsg() {
    
        return warningMsg;
    }
    
    public void setWarningMsg(String warningMsg) {
    
        this.warningMsg = warningMsg;
    }
    
    
}
