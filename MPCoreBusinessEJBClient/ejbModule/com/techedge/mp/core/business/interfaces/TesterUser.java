package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class TesterUser implements Serializable {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = -5882017014010618431L;
	
	private String email;
	private String password;
	private String encryptedPassword;
	private String pin;
	private String encryptedPin;
	private String response;
	private boolean isGenerated;
	private boolean isDeleted;
	
	
    public TesterUser() {}


	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getEncryptedPin() {
		return encryptedPin;
	}
	public void setEncryptedPin(String encryptedPin) {
		this.encryptedPin = encryptedPin;
	}


	public boolean isGenerated() {
		return isGenerated;
	}


	public void setGenerated(boolean isGenerated) {
		this.isGenerated = isGenerated;
	}
	
	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted= isDeleted;
	}


	public String getResponse() {
		return response;
	}


	public void setResponse(String response) {
		this.response = response;
	}

	
}
