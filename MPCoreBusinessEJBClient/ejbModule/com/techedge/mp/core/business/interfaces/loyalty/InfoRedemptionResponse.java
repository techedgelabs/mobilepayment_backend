package com.techedge.mp.core.business.interfaces.loyalty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionDetail;


public class InfoRedemptionResponse implements Serializable {

    /**
     * 
     */
    private static final long      serialVersionUID = -3600331263445417091L;

    private String                 statusCode;

    private List<RedemptionDetail> redemptions      = new ArrayList<RedemptionDetail>();

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<RedemptionDetail> getRedemptions() {

        return redemptions;
    }

    public void setRedemptions(List<RedemptionDetail> redemptions) {

        this.redemptions = redemptions;
    }

}
