package com.techedge.mp.core.business.interfaces.loyalty;


public enum EventNotificationType {
    LOYALTY("LOYALTY"),
    REWARD("REWARD");
    
    private String code;
    
    private EventNotificationType(String code) {
        this.code = code;
    }
    
    public String getName() {
        return name();
    }
    
    public String getValue() {
        return code;
    }
    
    public static EventNotificationType getEventNotification(String value) {
        for (EventNotificationType eventNotificationType : EventNotificationType.values()) {
            if (eventNotificationType.getName().equals(value) || eventNotificationType.getValue().equals(value)) {
                return eventNotificationType;
            }
        }
        
        return null;
    }
     
    
}
