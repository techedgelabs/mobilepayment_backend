package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class CheckTransactionResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5188766285618243003L;

    private String            statusCode;
    private TransactionDetail transationDetail;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public TransactionDetail getTransationDetail() {

        return transationDetail;
    }

    public void setTransationDetail(TransactionDetail transationDetail) {

        this.transationDetail = transationDetail;
    }

}
