package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GetActiveVouchersData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2332352997944515857L;

    private String            statusCode;
    private Double            total;
    private List<Voucher>     vouchers         = new ArrayList<Voucher>(0);
    private List<Voucher>     parkingVouchers  = new ArrayList<Voucher>(0);

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<Voucher> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<Voucher> vouchers) {
        this.vouchers = vouchers;
    }
    
    public List<Voucher> getParkingVouchers() {
        return parkingVouchers;
    }

    public void setParkingVouchers(List<Voucher> parkingVouchers) {
        this.parkingVouchers = parkingVouchers;
    }

    public void sortVouchers() {
        Collections.sort(vouchers, new Comparator<Voucher>() {
            public int compare(Voucher voucher1, Voucher voucher2) {
                return voucher1.getExpirationDate().compareTo(voucher2.getExpirationDate());
            }
        });
        Collections.sort(parkingVouchers, new Comparator<Voucher>() {
            public int compare(Voucher voucher1, Voucher voucher2) {
                return voucher1.getExpirationDate().compareTo(voucher2.getExpirationDate());
            }
        });
    }
}
