package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class CompletedMissionDataDetail implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -2107687150777051501L;

    private String  description;

    private String  code;

    private String  name;

    private String  type;

    private Integer stepCompleted;

    private Integer stepObjective;

    private AppLink appLinkInfo;
    
    public AppLink getAppLinkInfo() {

        return appLinkInfo;
    }

    public void setAppLinkInfo(AppLink appLinkInfo) {

        this.appLinkInfo = appLinkInfo;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public Integer getStepCompleted() {

        return stepCompleted;
    }

    public void setStepCompleted(Integer stepCompleted) {

        this.stepCompleted = stepCompleted;
    }

    public Integer getStepObjective() {

        return stepObjective;
    }

    public void setStepObjective(Integer stepObjective) {

        this.stepObjective = stepObjective;
    }

}
