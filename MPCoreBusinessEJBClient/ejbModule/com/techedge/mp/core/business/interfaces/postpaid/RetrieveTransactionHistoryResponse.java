package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ProductStatisticsInfo;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransaction;

public class RetrieveTransactionHistoryResponse implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -3362497768699869154L;
  private String statusCode;
  private int pageOffset;
  private int pagesTotal;
  private List<ProductStatisticsInfo> statistics = new ArrayList<ProductStatisticsInfo>(0);
  private List<PaymentHistory> paymentHistory = new ArrayList<PaymentHistory>(0);
  private List<ParkingTransaction> parkingHistory = new ArrayList<ParkingTransaction>(0);

  public String getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(String statusCode) {
    this.statusCode = statusCode;
  }

  public int getPageOffset() {
    return pageOffset;
  }

  public void setPageOffset(int pageOffset) {
    this.pageOffset = pageOffset;
  }

  public int getPagesTotal() {
    return pagesTotal;
  }

  public void setPagesTotal(int pagesTotal) {
    this.pagesTotal = pagesTotal;
  }

  public List<ProductStatisticsInfo> getStatistics() {
    return statistics;
  }

  public void setStatistics(List<ProductStatisticsInfo> statistics) {
    this.statistics = statistics;
  }

  public List<PaymentHistory> getPaymentHistory() {
    return paymentHistory;
  }

  public void setPaymentHistory(List<PaymentHistory> paymentHistory) {
    this.paymentHistory = paymentHistory;
  }

  public List<ParkingTransaction> getParkingHistory() {
    return parkingHistory;
  }

  public void setParkingHistory(List<ParkingTransaction> parkingHistory) {
    this.parkingHistory = parkingHistory;
  }

}
