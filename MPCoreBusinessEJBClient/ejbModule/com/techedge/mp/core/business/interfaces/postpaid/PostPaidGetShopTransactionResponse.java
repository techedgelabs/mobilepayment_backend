package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;



public class PostPaidGetShopTransactionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3535296326931310736L;
	
	private String statusCode;
	private String messageCode;
	private String transactionResult;
	private PaymentTransactionStatus paymentTransactionStatus;
	

	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getTransactionResult() {
		return transactionResult;
	}
	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}
	public PaymentTransactionStatus getPaymentTransactionStatus() {
		return paymentTransactionStatus;
	}
	public void setPaymentTransactionStatus(PaymentTransactionStatus paymentTransactionStatus) {
		this.paymentTransactionStatus = paymentTransactionStatus;
	}
	
	
	
	
	
	
}
