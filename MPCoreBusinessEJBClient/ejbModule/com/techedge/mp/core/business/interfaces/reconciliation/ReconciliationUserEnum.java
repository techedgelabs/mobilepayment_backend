package com.techedge.mp.core.business.interfaces.reconciliation;

public enum ReconciliationUserEnum {
    DWH_EVENT_ERROR("DWH_EVENT_ERROR"), PUSH_NOTIFICATION_ERROR("PUSH_NOTIFICATION_ERROR"), CRM_OFFER_ERROR("CRM_OFFER_ERROR"), CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR(
            "CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR"), REDEMPTION_ERROR("REDEMPTION_ERROR");

    private String error;

    private ReconciliationUserEnum(String error) {

        this.error = error;
    }

    public String getError() {

        return error;
    }

    public void setError(String error) {

        this.error = error;
    }
}
