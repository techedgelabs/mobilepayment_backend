package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveLandingResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7450064725510946785L;

    private String            statusCode;

    private List<LandingData> landingPages     = new ArrayList<LandingData>(0);

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<LandingData> getLandingPages() {

        return landingPages;
    }

    public void setLandingPages(List<LandingData> landingPages) {

        this.landingPages = landingPages;
    }

}
