package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StationsAdminData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6151506623043292900L;

	private String statusCode;
	private List<StationAdmin> stationAdminList = new ArrayList<StationAdmin>(0);
	
	
	public StationsAdminData() {
		
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public List<StationAdmin> getStationAdminList() {
		return stationAdminList;
	}

	public void setStationAdminList(List<StationAdmin> stationAdminList) {
		this.stationAdminList = stationAdminList;
	}
	
	
}
