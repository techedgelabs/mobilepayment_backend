package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionAdditionalData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionOperation;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.voucher.VoucherTransaction;

public class PostPaidTransaction implements Serializable {

    /**
     * 
     */
    private static final long                    serialVersionUID                    = -4017713379359854900L;

    private long                                 id;
    private String                               mpTransactionID;
    private User                                 user;
    private Station                              station;
    private String                               source;
    private String                               sourceID;
    private String                               sourceNumber;
    private String                               srcTransactionID;
    private String                               bankTansactionID;
    private String                               authorizationCode;
    private String                               token;
    private String                               paymentType;
    private Double                               amount;
    private String                               currency;
    private String                               productType;
    private Set<PostPaidCart>                    cartList                            = new HashSet<PostPaidCart>(0);
    private Set<PostPaidRefuel>                  refuelList                          = new HashSet<PostPaidRefuel>(0);
    private String                               statusType;
    private String                               mpTransactionStatus;
    private String                               srcTransactionStatus;
    private Date                                 creationTimestamp;
    private Date                                 lastModifyTimestamp;
    private Set<PostPaidTransactionEvent>        postPaidTransactionEventList        = new HashSet<PostPaidTransactionEvent>(0);
    private Set<PostPaidTransactionPaymentEvent> postPaidTransactionPaymentEventList = new HashSet<PostPaidTransactionPaymentEvent>(0);
    private Set<PostPaidTransactionOperation>    postPaidTransactionOperationBeanList = new HashSet<PostPaidTransactionOperation>(0);
    private String                               shopLogin;
    private String                               acquirerID;
    private String                               paymentMode;
    private Long                                 paymentMethodId;
    private String                               paymentMethodType;
    private Boolean                              notificationCreated;
    private Boolean                              notificationPaid;
    private Boolean                              notificationUser;
    private Boolean                              useVoucher;
    private Set<PostPaidConsumeVoucher>          postPaidConsumeVoucherList          = new HashSet<PostPaidConsumeVoucher>(0);
    private Set<PostPaidLoadLoyaltyCredits>      postPaidLoadLoyaltyCreditsList      = new HashSet<PostPaidLoadLoyaltyCredits>(0);
    private Set<PrePaidConsumeVoucher>           prePaidConsumeVoucherList           = new HashSet<PrePaidConsumeVoucher>(0);
    private VoucherTransaction                   voucherTransaction;
    private Boolean                              toReconcile;
    private String                               encodedSecretKey;
    private String                               groupAcquirer;
    private String                               gfgElectronicInvoiceID;
    private TransactionCategoryType              transactionCategory;
    private Set<PostPaidTransactionAdditionalData> postPaidTransactionAdditionalDataList = new HashSet<PostPaidTransactionAdditionalData>(0);
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public String getSourceNumber() {
        return sourceNumber;
    }

    public void setSourceNumber(String sourceNumber) {
        this.sourceNumber = sourceNumber;
    }

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public String getBankTansactionID() {
        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {
        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Set<PostPaidCart> getCartList() {
        return cartList;
    }

    public void setCartList(Set<PostPaidCart> cartList) {
        this.cartList = cartList;
    }

    public Set<PostPaidRefuel> getRefuelList() {
        return refuelList;
    }

    public void setRefuelList(Set<PostPaidRefuel> refuelList) {
        this.refuelList = refuelList;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public String getMpTransactionStatus() {
        return mpTransactionStatus;
    }

    public void setMpTransactionStatus(String mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }

    public String getSrcTransactionStatus() {
        return srcTransactionStatus;
    }

    public void setSrcTransactionStatus(String srcTransactionStatus) {
        this.srcTransactionStatus = srcTransactionStatus;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getLastModifyTimestamp() {
        return lastModifyTimestamp;
    }

    public void setLastModifyTimestamp(Date lastModifyTimestamp) {
        this.lastModifyTimestamp = lastModifyTimestamp;
    }

    public Set<PostPaidTransactionEvent> getPostPaidTransactionEventList() {
        return postPaidTransactionEventList;
    }

    public void setPostPaidTransactionEventList(Set<PostPaidTransactionEvent> postPaidTransactionEventList) {
        this.postPaidTransactionEventList = postPaidTransactionEventList;
    }

    public Set<PostPaidTransactionPaymentEvent> getPostPaidTransactionPaymentEventList() {
        return postPaidTransactionPaymentEventList;
    }

    public void setPostPaidTransactionPaymentEventList(Set<PostPaidTransactionPaymentEvent> postPaidTransactionPaymentEventList) {
        this.postPaidTransactionPaymentEventList = postPaidTransactionPaymentEventList;
    }

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
        this.acquirerID = acquirerID;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public Boolean getNotificationCreated() {
        return notificationCreated;
    }

    public void setNotificationCreated(Boolean notificationCreated) {
        this.notificationCreated = notificationCreated;
    }

    public Boolean getNotificationPaid() {
        return notificationPaid;
    }

    public void setNotificationPaid(Boolean notificationPaid) {
        this.notificationPaid = notificationPaid;
    }

    public Boolean getNotificationUser() {
        return notificationUser;
    }

    public void setNotificationUser(Boolean notificationUser) {
        this.notificationUser = notificationUser;
    }

    public Boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }

    public Set<PostPaidConsumeVoucher> getPostPaidConsumeVoucherList() {
        return postPaidConsumeVoucherList;
    }

    public void setPostPaidConsumeVoucherList(Set<PostPaidConsumeVoucher> postPaidConsumeVoucherList) {
        this.postPaidConsumeVoucherList = postPaidConsumeVoucherList;
    }

    public Set<PostPaidLoadLoyaltyCredits> getPostPaidLoadLoyaltyCreditsList() {
        return postPaidLoadLoyaltyCreditsList;
    }

    public void setPostPaidLoadLoyaltyCreditsList(Set<PostPaidLoadLoyaltyCredits> postPaidLoadLoyaltyCreditsList) {
        this.postPaidLoadLoyaltyCreditsList = postPaidLoadLoyaltyCreditsList;
    }

    public Set<PrePaidConsumeVoucher> getPrePaidConsumeVoucherList() {
        return prePaidConsumeVoucherList;
    }

    public void setPrePaidConsumeVoucherList(Set<PrePaidConsumeVoucher> prePaidConsumeVoucherList) {
        this.prePaidConsumeVoucherList = prePaidConsumeVoucherList;
    }

    public VoucherTransaction getVoucherTransaction() {
        return voucherTransaction;
    }

    public void setVoucherTransaction(VoucherTransaction voucherTransaction) {
        this.voucherTransaction = voucherTransaction;
    }
    
    public Boolean getToReconcile() {
        return toReconcile;
    }

    public void setToReconcile(Boolean toReconcile) {
        this.toReconcile = toReconcile;
    }

    public String getEncodedSecretKey() {

        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {

        this.encodedSecretKey = encodedSecretKey;
    }

    public String getGroupAcquirer() {

        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {

        this.groupAcquirer = groupAcquirer;
    }

    public String getGFGElectronicInvoiceID() {
        return gfgElectronicInvoiceID;
    }
    
    public void setGFGElectronicInvoiceID(String gfgElectronicInvoiceID) {
        this.gfgElectronicInvoiceID = gfgElectronicInvoiceID;
    }
    
    public TransactionCategoryType getTransactionCategory() {

        return transactionCategory;
    }
    
    public void setTransactionCategory(TransactionCategoryType transactionCategory) {

        this.transactionCategory = transactionCategory;
    }

    public Set<PostPaidTransactionAdditionalData> getPostPaidTransactionAdditionalDataList() {
    
        return postPaidTransactionAdditionalDataList;
    }

    public void setPostPaidTransactionAdditionalDataList(Set<PostPaidTransactionAdditionalData> postPaidTransactionAdditionalDataList) {
    
        this.postPaidTransactionAdditionalDataList = postPaidTransactionAdditionalDataList;
    }

    public Set<PostPaidTransactionOperation> getPostPaidTransactionOperationBeanList() {
    
        return postPaidTransactionOperationBeanList;
    }

    public void setPostPaidTransactionOperationBeanList(Set<PostPaidTransactionOperation> postPaidTransactionOperationBeanList) {
    
        this.postPaidTransactionOperationBeanList = postPaidTransactionOperationBeanList;
    }
    
}
