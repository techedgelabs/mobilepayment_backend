package com.techedge.mp.core.business.interfaces.reconciliation;

import java.io.Serializable;
import java.util.List;

public class ReconciliationUserData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8065814682228627470L;
	
	  
    private String reconciliationType;
    private List<String> transactionList;
    public String getReconciliationType() {
    
        return reconciliationType;
    }
    public void setReconciliationType(String reconciliationType) {
    
        this.reconciliationType = reconciliationType;
    }
    public List<String> getTransactionList() {
    
        return transactionList;
    }
    public void setTransactionList(List<String> transactionList) {
    
        this.transactionList = transactionList;
    }
}
