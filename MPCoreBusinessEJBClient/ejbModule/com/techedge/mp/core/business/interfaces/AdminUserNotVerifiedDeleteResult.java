package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AdminUserNotVerifiedDeleteResult implements Serializable {

    /**
     * 
     */
    private static final long                            serialVersionUID = -464665713675669524L;

    private String                                       statusCode;

    private List<AdminUserNotVerifiedDeleteSingleResult> results          = new ArrayList<AdminUserNotVerifiedDeleteSingleResult>(0);

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<AdminUserNotVerifiedDeleteSingleResult> getResults() {

        return results;
    }

    public void setResults(List<AdminUserNotVerifiedDeleteSingleResult> results) {

        this.results = results;
    }

}
