package com.techedge.mp.core.business.interfaces.loyalty;

import java.io.Serializable;

public class LoyaltyTransactionNonOil implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8681135337968878774L;

    private String            productID;

    private String            productDescription;

    private Double            amount;

    private Integer           credits;

    public LoyaltyTransactionNonOil() {

    }

    public String getProductID() {

        return productID;
    }

    public void setProductID(String productID) {

        this.productID = productID;
    }

    public String getProductDescription() {

        return productDescription;
    }

    public void setProductDescription(String productDescription) {

        this.productDescription = productDescription;
    }

    public Double getAmount() {

        return amount;
    }

    public void setAmount(Double amount) {

        this.amount = amount;
    }

    public Integer getCredits() {

        return credits;
    }

    public void setCredits(Integer credits) {

        this.credits = credits;
    }

}
