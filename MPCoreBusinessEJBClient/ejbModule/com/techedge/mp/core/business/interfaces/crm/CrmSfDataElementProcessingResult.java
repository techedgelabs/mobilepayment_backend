package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;

public class CrmSfDataElementProcessingResult implements Serializable {

    private static final long serialVersionUID = 2790170210159411778L;

    private final String fieldSeparator = "|";

    public CrmSfDataElementProcessingResult() {

    }

    public String fiscalCode;

    public String deliveryId;

    public String contactCode;

    public String cardCode;

    public String initiativeCode;

    public String offerCode;

    public String id;

    public String channel;

    public String createdDate;

    @Override
    public String toString() {

        return this.fiscalCode+this.fieldSeparator +this.deliveryId+this.fieldSeparator+this.contactCode+this.fieldSeparator
                +this.initiativeCode+this.fieldSeparator+this.offerCode+this.fieldSeparator+this.id+this.fieldSeparator+this.channel+this.fieldSeparator+this.createdDate;
    }

    
}
