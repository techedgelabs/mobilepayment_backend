package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;




public class PostPaidCartData implements Serializable {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6278951251534726807L;

		
	private String productId;
	private String productDescription;
	private Double amount;
	private Integer quantity;
	
	

	public PostPaidCartData() {}



	public String getProductId() {
		return productId;
	}



	public void setProductId(String productId) {
		this.productId = productId;
	}




	public String getProductDescription() {
		return productDescription;
	}




	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}




	public Double getAmount() {
		return amount;
	}




	public void setAmount(Double amount) {
		this.amount = amount;
	}




	public Integer getQuantity() {
		return quantity;
	}




	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}




	
	
	
	
}
