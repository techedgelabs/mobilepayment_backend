package com.techedge.mp.core.business.interfaces.refueling;


public class RefuelingGetMPTokenResponse extends StatusMessageCode  {

    /**
     * 
     */
    private static final long serialVersionUID = -8242478050699209462L;

    private String            mpToken;

    public String getMpToken() {
        return mpToken;
    }

    public void setMpToken(String mpToken) {
        this.mpToken = mpToken;
    }

}