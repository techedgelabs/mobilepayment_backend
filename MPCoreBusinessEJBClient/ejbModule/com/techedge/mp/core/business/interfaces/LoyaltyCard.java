package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.user.User;

public class LoyaltyCard implements Serializable {

    /**
     * 
     */
    private static final long  serialVersionUID               = 5454566644132950558L;

    public static final String LOYALTY_CARD_STATUS_VALIDA     = "V";
    public static final String LOYALTY_CARD_STATUS_NON_VALIDA = "I";

    private long               id;
    private String             panCode;
    private String             eanCode;
    private Boolean             isVirtual;
    private String             type;
    private String             status;
    private Boolean            defaultCard;
    private User               user;

    public LoyaltyCard() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPanCode() {
        return panCode;
    }

    public void setPanCode(String panCode) {
        this.panCode = panCode;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getDefaultCard() {
        return defaultCard;
    }

    public void setDefaultCard(Boolean defaultCard) {
        this.defaultCard = defaultCard;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getIsVirtual() {
    
        return isVirtual;
    }

    public void setIsVirtual(Boolean isVirtual) {
    
        this.isVirtual = isVirtual;
    }

}
