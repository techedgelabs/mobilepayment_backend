package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;


public class PostPaidTransactionPaymentEventData implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2102297108756602219L;

	private Integer sequence;
	private Integer eventType;
	private String transactionResult;
	private String authorizationCode;
	private String errorCode;
	private String errorDescription;
	private PostPaidTransactionData transactionData;
	
	
	public PostPaidTransactionPaymentEventData() {}


	public Integer getSequence() {
		return sequence;
	}


	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}


	public Integer getEventType() {
		return eventType;
	}


	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}


	public String getTransactionResult() {
		return transactionResult;
	}


	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}


	public String getAuthorizationCode() {
		return authorizationCode;
	}


	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}


	public String getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}


	public String getErrorDescription() {
		return errorDescription;
	}


	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}


	public PostPaidTransactionData getTransactionData() {
		return transactionData;
	}


	public void setTransactionData(PostPaidTransactionData transactionData) {
		this.transactionData = transactionData;
	}
	

	
	
}
