package com.techedge.mp.core.business.interfaces.voucher;

import java.io.Serializable;

public class CreateApplePayVoucherTransactionResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6894336059209710330L;
    private String            statusCode;
    private String            voucherTransactionID;
    private String            voucherCode;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getVoucherTransactionID() {

        return voucherTransactionID;
    }

    public void setVoucherTransactionID(String voucherTransactionID) {

        this.voucherTransactionID = voucherTransactionID;
    }

    public String getVoucherCode() {

        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {

        this.voucherCode = voucherCode;
    }

}
