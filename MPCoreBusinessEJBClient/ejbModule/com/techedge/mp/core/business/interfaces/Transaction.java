package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.user.User;

public class Transaction implements Serializable {

    /**
	 * 
	 */
    private static final long          serialVersionUID          = -5551922862944699328L;

    private long                       id;
    private String                     transactionID;
    private User                       user;
    private Station                    station;
    private Boolean                    useVoucher;
    private Boolean                    voucherReconciliation;
    private Date                       creationTimestamp;
    private Date                       endRefuelTimestamp;
    private Double                     initialAmount;
    private Double                     finalAmount;
    private Double                     fuelQuantity;
    private Double                     fuelAmount;
    private Double                     unitPrice;
    private String                     pumpID;
    private Integer                    pumpNumber;
    private String                     productID;
    private String                     productDescription;
    private String                     currency;
    private String                     shopLogin;
    private String                     acquirerID;
    private String                     bankTansactionID;
    private String                     authorizationCode;
    private String                     token;
    private String                     paymentType;
    private String                     finalStatusType;
    private Boolean                    GFGNotification;
    private Boolean                    confirmed;
    private Set<TransactionStatus>     transactionStatusData     = new HashSet<TransactionStatus>(0);
    private Set<TransactionEvent>      transactionEventData      = new HashSet<TransactionEvent>(0);
    private Long                       paymentMethodId;
    private String                     paymentMethodType;
    private String                     serverName;
    private Integer                    statusAttemptsLeft;
    private String                     outOfRange;
    private String                     refuelMode;
    private Set<PrePaidConsumeVoucher>     prePaidConsumeVoucherList         = new HashSet<PrePaidConsumeVoucher>(0);
    private Set<PrePaidLoadLoyaltyCredits> prePaidLoadLoyaltyCreditsBeanList = new HashSet<PrePaidLoadLoyaltyCredits>(0);
    private String                     srcTransactionID;
    private Boolean                    newPaymentFlow;
    private String                     encodedSecretKey;
    private String                     groupAcquirer;
    private String                     paymentMethodExpiration;
    private Boolean                    loyaltyReconcile;
    private String                     gfgElectronicInvoiceID;
    private Boolean                    business;
    private TransactionCategoryType    transactionCategory;
    private Set<TransactionAdditionalData> transactionAdditionalDataList = new HashSet<TransactionAdditionalData>(0);
    private Set<TransactionOperation>  transactionOperationList = new HashSet<TransactionOperation>(0);
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }

    public Boolean getVoucherReconciliation() {
        return voucherReconciliation;
    }

    public void setVoucherReconciliation(Boolean voucherReconciliation) {
        this.voucherReconciliation = voucherReconciliation;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getEndRefuelTimestamp() {
        return endRefuelTimestamp;
    }

    public void setEndRefuelTimestamp(Date endRefuelTimestamp) {
        this.endRefuelTimestamp = endRefuelTimestamp;
    }

    public Double getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(Double initialAmount) {
        this.initialAmount = initialAmount;
    }

    public Double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(Double finalAmount) {
        this.finalAmount = finalAmount;
    }

    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public Double getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(Double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public Double getUnitPrice() {
    
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
    
        this.unitPrice = unitPrice;
    }

    public String getPumpID() {
        return pumpID;
    }

    public void setPumpID(String pumpID) {
        this.pumpID = pumpID;
    }

    public Integer getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(Integer pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
        this.acquirerID = acquirerID;
    }

    public String getBankTansactionID() {
        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {
        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getFinalStatusType() {
        return finalStatusType;
    }

    public void setFinalStatusType(String finalStatusType) {
        this.finalStatusType = finalStatusType;
    }

    public Boolean getGFGNotification() {
        return GFGNotification;
    }

    public void setGFGNotification(Boolean gFGNotification) {
        GFGNotification = gFGNotification;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Set<TransactionStatus> getTransactionStatusData() {
        return transactionStatusData;
    }

    public void setTransactionStatusData(Set<TransactionStatus> transactionStatusData) {
        this.transactionStatusData = transactionStatusData;
    }

    public Set<TransactionEvent> getTransactionEventData() {
        return transactionEventData;
    }

    public void setTransactionEventData(Set<TransactionEvent> transactionEventData) {
        this.transactionEventData = transactionEventData;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public Integer getStatusAttemptsLeft() {
        return statusAttemptsLeft;
    }

    public void setStatusAttemptsLeft(Integer statusAttemptsLeft) {
        this.statusAttemptsLeft = statusAttemptsLeft;
    }

    public String getOutOfRange() {
        return outOfRange;
    }

    public void setOutOfRange(String outOfRange) {
        this.outOfRange = outOfRange;
    }

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    public Set<PrePaidConsumeVoucher> getPrePaidConsumeVoucherList() {
        return prePaidConsumeVoucherList;
    }

    public void setPrePaidConsumeVoucherList(Set<PrePaidConsumeVoucher> prePaidConsumeVoucherList) {
        this.prePaidConsumeVoucherList = prePaidConsumeVoucherList;
    }

    public String getSrcTransactionID() {
    
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
    
        this.srcTransactionID = srcTransactionID;
    }
    
    public Boolean getNewPaymentFlow() {

        return newPaymentFlow;
    }

    public void setNewPaymentFlow(Boolean newPaymentFlow) {

        this.newPaymentFlow = newPaymentFlow;
    }

    public String getEncodedSecretKey() {
    
        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {
    
        this.encodedSecretKey = encodedSecretKey;
    }

    public String getGroupAcquirer() {
    
        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {
    
        this.groupAcquirer = groupAcquirer;
    }

    public String getPaymentMethodExpiration() {
    
        return paymentMethodExpiration;
    }

    public void setPaymentMethodExpiration(String paymentMethodExpiration) {
    
        this.paymentMethodExpiration = paymentMethodExpiration;
    }

    public Set<PrePaidLoadLoyaltyCredits> getPrePaidLoadLoyaltyCreditsBeanList() {
    
        return prePaidLoadLoyaltyCreditsBeanList;
    }

    public void setPrePaidLoadLoyaltyCreditsBeanList(Set<PrePaidLoadLoyaltyCredits> prePaidLoadLoyaltyCreditsBeanList) {
    
        this.prePaidLoadLoyaltyCreditsBeanList = prePaidLoadLoyaltyCreditsBeanList;
    }

    public Boolean getLoyaltyReconcile() {
    
        return loyaltyReconcile;
    }

    public void setLoyaltyReconcile(Boolean loyaltyReconcile) {
    
        this.loyaltyReconcile = loyaltyReconcile;
    }
    
    public String getGFGElectronicInvoiceID() {

        return gfgElectronicInvoiceID;
    }
    
    public void setGFGElectronicInvoiceID(String gfgElectronicInvoiceID) {

        this.gfgElectronicInvoiceID = gfgElectronicInvoiceID;
    }
    
    public Boolean getBusiness() {

        return business;
    }
    
    public void setBusiness(Boolean business) {

        this.business = business;
    }
    
    public TransactionCategoryType getTransactionCategory() {

        return transactionCategory;
    }
    
    public void setTransactionCategory(TransactionCategoryType transactionCategory) {

        this.transactionCategory = transactionCategory;
    }

    public Set<TransactionAdditionalData> getTransactionAdditionalDataList() {
    
        return transactionAdditionalDataList;
    }

    public void setTransactionAdditionalDataList(Set<TransactionAdditionalData> transactionAdditionalDataList) {
    
        this.transactionAdditionalDataList = transactionAdditionalDataList;
    }

    public Set<TransactionOperation> getTransactionOperationList() {
    
        return transactionOperationList;
    }

    public void setTransactionOperationList(Set<TransactionOperation> transactionOperationList) {
    
        this.transactionOperationList = transactionOperationList;
    }

}
