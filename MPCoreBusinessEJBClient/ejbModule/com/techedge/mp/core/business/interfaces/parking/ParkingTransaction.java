package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.user.User;

public class ParkingTransaction implements Serializable {

    /**
     * 
     */
    private static final long             serialVersionUID             = 9116751849070106257L;

    private long                          id;

    private User                          user;

    private String                        plateNumber;

    private String                        plateNumberDescription;

    private Date                          timestamp;

    private String                        status;

    private String                        parkingZoneId;

    private String                        cityId;

    private String                        cityName;

    private String                        administrativeAreaLevel2Code;

    private String                        parkingZoneName;

    private String                        parkingZoneDescription;

    private String                        parkingZoneNotice;

    private Boolean                       stallCodeRequired;

    private Boolean                       userNotified;

    private String                        parkingTransactionId;

    private String                        parkingId;

    private String                        stallCode;

    private Boolean                       stickerRequired;

    private String                        stickerRequiredNotice;

    private BigDecimal                    finalPrice;

    private Date                          finalParkingEndTime;

    private Date                          estimatedParkingEndTime;

    private Boolean                       toReconcile;

    private Set<ParkingTransactionItem>   parkingTransactionItemList   = new HashSet<ParkingTransactionItem>(0);

    private Set<ParkingTransactionStatus> parkingTransactionStatusList = new HashSet<ParkingTransactionStatus>(0);

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public String getPlateNumber() {

        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {

        this.plateNumber = plateNumber;
    }

    public String getPlateNumberDescription() {

        return plateNumberDescription;
    }

    public void setPlateNumberDescription(String plateNumberDescription) {

        this.plateNumberDescription = plateNumberDescription;
    }

    public Date getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(Date timestamp) {

        this.timestamp = timestamp;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getParkingZoneId() {

        return parkingZoneId;
    }

    public void setParkingZoneId(String parkingZoneId) {

        this.parkingZoneId = parkingZoneId;
    }

    public String getParkingZoneName() {

        return parkingZoneName;
    }

    public void setParkingZoneName(String parkingZoneName) {

        this.parkingZoneName = parkingZoneName;
    }

    public String getParkingZoneDescription() {

        return parkingZoneDescription;
    }

    public void setParkingZoneDescription(String parkingZoneDescription) {

        this.parkingZoneDescription = parkingZoneDescription;
    }

    public String getParkingZoneNotice() {

        return parkingZoneNotice;
    }

    public void setParkingZoneNotice(String parkingZoneNotice) {

        this.parkingZoneNotice = parkingZoneNotice;
    }

    public Boolean getStallCodeRequired() {

        return stallCodeRequired;
    }

    public void setStallCodeRequired(Boolean stallCodeRequired) {

        this.stallCodeRequired = stallCodeRequired;
    }

    public Boolean getUserNotified() {

        return userNotified;
    }

    public void setUserNotified(Boolean userNotified) {

        this.userNotified = userNotified;
    }

    public String getParkingTransactionId() {

        return parkingTransactionId;
    }

    public void setParkingTransactionId(String parkingTransactionId) {

        this.parkingTransactionId = parkingTransactionId;
    }

    public String getParkingId() {

        return parkingId;
    }

    public void setParkingId(String parkingId) {

        this.parkingId = parkingId;
    }

    public String getStallCode() {

        return stallCode;
    }

    public void setStallCode(String stallCode) {

        this.stallCode = stallCode;
    }

    public Set<ParkingTransactionItem> getParkingTransactionItemList() {

        return parkingTransactionItemList;
    }

    public void setParkingTransactionItemList(Set<ParkingTransactionItem> parkingTransactionItemList) {

        this.parkingTransactionItemList = parkingTransactionItemList;
    }

    public String getCityId() {

        return cityId;
    }

    public void setCityId(String cityId) {

        this.cityId = cityId;
    }

    public Boolean getStickerRequired() {

        return stickerRequired;
    }

    public void setStickerRequired(Boolean stickerRequired) {

        this.stickerRequired = stickerRequired;
    }

    public String getStickerRequiredNotice() {

        return stickerRequiredNotice;
    }

    public void setStickerRequiredNotice(String stickerRequiredNotice) {

        this.stickerRequiredNotice = stickerRequiredNotice;
    }

    public BigDecimal getFinalPrice() {

        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {

        this.finalPrice = finalPrice;
    }

    public String getCityName() {

        return cityName;
    }

    public void setCityName(String cityName) {

        this.cityName = cityName;
    }

    public String getAdministrativeAreaLevel2Code() {

        return administrativeAreaLevel2Code;
    }

    public void setAdministrativeAreaLevel2Code(String administrativeAreaLevel2Code) {

        this.administrativeAreaLevel2Code = administrativeAreaLevel2Code;
    }

    public Date getFinalParkingEndTime() {

        return finalParkingEndTime;
    }

    public void setFinalParkingEndTime(Date finalParkingEndTime) {

        this.finalParkingEndTime = finalParkingEndTime;
    }

    public Date getEstimatedParkingEndTime() {

        return estimatedParkingEndTime;
    }

    public void setEstimatedParkingEndTime(Date estimatedParkingEndTime) {

        this.estimatedParkingEndTime = estimatedParkingEndTime;
    }

    public Boolean getToReconcile() {

        return toReconcile;
    }

    public void setToReconcile(Boolean toReconcile) {

        this.toReconcile = toReconcile;
    }

    public Set<ParkingTransactionStatus> getParkingTransactionStatusList() {

        return parkingTransactionStatusList;
    }

    public void setParkingTransactionStatusList(Set<ParkingTransactionStatus> parkingTransactionStatusList) {

        this.parkingTransactionStatusList = parkingTransactionStatusList;
    }

}
