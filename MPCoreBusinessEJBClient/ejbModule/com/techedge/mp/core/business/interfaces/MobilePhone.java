package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MobilePhone implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6085097250741369596L;
	
	public static final Integer MOBILE_PHONE_STATUS_PENDING      = 0;
	public static final Integer MOBILE_PHONE_STATUS_ACTIVE      = 1;
	public static final Integer MOBILE_PHONE_STATUS_CANCELED     = 5;
	
	private long id;
	private String verificationCode;
	private Integer status;
	private String prefix;
	private String number;
	private Date creationTimestamp;
	private Date lastUsedTimestamp;
	private Date expirationTimestamp;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
    
	public String getVerificationCode() {
    
        return verificationCode;
    }
 
    public void setVerificationCode(String verificationCode) {
    
        this.verificationCode = verificationCode;
    }

    public Integer getStatus() {
    
        return status;
    }

    public void setStatus(Integer status) {
    
        this.status = status;
    }

    public String getPrefix() {
    
        return prefix;
    }

    public void setPrefix(String prefix) {
    
        this.prefix = prefix;
    }

    public String getNumber() {
    
        return number;
    }

    public void setNumber(String number) {
    
        this.number = number;
    }

    public Date getCreationTimestamp() {
    
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
    
        this.creationTimestamp = creationTimestamp;
    }

    public Date getLastUsedTimestamp() {
    
        return lastUsedTimestamp;
    }

    public void setLastUsedTimestamp(Date lastUsedTimestamp) {
    
        this.lastUsedTimestamp = lastUsedTimestamp;
    }

    public Date getExpirationTimestamp() {
    
        return expirationTimestamp;
    }

    public void setExpirationTimestamp(Date expirationTimestamp) {
    
        this.expirationTimestamp = expirationTimestamp;
    }

    public static boolean isValidNumber(String value) {
        String regex = "3([\\d]{9})";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        boolean valid = matcher.matches();
        return valid;
    }

    public static boolean isValidPrefix(String value) {
        String regex = "00([\\d]{1,})";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        boolean valid = matcher.matches();
        return valid;
    }

    public static boolean isValidItalianMobilePhone(String value) {
        String regex = "((32[0234789])|(33[0-9])|(34[0-9])|(36[012368])|(38[03589])|(39[01237]))([\\d]{7})";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        boolean valid = matcher.matches();
        return valid;
    }

    

}
