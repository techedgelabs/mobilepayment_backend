package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CategoryBurnDataDetailResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3333251429798269440L;
    private String statusCode;
    private List<CategoryEarnDataDetail> categoryDataDetail     = new ArrayList<CategoryEarnDataDetail>(0);


    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<CategoryEarnDataDetail> getCategoryDataDetail() {
    
        return categoryDataDetail;
    }

    public void setCategoryDataDetail(List<CategoryEarnDataDetail> categoryDataDetail) {
    
        this.categoryDataDetail = categoryDataDetail;
    }

}
