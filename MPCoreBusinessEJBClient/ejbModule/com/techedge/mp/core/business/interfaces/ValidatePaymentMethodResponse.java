package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class ValidatePaymentMethodResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5167267643458178721L;
	
	private String statusCode;
	private Integer attemptsLeft;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public Integer getAttemptsLeft() {
		return attemptsLeft;
	}
	public void setAttemptsLeft(Integer attemptsLeft) {
		this.attemptsLeft = attemptsLeft;
	}
}
