package com.techedge.mp.core.business.interfaces.reconciliation;

import java.io.Serializable;

public class ReconciliationInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8065814682228627470L;
	private String transactionID;
	private String finalStatusType;
	private String statusCode;
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	public String getFinalStatusType() {
		return finalStatusType;
	}
	public void setFinalStatusType(String finalStatusType) {
		this.finalStatusType = finalStatusType;
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
}
