package com.techedge.mp.core.business.interfaces.user;

import java.io.Serializable;
import java.util.Date;

public class UserDevice implements Serializable {

    public static final Integer USER_DEVICE_STATUS_BLOCKED   = 0;

    public static final Integer USER_DEVICE_STATUS_PENDING   = 1;

    public static final Integer USER_DEVICE_STATUS_CANCELLED = 2;

    public static final Integer USER_DEVICE_STATUS_VERIFIED  = 3;

    private static final long   serialVersionUID             = -2786073372012846090L;

    private long                id;

    private String              deviceId;

    private String              deviceName;

    private String              deviceFamily;

    private String              deviceToken;

    private String              deviceEndpoint;

    private Integer             status;

    private User                user;

    private String              verificationCode;

    private Date                creationTimestamp;

    private Date                associationTimestamp;
    
    private Date                lastUsedTimestamp;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getDeviceId() {

        return deviceId;
    }

    public void setDeviceId(String deviceId) {

        this.deviceId = deviceId;
    }

    public String getDeviceName() {

        return deviceName;
    }

    public void setDeviceName(String deviceName) {

        this.deviceName = deviceName;
    }

    public String getDeviceFamily() {

        return deviceFamily;
    }

    public void setDeviceFamily(String deviceFamily) {

        this.deviceFamily = deviceFamily;
    }

    public String getDeviceToken() {

        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {

        this.deviceToken = deviceToken;
    }

    public String getDeviceEndpoint() {

        return deviceEndpoint;
    }

    public void setDeviceEndpoint(String deviceEndpoint) {

        this.deviceEndpoint = deviceEndpoint;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public Integer getStatus() {

        return status;
    }

    public void setStatus(Integer status) {

        this.status = status;
    }

    public String getVerificationCode() {

        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {

        this.verificationCode = verificationCode;
    }

    public Date getCreationTimestamp() {

        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {

        this.creationTimestamp = creationTimestamp;
    }

    public Date getAssociationTimestamp() {

        return associationTimestamp;
    }

    public void setAssociationTimestamp(Date associationTimestamp) {

        this.associationTimestamp = associationTimestamp;
    }

    public Date getLastUsedTimestamp() {
    
        return lastUsedTimestamp;
    }

    public void setLastUsedTimestamp(Date lastUsedTimestamp) {
    
        this.lastUsedTimestamp = lastUsedTimestamp;
    }

}
