package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ParkingCityZoneData implements Serializable {

    private static final long serialVersionUID = 1L;

    private ParkingCity       parkingCity;

    public ParkingCity getParkingCity() {

        return parkingCity;
    }

    public void setParkingCity(ParkingCity parkingCity) {

        this.parkingCity = parkingCity;
    }

    private List<ParkingZone> parkingZoneList = new ArrayList<ParkingZone>(0);

    public List<ParkingZone> getParkingZoneList() {

        return parkingZoneList;
    }

    public void setParkingZoneList(List<ParkingZone> parkingZoneList) {

        this.parkingZoneList = parkingZoneList;
    }

}
