package com.techedge.mp.core.business.interfaces.postpaid;

public class CoreGetLastRefuelMessageResponse {
	
	//TODO
	private String statusCode;
	private String messageCode;
	
	
	private String srcTransactionID;
	private String pumpID;
	private String pumpNumber;
	private String refuelMode;
	private CoreStationDetail stationDetail;
	private CoreRefuelDetails refuelDetail;
	
	
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public String getSrcTransactionID() {
		return srcTransactionID;
	}
	public void setSrcTransactionID(String srcTransactionID) {
		this.srcTransactionID = srcTransactionID;
	}
	public String getPumpID() {
		return pumpID;
	}
	public void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}
	public String getPumpNumber() {
		return pumpNumber;
	}
	public void setPumpNumber(String pumpNumber) {
		this.pumpNumber = pumpNumber;
	}
	public String getRefuelMode() {
		return refuelMode;
	}
	public void setRefuelMode(String refuelMode) {
		this.refuelMode = refuelMode;
	}
	public CoreStationDetail getStationDetail() {
		return stationDetail;
	}
	public void setStationDetail(CoreStationDetail stationDetail) {
		this.stationDetail = stationDetail;
	}
	public CoreRefuelDetails getRefuelDetail() {
		return refuelDetail;
	}
	public void setRefuelDetail(CoreRefuelDetails refuelDetail) {
		this.refuelDetail = refuelDetail;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	
}
