package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PaymentDataResponse implements Serializable {

    private static final long     serialVersionUID  = 180719082021491605L;

    private List<PaymentDataList> paymentMethodList = new ArrayList<>(0);

    private String                statusCode;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<PaymentDataList> getPaymentMethodList() {

        return paymentMethodList;
    }

    public void setPaymentMethodList(List<PaymentDataList> paymentMethodList) {

        this.paymentMethodList = paymentMethodList;
    }

}
