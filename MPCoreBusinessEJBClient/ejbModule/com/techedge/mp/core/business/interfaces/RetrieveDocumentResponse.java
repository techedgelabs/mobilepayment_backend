package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.List;

public class RetrieveDocumentResponse implements Serializable {

    /**
     * 
     */
    private static final long  serialVersionUID = -3279141551455765768L;

    private String             statusCode;
    private List<DocumentData> listDocument;

    
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<DocumentData> getListDocument() {
        return listDocument;
    }

    public void setListDocument(List<DocumentData> listDocument) {
        this.listDocument = listDocument;
    }

}
