package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class CreateUserGuestResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5580454906646450559L;

    private String            statusCode;

    private String            username;

    private String            encodedPassword;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getEncodedPassword() {

        return encodedPassword;
    }

    public void setEncodedPassword(String encodedPassword) {

        this.encodedPassword = encodedPassword;
    }

}
