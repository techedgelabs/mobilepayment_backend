package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;

public class PaymentHistory implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1313176272167935806L;

  private String sourceType;
  private String sourceStatus;
  private PostPaidTransactionInfo transaction;

  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  public String getSourceStatus() {
    return sourceStatus;
  }

  public void setSourceStatus(String sourceStatus) {
    this.sourceStatus = sourceStatus;
  }

  public PostPaidTransactionInfo getTransaction() {
    return transaction;
  }

  public void setTransaction(PostPaidTransactionInfo transaction) {
    this.transaction = transaction;
  }

}
