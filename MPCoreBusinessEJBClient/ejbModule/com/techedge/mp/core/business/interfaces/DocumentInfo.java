package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.List;

public class DocumentInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 298475837425169414L;
	
	private Integer position;
	private String title;
	private String subTitle;
	private String documentKey;
	private List<DocumentCheckInfo>  checkList;
	
	
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubTitle() {
        return subTitle;
    }
    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
    public String getDocumentKey() {
		return documentKey;
	}
	public void setDocumentKey(String documentKey) {
		this.documentKey = documentKey;
	}
	public List<DocumentCheckInfo> getCheckList() {
		return checkList;
	}
	public void setCheckList(List<DocumentCheckInfo> checkList) {
		this.checkList = checkList;
	}
	
		
}
