package com.techedge.mp.core.business.interfaces;

public class ConstantsHelper {

    public final static String TRANSACTION_SOURCE_TYPE_SHOP    = "SHOP";
    public final static String TRANSACTION_SOURCE_TYPE_FUEL    = "FUEL";
    public final static String TRANSACTION_SOURCE_TYPE_VOUCHER = "VOUCHER";
    public final static String TRANSACTION_SOURCE_TYPE_PARKING = "PARKING";
    
    public final static String TRANSACTION_SOURCE_STATUS_POST_PAY = "POST-PAY";
    public final static String TRANSACTION_SOURCE_STATUS_PRE_PAY  = "PRE-PAY";
    public final static String TRANSACTION_SOURCE_STATUS_VOUCHER  = "";

	public ConstantsHelper() {
	}
}
