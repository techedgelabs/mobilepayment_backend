package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class LastLoginErrorData implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -7265442736103785009L;

  private long id;
  // private String userId;
  private Date time;
  private Integer attempt;

  public LastLoginErrorData() {}

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  // public String getUserId() {
  // return userId;
  // }
  //
  // public void setUserId(String userId) {
  // this.userId = userId;
  // }

  public Integer getAttempt() {
    return attempt;
  }

  public void setAttempt(Integer attempt) {
    this.attempt = attempt;
  }

}
