package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

public class BaseParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -919460419132578598L;

    private String            statusCode;

    private String            parkingZoneId;

    private String            parkingZoneName;

    private String            parkingZoneDescription;
    
    private String            cityId;
    
    private String            cityName;
    
    private String            administrativeAreaLevel2Code;

    private String            stallCode;

    private String            plateNumber;
    
    private String            plateNumberDescription;

    private Date              parkingStartTime;

    private Date              parkingEndTime;

    private BigDecimal        price;

    private List<String>      notice = new ArrayList<String>(0);

    private String            transactionId;
    
    private Long              paymentMethodId;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getParkingZoneId() {

        return parkingZoneId;
    }

    public void setParkingZoneId(String parkingZoneId) {

        this.parkingZoneId = parkingZoneId;
    }

    public String getStallCode() {

        return stallCode;
    }

    public void setStallCode(String stallCode) {

        this.stallCode = stallCode;
    }

    public String getPlateNumber() {

        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {

        this.plateNumber = plateNumber;
    }

    public Date getParkingStartTime() {

        return parkingStartTime;
    }

    public void setParkingStartTime(Date parkingStartTime) {

        this.parkingStartTime = parkingStartTime;
    }

    public Date getParkingEndTime() {

        return parkingEndTime;
    }

    public void setParkingEndTime(Date parkingEndTime) {

        this.parkingEndTime = parkingEndTime;
    }

    public BigDecimal getPrice() {

        return price;
    }

    public void setPrice(BigDecimal price) {

        this.price = price;
    }

    public List<String> getNotice() {

        return notice;
    }

    public void setNotice(List<String> notice) {

        this.notice = notice;
    }

    public String getTransactionId() {

        return transactionId;
    }

    public void setTransactionId(String transactionId) {

        this.transactionId = transactionId;
    }

    public String getParkingZoneName() {

        return parkingZoneName;
    }

    public void setParkingZoneName(String parkingZoneName) {

        this.parkingZoneName = parkingZoneName;
    }

    public String getParkingZoneDescription() {

        return parkingZoneDescription;
    }

    public void setParkingZoneDescription(String parkingZoneDescription) {

        this.parkingZoneDescription = parkingZoneDescription;
    }

    public String getPlateNumberDescription() {
    
        return plateNumberDescription;
    }

    public void setPlateNumberDescription(String plateNumberDescription) {
    
        this.plateNumberDescription = plateNumberDescription;
    }

    public Long getPaymentMethodId() {
    
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
    
        this.paymentMethodId = paymentMethodId;
    }

    public String getCityId() {
    
        return cityId;
    }

    public void setCityId(String cityId) {
    
        this.cityId = cityId;
    }

    public String getCityName() {
    
        return cityName;
    }

    public void setCityName(String cityName) {
    
        this.cityName = cityName;
    }

    public String getAdministrativeAreaLevel2Code() {
    
        return administrativeAreaLevel2Code;
    }

    public void setAdministrativeAreaLevel2Code(String administrativeAreaLevel2Code) {
    
        this.administrativeAreaLevel2Code = administrativeAreaLevel2Code;
    }

}
