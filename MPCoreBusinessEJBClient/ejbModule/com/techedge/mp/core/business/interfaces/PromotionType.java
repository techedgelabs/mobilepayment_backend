package com.techedge.mp.core.business.interfaces;

public enum PromotionType {
    //000001 = WELCOME, 000002 = MASTERCARD
    WELCOME("000001", "WEMP"),
    MASTERCARD("000002", "MAST"),
    MAKERFAIRE2017("000003", "MF"),
    MOTORSHOW2017("000004", "MS");
    
    private final String promoCode;
    private final String voucherCode;

    PromotionType(final String promoCode, final String voucherCode) {
        this.promoCode = promoCode;
        this.voucherCode = voucherCode;
    }

    public String getPromoCode() {
        return promoCode; 
    }
    
    public String getVoucherCode() {
        return voucherCode; 
    }

}
