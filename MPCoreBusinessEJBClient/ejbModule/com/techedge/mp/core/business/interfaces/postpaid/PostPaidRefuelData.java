package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;



public class PostPaidRefuelData implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6017377613241482027L;
	
	private String fuelType;
	private String productId;
	private String productDescription;
	private Double fuelQuantity;
	private String pumpId;
	private Integer pumpNumber;
	private Double fuelAmount;
	private PostPaidTransactionData transactionData;
	
	
	public PostPaidRefuelData() {}


	public String getFuelType() {
		return fuelType;
	}


	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}




	public String getProductId() {
		return productId;
	}




	public void setProductId(String productId) {
		this.productId = productId;
	}




	public String getProductDescription() {
		return productDescription;
	}




	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}




	public Double getFuelQuantity() {
		return fuelQuantity;
	}




	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}




	public PostPaidTransactionData getTransactionData() {
		return transactionData;
	}




	public void setTransactionData(PostPaidTransactionData transactionData) {
		this.transactionData = transactionData;
	}


	public String getPumpId() {
		return pumpId;
	}


	public void setPumpId(String pumpId) {
		this.pumpId = pumpId;
	}


	public Integer getPumpNumber() {
        return pumpNumber;
    }


    public void setPumpNumber(Integer pumpNumber) {
        this.pumpNumber = pumpNumber;
    }


    public Double getFuelAmount() {
		return fuelAmount;
	}


	public void setFuelAmount(Double fuelAmount) {
		this.fuelAmount = fuelAmount;
	}
	
	
	
	
	
}
