package com.techedge.mp.core.business.interfaces;

public enum SmsStatusType {

    DELIVERED(0),
    DELETED(1),
    PENDING(2),
    ERROR(3);
    
    private final Integer code;

    SmsStatusType(final Integer code) {
        this.code = code;
    }

    public Integer getCode() { 
    
        return code; 
    }
    

}
