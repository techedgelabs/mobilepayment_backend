package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PostPaidConsumeVoucherData implements Serializable {

    /**
     * 
     */
    private static final long                      serialVersionUID               = -3176537978660205570L;

    private String                                 csTransactionID;
    private String                                 operationID;
    private String                                 operationType;
    private Long                                   requestTimestamp;
    private String                                 marketingMsg;
    private String                                 messageCode;
    private String                                 statusCode;
    private Double                                 totalConsumed;
    private PostPaidTransactionData                transactionData;
    private List<PostPaidConsumeVoucherDetailData> PostPaidConsumeVoucherDataList = new ArrayList<PostPaidConsumeVoucherDetailData>(0);

    public PostPaidConsumeVoucherData() {}

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotalConsumed() {
        return totalConsumed;
    }

    public void setTotalConsumed(Double totalConsumed) {
        this.totalConsumed = totalConsumed;
    }

    public PostPaidTransactionData getTransactionData() {
        return transactionData;
    }

    public void setTransactionData(PostPaidTransactionData transactionData) {
        this.transactionData = transactionData;
    }

    public List<PostPaidConsumeVoucherDetailData> getPostPaidConsumeVoucherDataList() {
        return PostPaidConsumeVoucherDataList;
    }

    public void setPostPaidConsumeVoucherDataList(List<PostPaidConsumeVoucherDetailData> postPaidConsumeVoucherDataList) {
        PostPaidConsumeVoucherDataList = postPaidConsumeVoucherDataList;
    }

}
