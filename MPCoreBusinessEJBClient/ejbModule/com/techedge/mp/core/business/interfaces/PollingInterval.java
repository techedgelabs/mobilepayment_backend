package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PollingInterval implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 652312319550486724L;

    private Long id;
    
    private String            status;

    private Integer           nextPollingInterval;

    public PollingInterval() {

    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public Integer getNextPollingInterval() {

        return nextPollingInterval;
    }

    public void setNextPollingInterval(Integer nextPollingInterval) {

        this.nextPollingInterval = nextPollingInterval;
    }

    public Long getId() {
    
        return id;
    }

    public void setId(Long id) {
    
        this.id = id;
    }

}
