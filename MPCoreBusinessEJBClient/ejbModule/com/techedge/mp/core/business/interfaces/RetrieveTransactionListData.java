package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveTransactionListData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3307851412746064243L;

	private String statusCode;
	private List<TransactionHistory> transactionHistoryList = new ArrayList<TransactionHistory>(0);
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public List<TransactionHistory> getTransactionHistoryList() {
		return transactionHistoryList;
	}
	public void setTransactionHistoryList(List<TransactionHistory> transactionHistoryList) {
		this.transactionHistoryList = transactionHistoryList;
	}
	
	
}
