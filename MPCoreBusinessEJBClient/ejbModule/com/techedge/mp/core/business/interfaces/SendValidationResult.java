package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class SendValidationResult implements Serializable {

    
    /**
     * 
     */
    private static final long serialVersionUID = 2692670888649332368L;
    private String statusCode;
    private String messageCode;
    private Integer errorCode;
    
    public String getStatusCode() {
    
        return statusCode;
    }
    
    public void setStatusCode(String statusCode) {
    
        this.statusCode = statusCode;
    }
    
    public String getMessageCode() {
    
        return messageCode;
    }
    
    public void setMessageCode(String messageCode) {
    
        this.messageCode = messageCode;
    }

    public Integer getErrorCode() {

        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {

        this.errorCode = errorCode;
    }
    
    
}
