package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.List;

public class DocumentData implements Serializable {

    /**
     * 
     */
    private static final long       serialVersionUID = 6813268136842713834L;

    private String                  documentKey;

    private Integer                 position;

    private String                  templateFile;

    private String                  title;

    private String                  subtitle;
    
    private List<DocumentAttribute> attributes;

    private String                  userCategory;
    
    private String                  groupCategory;

    public String getDocumentKey() {

        return documentKey;
    }

    public void setDocumentKey(String documentKey) {

        this.documentKey = documentKey;
    }

    public Integer getPosition() {

        return position;
    }

    public void setPosition(Integer position) {

        this.position = position;
    }

    public String getTemplateFile() {

        return templateFile;
    }

    public void setTemplateFile(String templateFile) {

        this.templateFile = templateFile;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public List<DocumentAttribute> getAttributes() {

        return attributes;
    }

    public void setAttributes(List<DocumentAttribute> attributes) {

        this.attributes = attributes;
    }

    public String getUserCategory() {

        return userCategory;
    }

    public void setUserCategory(String userCategory) {

        this.userCategory = userCategory;
    }

    public String getSubtitle() {
    
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
    
        this.subtitle = subtitle;
    }

    public String getGroupCategory() {
    
        return groupCategory;
    }

    public void setGroupCategory(String groupCategory) {
    
        this.groupCategory = groupCategory;
    }

}
