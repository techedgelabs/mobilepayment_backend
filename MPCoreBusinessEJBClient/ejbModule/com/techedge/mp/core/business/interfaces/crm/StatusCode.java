package com.techedge.mp.core.business.interfaces.crm;


public enum StatusCode {
    SUCCESS(0),
    WARNING(1),
    ERROR(2);
    
    private int code;
    
    private StatusCode(int code) {
        this.code = code;
    }
    
    public static StatusCode valueOf(int value) {
        switch (value) {
            case 0:
                return StatusCode.SUCCESS;

            case 1:
                return StatusCode.WARNING;

            case 2:
                return StatusCode.ERROR;

            default:
                return null;
        }
    }
    
    public String getName() {
        return this.name();
    }
    
    public int getValue() {
        return code;
    }
    
    
}
