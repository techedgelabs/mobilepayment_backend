package com.techedge.mp.core.business.interfaces.reconciliation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.techedge.mp.core.business.interfaces.StatusHelper;

public class ReconciliationUserSummary implements Serializable {
        
    /**
     * 
     */
    private static final long serialVersionUID = -5727339930484232860L;
    
    public static final String SUMMARY_DWH_EVENT_ERROR = StatusHelper.DWH_EVENT_STATUS_ERROR;
    public static final String SUMMARY_PUSH_NOTIFICATION_ERROR = StatusHelper.PUSH_NOTIFICATION_STATUS_ERROR;
    public static final String SUMMARY_CRM_OFFER_ERROR = StatusHelper.CRM_EVENT_STATUS_ERROR;
    public static final String SUMMARY_CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR = StatusHelper.CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR;
    public static final String SUMMARY_REDEMPTION_ERROR = StatusHelper.REDEMPTION_STATUS_ERROR;
    public static final String SUMMARY_REFUELING_SUBSCRIPTION_ERROR = StatusHelper.REFUELING_SUBSCRIPTION_ERROR;
    
    
    public static final String SUMMARY_STATUS_NOT_IDENTIFIED = "STATUS_NOT_IDENTIFIED";
    
    
    @SuppressWarnings("serial")
    HashMap<String, List<ReconciliationInfo>> summary = new HashMap<String, List<ReconciliationInfo>>() {
        {
            put(SUMMARY_DWH_EVENT_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_PUSH_NOTIFICATION_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_CRM_OFFER_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_REDEMPTION_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_REFUELING_SUBSCRIPTION_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_STATUS_NOT_IDENTIFIED, new ArrayList<ReconciliationInfo>(0));
        }
    };
    
    String statusCode;
    
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<ReconciliationInfo> getSummary(String type) {
        if (!summary.containsKey(type)) {
            return new ArrayList<ReconciliationInfo>(0);
        }
        
        return summary.get(type);
    }

    public boolean addToSummary(String type, ReconciliationInfo reconciliationInfo) {
        List<ReconciliationInfo> reconciliationInfoList;
        
        if (!summary.containsKey(type)) {
            return false;
        }
        
        reconciliationInfoList = summary.get(type);
        reconciliationInfoList.add(reconciliationInfo);
        summary.put(type, reconciliationInfoList);
        return true;
    }
    
    public Integer getProcessed() {
        return new Integer(summary.get(SUMMARY_DWH_EVENT_ERROR).size() +
                summary.get(SUMMARY_PUSH_NOTIFICATION_ERROR).size() +
                summary.get(SUMMARY_CRM_OFFER_ERROR).size() +
                summary.get(SUMMARY_CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR).size() +
                summary.get(SUMMARY_REDEMPTION_ERROR).size() +
                summary.get(SUMMARY_REFUELING_SUBSCRIPTION_ERROR).size() +
                summary.get(SUMMARY_STATUS_NOT_IDENTIFIED).size());
    }
}
