package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveUserDeviceData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1362829210069339152L;

    private String            statusCode;

    private List<Device>    deviceList   = new ArrayList<>(0);

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<Device> getDeviceList() {

        return deviceList;
    }

    public void setUserDeviceList(List<Device> userDeviceList) {

        this.deviceList = userDeviceList;
    }

}
