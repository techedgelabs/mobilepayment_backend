package com.techedge.mp.core.business.interfaces.crm;

public enum StatusCodeEnum {

    SUCCESS, INVALID_PROMO_CODE, INVALID_AMOUNT, USER_NOT_FOUND, SYSTEM_ERROR, INVALID_INPUT

}
