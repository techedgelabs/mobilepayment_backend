package com.techedge.mp.core.business.interfaces;

public class RequestHelper {

	public final static Integer MANAGE_PAYMENT_REQUEST_TYPE_CREATE = 0;
	public final static Integer MANAGE_PAYMENT_REQUEST_TYPE_UPDATE = 1;
	public final static Integer MANAGE_PAYMENT_REQUEST_TYPE_CANCEL = 2;
	
	public RequestHelper() {}
}
