package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class Station implements Serializable {

    /**
	 * 
	 */
    private static final long   serialVersionUID          = 431603150021927616L;

    public static final Integer STATION_STATUS_NOT_ACTIVE = 0;
    public static final Integer STATION_STATUS_INVISIBLE  = 1;
    public static final Integer STATION_STATUS_ACTIVE     = 2;

    private long                id;
    private String              stationID;
    private String              beaconCode;
    private String              address;
    private String              city;
    private String              country;
    private String              province;
    private Double              latitude;
    private Double              longitude;
    private String              oilShopLogin;
    private String              oilAcquirerID;
    private String              noOilShopLogin;
    private String              noOilAcquirerID;
    private Integer             stationStatus;
    private Boolean             prepaidActive;
    private Boolean             postpaidActive;
    private Boolean             shopActive;            
    private DataAcquirer        dataAcquirer;            
    protected Boolean           newAcquirerActive;
    protected Boolean           refuelingActive;
    protected Boolean           loyaltyActive;
    protected Boolean           voucherActive;
    protected Boolean           businessActive;

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getBeaconCode() {
        return beaconCode;
    }

    public void setBeaconCode(String beaconCode) {
        this.beaconCode = beaconCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getOilShopLogin() {
        return oilShopLogin;
    }

    public void setOilShopLogin(String oilShopLogin) {
        this.oilShopLogin = oilShopLogin;
    }

    public String getOilAcquirerID() {
        return oilAcquirerID;
    }

    public void setOilAcquirerID(String oilAcquirerID) {
        this.oilAcquirerID = oilAcquirerID;
    }

    public String getNoOilShopLogin() {
        return noOilShopLogin;
    }

    public void setNoOilShopLogin(String noOilShopLogin) {
        this.noOilShopLogin = noOilShopLogin;
    }

    public String getNoOilAcquirerID() {
        return noOilAcquirerID;
    }

    public void setNoOilAcquirerID(String noOilAcquirerID) {
        this.noOilAcquirerID = noOilAcquirerID;
    }

    public Integer getStationStatus() {
        return stationStatus;
    }

    public void setStationStatus(Integer stationStatus) {
        this.stationStatus = stationStatus;
    }

    public Boolean getPrepaidActive() {
        return prepaidActive;
    }

    public void setPrepaidActive(Boolean prepaidActive) {
        this.prepaidActive = prepaidActive;
    }

    public Boolean getPostpaidActive() {
        return postpaidActive;
    }

    public void setPostpaidActive(Boolean postpaidActive) {
        this.postpaidActive = postpaidActive;
    }

    public Boolean getShopActive() {
        return shopActive;
    }

    public void setShopActive(Boolean shopActive) {
        this.shopActive = shopActive;
    }

    public DataAcquirer getDataAcquirer() {
    
        return dataAcquirer;
    }

    public void setDataAcquirer(DataAcquirer dataAcquire) {
    
        this.dataAcquirer = dataAcquire;
    }

    public Boolean getNewAcquirerActive() {
    
        if (newAcquirerActive == null) {
            newAcquirerActive = Boolean.TRUE;
        }
        
        return newAcquirerActive;
    }

    public void setNewAcquirerActive(Boolean newAcquirerActive) {
    
        this.newAcquirerActive = newAcquirerActive;
    }

    public Boolean getRefuelingActive() {
    
        if (refuelingActive == null) {
            refuelingActive = Boolean.TRUE;
        }
        
        return refuelingActive;
    }

    public void setRefuelingActive(Boolean refuelingActive) {
    
        this.refuelingActive = refuelingActive;
    }

    public Boolean getLoyaltyActive() {
    
        if (loyaltyActive == null) {
            loyaltyActive = Boolean.TRUE;
        }

        return loyaltyActive;
    }

    public void setLoyaltyActive(Boolean loyaltyActive) {
    
        this.loyaltyActive = loyaltyActive;
    }
    
    public Boolean getVoucherActive() {
        /*
        if (voucherActive == null) {
            voucherActive = Boolean.TRUE;
        }
        */
        return voucherActive;
    }
    
    public void setVoucherActive(Boolean voucherActive) {
        
        this.voucherActive = voucherActive;
    }

    public Boolean getBusinessActive() {
        /*
        if (businessActive == null) {
            businessActive = Boolean.TRUE;
        }
        */
        return businessActive;
    }
    
    public void setBusinessActive(Boolean businessActive) {

        this.businessActive = businessActive;
    }
}
