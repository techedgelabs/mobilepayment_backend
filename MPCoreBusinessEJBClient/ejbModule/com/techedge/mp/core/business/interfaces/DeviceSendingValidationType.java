package com.techedge.mp.core.business.interfaces;

public enum DeviceSendingValidationType {

    SMS("sms"),
    EMAIL("email");
    
    private final String value;

    DeviceSendingValidationType(final String value) {
        this.value = value;
    }

    public String getValue() { 
    
        return value; 
    }

}
