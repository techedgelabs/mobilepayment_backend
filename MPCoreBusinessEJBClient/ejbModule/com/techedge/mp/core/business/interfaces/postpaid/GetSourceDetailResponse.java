package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.CashInfo;



public class GetSourceDetailResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3535296326931310736L;
	
	private String statusCode;
	private String messageCode;
	private String objectType;
	private String objectStatus;
	private CorePumpDetail pumpInfo;
	private CashInfo cashInfo;
	private String transactionID;
	private String transactionStatus;
	private PostPaidTransactionData transactionData;
	private String stationID;
	private String stationName;
	private String stationAddress;
	private String stationCity;
	private String stationProvince;
	private String stationCountry;
	private Double stationLatitude;
	private Double stationLongitude;
	private Double amount;
	private Boolean outOfRange;
	private Boolean newAcquirerEnabled;
	private Boolean refuelingEnabled;
	private Boolean loyaltyEnabled;
	private Boolean businessEnabled;
	private Set<PostPaidCartData> postPaidCartDataList = new HashSet<PostPaidCartData>();
	private Set<PostPaidRefuelData> postPaidRefuelDataList = new HashSet<PostPaidRefuelData>();
	 
		
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	public String getStationID() {
		return stationID;
	}
	public void setStationID(String stationID) {
		this.stationID = stationID;
	}
	
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	
	public String getStationAddress() {
		return stationAddress;
	}
	public void setStationAddress(String stationAddress) {
		this.stationAddress = stationAddress;
	}
	
	public String getStationCity() {
		return stationCity;
	}
	public void setStationCity(String stationCity) {
		this.stationCity = stationCity;
	}
	
	public String getStationProvince() {
		return stationProvince;
	}
	public void setStationProvince(String stationProvince) {
		this.stationProvince = stationProvince;
	}
	
	public String getStationCountry() {
		return stationCountry;
	}
	public void setStationCountry(String stationCountry) {
		this.stationCountry = stationCountry;
	}
	
	public Double getStationLatitude() {
		return stationLatitude;
	}
	public void setStationLatitude(Double stationLatitude) {
		this.stationLatitude = stationLatitude;
	}
	
	public Double getStationLongitude() {
		return stationLongitude;
	}
	public void setStationLongitude(Double stationLongitude) {
		this.stationLongitude = stationLongitude;
	}
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public Boolean getOutOfRange() {
        return outOfRange;
    }
	
    public void setOutOfRange(Boolean outOfRange) {
        this.outOfRange = outOfRange;
    }
    
    public Set<PostPaidCartData> getPostPaidCartDataList() {
		return postPaidCartDataList;
	}
	public void setPostPaidCartDataList(Set<PostPaidCartData> postPaidCartDataList) {
		this.postPaidCartDataList = postPaidCartDataList;
	}
	
	public Set<PostPaidRefuelData> getPostPaidRefuelDataList() {
		return postPaidRefuelDataList;
	}
	public void setPostPaidRefuelDataList(
			Set<PostPaidRefuelData> postPaidRefuelDataList) {
		this.postPaidRefuelDataList = postPaidRefuelDataList;
	}
	
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	
	public String getObjectStatus() {
		return objectStatus;
	}
	public void setObjectStatus(String objectStatus) {
		this.objectStatus = objectStatus;
	}
	
	public CorePumpDetail getPumpInfo() {
		return pumpInfo;
	}
	public void setPumpInfo(CorePumpDetail pumpInfo) {
		this.pumpInfo = pumpInfo;
	}
	
	public CashInfo getCashInfo() {
		return cashInfo;
	}
	public void setCashInfo(CashInfo cashInfo) {
		this.cashInfo = cashInfo;
	}
	
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	
	public PostPaidTransactionData getTransactionData() {
		return transactionData;
	}
	public void setTransactionData(PostPaidTransactionData transactionData) {
		this.transactionData = transactionData;
	}
	
    public Boolean getNewAcquirerEnabled() {
        if(newAcquirerEnabled == null) {
            return Boolean.TRUE;
        }
        return newAcquirerEnabled;
    }
    public void setNewAcquirerEnabled(Boolean newAcquirerEnabled) {
    
        this.newAcquirerEnabled = newAcquirerEnabled;
    }
    
    public Boolean getRefuelingEnabled() {
        if(refuelingEnabled == null) {
            return Boolean.TRUE;
        }
        return refuelingEnabled;
    }
    public void setRefuelingEnabled(Boolean refuelingEnabled) {
    
        this.refuelingEnabled = refuelingEnabled;
    }
    
    public Boolean getLoyaltyEnabled() {
        if(loyaltyEnabled == null) {
            return Boolean.TRUE;
        }
        return loyaltyEnabled;
    }
    public void setLoyaltyEnabled(Boolean loyaltyEnabled) {
    
        this.loyaltyEnabled = loyaltyEnabled;
    }
    
    public Boolean getBusinessEnabled() {

        return businessEnabled;
    }
    
    public void setBusinessEnabled(Boolean businessEnabled) {

        this.businessEnabled = businessEnabled;
    }
}