package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class Address implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3619910230575294536L;
	
	private String city;
	private String countryCodeId;
	private String countryCodeName;
	private String houseNumber;
	private String region;
	private String street;
	private String zipcode;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountryCodeId() {
		return countryCodeId;
	}
	public void setCountryCodeId(String countryCodeId) {
		this.countryCodeId = countryCodeId;
	}
	public String getCountryCodeName() {
		return countryCodeName;
	}
	public void setCountryCodeName(String countryCodeName) {
		this.countryCodeName = countryCodeName;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
}
