package com.techedge.mp.core.business.interfaces.crm;


public enum CrmSfOutboundDeliveryStatusType {
    
    CREATED("CREATED"),
    SUCCESS("SUCCESS"),
    INVALID_INPUT("INVALID_INPUT"),
    INVALID_PROMO_CODE("INVALID_PROMO_CODE"),
    INVALID_AMOUNT("INVALID_AMOUNT"),
    USER_NOT_FOUND("USER_NOT_FOUND"),
    SYSTEM_ERROR("SYSTEM_ERROR");

    
    private final String code;

    CrmSfOutboundDeliveryStatusType(final String code) {
        this.code = code;
    }
    
    public static CrmSfOutboundDeliveryStatusType getStatusType(String code) {
        for (CrmSfOutboundDeliveryStatusType statusType : CrmSfOutboundDeliveryStatusType.values()) {
            if (statusType.getCode().equals(code)) {
                return statusType;
            }
        }
        
        return null;
    }

    public String getCode() {
        return code; 
    }

    public String getName() {
        return name();
    }


}
