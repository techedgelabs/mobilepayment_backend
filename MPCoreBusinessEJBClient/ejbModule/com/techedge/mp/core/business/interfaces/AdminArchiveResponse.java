package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class AdminArchiveResponse implements Serializable {
	/**
     * 
     */
    private static final long serialVersionUID = -5240416674273870131L;

    private List<ArchiveTransactionResult> archiveTransactionResultList = new ArrayList<ArchiveTransactionResult>(0);

    private String statusCode;

    public List<ArchiveTransactionResult> getArchiveTransactionResultList() {
        return archiveTransactionResultList;
    }

    public void setArchiveTransactionResultList(List<ArchiveTransactionResult> archiveTransactionResultList) {
        this.archiveTransactionResultList = archiveTransactionResultList;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
	
}
