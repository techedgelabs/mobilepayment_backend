package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class ParameterNotification implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -383531942269169911L;
    
    private String name;
	private String value;
	
	   public ParameterNotification(String name, String value) {
	        this.name = name;
	        this.value = value;
	    }
	
	public String getName() {
		return name;
	}
	public void setName(String cityName) {
		this.name = cityName;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String cityProvince) {
		this.value = cityProvince;
	}
}
