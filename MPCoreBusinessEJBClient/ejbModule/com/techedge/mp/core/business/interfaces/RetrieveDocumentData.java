package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class RetrieveDocumentData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4022870436109333296L;
	
	private String statusCode;
	private String documentBase64 = new String();
	private Long dimension;
	private String filename;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getDocumentBase64() {
		return documentBase64;
	}
	public void setDocumentBase64(String documentBase64) {
		this.documentBase64 = documentBase64;
	}
	public Long getDimension() {
		return dimension;
	}
	public void setDimension(Long dimension) {
		this.dimension = dimension;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
		
	
}
