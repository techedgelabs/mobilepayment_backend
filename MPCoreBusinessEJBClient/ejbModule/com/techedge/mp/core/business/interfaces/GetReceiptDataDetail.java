package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class GetReceiptDataDetail implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 6375541413554961980L;
    private String documentBase64;
    private String filename;
    private int dimension;
    
    public String getDocumentBase64() {
        return documentBase64;
    }
    public void setDocumentBase64(String documentBase64) {
        this.documentBase64 = documentBase64;
    }
    public String getFilename() {
        return filename;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }
    public int getDimension() {
        return dimension;
    }
    public void setDimension(int dimension) {
        this.dimension = dimension;
    }


}
