package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MissionDetailDataResponse implements Serializable {

    /**
     * 
     */
    private static final long                    serialVersionUID     = -7361885072808872123L;

    private String                               statusCode;
    
    private List<PromotionInfoDataDetail> promotionList        = new ArrayList<PromotionInfoDataDetail>(0);
    
    private List<MissionInfoDataDetail>          missionDetailList    = new ArrayList<MissionInfoDataDetail>(0);
    
    private List<MissionInfoDataDetail>          completedMissionDetailList    = new ArrayList<MissionInfoDataDetail>(0);


    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<MissionInfoDataDetail> getMissionDetailList() {

        return missionDetailList;
    }

    public void setMissionDetailList(List<MissionInfoDataDetail> missionDetailList) {

        this.missionDetailList = missionDetailList;
    }

    public List<PromotionInfoDataDetail> getPromotionList() {

        return promotionList;
    }

    public void setPromotionList(List<PromotionInfoDataDetail> promotionList) {

        this.promotionList = promotionList;
    }

    public List<MissionInfoDataDetail> getCompletedMissionDetailList() {
    
        return completedMissionDetailList;
    }

    public void setCompletedMissionDetailList(List<MissionInfoDataDetail> completedMissionDetailList) {
    
        this.completedMissionDetailList = completedMissionDetailList;
    }
    
}
