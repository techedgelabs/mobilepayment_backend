package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetMissionsResponse implements Serializable {

   

    /**
     * 
     */
    private static final long serialVersionUID = 3229797773374080L;

    private String statusCode;
    
    List<Offer> offersList = new ArrayList<Offer>();

    public String getStatusCode() {
    
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
    
        this.statusCode = statusCode;
    }

    public List<Offer> getOffersList() {
    
        return offersList;
    }

    public void setOffersList(ArrayList<Offer> offersList) {
    
        this.offersList = offersList;
    }
    
}
