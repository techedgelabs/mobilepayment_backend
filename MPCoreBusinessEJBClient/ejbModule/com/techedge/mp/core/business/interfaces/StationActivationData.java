package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class StationActivationData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 35646169934582303L;

    private String            stationID;

    private Integer           status;

    private Boolean           newAcquirerActive;

    private Boolean           refuelingActive;

    private Boolean           loyaltyActive;

    public StationActivationData() {

    }

    public String getStationID() {

        return stationID;
    }

    public void setStationID(String stationID) {

        this.stationID = stationID;
    }

    public Integer getStatus() {

        return status;
    }

    public void setStatus(Integer status) {

        this.status = status;
    }

    public Boolean getNewAcquirerActive() {

        return newAcquirerActive;
    }

    public void setNewAcquirerActive(Boolean newAcquirerActive) {

        this.newAcquirerActive = newAcquirerActive;
    }

    public Boolean getRefuelingActive() {

        return refuelingActive;
    }

    public void setRefuelingActive(Boolean refuelingActive) {

        this.refuelingActive = refuelingActive;
    }

    public Boolean getLoyaltyActive() {

        return loyaltyActive;
    }

    public void setLoyaltyActive(Boolean loyaltyActive) {

        this.loyaltyActive = loyaltyActive;
    }

}
