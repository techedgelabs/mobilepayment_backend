package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.sql.Timestamp;

public class PaymentRefuelDetailResponse implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 8369270541185296116L;

  private String statusCode;
  private String refuelID;
  private String status;
  private String subStatus;
  private Boolean useVoucher;
  private Timestamp date;
  private Double initialAmount;
  private Double finalAmount;
  private Double fuelAmount;
  private Double fuelQuantity;
  private String bankTransactionID;
  private String shopLogin;
  private String currency;
  private String maskedPan;
  private String authCode;
  private String selectedStationID;
  private String selectedStationName;
  private String selectedStationAddress;
  private String selectedStationCity;
  private String selectedStationProvince;
  private String selectedStationCountry;
  private Double selectedStationLatitude;
  private Double selectedStationLongitude;
  private String selectedPumpID;
  private Integer selectedPumpNumber;
  private String selectedPumpFuelType;
  private String sourceType;
  private String sourceStatus;

  public PaymentRefuelDetailResponse() {}

  public String getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(String statusCode) {
    this.statusCode = statusCode;
  }

  public String getRefuelID() {
    return refuelID;
  }

  public void setRefuelID(String refuelID) {
    this.refuelID = refuelID;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getSubStatus() {
    return subStatus;
  }

  public void setSubStatus(String subStatus) {
    this.subStatus = subStatus;
  }

  public Boolean getUseVoucher() {
    return useVoucher;
  }

  public void setUseVoucher(Boolean useVoucher) {
    this.useVoucher = useVoucher;
  }

  public Timestamp getDate() {
    return date;
  }

  public void setDate(Timestamp date) {
    this.date = date;
  }

  public Double getInitialAmount() {
    return initialAmount;
  }

  public void setInitialAmount(Double initialAmount) {
    this.initialAmount = initialAmount;
  }

  public Double getFinalAmount() {
    return finalAmount;
  }

  public void setFinalAmount(Double finalAmount) {
    this.finalAmount = finalAmount;
  }

  public Double getFuelAmount() {
    return fuelAmount;
  }

  public void setFuelAmount(Double fuelAmount) {
    this.fuelAmount = fuelAmount;
  }

  public Double getFuelQuantity() {
    return fuelQuantity;
  }

  public void setFuelQuantity(Double fuelQuantity) {
    this.fuelQuantity = fuelQuantity;
  }

  public String getBankTransactionID() {
    return bankTransactionID;
  }

  public void setBankTransactionID(String bankTransactionID) {
    this.bankTransactionID = bankTransactionID;
  }

  public String getShopLogin() {
    return shopLogin;
  }

  public void setShopLogin(String shopLogin) {
    this.shopLogin = shopLogin;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getMaskedPan() {
    return maskedPan;
  }

  public void setMaskedPan(String maskedPan) {
    this.maskedPan = maskedPan;
  }

  public String getAuthCode() {
    return authCode;
  }

  public void setAuthCode(String authCode) {
    this.authCode = authCode;
  }

  public String getSelectedStationID() {
    return selectedStationID;
  }

  public void setSelectedStationID(String selectedStationID) {
    this.selectedStationID = selectedStationID;
  }

  public String getSelectedStationName() {
    return selectedStationName;
  }

  public void setSelectedStationName(String selectedStationName) {
    this.selectedStationName = selectedStationName;
  }

  public String getSelectedStationAddress() {
    return selectedStationAddress;
  }

  public void setSelectedStationAddress(String selectedStationAddress) {
    this.selectedStationAddress = selectedStationAddress;
  }

  public String getSelectedStationCity() {
    return selectedStationCity;
  }

  public void setSelectedStationCity(String selectedStationCity) {
    this.selectedStationCity = selectedStationCity;
  }

  public String getSelectedStationProvince() {
    return selectedStationProvince;
  }

  public void setSelectedStationProvince(String selectedStationProvince) {
    this.selectedStationProvince = selectedStationProvince;
  }

  public String getSelectedStationCountry() {
    return selectedStationCountry;
  }

  public void setSelectedStationCountry(String selectedStationCountry) {
    this.selectedStationCountry = selectedStationCountry;
  }

  public Double getSelectedStationLatitude() {
    return selectedStationLatitude;
  }

  public void setSelectedStationLatitude(Double selectedStationLatitude) {
    this.selectedStationLatitude = selectedStationLatitude;
  }

  public Double getSelectedStationLongitude() {
    return selectedStationLongitude;
  }

  public void setSelectedStationLongitude(Double selectedStationLongitude) {
    this.selectedStationLongitude = selectedStationLongitude;
  }

  public String getSelectedPumpID() {
    return selectedPumpID;
  }

  public void setSelectedPumpID(String selectedPumpID) {
    this.selectedPumpID = selectedPumpID;
  }

  public Integer getSelectedPumpNumber() {
    return selectedPumpNumber;
  }

  public void setSelectedPumpNumber(Integer selectedPumpNumber) {
    this.selectedPumpNumber = selectedPumpNumber;
  }

  public String getSelectedPumpFuelType() {
    return selectedPumpFuelType;
  }

  public void setSelectedPumpFuelType(String selectedPumpFuelType) {
    this.selectedPumpFuelType = selectedPumpFuelType;
  }

  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  public String getSourceStatus() {
    return sourceStatus;
  }

  public void setSourceStatus(String sourceStatus) {
    this.sourceStatus = sourceStatus;
  }
}
