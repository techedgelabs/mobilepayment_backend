package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class TransactionExistsResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 967579865153309724L;
	
	private String transactionID;
	private String status;
	private String subStatus;
	private String subStatusDescription;
	private String serverName;
	private String shopLogin;
	private String uicCode;
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getSubStatus() {
		return subStatus;
	}
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
	
	public String getSubStatusDescription() {
		return subStatusDescription;
	}
	public void setSubStatusDescription(String subStatusDescription) {
		this.subStatusDescription = subStatusDescription;
	}
	
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getShopLogin() {
		return shopLogin;
	}
	public void setShopLogin(String shopLogin) {
		this.shopLogin = shopLogin;
	}
	public String getUicCode() {
		return uicCode;
	}
	public void setUicCode(String uicCode) {
		this.uicCode = uicCode;
	}
}
