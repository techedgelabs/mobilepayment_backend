package com.techedge.mp.core.business.interfaces.loyalty;

import java.io.Serializable;

public class RewardTransactionParameter implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4291170580345610097L;

    private String            parameterID;

    private String            parameterValue;

    public RewardTransactionParameter() {

    }

    public String getParameterID() {

        return parameterID;
    }

    public void setParameterID(String parameterID) {

        this.parameterID = parameterID;
    }

    public String getParameterValue() {

        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {

        this.parameterValue = parameterValue;
    }


}
