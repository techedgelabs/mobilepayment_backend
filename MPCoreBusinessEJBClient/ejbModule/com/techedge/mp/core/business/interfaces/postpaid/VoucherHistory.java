package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.techedge.mp.core.business.interfaces.Voucher;

public class VoucherHistory implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 919754282699381017L;
    
    private String        stationID;
    private String        pumpNumber;
    private Date          date;
    private Double        totalConsumed;

    private List<Voucher> voucherList = new ArrayList<Voucher>(0);

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(String pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getTotalConsumed() {
        return totalConsumed;
    }

    public void setTotalConsumed(Double totalConsumed) {
        this.totalConsumed = totalConsumed;
    }

    public List<Voucher> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<Voucher> voucherList) {
        this.voucherList = voucherList;
    }

}
