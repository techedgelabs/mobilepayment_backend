package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.PostPaidTransaction;

public class PostPaidTransactionAdditionalData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6499754699250114644L;

    private long                id;
    private PostPaidTransaction postPaidTransaction;
    private String              dataKey;
    private String              dataValue;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public PostPaidTransaction getPostPaidTransaction() {
    
        return postPaidTransaction;
    }

    public void setPostPaidTransaction(PostPaidTransaction postPaidTransaction) {
    
        this.postPaidTransaction = postPaidTransaction;
    }

    public String getDataKey() {
    
        return dataKey;
    }

    public void setDataKey(String dataKey) {
    
        this.dataKey = dataKey;
    }

    public String getDataValue() {
    
        return dataValue;
    }

    public void setDataValue(String dataValue) {
    
        this.dataValue = dataValue;
    }

}
