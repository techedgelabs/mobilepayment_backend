package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class StationAdmin extends Station implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1232524162358873174L;
	
	
	private String response;
	private boolean isGenerated;
	private boolean isDeleted;
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public boolean isGenerated() {
		return isGenerated;
	}
	public void setGenerated(boolean isGenerated) {
		this.isGenerated = isGenerated;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	
	
}
