package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.user.User;

public class Voucher implements Serializable {

    /**
     * 
     */
    private static final long  serialVersionUID                           = -219321908338979534L;

    public static final String VOUCHER_TYPE_SCALARE                       = "SCALARE";

    public static final String VOUCHER_TYPE_INTERO                        = "INTERO";

    public static final String VOUCHER_STATUS_VALIDO                      = "V";

    public static final String VOUCHER_STATUS_SCADUTO                     = "S";

    public static final String VOUCHER_STATUS_ESAURITO                    = "E";

    public static final String VOUCHER_STATUS_ANNULLATO                   = "A";

    public static final String VOUCHER_STATUS_DA_CONFERMARE               = "D";

    public static final String VOUCHER_STATUS_INESISTENTE                 = "I";

    public static final String VOUCHER_STATUS_NON_SPENDIBILE              = "N";

    public static final String VOUCHER_STATUS_CANCELLATO                  = "C";

    public static final String VOUCHER_STATUS_NON_UTILIZZABILE_SUL_CANALE = "M";

    private long               id;

    private String             code;

    private User               user;

    private String             status;

    private String             type;

    private Double             value;

    private Double             initialValue;

    private Double             consumedValue;

    private Double             voucherBalanceDue;

    private Date               expirationDate;

    private String             promoCode;

    private String             promoDescription;

    private String             promoDoc;

    private Double             minQuantity;

    private Double             minAmount;

    //private List<String>     validProduct         = new ArrayList<String>(0);
    //private List<String>     validRefuelMode      = new ArrayList<String>(0);
    private String             validPV;

    private String             isCombinable;

    private String             promoPartner;

    private String             icon;

    public Voucher() {

    }

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public Double getValue() {

        return value;
    }

    public void setValue(Double value) {

        this.value = value;
    }

    public Double getInitialValue() {

        return initialValue;
    }

    public void setInitialValue(Double initialValue) {

        this.initialValue = initialValue;
    }

    public Double getConsumedValue() {

        return consumedValue;
    }

    public void setConsumedValue(Double consumedValue) {

        this.consumedValue = consumedValue;
    }

    public Double getVoucherBalanceDue() {

        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Double voucherBalanceDue) {

        this.voucherBalanceDue = voucherBalanceDue;
    }

    public Date getExpirationDate() {

        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {

        this.expirationDate = expirationDate;
    }

    public String getPromoCode() {

        return promoCode;
    }

    public void setPromoCode(String promoCode) {

        this.promoCode = promoCode;
    }

    public String getPromoDescription() {

        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {

        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {

        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {

        this.promoDoc = promoDoc;
    }

    public Double getMinQuantity() {

        return minQuantity;
    }

    public void setMinQuantity(Double minQuantity) {

        this.minQuantity = minQuantity;
    }

    public Double getMinAmount() {

        return minAmount;
    }

    public void setMinAmount(Double minAmount) {

        this.minAmount = minAmount;
    }

    /*
     * public List<String> getValidProduct() {
     * 
     * return validProduct;
     * }
     * 
     * public void setValidProduct(List<String> validProduct) {
     * 
     * this.validProduct = validProduct;
     * }
     * 
     * public List<String> getValidRefuelMode() {
     * 
     * return validRefuelMode;
     * }
     * 
     * public void setValidRefuelMode(List<String> validRefuelMode) {
     * 
     * this.validRefuelMode = validRefuelMode;
     * }
     */
    public String getValidPV() {

        return validPV;
    }

    public void setValidPV(String validPV) {

        this.validPV = validPV;
    }

    public String getIsCombinable() {

        return isCombinable;
    }

    public void setIsCombinable(String isCombinable) {

        this.isCombinable = isCombinable;
    }

    public String getPromoPartner() {

        return promoPartner;
    }

    public void setPromoPartner(String promoPartner) {

        this.promoPartner = promoPartner;
    }

    public String getIcon() {

        return icon;
    }

    public void setIcon(String icon) {

        this.icon = icon;
    }

}
