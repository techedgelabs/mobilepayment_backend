package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PrePaidLoadLoyaltyCredits implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3316654179175996040L;

    private long              id;
    private String            csTransactionID;
    private String            operationID;
    private String            operationIDReversed;
    private String            operationType;
    private Long              requestTimestamp;
    private String            marketingMsg;
    private String            messageCode;
    private String            statusCode;
    private String            warningMsg;
    private Integer           credits;
    private Integer           balance;
    private Double            balanceAmount;
    private String            cardCodeIssuer;
    private String            eanCode;
    private String            cardStatus;
    private String            cardType;
    private String            cardClassification;
    private Boolean           reconciled       = false;
    private Transaction       transaction;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getCsTransactionID() {

        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {

        this.csTransactionID = csTransactionID;
    }

    public String getOperationID() {

        return operationID;
    }

    public void setOperationID(String operationID) {

        this.operationID = operationID;
    }

    public String getOperationIDReversed() {

        return operationIDReversed;
    }

    public void setOperationIDReversed(String operationIDReversed) {

        this.operationIDReversed = operationIDReversed;
    }

    public String getOperationType() {

        return operationType;
    }

    public void setOperationType(String operationType) {

        this.operationType = operationType;
    }

    public Long getRequestTimestamp() {

        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {

        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {

        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {

        this.marketingMsg = marketingMsg;
    }

    public String getMessageCode() {

        return messageCode;
    }

    public void setMessageCode(String messageCode) {

        this.messageCode = messageCode;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getWarningMsg() {

        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {

        this.warningMsg = warningMsg;
    }

    public Integer getCredits() {

        return credits;
    }

    public void setCredits(Integer credits) {

        this.credits = credits;
    }

    public Integer getBalance() {

        return balance;
    }

    public void setBalance(Integer balance) {

        this.balance = balance;
    }

    public Double getBalanceAmount() {

        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {

        this.balanceAmount = balanceAmount;
    }

    public String getCardCodeIssuer() {

        return cardCodeIssuer;
    }

    public void setCardCodeIssuer(String cardCodeIssuer) {

        this.cardCodeIssuer = cardCodeIssuer;
    }

    public String getEanCode() {

        return eanCode;
    }

    public void setEanCode(String eanCode) {

        this.eanCode = eanCode;
    }

    public String getCardStatus() {

        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {

        this.cardStatus = cardStatus;
    }

    public String getCardType() {

        return cardType;
    }

    public void setCardType(String cardType) {

        this.cardType = cardType;
    }

    public String getCardClassification() {

        return cardClassification;
    }

    public void setCardClassification(String cardClassification) {

        this.cardClassification = cardClassification;
    }

    public Boolean getReconciled() {

        return reconciled;
    }

    public void setReconciled(Boolean reconciled) {

        this.reconciled = reconciled;
    }

    public Transaction getTransaction() {

        return transaction;
    }

    public void setTransaction(Transaction transaction) {

        this.transaction = transaction;
    }

    public PrePaidLoadLoyaltyCredits() {

    }

}
