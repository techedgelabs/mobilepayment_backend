package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class TransactionExtended extends Transaction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5866785067735562789L;

    private Boolean newPaymentFlow;

    public Boolean getNewPaymentFlow() {

        return newPaymentFlow;
    }

    public void setNewPaymentFlow(Boolean newPaymentFlow) {

        this.newPaymentFlow = newPaymentFlow;
    }
}
