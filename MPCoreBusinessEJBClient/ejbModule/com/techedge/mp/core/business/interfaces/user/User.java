package com.techedge.mp.core.business.interfaces.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;

import com.techedge.mp.core.business.interfaces.LastLoginData;
import com.techedge.mp.core.business.interfaces.LastLoginErrorData;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PlateNumber;
import com.techedge.mp.core.business.interfaces.UserSocialData;
import com.techedge.mp.core.business.interfaces.Voucher;

public class User implements Serializable {

    public static final Integer        USER_TYPE_CUSTOMER               = 1;

    public static final Integer        USER_TYPE_SERVICE                = 2;

    public static final Integer        USER_TYPE_TESTER                 = 3;

    public static final Integer        USER_TYPE_REFUELING              = 4;

    public static final Integer        USER_TYPE_VOUCHER_TESTER         = 5;

    public static final Integer        USER_TYPE_NEW_ACQUIRER_TESTER    = 6;

    public static final Integer        USER_TYPE_REFUELING_NEW_ACQUIRER = 7;

    public static final Integer        USER_TYPE_GUEST                  = 8;

    public static final Integer        USER_TYPE_BUSINESS               = 9;

    public static final Integer        USER_TYPE_MULTICARD              = 10;
    
    public static final Integer        USER_TYPE_REFUELING_OAUTH2       = 11;

    public static final Integer        USER_STATUS_BLOCKED              = 0;

    public static final Integer        USER_STATUS_VERIFIED             = 2;

    public static final Integer        USER_STATUS_NEW                  = 5;

    public static final Integer        USER_STATUS_TEMPORARY_PASSWORD   = 6;

    public static final Integer        USER_STATUS_MIGRATING            = 7;

    public static final Integer        USER_STATUS_CANCELLED            = 9;

    public static final Integer        USER_STATUS_DEVICE_TO_VERIFIED   = 10;

    public static final Integer        USER_STATUS_SOCIAL_REGISTRATION  = 11;

    public static final Integer        USER_STATUS_MIGRATING_BUSINESS   = 12;

    public static final String         USER_SOURCE_MYCICERO             = "MYCICERO";

    public static final String         USER_SOURCE_ENJOY                = "ENJOY";

    public static final String         USER_ENISTATION                  = "ENISTATION";

    public static final String         USER_MULTICARD                   = "MULTICARD";

    /**
	 * 
	 */
    private static final long          serialVersionUID                 = -5703527938688791684L;

    private long                       id;

    private PersonalData               personalData;

    private LastLoginData              lastLoginData;

    private Integer                    userStatus;

    private Boolean                    userStatusRegistrationCompleted;

    private Date                       userStatusRegistrationTimestamp;

    private Double                     capAvailable;

    private Double                     capEffective;

    private String                     contactDataMobilephone;

    private Integer                    contactDataStatus;

    private Set<PaymentInfo>           paymentData                      = new HashSet<PaymentInfo>(0);

    private Integer                    creditDataVoucher;

    private Integer                    userType;

    private String                     externalUserId;

    private Date                       createDate;

    private LastLoginErrorData         lastLoginErrorData;

    private Boolean                    useVoucher;

    private Boolean                    oldUser;

    private Set<Voucher>               voucherList                      = new HashSet<Voucher>(0);

    private Set<LoyaltyCard>           loyaltyCardList                  = new HashSet<LoyaltyCard>(0);

    private List<MobilePhone>          mobilePhoneList                  = new ArrayList<MobilePhone>(0);

    private String                     deviceId;

    private Boolean                    virtualizationCompleted;

    private Integer                    virtualizationAttemptsLeft;

    private Boolean                    eniStationUserType;

    private Boolean                    loyaltyCheckEnabled;

    private Boolean                    depositCardStepCompleted;

    private Date                       depositCardStepTimestamp;

    private Boolean                    updatedMission;

    private Set<UserDevice>            userDeviceList                   = new HashSet<UserDevice>(0);

    private Set<UserSocialData>        userSocialData                   = new HashSet<UserSocialData>(0);

    private Set<PlateNumber>           plateNumberList                  = new HashSet<PlateNumber>(0);

    private String                     source;

    private List<PersonalDataBusiness> personalDataBusinessList         = new ArrayList<PersonalDataBusiness>(0);

    private String                     sourceToken;

    private Boolean                    sourcePaymentMethodFound;
    
    private String                     contactKey;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public PersonalData getPersonalData() {

        return personalData;
    }

    public void setPersonalData(PersonalData personalData) {

        this.personalData = personalData;
    }

    public LastLoginData getLastLoginData() {

        return lastLoginData;
    }

    public void setLastLoginData(LastLoginData lastLoginData) {

        this.lastLoginData = lastLoginData;
    }

    public Integer getUserStatus() {

        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {

        this.userStatus = userStatus;
    }

    public Boolean getUserStatusRegistrationCompleted() {

        return userStatusRegistrationCompleted;
    }

    public void setUserStatusRegistrationCompleted(Boolean userStatusRegistrationCompleted) {

        this.userStatusRegistrationCompleted = userStatusRegistrationCompleted;
    }

    public Double getCapAvailable() {

        return capAvailable;
    }

    public void setCapAvailable(Double capAvailable) {

        this.capAvailable = capAvailable;
    }

    public Double getCapEffective() {

        return capEffective;
    }

    public void setCapEffective(Double capEffective) {

        this.capEffective = capEffective;
    }

    public String getContactDataMobilephone() {

        return contactDataMobilephone;
    }

    public void setContactDataMobilephone(String contactDataMobilephone) {

        this.contactDataMobilephone = contactDataMobilephone;
    }

    public Integer getContactDataStatus() {

        return contactDataStatus;
    }

    public void setContactDataStatus(Integer contactDataStatus) {

        this.contactDataStatus = contactDataStatus;
    }

    public Set<PaymentInfo> getPaymentData() {

        return paymentData;
    }

    public void setPaymentData(Set<PaymentInfo> paymentData) {

        this.paymentData = paymentData;
    }

    public Integer getCreditDataVoucher() {

        return creditDataVoucher;
    }

    public void setCreditDataVoucher(Integer creditDataVoucher) {

        this.creditDataVoucher = creditDataVoucher;
    }

    public Integer getUserType() {

        return userType;
    }

    public void setUserType(Integer userType) {

        this.userType = userType;
    }

    public String getExternalUserId() {

        return externalUserId;
    }

    public void setExternalUserId(String externalUserId) {

        this.externalUserId = externalUserId;
    }

    public Date getCreateDate() {

        return createDate;
    }

    public void setCreateDate(Date createDate) {

        this.createDate = createDate;
    }

    public LastLoginErrorData getLastLoginErrorData() {

        return lastLoginErrorData;
    }

    public void setLastLoginErrorData(LastLoginErrorData lastLoginErrorData) {

        this.lastLoginErrorData = lastLoginErrorData;
    }

    public Boolean getUseVoucher() {

        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {

        this.useVoucher = useVoucher;
    }

    public Set<Voucher> getVoucherList() {

        return voucherList;
    }

    public void setVoucherList(Set<Voucher> voucherList) {

        this.voucherList = voucherList;
    }

    public Set<LoyaltyCard> getLoyaltyCardList() {

        return loyaltyCardList;
    }

    public void setLoyaltyCardList(Set<LoyaltyCard> loyaltyCardList) {

        this.loyaltyCardList = loyaltyCardList;
    }

    public List<MobilePhone> getMobilePhoneList() {

        return mobilePhoneList;
    }

    public void setMobilePhoneList(List<MobilePhone> mobilePhoneList) {

        this.mobilePhoneList = mobilePhoneList;
    }

    public Boolean getOldUser() {

        return oldUser;
    }

    public void setOldUser(Boolean oldUser) {

        this.oldUser = oldUser;
    }

    public String getDeviceId() {

        return deviceId;
    }

    public void setDeviceId(String deviceId) {

        this.deviceId = deviceId;
    }

    public Boolean getVirtualizationCompleted() {

        return virtualizationCompleted;
    }

    public void setVirtualizationCompleted(Boolean virtualizationCompleted) {

        this.virtualizationCompleted = virtualizationCompleted;
    }

    public Integer getVirtualizationAttemptsLeft() {

        return virtualizationAttemptsLeft;
    }

    public void setVirtualizationAttemptsLeft(Integer virtualizationAttemptsLeft) {

        this.virtualizationAttemptsLeft = virtualizationAttemptsLeft;
    }

    public Boolean getEniStationUserType() {

        return eniStationUserType;
    }

    public void setEniStationUserType(Boolean eniStationUserType) {

        this.eniStationUserType = eniStationUserType;
    }

    public Boolean getLoyaltyCheckEnabled() {

        return loyaltyCheckEnabled;
    }

    public void setLoyaltyCheckEnabled(Boolean loyaltyCheckEnabled) {

        this.loyaltyCheckEnabled = loyaltyCheckEnabled;
    }

    public Set<UserDevice> getUserDeviceList() {

        return userDeviceList;
    }

    public void setUserDeviceList(Set<UserDevice> userDeviceList) {

        this.userDeviceList = userDeviceList;
    }

    public Boolean getDepositCardStepCompleted() {

        return depositCardStepCompleted;
    }

    public void setDepositCardStepCompleted(Boolean depositCardStepCompleted) {

        this.depositCardStepCompleted = depositCardStepCompleted;
    }

    public Date getDepositCardStepTimestamp() {

        return depositCardStepTimestamp;
    }

    public void setDepositCardStepTimestamp(Date depositCardStepTimestamp) {

        this.depositCardStepTimestamp = depositCardStepTimestamp;
    }

    public Date getUserStatusRegistrationTimestamp() {

        return userStatusRegistrationTimestamp;
    }

    public void setUserStatusRegistrationTimestamp(Date userStatusRegistrationTimestamp) {

        this.userStatusRegistrationTimestamp = userStatusRegistrationTimestamp;
    }

    public Set<UserSocialData> getUserSocialData() {

        return userSocialData;
    }

    public void setUserSocialData(Set<UserSocialData> userSocialData) {

        this.userSocialData = userSocialData;
    }

    public Boolean getUpdatedMission() {

        return updatedMission;
    }

    public void setUpdatedMission(Boolean updatedMission) {

        this.updatedMission = updatedMission;
    }

    public Set<PlateNumber> getPlateNumberList() {

        return plateNumberList;
    }

    public void setPlateNumberList(Set<PlateNumber> plateNumberList) {

        this.plateNumberList = plateNumberList;
    }

    public String getSource() {

        return source;
    }

    public void setSource(String source) {

        this.source = source;
    }

    public List<PersonalDataBusiness> getPersonalDataBusinessList() {

        return personalDataBusinessList;
    }

    public void setPersonalDataBusinessList(List<PersonalDataBusiness> personalDataBusinessList) {

        this.personalDataBusinessList = personalDataBusinessList;
    }

    public String getSourceToken() {

        return sourceToken;
    }

    public void setSourceToken(String sourceToken) {

        this.sourceToken = sourceToken;
    }

    public Boolean getSourcePaymentMethodFound() {

        return sourcePaymentMethodFound;
    }

    public void setSourcePaymentMethodFound(Boolean sourcePaymentMethodFound) {

        this.sourcePaymentMethodFound = sourcePaymentMethodFound;
    }

    public String getContactKey() {
    
        return contactKey;
    }

    public void setContactKey(String contactKey) {
    
        this.contactKey = contactKey;
    }
    
}
