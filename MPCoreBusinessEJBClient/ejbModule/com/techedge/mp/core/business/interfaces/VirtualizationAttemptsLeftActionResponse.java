package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class VirtualizationAttemptsLeftActionResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6984205385177820363L;

    private String            statusCode;

    private Integer           attemptsLeft;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public Integer getAttemptsLeft() {

        return attemptsLeft;
    }

    public void setAttemptsLeft(Integer attemptsLeft) {

        this.attemptsLeft = attemptsLeft;
    }

}
