package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionAdditionalData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionHistoryAdditionalData;
import com.techedge.mp.core.business.interfaces.user.User;

public class PostPaidTransactionHistory implements Serializable {

    /**
     * 
     */
    private static final long                    serialVersionUID                    = -4017713379359854900L;

    private long                                 id;
    private String                               mpTransactionID;
    private Date                                 archivingDate;
    private User                                 user;
    private Station                              station;
    private String                               source;
    private String                               sourceID;
    private String                               sourceNumber;
    private String                               srcTransactionID;
    private String                               bankTansactionID;
    private String                               authorizationCode;
    private String                               token;
    private String                               paymentType;
    private Double                               amount;
    private String                               currency;
    private String                               productType;
    private Set<PostPaidCart>                    cartBean                            = new HashSet<PostPaidCart>(0);
    private Set<PostPaidRefuel>                  refuelBean                          = new HashSet<PostPaidRefuel>(0);
    private String                               statusType;
    private String                               mpTransactionStatus;
    private String                               srcTransactionStatus;
    private Date                                 creationTimestamp;
    private Date                                 lastModifyTimestamp;
    private Set<PostPaidTransactionEvent>        postPaidTransactionEventBean        = new HashSet<PostPaidTransactionEvent>(0);
    private Set<PostPaidTransactionPaymentEvent> postPaidTransactionPaymentEventBean = new HashSet<PostPaidTransactionPaymentEvent>(0);
    private String                               shopLogin;
    private String                               acquirerID;
    private String                               paymentMode;
    private Long                                 paymentMethodId;
    private String                               paymentMethodType;
    private Boolean                              notificationCreated;
    private Boolean                              notificationPaid;
    private Boolean                              notificationUser;
    private Boolean                              useVoucher;
    private Boolean                              toReconcile;
    private Set<PostPaidConsumeVoucher>          postPaidConsumeVoucherBeanList      = new HashSet<PostPaidConsumeVoucher>(0);
    private Set<PostPaidLoadLoyaltyCredits>      postPaidLoadLoyaltyCreditsBeanList  = new HashSet<PostPaidLoadLoyaltyCredits>(0);
    private String                               encodedSecretKey;
    private String                               groupAcquirer;
    private String                               gfgElectronicInvoiceID;
    private TransactionCategoryType              transactionCategory;
    private Set<PostPaidTransactionHistoryAdditionalData> postPaidTransactionAdditionalDataList = new HashSet<PostPaidTransactionHistoryAdditionalData>(0);

    public PostPaidTransactionHistory() {}

    public PostPaidTransactionHistory(PostPaidTransaction postPaidTransaction) {

        this.id = postPaidTransaction.getId();
        this.source = postPaidTransaction.getSource();
        this.sourceID = postPaidTransaction.getSourceID();
        this.srcTransactionID = postPaidTransaction.getSrcTransactionID();
        this.mpTransactionID = postPaidTransaction.getMpTransactionID();
        this.bankTansactionID = postPaidTransaction.getBankTansactionID();
        this.authorizationCode = postPaidTransaction.getAuthorizationCode();
        this.token = postPaidTransaction.getToken();
        this.paymentType = postPaidTransaction.getPaymentType();
        this.amount = postPaidTransaction.getAmount();
        this.currency = postPaidTransaction.getCurrency();
        this.productType = postPaidTransaction.getProductType();
        this.toReconcile = postPaidTransaction.getToReconcile();

        if (postPaidTransaction.getCartList() != null) {
            Set<PostPaidCart> cartList = postPaidTransaction.getCartList();
            for (PostPaidCart cartBean : cartList) {
                this.cartBean.add(cartBean);
            }
        }

        if (postPaidTransaction.getRefuelList() != null) {
            Set<PostPaidRefuel> refuelList = postPaidTransaction.getRefuelList();
            for (PostPaidRefuel refuelBean : refuelList) {
                this.refuelBean.add(refuelBean);
            }
        }

        this.statusType = postPaidTransaction.getStatusType();
        this.mpTransactionStatus = postPaidTransaction.getMpTransactionStatus();
        this.srcTransactionStatus = postPaidTransaction.getSrcTransactionStatus();
        this.creationTimestamp = postPaidTransaction.getCreationTimestamp();
        this.lastModifyTimestamp = postPaidTransaction.getLastModifyTimestamp();

        if (postPaidTransaction.getPostPaidTransactionEventList() != null) {
            for (PostPaidTransactionEvent postPaidTransactionEvent : postPaidTransaction.getPostPaidTransactionEventList()) {
                this.postPaidTransactionEventBean.add(postPaidTransactionEvent);
            }
        }

        if (postPaidTransaction.getPostPaidTransactionPaymentEventList() != null) {
            for (PostPaidTransactionPaymentEvent postPaidTransactionPaymentEvent : postPaidTransaction.getPostPaidTransactionPaymentEventList()) {
                this.postPaidTransactionPaymentEventBean.add(postPaidTransactionPaymentEvent);
            }
        }

        this.shopLogin = postPaidTransaction.getShopLogin();
        this.acquirerID = postPaidTransaction.getAcquirerID();
        this.paymentMode = postPaidTransaction.getPaymentMode();
        this.paymentMethodId = postPaidTransaction.getPaymentMethodId();
        this.paymentMethodType = postPaidTransaction.getPaymentMethodType();
        this.notificationCreated = postPaidTransaction.getNotificationCreated();
        this.notificationPaid = postPaidTransaction.getNotificationPaid();
        this.notificationUser = postPaidTransaction.getNotificationUser();
        this.user = postPaidTransaction.getUser();
        this.station = postPaidTransaction.getStation();
        this.useVoucher = postPaidTransaction.getUseVoucher();

        if (postPaidTransaction.getPostPaidConsumeVoucherList() != null) {
            for (PostPaidConsumeVoucher postPaidConsumeVoucher : postPaidTransaction.getPostPaidConsumeVoucherList()) {
                this.postPaidConsumeVoucherBeanList.add(postPaidConsumeVoucher);
            }
        }

        if (postPaidTransaction.getPostPaidLoadLoyaltyCreditsList() != null) {
            for (PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits : postPaidTransaction.getPostPaidLoadLoyaltyCreditsList()) {
                this.postPaidLoadLoyaltyCreditsBeanList.add(postPaidLoadLoyaltyCredits);
            }
        }
        
        this.groupAcquirer = postPaidTransaction.getGroupAcquirer();
        this.encodedSecretKey = postPaidTransaction.getEncodedSecretKey();
        this.gfgElectronicInvoiceID = postPaidTransaction.getGFGElectronicInvoiceID();
        this.transactionCategory = postPaidTransaction.getTransactionCategory();
        
        if (postPaidTransaction.getPostPaidTransactionAdditionalDataList() != null) {
            for (PostPaidTransactionAdditionalData postPaidTransactionAdditionalData : postPaidTransaction.getPostPaidTransactionAdditionalDataList()) {
                PostPaidTransactionHistoryAdditionalData postPaidTransactionHistoryAdditionalData = new PostPaidTransactionHistoryAdditionalData(postPaidTransactionAdditionalData);
                postPaidTransactionHistoryAdditionalData.setPostPaidTransactionHistory(this);
                this.postPaidTransactionAdditionalDataList.add(postPaidTransactionHistoryAdditionalData);
            }
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public Date getArchivingDate() {
        return archivingDate;
    }

    public void setArchivingDate(Date archivingDate) {
        this.archivingDate = archivingDate;
    }

    public Boolean getToReconcile() {
        return toReconcile;
    }

    public void setToReconcile(Boolean toReconcile) {
        this.toReconcile = toReconcile;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public String getSourceNumber() {
        return sourceNumber;
    }

    public void setSourceNumber(String sourceNumber) {
        this.sourceNumber = sourceNumber;
    }

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public String getBankTansactionID() {
        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {
        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Set<PostPaidCart> getCartBean() {
        return cartBean;
    }

    public void setCartBean(Set<PostPaidCart> cartBean) {
        this.cartBean = cartBean;
    }

    public Set<PostPaidRefuel> getRefuelBean() {
        return refuelBean;
    }

    public void setRefuelBean(Set<PostPaidRefuel> refuelBean) {
        this.refuelBean = refuelBean;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public String getMpTransactionStatus() {
        return mpTransactionStatus;
    }

    public void setMpTransactionStatus(String mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }

    public String getSrcTransactionStatus() {
        return srcTransactionStatus;
    }

    public void setSrcTransactionStatus(String srcTransactionStatus) {
        this.srcTransactionStatus = srcTransactionStatus;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getLastModifyTimestamp() {
        return lastModifyTimestamp;
    }

    public void setLastModifyTimestamp(Date lastModifyTimestamp) {
        this.lastModifyTimestamp = lastModifyTimestamp;
    }

    public Set<PostPaidTransactionEvent> getPostPaidTransactionEventBean() {
        return postPaidTransactionEventBean;
    }

    public void setPostPaidTransactionEventBean(Set<PostPaidTransactionEvent> postPaidTransactionEventBean) {
        this.postPaidTransactionEventBean = postPaidTransactionEventBean;
    }

    public Set<PostPaidTransactionPaymentEvent> getPostPaidTransactionPaymentEventBean() {
        return postPaidTransactionPaymentEventBean;
    }

    public void setPostPaidTransactionPaymentEventBean(Set<PostPaidTransactionPaymentEvent> postPaidTransactionPaymentEventBean) {
        this.postPaidTransactionPaymentEventBean = postPaidTransactionPaymentEventBean;
    }

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
        this.acquirerID = acquirerID;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public Boolean getNotificationCreated() {
        return notificationCreated;
    }

    public void setNotificationCreated(Boolean notificationCreated) {
        this.notificationCreated = notificationCreated;
    }

    public Boolean getNotificationPaid() {
        return notificationPaid;
    }

    public void setNotificationPaid(Boolean notificationPaid) {
        this.notificationPaid = notificationPaid;
    }

    public Boolean getNotificationUser() {
        return notificationUser;
    }

    public void setNotificationUser(Boolean notificationUser) {
        this.notificationUser = notificationUser;
    }

    public Boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }

    public Set<PostPaidConsumeVoucher> getPostPaidConsumeVoucherBeanList() {
        return postPaidConsumeVoucherBeanList;
    }

    public void setPostPaidConsumeVoucherBeanList(Set<PostPaidConsumeVoucher> postPaidConsumeVoucherBeanList) {
        this.postPaidConsumeVoucherBeanList = postPaidConsumeVoucherBeanList;
    }

    public Set<PostPaidLoadLoyaltyCredits> getPostPaidLoadLoyaltyCreditsBeanList() {
        return postPaidLoadLoyaltyCreditsBeanList;
    }

    public void setPostPaidLoadLoyaltyCreditsBeanList(Set<PostPaidLoadLoyaltyCredits> postPaidLoadLoyaltyCreditsBeanList) {
        this.postPaidLoadLoyaltyCreditsBeanList = postPaidLoadLoyaltyCreditsBeanList;
    }

    public String getEncodedSecretKey() {

        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {

        this.encodedSecretKey = encodedSecretKey;
    }

    public String getGroupAcquirer() {

        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {

        this.groupAcquirer = groupAcquirer;
    }

    public String getGFGElectronicInvoiceID() {
        return gfgElectronicInvoiceID;
    }
    
    public void setGFGElectronicInvoiceID(String gfgElectronicInvoiceID) {
        this.gfgElectronicInvoiceID = gfgElectronicInvoiceID;
    }
    
    public TransactionCategoryType getTransactionCategory() {

        return transactionCategory;
    }
    
    public void setTransactionCategory(TransactionCategoryType transactionCategory) {

        this.transactionCategory = transactionCategory;
    }

    public Set<PostPaidTransactionHistoryAdditionalData> getPostPaidTransactionAdditionalDataList() {
    
        return postPaidTransactionAdditionalDataList;
    }

    public void setPostPaidTransactionAdditionalDataList(Set<PostPaidTransactionHistoryAdditionalData> postPaidTransactionAdditionalDataList) {
    
        this.postPaidTransactionAdditionalDataList = postPaidTransactionAdditionalDataList;
    }
    
}
