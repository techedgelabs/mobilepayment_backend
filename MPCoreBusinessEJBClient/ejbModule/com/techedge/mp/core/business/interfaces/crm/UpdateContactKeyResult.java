package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;

public class UpdateContactKeyResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3626646734103351417L;

    private StatusCodeEnum    statusCode;

    private String            operationId;

    public StatusCodeEnum getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(StatusCodeEnum statusCode) {

        this.statusCode = statusCode;
    }

    public String getOperationId() {

        return operationId;
    }

    public void setOperationId(String operationId) {

        this.operationId = operationId;
    }

}