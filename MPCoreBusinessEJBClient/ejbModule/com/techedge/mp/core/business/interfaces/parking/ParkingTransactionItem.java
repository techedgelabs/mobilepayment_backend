package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ParkingTransactionItem implements Serializable {

    private static final long                 serialVersionUID                 = 9116751849070106257L;

    private long                              id;

    private ParkingTransaction                parkingTransaction;

    private Date                              parkingStartTime;

    private Date                              parkingEndTime;

    private String                            parkingTimeCorrection;

    private String                            parkingItemStatus;

    private BigDecimal                        price;

    private BigDecimal                        priceDifference;

    private BigDecimal                        currentPrice;

    private BigDecimal                        previousPrice;

    private Date                              requestedEndTime;

    private Long                              paymentMethodId;

    private String                            paymentMethodType;

    private String                            serverName;

    private Integer                           reconciliationAttemptsLeft;

    private String                            groupAcquirer;

    private String                            currency;

    private String                            shopLogin;

    private String                            acquirerID;

    private String                            bankTansactionID;

    private String                            authorizationCode;

    private String                            paymentToken;
    
    private String                            pan;
    
    private Timestamp                         creationTimestamp;

    private Set<ParkingTransactionItemEvent>  parkingTransactionItemEventList  = new HashSet<ParkingTransactionItemEvent>(0);

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public ParkingTransaction getParkingTransaction() {

        return parkingTransaction;
    }

    public void setParkingTransaction(ParkingTransaction parkingTransaction) {

        this.parkingTransaction = parkingTransaction;
    }

    public String getParkingTimeCorrection() {
    
        return parkingTimeCorrection;
    }

    public void setParkingTimeCorrection(String parkingTimeCorrection) {
    
        this.parkingTimeCorrection = parkingTimeCorrection;
    }

    public String getParkingItemStatus() {

        return parkingItemStatus;
    }

    public void setParkingItemStatus(String parkingItemStatus) {

        this.parkingItemStatus = parkingItemStatus;
    }

    public BigDecimal getPrice() {

        return price;
    }

    public void setPrice(BigDecimal price) {

        this.price = price;
    }

    public Date getParkingStartTime() {

        return parkingStartTime;
    }

    public void setParkingStartTime(Date parkingStartTime) {

        this.parkingStartTime = parkingStartTime;
    }

    public Date getParkingEndTime() {

        return parkingEndTime;
    }

    public void setParkingEndTime(Date parkingEndTime) {

        this.parkingEndTime = parkingEndTime;
    }

    public Date getRequestedEndTime() {

        return requestedEndTime;
    }

    public void setRequestedEndTime(Date requestedEndTime) {

        this.requestedEndTime = requestedEndTime;
    }

    public void setParkingStartTime(Timestamp parkingStartTime) {

        this.parkingStartTime = parkingStartTime;
    }

    public Long getPaymentMethodId() {

        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {

        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {

        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {

        this.paymentMethodType = paymentMethodType;
    }

    public String getServerName() {

        return serverName;
    }

    public void setServerName(String serverName) {

        this.serverName = serverName;
    }

    public Integer getReconciliationAttemptsLeft() {

        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {

        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public String getGroupAcquirer() {

        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {

        this.groupAcquirer = groupAcquirer;
    }

    public String getCurrency() {

        return currency;
    }

    public void setCurrency(String currency) {

        this.currency = currency;
    }

    public String getShopLogin() {

        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {

        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {

        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {

        this.acquirerID = acquirerID;
    }

    public String getBankTansactionID() {

        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {

        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {

        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {

        this.authorizationCode = authorizationCode;
    }

    public String getPaymentToken() {

        return paymentToken;
    }

    public void setPaymentToken(String paymentToken) {

        this.paymentToken = paymentToken;
    }

    public Set<ParkingTransactionItemEvent> getParkingTransactionItemEventList() {

        return parkingTransactionItemEventList;
    }

    public void setParkingTransactionItemEventList(Set<ParkingTransactionItemEvent> parkingTransactionItemEventList) {

        this.parkingTransactionItemEventList = parkingTransactionItemEventList;
    }

    public BigDecimal getPriceDifference() {

        return priceDifference;
    }

    public void setPriceDifference(BigDecimal priceDifference) {

        this.priceDifference = priceDifference;
    }

    public BigDecimal getCurrentPrice() {

        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {

        this.currentPrice = currentPrice;
    }

    public BigDecimal getPreviousPrice() {

        return previousPrice;
    }

    public void setPreviousPrice(BigDecimal previousPrice) {

        this.previousPrice = previousPrice;
    }

    public String getPan() {
    
        return pan;
    }

    public void setPan(String pan) {
    
        this.pan = pan;
    }

    public Timestamp getCreationTimestamp() {
    
        return creationTimestamp;
    }

    public void setCreationTimestamp(Timestamp creationTimestamp) {
    
        this.creationTimestamp = creationTimestamp;
    }

}
