package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class ParamUpdateInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2331619786246480501L;
	
	private String key;
	private String statusCode;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
}
