package com.techedge.mp.core.business.interfaces;

public enum TransactionInterfaceType {

    TRANSACTION_PREPAID,
    TRANSACTION_POSTPAID,
    TRANSACTION_VOUCHER;
}
