package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class LandingDataResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3239923611447898976L;

    /**
     * 
     */

    private String            statusCode;
    private LandingData       landingData;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public LandingData getLandingData() {

        return landingData;
    }

    public void setLandingData(LandingData landingData) {

        this.landingData = landingData;
    }

}
