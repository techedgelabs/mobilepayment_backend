package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;



public class PostPaidGetShopTransactionJsonResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3535296326931310736L;
	
	private String statusCode;
	private String messageCode;
	private String transactionID;
	private String transactionType;
	
	private String stationID;
	private String stationName;
	private String stationAddress;
	private String stationCity;
	private String stationProvince;
	private String stationCountry;
	private Double stationLatitude;
	private Double stationLongitude;
	
	private Double amount;
	
	private Set<PostPaidCartData> postPaidCartDataList = new HashSet<PostPaidCartData>();
	private Set<PostPaidRefuelData> postPaidRefuelDataList = new HashSet<PostPaidRefuelData>();
	 
		

	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getStationID() {
		return stationID;
	}
	public void setStationID(String stationID) {
		this.stationID = stationID;
	}
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	public String getStationAddress() {
		return stationAddress;
	}
	public void setStationAddress(String stationAddress) {
		this.stationAddress = stationAddress;
	}
	public String getStationCity() {
		return stationCity;
	}
	public void setStationCity(String stationCity) {
		this.stationCity = stationCity;
	}
	public String getStationProvince() {
		return stationProvince;
	}
	public void setStationProvince(String stationProvince) {
		this.stationProvince = stationProvince;
	}
	public String getStationCountry() {
		return stationCountry;
	}
	public void setStationCountry(String stationCountry) {
		this.stationCountry = stationCountry;
	}
	public Double getStationLatitude() {
		return stationLatitude;
	}
	public void setStationLatitude(Double stationLatitude) {
		this.stationLatitude = stationLatitude;
	}
	public Double getStationLongitude() {
		return stationLongitude;
	}
	public void setStationLongitude(Double stationLongitude) {
		this.stationLongitude = stationLongitude;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Set<PostPaidCartData> getPostPaidCartDataList() {
		return postPaidCartDataList;
	}
	public void setPostPaidCartDataList(Set<PostPaidCartData> postPaidCartDataList) {
		this.postPaidCartDataList = postPaidCartDataList;
	}
	public Set<PostPaidRefuelData> getPostPaidRefuelDataList() {
		return postPaidRefuelDataList;
	}
	public void setPostPaidRefuelDataList(
			Set<PostPaidRefuelData> postPaidRefuelDataList) {
		this.postPaidRefuelDataList = postPaidRefuelDataList;
	}

			
	
}
