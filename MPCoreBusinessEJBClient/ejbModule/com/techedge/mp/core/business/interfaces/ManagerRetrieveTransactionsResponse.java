package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.postpaid.PaymentHistory;

public class ManagerRetrieveTransactionsResponse implements Serializable {

    /**
     * 
     */
    private static final long    serialVersionUID   = -7531558136928552725L;

    private String               statusCode;
    private List<PaymentHistory> transactionHistory = new ArrayList<PaymentHistory>(0);

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<PaymentHistory> getTransactionHistory() {
        return transactionHistory;
    }

    public void setTransactionHistory(List<PaymentHistory> transactionHistory) {
        this.transactionHistory = transactionHistory;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
