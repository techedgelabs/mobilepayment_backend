package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;

public class NotifyEventResponse implements Serializable {

    private static final long serialVersionUID = 7351754397841854216L;

    private Boolean           success;
    private String            errorCode;
    private String            message;
    private String            requestId;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

}