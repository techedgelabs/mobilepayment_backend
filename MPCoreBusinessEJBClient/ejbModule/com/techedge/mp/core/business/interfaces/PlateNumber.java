package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.user.User;

public class PlateNumber implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3935963140339679930L;

    private long              id;

    private String            plateNumber;

    private String            description;

    private Date              creationTimestamp;
    
    private Boolean           defaultPlateNumber;

    private User              user;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getPlateNumber() {

        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {

        this.plateNumber = plateNumber;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public Date getCreationTimestamp() {

        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {

        this.creationTimestamp = creationTimestamp;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public Boolean getDefaultPlateNumber() {
    
        return defaultPlateNumber;
    }

    public void setDefaultPlateNumber(Boolean defaultPlateNumber) {
    
        this.defaultPlateNumber = defaultPlateNumber;
    }

}
