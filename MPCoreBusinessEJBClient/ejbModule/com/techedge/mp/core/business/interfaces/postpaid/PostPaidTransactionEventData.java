package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;
import java.util.Date;


public class PostPaidTransactionEventData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2994395480681635498L;

	private Date eventTimestamp;
	private Integer eventType;
	private String result;
	private String errorCode;
	private String errorDescription;
	private String oldState;
	private String newState;
	private String stateType;   // Stantard/Reconciliation
	private PostPaidTransactionData transactionData;
		
	public PostPaidTransactionEventData() {}



	public Date getEventTimestamp() {
		return eventTimestamp;
	}



	public void setEventTimestamp(Date eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}



	public Integer getEventType() {
		return eventType;
	}



	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}



	public String getResult() {
		return result;
	}



	public void setResult(String result) {
		this.result = result;
	}



	public String getErrorCode() {
		return errorCode;
	}



	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}



	public String getErrorDescription() {
		return errorDescription;
	}



	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}



	public String getOldState() {
		return oldState;
	}



	public void setOldState(String oldState) {
		this.oldState = oldState;
	}



	public String getNewState() {
		return newState;
	}



	public void setNewState(String newState) {
		this.newState = newState;
	}



	public String getStateType() {
		return stateType;
	}



	public void setStateType(String stateType) {
		this.stateType = stateType;
	}



	public PostPaidTransactionData getTransactionData() {
		return transactionData;
	}



	public void setTransactionData(PostPaidTransactionData transactionData) {
		this.transactionData = transactionData;
	}
	
	
	
	
	
}
