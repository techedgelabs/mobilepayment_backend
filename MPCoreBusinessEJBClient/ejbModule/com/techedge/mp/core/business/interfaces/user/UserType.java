package com.techedge.mp.core.business.interfaces.user;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class UserType implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1939605801095376694L;

    private long              id;
    private Integer           code;
    private Set<UserCategory> userCategories   = new HashSet<UserCategory>(0);

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Set<UserCategory> getUserCategories() {
        return userCategories;
    }

    public void setUserCategories(Set<UserCategory> userCategories) {
        this.userCategories = userCategories;
    }

}
