package com.techedge.mp.core.business.interfaces.loyalty;

import java.io.Serializable;

public class LoyaltyTransactionRefuel implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6781840634284009256L;

    private RefuelModeType    refuelMode;

    private String            pumpNumber;

    private ProductIdType     productID;

    private String            productDescription;

    private Double            fuelQuantity;

    private Double            amount;

    private Integer           credits;

    public LoyaltyTransactionRefuel() {

    }

    public RefuelModeType getRefuelMode() {

        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {

        this.refuelMode = RefuelModeType.getRefuelMode(refuelMode);
    }

    public String getPumpNumber() {

        return pumpNumber;
    }

    public void setPumpNumber(String pumpNumber) {

        this.pumpNumber = pumpNumber;
    }

    public ProductIdType getProductID() {

        return productID;
    }

    public void setProductID(String productID) {

        this.productID = ProductIdType.getProduct(productID);
    }

    public String getProductDescription() {

        return productDescription;
    }

    public void setProductDescription(String productDescription) {

        this.productDescription = productDescription;
    }

    public Double getFuelQuantity() {

        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {

        this.fuelQuantity = fuelQuantity;
    }

    public Double getAmount() {

        return amount;
    }

    public void setAmount(Double amount) {

        this.amount = amount;
    }

    public Integer getCredits() {

        return credits;
    }

    public void setCredits(Integer credits) {

        this.credits = credits;
    }

}
