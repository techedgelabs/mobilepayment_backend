package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;

public class SurveyList implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3451103900034257374L;
    
    private ArrayList<Survey> surveyList = new ArrayList<Survey>(0);
    private String            statusCode;

    public ArrayList<Survey> getSurveyList() {
        return surveyList;
    }

    public void setSurveyList(ArrayList<Survey> surveyList) {
        this.surveyList = surveyList;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
