
package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class CategoryBurnDataDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1148915889220550136L;
    private Integer categoryId;
    private String category;
    private String categoryImageUrl;
    
    public Integer getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getCategoryImageUrl() {
        return categoryImageUrl;
    }
    public void setCategoryImageUrl(String categoryImageUrl) {
        this.categoryImageUrl = categoryImageUrl;
    }
    
}
