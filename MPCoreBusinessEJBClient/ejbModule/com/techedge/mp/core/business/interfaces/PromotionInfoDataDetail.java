package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class PromotionInfoDataDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1958343045636162030L;

    private String            description;

    private String            code;

    private String            name;

    private String            imageUrl;

    private AppLink           appLinkInfo;
    
    private Date              startDate        = new Date();

    private Date              endDate          = new Date();

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getImageUrl() {

        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {

        this.imageUrl = imageUrl;
    }

    public AppLink getAppLinkInfo() {

        return appLinkInfo;
    }

    public void setAppLinkInfo(AppLink appLinkInfo) {

        this.appLinkInfo = appLinkInfo;
    }

    public Date getStartDate() {
    
        return startDate;
    }

    public void setStartDate(Date startDate) {
    
        this.startDate = startDate;
    }

    public Date getEndDate() {
    
        return endDate;
    }

    public void setEndDate(Date endDate) {
    
        this.endDate = endDate;
    }

}
