package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class AdminRemoteSystemCheckResult implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -5573521355579733649L;

    private RemoteSystemType systemId;
    
    private String statusCode;
    
    private String statusMessage;

    public RemoteSystemType getSystemId() {
    
        return systemId;
    }

    public void setSystemId(RemoteSystemType systemId) {
    
        this.systemId = systemId;
    }

    public String getStatusCode() {
    
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
    
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
    
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
    
        this.statusMessage = statusMessage;
    }
    
    
}
