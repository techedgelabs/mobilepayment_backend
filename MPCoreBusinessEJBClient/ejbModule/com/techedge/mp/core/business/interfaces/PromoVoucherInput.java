package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

public class PromoVoucherInput implements Serializable{

    private static final long serialVersionUID = -1329922800714814182L;
    
    private ConcurrentHashMap<String, Double> promoAssociation = new ConcurrentHashMap<String, Double>(0);

    public PromoVoucherInput() {
        super();
    }

    public ConcurrentHashMap<String, Double> getPromoAssociation() {
        return promoAssociation;
    }

    public void setPromoAssociation(ConcurrentHashMap<String, Double> promoAssociation) {
        this.promoAssociation = promoAssociation;
    }
    
    
}
