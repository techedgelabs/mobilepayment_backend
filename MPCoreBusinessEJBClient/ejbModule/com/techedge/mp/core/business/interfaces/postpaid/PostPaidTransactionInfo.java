package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.Station;

public class PostPaidTransactionInfo extends PostPaidTransactionData implements Serializable {

    /**
   * 
   */
    private static final long serialVersionUID = 5366418589925653903L;
    private String            status;
    private String            subStatus;
    private Double            voucherAmount;
    private Integer           loyaltyBalance;
    private String            statusTitle;
    private String            statusDescription;

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(String subStatus) {
        this.subStatus = subStatus;
    }

    public Set<PostPaidCartData> getCart() {
        return super.getCartData();
    }

    public void setCart(Set<PostPaidCartData> cart) {
        super.setCartData(cart);
    }

    public Set<PostPaidRefuelData> getRefuel() {
        return super.getRefuelData();
    }

    public void setRefuel(Set<PostPaidRefuelData> refuel) {
        super.setRefuelData(refuel);
    }

    public Station getStation() {
        return super.getStationData();
    }

    public void setStation(Station station) {
        super.setStationData(station);
    }

    public Double getVoucherAmount() {
        return voucherAmount;
    }

    public void setVoucherAmount(Double voucherAmount) {
        this.voucherAmount = voucherAmount;
    }

    public Integer getLoyaltyBalance() {
        return loyaltyBalance;
    }

    public void setLoyaltyBalance(Integer loyaltyBalance) {
        this.loyaltyBalance = loyaltyBalance;
    }

}
