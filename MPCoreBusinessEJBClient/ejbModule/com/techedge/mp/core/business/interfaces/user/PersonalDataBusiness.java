package com.techedge.mp.core.business.interfaces.user;

import java.io.Serializable;

public class PersonalDataBusiness implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1127417041391758908L;

    private String            firstName;

    private String            lastName;

    private String            businessName;

    private String            fiscalCode;
    
    private String            vatNumber;

    private String            pecEmail;

    private String            sdiCode;

    private String            city;

    private String            province;

    private String            country;

    private String            zipCode;

    private String            address;

    private String            streetNumber;
    
    private String            licensePlate;

    public String getFirstName() {
    
        return firstName;
    }

    public void setFirstName(String firstName) {
    
        this.firstName = firstName;
    }

    public String getLastName() {
    
        return lastName;
    }

    public void setLastName(String lastName) {
    
        this.lastName = lastName;
    }

    public String getBusinessName() {
    
        return businessName;
    }

    public void setBusinessName(String businessName) {
    
        this.businessName = businessName;
    }

    public String getVatNumber() {
    
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
    
        this.vatNumber = vatNumber;
    }

    public String getPecEmail() {
    
        return pecEmail;
    }

    public void setPecEmail(String pecEmail) {
    
        this.pecEmail = pecEmail;
    }

    public String getSdiCode() {
    
        return sdiCode;
    }

    public void setSdiCode(String sdiCode) {
    
        this.sdiCode = sdiCode;
    }

    public String getCity() {
    
        return city;
    }

    public void setCity(String city) {
    
        this.city = city;
    }

    public String getProvince() {
    
        return province;
    }

    public void setProvince(String province) {
    
        this.province = province;
    }

    public String getZipCode() {
    
        return zipCode;
    }

    public void setZipCode(String zipCode) {
    
        this.zipCode = zipCode;
    }

    public String getAddress() {
    
        return address;
    }

    public void setAddress(String address) {
    
        this.address = address;
    }

    public String getLicensePlate() {
    
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
    
        this.licensePlate = licensePlate;
    }

    public String getStreetNumber() {

        return streetNumber;
    }
    
    public void setStreetNumber(String streetNumber) {

        this.streetNumber = streetNumber;
    }
    
    public String getCountry() {

        return country;
    }
    
    public void setCountry(String country) {

        this.country = country;
    }

    public String getFiscalCode() {
    
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
    
        this.fiscalCode = fiscalCode;
    }
    
}
