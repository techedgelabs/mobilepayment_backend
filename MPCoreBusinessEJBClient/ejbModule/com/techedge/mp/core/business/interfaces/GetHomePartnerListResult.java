package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetHomePartnerListResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4288463703765623703L;

    private String            statusCode;

    private List<HomePartner> homePartnerList = new ArrayList<HomePartner>(0);

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<HomePartner> getHomePartnerList() {

        return homePartnerList;
    }

    public void setHomePartnerList(List<HomePartner> homePartnerList) {

        this.homePartnerList = homePartnerList;
    }

}
