package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;

public class ApproveParkingTransactionResult extends FullParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 242383489485148815L;
    
    Integer pinCheckMaxAttempts;

    public Integer getPinCheckMaxAttempts() {
    
        return pinCheckMaxAttempts;
    }

    public void setPinCheckMaxAttempts(Integer pinCheckMaxAttempts) {
    
        this.pinCheckMaxAttempts = pinCheckMaxAttempts;
    }
    
}
