package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class FidelityConsumeVoucherData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4919715710570629902L;

    private String            operationID;

    private Long              requestTimestamp;

    private String            csTransactionID;

    private String            statusCode;

    private String            messageCode;

    private String            marketingMsg;

    private String            warningMsg;

    private Double            amount;

    private String            preAuthOperationID;

    public String getOperationID() {

        return operationID;
    }

    public void setOperationID(String operationID) {

        this.operationID = operationID;
    }

    public Long getRequestTimestamp() {

        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {

        this.requestTimestamp = requestTimestamp;
    }

    public String getCsTransactionID() {

        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {

        this.csTransactionID = csTransactionID;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getMessageCode() {

        return messageCode;
    }

    public void setMessageCode(String messageCode) {

        this.messageCode = messageCode;
    }

    public String getMarketingMsg() {

        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {

        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {

        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {

        this.warningMsg = warningMsg;
    }

    public Double getAmount() {

        return amount;
    }

    public void setAmount(Double amount) {

        this.amount = amount;
    }

    public String getPreAuthOperationID() {

        return preAuthOperationID;
    }

    public void setPreAuthOperationID(String preAuthOperationID) {

        this.preAuthOperationID = preAuthOperationID;
    }

}
