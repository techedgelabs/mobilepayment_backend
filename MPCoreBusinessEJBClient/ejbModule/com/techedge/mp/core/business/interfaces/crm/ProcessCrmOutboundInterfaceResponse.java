package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProcessCrmOutboundInterfaceResponse implements Serializable {

    /**
     * 
     */
    private static final long                    serialVersionUID                   = -2707293516761595846L;

    private List<CrmDataElementProcessingResult> crmDataElementProcessingResultList = new ArrayList<CrmDataElementProcessingResult>(0);

    private String                               statusCode;

    public List<CrmDataElementProcessingResult> getCrmDataElementProcessingResultList() {

        return crmDataElementProcessingResultList;
    }

    public void setCrmDataElementProcessingResultList(List<CrmDataElementProcessingResult> crmDataElementProcessingResultList) {

        this.crmDataElementProcessingResultList = crmDataElementProcessingResultList;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

}
