package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;




public class TransactionEventHistory implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7099877186497782098L;
	
	private long id;
	private TransactionHistory transaction;
	private Integer sequenceID;
	private String eventType;
	private Double eventAmount;
	private String transactionResult;
	private String errorCode;
	private String errorDescription;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public TransactionHistory getTransaction() {
		return transaction;
	}
	public void setTransaction(TransactionHistory transaction) {
		this.transaction = transaction;
	}
	
	public Integer getSequenceID() {
		return sequenceID;
	}
	public void setSequenceID(Integer sequenceID) {
		this.sequenceID = sequenceID;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public Double getEventAmount() {
		return eventAmount;
	}
	public void setEventAmount(Double eventAmount) {
		this.eventAmount = eventAmount;
	}
	public String getTransactionResult() {
		return transactionResult;
	}
	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
	


}
