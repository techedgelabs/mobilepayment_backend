package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class MulticardStatusNotificationResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9127567898833476291L;

    private String            statusCode;

    private String            operationID;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getOperationID() {

        return operationID;
    }

    public void setOperationID(String operationID) {

        this.operationID = operationID;
    }

}
