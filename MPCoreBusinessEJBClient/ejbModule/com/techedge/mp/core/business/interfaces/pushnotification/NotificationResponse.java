package com.techedge.mp.core.business.interfaces.pushnotification;

import java.io.Serializable;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.AppLink;

public class NotificationResponse implements Serializable {

    /**
     * 
     */
    private static final long          serialVersionUID = -2116585254041348957L;

    private Long                       id;
    private PushNotificationSourceType source;
    private String                     title;
    private String                     text;
    private Date                       sendDate;
    private Date                       expireDate;
    private String                     actionText;
    private String                     actionLocation;
    private String                     statusCode;
    private Boolean                    showDialog;
    private AppLink                    appLink;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PushNotificationSourceType getSource() {
        return source;
    }
    
    public void setSource(PushNotificationSourceType source) {
        this.source = source;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getActionText() {
        return actionText;
    }

    public void setActionText(String appText) {
        this.actionText = appText;
    }

    public String getActionLocation() {
        return actionLocation;
    }

    public void setActionLocation(String appLocation) {
        this.actionLocation = appLocation;
    }

    public AppLink getAppLink() {
        return appLink;
    }

    public void setAppLink(AppLink appLink) {
        this.appLink = appLink;
    }

    public Boolean getShowDialog() {
        return showDialog;
    }

    public void setShowDialog(Boolean showDialog) {
        this.showDialog = showDialog;
    }

}
