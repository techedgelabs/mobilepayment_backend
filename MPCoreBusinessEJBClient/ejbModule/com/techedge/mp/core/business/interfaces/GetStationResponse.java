package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetStationResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4777629592658214824L;
	
	private String statusCode;
	private Boolean outOfRange;
	private List<GetStationInfo> stationInfoList = new ArrayList<GetStationInfo>(0);
	

	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
    public Boolean getOutOfRange() {
        return this.outOfRange;
    }
    public void setOutOfRange(Boolean outOfRange) {
        this.outOfRange = outOfRange;
    }
	
	public List<GetStationInfo> getStationInfoList() {
		return this.stationInfoList;
	}
	public void setStationInfoList(List<GetStationInfo> stationInfoList) {
		this.stationInfoList = stationInfoList;
	}
}
