package com.techedge.mp.core.business.interfaces.loyalty;

import java.io.Serializable;
import java.util.Date;

public class EventNotification implements Serializable {

    /**
     * 
     */
    private static final long     serialVersionUID = -6947342787701334203L;

    private Long                  id;

    private EventNotificationType eventType;

    private String                srcTransactionID;

    private Date                  requestTimestamp;

    private String                fiscalCode;

    private String                operationType;

    private String                subtype;

    private RewardTransaction     rewardTransactionDetail;

    private LoyaltyTransaction    loyaltyTransactionDetail;

    private String                sessionID;

    private String                panCode;

    private String                cardStatus;

    private String                stationID;

    private String                terminalID;

    private String                transactionResult;

    private Date                  transactionDate;

    private Integer               credits;

    private Integer               balance;

    private Integer               balanceAmount;

    private String                marketingMsg;

    private String                warningMsg;

    private String                statusCode;

    private String                statusMessage;

    public EventNotification() {

    }

    public Long getId() {

        return id;
    }
    
    public void setId(Long id) {

        this.id = id;
    }
    
    
    public Date getRequestTimestamp() {

        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {

        this.requestTimestamp = requestTimestamp;
    }

    public EventNotificationType getEventType() {

        return eventType;
    }

    public void setEventType(EventNotificationType eventType) {

        this.eventType = eventType;
    }

    public String getSrcTransactionID() {

        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {

        this.srcTransactionID = srcTransactionID;
    }

    public String getFiscalCode() {

        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {

        this.fiscalCode = fiscalCode;
    }

    public RewardTransaction getRewardTransactionDetail() {

        return rewardTransactionDetail;
    }

    public void setRewardTransactionDetail(RewardTransaction rewardTransactionDetail) {

        this.rewardTransactionDetail = rewardTransactionDetail;
    }

    public LoyaltyTransaction getLoyaltyTransactionDetail() {

        return loyaltyTransactionDetail;
    }

    public void setLoyaltyTransactionDetail(LoyaltyTransaction loyaltyTransactionDetail) {

        this.loyaltyTransactionDetail = loyaltyTransactionDetail;
    }

    public String getStatusMessage() {

        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {

        this.statusMessage = statusMessage;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getSessionID() {

        return sessionID;
    }

    public void setSessionID(String sessionID) {

        this.sessionID = sessionID;
    }

    public String getCardStatus() {

        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {

        this.cardStatus = cardStatus;
    }

    public String getPanCode() {

        return panCode;
    }

    public void setPanCode(String panCode) {

        this.panCode = panCode;
    }

    public String getStationID() {

        return stationID;
    }

    public void setStationID(String stationID) {

        this.stationID = stationID;
    }

    public String getTerminalID() {

        return terminalID;
    }

    public void setTerminalID(String terminalID) {

        this.terminalID = terminalID;
    }

    public String getTransactionResult() {

        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {

        this.transactionResult = transactionResult;
    }

    public Date getTransactionDate() {

        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {

        this.transactionDate = transactionDate;
    }

    public Integer getCredits() {

        return credits;
    }

    public void setCredits(Integer credits) {

        this.credits = credits;
    }

    public Integer getBalance() {

        return balance;
    }

    public void setBalance(Integer balance) {

        this.balance = balance;
    }

    public String getMarketingMsg() {

        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {

        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {

        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {

        this.warningMsg = warningMsg;
    }

    public String getOperationType() {

        return operationType;
    }

    public void setOperationType(String operationType) {

        this.operationType = operationType;
    }

    public String getSubtype() {

        return subtype;
    }

    public void setSubtype(String subtype) {

        this.subtype = subtype;
    }
    
    public Integer getBalanceAmount() {

        return balanceAmount;
    }
    
    public void setBalanceAmount(Integer balanceAmount) {

        this.balanceAmount = balanceAmount;
    }
}
