package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GeneratePvResponse implements Serializable {

    /**
     * 
     */
    private static final long      serialVersionUID     = 4146496797236376438L;

    private String                 statusCode;

    private List<PvGenerationData> pvGenerationDataList = new ArrayList<PvGenerationData>(0);

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<PvGenerationData> getPvGenerationDataList() {

        return pvGenerationDataList;
    }

    public void setPvGenerationDataList(List<PvGenerationData> pvGenerationDataList) {

        this.pvGenerationDataList = pvGenerationDataList;
    }

}
