package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PumpInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2932558526948100965L;

	private String pumpId;
	private String description;
	private String number;
	private String fuelType;
	private String status;
	
	public String getPumpId() {
		return pumpId;
	}
	public void setPumpId(String pumpId) {
		this.pumpId = pumpId;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
