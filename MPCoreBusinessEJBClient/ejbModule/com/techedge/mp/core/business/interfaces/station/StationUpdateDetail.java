package com.techedge.mp.core.business.interfaces.station;

import java.io.Serializable;
import java.util.Date;

public class StationUpdateDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5505733466248006593L;

    private String            stationID;

    private String            address;

    private String            city;

    private String            country;

    private String            province;

    private Double            latitude;

    private Double            longitude;

    private boolean           loyalty;

    private boolean           business;

    private boolean           voucherPayment;

    private String            alias;

    private String            macKey;

    private boolean           temporarilyClosed;

    private Date              validityDateDetails;

    public String getStationID() {

        return stationID;
    }

    public void setStationID(String stationID) {

        this.stationID = stationID;
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    public String getCity() {

        return city;
    }

    public void setCity(String city) {

        this.city = city;
    }

    public String getCountry() {

        return country;
    }

    public void setCountry(String country) {

        this.country = country;
    }

    public String getProvince() {

        return province;
    }

    public void setProvince(String province) {

        this.province = province;
    }

    public Double getLatitude() {

        return latitude;
    }

    public void setLatitude(Double latitude) {

        this.latitude = latitude;
    }

    public Double getLongitude() {

        return longitude;
    }

    public void setLongitude(Double longitude) {

        this.longitude = longitude;
    }

    public boolean isLoyalty() {

        return loyalty;
    }

    public void setLoyalty(boolean loyalty) {

        this.loyalty = loyalty;
    }

    public boolean isBusiness() {

        return business;
    }

    public void setBusiness(boolean business) {

        this.business = business;
    }

    public boolean isVoucherPayment() {

        return voucherPayment;
    }

    public void setVoucherPayment(boolean voucherPayment) {

        this.voucherPayment = voucherPayment;
    }

    public String getAlias() {

        return alias;
    }

    public void setAlias(String alias) {

        this.alias = alias;
    }

    public String getMacKey() {

        return macKey;
    }

    public void setMacKey(String macKey) {

        this.macKey = macKey;
    }

    public boolean isTemporarilyClosed() {

        return temporarilyClosed;
    }

    public void setTemporarilyClosed(boolean temporarilyClosed) {

        this.temporarilyClosed = temporarilyClosed;
    }

    public Date getValidityDateDetails() {

        return validityDateDetails;
    }

    public void setValidityDateDetails(Date validityDateDetails) {

        this.validityDateDetails = validityDateDetails;
    }
}
