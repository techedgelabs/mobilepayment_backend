package com.techedge.mp.core.business.interfaces.pushnotification;

import com.techedge.mp.core.business.interfaces.StatusHelper;


public enum PushNotificationStatusType {

    DELIVERED(StatusHelper.PUSH_NOTIFICATION_STATUS_DELIVERED),
    DELETED(StatusHelper.PUSH_NOTIFICATION_STATUS_DELETED),
    PENDING(StatusHelper.PUSH_NOTIFICATION_STATUS_PENDING),
    ARN_NOT_FOUND(StatusHelper.PUSH_NOTIFICATION_STATUS_ARN_NOT_FOUND),
    ERROR(StatusHelper.PUSH_NOTIFICATION_STATUS_ERROR);
    
    
    private final String code;

    PushNotificationStatusType(final String code) {
        this.code = code;
    }

    public static PushNotificationStatusType getStatusType(Integer code) {
        for (PushNotificationStatusType statusType : PushNotificationStatusType.values()) {
            if (statusType.getCode() == code) {
                return statusType;
            }
        }
        
        return null;
    }

    public Integer getCode() {
        return Integer.valueOf(code); 
    }
    

}
