package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class AdminErrorResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1290731776497034459L;

    private String            gpErrorCode;

    private String            mpStatusCode;

    public String getGpErrorCode() {

        return gpErrorCode;
    }

    public void setGpErrorCode(String gpErrorCode) {

        this.gpErrorCode = gpErrorCode;
    }

    public String getMpStatusCode() {

        return mpStatusCode;
    }

    public void setMpStatusCode(String mpStatusCode) {

        this.mpStatusCode = mpStatusCode;
    }

}