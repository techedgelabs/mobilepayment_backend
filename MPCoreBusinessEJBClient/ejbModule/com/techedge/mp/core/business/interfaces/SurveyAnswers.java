package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;

public class SurveyAnswers implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -902172302132880740L;
    private ArrayList<Answer> answers = new ArrayList<Answer>(0);
    private String statusCode = null;

    public void addAnswer(String code, String value) {
        this.answers.add(new Answer(code, value));
    }
    
    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public class Answer implements Serializable {
        
        /**
         * 
         */
        private static final long serialVersionUID = 6164879555679057249L;
        private String code;        
        private String value;        

        public Answer(String code, String value) {
            this.code = code;
            this.value = value;
        }
        
        public String getCode() {
            return code;
        }

        public String getValue() {
            return value;
        }
    }
}
