package com.techedge.mp.core.business.interfaces.crm;


public enum CrmOutboundProcessedStatusType {
    
    CREATED(0),
    PROCESSED(1),
    NOTIFICATION_SENT(2),
    UNAVAILABLE(3);
    
    private final Integer value;

    CrmOutboundProcessedStatusType(final Integer value) {
        this.value = value;
    }
    
    public static CrmOutboundProcessedStatusType getValueType(Integer value) {
        for (CrmOutboundProcessedStatusType statusType : CrmOutboundProcessedStatusType.values()) {
            if (statusType.getValue().equals(value)) {
                return statusType;
            }
        }
        
        return null;
    }

    public Integer getValue() {
        return value; 
    }

    public String getName() {
        return name();
    }


}
