package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PromotionSF implements Serializable {

    private static final long serialVersionUID = 5844113571659155665L;

    private String     offerCode;

    private String     nome;

    private String     bannerId;

    private Date       dataInizio;

    private Date       dataFine;

    private String     url;

    private String     parameter1;

    private String     parameter2;

    private String     parameter3;

    private BigDecimal parameter4;

    public PromotionSF() {

    }

    public PromotionSF(String offerCode, String nome, String bannerId, Date dataInizio, Date dataFine, String url, String parameter1, String parameter2, String parameter3, BigDecimal parameter4) {

        this.offerCode = offerCode;
        this.nome = nome;
        this.bannerId = bannerId;
        this.dataInizio = dataInizio;
        this.dataFine = dataFine;
        this.url = url;
        this.parameter1 = parameter1;
        this.parameter2 = parameter2;
        this.parameter3 = parameter3;
        this.parameter4 = parameter4;
    }

    public String getOfferCode() {

        return offerCode;
    }

    public void setOfferCode(String offerCode) {

        this.offerCode = offerCode;
    }

    public String getNome() {

        return nome;
    }

    public void setNome(String nome) {

        this.nome = nome;
    }

    public String getBannerId() {

        return bannerId;
    }

    public void setBannerId(String bannerId) {

        this.bannerId = bannerId;
    }

    public Date getDataInizio() {

        return dataInizio;
    }

    public void setDataInizio(Date dataInizio) {

        this.dataInizio = dataInizio;
    }

    public Date getDataFine() {

        return dataFine;
    }

    public void setDataFine(Date dataFine) {

        this.dataFine = dataFine;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    public String getParameter1() {

        return parameter1;
    }

    public void setParameter1(String parameter1) {

        this.parameter1 = parameter1;
    }

    public String getParameter2() {

        return parameter2;
    }

    public void setParameter2(String parameter2) {

        this.parameter2 = parameter2;
    }

    public String getParameter3() {

        return parameter3;
    }

    public void setParameter3(String parameter3) {

        this.parameter3 = parameter3;
    }

    public BigDecimal getParameter4() {

        return parameter4;
    }

    public void setParameter4(BigDecimal parameter4) {

        this.parameter4 = parameter4;
    }

}
