package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PostPaidCart implements Serializable {

    /**
     * 
     */
    private static final long   serialVersionUID = 1849647418891304853L;
    
    private long                id;
    private String              productId;
    private String              productDescription;
    private Double              amount;
    private Integer             quantity;
    private PostPaidTransaction postPaidTransaction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public PostPaidTransaction getPostPaidTransaction() {
        return postPaidTransaction;
    }

    public void setPostPaidTransaction(PostPaidTransaction postPaidTransaction) {
        this.postPaidTransaction = postPaidTransaction;
    }

}
