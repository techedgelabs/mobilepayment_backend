package com.techedge.mp.core.business.interfaces.pushnotification;

public enum PushNotificationContentType {

    TEXT,
    INTERNAL,
    EXTERNAL,
    SECTION;

}
