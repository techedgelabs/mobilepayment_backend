package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class RefuelingMappingError implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6528612122484670238L;
    
    private String errorCode;
    private String statusCode;
    public String getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    public String getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
    
}
