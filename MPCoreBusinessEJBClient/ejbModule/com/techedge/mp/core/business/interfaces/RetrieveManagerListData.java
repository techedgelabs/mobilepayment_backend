package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveManagerListData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3307851412746064243L;

	private String statusCode;
	private List<Manager> managerList = new ArrayList<Manager>(0);
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public List<Manager> getManagerList() {
		return managerList;
	}
	public void setManagerList(List<Manager> managerList) {
		this.managerList = managerList;
	}
}
