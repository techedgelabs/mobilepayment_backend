package com.techedge.mp.core.business.interfaces.voucher;

import java.io.Serializable;
import java.sql.Timestamp;

public class VoucherTransactionStatus implements Serializable {
	
	
	/**
     * 
     */
    private static final long serialVersionUID = -5600964410285220559L;
    private long id;
	private VoucherTransaction voucherTransaction;
	private Integer sequenceID;
	private Timestamp timestamp;
	private String status;
	private String subStatus;
	private String subStatusDescription;
	private String requestID;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public VoucherTransaction getVoucherTransaction() {
		return voucherTransaction;
	}
	public void setVoucherTransaction(VoucherTransaction voucherTransaction) {
		this.voucherTransaction = voucherTransaction;
	}
	
	public Integer getSequenceID() {
		return sequenceID;
	}
	public void setSequenceID(Integer sequenceID) {
		this.sequenceID = sequenceID;
	}
	
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getSubStatus() {
		return subStatus;
	}
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
	
	public String getSubStatusDescription() {
		return subStatusDescription;
	}
	public void setSubStatusDescription(String subStatusDescription) {
		this.subStatusDescription = subStatusDescription;
	}
	
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
}
