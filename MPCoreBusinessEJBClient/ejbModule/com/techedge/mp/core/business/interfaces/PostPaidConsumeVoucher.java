package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class PostPaidConsumeVoucher implements Serializable {

    /**
     * 
     */
    private static final long                 serialVersionUID             = -1939112316225666305L;

    private long                              id;
    private String                            csTransactionID;
    private String                            operationID;
    private String                            operationIDReversed;
    private String                            operationType;
    private Long                              requestTimestamp;
    private String                            marketingMsg;
    private String                            warningMsg;
    private String                            messageCode;
    private String                            statusCode;
    private Double                            totalConsumed;
    private Double                            amount;
    private Boolean                           reconciled = false;
    private String                            preAuthOperationID;
    private PostPaidTransaction               postPaidTransaction;
    private Set<PostPaidConsumeVoucherDetail> postPaidConsumeVoucherDetail = new HashSet<PostPaidConsumeVoucherDetail>(0);

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getOperationIDReversed() {
        return operationIDReversed;
    }

    public void setOperationIDReversed(String operationIDReversed) {
        this.operationIDReversed = operationIDReversed;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotalConsumed() {
        return totalConsumed;
    }

    public void setTotalConsumed(Double totalConsumed) {
        this.totalConsumed = totalConsumed;
    }

    public Boolean getReconciled() {
        return reconciled;
    }

    public void setReconciled(Boolean reconciled) {
        this.reconciled = reconciled;
    }

    public Double getAmount() {

        return amount;
    }

    public void setAmount(Double amount) {

        this.amount = amount;
    }

    public String getPreAuthOperationID() {

        return preAuthOperationID;
    }

    public void setPreAuthOperationID(String preAuthOperationID) {

        this.preAuthOperationID = preAuthOperationID;
    }

    public PostPaidTransaction getPostPaidTransaction() {
        return postPaidTransaction;
    }

    public void setPostPaidTransaction(PostPaidTransaction postPaidTransaction) {
        this.postPaidTransaction = postPaidTransaction;
    }

    public Set<PostPaidConsumeVoucherDetail> getPostPaidConsumeVoucherDetail() {
        return postPaidConsumeVoucherDetail;
    }

    public void setPostPaidConsumeVoucherDetail(Set<PostPaidConsumeVoucherDetail> postPaidConsumeVoucherDetail) {
        this.postPaidConsumeVoucherDetail = postPaidConsumeVoucherDetail;
    }

}
