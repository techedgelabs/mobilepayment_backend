package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PartnerListInfoDataResponse implements Serializable {

    /**
     * 
     */
    private static final long           serialVersionUID      = 1540751499360159260L;

    private String                      statusCode;

    private List<PartnerDetailInfoData> partnerDetailInfoData = new ArrayList<PartnerDetailInfoData>(0);


    private List<CategoryEarnDataDetail>    categoryDataDetailList    = new ArrayList<CategoryEarnDataDetail>(0);

    public List<CategoryEarnDataDetail> getCategoryDataDetailList() {
    
        return categoryDataDetailList;
    }

    public void setCategoryDataDetailList(List<CategoryEarnDataDetail> categoryDataDetailList) {
    
        this.categoryDataDetailList = categoryDataDetailList;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<PartnerDetailInfoData> getPartnerDetailInfoData() {

        return partnerDetailInfoData;
    }

    public void setPartnerDetailInfoData(List<PartnerDetailInfoData> partnerDetailInfoData) {

        this.partnerDetailInfoData = partnerDetailInfoData;
    }

}
