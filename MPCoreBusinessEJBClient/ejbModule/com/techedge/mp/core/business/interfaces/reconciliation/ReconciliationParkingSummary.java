package com.techedge.mp.core.business.interfaces.reconciliation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.techedge.mp.core.business.interfaces.StatusHelper;

public class ReconciliationParkingSummary implements Serializable {
        
    /**
     * 
     */
    private static final long serialVersionUID = -5727339930484232860L;
    
    public static final String SUMMARY_STATUS_PAYMENT_ERROR        = StatusHelper.PARKING_STATUS_ENDED;
    public static final String SUMMARY_STATUS_PAYMENT_DELETE_ERROR = StatusHelper.PARKING_STATUS_FAILED;
    
    
    @SuppressWarnings("serial")
    HashMap<String, List<ReconciliationInfo>> summary = new HashMap<String, List<ReconciliationInfo>>() {
        {
            put(SUMMARY_STATUS_PAYMENT_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_STATUS_PAYMENT_DELETE_ERROR, new ArrayList<ReconciliationInfo>(0));
        }
    };
    
    String statusCode;
    
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<ReconciliationInfo> getSummary(String type) {
        if (!summary.containsKey(type)) {
            return new ArrayList<ReconciliationInfo>(0);
        }
        
        return summary.get(type);
    }

    public boolean addToSummary(String type, ReconciliationInfo reconciliationInfo) {
        List<ReconciliationInfo> reconciliationInfoList;
        
        if (!summary.containsKey(type)) {
            return false;
        }
        
        reconciliationInfoList = summary.get(type);
        reconciliationInfoList.add(reconciliationInfo);
        summary.put(type, reconciliationInfoList);
        return true;
    }
    
    public Integer getProcessed() {
        return new Integer(summary.get(SUMMARY_STATUS_PAYMENT_ERROR).size() +
                summary.get(SUMMARY_STATUS_PAYMENT_DELETE_ERROR).size());
    }
}
