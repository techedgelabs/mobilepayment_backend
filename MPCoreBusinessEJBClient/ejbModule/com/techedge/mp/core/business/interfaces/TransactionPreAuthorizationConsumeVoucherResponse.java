package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class TransactionPreAuthorizationConsumeVoucherResponse implements Serializable {

    /**
     * 
     */
    private static final long          serialVersionUID = -874033004449214070L;

    private String                     statusCode;

    private FidelityConsumeVoucherData fidelityConsumeVoucherData;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public FidelityConsumeVoucherData getFidelityConsumeVoucherData() {

        return fidelityConsumeVoucherData;
    }

    public void setFidelityConsumeVoucherData(FidelityConsumeVoucherData fidelityConsumeVoucherData) {

        this.fidelityConsumeVoucherData = fidelityConsumeVoucherData;
    }

}
