package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PaymentMethod implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3969037328793274123L;
    /**
	 * 
	 */

    private Long              id;
    private String            type;
    public Long getId() {
    
        return id;
    }
    public void setId(Long id) {
    
        this.id = id;
    }
    public String getType() {
    
        return type;
    }
    public void setType(String type) {
    
        this.type = type;
    }

   

}
