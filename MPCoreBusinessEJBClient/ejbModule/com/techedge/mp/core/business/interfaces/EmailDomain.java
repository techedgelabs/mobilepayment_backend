package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class EmailDomain implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6500265694059267707L;

    public EmailDomain() {}

    private long   id;
    private String email;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
