package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;

import com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionResult;

public class PostPaidReverseShopTransactionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3535296326931310736L;
	
	private String statusCode;
	private String messageCode;
	private PaymentTransactionResult paymentTransactionResult;

	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public PaymentTransactionResult getPaymentTransactionResult() {
		return paymentTransactionResult;
	}
	public void setPaymentTransactionResult(PaymentTransactionResult paymentTransactionResult) {
		this.paymentTransactionResult = paymentTransactionResult;
	}
	
	
	
	
	
	
}
