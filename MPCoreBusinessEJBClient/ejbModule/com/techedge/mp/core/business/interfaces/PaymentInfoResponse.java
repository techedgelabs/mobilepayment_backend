package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PaymentInfoResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1044090420828645828L;

	
	private String statusCode;
	private PaymentInfo paymentInfo;
	
	public String getStatusCode() {
		return statusCode;
	}
	
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
}
