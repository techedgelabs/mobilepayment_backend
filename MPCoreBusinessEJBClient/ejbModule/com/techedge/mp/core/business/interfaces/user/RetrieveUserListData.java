package com.techedge.mp.core.business.interfaces.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveUserListData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3307851412746064243L;

	private String statusCode;
	private List<User> userList = new ArrayList<User>(0);
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public List<User> getUserList() {
		return userList;
	}
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
}
