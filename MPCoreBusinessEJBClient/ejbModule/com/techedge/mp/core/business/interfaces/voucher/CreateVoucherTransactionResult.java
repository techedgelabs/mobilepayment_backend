package com.techedge.mp.core.business.interfaces.voucher;

import java.io.Serializable;

public class CreateVoucherTransactionResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2851484085955623011L;
    private String statusCode;
    private String voucherTransactionID;
    private String voucherCode;
    private Integer pinCheckMaxAttempts;
    
    public String getStatusCode() {
    
        return statusCode;
    }
    
    public void setStatusCode(String statusCode) {
    
        this.statusCode = statusCode;
    }
    
    public String getVoucherTransactionID() {
    
        return voucherTransactionID;
    }
    
    public void setVoucherTransactionID(String voucherTransactionID) {
    
        this.voucherTransactionID = voucherTransactionID;
    }
    
    public String getVoucherCode() {
    
        return voucherCode;
    }
    
    public void setVoucherCode(String voucherCode) {
    
        this.voucherCode = voucherCode;
    }

    public Integer getPinCheckMaxAttempts() {
    
        return pinCheckMaxAttempts;
    }

    public void setPinCheckMaxAttempts(Integer pinCheckMaxAttempts) {
    
        this.pinCheckMaxAttempts = pinCheckMaxAttempts;
    }
}
