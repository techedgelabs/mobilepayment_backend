package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StationInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7281546035710989550L;

	private String name;
    private String code;
    private String address;
    private String latitude;
    private String longitude;
    private List<PumpInfo> pumpList = new ArrayList<PumpInfo>(0);
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public List<PumpInfo> getPumpList() {
		return pumpList;
	}
	public void setPumpList(List<PumpInfo> pumpList) {
		this.pumpList = pumpList;
	}
}
