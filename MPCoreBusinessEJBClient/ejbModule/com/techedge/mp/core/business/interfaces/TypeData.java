package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class TypeData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2087390383307941112L;

    private String            field;
    private String            value;

    public TypeData() {

    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
