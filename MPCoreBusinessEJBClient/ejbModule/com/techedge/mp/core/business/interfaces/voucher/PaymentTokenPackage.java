package com.techedge.mp.core.business.interfaces.voucher;

import java.io.Serializable;

public class PaymentTokenPackage implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2648034104254351993L;

    private Long              id;

    private String            data;
    
    private String            type;

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getData() {

        return data;
    }

    public void setData(String data) {

        this.data = data;
    }

    public String getType() {
    
        return type;
    }

    public void setType(String type) {
    
        this.type = type;
    }

}
