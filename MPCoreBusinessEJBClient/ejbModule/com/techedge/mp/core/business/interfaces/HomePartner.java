package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class HomePartner implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5030465073436961193L;

    private Integer sequence;

    private String name;

    public Integer getSequence() {
    
        return sequence;
    }

    public void setSequence(Integer sequence) {
    
        this.sequence = sequence;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

}
