package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;

public class PostPaidApproveShopTransactionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3535296326931310736L;
	
	private String statusCode;
	private String messageCode;
	private Integer pinCheckMaxAttempts;

	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public Integer getPinCheckMaxAttempts() {
		return pinCheckMaxAttempts;
	}
	public void setPinCheckMaxAttempts(Integer pinCheckMaxAttempts) {
		this.pinCheckMaxAttempts = pinCheckMaxAttempts;
	}
	
	
	
	
	
	
}
