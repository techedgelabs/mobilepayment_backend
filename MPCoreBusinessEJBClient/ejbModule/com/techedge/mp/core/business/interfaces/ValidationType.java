package com.techedge.mp.core.business.interfaces;

public enum ValidationType {
    
    MOBILE_PHONE("mobile_phone"),
    DEVICE("device");
    
    private final String value;

    ValidationType(final String value) {
        this.value = value;
    }

    public String getValue() { 
    
        return value; 
    }
}
