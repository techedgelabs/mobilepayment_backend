package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.user.PersonalData;


public class TermsOfService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8764321063338985566L;
	
    private long id;
    private PersonalData personalData;
    private String keyval;
    private Boolean accepted;
    private Boolean valid;
	
    public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public PersonalData getPersonalData() {
		return personalData;
	}
	public void setPersonalData(PersonalData personalData) {
		this.personalData = personalData;
	}
	
	public String getKeyval() {
		return keyval;
	}
	public void setKeyval(String keyval) {
		this.keyval = keyval;
	}
	
	public Boolean getAccepted() {
		return accepted;
	}
	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}
	
    public Boolean getValid() {
    
        return valid;
    }
    public void setValid(Boolean valid) {
    
        this.valid = valid;
    }
}
