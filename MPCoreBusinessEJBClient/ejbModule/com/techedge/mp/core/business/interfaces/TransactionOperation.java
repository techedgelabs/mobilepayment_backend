package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class TransactionOperation implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1058891585136388223L;

    private long              id;

    private Transaction       transaction;

    private String            operationType;

    private Integer           sequenceID;

    private String            operationId;

    private Long              requestTimestamp;

    private String            remoteTransactionId;

    private String            status;

    private String            code;

    private String            message;

    private Integer           amount;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public Transaction getTransaction() {

        return transaction;
    }

    public void setTransaction(Transaction transaction) {

        this.transaction = transaction;
    }

    public String getOperationType() {

        return operationType;
    }

    public void setOperationType(String operationType) {

        this.operationType = operationType;
    }

    public Integer getSequenceID() {

        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {

        this.sequenceID = sequenceID;
    }

    public String getOperationId() {

        return operationId;
    }

    public void setOperationId(String operationId) {

        this.operationId = operationId;
    }

    public Long getRequestTimestamp() {

        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {

        this.requestTimestamp = requestTimestamp;
    }

    public String getRemoteTransactionId() {

        return remoteTransactionId;
    }

    public void setRemoteTransactionId(String remoteTransactionId) {

        this.remoteTransactionId = remoteTransactionId;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }

    public Integer getAmount() {

        return amount;
    }

    public void setAmount(Integer amount) {

        this.amount = amount;
    }

}
