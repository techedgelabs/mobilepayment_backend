package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.util.Map;

public class UpdateContactKeyBulkResult implements Serializable {

    /**
     * 
     */
    private static final long   serialVersionUID = -3626646734103351417L;

    private StatusCodeEnum      statusCode;

    private String              operationId;

    private Map<String, String> errorList;

    public StatusCodeEnum getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(StatusCodeEnum statusCode) {

        this.statusCode = statusCode;
    }

    public String getOperationId() {

        return operationId;
    }

    public void setOperationId(String operationId) {

        this.operationId = operationId;
    }

    public Map<String, String> getErrorList() {
    
        return errorList;
    }

    public void setErrorList(Map<String, String> errorList) {
    
        this.errorList = errorList;
    }

   

}