package com.techedge.mp.core.business.interfaces;

public enum PromotionVoucherStatus {

    //1 = new, 2 = assigned, 3 = consumed, 4 = sent, 5 = deleted, 6 = invalid
    NEW(1), ASSIGNED(2), CONSUMED(3), SENT(4), DELETED(5), INVALID(6);

    private final Integer value;

    PromotionVoucherStatus(final Integer newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }

}
