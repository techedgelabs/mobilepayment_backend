package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PvGenerationData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -797322601449001592L;

    private String            stationId;

    private String            statusCode;

    public PvGenerationData() {

    }

    public String getStationId() {

        return stationId;
    }

    public void setStationId(String stationId) {

        this.stationId = stationId;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

}
