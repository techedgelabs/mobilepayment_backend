package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DeleteTestersData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1901461604120648436L;
	
	private String statusCode;
	private List<TesterUser> testerList = new ArrayList<TesterUser>(0);
	
	
	public DeleteTestersData() {
		
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public List<TesterUser> getTesterList() {
		return testerList;
	}
	public void setTesterList(List<TesterUser> testerList) {
		this.testerList = testerList;
	}
}
