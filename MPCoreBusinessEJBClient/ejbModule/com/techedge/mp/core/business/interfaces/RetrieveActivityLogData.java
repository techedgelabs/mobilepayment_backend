package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveActivityLogData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1086013348183167920L;
	
	private String statusCode;
	private List<ActivityLog> activityLogList = new ArrayList<ActivityLog>(0);
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public List<ActivityLog> getActivityLogList() {
		return activityLogList;
	}
	public void setActivityLogList(List<ActivityLog> activityLogList) {
		this.activityLogList = activityLogList;
	}
}
