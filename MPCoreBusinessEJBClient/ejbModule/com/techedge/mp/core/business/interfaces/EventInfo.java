package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class EventInfo implements Serializable{
	
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -5294537725041709791L;

	protected String sequence;
	
    protected String eventType;
  	
    protected Double eventAmount;
   
    protected String transactionResult;
    protected String errorCode;
    protected String errorDescription;
    /**
	 * @return the sequence
	 */
	public String getSequence() {
		return sequence;
	}
	/**
	 * @param sequence the sequence to set
	 */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	
	/**
	 * @return the eventType
	 */
	public String getEventType() {
		return eventType;
	}
	/**
	 * @param eventType the eventType to set
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	/**
	 * @return the eventAmount
	 */
	public Double getEventAmount() {
		return eventAmount;
	}
	/**
	 * @param eventAmount the eventAmount to set
	 */
	public void setEventAmount(Double eventAmount) {
		this.eventAmount = eventAmount;
	}
	/**
	 * @return the transactionResult
	 */
	public String getTransactionResult() {
		return transactionResult;
	}
	/**
	 * @param transactionResult the transactionResult to set
	 */
	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return errorDescription;
	}
	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
    

}
