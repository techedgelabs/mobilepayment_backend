package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.PostPaidTransaction;

public class PostPaidGetTransactionDetailResponse extends PostPaidTransaction implements Serializable {

    /**
	 * 
	 */
    private static final long                   serialVersionUID       = -3535296326931310736L;

    private String                              statusCode;
    private String                              messageCode;

    private String                              sourceType;
    private String                              sourceStatus;

    private String                              transactionType;

    /*
    private String                              transactionID;
    private String                              transactionStatus;
    private String                              transactionSubStatus;
    private Date                                creationDate;

    private String                              stationID;
    private String                              stationName;
    private String                              stationAddress;
    private String                              stationCity;
    private String                              stationProvince;
    private String                              stationCountry;
    private Double                              stationLatitude;
    private Double                              stationLongitude;
    */
    private String                              pumpID;
    private String                              pumpStatus;
    private String                              pumpNumber;
    private String                              pumpFuelType;

    private String                              cashID;
    private String                              cashNumber;
    
    private String                              panCode;

    /*
    private Double                              amount;

    //private Set<ReceiptData>                    receiptDataList                    = new HashSet<ReceiptData>();

    private Set<PaymentInfo>                    paymentDataList        = new HashSet<PaymentInfo>();
    private Set<PostPaidCartData>               postPaidCartDataList   = new HashSet<PostPaidCartData>();
    private Set<PostPaidRefuelData>             postPaidRefuelDataList = new HashSet<PostPaidRefuelData>();
    private Set<PostPaidLoadLoyaltyCreditsData> LoyaltyCardDataList    = new HashSet<PostPaidLoadLoyaltyCreditsData>();
    private Set<PostPaidConsumeVoucherData>     voucherDataList        = new HashSet<PostPaidConsumeVoucherData>();

    private Boolean                             useVoucher;
    */
    
    private String                              status;
    private String                              statusTitle;
    private String                              statusDescription;
    private String                              subStatus;
    
    private String                              surveyUrl;
    private String                              gfgElectronicInvoiceID;
    
    
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourceStatus() {
        return sourceStatus;
    }

    public void setSourceStatus(String sourceStatus) {
        this.sourceStatus = sourceStatus;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /*
    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

     public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationAddress() {
        return stationAddress;
    }

    public void setStationAddress(String stationAddress) {
        this.stationAddress = stationAddress;
    }

    public String getStationCity() {
        return stationCity;
    }

    public void setStationCity(String stationCity) {
        this.stationCity = stationCity;
    }

    public String getStationProvince() {
        return stationProvince;
    }

    public void setStationProvince(String stationProvince) {
        this.stationProvince = stationProvince;
    }

    public String getStationCountry() {
        return stationCountry;
    }

    public void setStationCountry(String stationCountry) {
        this.stationCountry = stationCountry;
    }

    public Double getStationLatitude() {
        return stationLatitude;
    }

    public void setStationLatitude(Double stationLatitude) {
        this.stationLatitude = stationLatitude;
    }

    public Double getStationLongitude() {
        return stationLongitude;
    }

    public void setStationLongitude(Double stationLongitude) {
        this.stationLongitude = stationLongitude;
    }
    */
    public String getPumpID() {
        return pumpID;
    }

    public void setPumpID(String pumpID) {
        this.pumpID = pumpID;
    }

    public String getPumpStatus() {
        return pumpStatus;
    }

    public void setPumpStatus(String pumpStatus) {
        this.pumpStatus = pumpStatus;
    }

    public String getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(String pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public String getPumpFuelType() {
        return pumpFuelType;
    }

    public void setPumpFuelType(String pumpFuelType) {
        this.pumpFuelType = pumpFuelType;
    }

    public String getCashID() {
        return cashID;
    }

    public void setCashID(String cashID) {
        this.cashID = cashID;
    }

    public String getCashNumber() {
        return cashNumber;
    }

    public void setCashNumber(String cashNumber) {
        this.cashNumber = cashNumber;
    }
    /*
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Set<PostPaidCartData> getPostPaidCartDataList() {
        return postPaidCartDataList;
    }

    public void setPostPaidCartDataList(Set<PostPaidCartData> postPaidCartDataList) {
        this.postPaidCartDataList = postPaidCartDataList;
    }

    public Set<PostPaidRefuelData> getPostPaidRefuelDataList() {
        return postPaidRefuelDataList;
    }

    public void setPostPaidRefuelDataList(Set<PostPaidRefuelData> postPaidRefuelDataList) {
        this.postPaidRefuelDataList = postPaidRefuelDataList;
    }

    public Set<PaymentInfo> getPaymentDataList() {
        return paymentDataList;
    }

    public void setPaymentDataList(Set<PaymentInfo> paymentDataList) {
        this.paymentDataList = paymentDataList;
    }

    public Set<PostPaidLoadLoyaltyCreditsData> getLoyaltyCardDataList() {
        return LoyaltyCardDataList;
    }

    public void setLoyaltyCardDataList(Set<PostPaidLoadLoyaltyCreditsData> loyaltyCardDataList) {
        LoyaltyCardDataList = loyaltyCardDataList;
    }

    public Set<PostPaidConsumeVoucherData> getVoucherDataList() {
        return voucherDataList;
    }

    public void setVoucherDataList(Set<PostPaidConsumeVoucherData> voucherDataList) {
        this.voucherDataList = voucherDataList;
    }
    
//    public Set<ReceiptData> getReceiptDataList() {
//        return receiptDataList;
//    }
    
//    public void setReceiptDataList(Set<ReceiptData> receiptDataList) {
        this.receiptDataList = receiptDataList;
//    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionSubStatus() {
        return transactionSubStatus;
    }

    public void setTransactionSubStatus(String transactionSubStatus) {
        this.transactionSubStatus = transactionSubStatus;
    }

    public Boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }
    */

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(String subStatus) {
        this.subStatus = subStatus;
    }

    public String getPanCode() {
        return panCode;
    }

    public void setPanCode(String panCode) {
        this.panCode = panCode;
    }

    public String getSurveyUrl() {
        return surveyUrl;
    }

    public void setSurveyUrl(String surveyUrl) {
        this.surveyUrl = surveyUrl;
    }

    public void setGFGElectronicInvoiceID(String gfgElectronicInvoiceID) {

        this.gfgElectronicInvoiceID = gfgElectronicInvoiceID;
    }
    
    public String getGFGElectronicInvoiceID() {

        return gfgElectronicInvoiceID;
    }
}
