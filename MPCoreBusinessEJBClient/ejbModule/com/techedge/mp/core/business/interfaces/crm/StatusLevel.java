package com.techedge.mp.core.business.interfaces.crm;


public enum StatusLevel {
    INFO(0),
    WARNING(1),
    ERROR(2);
    
    private int level;
    
    private StatusLevel(int level) {
        this.level = level;
    }
    
    public static StatusLevel valueOf(int value) {
        switch (value) {
            case 0:
                return StatusLevel.INFO;

            case 1:
                return StatusLevel.WARNING;

            case 2:
                return StatusLevel.ERROR;

            default:
                return null;
        }
    }
    
    public String getName() {
        return this.name();
    }
    
    public int getValue() {
        return level;
    }
    
}
