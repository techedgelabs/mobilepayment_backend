package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class DpanStatus implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -3185530060228182098L;
    
    private String     DPAN;
    private DpanStatusEnum     status;
	public String getDPAN() {
		return DPAN;
	}
	public void setDPAN(String dPAN) {
		DPAN = dPAN;
	}
	public DpanStatusEnum getStatus() {
		return status;
	}
	public void setStatus(DpanStatusEnum status) {
		this.status = status;
	} 
}
