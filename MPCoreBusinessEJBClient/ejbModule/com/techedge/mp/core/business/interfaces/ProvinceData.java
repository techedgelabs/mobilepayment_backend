package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class ProvinceData implements Serializable {

 
    /**
     * 
     */
    private static final long serialVersionUID = 3457474244925234663L;

    private String            name;

    private String            region;

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getRegion() {

        return region;
    }

    public void setRegion(String region) {

        this.region = region;
    }

}
