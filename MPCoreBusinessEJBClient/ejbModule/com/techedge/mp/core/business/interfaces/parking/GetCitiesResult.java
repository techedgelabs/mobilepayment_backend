package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetCitiesResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5527071852908111918L;

    private String            statusCode;

    List<ParkingCity>         parkingCities    = new ArrayList<ParkingCity>(0);

    public List<ParkingCity> getParkingCities() {

        return parkingCities;
    }

    public void setParkingCities(List<ParkingCity> parkingCities) {

        this.parkingCities = parkingCities;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

}
