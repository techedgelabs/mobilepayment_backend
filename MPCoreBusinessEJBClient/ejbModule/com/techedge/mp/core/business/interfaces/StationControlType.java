package com.techedge.mp.core.business.interfaces;


public enum StationControlType {
    GET_LAST_REFUEL("GET_LAST_REFUEL");
    
    private String code;
    
    private StationControlType(String code) {
        this.code = code;
    }
    
    public static StationControlType getType(String value) {
        for (StationControlType stationControlType : StationControlType.values()) {
            if (stationControlType.getCode().equals(value)) {
                return stationControlType;
            }
        }
        
        return null;
    }
    
    public String getCode() {
        return code;
    }
}
