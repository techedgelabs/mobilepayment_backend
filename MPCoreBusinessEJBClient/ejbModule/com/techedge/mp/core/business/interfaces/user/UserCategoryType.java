package com.techedge.mp.core.business.interfaces.user;

public enum UserCategoryType {

    NEW_PAYMENT_FLOW("UTENTI_NUOVO_FLUSSO_VOUCHER"),
    OLD_PAYMENT_FLOW("UTENTI_VECCHIO_FLUSSO_VOUCHER"),
    NEW_ACQUIRER_FLOW("UTENTI_NUOVO_ACQUIRER"),
    GUEST_FLOW("UTENTI_GUEST"),
    BUSINESS("BUSINESS"),
    MULTICARD_FLOW("UTENTI_MULTICARD"),
    UTENTI_REFUELING_OAUTH2("UTENTI_REFUELING_OAUTH2");

    private final String code;

    UserCategoryType(final String code) {
        this.code = code;
    }

    public String getCode() { 
    
        return code; 
    }
    
}
