package com.techedge.mp.core.business.interfaces.user;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.Address;
import com.techedge.mp.core.business.interfaces.TermsOfService;


public class PersonalData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5507205074932309131L;
	
	private long id;
	private String firstName;
    private String lastName;
    private String fiscalCode;
    private Date birthDate;
    private String birthMunicipality;
    private String birthProvince;
    private String language;
    private String sex;
    private String securityDataEmail;
    private String securityDataPassword;
    private Address address;
    private Address billingAddress;
    private Set<TermsOfService> termsOfServiceData = new HashSet<TermsOfService>(0);
    
    public PersonalData() {}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}
	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthMunicipality() {
		return birthMunicipality;
	}
	public void setBirthMunicipality(String birthMunicipality) {
		this.birthMunicipality = birthMunicipality;
	}

	public String getBirthProvince() {
		return birthProvince;
	}
	public void setBirthProvince(String birthProvince) {
		this.birthProvince = birthProvince;
	}

	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSecurityDataEmail() {
		return securityDataEmail;
	}
	public void setSecurityDataEmail(String securityDataEmail) {
		this.securityDataEmail = securityDataEmail;
	}

	public String getSecurityDataPassword() {
		return securityDataPassword;
	}
	public void setSecurityDataPassword(String securityDataPassword) {
		this.securityDataPassword = securityDataPassword;
	}

	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	public Set<TermsOfService> getTermsOfServiceData() {
		return termsOfServiceData;
	}
	public void setTermsOfServiceData(Set<TermsOfService> termsOfServiceData) {
		this.termsOfServiceData = termsOfServiceData;
	}
}
