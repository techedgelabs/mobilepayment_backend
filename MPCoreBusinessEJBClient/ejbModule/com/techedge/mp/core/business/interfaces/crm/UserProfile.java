package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class UserProfile implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -2012142293514364559L;

    private HashMap<String, Object> parameters = new HashMap<String, Object>();
    
    //private static final SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyy-MM-dd+hh:mm:ss");
    private static final SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
    private static final DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ENGLISH);
        
    public static UserProfile getUserProfileForEventRegistration(String fiscalCode, Date timestamp, String name, String surname, String email, Date dateOfBirth, 
            boolean flagNotification, boolean flagCreditCard, String cardBrand, ClusterType cluster) {
        
        UserProfile userProfile = new UserProfile();
        userProfile.parameters.clear();
        
        userProfile.parameters.put(Parameter.CODICE_FISCALE.name(), fiscalCode);
        
        /*
        try {
            userProfile.parameters.put(Parameter.DATA.name(), sdf.parse(timestamp.toString()));
        }
        catch (ParseException ex) {
            System.err.println("Errore nel parsing della data: " + ex.getMessage());
            userProfile.parameters.put(Parameter.DATA.name(), sdf.format(timestamp));
        }
        */
        userProfile.parameters.put(Parameter.DATA.name(), sdfDate.format(timestamp));
        userProfile.parameters.put(Parameter.NOME.name(), name);
        userProfile.parameters.put(Parameter.COGNOME.name(), surname);
        userProfile.parameters.put(Parameter.INDIRIZZO_EMAIL.name(), email);
        userProfile.parameters.put(Parameter.DATA_DI_NASCITA.name(), sdfDate.format(dateOfBirth));
        userProfile.parameters.put(Parameter.FG_NOTIFICA.name(), (flagNotification ? new String("1") : new String("0")));
        userProfile.parameters.put(Parameter.FG_1.name(), (flagCreditCard ? new Integer(1) : new Integer(0)));
        userProfile.parameters.put(Parameter.CARTA.name(), (cardBrand != null ) ? cardBrand.toUpperCase() : cardBrand);
        userProfile.parameters.put(Parameter.CLUSTER.name(), cluster.getValue());
        
        return userProfile;
    }

    public static UserProfile getUserProfileForEventRefueling(String fiscalCode, Date timestamp, String surname, String pv, String product, boolean flagPayment, Integer loyaltyCredits, 
            Double refuelQuantity, Double amount, String refuelMode, boolean flagNotification, boolean flagCreditCard, String cardBrand, ClusterType cluster) {
        
        UserProfile userProfile = new UserProfile();
        userProfile.parameters.clear();
        numberFormat.applyPattern("##0.000");
        
        userProfile.parameters.put(Parameter.CODICE_FISCALE.name(), fiscalCode);
        /*
        try {
            userProfile.parameters.put(Parameter.DATA.name(), sdf.parse(timestamp.toString()));
        }
        catch (ParseException ex) {
            System.err.println("Errore nel parsing della data: " + ex.getMessage());
            userProfile.parameters.put(Parameter.DATA.name(), sdf.format(timestamp));
        }
        */
        
        /*
         * Mapping campo prodotto per evento di caricamento punti da POS
         * 
         * Per uniformare i prodotti inviati a seguito di eventi di rifornimento e di caricamento punti si utilizzerà il seguente mapping
         * 
         * Gasolio      --> Gasolio
         * S.Piombo     --> SenzaPb
         * GPL          --> GPL
         * BluSuper     --> BluSuper
         * Eni Diesel + --> Diesel+
         * Metano       --> Metano
         * 
         */
        
        //System.out.println("UserProfile.getUserProfileForEventLoyaltyCredits");
        
        //System.out.println("product: " + product);
        
        String crmProduct = product;
        if (product.equals("S.Piombo")) {
            crmProduct = "SenzaPb";
        }
        if (product.equals("Eni Diesel +")) {
            crmProduct = "Diesel+";
        }
        
        //System.out.println("crmProduct: " + crmProduct);
        
        refuelQuantity = Math.floor(refuelQuantity*100) / 100;
        
        userProfile.parameters.put(Parameter.DATA.name(), sdfDate.format(timestamp));
        userProfile.parameters.put(Parameter.IMPIANTO.name(), pv);
        userProfile.parameters.put(Parameter.PRODOTTO.name(), crmProduct);
        userProfile.parameters.put(Parameter.FLAG_PAGAMENTO.name(), (flagPayment ? new String("1") : new String("0")));
        userProfile.parameters.put(Parameter.PUNTI.name(), loyaltyCredits);
        userProfile.parameters.put(Parameter.LITRI.name(), refuelQuantity);
        userProfile.parameters.put(Parameter.MODALITA_OPERATIVA.name(), refuelMode);
        userProfile.parameters.put(Parameter.FG_NOTIFICA.name(), (flagNotification ? new String("1") : new String("0")));
        userProfile.parameters.put(Parameter.FG_1.name(), (flagCreditCard ? new Integer(1) : new Integer(0)));
        userProfile.parameters.put(Parameter.CARTA.name(), (cardBrand != null ) ? cardBrand.toUpperCase() : cardBrand);
        userProfile.parameters.put(Parameter.CLUSTER.name(), cluster.getValue());
        userProfile.parameters.put(Parameter.IMPORTO.name(), amount);
        userProfile.parameters.put(Parameter.COGNOME.name(), surname);
        
        
        return userProfile;
    }

    public static UserProfile getUserProfileForEventLoyaltyCredits(String fiscalCode, Date timestamp, String pv, String product, boolean flagPayment, 
            Integer loyaltyCredits, Double refuelQuantity, String refuelMode, boolean flagNotification, boolean flagCreditCard, String cardBrand, ClusterType cluster) {
        
        UserProfile userProfile = new UserProfile();
        userProfile.parameters.clear();
        numberFormat.applyPattern("##0.000");
        
        userProfile.parameters.put(Parameter.CODICE_FISCALE.name(), fiscalCode);
        /*
        try {
            userProfile.parameters.put(Parameter.DATA.name(), sdf.parse(timestamp.toString()));
        }
        catch (ParseException ex) {
            System.err.println("Errore nel parsing della data: " + ex.getMessage());
            userProfile.parameters.put(Parameter.DATA.name(), sdf.format(timestamp));
        }
        */
        
        /*
         * Mapping campo prodotto per evento di caricamento punti da POS
         * 
         * Per uniformare i prodotti inviati a seguito di eventi di rifornimento e di caricamento punti si utilizzerà il seguente mapping
         * 
         * Gasolio      --> Gasolio
         * S.Piombo     --> SenzaPb
         * GPL          --> GPL
         * BluSuper     --> BluSuper
         * Eni Diesel + --> Diesel+
         * Metano       --> Metano
         * 
         */
        
        //System.out.println("UserProfile.getUserProfileForEventLoyaltyCredits");
        
        //System.out.println("product: " + product);
        
        String crmProduct = product;
        if (product.equals("S.Piombo")) {
            crmProduct = "SenzaPb";
        }
        if (product.equals("Eni Diesel +")) {
            crmProduct = "Diesel+";
        }
        
        //System.out.println("crmProduct: " + crmProduct);
        
        userProfile.parameters.put(Parameter.DATA.name(), sdfDate.format(timestamp));
        userProfile.parameters.put(Parameter.IMPIANTO.name(), pv);
        userProfile.parameters.put(Parameter.PRODOTTO.name(), crmProduct);
        userProfile.parameters.put(Parameter.FLAG_PAGAMENTO.name(), (flagPayment ? new String("1") : new String("0")));
        userProfile.parameters.put(Parameter.PUNTI.name(), loyaltyCredits);
        userProfile.parameters.put(Parameter.LITRI.name(), numberFormat.format(refuelQuantity));
        userProfile.parameters.put(Parameter.MODALITA_OPERATIVA.name(), refuelMode);
        userProfile.parameters.put(Parameter.FG_NOTIFICA.name(), (flagNotification ? new String("1") : new String("0")));
        userProfile.parameters.put(Parameter.FG_1.name(), (flagCreditCard ? new Integer(1) : new Integer(0)));
        userProfile.parameters.put(Parameter.CARTA.name(), (cardBrand != null ) ? cardBrand.toUpperCase() : cardBrand);
        userProfile.parameters.put(Parameter.CLUSTER.name(), cluster.getValue());
        
        return userProfile;
    }
    
    public static UserProfile getUserProfileForEventCreditCard(String fiscalCode, Date timestamp, boolean flagNotification, boolean flagCreditCard, 
            String cardBrand, ClusterType cluster) {
        
        UserProfile userProfile = new UserProfile();
        userProfile.parameters.clear();
        
        userProfile.parameters.put(Parameter.CODICE_FISCALE.name(), fiscalCode);
        /*
        try {
            userProfile.parameters.put(Parameter.DATA.name(), sdf.parse(timestamp.toString()));
        }
        catch (ParseException ex) {
            System.err.println("Errore nel parsing della data: " + ex.getMessage());
            userProfile.parameters.put(Parameter.DATA.name(), sdf.format(timestamp));
        }
        */
        userProfile.parameters.put(Parameter.DATA.name(), sdfDate.format(timestamp));
        userProfile.parameters.put(Parameter.FG_NOTIFICA.name(), (flagNotification ? new String("1") : new String("0")));
        userProfile.parameters.put(Parameter.FG_1.name(), (flagCreditCard ? new Integer(1) : new Integer(0)));
        userProfile.parameters.put(Parameter.CARTA.name(), (cardBrand != null ) ? cardBrand.toUpperCase() : cardBrand);
        userProfile.parameters.put(Parameter.CLUSTER.name(), cluster.getValue());
        
        return userProfile;
    }
    
    public static UserProfile getUserProfileForEventPromo4Me(String fiscalCode, Date timestamp, Integer loyaltyCredits, Double refuelQuantity) {
        
        UserProfile userProfile = new UserProfile();
        userProfile.parameters.clear();
        numberFormat.applyPattern("##0.000");
        
        userProfile.parameters.put(Parameter.CODICE_FISCALE.name(), fiscalCode);
        /*
        try {
            userProfile.parameters.put(Parameter.DATA.name(), sdf.parse(timestamp.toString()));
        }
        catch (ParseException ex) {
            System.err.println("Errore nel parsing della data: " + ex.getMessage());
            userProfile.parameters.put(Parameter.DATA.name(), sdf.format(timestamp));
        }
        */
        userProfile.parameters.put(Parameter.DATA.name(), sdfDate.format(timestamp));
        userProfile.parameters.put(Parameter.PUNTI.name(), loyaltyCredits);
        userProfile.parameters.put(Parameter.LITRI.name(), numberFormat.format(refuelQuantity));
        
        return userProfile;
    }
    
    public static UserProfile getUserProfileForEventPromotions(String fiscalCode) {
        
        UserProfile userProfile = new UserProfile();
        userProfile.parameters.clear();
        
        return userProfile;
    }
    
    public HashMap<String, Object> getParameters() {
        return parameters;
    }
    
    public enum Parameter {
        CODICE_FISCALE,
        DATA,
        IMPIANTO,
        PRODOTTO,
        FLAG_PAGAMENTO,
        PUNTI,
        LITRI,
        MODALITA_OPERATIVA,
        NOME,
        COGNOME,
        INDIRIZZO_EMAIL,
        DATA_DI_NASCITA,
        FG_NOTIFICA,
        FG_1,
        CARTA,
        CLUSTER,
        IMPORTO;
        
        public static Parameter getValueOf(String value) {
            for (Parameter parameter : Parameter.values()) {
                if (parameter.name().equals(value)) {
                    return parameter;
                }
            }
            
            return null;
        }
    }
    
    public enum ClusterType {
        YOUENI("1"),
        ENIPAY("2"),
        YOUENI_ENIPAY("3");

        private String value;
        
        private ClusterType(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
        
        public static ClusterType getValueOf(String value) {
            for (ClusterType clusterType : ClusterType.values()) {
                if (clusterType.getValue().equals(value)) {
                    return clusterType;
                }
            }
            
            return null;
        }
        
    }
    
}
