package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;

import java.util.Set;

public class PaymentTransactionStatus implements Serializable{
	
	//TODO
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6516067635086212238L;
	private String shopLogin;
	private String acquirerID;
	private String paymentMode;
	private String shopTransactionID;
	private String bankTransactionID;
	private String authorizationCode;
	private String mpTransactionStatus;
	private String currency;
	private Double amount;
	private Date creationTimestamp;
	private Date lastModificationTimestamp;
	private Set<PaymentTransactionEvent> eventList = new HashSet<PaymentTransactionEvent>(0);
	
	
	public String getShopLogin() {
		return shopLogin;
	}
	public void setShopLogin(String shopLogin) {
		this.shopLogin = shopLogin;
	}
	public String getAcquirerID() {
		return acquirerID;
	}
	public void setAcquirerID(String acquirerID) {
		this.acquirerID = acquirerID;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getShopTransactionID() {
		return shopTransactionID;
	}
	public void setShopTransactionID(String shopTransactionID) {
		this.shopTransactionID = shopTransactionID;
	}
	public String getBankTransactionID() {
		return bankTransactionID;
	}
	public void setBankTransactionID(String bankTransactionID) {
		this.bankTransactionID = bankTransactionID;
	}
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}
	public String getMpTransactionStatus() {
		return mpTransactionStatus;
	}
	public void setMpTransactionStatus(String mpTransactionStatus) {
		this.mpTransactionStatus = mpTransactionStatus;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public Date getLastModificationTimestamp() {
		return lastModificationTimestamp;
	}
	public void setLastModificationTimestamp(Date lastModificationTimestamp) {
		this.lastModificationTimestamp = lastModificationTimestamp;
	}
	public Set<PaymentTransactionEvent> getEventList() {
		return eventList;
	}
	public void setEventList(Set<PaymentTransactionEvent> eventList) {
		this.eventList = eventList;
	}
	
	

	
	
	
	

}
