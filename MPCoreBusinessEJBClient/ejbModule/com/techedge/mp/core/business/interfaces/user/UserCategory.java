package com.techedge.mp.core.business.interfaces.user;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class UserCategory implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9104669307642365258L;

    private long              id;
    private String            name;
    private Set<UserType>     userTypes        = new HashSet<UserType>(0);

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserType> getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(Set<UserType> userTypes) {
        this.userTypes = userTypes;
    }

}
