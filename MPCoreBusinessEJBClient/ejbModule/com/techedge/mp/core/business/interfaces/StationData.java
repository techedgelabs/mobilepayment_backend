package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class StationData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3705317961721715323L;

    private String            stationID;
    private String            acquirerID;
    private String            apiKey;
    private String            encodedSecretKey;
    private String            currency;
    private String            groupAcquirer;

    public StationData() {

    }

    public String getStationID() {

        return stationID;
    }

    public void setStationID(String stationID) {

        this.stationID = stationID;
    }

    public String getAcquirerID() {
    
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
    
        this.acquirerID = acquirerID;
    }

    public String getApiKey() {

        return apiKey;
    }

    public void setApiKey(String apiKey) {

        this.apiKey = apiKey;
    }

    public String getEncodedSecretKey() {

        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {

        this.encodedSecretKey = encodedSecretKey;
    }

    public String getCurrency() {

        return currency;
    }

    public void setCurrency(String currency) {

        this.currency = currency;
    }

    public String getGroupAcquirer() {

        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {

        this.groupAcquirer = groupAcquirer;
    }

}
