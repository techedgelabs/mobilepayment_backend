package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class CountPendingTransactionsResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3028074633924102265L;

    private Integer           count;

    private String            statusCode;

    public Integer getCount() {

        return count;
    }

    public void setCount(Integer count) {

        this.count = count;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

}
