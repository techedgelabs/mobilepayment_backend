package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrievePoPTransactionListData implements Serializable {

    /**
	 * 
	 */
    private static final long                serialVersionUID               = 3307851412746064243L;

    private String                           statusCode;
    private List<PostPaidTransactionHistory> postPaidTransactionHistoryList = new ArrayList<PostPaidTransactionHistory>(0);

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<PostPaidTransactionHistory> getPostPaidTransactionHistoryList() {
        return postPaidTransactionHistoryList;
    }

    public void setPostPaidTransactionHistoryList(List<PostPaidTransactionHistory> postPaidTransactionHistoryList) {
        this.postPaidTransactionHistoryList = postPaidTransactionHistoryList;
    }

}
