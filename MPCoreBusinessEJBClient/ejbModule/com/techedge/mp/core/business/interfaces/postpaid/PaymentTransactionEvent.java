package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;

public class PaymentTransactionEvent implements Serializable{
	
	//TODO
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8774964505375506126L;
	private String sequence;
	private String type;  				//eventType  MOV||STO
	private Double amount;
	private String transactionResult;
	private String errorCode;
	private String errorDescription;
	
	
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getTransactionResult() {
		return transactionResult;
	}
	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	

		

}
