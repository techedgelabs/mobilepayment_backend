package com.techedge.mp.core.business.interfaces;

import com.techedge.mp.fidelity.adapter.business.interfaces.CodeEnum;


public class ResponseHelper {

    public final static String SYSTEM_ERROR                                                                      = "SYSTEM_ERROR_500";

    public final static String USER_CREATE_SUCCESS                                                               = "USER_CREATE_200";

    public final static String USER_CREATE_INVALID_TICKET                                                        = "USER_REQU_401";

    public final static String USER_CREATE_USER_EXISTS                                                           = "USER_CREATE_501";

    public final static String USER_CREATE_FISCALCODE_EXISTS                                                     = "USER_CREATE_502";

    public final static String USER_CREATE_MOBILE_PHONE_NOT_FOUND                                                = "USER_CREATE_503";

    public final static String USER_CREATE_MOBILE_PHONE_INVALID                                                  = "USER_CREATE_504";

    public final static String USER_CREATE_MOBILE_PHONE_EXISTS                                                   = "USER_CREATE_505";

    public final static String USER_CREATE_MOBILE_PHONE_INVALID_LIST_SIZE                                        = "USER_CREATE_506";

    public final static String USER_CREATE_NOT_ENABLE                                                            = "USER_CREATE_507";

    public final static String USER_CREATE_INVALID_MAIL                                                          = "USER_CREATE_508";

    public final static String USER_CREATE_FISCALCODE_MISMATCH                                                   = "USER_CREATE_509";

    public final static String USER_CREATE_MAX_USERDEVICE                                                        = "USER_CREATE_510";

    public final static String USER_CREATE_VIRTUALIZATION_ATTEMPTS_NULL                                          = "USER_CREATE_511";

    public final static String USER_CREATE_INVALID_REQUEST                                                       = "USER_REQU_400";

    public final static String USER_CREATE_FAILURE                                                               = "USER_CREATE_300";

    public final static String USER_CREATE_USER_SOCIAL_EXISTS                                                    = "USER_CREATE_512";

    public final static String USER_CREATE_INVALID_TOKEN                                                         = "USER_CREATE_513";
    
    public final static String USER_CREATE_PLATE_NUMBER_INVALID                                                  = "USER_CREATE_514";
    
    public final static String USER_CREATE_SOCIAL_EMAIL_NOT_MATCH                                                = "USER_CREATE_515";

    public final static String MAIL_CREATE_SUCCESS                                                               = "MAIL_CREATE_200";

    public final static String MAIL_CREATE_FAILURE                                                               = "MAIL_CREATE_300";

    public final static String MAIL_CREATE_INVALID_TICKET                                                        = "MAIL_REQU_401";

    public final static String MAIL_CREATE_MAIL_EXISTS                                                           = "MAIL_CREATE_501";

    public final static String MAIL_EDIT_SUCCESS                                                                 = "MAIL_EDIT_200";

    public final static String MAIL_EDIT_FAILURE                                                                 = "MAIL_EDIT_300";

    public final static String MAIL_EDIT_INVALID_TICKET                                                          = "MAIL_REQU_401";

    public final static String MAIL_REMOVE_SUCCESS                                                               = "MAIL_REMOVE_200";

    public final static String MAIL_REMOVE_INVALID_TICKET                                                        = "MAIL_REQU_401";

    public final static String MAIL_REMOVE_MAIL_NOT_EXISTS                                                       = "MAIL_REMOVE_501";

    public final static String MAIL_GET_SUCCESS                                                                  = "MAIL_GET_200";

    public final static String MAIL_GET_FAILURE                                                                  = "MAIL_GET_300";

    public final static String MAIL_GET_INVALID_TICKET                                                           = "MAIL_REQU_401";

    public final static String MAIL_GET_MAIL_NOT_EXISTS                                                          = "MAIL_GET_501";

    public final static String USER_RESCUE_PASSWORD_SUCCESS                                                      = "USER_RESCUE_PASSWORD_200";

    public final static String USER_RESCUE_PASSWORD_INVALID_TICKET                                               = "USER_REQU_401";

    public final static String USER_RESCUE_PASSWORD_INVALID_USER_TYPE                                            = "USER_RESCUE_PASSWORD_400";

    public final static String USER_RESCUE_PASSWORD_INVALID_GOOGLE_USER                                          = "USER_RESCUE_PASSWORD_401";

    public final static String USER_RESCUE_PASSWORD_INVALID_FACEBOOK_USER                                        = "USER_RESCUE_PASSWORD_402";

    public final static String USER_RESCUE_PASSWORD_NOT_EXISTS                                                   = "USER_RESCUE_PASSWORD_501";

    public final static String USER_UPDATE_SUCCESS                                                               = "USER_UPD_200";

    public final static String USER_UPDATE_INVALID_TICKET                                                        = "USER_REQU_401";

    public final static String USER_UPDATE_UNAUTHORIZED                                                          = "USER_REQU_403";

    public final static String USER_AUTH_SUCCESS                                                                 = "USER_AUTH_200";

    public final static String USER_AUTH_LOGIN_ERROR                                                             = "USER_AUTH_301";

    public final static String USER_AUTH_UNAUTHORIZED                                                            = "USER_REQU_403";

    public final static String USER_AUTH_LOGIN_LOCKED                                                            = "USER_AUTH_302";

    public final static String USER_AUTH_NOT_VERIFIED                                                            = "USER_AUTH_303";

    public final static String USER_AUTH_FAILED                                                                  = "USER_AUTH_500";

    public final static String USER_AUTH_DWH_UNAVAILABLE                                                         = "USER_AUTH_304";

    public final static String USER_AUTH_DWH_REG_LITE                                                            = "USER_AUTH_305";

    public final static String USER_AUTH_INVALID_USER                                                            = "USER_AUTH_306";

    public final static String USER_AUTH_DWH_BLACKLIST                                                           = "USER_AUTH_307";

    public final static String USER_AUTH_INVALID_TICKET                                                          = "USER_REQU_401";

    public final static String USER_LOGOUT_SUCCESS                                                               = "USER_LOGOUT_200";

    public final static String USER_LOGOUT_INVALID_TICKET                                                        = "USER_REQU_401";

    public final static String USER_CHECK_SUCCESS                                                                = "USER_CHECK_200";

    public final static String USER_CHECK_FAILURE                                                                = "USER_CHECK_300";

    public final static String USER_CHECK_WRONG_CODE                                                             = "USER_CHECK_400";

    public final static String USER_CHECK_INVALID_REQUEST                                                        = "USER_REQU_400";

    public final static String USER_CHECK_INVALID_TICKET                                                         = "USER_REQU_401";

    public final static String USER_CHECK_MOBILE_PHONE_FAILURE                                                   = "USER_CHECK_301";

    public final static String USER_CHECK_DEVICEID_FAILURE                                                       = "USER_CHECK_302";

    public final static String USER_VALIDATE_PAYMENT_METHOD_SUCCESS                                              = "USER_VALPAY_200";

    public final static String USER_VALIDATE_PAYMENT_METHOD_FAILED                                               = "USER_VALPAY_300";

    public final static String USER_VALIDATE_PAYMENT_METHOD_NOT_FOUND                                            = "USER_VALPAY_403";

    public final static String USER_VALIDATE_PAYMENT_METHOD_INVALID_TICKET                                       = "USER_REQU_401";

    public final static String USER_VALIDATE_PAYMENT_METHOD_UNAUTHORIZED                                         = "USER_REQU_403";

    public final static String USER_VALIDATE_PAYMENT_METHOD_STATUS_NOT_VALID                                     = "USER_VALPAY_404";

    public final static String USER_PWD_SUCCESS                                                                  = "USER_PWD_200";

    public final static String USER_PWD_FAILURE                                                                  = "USER_PWD_300";

    public final static String USER_PWD_NEW_PASSWORD_WRONG                                                       = "USER_PWD_401";

    public final static String USER_PWD_OLD_PASSWORD_WRONG                                                       = "USER_PWD_402";

    public final static String USER_PWD_USERNAME_PASSWORD_EQUALS                                                 = "USER_PWD_403";

    public final static String USER_PWD_PASSWORD_SHORT                                                           = "USER_PWD_404";

    public final static String USER_PWD_NUMBER_LESS                                                              = "USER_PWD_405";

    public final static String USER_PWD_LOWER_LESS                                                               = "USER_PWD_406";

    public final static String USER_PWD_UPPER_LESS                                                               = "USER_PWD_407";

    public final static String USER_PWD_OLD_NEW_EQUALS                                                           = "USER_PWD_408";

    public final static String USER_PWD_OLD_ERROR                                                                = "USER_PWD_409";

    public final static String USER_PWD_HISTORY_NEW_EQUALS                                                       = "USER_PWD_415";

    public final static String USER_PWD_INVALID_TICKET                                                           = "USER_REQU_401";

    public final static String USER_PWD_UNAUTHORIZED                                                             = "USER_REQU_403";

    public final static String USER_PWD_INVALID_USER_TYPE_SOCIAL                                                 = "USER_PWD_416";

    public final static String USER_PWD_INVALID_USER_SOURCE                                                      = "USER_PWD_418";

    public final static String USER_PWD_INVALID_USER_TYPE_GUEST                                                  = "USER_PWD_417";

    public final static String USER_PWD_INVALID_USER_TYPE_MULTICARD                                              = "USER_PWD_419";

    public final static String USER_PIN_SUCCESS                                                                  = "USER_PIN_200";

    public final static String USER_PIN_FAILURE                                                                  = "USER_PIN_300";

    public final static String USER_PIN_OLD_LESS                                                                 = "USER_PIN_400";

    public final static String USER_PIN_NEW_WRONG                                                                = "USER_PIN_401";

    public final static String USER_PIN_OLD_WRONG                                                                = "USER_PIN_402";

    public final static String USER_PIN_OLD_DB_LESS                                                              = "USER_PIN_403";

    public final static String USER_PIN_OLD_NEW_EQUALS                                                           = "USER_PIN_404";

    public final static String USER_PIN_OLD_ERROR                                                                = "USER_PIN_405";

    public final static String USER_PIN_INVALID_TICKET                                                           = "USER_REQU_401";

    public final static String USER_PIN_UNAUTHORIZED                                                             = "USER_REQU_403";

    public final static String USER_PIN_METHOD_NOT_FOUND                                                         = "USER_PIN_406";

    public final static String USER_PIN_STATUS_ERROR                                                             = "USER_PIN_407";

    public final static String USER_PIN_NOT_STRONG                                                               = "USER_PIN_410";

    public final static String USER_PIN_ERROR_PIN_LAST_ATTEMPT                                                   = "USER_PIN_411";

    public final static String USER_PIN_ERROR_PIN_ATTEMPTS_LEFT                                                  = "USER_PIN_412";

    public final static String USER_RESET_PIN_SUCCESS                                                            = "USER_RESET_PIN_200";

    public final static String USER_RESET_PIN_FAILURE                                                            = "USER_RESET_PIN_300";

    public final static String USER_RESET_PIN_WRONG_PASSWORD                                                     = "USER_RESET_PIN_400";

    public final static String USER_RESET_PIN_WRONG_CODE                                                         = "USER_RESET_PIN_402";

    public final static String USER_RESET_PIN_INVALID_TICKET                                                     = "USER_REQU_401";

    public final static String USER_RESET_PIN_UNAUTHORIZED                                                       = "USER_REQU_403";

    public final static String USER_SKIP_PAYMENT_METHOD_CONFIGURATION_SUCCESS                                    = "USER_SKIP_PAYMENT_CONF_200";

    public final static String USER_SKIP_PAYMENT_METHOD_CONFIGURATION_FAILURE                                    = "USER_SKIP_PAYMENT_CONF_300";

    public final static String USER_SKIP_PAYMENT_METHOD_CONFIGURATION_INVALID_TICKET                             = "USER_REQU_401";

    public final static String USER_SKIP_PAYMENT_METHOD_CONFIGURATION_UNAUTHORIZED                               = "USER_REQU_403";

    public final static String USER_SKIP_VIRTUALIZATION_SUCCESS                                                  = "USER_SKIP_VIRTUALIZATION_200";

    public final static String USER_SKIP_VIRTUALIZATION_FAILURE                                                  = "USER_SKIP_VIRTUALIZATION_300";

    public final static String USER_SKIP_VIRTUALIZATION_INVALID_TICKET                                           = "USER_REQU_401";

    public final static String USER_SKIP_VIRTUALIZATION_UNAUTHORIZED                                             = "USER_REQU_403";

    public final static String USER_INSERT_PAYMENT_METHOD_SUCCESS                                                = "USER_INSPAY_200";

    public final static String USER_INSERT_PAYMENT_METHOD_FAILED                                                 = "USER_INSPAY_300";

    public final static String USER_INSERT_PAYMENT_METHOD_NOT_FOUND                                              = "USER_INSPAY_404";

    public final static String USER_INSERT_PAYMENT_METHOD_CONNECTION_ERROR                                       = "USER_INSPAY_405";

    public final static String USER_INSERT_PAYMENT_METHOD_INVALID_PIN                                            = "USER_INSPAY_406";

    public final static String USER_INSERT_PAYMENT_METHOD_WRONG_PIN                                              = "USER_INSPAY_407";

    public final static String USER_INSERT_PAYMENT_METHOD_UNAUTHORIZED                                           = "USER_REQU_403";

    public final static String USER_INSERT_PAYMENT_METHOD_INVALID_TICKET                                         = "USER_REQU_401";

    public final static String USER_INSERT_PAYMENT_METHOD_PIN_EXISTS                                             = "USER_INSPAY_408";

    public final static String USER_INSERT_PAYMENT_METHOD_ERROR_PIN_ATTEMPTS_LEFT                                = "USER_INSPAY_409";

    public final static String USER_INSERT_PAYMENT_METHOD_ERROR_PIN_LAST_ATTEMPT                                 = "USER_INSPAY_410";

    public final static String USER_SET_DEFAULT_PAYMENT_METHOD_SUCCESS                                           = "USER_DEFPAY_200";

    public final static String USER_SET_DEFAULT_PAYMENT_METHOD_NOT_FOUND                                         = "USER_DEFPAY_404";

    public final static String USER_SET_DEFAULT_PAYMENT_METHOD_INVALID_TICKET                                    = "USER_REQU_401";

    public final static String USER_REMOVE_PAYMENT_METHOD_SUCCESS                                                = "USER_REMPAY_200";

    public final static String USER_REMOVE_PAYMENT_METHOD_FAILED                                                 = "USER_REMPAY_300";

    public final static String USER_REMOVE_PAYMENT_METHOD_NOT_FOUND                                              = "USER_REMPAY_404";

    public final static String USER_REMOVE_PAYMENT_METHOD_INVALID_TICKET                                         = "USER_REQU_401";

    public final static String USER_RETRIEVE_PAYMENT_SUCCESS                                                     = "PAYMENT_RETRIEVEDATA_200";

    public final static String USER_RETRIEVE_PAYMENT_FAILED                                                      = "PAYMENT_RETRIEVEDATA_300";

    public final static String USER_RETRIEVE_PAYMENT_INVALID_TICKET                                              = "USER_REQU_401";

    public final static String USER_RETRIEVE_PAYMENT_ALREADY_USED                                                = "PAYMENT_RETRIEVEDATA_400";

    public final static String USER_PAYMENT_CONFIRM_SUCCESS                                                      = "PAYMENT_CONFIRM_200";

    public final static String USER_PAYMENT_CONFIRM_FAILURE                                                      = "PAYMENT_CONFIRM_300";

    public final static String USER_LOAD_VOUCHER_SUCCESS                                                         = "USER_LOAD_VOUCHER_200";

    public final static String USER_LOAD_VOUCHER_ERROR                                                           = "USER_LOAD_VOUCHER_300";

    public final static String USER_LOAD_VOUCHER_GENERIC_ERROR                                                   = "USER_LOAD_VOUCHER_301";

    public final static String USER_LOAD_VOUCHER_EMPTY_LIST                                                      = "USER_LOAD_VOUCHER_302";

    public final static String USER_LOAD_VOUCHER_MAX_LIMIT_REACHED                                               = "USER_LOAD_VOUCHER_303";

    public final static String USER_LOAD_VOUCHER_INVALID_TICKET                                                  = "USER_REQU_401";

    public final static String USER_LOAD_VOUCHER_UNAUTHORIZED                                                    = "USER_REQU_403";

    public final static String USER_LOAD_VOUCHER_MAX_REACHED                                                     = "USER_LOAD_VOUCHER_401";
    
    public final static String USER_LOAD_VOUCHER_ALREADY_USED                                                    = "USER_LOAD_VOUCHER_402";

    public final static String USER_LOAD_VOUCHER_USER_NOT_ENABLED                                                = "USER_LOAD_VOUCHER_403";
    
    public final static String USER_LOAD_VOUCHER_USER_GUEST_NOT_ENABLED                                          = "USER_LOAD_VOUCHER_404";
    
    public static final String USER_LOAD_VOUCHER_VALID                                                           = "USER_LOAD_VOUCHER_500";

    public final static String USER_LOAD_VOUCHER_NOT_VALID                                                       = "USER_LOAD_VOUCHER_501";

    public static final String USER_LOAD_VOUCHER_EXPIRED                                                         = "USER_LOAD_VOUCHER_502";

    public static final String USER_LOAD_VOUCHER_SPENT                                                           = "USER_LOAD_VOUCHER_503";

    public static final String USER_LOAD_VOUCHER_CANCELED                                                        = "USER_LOAD_VOUCHER_504";

    public static final String USER_LOAD_VOUCHER_TO_CONFIRM                                                      = "USER_LOAD_VOUCHER_505";

    public static final String USER_LOAD_VOUCHER_INEXISTENT                                                      = "USER_LOAD_VOUCHER_506";

    public static final String USER_LOAD_VOUCHER_REMOVED                                                         = "USER_LOAD_VOUCHER_507";

    public static final String USER_LOAD_VOUCHER_UNKNOW                                                          = "USER_LOAD_VOUCHER_508";

    public static final String USER_LOAD_VOUCHER_NOT_ASSIGNED                                                    = "USER_LOAD_VOUCHER_509";

    public static final String USER_LOAD_VOUCHER_PROMOTION_NOT_ACTIVE                                            = "USER_LOAD_VOUCHER_510";

    public static final String USER_LOAD_VOUCHER_PROMOTION_EXPIRED                                               = "USER_LOAD_VOUCHER_511";

    public static final String USER_LOAD_VOUCHER_USER_NOT_ASSIGNED                                               = "USER_LOAD_VOUCHER_512";

    public static final String USER_LOAD_VOUCHER_NOT_PROMO_FOR_CODE                                              = "USER_LOAD_VOUCHER_513";

    public static final String USER_LOAD_VOUCHER_ERROR_USER_CODE_NOT_VALID                                       = "USER_LOAD_VOUCHER_514";

    public static final String USER_LOAD_VOUCHER_PROMOTION_VOUCHER_CODE_NOT_VALID                                = "USER_LOAD_VOUCHER_515";

    public static final String USER_LOAD_VOUCHER_CAN_NOT_BE_USED_ON_THE_CHANNEL                                  = "USER_LOAD_VOUCHER_516";

    public static final String USER_LOAD_VOUCHER_TO_RETRY                                                        = "USER_LOAD_VOUCHER_517";

    public static final String USER_LOAD_VOUCHER_VOUCHER_LIST_EMPTY                                              = "USER_LOAD_VOUCHER_518";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_SUCCESS                                                = "USER_LOAD_VOUCHER_CAMPAIGN_200";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_ERROR                                                  = "USER_LOAD_VOUCHER_CAMPAIGN_300";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_GENERIC_ERROR                                          = "USER_LOAD_VOUCHER_CAMPAIGN_301";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_FISCAL_CODE_NOT_FOUND                                  = "USER_LOAD_VOUCHER_CAMPAIGN_302";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_INVALID_FISCAL_CODE                                    = "USER_LOAD_VOUCHER_CAMPAIGN_303";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_INVALID_PROMO_CODE                                     = "USER_LOAD_VOUCHER_CAMPAIGN_304";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_VOUCHER_CODE_INVALID_FORMAT                            = "USER_LOAD_VOUCHER_CAMPAIGN_501";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_NO_VALID_CAMPAIGN                                      = "USER_LOAD_VOUCHER_CAMPAIGN_502";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_CAMPAIGN_NOT_ACTIVE                                    = "USER_LOAD_VOUCHER_CAMPAIGN_503";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_USER_REGISTRATION_NOT_IN_PERIOD                        = "USER_LOAD_VOUCHER_CAMPAIGN_504";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_USED                                                   = "USER_LOAD_VOUCHER_CAMPAIGN_505";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_DECRYPT_ERROR                                          = "USER_LOAD_VOUCHER_CAMPAIGN_506";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_INVALID_STRING                                         = "USER_LOAD_VOUCHER_CAMPAIGN_507";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_CODE_NOT_MATCH_WITH_PROMO_CODE                         = "USER_LOAD_VOUCHER_CAMPAIGN_508";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_EXPIRED_CODE                                           = "USER_LOAD_VOUCHER_CAMPAIGN_509";

    public final static String USER_LOAD_VOUCHER_CAMPAIGN_ASSOCIATION_ERROR                                      = "USER_LOAD_VOUCHER_CAMPAIGN_510";

    public final static String USER_SET_USE_VOUCHER_SUCCESS                                                      = "USER_SET_USE_VOUCHER_200";

    public final static String USER_SET_USE_VOUCHER_FAILURE                                                      = "USER_SET_USE_VOUCHER_300";

    public final static String USER_SET_USE_VOUCHER_INVALID_TICKET                                               = "USER_REQU_401";

    public final static String USER_SET_USE_VOUCHER_UNAUTHORIZED                                                 = "USER_REQU_403";

    public final static String USER_GET_ACTIVE_VOUCHERS_SUCCESS                                                  = "USER_GET_ACTIVE_VOUCHERS_200";

    public final static String USER_GET_ACTIVE_VOUCHERS_ERROR                                                    = "USER_GET_ACTIVE_VOUCHERS_300";

    public final static String USER_GET_ACTIVE_VOUCHERS_GENERIC_ERROR                                            = "USER_GET_ACTIVE_VOUCHERS_301";

    public final static String USER_GET_ACTIVE_VOUCHERS_EMPTY_LIST                                               = "USER_GET_ACTIVE_VOUCHERS_302";

    public final static String USER_GET_ACTIVE_VOUCHERS_MAX_LIMIT_REACHED                                        = "USER_GET_ACTIVE_VOUCHERS_303";

    public final static String USER_GET_ACTIVE_VOUCHERS_INVALID_TICKET                                           = "USER_REQU_401";

    public final static String USER_GET_ACTIVE_VOUCHERS_UNAUTHORIZED                                             = "USER_REQU_403";

    public final static String USER_REMOVE_VOUCHER_SUCCESS                                                       = "USER_REMOVE_VOUCHER_200";

    public final static String USER_REMOVE_VOUCHER_FAILURE                                                       = "USER_REMOVE_VOUCHER_300";

    public final static String USER_REMOVE_VOUCHER_INVALID_TICKET                                                = "USER_REQU_401";

    public final static String USER_REMOVE_VOUCHER_UNAUTHORIZED                                                  = "USER_REQU_403";

    public final static String USER_REMOVE_VOUCHER_NOT_FOUND                                                     = "USER_REMOVE_VOUCHER_404";

    public final static String USER_REMOVE_DEVICE_SUCCESS                                                        = "USER_REMOVE_DEVICE_200";

    public final static String USER_REMOVE_DEVICE_FAILURE                                                        = "USER_REMOVE_DEVICE_300";

    public final static String USER_REMOVE_DEVICE_INVALID_TICKET                                                 = "USER_REQU_401";

    public final static String USER_REMOVE_DEVICE_UNAUTHORIZED                                                   = "USER_REQU_403";

    public final static String USER_REMOVE_DEVICE_NOT_FOUND                                                      = "USER_REMOVE_DEVICE_404";

    public final static String USER_SET_DEFAULT_LOYALTY_CARD_SUCCESS                                             = "USER_SET_DEFAULT_LOYALTY_CARD_200";

    public final static String USER_SET_DEFAULT_LOYALTY_CARD_FAILURE                                             = "USER_SET_DEFAULT_LOYALTY_CARD_300";

    public final static String USER_SET_DEFAULT_LOYALTY_CARD_INVALID_TICKET                                      = "USER_REQU_401";

    public final static String USER_SET_DEFAULT_LOYALTY_CARD_UNAUTHORIZED                                        = "USER_REQU_403";

    public final static String USER_SET_DEFAULT_LOYALTY_CARD_MIGRATION_ERROR                                     = "USER_SET_DEFAULT_LOYALTY_CARD_400";

    public final static String USER_SET_DEFAULT_LOYALTY_CARD_MIGRATION_ONLINE_ERROR                              = "USER_SET_DEFAULT_LOYALTY_CARD_401";

    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_SUCCESS                                          = "USER_GET_AVAILABLE_LOYALTY_CARDS_200";

    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_REQUEST                                  = "USER_REQU_400";

    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_TICKET                                   = "USER_REQU_401";

    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_UNAUTHORIZED                                     = "USER_REQU_403";

    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_CF_NOT_FOUND                                     = "USER_GET_AVAILABLE_LOYALTY_CARDS_300";

    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_CF                                       = "USER_GET_AVAILABLE_LOYALTY_CARDS_302";

    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_GENERIC_ERROR                                    = "USER_GET_AVAILABLE_LOYALTY_CARDS_400";

    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_MIGRATION_ERROR                                  = "USER_GET_AVAILABLE_LOYALTY_CARDS_401";

    public final static String USER_RECOVER_USERNAME_SUCCESS                                                     = "USER_RECOVER_USERNAME_200";

    public final static String USER_RECOVER_USERNAME_FAILURE                                                     = "USER_RECOVER_USERNAME_300";

    public final static String USER_RECOVER_USERNAME_INVALID_TAX_CODE                                            = "USER_RECOVER_USERNAME_301";

    public final static String USER_RECOVER_USERNAME_INVALID_REQUEST                                             = "USER_REQU_400";

    public final static String USER_RECOVER_USERNAME_UNAUTHORIZED                                                = "USER_REQU_403";

    public final static String USER_UPDATE_SMS_LOG_SUCCESS                                                       = "USER_UPDATE_SMS_LOG_200";

    public final static String USER_UPDATE_SMS_LOG_INVALID_CORRELATIONID                                         = "USER_UPDATE_SMS_LOG_300";

    public final static String USER_UPDATE_SMS_LOG_INVALID_USER                                                  = "USER_UPDATE_SMS_LOG_301";

    public final static String USER_UPDATE_SMS_LOG_INVALID_TICKET                                                = "USER_REQU_401";

    public final static String USER_UPDATE_SMS_LOG_UNAUTHORIZED                                                  = "USER_REQU_403";

    public final static String USER_REQU_INVALID_REQUEST                                                         = "USER_REQU_404";

    public final static String USER_REQU_INVALID_TICKET                                                          = "USER_REQU_401";

    public final static String USER_REQU_JSON_SYNTAX_ERORR                                                       = "USER_REQU_402";

    public final static String USER_REQU_UNAUTHORIZED                                                            = "USER_REQU_403";

    public final static String USER_MIGRATION_SUCCESS                                                            = "USER_MIGRATION_200";

    public final static String USER_MIGRATION_FAILURE                                                            = "USER_MIGRATION_401";

    public final static String USER_MIGRATION_USER_NOT_FOUND                                                     = "USER_MIGRATION_301";

    public final static String USER_MIGRATION_USER_TYPE_NOT_CUSTOMER                                             = "USER_MIGRATION_302";

    public final static String USER_CHECK_LOYALTY_SESSION_SUCCESS                                                = "USER_CHECK_LOYALTY_SESSION_200";

    public final static String USER_CHECK_LOYALTY_SESSION_NOT_FOUND                                              = "USER_CHECK_LOYALTY_SESSION_400";

    public final static String USER_CHECK_LOYALTY_SESSION_INVALID_USER_STATUS                                    = "USER_CHECK_LOYALTY_SESSION_301";

    public final static String USER_CHECK_LOYALTY_SESSION_EXPIRED                                                = "USER_CHECK_LOYALTY_SESSION_301";

    public final static String USER_SET_EVENT_NOTIFICATION_SUCCESS                                               = "USER_SET_EVENT_NOTIFICATION_200";

    public final static String USER_SET_EVENT_NOTIFICATION_FAILURE                                               = "USER_SET_EVENT_NOTIFICATION_300";

    public final static String USER_SET_EVENT_NOTIFICATION_USER_NOT_FOUND                                        = "USER_SET_EVENT_NOTIFICATION_400";

    public final static String USER_SET_EVENT_NOTIFICATION_STATION_NOT_FOUND                                     = "USER_SET_EVENT_NOTIFICATION_401";

    public final static String USER_SET_EVENT_NOTIFICATION_ENDPOINT_NOT_FOUND                                    = "USER_SET_EVENT_NOTIFICATION_402";

    public final static String USER_SET_EVENT_NOTIFICATION_SESSIONID_NOT_FOUND                                   = "USER_SET_EVENT_NOTIFICATION_403";

    public final static String USER_SET_EVENT_NOTIFICATION_CRM_NOTIFIY_NOT_SEND                                  = "USER_SET_EVENT_NOTIFICATION_404";

    public final static String USER_INFO_REDEMPTION_SUCCESS                                                      = "USER_INFO_REDEMPTION_200";

    public final static String USER_INFO_REDEMPTION_ERROR                                                        = "USER_INFO_REDEMPTION_300";

    public final static String USER_INFO_REDEMPTION_GENERIC_ERROR                                                = "USER_INFO_REDEMPTION_301";

    public final static String USER_INFO_REDEMPTION_INVALID_FISCAL_CODE                                          = "USER_INFO_REDEMPTION_302";

    public final static String USER_INFO_REDEMPTION_INVALID_BALANCE                                              = "USER_INFO_REDEMPTION_303";

    public final static String USER_INFO_REDEMPTION_CUSTOMER_BLOCKED                                             = "USER_INFO_REDEMPTION_304";

    public final static String USER_INFO_REDEMPTION_FISCAL_CODE_NULL                                             = "USER_INFO_REDEMPTION_305";

    public final static String USER_INFO_REDEMPTION_INVALID_TICKET                                               = "USER_REQU_401";

    public final static String USER_INFO_REDEMPTION_UNAUTHORIZED                                                 = "USER_REQU_403";

    public final static String USER_REDEMPTION_SUCCESS                                                           = "USER_REDEMPTION_200";

    public final static String USER_REDEMPTION_ERROR                                                             = "USER_REDEMPTION_300";

    public final static String USER_REDEMPTION_GENERIC_ERROR                                                     = "USER_REDEMPTION_301";

    public final static String USER_REDEMPTION_INVALID_BALANCE                                                   = "USER_REDEMPTION_302";

    public final static String USER_REDEMPTION_INVALID_FISCAL_CODE                                               = "USER_REDEMPTION_303";

    public final static String USER_REDEMPTION_CUSTOMER_BLOCKED                                                  = "USER_REDEMPTION_304";

    public final static String USER_REDEMPTION_INVALID_REDEMPTION_CODE                                           = "USER_REDEMPTION_305";

    public final static String USER_REDEMPTION_ERROR_PIN_ATTEMPTS_LEFT                                           = "USER_REDEMPTION_306";

    public final static String USER_REDEMPTION_ERROR_PIN                                                         = "USER_REDEMPTION_307";

    public final static String USER_REDEMPTION_ERROR_PIN_LAST_ATTEMPT                                            = "USER_REDEMPTION_308";

    public final static String USER_REDEMPTION_ERROR_PIN_NOT_FOUND                                               = "USER_REDEMPTION_309";

    public final static String USER_REDEMPTION_FAILURE                                                           = "USER_REDEMPTION_400";

    public final static String USER_REDEMPTION_INVALID_TICKET                                                    = "USER_REQU_401";

    public final static String USER_REDEMPTION_UNAUTHORIZED                                                      = "USER_REQU_403";

    public final static String USER_UPDATE_PUSH_NOTIFICATION_SUCCESS                                             = "USER_UPDATE_PUSH_NOTIFICATION_200";

    public final static String USER_UPDATE_PUSH_NOTIFICATION_FAILURE                                             = "USER_UPDATE_PUSH_NOTIFICATION_300";

    public final static String USER_UPDATE_PUSH_NOTIFICATION_INVALID_USER                                        = "USER_UPDATE_PUSH_NOTIFICATION_301";

    public final static String CHECK_AUTHORIZATION_SUCCESS                                                       = "USER_REQU_200";

    public final static String CHECK_AUTHORIZATION_FAILED                                                        = "USER_REQU_403";

    public final static String CHECK_AUTHORIZATION_INVALID_TICKET                                                = "USER_REQU_401";

    public final static String REFUEL_TRANSACTION_CREATE_SUCCESS                                                 = "REFUEL_TRANSACTION_CREATE_200";

    public final static String REFUEL_TRANSACTION_CREATE_FAILURE                                                 = "REFUEL_TRANSACTION_CREATE_300";

    public final static String REFUEL_TRANSACTION_CREATE_INSUFFICIENT_VOUCHER_AMOUNT                             = "REFUEL_TRANSACTION_CREATE_301";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR                                           = "REFUEL_TRANSACTION_CREATE_302";
    
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_PARAMETERS                                      = "REFUEL_TRANSACTION_CREATE_303";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_OPERATION                                       = "REFUEL_TRANSACTION_CREATE_304";
    public final static String REFUEL_TRANSACTION_CREATE_DPAN_NOT_FOUND                                          = "REFUEL_TRANSACTION_CREATE_305";
    public final static String REFUEL_TRANSACTION_CREATE_OTP_NOT_FOUND                                           = "REFUEL_TRANSACTION_CREATE_306";
    public final static String REFUEL_TRANSACTION_CREATE_TOTP_NOT_FOUND                                          = "REFUEL_TRANSACTION_CREATE_307";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_TOTP_OR_DPAN                                    = "REFUEL_TRANSACTION_CREATE_308";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_LOYALTY_CARD                                    = "REFUEL_TRANSACTION_CREATE_309";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_PARTNER_TYPE                                    = "REFUEL_TRANSACTION_CREATE_310";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_ALREADY_ENABLED                                    = "REFUEL_TRANSACTION_CREATE_311";
    public final static String REFUEL_TRANSACTION_CREATE_MSGREASONCODE_MANDATORY                                 = "REFUEL_TRANSACTION_CREATE_312";
    public final static String REFUEL_TRANSACTION_CREATE_PAN_NOT_FOUND                                           = "REFUEL_TRANSACTION_CREATE_313";
    public final static String REFUEL_TRANSACTION_CREATE_PAN_ALREADY_USED                                        = "REFUEL_TRANSACTION_CREATE_314";
    public final static String REFUEL_TRANSACTION_CREATE_EMAIL_ALREADY_USED                                      = "REFUEL_TRANSACTION_CREATE_315";
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_NOT_FOUND_FOR_OPERATION                         = "REFUEL_TRANSACTION_CREATE_316";
    public final static String REFUEL_TRANSACTION_CREATE_USER_NOT_FOUND                                          = "REFUEL_TRANSACTION_CREATE_317";
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_WITH_ANOTHER_DEVICE                             = "REFUEL_TRANSACTION_CREATE_318";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_CRIPTOGRAM                                      = "REFUEL_TRANSACTION_CREATE_319";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZATION_NOT_FOUND                                 = "REFUEL_TRANSACTION_CREATE_320";
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_NOT_FOUND_FOR_REVERSAL                          = "REFUEL_TRANSACTION_CREATE_321";
    public final static String REFUEL_TRANSACTION_CREATE_TIMEOUT_OPERATION                                       = "REFUEL_TRANSACTION_CREATE_322";
    public final static String REFUEL_TRANSACTION_CREATE_OPERATION_NOT_COMPLETED                                 = "REFUEL_TRANSACTION_CREATE_323";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZED_0                                            = "REFUEL_TRANSACTION_CREATE_324";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZED_1                                            = "REFUEL_TRANSACTION_CREATE_325";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZED_PARTIAL_AMOUNT_2                             = "REFUEL_TRANSACTION_CREATE_326";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZED_3                                            = "REFUEL_TRANSACTION_CREATE_327";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZED_5                                            = "REFUEL_TRANSACTION_CREATE_328";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZED_PARTIAL_AMOUNT_6                             = "REFUEL_TRANSACTION_CREATE_329";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZED_7                                            = "REFUEL_TRANSACTION_CREATE_330";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZED_10                                           = "REFUEL_TRANSACTION_CREATE_331";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZED_80                                           = "REFUEL_TRANSACTION_CREATE_332";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZED_81                                           = "REFUEL_TRANSACTION_CREATE_333";
    public final static String REFUEL_TRANSACTION_CREATE_AUTHORIZATION_DENIED                                    = "REFUEL_TRANSACTION_CREATE_334";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_EXPIRED                                            = "REFUEL_TRANSACTION_CREATE_335";
    public final static String REFUEL_TRANSACTION_CREATE_CONTACT_CALL_CENTER                                     = "REFUEL_TRANSACTION_CREATE_336";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_WITH_RESTRICTIONS                                  = "REFUEL_TRANSACTION_CREATE_337";
    public final static String REFUEL_TRANSACTION_CREATE_PIN_ATTEMPTS_EXPIRED                                    = "REFUEL_TRANSACTION_CREATE_338";
    public final static String REFUEL_TRANSACTION_CREATE_CONTACT_ISSUER                                          = "REFUEL_TRANSACTION_CREATE_339";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_OPERATOR                                        = "REFUEL_TRANSACTION_CREATE_340";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_AMOUNT                                          = "REFUEL_TRANSACTION_CREATE_341";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_CARD_NUMBER                                     = "REFUEL_TRANSACTION_CREATE_342";
    public final static String REFUEL_TRANSACTION_CREATE_PIN_REQUIRED                                            = "REFUEL_TRANSACTION_CREATE_343";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_ACCOUNT                                         = "REFUEL_TRANSACTION_CREATE_344";
    public final static String REFUEL_TRANSACTION_CREATE_UNSUPPORTED_FUNCTION                                    = "REFUEL_TRANSACTION_CREATE_345";
    public final static String REFUEL_TRANSACTION_CREATE_MAX_LIMIT_REACHED                                       = "REFUEL_TRANSACTION_CREATE_346";
    public final static String REFUEL_TRANSACTION_CREATE_WRONG_PIN                                               = "REFUEL_TRANSACTION_CREATE_347";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_NOT_FOUND                                          = "REFUEL_TRANSACTION_CREATE_348";
    public final static String REFUEL_TRANSACTION_CREATE_TRANSACTION_NOT_ENABLED_TO_HOLDER                       = "REFUEL_TRANSACTION_CREATE_349";
    public final static String REFUEL_TRANSACTION_CREATE_TRANSACTION_NOT_ENABLED_TO_POS                          = "REFUEL_TRANSACTION_CREATE_350";
    public final static String REFUEL_TRANSACTION_CREATE_PLAFOND_EXCEEDED                                        = "REFUEL_TRANSACTION_CREATE_351";
    public final static String REFUEL_TRANSACTION_CREATE_LIMITATION_TIME_EXCEEDED                                = "REFUEL_TRANSACTION_CREATE_352";
    public final static String REFUEL_TRANSACTION_CREATE_INACTIVE_CARD                                           = "REFUEL_TRANSACTION_CREATE_353";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_PIN_BLOCK                                       = "REFUEL_TRANSACTION_CREATE_354";
    public final static String REFUEL_TRANSACTION_CREATE_WRONG_PIN_LENGTH                                        = "REFUEL_TRANSACTION_CREATE_355";
    public final static String REFUEL_TRANSACTION_CREATE_PIN_KEY_ERROR                                           = "REFUEL_TRANSACTION_CREATE_356";
    public final static String REFUEL_TRANSACTION_CREATE_OUTDOOR_TRANSACTION_FORBIDDEN                           = "REFUEL_TRANSACTION_CREATE_357";
    public final static String REFUEL_TRANSACTION_CREATE_PUMP_NOT_CONNECTED_TRANSACTION_FORBIDDEN                = "REFUEL_TRANSACTION_CREATE_358";
    public final static String REFUEL_TRANSACTION_CREATE_WRONG_MILEAGE                                           = "REFUEL_TRANSACTION_CREATE_359";
    public final static String REFUEL_TRANSACTION_CREATE_PARTNER_NOT_ENABLED_IN_PV                               = "REFUEL_TRANSACTION_CREATE_360";
    public final static String REFUEL_TRANSACTION_CREATE_PV_NOT_ENABLED_FOR_CARD                                 = "REFUEL_TRANSACTION_CREATE_361";
    public final static String REFUEL_TRANSACTION_CREATE_DAY_NOT_ENABLED_FOR_CARD                                = "REFUEL_TRANSACTION_CREATE_362";
    public final static String REFUEL_TRANSACTION_CREATE_TIME_NOT_ENABLED_FOR_CARD                               = "REFUEL_TRANSACTION_CREATE_363";
    public final static String REFUEL_TRANSACTION_CREATE_MANUAL_TRANSACTION_FORBIDDEN                            = "REFUEL_TRANSACTION_CREATE_364";
    public final static String REFUEL_TRANSACTION_CREATE_IPERSELF_FORBIDDEN                                      = "REFUEL_TRANSACTION_CREATE_365";
    public final static String REFUEL_TRANSACTION_CREATE_FAIDATE_FORBIDDEN                                       = "REFUEL_TRANSACTION_CREATE_366";
    public final static String REFUEL_TRANSACTION_CREATE_OVERRIDE_FORBIDDEN                                      = "REFUEL_TRANSACTION_CREATE_367";
    public final static String REFUEL_TRANSACTION_CREATE_BOOKING_REQUIRED                                        = "REFUEL_TRANSACTION_CREATE_368";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_BOOKING_FOR_PRODUCT                             = "REFUEL_TRANSACTION_CREATE_369";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_BOOKING_FOR_PV                                  = "REFUEL_TRANSACTION_CREATE_370";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_BOOKING_FOR_DAY                                 = "REFUEL_TRANSACTION_CREATE_371";
    public final static String REFUEL_TRANSACTION_CREATE_DRIVER_BLOCKED                                          = "REFUEL_TRANSACTION_CREATE_372";
    public final static String REFUEL_TRANSACTION_CREATE_PRODUCT_NOT_REGISTERED                                  = "REFUEL_TRANSACTION_CREATE_373";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_PRICE                                           = "REFUEL_TRANSACTION_CREATE_374";
    public final static String REFUEL_TRANSACTION_CREATE_UNKNOWN_POS                                             = "REFUEL_TRANSACTION_CREATE_375";
    public final static String REFUEL_TRANSACTION_CREATE_BLOCKED_POS                                             = "REFUEL_TRANSACTION_CREATE_376";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_NOT_ENABLED                                        = "REFUEL_TRANSACTION_CREATE_377";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_BLOCKED                                            = "REFUEL_TRANSACTION_CREATE_378";
    public final static String REFUEL_TRANSACTION_CREATE_BLOCKED_CLIENT                                          = "REFUEL_TRANSACTION_CREATE_379";
    public final static String REFUEL_TRANSACTION_CREATE_BLOCKED_CARD_ACCOUNT                                    = "REFUEL_TRANSACTION_CREATE_380";
    public final static String REFUEL_TRANSACTION_CREATE_BLOCKED_ISSUER                                          = "REFUEL_TRANSACTION_CREATE_381";
    public final static String REFUEL_TRANSACTION_CREATE_PRODUCT_NOT_ENABLED                                     = "REFUEL_TRANSACTION_CREATE_382";
    public final static String REFUEL_TRANSACTION_CREATE_PIN_ATTEMPTS_EXHAUSTED                                  = "REFUEL_TRANSACTION_CREATE_383";
    public final static String REFUEL_TRANSACTION_CREATE_INVALID_AMOUNT_FOR_COMPANY                              = "REFUEL_TRANSACTION_CREATE_384";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_NOT_ENABLED_0                                      = "REFUEL_TRANSACTION_CREATE_385";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_NOT_ENABLED_1                                      = "REFUEL_TRANSACTION_CREATE_386";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_NOT_ENABLED_2                                      = "REFUEL_TRANSACTION_CREATE_387";
    public final static String REFUEL_TRANSACTION_CREATE_PRODUCT_PLAFOND_EXCEEDED                                = "REFUEL_TRANSACTION_CREATE_388";
    public final static String REFUEL_TRANSACTION_CREATE_INSUFFICIENT_CREDIT                                     = "REFUEL_TRANSACTION_CREATE_389";
    public final static String REFUEL_TRANSACTION_CREATE_PLAFOND_EXCEEDED_1                                      = "REFUEL_TRANSACTION_CREATE_390";
    public final static String REFUEL_TRANSACTION_CREATE_EXPIRED_CARD                                            = "REFUEL_TRANSACTION_CREATE_391";
    public final static String REFUEL_TRANSACTION_CREATE_EXPIRED_CARD_PICKUP                                     = "REFUEL_TRANSACTION_CREATE_392";
    public final static String REFUEL_TRANSACTION_CREATE_SUSPECTED_FRAUD                                         = "REFUEL_TRANSACTION_CREATE_393";
    public final static String REFUEL_TRANSACTION_CREATE_CALL_ACQUIRER                                           = "REFUEL_TRANSACTION_CREATE_394";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_WITH_RESTRICTIONS_0                                = "REFUEL_TRANSACTION_CREATE_395";
    public final static String REFUEL_TRANSACTION_CREATE_PIN_ATTEMPTS_EXHAUSTED_PICKUP                           = "REFUEL_TRANSACTION_CREATE_396";
    public final static String REFUEL_TRANSACTION_CREATE_LOST_CARD                                               = "REFUEL_TRANSACTION_CREATE_397";
    public final static String REFUEL_TRANSACTION_CREATE_STOLEN_CARD                                             = "REFUEL_TRANSACTION_CREATE_398";
    public final static String REFUEL_TRANSACTION_CREATE_WRONG_FORMAT                                            = "REFUEL_TRANSACTION_CREATE_399";
    public final static String REFUEL_TRANSACTION_CREATE_CLOSURE_IN_PROGRESS                                     = "REFUEL_TRANSACTION_CREATE_500";
    public final static String REFUEL_TRANSACTION_CREATE_CENTRAL_SYSTEM_NOT_WORKING                              = "REFUEL_TRANSACTION_CREATE_501";
    public final static String REFUEL_TRANSACTION_CREATE_SYSTEM_MALFUNCTION                                      = "REFUEL_TRANSACTION_CREATE_502";
    public final static String REFUEL_TRANSACTION_CREATE_TIME_OUT_CARD_EMITTER                                   = "REFUEL_TRANSACTION_CREATE_503";
    public final static String REFUEL_TRANSACTION_CREATE_CARD_EMITTER_UNREACHABLE                                = "REFUEL_TRANSACTION_CREATE_504";
    public final static String REFUEL_TRANSACTION_CREATE_WRONG_MAC                                               = "REFUEL_TRANSACTION_CREATE_505";
    public final static String REFUEL_TRANSACTION_CREATE_ERROR_SINC_MAC_KEY                                      = "REFUEL_TRANSACTION_CREATE_506";
    public final static String REFUEL_TRANSACTION_CREATE_DECRYPTION_ERROR                                        = "REFUEL_TRANSACTION_CREATE_507";
    public final static String REFUEL_TRANSACTION_CREATE_SECURITY_ERROR                                          = "REFUEL_TRANSACTION_CREATE_508";
    public final static String REFUEL_TRANSACTION_CREATE_MESSAGE_OUT_OF_SEQUENCE                                 = "REFUEL_TRANSACTION_CREATE_509";
    public final static String REFUEL_TRANSACTION_CREATE_WRONG_OPERATOR                                          = "REFUEL_TRANSACTION_CREATE_510";
    public final static String REFUEL_TRANSACTION_CREATE_WRONG_COMPANY                                           = "REFUEL_TRANSACTION_CREATE_511";
    public final static String REFUEL_TRANSACTION_CREATE_GENERIC_ERROR                                           = "REFUEL_TRANSACTION_CREATE_512";
    /*
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR                                           = "REFUEL_TRANSACTION_CREATE_302";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_WRONG_INPUT_PARAMETERS                    = "REFUEL_TRANSACTION_CREATE_303";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_INVALID_PARTNER_TYPE                      = "REFUEL_TRANSACTION_CREATE_304";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PAN_NOT_FOUND                             = "REFUEL_TRANSACTION_CREATE_305";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_ANOTHER_DEVICE_USED                       = "REFUEL_TRANSACTION_CREATE_306";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_INVALID_CRYPTOGRAM_PARAMETERS             = "REFUEL_TRANSACTION_CREATE_307";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PIN_NOT_VERIFIED                          = "REFUEL_TRANSACTION_CREATE_308";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_CARD_EXPIRED                              = "REFUEL_TRANSACTION_CREATE_309";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PAYMENT_FAILED_MERCHANT                   = "REFUEL_TRANSACTION_CREATE_310";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PAYMENT_FAILED_SECURITY                   = "REFUEL_TRANSACTION_CREATE_311";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PAYMENT_FAILED_TERMINAL                   = "REFUEL_TRANSACTION_CREATE_312";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_PAYMENT_FAILED_CUSTOMER                   = "REFUEL_TRANSACTION_CREATE_313";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_INSUFFICIENT_CREDIT                       = "REFUEL_TRANSACTION_CREATE_314";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_AUTHCODE_NOT_FOUND                        = "REFUEL_TRANSACTION_CREATE_315";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_AMOUNT_GREATER_THAN_AUTHORIZED            = "REFUEL_TRANSACTION_CREATE_316";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_AMOUNT_GREATER_THAN_SETTLED               = "REFUEL_TRANSACTION_CREATE_317";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_MAX_LIMIT_REACHED                         = "REFUEL_TRANSACTION_CREATE_318";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_REVERSAL_FAILED                           = "REFUEL_TRANSACTION_CREATE_319";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_INVALID_AMOUNT                            = "REFUEL_TRANSACTION_CREATE_320";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_TIMEOUT_OPERATION                         = "REFUEL_TRANSACTION_CREATE_321";

    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_OPERATION_NOT_COMPLETED                   = "REFUEL_TRANSACTION_CREATE_322";
    
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_ERROR_GENERIC_ERROR                             = "REFUEL_TRANSACTION_CREATE_323";
    */
    public final static String REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG                                      = "REFUEL_TRANSACTION_CREATE_400";

    public final static String REFUEL_TRANSACTION_CREATE_PIN_WRONG                                               = "REFUEL_TRANSACTION_CREATE_401";

    public final static String REFUEL_TRANSACTION_CREATE_CAP_FAILURE                                             = "REFUEL_TRANSACTION_CREATE_402";

    public final static String REFUEL_TRANSACTION_CREATE_ERROR_CREATE_VOUCHER                                    = "REFUEL_TRANSACTION_CREATE_403";

    public final static String REFUEL_TRANSACTION_CREATE_PV_NOT_ENABLED                                          = "REFUEL_TRANSACTION_CREATE_404";

    public final static String REFUEL_TRANSACTION_CREATE_USE_CREDIT_CARD                                         = "REFUEL_TRANSACTION_CREATE_405";

    public final static String REFUEL_TRANSACTION_CREATE_ERROR_PIN_ATTEMPTS_LEFT                                 = "REFUEL_TRANSACTION_CREATE_406";

    public final static String REFUEL_TRANSACTION_CREATE_ERROR_PIN_LAST_ATTEMPT                                  = "REFUEL_TRANSACTION_CREATE_407";

    public final static String REFUEL_TRANSACTION_CREATE_ERROR_PIN                                               = "REFUEL_TRANSACTION_CREATE_408";

    public final static String REFUEL_TRANSACTION_CREATE_BUSINESS_NOT_ACTIVE                                     = "REFUEL_TRANSACTION_CREATE_409";
    
    public final static String REFUEL_TRANSACTION_CREATE_MULTICARD_NOT_ACTIVE                                    = "REFUEL_TRANSACTION_CREATE_410";
    
    public final static String REFUEL_TRANSACTION_CREATE_CREDIT_CARD_INVALID_AMOUNT                              = "REFUEL_TRANSACTION_CREATE_413";
   
    public final static String REFUEL_TRANSACTION_CREATE_MULTICARD_INVALID_AMOUNT                                = "REFUEL_TRANSACTION_CREATE_414";
    
    public final static String UNDO_REFUEL_SUCCESS                                                               = "REFUEL_CANCEL_200";

    public final static String UNDO_REFUEL_FAILURE                                                               = "REFUEL_CANCEL_300";

    public final static String UNDO_REFUEL_ID_WRONG                                                              = "REFUEL_CANCEL_400";

    public final static String CONFIRM_REFUEL_SUCCESS                                                            = "REFUEL_CONFIRM_200";

    public final static String CONFIRM_REFUEL_FAILURE                                                            = "REFUEL_CONFIRM_300";

    public final static String CONFIRM_REFUEL_ID_WRONG                                                           = "REFUEL_CONFIRM_400";

    public final static String RETRIEVE_PENDING_REFUEL_SUCCESS                                                   = "REFUEL_PENDING_200";

    public final static String RETRIEVE_PENDING_REFUEL_FAILURE                                                   = "REFUEL_PENDING_300";

    public final static String RETRIEVE_PENDING_REFUEL_INVALID_REQUEST                                           = "USER_REQU_400";

    public final static String RETRIEVE_PENDING_REFUEL_INVALID_TICKET                                            = "USER_REQU_401";

    public final static String RETRIEVE_REFUEL_DETAIL_SUCCESS                                                    = "RETRIEVE_PAYMENT_DETAIL_200";

    public final static String RETRIEVE_REFUEL_DETAIL_FAILURE                                                    = "RETRIEVE_PAYMENT_DETAIL_300";

    public final static String RETRIEVE_REFUEL_DETAIL_WRONG_ID                                                   = "RETRIEVE_PAYMENT_DETAIL_400";

    public final static String RETRIEVE_REFUEL_DETAIL_INVALID_REQUEST                                            = "USER_REQU_400";

    public final static String RETRIEVE_REFUEL_DETAIL_INVALID_TICKET                                             = "USER_REQU_401";

    public final static String RETRIEVE_REFUEL_HISTORY_SUCCESS                                                   = "RETRIEVE_PAYMENT_HISTORY_200";

    public final static String RETRIEVE_REFUEL_HISTORY_FAILURE                                                   = "RETRIEVE_PAYMENT_HISTORY_300";

    public final static String RETRIEVE_REFUEL_HISTORY_WRONG_ID                                                  = "RETRIEVE_PAYMENT_HISTORY_400";

    public final static String RETRIEVE_REFUEL_HISTORY_INVALID_REQUEST                                           = "USER_REQU_400";

    public final static String RETRIEVE_REFUEL_HISTORY_INVALID_TICKET                                            = "USER_REQU_401";

    public final static String RETRIEVE_TRANSACTION_HISTORY_SUCCESS                                              = "RETRIEVE_TRANSACTION_HISTORY_200";

    public final static String RETRIEVE_TRANSACTION_HISTORY_FAILURE                                              = "RETRIEVE_TRANSACTION_HISTORY_300";

    public final static String RETRIEVE_TRANSACTION_HISTORY_WRONG_ID                                             = "RETRIEVE_TRANSACTION_HISTORY_400";

    public final static String RETRIEVE_TRANSACTION_HISTORY_INVALID_REQUEST                                      = "USER_REQU_400";

    public final static String RETRIEVE_TRANSACTION_HISTORY_INVALID_TICKET                                       = "USER_REQU_401";

    public final static String RETRIEVE_REFUEL_EVENTS_SUCCESS                                                    = "MESSAGE_RECEIVED_200";

    public final static String RETRIEVE_REFUEL_EVENTS_FAILURE                                                    = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String RETRIEVE_REFUEL_EVENTS_WRONG_ID                                                   = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String RETRIEVE_CITIES_SUCCESS                                                           = "RET_CIT_200";

    public final static String RETRIEVE_CITIES_FAILED                                                            = "RET_CIT_300";

    public final static String RETRIEVE_CITIES_INVALID_TICKET                                                    = "USER_REQU_401";

    public final static String REFRESH_STATION_SUCCESS                                                           = "REFRESH_STATION_200";

    public final static String REFRESH_STATION_NO_UPDATE                                                         = "REFRESH_STATION_202";

    public final static String REFRESH_STATION_FAILED                                                            = "REFRESH_STATION_300";

    public final static String REFRESH_STATION_NOT_FOUND                                                         = "REFRESH_STATION_400";

    public final static String REFRESH_STATION_INVALID_TICKET                                                    = "USER_REQU_401";

    public final static String TRANSACTION_UPDATE_FINAL_STATUS_SUCCESS                                           = "TRANSACTION_UPDATE_FINAL_STATUS_200";

    public final static String TRANSACTION_UPDATE_FINAL_STATUS_ERROR                                             = "TRANSACTION_UPDATE_FINAL_STATUS_300";

    public final static String TRANSACTION_UPDATE_FINAL_AMOUNT_SUCCESS                                           = "TRANSACTION_UPDATE_FINAL_AMOUNT_200";

    public final static String TRANSACTION_UPDATE_FINAL_AMOUNT_ERROR                                             = "TRANSACTION_UPDATE_FINAL_AMOUNT_300";

    public final static String TRANSACTION_UPDATE_RECONCILIATION_ATTEMPTS_SUCCESS                                = "TRANSACTION_UPDATE_RECONCILIATION_ATTEMPTS_200";

    public final static String TRANSACTION_UPDATE_RECONCILIATION_ATTEMPTS_ERROR                                  = "TRANSACTION_UPDATE_RECONCILIATION_ATTEMPTS_300";

    public final static String TRANSACTION_FINALIZE_PENDING_REFUEL_SUCCESS                                       = "TRANSACTION_FINALIZE_PENDING_REFUEL_200";

    public final static String TRANSACTION_FINALIZE_PENDING_REFUEL_FAILURE                                       = "TRANSACTION_FINALIZE_PENDING_REFUEL_300";

    public final static String TRANSACTION_UPDATE_VOUCHER_RECONCILIATE_SUCCESS                                   = "TRANSACTION_UPDATE_VOUCHER_RECONCILIATE_SUCCESS_200";

    public final static String TRANSACTION_UPDATE_VOUCHER_RECONCILIATE_FAILURE                                   = "TRANSACTION_UPDATE_VOUCHER_RECONCILIATE_SUCCESS_300";

    public final static String TRANSACTION_CREATE_AND_CONSUME_VOUCHER_SUCCESS                                    = "TRANSACTION_CREATE_AND_CONSUME_VOUCHER_200";

    public final static String TRANSACTION_CREATE_AND_CONSUME_VOUCHER_KO                                         = "TRANSACTION_CREATE_AND_CONSUME_VOUCHER_300";

    public final static String TRANSACTION_CREATE_AND_CONSUME_VOUCHER_ERROR                                      = "TRANSACTION_CREATE_AND_CONSUME_VOUCHER_301";

    public final static String TRANSACTION_CREATE_AND_CONSUME_VOUCHER_TRANSACTION_NOT_FOUND                      = "TRANSACTION_CREATE_AND_CONSUME_VOUCHER_302";

    public final static String TRANSACTION_CREATE_AND_CONSUME_VOUCHER_AMOUNT_ERROR                               = "TRANSACTION_CREATE_AND_CONSUME_VOUCHER_303";

    public final static String TRANSACTION_REVERSE_CREATE_AND_CONSUME_VOUCHER_SUCCESS                            = "TRANSACTION_REVERSE_CREATE_AND_CONSUME_VOUCHER_200";

    public final static String TRANSACTION_REVERSE_CREATE_AND_CONSUME_VOUCHER_KO                                 = "TRANSACTION_REVERSE_CREATE_AND_CONSUME_VOUCHER_300";

    public final static String TRANSACTION_REVERSE_CREATE_AND_CONSUME_VOUCHER_ERROR                              = "TRANSACTION_REVERSE_CREATE_AND_CONSUME_VOUCHER_301";

    public final static String TRANSACTION_REVERSE_CREATE_AND_CONSUME_VOUCHER_TRANSACTION_NOT_FOUND              = "TRANSACTION_REVERSE_CREATE_AND_CONSUME_VOUCHER_302";

    public final static String CHECK_TRANSACTION_AUTHORIZATION_SUCCESS                                           = "CHECK_TRANSACTION_AUTH_200";

    public final static String CHECK_TRANSACTION_AUTHORIZATION_FAILURE                                           = "CHECK_TRANSACTION_AUTH_300";

    public final static String CHECK_TRANSACTION_AUTHORIZATION_NOT_FOUND                                         = "CHECK_TRANSACTION_AUTH_400";

    public final static String CHECK_TRANSACTION_AUTHORIZATION_USER_NOT_FOUND                                    = "CHECK_TRANSACTION_AUTH_401";

    public final static String CHECK_TRANSACTION_AUTHORIZATION_WRONG_STATUS                                      = "CHECK_TRANSACTION_AUTH_402";

    public final static String CHECK_TRANSACTION_AUTHORIZATION_INSUFFICIENT_CAP                                  = "CHECK_TRANSACTION_AUTH_403";

    public final static String CHECK_TRANSACTION_AUTHORIZATION_UNAUTHORIZED                                      = "CHECK_TRANSACTION_AUTH_404";

    public final static String CHECK_TRANSACTION_AUTHORIZATION_TESTER                                            = "CHECK_TRANSACTION_AUTH_201";

    public final static String CHECK_TRANSACTION_AUTHORIZATION_NO_DELETE                                         = "CHECK_TRANSACTION_AUTH_301";

    public final static String UPDATE_AVAILABLE_CAP_SUCCESS                                                      = "UPDATE_AVAILABLE_CAP_200";

    public final static String UPDATE_AVAILABLE_CAP_FAILURE                                                      = "UPDATE_AVAILABLE_CAP_300";

    public final static String UPDATE_AVAILABLE_CAP_INSUFFICIENT_CAP                                             = "UPDATE_AVAILABLE_CAP_400";

    public final static String UPDATE_AVAILABLE_CAP_TRANSACTION_NOT_FOUND                                        = "UPDATE_AVAILABLE_CAP_401";

    public final static String UPDATE_AVAILABLE_CAP_USER_NOT_FOUND                                               = "UPDATE_AVAILABLE_CAP_402";

    public final static String UPDATE_AVAILABLE_CAP_INVALID_PAYMENT_STATUS                                       = "UPDATE_AVAILABLE_CAP_403";

    public final static String UPDATE_AVAILABLE_CAP_WRONG_TRANSACTION_STATUS                                     = "UPDATE_AVAILABLE_CAP_404";

    public final static String UPDATE_EFFECTIVE_CAP_SUCCESS                                                      = "UPDATE_EFFECTIVE_CAP_200";

    public final static String UPDATE_EFFECTIVE_CAP_FAILURE                                                      = "UPDATE_EFFECTIVE_CAP_300";

    public final static String UPDATE_EFFECTIVE_CAP_INSUFFICIENT_CAP                                             = "UPDATE_EFFECTIVE_CAP_400";

    public final static String UPDATE_EFFECTIVE_CAP_TRANSACTION_NOT_FOUND                                        = "UPDATE_EFFECTIVE_CAP_401";

    public final static String UPDATE_EFFECTIVE_CAP_USER_NOT_FOUND                                               = "UPDATE_EFFECTIVE_CAP_402";

    public final static String UPDATE_EFFECTIVE_CAP_INVALID_PAYMENT_STATUS                                       = "UPDATE_EFFECTIVE_CAP_403";

    public final static String UPDATE_EFFECTIVE_CAP_WRONG_TRANSACTION_STATUS                                     = "UPDATE_EFFECTIVE_CAP_404";

    public final static String UPDATE_EFFECTIVE_CAP_FINAL_AMOUNT_NOT_VALID                                       = "UPDATE_EFFECTIVE_CAP_405";

    public final static String REFUND_AVAILABLE_CAP_SUCCESS                                                      = "REFUND_AVAILABLE_CAP_200";

    public final static String REFUND_AVAILABLE_CAP_FAILURE                                                      = "REFUND_AVAILABLE_CAP_300";

    public final static String REFUND_AVAILABLE_CAP_TRANSACTION_NOT_FOUND                                        = "REFUND_AVAILABLE_CAP_401";

    public final static String REFUND_AVAILABLE_CAP_USER_NOT_FOUND                                               = "REFUND_AVAILABLE_CAP_402";

    public final static String REFUND_AVAILABLE_CAP_INVALID_PAYMENT_STATUS                                       = "REFUND_AVAILABLE_CAP_403";

    public final static String REFUND_AVAILABLE_CAP_WRONG_TRANSACTION_STATUS                                     = "REFUND_AVAILABLE_CAP_404";

    public final static String ADMIN_AUTH_SUCCESS                                                                = "ADMIN_AUTH_200";

    public final static String ADMIN_AUTH_LOGIN_ERROR                                                            = "ADMIN_AUTH_300";

    public final static String ADMIN_AUTH_UNAUTHORIZED                                                           = "ADMIN_AUTH_400";

    public final static String ADMIN_INVALID_TICKET                                                              = "ADMIN_REQU_401";

    public final static String ADMIN_LOGOUT_SUCCESS                                                              = "ADMIN_LOGOUT_200";

    public final static String ADMIN_LOGOUT_INVALID_TICKET                                                       = "ADMIN_REQU_401";

    public final static String ADMIN_CREATE_ADMIN_SUCCESS                                                        = "ADMIN_CREATE_200";

    public final static String ADMIN_CREATE_ADMIN_FAILURE                                                        = "ADMIN_CREATE_300";

    public final static String ADMIN_CREATE_ADMIN_EXISTS                                                         = "ADMIN_CREATE_400";

    public final static String ADMIN_CREATE_INVALID_TICKET                                                       = "ADMIN_REQU_401";

    public final static String ADMIN_CREATE_MANAGER_SUCCESS                                                      = "ADMIN_CREATE_MANAGER_200";

    public final static String ADMIN_CREATE_MANAGER_USERNAME_OR_EMAIL_EXIST                                      = "ADMIN_CREATE_MANAGER_400";

    public final static String ADMIN_DELETE_MANAGER_SUCCESS                                                      = "ADMIN_DELETE_MANAGER_200";

    public final static String ADMIN_DELETE_MANAGER_FAILURE                                                      = "ADMIN_DELETE_MANAGER_300";

    public final static String ADMIN_DELETE_MANAGER_INVALID_PARAMETERS                                           = "ADMIN_DELETE_MANAGER_301";

    public final static String ADMIN_DELETE_MANAGER_USER_NOT_FOUND                                               = "ADMIN_DELETE_MANAGER_302";

    public final static String ADMIN_UPDATE_MANAGER_SUCCESS                                                      = "ADMIN_UPDATE_MANAGER_200";

    public final static String ADMIN_UPDATE_MANAGER_FAILURE                                                      = "ADMIN_UPDATE_MANAGER_300";

    public final static String ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS                                           = "ADMIN_UPDATE_MANAGER_301";

    public final static String ADMIN_UPDATE_MANAGER_USER_NOT_FOUND                                               = "ADMIN_UPDATE_MANAGER_302";

    public final static String ADMIN_UPDATE_MANAGER_USERNAME_OR_EMAIL_EXIST                                      = "ADMIN_UPDATE_MANAGER_400";

    public final static String ADMIN_SEARCH_MANAGER_SUCCESS                                                      = "ADMIN_SEARCH_MANAGER_200";

    public final static String ADMIN_SEARCH_MANAGER_FAILURE                                                      = "ADMIN_SEARCH_MANAGER_300";

    public final static String ADMIN_SEARCH_MANAGER_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_SEARCH_MANAGER_INVALID_PARAMETERS                                           = "ADMIN_SEARCH_MANAGER_301";

    public final static String ADMIN_ADDSTATION_MANAGER_SUCCESS                                                  = "ADMIN_ADDSTATION_MANAGER_200";

    public final static String ADMIN_ADDSTATION_MANAGER_FAILURE                                                  = "ADMIN_ADDSTATION_MANAGER_300";

    public final static String ADMIN_ADDSTATION_MANAGER_INVALID_PARAMETERS                                       = "ADMIN_ADDSTATION_MANAGER_301";

    public final static String ADMIN_ADDSTATION_MANAGER_INVALID_TICKET                                           = "ADMIN_REQU_401";

    public final static String ADMIN_ADDSTATION_MANAGER_MANAGERID_NOT_FOUND                                      = "ADMIN_ADDSTATION_MANAGER_302";

    public final static String ADMIN_ADDSTATION_MANAGER_STATIONID_NOT_FOUND                                      = "ADMIN_ADDSTATION_MANAGER_303";

    public final static String ADMIN_ADDSTATION_MANAGER_STATION_ALREADY_ADDED                                    = "ADMIN_ADDSTATION_MANAGER_304";

    public final static String ADMIN_RETRIEVETRANSACTIONS_MANAGER_SUCCESS                                        = "ADMIN_RETRIEVETRANSACTIONS_MANAGER_200";

    public final static String ADMIN_RETRIEVETRANSACTIONS_MANAGER_FAILURE                                        = "ADMIN_RETRIEVETRANSACTIONS_MANAGER_300";

    public final static String ADMIN_RETRIEVETRANSACTIONS_INVALID_PARAMETERS                                     = "ADMIN_RETRIEVETRANSACTIONS_MANAGER_301";

    public final static String ADMIN_RETRIEVETRANSACTIONS_INVALID_TICKET                                         = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS                                               = "ADMIN_RETRIVE_LOG_200";

    public final static String ADMIN_RETRIEVE_ACTIVITY_LOG_FAILURE                                               = "ADMIN_RETRIVE_LOG_300";

    public final static String ADMIN_RETRIEVE_ACTIVITY_LOG_INVALID_TICKET                                        = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_USER_LIST_SUCCESS                                                  = "ADMIN_RETRIVE_USER_LIST_200";

    public final static String ADMIN_RETRIEVE_USER_LIST_FAILURE                                                  = "ADMIN_RETRIVE_USER_LIST_300";

    public final static String ADMIN_RETRIEVE_USER_LIST_INVALID_TICKET                                           = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_TRANSACTION_LIST_SUCCESS                                           = "ADMIN_RETRIVE_TRANSACTION_200";

    public final static String ADMIN_RETRIEVE_TRANSACTION_LIST_FAILURE                                           = "ADMIN_RETRIVE_TRANSACTION_300";

    public final static String ADMIN_RETRIEVE_TRANSACTION_LIST_INVALID_TICKET                                    = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_POP_TRANSACTION_LIST_SUCCESS                                       = "ADMIN_RETRIVE_POP_TRANSACTION_200";

    public final static String ADMIN_RETRIEVE_POP_TRANSACTION_LIST_FAILURE                                       = "ADMIN_RETRIVE_POP_TRANSACTION_300";

    public final static String ADMIN_RETRIEVE_POP_TRANSACTION_LIST_INVALID_TICKET                                = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_POP_TRANSACTION_RECONCILIATION_SUCCESS                             = "ADMIN_RETRIVE_POP_TRANSACTION_RECONCILIATION_200";

    public final static String ADMIN_RETRIEVE_POP_TRANSACTION_RECONCILIATION_FAILURE                             = "ADMIN_RETRIVE_POP_TRANSACTION_RECONCILIATION_300";

    public final static String ADMIN_RETRIEVE_POP_TRANSACTION_RECONCILIATION_INVALID_TICKET                      = "ADMIN_REQU_401";

    public final static String ADMIN_FIND_PAYMENT_METHOD_SUCCESS                                                 = "ADMIN_FIND_PAYMENT_METHOD_200";

    public final static String ADMIN_FIND_PAYMENT_METHOD_FAILURE                                                 = "ADMIN_FIND_PAYMENT_METHOD_300";

    public final static String ADMIN_FIND_PAYMENT_METHOD_INVALID_TICKET                                          = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_USER_SUCCESS                                                         = "ADMIN_UPDATE_USER_200";

    public final static String ADMIN_UPDATE_USER_FAILURE                                                         = "ADMIN_UPDATE_USER_300";

    public final static String ADMIN_UPDATE_USER_INVALID_TICKET                                                  = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_TRANSACTION_SUCCESS                                                  = "ADMIN_UPDATE_TRANSACTION_200";

    public final static String ADMIN_UPDATE_TRANSACTION_FAILURE                                                  = "ADMIN_UPDATE_TRANSACTION_300";

    public final static String ADMIN_UPDATE_TRANSACTION_INVALID_TICKET                                           = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_PAYMENT_METHOD_SUCCESS                                               = "ADMIN_UPDATE_PAYMENT_METHOD_200";

    public final static String ADMIN_UPDATE_PAYMENT_METHOD_FAILURE                                               = "ADMIN_UPDATE_PAYMENT_METHOD_300";

    public final static String ADMIN_UPDATE_PAYMENT_METHOD_INVALID_TICKET                                        = "ADMIN_REQU_401";

    public final static String ADMIN_DELETE_USER_SUCCESS                                                         = "ADMIN_DELETE_USER_200";

    public final static String ADMIN_DELETE_USER_FAILURE                                                         = "ADMIN_DELETE_USER_300";

    public final static String ADMIN_DELETE_USER_INVALID_TICKET                                                  = "ADMIN_REQU_401";

    public final static String ADMIN_DELETE_INVALID_TICKET                                                       = "ADMIN_REQU_401";

    public final static String ADMIN_GENERATE_TESTERS_SUCCESS                                                    = "ADMIN_GENERATE_TESTERS_200";

    public final static String ADMIN_GENERATE_TESTERS_FAILURE                                                    = "ADMIN_GENERATE_TESTERS_300";

    public final static String ADMIN_GENERATE_TESTERS_INVALID_TICKET                                             = "ADMIN_REQU_401";

    public final static String ADMIN_DELETE_TESTERS_SUCCESS                                                      = "ADMIN_DELETE_TESTERS_200";

    public final static String ADMIN_DELETE_TESTERS_FAILURE                                                      = "ADMIN_DELETE_TESTERS_300";

    public final static String ADMIN_DELETE_TESTERS_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_STATISTICS_SUCCESS                                                 = "ADMIN_RETRIEVE_STATISTICS_200";

    public final static String ADMIN_RETRIEVE_STATISTICS_FAILURE                                                 = "ADMIN_RETRIEVE_STATISTICS_300";

    public final static String ADMIN_RETRIEVE_STATISTICS_INVALID_TICKET                                          = "ADMIN_REQU_401";

    public final static String ADMIN_ADD_STATION_SUCCESS                                                         = "ADMIN_ADD_STATION_200";

    public final static String ADMIN_ADD_STATION_FAILURE                                                         = "ADMIN_ADD_STATION_300";

    public final static String ADMIN_ADD_STATION_INVALID_TICKET                                                  = "ADMIN_REQU_401";

    public final static String ADMIN_DELETE_STATION_SUCCESS                                                      = "ADMIN_DELETE_STATION_200";

    public final static String ADMIN_DELETE_STATION_FAILURE                                                      = "ADMIN_DELETE_STATION_300";

    public final static String ADMIN_DELETE_STATION_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_STATION_SUCCESS                                                      = "ADMIN_UPDATE_STATION_200";

    public final static String ADMIN_UPDATE_STATION_FAILURE                                                      = "ADMIN_UPDATE_STATION_300";

    public final static String ADMIN_UPDATE_STATION_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_SEARCH_STATION_SUCCESS                                                      = "ADMIN_SEARCH_STATION_200";

    public final static String ADMIN_SEARCH_STATION_FAILURE                                                      = "ADMIN_SEARCH_STATION_300";

    public final static String ADMIN_SEARCH_STATION_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_ADD_STATIONS_SUCCESS                                                        = "ADMIN_ADD_STATIONS_200";

    public final static String ADMIN_ADD_STATIONS_FAILURE                                                        = "ADMIN_ADD_STATIONS_300";

    public final static String ADMIN_ADD_STATIONS_INVALID_TICKET                                                 = "ADMIN_REQU_401";

    public final static String ADMIN_DELETE_STATIONS_SUCCESS                                                     = "ADMIN_DELETE_STATIONS_200";

    public final static String ADMIN_DELETE_STATIONS_FAILURE                                                     = "ADMIN_DELETE_STATIONS_300";

    public final static String ADMIN_DELETE_STATIONS_INVALID_TICKET                                              = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_PARAMS_SUCCESS                                                     = "ADMIN_RETRIEVE_PARAMS_200";

    public final static String ADMIN_RETRIEVE_PARAMS_FAILURE                                                     = "ADMIN_RETRIEVE_PARAMS_300";

    public final static String ADMIN_RETRIEVE_PARAMS_INVALID_TICKET                                              = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_PARAM_SUCCESS                                                        = "ADMIN_UPDATE_PARAM_200";

    public final static String ADMIN_UPDATE_PARAM_FAILURE                                                        = "ADMIN_UPDATE_PARAM_300";

    public final static String ADMIN_UPDATE_PARAM_INVALID_TICKET                                                 = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_PARAM_UNRECOGNIZED_OPERATION                                         = "ADMIN_UPDATE_PARAM_402";

    public final static String ADMIN_UPDATE_PARAM_ALREADY_EXISTS                                                 = "ADMIN_UPDATE_PARAM_403";

    public final static String ADMIN_UPDATE_PARAMS_SUCCESS                                                       = "ADMIN_UPDATE_PARAMS_200";

    public final static String ADMIN_UPDATE_PARAMS_FAILURE                                                       = "ADMIN_UPDATE_PARAMS_300";

    public final static String USER_RETRIEVE_TERMS_SERVICE_SUCCESS                                               = "USER_RETRIEVE_TERMS_SERVICE_200";

    public final static String USER_RETRIEVE_TERMS_SERVICE_FAILURE                                               = "USER_RETRIEVE_TERMS_SERVICE_300";

    public final static String USER_RETRIEVE_TERMS_SERVICE_INVALID_TICKET                                        = "USER_REQU_401";

    public final static String USER_RETRIEVE_DOCUMENT_SUCCESS                                                    = "USER_RETRIEVE_DOCUMENT_200";

    public final static String USER_RETRIEVE_DOCUMENT_FAILURE                                                    = "USER_RETRIEVE_DOCUMENT_300";

    public final static String USER_RETRIEVE_DOCUMENT_INVALID_TICKET                                             = "USER_REQU_401";

    public final static String USER_RETRIEVE_ACTIVE_DEVICE_SUCCESS                                               = "USER_RETACTDEV_200";

    public final static String USER_RETRIEVE_ACTIVE_DEVICE_FAILURE                                               = "USER_RETACTDEV_300";

    public final static String USER_RETRIEVE_ACTIVE_INVALID_TICKET                                               = "USER_REQU_401";

    public final static String USER_RESEND_VALIDATATION_SUCCESS                                                  = "USER_RESEND_VALIDATATION_200";

    public final static String USER_RESEND_VALIDATATION_STATUS_NOT_PENDING                                       = "USER_RESEND_VALIDATATION_300";

    public final static String USER_RESEND_VALIDATATION_NOT_ASSOCIATED                                           = "USER_RESEND_VALIDATATION_301";

    public final static String USER_RESEND_VALIDATATION_INVALID_TYPE                                             = "USER_RESEND_VALIDATATION_302";

    public final static String USER_RESEND_VALIDATATION_FAILURE                                                  = "USER_RESEND_VALIDATATION_400";

    public final static String USER_RESEND_DEVICE_VALIDATATION_SUCCESS                                           = "USER_RESEND_DEVICE_VALIDATATION_200";

    public final static String USER_RESEND_DEVICE_VALIDATATION_STATUS_NOT_PENDING                                = "USER_RESEND_DEVICE_VALIDATATION_300";

    public final static String USER_RESEND_DEVICE_VALIDATATION_NOT_ASSOCIATED                                    = "USER_RESEND_DEVICE_VALIDATATION_301";

    public final static String USER_RESEND_DEVICE_VALIDATATION_INVALID_TYPE                                      = "USER_RESEND_DEVICE_VALIDATATION_302";

    public final static String USER_RESEND_DEVICE_VALIDATATION_FAILURE                                           = "USER_RESEND_DEVICE_VALIDATATION_400";

    public final static String ADMIN_ARCHIVE_SUCCESS                                                             = "ADMIN_ARCHIVE_200";

    public final static String ADMIN_ARCHIVE_FAILURE                                                             = "ADMIN_ARCHIVE_300";

    public final static String ADMIN_ARCHIVE_INVALID_TICKET                                                      = "USER_REQU_401";

    public final static String ADMIN_POPARCHIVE_SUCCESS                                                          = "ADMIN_POPARCHIVE_200";

    public final static String ADMIN_POPARCHIVE_FAILURE                                                          = "ADMIN_POPARCHIVE_300";

    public final static String ADMIN_POPARCHIVE_INVALID_TICKET                                                   = "ADMIN_REQU_401";

    public final static String ADMIN_RESEND_CONFIRM_USER_EMAIL_SUCCESS                                           = "ADMIN_RESEND_CONFIRM_EMAIL_200";

    public final static String ADMIN_RESEND_CONFIRM_USER_EMAIL_FAILURE                                           = "ADMIN_RESEND_CONFIRM_EMAIL_300";

    public final static String ADMIN_RECONCILE_INVALID_TICKET                                                    = "ADMIN_REQU_401";

    public final static String ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS                                           = "ADMIN_CHECK_AUTH_200";

    public final static String ADMIN_CHECK_ADMIN_AUTHORIZATION_UNAUTHORIZED                                      = "ADMIN_CHECK_AUTH_300";

    public final static String ADMIN_CHECK_ADMIN_AUTHORIZATION_INVALID_TICKET                                    = "ADMIN_REQU_401";

    public final static String ADMIN_ADD_VOUCHER_TP_PROMOTION_INVALID_TICKET                                     = "ADMIN_REQU_401";

    public final static String ADMIN_ADD_VOUCHER_TO_PROMOTION_SUCCESS                                            = "ADMIN_ADD_VOUCHER_TO_PROMOTION_200";

    public final static String ADMIN_ADD_VOUCHER_TO_PROMOTION_INVALID_PROMOTION                                  = "ADMIN_ADD_VOUCHER_TO_PROMOTION_300";

    public final static String ADMIN_ADD_VOUCHER_TO_PROMOTION_NO_VOUCHER                                         = "ADMIN_ADD_VOUCHER_TO_PROMOTION_302";

    public final static String ADMIN_ADD_VOUCHER_TO_PROMOTION_VOUCHER_ALREADY_PRESENT                            = "ADMIN_ADD_VOUCHER_TO_PROMOTION_304";

    public final static String ADMIN_SURVEY_CREATE_SUCCESS                                                       = "ADMIN_SURVEY_CREATE_200";

    public final static String ADMIN_SURVEY_CREATE_FAILURE                                                       = "ADMIN_SURVEY_CREATE_300";

    public final static String ADMIN_SURVEY_CREATE_INVALID_CODE                                                  = "ADMIN_SURVEY_CREATE_301";

    public final static String ADMIN_SURVEY_UPDATE_SUCCESS                                                       = "ADMIN_SURVEY_UPDATE_200";

    public final static String ADMIN_SURVEY_UPDATE_FAILURE                                                       = "ADMIN_SURVEY_UPDATE_300";

    public final static String ADMIN_SURVEY_UPDATE_INVALID_CODE                                                  = "ADMIN_SURVEY_UPDATE_301";

    public final static String ADMIN_SURVEY_QUESTION_ADD_SUCCESS                                                 = "ADMIN_SURVEY_QUESTION_ADD_200";

    public final static String ADMIN_SURVEY_QUESTION_ADD_FAILURE                                                 = "ADMIN_SURVEY_QUESTION_ADD_300";

    public final static String ADMIN_SURVEY_QUESTION_ADD_INVALID_SURVEY_CODE                                     = "ADMIN_SURVEY_QUESTION_ADD_301";

    public final static String ADMIN_SURVEY_QUESTION_ADD_INVALID_QUESTION_CODE                                   = "ADMIN_SURVEY_QUESTION_ADD_302";

    public final static String ADMIN_SURVEY_QUESTION_UPDATE_SUCCESS                                              = "ADMIN_SURVEY_QUESTION_UPDATE_200";

    public final static String ADMIN_SURVEY_QUESTION_UPDATE_FAILURE                                              = "ADMIN_SURVEY_QUESTION_UPDATE_300";

    public final static String ADMIN_SURVEY_QUESTION_UPDATE_INVALID_QUESTION_CODE                                = "ADMIN_SURVEY_QUESTION_UPDATE_301";

    public final static String ADMIN_SURVEY_QUESTION_DELETE_SUCCESS                                              = "ADMIN_SURVEY_QUESTION_DELETE_200";

    public final static String ADMIN_SURVEY_QUESTION_DELETE_FAILURE                                              = "ADMIN_SURVEY_QUESTION_DELETE_300";

    public final static String ADMIN_SURVEY_QUESTION_DELETE_INVALID_QUESTION_CODE                                = "ADMIN_SURVEY_QUESTION_DELETE_301";

    public final static String ADMIN_SURVEY_GET_SUCCESS                                                          = "ADMIN_SURVEY_GET_200";

    public final static String ADMIN_SURVEY_GET_FAILURE                                                          = "ADMIN_SURVEY_GET_300";

    public final static String ADMIN_SURVEY_RESET_SUCCESS                                                        = "ADMIN_SURVEY_RESET_200";

    public final static String ADMIN_SURVEY_RESET_FAILURE                                                        = "ADMIN_SURVEY_RESET_300";

    public final static String ADMIN_SURVEY_RESET_INVALID_SURVEY_CODE                                            = "ADMIN_SURVEY_RESET_301";

    public final static String RECONCILIATION_RECONCILE_SUCCESS                                                  = "RECONCILIATION_RECONCILE_200";

    public final static String RECONCILIATION_RECONCILE_FAILURE                                                  = "RECONCILIATION_RECONCILE_300";

    public final static String RECONCILIATION_RECONCILE_INVALID_TICKET                                           = "RECONCILIATION_REQU_401";

    public final static String RECONCILIATION_RECONCILE_INVALID_ID                                               = "RECONCILIATION_RECONCILE_402";

    public final static String RECONCILIATION_RECONCILE_INVALID_ACTION                                           = "RECONCILIATION_RECONCILE_403";

    public final static String RECONCILIATION_DETAIL_SUCCESS                                                     = "RECONCILIATION_DETAIL_200";

    public final static String RECONCILIATION_DETAIL_FAILURE                                                     = "RECONCILIATION_DETAIL_300";

    public final static String RECONCILIATION_DETAIL_INVALID_TICKET                                              = "RECONCILIATION_REQU_401";

    public final static String RECONCILIATION_TRANSACTION_SUCCESS                                                = "RECONCILIATION_TRANSACTION_200";

    public final static String RECONCILIATION_TRANSACTION_FAILURE                                                = "RECONCILIATION_TRANSACTION_300";

    public final static String RECONCILIATION_TRANSACTION_TO_RETRY                                               = "RECONCILIATION_TRANSACTION_400";

    public final static String RECONCILIATION_TRANSACTION_CANCELLED                                              = "RECONCILIATION_TRANSACTION_500";

    public final static String PP_TRANSACTION_CREATE_SUCCESS                                                     = "TRANSACTION_CREATED_200";

    public final static String PP_TRANSACTION_CREATE_FAILURE                                                     = "TRANSACTION_NOT_CREATED_500";

    public final static String PP_TRANSACTION_CREATE_STATION_PUMP_NOT_FOUND                                      = "STATION_PUMP_NOT_FOUND_404";

    public final static String PP_TRANSACTION_CREATE_INVALID_TICKET                                              = "USER_REQU_401";

    public final static String PP_TRANSACTION_CREATE_UNAUTHORIZED                                                = "USER_REQU_403";

    public final static String PP_TRANSACTION_CREATE_PAYMENT_WRONG                                               = "TRANSACTION_CREATED_400";

    public final static String PP_TRANSACTION_CREATE_PUMP_IN_FUELLING                                            = "TRANSACTION_CREATED_401";

    public final static String PP_TRANSACTION_CREATE_NOT_FOUND                                                   = "TRANSACTION_CREATED_402";

    public final static String PP_TRANSACTION_ENABLE_SUCCESS                                                     = "TRANSACTION_ENABLED_200";

    public final static String PP_TRANSACTION_ENABLE_NOT_RECOGNIZED                                              = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String PP_TRANSACTION_ENABLE_FAILURE                                                     = "TRANSACTION_NOT_ENABLED_500";

    public final static String PP_TRANSACTION_GET_SUCCESS                                                        = "MESSAGE_RECEIVED_200";

    public final static String PP_TRANSACTION_GET_NOT_RECOGNIZED                                                 = "TRANSACTION_NOT_RECOGNIZED_400";

    /*
     * public final static String PP_TRANSACTION_GET_UNAUTHORIZED = "TRANSACTION_NOT_RECOGNIZED_400";
     * public final static String PP_TRANSACTION_GET_FAILURE = "TRANSACTION_NOT_RECOGNIZED_400";
     */
    public final static String PP_GET_SOURCE_DETAIL_SUCCESS                                                      = "PP_GET_SOURCE_DETAIL_200";

    public final static String PP_GET_SOURCE_DETAIL_FAILURE                                                      = "PP_GET_SOURCE_DETAIL_300";

    public final static String PP_GET_SOURCE_DETAIL_NOT_RECOGNIZED                                               = "PP_GET_SOURCE_DETAIL_NOT_RECOGNIZED_400";

    public final static String PP_GET_SOURCE_DETAIL_INVALID_TICKET                                               = "USER_REQU_401";

    public final static String PP_GET_SOURCE_DETAIL_UNAUTHORIZED                                                 = "USER_REQU_403";

    public final static String PP_GET_SOURCE_DETAIL_INVALID_PAYMENT_METHOD                                       = "PP_GET_SOURCE_DETAIL_301";

    public final static String PP_GET_SOURCE_DETAIL_NO_VALID_PAYMENT_METHOD                                      = "PP_GET_SOURCE_DETAIL_302";
    
    public final static String PP_GET_SOURCE_DETAIL_NO_VALID_MULTICARD                                           = "PP_GET_SOURCE_DETAIL_303";

    public final static String PP_GET_SOURCE_DETAIL_PUMP_NOT_FOUND                                               = "PP_GET_SOURCE_DETAIL_401";

    public final static String PP_GET_SOURCE_DETAIL_CASH_NOT_FOUND                                               = "PP_GET_SOURCE_DETAIL_402";

    public final static String PP_GET_SOURCE_DETAIL_NO_PRE_PAID                                                  = "PP_GET_SOURCE_DETAIL_403";

    public final static String PP_GET_SOURCE_DETAIL_NO_POST_PAY_SHOP_ACTIVE                                      = "PP_GET_SOURCE_DETAIL_404";

    public final static String PP_GET_SOURCE_DETAIL_NO_POST_PAY_NO_SHOP_ACTIVE                                   = "PP_GET_SOURCE_DETAIL_405";

    public final static String PP_GET_SOURCE_DETAIL_NO_SHOP_ACTIVE                                               = "PP_GET_SOURCE_DETAIL_406";

    public final static String PP_GET_SOURCE_DETAIL_NO_STATION_ACTIVE                                            = "PP_GET_SOURCE_DETAIL_407";

    public final static String PP_GET_SOURCE_DETAIL_NO_POSTPAID_TRANSACTION                                      = "PP_TRANSACTION_NOT_RECOGNIZED_400";

    public final static String PP_GET_SOURCE_DETAIL_NO_SHOP_TRANSACTION                                          = "PP_TRANSACTION_NOT_RECOGNIZED_400";

    public final static String PP_GET_SOURCE_DETAIL_STATION_NOT_AVAILABLE                                        = "PP_GET_SOURCE_DETAIL_500";

    public final static String PP_GET_SOURCE_DETAIL_PUMP_NOT_AVAILABLE                                           = "PP_GET_SOURCE_DETAIL_501";

    public final static String PP_GET_SOURCE_DETAIL_PUMP_BUSY                                                    = "PUMP_BUSY_401";

    public final static String PP_GET_SOURCE_DETAIL_BUSINESS_NOT_ACTIVE                                          = "PP_GET_SOURCE_DETAIL_408";

    public final static String PP_TRANSACTION_REJECT_SUCCESS                                                     = "PP_TRANSACTION_REJECT_200";

    public final static String PP_TRANSACTION_REJECT_INVALID_TICKET                                              = "PP_TRANSACTION_REJECT_401";

    public final static String PP_TRANSACTION_REJECT_UNAUTHORIZED                                                = "PP_TRANSACTION_REJECT_403";

    public final static String PP_TRANSACTION_REJECT_NOT_RECOGNIZED                                              = "PP_TRANSACTION_REJECT_404";

    public final static String PP_TRANSACTION_REJECT_FAILURE                                                     = "PP_TRANSACTION_REJECT_500";

    public final static String PP_TRANSACTION_APPROVE_SUCCESS                                                    = "PP_TRANSACTION_APPROVE_200";

    public final static String PP_TRANSACTION_APPROVE_NOT_RECOGNIZED                                             = "PP_TRANSACTION_APPROVE_400";

    public final static String PP_TRANSACTION_APPROVE_INVALID_TICKET                                             = "USER_REQU_401";

    public final static String PP_TRANSACTION_APPROVE_UNAUTHORIZED                                               = "USER_REQU_403";

    public final static String PP_TRANSACTION_APPROVE_PIN_WRONG                                                  = "PP_TRANSACTION_APPROVE_401";

    public final static String PP_TRANSACTION_APPROVE_PAYMENT_WRONG                                              = "PP_TRANSACTION_APPROVE_402";

    public final static String PP_TRANSACTION_APPROVE_FAILURE                                                    = "PP_TRANSACTION_APPROVE_403";

    public final static String PP_TRANSACTION_APPROVE_AUTH_KO                                                    = "PP_TRANSACTION_APPROVE_405";

    public final static String PP_TRANSACTION_APPROVE_SETTLE_KO                                                  = "PP_TRANSACTION_APPROVE_406";

    public final static String PP_TRANSACTION_APPROVE_DELETE_KO                                                  = "PP_TRANSACTION_APPROVE_407";

    public final static String PP_TRANSACTION_APPROVE_REVERSE_KO                                                 = "PP_TRANSACTION_APPROVE_408";

    public final static String PP_TRANSACTION_APPROVE_REVERSE_OK                                                 = "PP_TRANSACTION_APPROVE_404";

    public final static String PP_TRANSACTION_APPROVE_SETTLE_OK                                                  = "PP_TRANSACTION_APPROVE_200";

    public final static String PP_TRANSACTION_NO_REFUEL_AND_CART_FOUND                                           = "PP_TRANSACTION_APPROVE_409";

    public final static String PP_TRANSACTION_APPROVE_CAP_FAILURE                                                = "PP_TRANSACTION_APPROVE_410";

    public final static String PP_TRANSACTION_APPROVE_ABEND                                                      = "PP_TRANSACTION_APPROVE_411";

    //public final static String PP_TRANSACTION_APPROVE_VOUCHER_AUTH_KO                       = "PP_TRANSACTION_APPROVE_412";
    public final static String PP_TRANSACTION_APPROVE_VOUCHER_PAY_KO                                             = "PP_TRANSACTION_APPROVE_413";

    public final static String PP_TRANSACTION_APPROVE_VOUCHER_UNUSABLE                                           = "PP_TRANSACTION_APPROVE_414";

    public final static String PP_TRANSACTION_APPROVE_USE_CREDIT_CARD                                            = "PP_TRANSACTION_APPROVE_415";

    public final static String PP_TRANSACTION_APPROVE_PV_NOT_ENABLED                                             = "PP_TRANSACTION_APPROVE_416";

    public final static String PP_TRANSACTION_APPROVE_BUSINESS_NOT_ACTIVE                                        = "PP_TRANSACTION_APPROVE_417";
    
    
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_ERROR                                    = "PP_TRANSACTION_APPROVE_420";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PARAMETERS                               = "PP_TRANSACTION_APPROVE_421";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_OPERATION                                = "PP_TRANSACTION_APPROVE_422";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_DPAN_NOT_FOUND                                   = "PP_TRANSACTION_APPROVE_423";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_OTP_NOT_FOUND                                    = "PP_TRANSACTION_APPROVE_424";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_TOTP_NOT_FOUND                                   = "PP_TRANSACTION_APPROVE_425";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_TOTP_OR_DPAN                             = "PP_TRANSACTION_APPROVE_426";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_LOYALTY_CARD                             = "PP_TRANSACTION_APPROVE_427";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PARTNER_TYPE                             = "PP_TRANSACTION_APPROVE_428";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_ALREADY_ENABLED                             = "PP_TRANSACTION_APPROVE_429";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_MSGREASONCODE_MANDATORY                          = "PP_TRANSACTION_APPROVE_430";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAN_NOT_FOUND                                    = "PP_TRANSACTION_APPROVE_431";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAN_ALREADY_USED                                 = "PP_TRANSACTION_APPROVE_432";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_EMAIL_ALREADY_USED                               = "PP_TRANSACTION_APPROVE_433";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_NOT_FOUND_FOR_OPERATION                  = "PP_TRANSACTION_APPROVE_434";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_USER_NOT_FOUND                                   = "PP_TRANSACTION_APPROVE_435";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_WITH_ANOTHER_DEVICE                      = "PP_TRANSACTION_APPROVE_436";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_CRIPTOGRAM                               = "PP_TRANSACTION_APPROVE_437";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZATION_NOT_FOUND                          = "PP_TRANSACTION_APPROVE_438";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_NOT_FOUND_FOR_REVERSAL                   = "PP_TRANSACTION_APPROVE_439";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_TIMEOUT_OPERATION                                = "PP_TRANSACTION_APPROVE_440";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_OPERATION_NOT_COMPLETED                          = "PP_TRANSACTION_APPROVE_441";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_0                                     = "PP_TRANSACTION_APPROVE_442";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_1                                     = "PP_TRANSACTION_APPROVE_443";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_PARTIAL_AMOUNT_2                      = "PP_TRANSACTION_APPROVE_444";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_3                                     = "PP_TRANSACTION_APPROVE_445";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_5                                     = "PP_TRANSACTION_APPROVE_446";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_PARTIAL_AMOUNT_6                      = "PP_TRANSACTION_APPROVE_447";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_7                                     = "PP_TRANSACTION_APPROVE_448";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_10                                    = "PP_TRANSACTION_APPROVE_449";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_80                                    = "PP_TRANSACTION_APPROVE_450";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_81                                    = "PP_TRANSACTION_APPROVE_451";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZATION_DENIED                             = "PP_TRANSACTION_APPROVE_452";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_EXPIRED                                     = "PP_TRANSACTION_APPROVE_453";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CONTACT_CALL_CENTER                              = "PP_TRANSACTION_APPROVE_454";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_WITH_RESTRICTIONS                           = "PP_TRANSACTION_APPROVE_455";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PIN_ATTEMPTS_EXPIRED                             = "PP_TRANSACTION_APPROVE_456";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CONTACT_ISSUER                                   = "PP_TRANSACTION_APPROVE_457";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_OPERATOR                                 = "PP_TRANSACTION_APPROVE_458";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_AMOUNT                                   = "PP_TRANSACTION_APPROVE_459";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_CARD_NUMBER                              = "PP_TRANSACTION_APPROVE_460";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PIN_REQUIRED                                     = "PP_TRANSACTION_APPROVE_461";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_ACCOUNT                                  = "PP_TRANSACTION_APPROVE_462";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_UNSUPPORTED_FUNCTION                             = "PP_TRANSACTION_APPROVE_463";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_MAX_LIMIT_REACHED                                = "PP_TRANSACTION_APPROVE_464";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_WRONG_PIN                                        = "PP_TRANSACTION_APPROVE_465";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_NOT_FOUND                                   = "PP_TRANSACTION_APPROVE_466";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_TRANSACTION_NOT_ENABLED_TO_HOLDER                = "PP_TRANSACTION_APPROVE_467";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_TRANSACTION_NOT_ENABLED_TO_POS                   = "PP_TRANSACTION_APPROVE_468";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PLAFOND_EXCEEDED                                 = "PP_TRANSACTION_APPROVE_469";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_LIMITATION_TIME_EXCEEDED                         = "PP_TRANSACTION_APPROVE_470";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INACTIVE_CARD                                    = "PP_TRANSACTION_APPROVE_471";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PIN_BLOCK                                = "PP_TRANSACTION_APPROVE_472";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_WRONG_PIN_LENGTH                                 = "PP_TRANSACTION_APPROVE_473";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PIN_KEY_ERROR                                    = "PP_TRANSACTION_APPROVE_474";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_OUTDOOR_TRANSACTION_FORBIDDEN                    = "PP_TRANSACTION_APPROVE_475";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PUMP_NOT_CONNECTED_TRANSACTION_FORBIDDEN         = "PP_TRANSACTION_APPROVE_476";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_WRONG_MILEAGE                                    = "PP_TRANSACTION_APPROVE_477";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PARTNER_NOT_ENABLED_IN_PV                        = "PP_TRANSACTION_APPROVE_478";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PV_NOT_ENABLED_FOR_CARD                          = "PP_TRANSACTION_APPROVE_479";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_DAY_NOT_ENABLED_FOR_CARD                         = "PP_TRANSACTION_APPROVE_480";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_TIME_NOT_ENABLED_FOR_CARD                        = "PP_TRANSACTION_APPROVE_481";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_MANUAL_TRANSACTION_FORBIDDEN                     = "PP_TRANSACTION_APPROVE_482";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_IPERSELF_FORBIDDEN                               = "PP_TRANSACTION_APPROVE_483";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_FAIDATE_FORBIDDEN                                = "PP_TRANSACTION_APPROVE_484";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_OVERRIDE_FORBIDDEN                               = "PP_TRANSACTION_APPROVE_485";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_BOOKING_REQUIRED                                 = "PP_TRANSACTION_APPROVE_486";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_BOOKING_FOR_PRODUCT                      = "PP_TRANSACTION_APPROVE_487";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_BOOKING_FOR_PV                           = "PP_TRANSACTION_APPROVE_488";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_BOOKING_FOR_DAY                          = "PP_TRANSACTION_APPROVE_489";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_DRIVER_BLOCKED                                   = "PP_TRANSACTION_APPROVE_490";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PRODUCT_NOT_REGISTERED                           = "PP_TRANSACTION_APPROVE_491";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PRICE                                    = "PP_TRANSACTION_APPROVE_492";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_UNKNOWN_POS                                      = "PP_TRANSACTION_APPROVE_493";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_BLOCKED_POS                                      = "PP_TRANSACTION_APPROVE_494";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_NOT_ENABLED                                 = "PP_TRANSACTION_APPROVE_495";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_BLOCKED                                     = "PP_TRANSACTION_APPROVE_496";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_BLOCKED_CLIENT                                   = "PP_TRANSACTION_APPROVE_497";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_BLOCKED_CARD_ACCOUNT                             = "PP_TRANSACTION_APPROVE_498";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_BLOCKED_ISSUER                                   = "PP_TRANSACTION_APPROVE_499";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PRODUCT_NOT_ENABLED                              = "PP_TRANSACTION_APPROVE_500";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PIN_ATTEMPTS_EXHAUSTED                           = "PP_TRANSACTION_APPROVE_501";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_AMOUNT_FOR_COMPANY                       = "PP_TRANSACTION_APPROVE_502";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_NOT_ENABLED_0                               = "PP_TRANSACTION_APPROVE_503";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_NOT_ENABLED_1                               = "PP_TRANSACTION_APPROVE_504";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_NOT_ENABLED_2                               = "PP_TRANSACTION_APPROVE_505";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PRODUCT_PLAFOND_EXCEEDED                         = "PP_TRANSACTION_APPROVE_506";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INSUFFICIENT_CREDIT                              = "PP_TRANSACTION_APPROVE_507";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PLAFOND_EXCEEDED_1                               = "PP_TRANSACTION_APPROVE_508";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_EXPIRED_CARD                                     = "PP_TRANSACTION_APPROVE_509";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_EXPIRED_CARD_PICKUP                              = "PP_TRANSACTION_APPROVE_510";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_SUSPECTED_FRAUD                                  = "PP_TRANSACTION_APPROVE_511";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CALL_ACQUIRER                                    = "PP_TRANSACTION_APPROVE_512";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_WITH_RESTRICTIONS_0                         = "PP_TRANSACTION_APPROVE_513";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PIN_ATTEMPTS_EXHAUSTED_PICKUP                    = "PP_TRANSACTION_APPROVE_514";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_LOST_CARD                                        = "PP_TRANSACTION_APPROVE_515";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_STOLEN_CARD                                      = "PP_TRANSACTION_APPROVE_516";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_WRONG_FORMAT                                     = "PP_TRANSACTION_APPROVE_517";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CLOSURE_IN_PROGRESS                              = "PP_TRANSACTION_APPROVE_518";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CENTRAL_SYSTEM_NOT_WORKING                       = "PP_TRANSACTION_APPROVE_519";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_SYSTEM_MALFUNCTION                               = "PP_TRANSACTION_APPROVE_520";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_TIME_OUT_CARD_EMITTER                            = "PP_TRANSACTION_APPROVE_521";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_EMITTER_UNREACHABLE                         = "PP_TRANSACTION_APPROVE_522";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_WRONG_MAC                                        = "PP_TRANSACTION_APPROVE_523";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_ERROR_SINC_MAC_KEY                               = "PP_TRANSACTION_APPROVE_524";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_DECRYPTION_ERROR                                 = "PP_TRANSACTION_APPROVE_525";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_SECURITY_ERROR                                   = "PP_TRANSACTION_APPROVE_526";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_MESSAGE_OUT_OF_SEQUENCE                          = "PP_TRANSACTION_APPROVE_527";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_WRONG_OPERATOR                                   = "PP_TRANSACTION_APPROVE_528";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_WRONG_COMPANY                                    = "PP_TRANSACTION_APPROVE_529";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_GENERIC_ERROR                                    = "PP_TRANSACTION_APPROVE_530";

    /*
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_WRONG_INPUT_PARAMETERS                    = "PP_TRANSACTION_APPROVE_421";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PARTNER_TYPE                      = "PP_TRANSACTION_APPROVE_422";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAN_NOT_FOUND                             = "PP_TRANSACTION_APPROVE_423";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_ANOTHER_DEVICE_USED                       = "PP_TRANSACTION_APPROVE_424";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_CRYPTOGRAM_PARAMETERS             = "PP_TRANSACTION_APPROVE_425";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PIN_NOT_VERIFIED                          = "PP_TRANSACTION_APPROVE_426";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_CARD_EXPIRED                              = "PP_TRANSACTION_APPROVE_427";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_FAILED_MERCHANT                   = "PP_TRANSACTION_APPROVE_428";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_FAILED_SECURITY                   = "PP_TRANSACTION_APPROVE_429";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_FAILED_TERMINAL                   = "PP_TRANSACTION_APPROVE_430";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_FAILED_CUSTOMER                   = "PP_TRANSACTION_APPROVE_431";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INSUFFICIENT_CREDIT                       = "PP_TRANSACTION_APPROVE_432";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AUTHCODE_NOT_FOUND                        = "PP_TRANSACTION_APPROVE_433";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AMOUNT_GREATER_THAN_AUTHORIZED            = "PP_TRANSACTION_APPROVE_434";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_AMOUNT_GREATER_THAN_SETTLED               = "PP_TRANSACTION_APPROVE_435";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_MAX_LIMIT_REACHED                         = "PP_TRANSACTION_APPROVE_436";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_REVERSAL_FAILED                           = "PP_TRANSACTION_APPROVE_437";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_AMOUNT                            = "PP_TRANSACTION_APPROVE_438";
    
    public final static String RPP_TRANSACTION_APPROVE_MULTICARD_TIMEOUT_OPERATION                        = "PP_TRANSACTION_APPROVE_439";

    public final static String PP_TRANSACTION_APPROVE_MULTICARD_OPERATION_NOT_COMPLETED                   = "PP_TRANSACTION_APPROVE_440";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_GENERIC_ERROR                             = "PP_TRANSACTION_APPROVE_441";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_INVALID_OPERATION                         = "PP_TRANSACTION_APPROVE_442";
    
    public final static String PP_TRANSACTION_APPROVE_MULTICARD_PRODUCT_NOT_ENABLED                       = "PP_TRANSACTION_APPROVE_443";
    */

    public final static String PP_TRANSACTION_REVERSE_SUCCESS                                                    = "MESSAGE_RECEIVED_200";

    public final static String PP_TRANSACTION_REVERSE_NOT_RECOGNIZED                                             = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String PP_TRANSACTION_REVERSE_FAILURE                                                    = "TRANSACTION_NOT_REVERSED_500";

    /*
     * public final static String PP_TRANSACTION_GETQRCODE_SUCCESS =
     * "PP_TRANSACTION_GETQRCODE_200"; public final static String
     * PP_TRANSACTION_GETQRCODE_INVALID_TICKET = "PP_TRANSACTION_GETQRCODE_401";
     * public final static String PP_TRANSACTION_GETQRCODE_UNAUTHORIZED =
     * "PP_TRANSACTION_GETQRCODE_402"; public final static String
     * PP_TRANSACTION_GETQRCODE_NOT_RECOGNIZED =
     * "PP_TRANSACTION_GETQRCODE_NOT_RECOGNIZED_404"; public final static String
     * PP_TRANSACTION_GETQRCODE_FAILURE = "PP_TRANSACTION_GETQRCODE_500";
     */
    public final static String PP_TRANSACTION_GETSTATIONS_SUCCESS                                                = "STATION_RETRIEVE_200";

    public final static String PP_TRANSACTION_GETSTATIONS_FAILURE                                                = "STATION_RETRIEVE_300";

    public final static String PP_TRANSACTION_GETSTATIONS_INVALID_CODETYPE                                       = "STATION_RETRIEVE_400";

    public final static String PP_TRANSACTION_GETSTATIONS_INVALID_TICKET                                         = "USER_REQU_401";

    public final static String PP_TRANSACTION_GETSTATIONS_UNAUTHORIZED                                           = "USER_REQU_403";

    public final static String PP_TRANSACTION_GETSTATIONS_NOT_FOUND                                              = "STATION_PUMP_NOT_FOUND_404";

    public final static String PP_TRANSACTION_GETSTATIONS_BEACON_CODE_NOT_FOUND                                  = "STATION_RETRIEVE_403";

    public final static String PP_TRANSACTION_GETSTATIONS_ID_NOT_FOUND                                           = "STATION_RETRIEVE_406";

    public final static String PP_TRANSACTION_GETSTATIONS_USER_NOT_IN_RANGE                                      = "STATION_RETRIEVE_405";

    public final static String PP_TRANSACTION_GETSTATIONS_BUSINESS_NOT_ACTIVE                                    = "STATION_RETRIEVE_407";

    public final static String PP_TRANSACTION_STATION_LIST_RETRIEVE_SUCCESS                                      = "STATION_LIST_RETRIEVE_200";

    public final static String PP_TRANSACTION_STATION_LIST_RETRIEVE_FAILURE                                      = "STATION_LIST_RETRIEVE_300";

    public final static String PP_TRANSACTION_STATION_LIST_NOT_FOUND                                             = "STATION_LIST_NOT_FOUND_404";

    public final static String PP_TRANSACTION_ACTIVE_CITY_LIST_NOT_FOUND                                         = "ACTIVE_CITY_LIST_NOT_FOUND_404";

    public final static String PP_TRANSACTION_CONFIRM_SUCCESS                                                    = "POP_CONFIRM_200";

    public final static String PP_TRANSACTION_CONFIRM_FAILURE                                                    = "POP_CONFIRM_300";

    public final static String PP_TRANSACTION_CONFIRM_WRONG_ID                                                   = "POP_CONFIRM_400";

    public final static String PP_TRANSACTION_CONFIRM_INVALID_TICKET                                             = "USER_REQU_401";

    public final static String PP_TRANSACTION_CONFIRM_UNAUTHORIZED                                               = "USER_REQU_403";

    public final static String MANAGER_REQU_INVALID_REQUEST                                                      = "MANAGER_REQU_400";

    public final static String MANAGER_REQU_INVALID_TICKET                                                       = "MANAGER_REQU_401";

    public final static String MANAGER_REQU_JSON_SYNTAX_ERROR                                                    = "MANAGER_REQU_402";

    public final static String MANAGER_REQU_USER_NOT_ACTIVE                                                      = "MANAGER_REQU_403";

    public final static String MANAGER_AUTH_SUCCESS                                                              = "MANAGER_AUTH_200";

    public final static String MANAGER_AUTH_LOGIN_ERROR                                                          = "MANAGER_AUTH_301";

    public final static String MANAGER_AUTH_UNAUTHORIZED                                                         = "MANAGER_AUTH_302";

    public final static String MANAGER_AUTH_LOGIN_LOCKED                                                         = "MANAGER_AUTH_303";

    public final static String MANAGER_LOGOUT_SUCCESS                                                            = "MANAGER_LOGOUT_200";

    public final static String MANAGER_LOGOUT_INVALID_TICKET                                                     = "MANAGER_REQU_401";

    public final static String MANAGER_RESCUE_PASSWORD_SUCCESS                                                   = "MANAGER_RESCUE_PASSWORD_200";

    public final static String MANAGER_RESCUE_PASSWORD_INVALID_TICKET                                            = "MANAGER_REQU_401";

    public final static String MANAGER_RESCUE_PASSWORD_NOT_EXISTS                                                = "MANAGER_RESCUE_PASSWORD_501";

    public final static String MANAGER_RESCUE_USER_NOT_ACTIVE                                                    = "MANAGER_REQU_402";

    public final static String MANAGER_PWD_SUCCESS                                                               = "MANAGER_PWD_200";

    public final static String MANAGER_PWD_FAILURE                                                               = "MANAGER_PWD_300";

    public final static String MANAGER_PWD_NEW_PASSWORD_WRONG                                                    = "MANAGER_PWD_401";

    public final static String MANAGER_PWD_OLD_PASSWORD_WRONG                                                    = "MANAGER_PWD_402";

    public final static String MANAGER_PWD_USERNAME_PASSWORD_EQUALS                                              = "MANAGER_PWD_403";

    public final static String MANAGER_PWD_PASSWORD_SHORT                                                        = "MANAGER_PWD_404";

    public final static String MANAGER_PWD_NUMBER_LESS                                                           = "MANAGER_PWD_405";

    public final static String MANAGER_PWD_LOWER_LESS                                                            = "MANAGER_PWD_406";

    public final static String MANAGER_PWD_UPPER_LESS                                                            = "MANAGER_PWD_407";

    public final static String MANAGER_PWD_OLD_NEW_EQUALS                                                        = "MANAGER_PWD_408";

    public final static String MANAGER_PWD_OLD_ERROR                                                             = "MANAGER_PWD_409";

    public final static String MANAGER_PWD_INVALID_TICKET                                                        = "MANAGER_REQU_401";

    public final static String MANAGER_PWD_USER_NOT_ACTIVE                                                       = "MANAGER_REQU_402";

    public final static String MANAGER_STATION_SUCCESS                                                           = "MANAGER_STATION_200";

    public final static String MANAGER_STATION_FAILURE                                                           = "MANAGER_STATION_300";

    public final static String MANAGER_STATION_INVALID_STATION_ID                                                = "MANAGER_STATION_301";

    public final static String MANAGER_STATION_USER_NOT_ACTIVE                                                   = "MANAGER_REQU_403";

    public final static String MANAGER_STATION_NOT_FOUND                                                         = "MANAGER_STATION_400";

    public final static String MANAGER_RETRIEVE_TRANSACTIONS_SUCCESS                                             = "MANAGER_RETRIEVE_TRANSACTIONS_200";

    public final static String MANAGER_RETRIEVE_TRANSACTIONS_FAILURE                                             = "MANAGER_RETRIEVE_TRANSACTIONS_300";

    public final static String MANAGER_RETRIEVE_TRANSACTIONS_INVALID_PARAMETERS                                  = "MANAGER_RETRIEVE_TRANSACTIONS_301";

    public final static String MANAGER_RETRIEVE_TRANSACTIONS_INVALID_STATION_ID                                  = "MANAGER_STATION_301";

    public final static String MANAGER_RETRIEVE_VOUCHERS_SUCCESS                                                 = "MANAGER_RETRIEVE_VOUCHER_200";

    public final static String MANAGER_RETRIEVE_VOUCHERS_FAILURE                                                 = "MANAGER_RETRIEVE_VOUCHER_300";

    public final static String MANAGER_RETRIEVE_VOUCHERS_INVALID_PARAMETERS                                      = "MANAGER_RETRIEVE_VOUCHER_301";

    public final static String MANAGER_RETRIEVE_VOUCHERS_INVALID_STATION_ID                                      = "MANAGER_STATION_301";

    public final static String SCHEDULER_JOB_EXECUTION_SUCCESS                                                   = "SCHEDULER_JOB_200";

    public final static String SCHEDULER_JOB_EXECUTION_FAILURE                                                   = "SCHEDULER_JOB_300";

    public final static String SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_SUCCESS                              = "SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_200";

    public final static String SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_FAILURE                              = "SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_300";

    public final static String SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_NO_DATA_TO_PROCESS                   = "SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_301";

    public final static String SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_TOPIC_IS_NOT_EMPTY                   = "SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_302";

    public final static String SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_SUCCESS                         = "SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_200";

    public final static String SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_FAILURE                         = "SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_300";

    public final static String SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_NO_DATA_TO_PROCESS              = "SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_301";

    public final static String SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_RETRY                           = "SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_302";

    public final static String SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_ERROR_SFTP_PUT_RESULT           = "SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_303";

    public final static String SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_ERROR_PUBLISH_MESSAGE           = "SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_304";
    
    public final static String SURVEY_INVALID_TICKET                                                             = "USER_REQU_401";

    public final static String SURVEY_GET_SURVEY_SUCCESS                                                         = "SURVEY_GET_SURVEY_200";

    public final static String SURVEY_GET_SURVEY_FAILURE                                                         = "SURVEY_GET_SURVEY_300";

    public final static String SURVEY_GET_SURVEY_INVALID_CODE                                                    = "SURVEY_GET_SURVEY_301";

    public final static String SURVEY_SUBMIT_SURVEY_SUCCESS                                                      = "SURVEY_SUBMIT_SURVEY_200";

    public final static String SURVEY_SUBMIT_SURVEY_FAILURE                                                      = "SURVEY_SUBMIT_SURVEY_300";

    public final static String SURVEY_SUBMIT_SURVEY_INVALID_CODE                                                 = "SURVEY_SUBMIT_SURVEY_301";

    public final static String SURVEY_SUBMIT_SURVEY_KEY_EXISTS                                                   = "SURVEY_SUBMIT_SURVEY_302";

    public final static String SURVEY_SUBMIT_SURVEY_INVALID_KEY                                                  = "SURVEY_SUBMIT_SURVEY_303";

    public final static String ADMIN_CARD_BIN_ADD_SUCCESS                                                        = "ADMIN_CARD_BIN_ADD_200";

    public final static String ADMIN_CARD_BIN_ADD_FAILURE                                                        = "ADMIN_CARD_BIN_ADD_300";

    public final static String ADMIN_CARD_BIN_ADD_INVALID_TICKET                                                 = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_PROMOTIONS_SUCCESS                                                 = "ADMIN_RETRIEVE_PROMOTIONS_200";

    public final static String ADMIN_RETRIEVE_PROMOTIONS_FAILURE                                                 = "ADMIN_RETRIEVE_PROMOTIONS_300";

    public final static String ADMIN_RETRIEVE_PROMOTIONS_INVALID_TICKET                                          = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_PROMOTION_SUCCESS                                                    = "ADMIN_PROMOTION_UPD_200";

    public final static String ADMIN_UPDATE_PROMOTION__INVALID_TICKET                                            = "USER_REQU_401";

    public final static String ADMIN_UPDATE_PROMOTION_CODE_NOT_FOUND                                             = "ADMIN_PROMOTION_UPD_400";

    public final static String ADMIN_MAPPING_ERROR_CREATE__INVALID_TICKET                                        = "ADMIN_REQU_401";

    public final static String ADMIN_MAPPING_ERROR_CREATE__FAILURE                                               = "ADMIN_MAPPING_ERROR_CREATE_300";

    public final static String ADMIN_MAPPING_ERROR_CREATE__SUCCESS                                               = "ADMIN_MAPPING_ERROR_CREATE_200";

    public final static String ADMIN_MAPPING_ERROR_CREATE__NOT_EXISTS                                            = "ADMIN_MAPPING_ERROR_CREATE_302";

    public final static String ADMIN_MAPPING_ERROR_CREATE__INVALID_CODE                                          = "ADMIN_MAPPING_ERROR_CREATE_301";

    public final static String ADMIN_MAPPING_ERROR_EDIT__INVALID_TICKET                                          = "ADMIN_REQU_401";

    public final static String ADMIN_MAPPING_ERROR_EDIT__FAILURE                                                 = "ADMIN_MAPPING_ERROR_EDIT_300";

    public final static String ADMIN_MAPPING_ERROR_EDIT__SUCCESS                                                 = "ADMIN_MAPPING_ERROR_EDIT_200";

    public final static String ADMIN_MAPPING_ERROR_EDIT__NOT_EXISTS                                              = "ADMIN_MAPPING_ERROR_EDIT_302";

    public final static String ADMIN_MAPPING_ERROR_EDIT__INVALID_ERROR_CODE                                      = "ADMIN_MAPPING_ERROR_EDIT_301";

    public final static String ADMIN_MAPPING_ERROR_EDIT__INVALID_STATUS_CODE                                     = "ADMIN_MAPPING_ERROR_EDIT_301";

    public final static String ADMIN_MAPPING_ERROR_EDIT__ERROR_CODE_NOT_FOUND                                    = "ADMIN_MAPPING_ERROR_EDIT_400";

    public final static String ADMIN_MAPPING_ERROR_GET__INVALID_TICKET                                           = "ADMIN_REQU_401";

    public final static String ADMIN_MAPPING_ERROR_GET__FAILURE                                                  = "ADMIN_MAPPING_ERROR_GET_300";

    public final static String ADMIN_MAPPING_ERROR_GET__SUCCESS                                                  = "ADMIN_MAPPING_ERROR_GET_200";

    public final static String ADMIN_MAPPING_ERROR_GET__NOT_EXISTS                                               = "ADMIN_MAPPING_ERROR_GET_302";

    public final static String ADMIN_MAPPING_ERROR_GET__INVALID_CODE                                             = "ADMIN_MAPPING_ERROR_GET_301";

    public final static String ADMIN_MAPPING_ERROR_REMOVE__INVALID_TICKET                                        = "ADMIN_REQU_401";

    public final static String ADMIN_MAPPING_ERROR_REMOVE__FAILURE                                               = "ADMIN_MAPPING_ERROR_REMOVE_300";

    public final static String ADMIN_MAPPING_ERROR_REMOVE__SUCCESS                                               = "ADMIN_MAPPING_ERROR_REMOVE_200";

    public final static String ADMIN_MAPPING_ERROR_REMOVE__NOT_EXISTS                                            = "ADMIN_MAPPING_ERROR_REMOVE_302";

    public final static String ADMIN_MAPPING_ERROR_REMOVE__INVALID_ERROR_CODE                                    = "ADMIN_MAPPING_ERROR_REMOVE_301";

    public final static String TOKEN_INVALID_500                                                                 = "TOKEN_INVALID_500";

    public final static String MESSAGE_RECEIVED_200                                                              = "MESSAGE_RECEIVED_200";

    public final static String ADMIN_USER_CATEGORY_CREATE                                                        = "ADMIN_USERCATEGORY_CREATE_200";

    public final static String ADMIN_USER_CATEGORY_CREATE_FAILURE                                                = "ADMIN_USERCATEGORY_CREATE_300";

    public final static String ADMIN_USER_CATEGORY_CREATE_EXISTS                                                 = "ADMIN_USERCATEGORY_CREATE_400";

    public final static String ADMIN_USER_CATEGORY_INVALID_TICKET                                                = "ADMIN_REQU_401";

    public final static String ADMIN_USER_CATEGORY_RETRIEVE                                                      = "ADMIN_USERCATEGORY_RETRIEVE_200";

    public final static String ADMIN_USER_CATEGORY_RETRIEVE_FAILURE                                              = "ADMIN_USERCATEGORY_RETRIEVE_300";

    public final static String ADMIN_USER_CATEGORY_RETRIEVE_EXISTS                                               = "ADMIN_USERCATEGORY_RETRIEVE_400";

    public final static String ADMIN_USER_TYPE_CREATE                                                            = "ADMIN_USERTYPE_CREATE_200";

    public final static String ADMIN_USER_TYPE_CREATE_FAILURE                                                    = "ADMIN_USERTYPE_CREATE_300";

    public final static String ADMIN_USER_TYPE_CREATE_EXISTS                                                     = "ADMIN_USERTYPE_CREATE_400";

    public final static String ADMIN_USER_TYPE_RETRIEVE                                                          = "ADMIN_USERTYPE_RETRIEVE_200";

    public final static String ADMIN_USER_TYPE_RETRIEVE_FAILURE                                                  = "ADMIN_USERTYPE_RETRIEVE_300";

    public final static String ADMIN_USER_TYPE_RETRIEVE_EXISTS                                                   = "ADMIN_USERTYPE_RETRIEVE_400";

    public final static String ADMIN_USER_TYPE_INVALID_TICKET                                                    = "ADMIN_REQU_401";

    public final static String ADMIN_USERTYPE_CATEGORY_UPDATE                                                    = "ADMIN_USERTYPE_CATEGORY_UPDATE_200";

    public final static String ADMIN_USERTYPE_CATEGORY_UPDATE_FAILURE                                            = "ADMIN_USERTYPE_CATEGORY_UPDATE_300";

    public final static String ADMIN_USERTYPE_CATEGORY_UPDATE_EXISTS                                             = "ADMIN_USERTYPE_CATEGORY_UPDATE_400";

    public final static String ADMIN_USERTYPE_CATEGORY_INVALID_TICKET                                            = "ADMIN_REQU_401";

    public final static String TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS                             = "TRANSACTION_PRE_AUTH_CONSUME_VOUCHER_200";

    public final static String TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE                             = "TRANSACTION_PRE_AUTH_CONSUME_VOUCHER_300";

    public final static String TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_NOT_FOUND                           = "TRANSACTION_PRE_AUTH_CONSUME_VOUCHER_400";

    public final static String TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_PV_NOT_ENABLED                      = "TRANSACTION_PRE_AUTH_CONSUME_VOUCHER_401";

    public final static String TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS                      = "TRANSACTION_CANCEL_PRE_AUTH_CONSUME_VOUCHER_200";

    public final static String TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE                      = "TRANSACTION_CANCEL_PRE_AUTH_CONSUME_VOUCHER_300";

    public final static String TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_SYSTEM_ERRROR                = "TRANSACTION_CANCEL_PRE_AUTH_CONSUME_VOUCHER_301";

    public final static String TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_NOT_FOUND                    = "TRANSACTION_CANCEL_PRE_AUTH_CONSUME_VOUCHER_400";

    public final static String TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_SUCCESS                                      = "TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_200";

    public final static String TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_FAILURE                                      = "TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_300";

    public final static String TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_SYSTEM_ERROR                                 = "TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_301";

    public final static String TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_NOT_FOUND                                    = "TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_400";

    public final static String PREFIX_RETRIEVE_SUCCESS                                                           = "PREFIX_RETRIEVE_200";

    public final static String PREFIX_RETRIEVE_FAILURE                                                           = "PREFIX_RETRIEVE_300";

    public final static String PREFIX_RETRIEVE_INVALID_TICKET                                                    = "USER_REQU_401";

    public final static String PREFIX_CREATE_SUCCESS                                                             = "PREFIX_CREATE_200";

    public final static String PREFIX_CREATE_FAILURE                                                             = "PREFIX_CREATE_300";

    public final static String PREFIX_CREATE_INVALID_TICKET                                                      = "USER_REQU_401";

    public final static String PREFIX_REMOVE_SUCCESS                                                             = "PREFIX_REMOVE_200";

    public final static String PREFIX_REMOVE_FAILURE                                                             = "PREFIX_REMOVE_300";

    public final static String PREFIX_REMOVE_INVALID_TICKET                                                      = "USER_REQU_401";

    public final static String MOBILE_PHONE_UPDATE_INVALID_TICKET                                                = "USER_REQU_401";

    public final static String MOBILE_PHONE_UPDATE_UNAUTHORIZED                                                  = "USER_REQU_403";

    public final static String MOBILE_PHONE_UPDATE_SUCCESS                                                       = "MOBILE_PHONE_UPDATE_200";

    public final static String MOBILE_PHONE_UPDATE_FAILURE                                                       = "MOBILE_PHONE_UPDATE_300";

    public final static String MOBILE_PHONE_UPDATE_CHECK_FAILURE                                                 = "MOBILE_PHONE_UPDATE_301";

    public final static String MOBILE_PHONE_UPDATE_NUMBER_EXISTS                                                 = "MOBILE_PHONE_UPDATE_302";

    public final static String MOBILE_PHONE_UPDATE_SENDING_FAILURE                                               = "MOBILE_PHONE_UPDATE_303";

    public final static String MOBILE_PHONE_UPDATE_INVALID_PREFIX                                                = "MOBILE_PHONE_UPDATE_304";

    public final static String MOBILE_PHONE_UPDATE_USER_NOT_ENABLE                                               = "MOBILE_PHONE_UPDATE_305";

    public final static String MOBILE_PHONE_UPDATE_INVALID_USER_SOURCE                                           = "MOBILE_PHONE_UPDATE_306";

    public final static String MOBILE_PHONE_CANCEL_SUCCESS                                                       = "MOBILE_PHONE_CANCEL_200";

    public final static String MOBILE_PHONE_CANCEL_FAILURE                                                       = "MOBILE_PHONE_CANCEL_300";

    public final static String MOBILE_PHONE_CANCEL_CHECK_FAILURE                                                 = "MOBILE_PHONE_CANCEL_301";

    public final static String MOBILE_PHONE_SEND_VALIDATION_SUCCESS                                              = "MOBILE_PHONE_SEND_VALIDATION_200";

    public final static String MOBILE_PHONE_SEND_VALIDATION_FAILURE                                              = "MOBILE_PHONE_SEND_VALIDATION_300";

    public final static String VOUCHER_CREATE_SUCCESS                                                            = "CREATE_VOUCHER_200";

    public final static String VOUCHER_CREATE_FAILURE                                                            = "CREATE_VOUCHER_300";

    public final static String VOUCHER_CREATE_INVALID_TICKET                                                     = "USER_REQU_401";

    public final static String VOUCHER_CREATE_INVALID_USER_STATUS                                                = "CREATE_VOUCHER_301";

    public final static String VOUCHER_CREATE_INVALID_USER_TYPE                                                  = "CREATE_VOUCHER_302";

    public final static String VOUCHER_CREATE_PAYMENT_DATA_WRONG                                                 = "CREATE_VOUCHER_303";

    public final static String VOUCHER_CREATE_PAYMENT_VOUCHER_TYPE_REQUIRED                                      = "CREATE_VOUCHER_304";

    public final static String VOUCHER_CREATE_ERROR                                                              = "CREATE_VOUCHER_305";

    public final static String VOUCHER_CREATE_CAP_FAILURE                                                        = "CREATE_VOUCHER_306";

    public final static String VOUCHER_CREATE_PIN_WRONG                                                          = "CREATE_VOUCHER_307";

    public final static String VOUCHER_CREATE_PAYMENT_KO                                                         = "CREATE_VOUCHER_308";

    public final static String VOUCHER_APPLE_PAY_CREATE_SUCCESS                                                  = "CREATE_APPLE_PAY_VOUCHER_200";

    public final static String VOUCHER_APPLE_PAY_CREATE_FAILURE                                                  = "CREATE_APPLE_PAY_VOUCHER_300";

    public final static String VOUCHER_APPLE_PAY_CREATE_INVALID_TICKET                                           = "USER_REQU_401";

    public final static String VOUCHER_APPLE_PAY_CREATE_INVALID_USER_STATUS                                      = "CREATE_APPLE_PAY_VOUCHER_301";

    public final static String VOUCHER_APPLE_PAY_CREATE_INVALID_USER_TYPE                                        = "CREATE_APPLE_PAY_VOUCHER_302";

    public final static String VOUCHER_APPLE_PAY_CREATE_PAYMENT_DATA_WRONG                                       = "CREATE_APPLE_PAY_VOUCHER_303";

    public final static String VOUCHER_APPLE_PAY_CREATE_PAYMENT_VOUCHER_TYPE_REQUIRED                            = "CREATE_APPLE_PAY_VOUCHER_304";

    public final static String VOUCHER_APPLE_PAY_CREATE_ERROR                                                    = "CREATE_APPLE_PAY_VOUCHER_305";

    public final static String VOUCHER_APPLE_PAY_CREATE_CAP_FAILURE                                              = "CREATE_APPLE_PAY_VOUCHER_306";

    public final static String VOUCHER_APPLE_PAY_CREATE_PIN_WRONG                                                = "CREATE_APPLE_PAY_VOUCHER_307";

    public final static String VOUCHER_APPLE_PAY_CREATE_PAYMENT_KO                                               = "CREATE_APPLE_PAY_VOUCHER_308";

    public final static String VOUCHER_UPDATER_CHECK_SUCCESS                                                     = "VOUCHER_UPDATER_CHECK_200";

    public final static String VOUCHER_UPDATER_CHECK_VOUCHER_NOT_FOUND                                           = "VOUCHER_UPDATER_CHECK_301";

    public final static String VOUCHER_UPDATER_CHECK_SYSTEM_ERROR                                                = "VOUCHER_UPDATER_CHECK_302";

    public final static String VOUCHER_UPDATER_CONSUME_SUCCESS                                                   = "VOUCHER_UPDATER_CONSUME_200";

    public final static String VOUCHER_UPDATER_CONSUME_PRE_AUTH_SUCCESS                                          = "VOUCHER_UPDATER_CONSUME_PRE_AUTH_200";

    public final static String VOUCHER_UPDATER_CONSUME_PRE_AUTH_ID_NOT_FOUND                                     = "VOUCHER_UPDATER_CONSUME_301";

    public final static String VOUCHER_UPDATER_CONSUME_SYSTEM_ERROR                                              = "VOUCHER_UPDATER_CONSUME_302";

    public final static String VOUCHER_UPDATER_CONSUME_FAILURE                                                   = "VOUCHER_UPDATER_CONSUME_303";

    public final static String VOUCHER_UPDATER_CANCEL_PRE_AUTH_SUCCESS                                           = "VOUCHER_UPDATER_CANCEL_PRE_AUTH_200";

    public final static String VOUCHER_UPDATER_CANCEL_PRE_AUTH_PRE_AUTH_ID_NOT_FOUND                             = "VOUCHER_UPDATER_CANCEL_PRE_AUTH_301";

    public final static String VOUCHER_UPDATER_CANCEL_PRE_AUTH_SYSTEM_ERROR                                      = "VOUCHER_UPDATER_CANCEL_PRE_AUTH_302";

    public final static String VOUCHER_UPDATER_CANCEL_PRE_AUTH_FAILURE                                           = "VOUCHER_UPDATER_CANCEL_PRE_AUTH_303";

    public final static String ADMIN_RETRIEVE_VOUCHER_TRANSACTIONS_SUCCESS                                       = "ADMIN_RETRIEVE_VOUCHER_TRANSACTIONS_200";

    public final static String ADMIN_RETRIEVE_VOUCHER_TRANSACTIONS_FAILURE                                       = "ADMIN_RETRIEVE_VOUCHER_TRANSACTIONS_300";

    public final static String ADMIN_RETRIEVE_VOUCHER_TRANSACTIONS_INVALID_TICKET                                = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_VOUCHER_TRANSACTIONS_LIST_SUCCESS                                  = "ADMIN_RETRIEVE_VOUCHER_TRANSACTIONS_LIST_200";

    public final static String USER_CHECK_AVAILABILITY_AMOUNT_SUCCESS                                            = "CHECK_AVAILABILITY_AMOUNT_200";

    public final static String USER_CHECK_AVAILABILITY_AMOUNT_FAILURE                                            = "CHECK_AVAILABILITY_AMOUNT_300";

    public final static String USER_CHECK_AVAILABILITY_AMOUNT_INVALID_TICKET                                     = "USER_REQU_401";

    public final static String USER_CHECK_AVAILABILITY_AMOUNT_UNAUTHORIZED                                       = "CHECK_AVAILABILITY_AMOUNT_400";

    public final static String RETRIEVE_VOUCHER_TRANSACTION_DETAIL_SUCCESS                                       = "RETRIEVE_VOUCHER_TRANSACTION_200";

    public final static String RETRIEVE_VOUCHER_TRANSACTION_DETAIL_FAILURE                                       = "RETRIEVE_VOUCHER_TRANSACTION_300";

    public final static String RETRIEVE_VOUCHER_TRANSACTION_DETAIL_NOT_RECOGNIZED                                = "RETRIEVE_VOUCHER_TRANSACTION_400";

    public final static String RETRIEVE_VOUCHER_TRANSACTION_DETAIL_INVALID_TICKET                                = "USER_REQU_401";

    public final static String RETRIEVE_VOUCHER_TRANSACTION_DETAIL_UNAUTHORIZED                                  = "USER_REQU_403";

    public final static String USER_UPDATE_TERMS_OF_SERVICE_SUCCESS                                              = "USER_UPDATE_TERMS_OF_SERVICE_200";

    public final static String USER_UPDATE_TERMS_OF_SERVICE_FAILURE                                              = "USER_UPDATE_TERMS_OF_SERVICE_300";

    public final static String USER_UPDATE_TERMS_OF_SERVICE_CHECK_FAILURE                                        = "USER_UPDATE_TERMS_OF_SERVICE_301";

    public final static String USER_UPDATE_TERMS_OF_SERVICE_INVALID_TICKET                                       = "USER_REQU_401";

    public final static String ADMIN_UPDATE_TERMS_OF_SERVICE_SUCCESS                                             = "ADMIN_UPDATE_TERMS_OF_SERVICE_200";

    public final static String ADMIN_UPDATE_TERMS_OF_SERVICE_FAILURE                                             = "ADMIN_UPDATE_TERMS_OF_SERVICE_300";

    public final static String ADMIN_UPDATE_TERMS_OF_SERVICE_CHECK_FAILURE                                       = "ADMIN_UPDATE_TERMS_OF_SERVICE_301";

    public final static String ADMIN_UPDATE_TERMS_OF_SERVICE_INVALID_TICKET                                      = "ADMIN_REQU_401";

    public final static String ADMIN_BLOCK_PERIOD_CREATE_SUCCESS                                                 = "ADMIN_BLOCK_PERIOD_CREATE_200";

    public final static String ADMIN_BLOCK_PERIOD_CREATE_FAILURE                                                 = "ADMIN_BLOCK_PERIOD_CREATE_300";

    public final static String ADMIN_BLOCK_PERIOD_CREATE_CHECK_FAILURE                                           = "ADMIN_BLOCK_PERIOD_CREATE_301";

    public final static String ADMIN_BLOCK_PERIOD_CREATE_INVALID_TICKET                                          = "ADMIN_REQU_401";

    public final static String ADMIN_BLOCK_PERIOD_CREATE_EXIST_CODE                                              = "ADMIN_BLOCK_PERIOD_CREATE_302";

    public final static String ADMIN_BLOCK_PERIOD_UPDATE_SUCCESS                                                 = "ADMIN_BLOCK_PERIOD_UPDATE_200";

    public final static String ADMIN_BLOCK_PERIOD_UPDATE_FAILURE                                                 = "ADMIN_BLOCK_PERIOD_UPDATE_300";

    public final static String ADMIN_BLOCK_PERIOD_UPDATE_CHECK_FAILURE                                           = "ADMIN_BLOCK_PERIOD_UPDATE_301";

    public final static String ADMIN_BLOCK_PERIOD_UPDATE_INVALID_TICKET                                          = "ADMIN_REQU_401";

    public final static String ADMIN_BLOCK_PERIOD_UPDATE_NOT_EXIST_CODE                                          = "ADMIN_BLOCK_PERIOD_UPDATE_302";

    public final static String ADMIN_BLOCK_PERIOD_DELETE_SUCCESS                                                 = "ADMIN_BLOCK_PERIOD_DELETE_200";

    public final static String ADMIN_BLOCK_PERIOD_DELETE_FAILURE                                                 = "ADMIN_BLOCK_PERIOD_DELETE_300";

    public final static String ADMIN_BLOCK_PERIOD_DELETE_CHECK_FAILURE                                           = "ADMIN_BLOCK_PERIOD_DELETE_301";

    public final static String ADMIN_BLOCK_PERIOD_DELETE_INVALID_TICKET                                          = "ADMIN_REQU_401";

    public final static String ADMIN_BLOCK_PERIOD_DELETE_NOT_EXIST_CODE                                          = "ADMIN_BLOCK_PERIOD_DELETE_302";

    public final static String ADMIN_BLOCK_PERIOD_RETRIEVE_SUCCESS                                               = "ADMIN_BLOCK_PERIOD_RETRIEVE_200";

    public final static String ADMIN_BLOCK_PERIOD_RETRIEVE_FAILURE                                               = "ADMIN_BLOCK_PERIOD_RETRIEVE_300";

    public final static String ADMIN_BLOCK_PERIOD_RETRIEVE_CHECK_FAILURE                                         = "ADMIN_BLOCK_PERIOD_RETRIEVE_301";

    public final static String ADMIN_BLOCK_PERIOD_RETRIEVE_INVALID_TICKET                                        = "ADMIN_REQU_401";

    public final static String ADMIN_BLOCK_PERIOD_RETRIEVE_NOT_EXIST_CODE                                        = "ADMIN_BLOCK_PERIOD_RETRIEVE_302";

    public final static String ADMIN_DOCUMENT_CREATE_SUCCESS                                                     = "ADMIN_DOCUMENT_CREATE_200";

    public final static String ADMIN_DOCUMENT_CREATE_FAILURE                                                     = "ADMIN_DOCUMENT_CREATE_300";

    public final static String ADMIN_DOCUMENT_CREATE_CHECK_FAILURE                                               = "ADMIN_DOCUMENT_CREATE_301";

    public final static String ADMIN_DOCUMENT_CREATE_INVALID_TICKET                                              = "ADMIN_REQU_401";

    public final static String ADMIN_DOCUMENT_CREATE_NOT_EXIST_CODE                                              = "ADMIN_DOCUMENT_CREATE_302";

    public final static String ADMIN_DOCUMENT_UPDATE_SUCCESS                                                     = "ADMIN_DOCUMENT_UPDATE_200";

    public final static String ADMIN_DOCUMENT_UPDATE_FAILURE                                                     = "ADMIN_DOCUMENT_UPDATE_300";

    public final static String ADMIN_DOCUMENT_UPDATE_CHECK_FAILURE                                               = "ADMIN_DOCUMENT_UPDATE_301";

    public final static String ADMIN_DOCUMENT_UPDATE_INVALID_TICKET                                              = "ADMIN_REQU_401";

    public final static String ADMIN_DOCUMENT_UPDATE_NOT_EXIST_CODE                                              = "ADMIN_DOCUMENT_UPDATE_302";

    public final static String ADMIN_DOCUMENT_DELETE_SUCCESS                                                     = "ADMIN_DOCUMENT_DELETE_200";

    public final static String ADMIN_DOCUMENT_DELETE_FAILURE                                                     = "ADMIN_DOCUMENT_DELETE_300";

    public final static String ADMIN_DOCUMENT_DELETE_CHECK_FAILURE                                               = "ADMIN_DOCUMENT_DELETE_301";

    public final static String ADMIN_DOCUMENT_DELETE_INVALID_TICKET                                              = "ADMIN_REQU_401";

    public final static String ADMIN_DOCUMENT_DELETE_NOT_EXIST_CODE                                              = "ADMIN_DOCUMENT_DELETE_302";

    public final static String ADMIN_DOCUMENT_RETRIEVE_SUCCESS                                                   = "ADMIN_DOCUMENT_RETRIEVE_200";

    public final static String ADMIN_DOCUMENT_RETRIEVE_FAILURE                                                   = "ADMIN_DOCUMENT_RETRIEVE_300";

    public final static String ADMIN_DOCUMENT_RETRIEVE_CHECK_FAILURE                                             = "ADMIN_DOCUMENT_RETRIEVE_301";

    public final static String ADMIN_DOCUMENT_RETRIEVE_INVALID_TICKET                                            = "ADMIN_REQU_401";

    public final static String ADMIN_DOCUMENT_RETRIEVE_NOT_EXIST_CODE                                            = "ADMIN_DOCUMENT_RETRIEVE_302";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_CREATE_SUCCESS                                           = "ADMIN_DOCUMENT_ATTRIBUTE_CREATE_200";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_CREATE_FAILURE                                           = "ADMIN_DOCUMENT_ATTRIBUTE_CREATE_300";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_CREATE_CHECK_FAILURE                                     = "ADMIN_DOCUMENT_ATTRIBUTE_CREATE_301";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_CREATE_INVALID_TICKET                                    = "ADMIN_REQU_401";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_CREATE_NOT_EXIST_CODE                                    = "ADMIN_DOCUMENT_ATTRIBUTE_CREATE_302";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_DELETE_SUCCESS                                           = "ADMIN_DOCUMENT_ATTRIBUTE_DELETE_200";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_DELETE_FAILURE                                           = "ADMIN_DOCUMENT_ATTRIBUTE_DELETE_300";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_DELETE_CHECK_FAILURE                                     = "ADMIN_DOCUMENT_ATTRIBUTE_DELETE_301";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_DELETE_INVALID_TICKET                                    = "ADMIN_REQU_401";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_DELETE_NOT_EXIST_CODE                                    = "ADMIN_DOCUMENT_ATTRIBUTE_DELETE_302";

    public final static String ADMIN_GENERATE_PV_SUCCESS                                                         = "ADMIN_GENERATE_PV_200";

    public final static String ADMIN_GENERATE_PV_FAILURE                                                         = "ADMIN_GENERATE_PV_300";

    public final static String ADMIN_GENERATE_PV_CHECK_FAILURE                                                   = "ADMIN_GENERATE_PV_301";

    public final static String ADMIN_GENERATE_PV_INVALID_TICKET                                                  = "ADMIN_REQU_401";

    public final static String ADMIN_GENERATE_SINGLE_PV_SUCCESS                                                  = "ADMIN_GENERATE_SINGLE_PV_200";

    public final static String ADMIN_GENERATE_SINGLE_PV_FAILURE                                                  = "ADMIN_GENERATE_SINGLE_PV_300";

    public final static String ADMIN_GENERATE_SINGLE_PV_NOT_FOUND                                                = "ADMIN_GENERATE_SINGLE_PV_301";

    public final static String ADMIN_GENERATE_SINGLE_PV_PARAMETER_MISSING                                        = "ADMIN_GENERATE_SINGLE_PV_302";

    public final static String ADMIN_UPDATE_PASSWORD_SUCCESS                                                     = "ADMIN_UPDATE_PASSWORD_200";

    public final static String ADMIN_UPDATE_PASSWORD_FAILURE                                                     = "ADMIN_UPDATE_PASSWORD_300";

    public final static String ADMIN_UPDATE_PASSWORD_CHECK_FAILURE                                               = "ADMIN_UPDATE_PASSWORD_301";

    public final static String ADMIN_UPDATE_PASSWORD_INVALID_TICKET                                              = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_PASSWORD_USERNAME_PASSWORD_EQUALS                                    = "ADMIN_UPDATE_PASSWORD_403";

    public final static String ADMIN_UPDATE_PASSWORD_OLD_PASSWORD_WRONG                                          = "ADMIN_UPDATE_PASSWORD_402";

    public final static String ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_SUCCESS                                      = "ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_200";

    public final static String ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_FAILURE                                      = "ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_300";

    public final static String ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_CHECK_FAILURE                                = "ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_301";

    public final static String ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_INVALID_TICKET                               = "ADMIN_REQU_401";

    public final static String ADMIN_GENERATE_MAILING_LIST_SUCCESS                                               = "ADMIN_GENERATE_MAILING_LIST_200";

    public final static String ADMIN_GENERATE_MAILING_LIST_FAILURE                                               = "ADMIN_GENERATE_MAILING_LIST_300";

    public final static String ADMIN_GENERATE_MAILING_LIST_INVALID_TICKET                                        = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_MAILING_LIST_SUCCESS                                                 = "ADMIN_UPDATE_MAILING_LIST_200";

    public final static String ADMIN_UPDATE_MAILING_LIST_FAILURE                                                 = "ADMIN_UPDATE_MAILING_LIST_300";

    public final static String ADMIN_UPDATE_MAILING_LIST_INVALID_TICKET                                          = "ADMIN_REQU_401";

    public final static String ADMIN_ASSIGN_VOUCHER_SUCCESS                                                      = "ADMIN_ASSIGN_VOUCHER_200";

    public final static String ADMIN_ASSIGN_VOUCHER_FAILURE                                                      = "ADMIN_ASSIGN_VOUCHER_300";

    public final static String ADMIN_ASSIGN_VOUCHER_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_REMOVE_TRANSACTION_EVENT_SUCCESS                                            = "ADMIN_REMOVE_TRANSACTION_EVENT_200";

    public final static String ADMIN_REMOVE_TRANSACTION_EVENT_FAILURE                                            = "ADMIN_REMOVE_TRANSACTION_EVENT_300";

    public final static String ADMIN_REMOVE_TRANSACTION_EVENT_INVALID_TICKET                                     = "ADMIN_REQU_401";

    public final static String ADMIN_INSERT_USER_IN_PROMO_ONHOLD_SUCCESS                                         = "ADMIN_INSERT_USER_IN_PROMO_ONHOLD_200";

    public final static String ADMIN_INSERT_USER_IN_PROMO_ONHOLD_FAILURE                                         = "ADMIN_INSERT_USER_IN_PROMO_ONHOLD_300";

    public final static String ADMIN_INSERT_USER_IN_PROMO_ONHOLD_INVALID_TICKET                                  = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_VOUCHER_SUCCESS                                                      = "ADMIN_UPDATE_VOUCHER_200";

    public final static String ADMIN_UPDATE_VOUCHER_FAILURE                                                      = "ADMIN_UPDATE_VOUCHER_300";

    public final static String ADMIN_UPDATE_VOUCHER_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_DELETE_VOUCHER_SUCCESS                                                      = "ADMIN_DELETE_VOUCHER_200";

    public final static String ADMIN_DELETE_VOUCHER_FAILURE                                                      = "ADMIN_DELETE_VOUCHER_300";

    public final static String ADMIN_DELETE_VOUCHER_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_CREATE_PROVINCE_SUCCESS                                                     = "ADMIN_CREATE_PROVINCE_200";

    public final static String ADMIN_CREATE_PROVINCE_FAILURE                                                     = "ADMIN_CREATE_PROVINCE_300";

    public final static String ADMIN_CREATE_PROVINCE_INVALID_TICKET                                              = "ADMIN_REQU_401";

    public final static String ADMIN_CREATE_PROVINCE_EXISTS                                                      = "ADMIN_CREATE_PROVINCE_301";

    public final static String ADMIN_REMOVE_MOBILE_PHONE_SUCCESS                                                 = "ADMIN_REMOVE_MOBILE_PHONE_200";

    public final static String ADMIN_REMOVE_MOBILE_PHONE_FAILURE                                                 = "ADMIN_REMOVE_MOBILE_PHONE_300";

    public final static String ADMIN_REMOVE_MOBILE_PHONE_INVALID_TICKET                                          = "ADMIN_REQU_401";

    public final static String DWH_UPDATE_PASSWORD_SUCCESS                                                       = "DWH_UPDATE_PASSWORD_200";

    public final static String DWH_UPDATE_PASSWORD_FAILURE                                                       = "DWH_UPDATE_PASSWORD_300";

    public final static String DWH_UPDATE_PASSWORD_USER_NOT_FOUND                                                = "DWH_UPDATE_PASSWORD_400";

    public final static String DWH_UPDATE_PASSWORD_EMAIL_NOT_FOUND                                               = "DWH_UPDATE_PASSWORD_401";

    public final static String DWH_UPDATE_PASSWORD_USER_UNAUTHORIZED                                             = "USER_REQU_403";

    public final static String DWH_UPDATE_PASSWORD_USERNAME_PASSWORD_EQUALS                                      = "DWH_UPDATE_PASSWORD_301";

    public final static String DWH_UPDATE_PASSWORD_OLD_PASSWORD_WRONG                                            = "DWH_UPDATE_PASSWORD_302";

    public final static String DWH_UPDATE_PASSWORD_NEW_PASSWORD_WRONG                                            = "DWH_UPDATE_PASSWORD_303";

    public final static String DWH_UPDATE_PASSWORD_OLD_AND_NEW_EQUALS                                            = "DWH_UPDATE_PASSWORD_304";

    public final static String DWH_UPDATE_PASSWORD_HISTORY_NEW_EQUALS                                            = "DWH_UPDATE_PASSWORD_305";

    public final static String DWH_UPDATE_PASSWORD_PASSWORD_SHORT                                                = "DWH_UPDATE_PASSWORD_306";

    public final static String DWH_UPDATE_PASSWORD_UPPER_LESS                                                    = "DWH_UPDATE_PASSWORD_307";

    public final static String DWH_UPDATE_PASSWORD_LOWER_LESS                                                    = "DWH_UPDATE_PASSWORD_308";

    public final static String DWH_UPDATE_PASSWORD_NUMBER_LESS                                                   = "DWH_UPDATE_PASSWORD_309";

    public final static String DWH_ACTIVATE_PROMOTION_SUCCESS                                                    = "DWH_ACTIVATE_PROMOTION_200";
    
    public final static String DWH_ACTIVATE_PROMOTION_USER_NOT_FOUND                                             = "DWH_ACTIVATE_PROMOTION_USER_NOT_FOUND_400";
    
    public final static String DWH_ACTIVATE_PROMOTION_CARD_NOT_FOUND                                             = "DWH_ACTIVATE_PROMOTION_CARD_NOT_FOUND_301";
    
    public final static String DWH_ACTIVATE_PROMOTION_ALREADY_EXISTS                                             = "DWH_ACTIVATE_PROMOTION_ALREADY_EXISTS_409";
    
    public final static String DWH_ACTIVATE_PROMOTION_SYSTEM_ERROR                                               = "DWH_ACTIVATE_PROMOTION_SYSTEM_ERROR_500";
    
    public final static String USER_V2_UPDATE_USERS_DATA_SUCCESS                                                 = "USER_V2_UPDATE_USERS_DATA_200";

    public final static String USER_V2_UPDATE_USERS_DATA_FAILURE                                                 = "USER_V2_UPDATE_USERS_DATA_300";

    public final static String USER_V2_UPDATE_USERS_DATA_INVALID_TICKET                                          = "USER_REQU_401";

    public final static String USER_V2_UPDATE_USERS_DATA_UNAUTHORIZED                                            = "USER_REQU_403";

    public final static String EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_SUCCESS                                  = "EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_200";

    public final static String EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_FAILURE                                  = "EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_300";

    public final static String EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_NOT_FOUND                                = "EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_401";

    public final static String EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_INVALID_TICKET                           = "USER_REQU_401";

    public final static String EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_UNAUTHORIZED                             = "USER_REQU_403";

    public final static String EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_SUCCESS                                 = "EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_200";

    public final static String EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_FAILURE                                 = "EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_300";

    public final static String EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_NOT_FOUND                               = "EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_301";

    public final static String EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_NOT_RECOGNIZED                          = "EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_400";

    public final static String EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_INVALID_TICKET                          = "USER_REQU_401";

    public final static String EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_UNAUTHORIZED                            = "USER_REQU_403";

    public final static String CRM_NOTIFY_EVENT_SUCCESS                                                          = "CRM_NOTIFY_EVENT_200";

    public final static String CRM_NOTIFY_EVENT_FAILURE                                                          = "CRM_NOTIFY_EVENT_300";

    public final static String CRM_NOTIFY_EVENT_NOT_FOUND                                                        = "CRM_NOTIFY_EVENT_401";

    public final static String CRM_NOTIFY_EVENT_ENDPOINT_NOT_FOUND                                               = "CRM_NOTIFY_EVENT_402";

    public final static String CRM_NOTIFY_EVENT_OFFERS_NOT_FOUND                                                 = "CRM_NOTIFY_EVENT_402";

    public final static String CRM_NOTIFY_EVENT_INVALID_TICKET                                                   = "USER_REQU_401";

    public final static String CRM_NOTIFY_EVENT_UNAUTHORIZED                                                     = "USER_REQU_403";

    public final static String CRM_PROCESS_OUTBOUND_INTERFACE_SUCCESS                                            = "CRM_PROCESS_OUTBOUND_INTERFACE_200";

    public final static String CRM_PROCESS_OUTBOUND_INTERFACE_FAILURE                                            = "CRM_PROCESS_OUTBOUND_INTERFACE_400";

    public final static String CRM_PROCESS_OUTBOUND_INTERFACE_NOT_FOUND                                          = "CRM_PROCESS_OUTBOUND_INTERFACE_401";

    public final static String CRM_PROCESS_OUTBOUND_INTERFACE_INVALID_TICKET                                     = "USER_REQU_401";

    public final static String CRM_PROCESS_OUTBOUND_INTERFACE_UNAUTHORIZED                                       = "USER_REQU_403";

    public final static String USER_V2_NOTIFICATION_VIRTUALIZATION_SUCCESS                                       = "USER_V2_NOTIFICATION_VIRTUALIZATION_200";

    public final static String USER_V2_NOTIFICATION_VIRTUALIZATION_FAILURE                                       = "USER_V2_NOTIFICATION_VIRTUALIZATION_300";

    public final static String USER_V2_NOTIFICATION_VIRTUALIZATION_INVALID_TICKET                                = "USER_REQU_401";

    public final static String USER_V2_NOTIFICATION_VIRTUALIZATION_UNAUTHORIZED                                  = "USER_REQU_403";

    public final static String CRM_PROMO4ME_SUCCESS                                                              = "CRM_PROMO4ME_200";

    public final static String CRM_PROMO4ME_FAILURE                                                              = "CRM_PROMO4ME_300";

    public final static String CRM_PROMO4ME_INVALID_TICKET                                                       = "USER_REQU_401";

    public final static String CRM_PROMO4ME_UNAUTHORIZED                                                         = "USER_REQU_403";

    public final static String ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_SUCCESS                                      = "ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_200";

    public final static String ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_FAILURE                                      = "ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_300";

    public final static String ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_INVALID_TICKET                               = "ADMIN_REQU_401";

    public final static String CRM_GET_MISSIONS_SUCCESS                                                          = "CRM_GET_MISSIONS_200";

    public final static String CRM_GET_MISSIONS_FAILURE                                                          = "CRM_GET_MISSIONS_300";

    public final static String CRM_GET_MISSIONS_NOT_FOUND                                                        = "CRM_GET_MISSIONS_402";

    public final static String CRM_GET_MISSIONS_INVALID_TICKET                                                   = "USER_REQU_401";

    public final static String CRM_GET_MISSIONS_UNAUTHORIZED                                                     = "USER_REQU_403";

    public final static String ADMIN_PROPAGATION_USERDATA_SUCCESS                                                = "ADMIN_PROPAGATION_USERDATA_200";

    public final static String ADMIN_PROPAGATION_USERDATA_FAILURE                                                = "ADMIN_PROPAGATION_USERDATA_300";

    public final static String ADMIN_PROPAGATION_USERDATA_INVALID_TICKET                                         = "ADMIN_REQU_401";

    public final static String ADMIN_PROPAGATION_USERDATA_EXISTS                                                 = "ADMIN_PROPAGATION_USERDATA_301";

    public final static String CREATE_VOUCHER_PROMOTIONAL_SUCCESS                                                = "CREATE_VOUCHER_PROMOTIONAL_200";

    public final static String CREATE_VOUCHER_PROMOTIONAL_CREATE_ERROR                                           = "CREATE_VOUCHER_PROMOTIONAL_300";

    public final static String CREATE_VOUCHER_PROMOTIONAL_ASSIGN_ERROR                                           = "CREATE_VOUCHER_PROMOTIONAL_301";

    public final static String USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_SUCCESS                                  = "USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_200";

    public final static String USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_FAILURE                                  = "USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_300";

    public final static String USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_NOATTEMPTS                               = "USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_301";

    public final static String USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_NOMOREATTEMPTS                           = "USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_302";

    public final static String USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_INVALID_TICKET                           = "USER_REQU_401";

    public final static String USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_UNAUTHORIZED                             = "USER_REQU_403";

    public final static String ADMIN_GENERAL_CREATE_SUCCESS                                                      = "ADMIN_GENERAL_CREATE_200";

    public final static String ADMIN_GENERAL_CREATE_INVALID_BODY                                                 = "ADMIN_GENERAL_CREATE_REQU_401";

    public final static String ADMIN_GENERAL_CREATE_FAILURE                                                      = "ADMIN_GENERAL_CREATE_300";

    public final static String ADMIN_GENERAL_CREATE_ERROR_FIND_ID                                                = "ADMIN_GENERAL_CREATE_301";

    public final static String ADMIN_GENERAL_CREATE_ERROR_MANDATORY_FIELD_NULL                                   = "ADMIN_GENERAL_CREATE_302";

    public final static String ADMIN_GENERAL_CREATE_NOT_EXISTS                                                   = "ADMIN_GENERAL_CREATE_303";

    public final static String ADMIN_GENERAL_CREATE_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_GENERAL_UPDATE_SUCCESS                                                      = "ADMIN_GENERAL_UPDATE_200";

    public final static String ADMIN_GENERAL_UPDATE_INVALID_BODY                                                 = "ADMIN_GENERAL_UPDATE_REQU_401";

    public final static String ADMIN_GENERAL_UPDATE_FAILURE                                                      = "ADMIN_GENERAL_UPDATE_300";

    public final static String ADMIN_GENERAL_UPDATE_ERROR_FIND_ID                                                = "ADMIN_GENERAL_UPDATE_301";

    public final static String ADMIN_GENERAL_UPDATE_ERROR_MANDATORY_FIELD_NULL                                   = "ADMIN_GENERAL_UPDATE_302";

    public final static String ADMIN_GENERAL_UPDATE_NOT_EXISTS                                                   = "ADMIN_GENERAL_UPDATE_303";

    public final static String ADMIN_GENERAL_UPDATE_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_SUCCESS                                  = "ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_200";

    public final static String ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_FAILURE                                  = "ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_300";

    public final static String ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_INVALID_TICKET                           = "ADMIN_REQU_401";

    public final static String ADMIN_REMOVE_SUCCESS                                                              = "ADMIN_REMOVE_200";

    public final static String ADMIN_REMOVE_INVALID_BODY                                                         = "ADMIN_REMOVE_REQU_401";

    public final static String ADMIN_REMOVE_NOT_EXISTS                                                           = "ADMIN_REMOVE_501";

    public final static String ADMIN_REMOVE_FAILURE                                                              = "ADMIN_REMOVE_400";

    public final static String ADMIN_MASSIVE_REMOVE_SUCCESS                                                      = "ADMIN_MASSIVE_REMOVE_200";

    public final static String ADMIN_MASSIVE_REMOVE_INVALID_BODY                                                 = "ADMIN_MASSIVE_REMOVE_REQU_401";

    public final static String ADMIN_MASSIVE_REMOVE_NOT_EXISTS                                                   = "ADMIN_MASSIVE_REMOVE_501";

    public final static String ADMIN_MASSIVE_REMOVE_FAILURE                                                      = "ADMIN_MASSIVE_REMOVE_400";

    public final static String ADMIN_MASSIVE_REMOVE_INVALID_TICKET                                               = "ADMIN_REQU_401";

    public final static String ADMIN_ADMIN_ROLE_CREATE_SUCCESS                                                   = "ADMIN_ADMIN_ROLE_CREATE_200";

    public final static String ADMIN_ADMIN_ROLE_CREATE_FAILURE                                                   = "ADMIN_ADMIN_ROLE_CREATE_300";

    public final static String ADMIN_ADMIN_ROLE_CREATE_CHECK_FAILURE                                             = "ADMIN_ADMIN_ROLE_CREATE_301";

    public final static String ADMIN_ADMIN_ROLE_CREATE_INVALID_TICKET                                            = "ADMIN_REQU_401";

    public final static String ADMIN_ADMIN_ROLE_CREATE_NOT_EXIST_NAME                                            = "ADMIN_ADMIN_ROLE_CREATE_302";

    public final static String ADMIN_ADMIN_ROLE_DELETE_SUCCESS                                                   = "ADMIN_ADMIN_ROLE_DELETE_200";

    public final static String ADMIN_ADMIN_ROLE_DELETE_FAILURE                                                   = "ADMIN_ADMIN_ROLE_DELETE_300";

    public final static String ADMIN_ADMIN_ROLE_DELETE_CHECK_FAILURE                                             = "ADMIN_ADMIN_ROLE_DELETE_301";

    public final static String ADMIN_ADMIN_ROLE_DELETE_INVALID_TICKET                                            = "ADMIN_REQU_401";

    public final static String ADMIN_ADMIN_ROLE_DELETE_NOT_EXIST_NAME                                            = "ADMIN_ADMIN_ROLE_DELETE_302";

    public final static String ADMIN_ADD_ROLE_TO_ADMIN_SUCCESS                                                   = "ADMIN_ADD_ROLE_TO_ADMIN_200";

    public final static String ADMIN_ADD_ROLE_TO_ADMIN_FAILURE                                                   = "ADMIN_ADD_ROLE_TO_ADMIN_300";

    public final static String ADMIN_ADD_ROLE_TO_ADMIN_CHECK_FAILURE                                             = "ADMIN_ADD_ROLE_TO_ADMIN_301";

    public final static String ADMIN_ADD_ROLE_TO_ADMIN_INVALID_TICKET                                            = "ADMIN_REQU_401";

    public final static String ADMIN_ADD_ROLE_TO_ADMIN_NOT_EXIST_NAME                                            = "ADMIN_ADD_ROLE_TO_ADMIN_302";

    public final static String ADMIN_ADMIN_ROLE_RETRIEVE_SUCCESS                                                 = "ADMIN_ADMIN_ROLE_RETRIEVE_200";

    public final static String ADMIN_ADMIN_ROLE_RETRIEVE_FAILURE                                                 = "ADMIN_ADMIN_ROLE_RETRIEVE_300";

    public final static String ADMIN_ADMIN_ROLE_RETRIEVE_CHECK_FAILURE                                           = "ADMIN_ADMIN_ROLE_RETRIEVE_301";

    public final static String ADMIN_ADMIN_ROLE_RETRIEVE_INVALID_TICKET                                          = "ADMIN_REQU_401";

    public final static String ADMIN_ADMIN_ROLE_RETRIEVE_NOT_EXIST_NAME                                          = "ADMIN_ADMIN_ROLE_RETRIEVE_302";

    public final static String ADMIN_ADMIN_ROLE_REMOVE_SUCCESS                                                   = "ADMIN_ADMIN_ROLE_REMOVE_200";

    public final static String ADMIN_ADMIN_ROLE_REMOVE_FAILURE                                                   = "ADMIN_ADMIN_ROLE_REMOVE_300";

    public final static String ADMIN_ADMIN_ROLE_REMOVE_CHECK_FAILURE                                             = "ADMIN_ADMIN_ROLE_REMOVE_301";

    public final static String ADMIN_ADMIN_ROLE_REMOVE_INVALID_TICKET                                            = "ADMIN_REQU_401";

    public final static String ADMIN_ADMIN_ROLE_REMOVE_NOT_EXIST_NAME                                            = "ADMIN_ADMIN_ROLE_REMOVE_302";

    public final static String ADMIN_PUSH_NOTIFICATION_TEST_INVALID_TICKET                                       = "ADMIN_REQU_401";

    public final static String ADMIN_PUSH_NOTIFICATION_TEST_FAILURE                                              = "ADMIN_PUSH_NOTIFICATION_TEST_300";

    public final static String ADMIN_PUSH_NOTIFICATION_TEST_SUCCESS                                              = "ADMIN_PUSH_NOTIFICATION_TEST_200";

    public final static String ADMIN_PUSH_NOTIFICATION_TEST_NOT_EXISTS                                           = "ADMIN_PUSH_NOTIFICATION_TEST_302";

    public final static String ADMIN_PUSH_NOTIFICATION_TEST_INVALID_PARAMETER                                    = "ADMIN_PUSH_NOTIFICATION_TEST_301";

    public final static String ADMIN_UPDATE_SUCCESS                                                              = "ADMIN_UPD_200";

    public final static String ADMIN_UPDATE_FAILURE                                                              = "ADMIN_UPD_400";

    public final static String ADMIN_UPDATE_INVALID_TICKET                                                       = "ADMIN_REQU_401";

    public final static String ADMIN_UPDATE_UNAUTHORIZED                                                         = "ADMIN_UPD_403";

    public final static String ADMIN_RETRIEVE_SUCCESS                                                            = "ADMIN_RETRIEVE_200";

    public final static String ADMIN_RETRIEVE_FAILURE                                                            = "ADMIN_RETRIEVE_400";

    public final static String ADMIN_RETRIEVE_INVALID_TICKET                                                     = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_UNAUTHORIZED                                                       = "ADMIN_RETRIEVE_403";

    public final static String ADMIN_RETRIEVE_LANDING_SUCCESS                                                    = "ADMIN_RETRIEVE_LANDING_200";

    public final static String ADMIN_RETRIEVE_LANDING_FAILURE                                                    = "ADMIN_RETRIEVE_LANDING_300";

    public final static String ADMIN_RETRIEVE_LANDING_INVALID_TICKET                                             = "ADMIN_REQU_401";

    public final static String ADMIN_RETRIEVE_LANDING_UNAUTHORIZED                                               = "ADMIN_RETRIEVE_LANDING_403";

    public final static String ADMIN_DELETE_SUCCESS                                                              = "ADMIN_DELETE_200";

    public final static String ADMIN_DELETE_FAILURE                                                              = "ADMIN_DELETE_300";

    public final static String ADMIN_DELETE_UNAUTHORIZED                                                         = "ADMIN_DELETE_403";

    public final static String ADMIN_ROLE_UNAUTHORIZED                                                           = "ADMIN_ROLE_300";

    public final static String ADMIN_CREATE_REFUELING_USER_SUCCESS                                               = "ADMIN_CREATE_REFUELING_USER_200";

    public final static String ADMIN_CREATE_REFUELING_USER_INVALID_TICKET                                        = "USER_REQU_401";

    public final static String ADMIN_CREATE_REFUELING_USER_USER_EXISTS                                           = "ADMIN_CREATE_REFUELING_USER_501";

    public final static String ADMIN_CREATE_REFUELING_USER_INVALID_MAIL                                          = "ADMIN_CREATE_REFUELING_USER_502";

    public final static String ADMIN_UPD_PWD_REFUELING_USER_SUCCESS                                              = "ADMIN_UPD_PWD_REFUELING_USER_200";

    public final static String ADMIN_UPD_PWD_REFUELING_USER_INVALID_TICKET                                       = "USER_REQU_401";

    public final static String ADMIN_UPD_PWD_REFUELING_USER_USER_NOT_EXISTS                                      = "ADMIN_UPD_PWD_REFUELING_USER_501";

    public final static String ADMIN_UPD_PWD_REFUELING_USER_USER_NOT_REFUELING                                   = "ADMIN_UPD_PWD_REFUELING_USER_402";

    public final static String ADMIN_INSERT_PAY_METH_REFUELING_USER_SUCCESS                                      = "ADMIN_INSERT_PAY_METH_REFUELING_USER_200";

    public final static String ADMIN_INSERT_PAY_METH_REFUELING_USER_USER_NOT_REFUELING                           = "ADMIN_INSERT_PAY_METH_REFUELING_USER_402";

    public final static String ADMIN_INSERT_PAY_METH_REFUELING_USER_INVALID_TICKET                               = "USER_REQU_401";

    public final static String ADMIN_INSERT_PAY_METH_REFUELING_USER_USER_NOT_EXISTS                              = "ADMIN_INSERT_PAY_METH_REFUELING_USER_501";

    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_SUCCESS                                          = "ADMIN_RM_PAY_METH_REFUELING_USER_200";

    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_FAILURE                                          = "ADMIN_RM_PAY_METH_REFUELING_USER_300";

    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_USER_NOT_EXISTS                                  = "ADMIN_RM_PAY_METH_REFUELING_USER_501";

    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_USER_NOT_REFUELING                               = "ADMIN_RM_PAY_METH_REFUELING_USER_402";
    
    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_INVALID_PARAMETERS                               = "ADMIN_RM_PAY_METH_REFUELING_USER_403";
    
    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_INVALID_PARTNER_TYPE                             = "ADMIN_RM_PAY_METH_REFUELING_USER_404";
    
    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_PAN_NOT_FOUND                                    = "ADMIN_RM_PAY_METH_REFUELING_USER_405";

    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_INVALID_TICKET                                   = "USER_REQU_401";

    public final static String ADMIN_RETRY_PAY_DATA_REFUELING_USER_SUCCESS                                       = "ADMIN_RETRY_PAY_DATA_REFUELING_USER_200";

    public final static String ADMIN_RETRY_PAY_DATA_REFUELING_USER_USER_NOT_REFUELING                            = "ADMIN_RETRY_PAY_DATA_REFUELING_USER_402";

    public final static String ADMIN_RETRY_PAY_DATA_REFUELING_USER_INVALID_TICKET                                = "USER_REQU_401";

    public final static String ADMIN_RETRY_PAY_DATA_REFUELING_USER_USER_NOT_EXISTS                               = "ADMIN_RETRY_PAY_DATA_REFUELING_USER_501";

    public final static String USER_V2_GETLANDING_SUCCESS                                                        = "USER_V2_GETLANDING_200";

    public final static String USER_V2_GETLANDING_FAILURE                                                        = "USER_V2_GETLANDING_300";

    public final static String USER_V2_GETLANDING_NOTFOUND                                                       = "USER_V2_GETLANDING_301";

    public final static String USER_V2_GETLANDING_INVALID_TICKET                                                 = "USER_REQU_401";

    public final static String USER_V2_GETLANDING_UNAUTHORIZED                                                   = "USER_V2_GETLANDING_302";

    public final static String USER_V2_GET_PARTNER_LIST_SUCCESS                                                  = "USER_V2_GET_PARTNER_LIST_200";

    public final static String USER_V2_GET_PARTNER_LIST_FAILURE                                                  = "USER_V2_GET_PARTNER_LIST_300";

    public final static String USER_V2_GET_PARTNER_LIST_NOTFOUND                                                 = "USER_V2_GET_PARTNER_LIST_400";

    public final static String USER_V2_GET_PARTNER_LIST_INVALID_TICKET                                           = "USER_REQU_401";

    public final static String USER_V2_GET_PARTNER_LIST_UNAUTHORIZED                                             = "USER_REQU_403";

    public final static String USER_V2_GET_CATEGORY_LIST_SUCCESS                                                 = "USER_V2_GET_CATEGORY_LIST_200";

    public final static String USER_V2_GET_CATEGORY_LIST_FAILURE                                                 = "USER_V2_GET_CATEGORY_LIST_300";

    public final static String USER_V2_GET_CATEGORY_LIST_NOTFOUND                                                = "USER_V2_GET_CATEGORY_LIST_400";

    public final static String USER_V2_GET_CATEGORY_LIST_INVALID_TICKET                                          = "USER_REQU_401";

    public final static String USER_V2_GET_CATEGORY_LIST_UNAUTHORIZED                                            = "USER_REQU_403";

    public final static String USER_V2_GET_PARTNER_ACTION_URL_SUCCESS                                            = "USER_V2_GET_PARTNER_ACTION_URL_200";

    public final static String USER_V2_GET_PARTNER_ACTION_URL_FAILURE                                            = "USER_V2_GET_PARTNER_ACTION_URL_300";

    public final static String USER_V2_GET_PARTNER_ACTION_URL_NOTFOUND                                           = "USER_V2_GET_PARTNER_ACTION_URL_400";

    public final static String USER_V2_GET_PARTNER_ACTION_URL_INVALID_TICKET                                     = "USER_REQU_401";

    public final static String USER_V2_GET_PARTNER_ACTION_URL_UNAUTHORIZED                                       = "USER_REQU_403";

    public final static String USER_V2_GET_AWARD_LIST_SUCCESS                                                    = "USER_V2_GET_AWARD_LIST_200";

    public final static String USER_V2_GET_AWARD_LIST_FAILURE                                                    = "USER_V2_GET_AWARD_LIST_300";

    public final static String USER_V2_GET_AWARD_LIST_NOTFOUND                                                   = "USER_V2_GET_AWARD_LIST_400";

    public final static String USER_V2_GET_AWARD_LIST_INVALID_TICKET                                             = "USER_REQU_401";

    public final static String USER_V2_GET_AWARD_LIST_UNAUTHORIZED                                               = "USER_REQU_403";

    public final static String USER_V2_GET_MISSION_LIST_SUCCESS                                                  = "USER_V2_GET_MISSION_LIST_200";

    public final static String USER_V2_GET_MISSIOJN_LIST_FAILURE                                                 = "USER_V2_GET_MISSION_LIST_300";

    public final static String USER_V2_GET_MISSION_LIST_NOTFOUND                                                 = "USER_V2_GET_MISSION_LIST_400";

    public final static String USER_V2_GET_MISSION_LIST_INVALID_TICKET                                           = "USER_REQU_401";

    public final static String USER_V2_GET_MISSION_LIST_UNAUTHORIZED                                             = "USER_REQU_403";

    public final static String USER_V2_GET_RECEIPT_SUCCESS                                                       = "USER_V2_GET_RECEIPT_200";

    public final static String USER_V2_GET_RECEIPT_FAILURE                                                       = "USER_V2_GET_RECEIPT_300";

    public final static String USER_V2_GET_RECEIPT_NOTFOUND                                                      = "USER_V2_GET_RECEIPT_400";

    public final static String USER_V2_GET_RECEIPT_INVALID_TICKET                                                = "USER_REQU_401";

    public final static String USER_V2_GET_RECEIPT_UNAUTHORIZED                                                  = "USER_REQU_403";

    public final static String USER_V2_GET_BRAND_LIST_SUCCESS                                                    = "USER_V2_GET_BRAND_LIST_200";

    public final static String USER_V2_GET_BRAND_LIST_FAILURE                                                    = "USER_V2_GET_BRAND_LIST_300";

    public final static String USER_V2_GET_BRAND_LIST_NOTFOUND                                                   = "USER_V2_GET_BRAND_LIST_400";

    public final static String USER_V2_GET_BRAND_LIST_INVALID_TICKET                                             = "USER_REQU_401";

    public final static String USER_V2_GET_REDEMPTION_AWARD_SUCCESS                                              = "USER_V2_GET_REDEMPTION_AWARD_200";

    public final static String USER_V2_GET_REDEMPTION_AWARD_LIST_FAILURE                                         = "USER_V2_GET_REDEMPTION_AWARD_300";
    
    public final static String USER_V2_GET_REDEMPTION_AWARD_LIST_UPDATE_APP                                      = "USER_V2_GET_REDEMPTION_AWARD_301";

    public final static String USER_V2_GET_REDEMPTION_AWARD_REQUESTID_ALREADY_EXISTS                             = "USER_V2_GET_REDEMPTION_AWARD_400";

    public final static String USER_V2_GET_REDEMPTION_AWARD_LIST_NOTFOUND                                        = "USER_V2_GET_REDEMPTION_AWARD_404";

    public final static String USER_V2_GET_REDEMPTION_AWARD_LIST_INVALID_TICKET                                  = "USER_REQU_401";

    public final static String USER_V2_GET_REDEMPTION_AWARD_LIST_UNAUTHORIZED                                    = "USER_REQU_403";

    public final static String USER_V2_GET_AWARD_LIST_GENERIC_ERROR                                              = "USER_V2_GET_AWARD_LIST_301";

    public final static String USER_V2_GET_AWARD_LIST_FISCAL_CODE                                                = "USER_V2_GET_AWARD_LIST_302";

    public final static String USER_V2_GET_AWARD_LIST_INVALID_BALANCE                                            = "USER_V2_GET_AWARD_LIST_303";

    public final static String USER_V2_GET_AWARD_LIST_CUSTOMER_BLOCKED                                           = "USER_V2_GET_AWARD_LIST_304";

    public final static String USER_V2_GETPROMOPOPUP_SUCCESS                                                     = "USER_GETPROMOPOPUP_200";

    public final static String USER_V2_GETPROMOPOPUP_FAILURE                                                     = "USER_GETPROMOPOPUP_300";

    public final static String USER_V2_GETPROMOPOPUP_PROMO_NOT_FOUND                                             = "USER_GETPROMOPOPUP_400";

    public final static String USER_V2_GETPROMOPOPUP_INVALID_TICKET                                              = "USER_REQU_401";

    public final static String USER_V2_GETPROMOPOPUP_UNAUTHORIZED                                                = "USER_REQU_403";

    public final static String RETRIEVE_POLLING_INTERVAL_SUCCESS                                                 = "RETRIEVE_POLLING_INTERVAL_200";

    public final static String RETRIEVE_POLLING_INTERVAL_FAILURE                                                 = "RETRIEVE_POLLING_INTERVAL_300";

    public final static String RETRIEVE_POLLING_INTERVAL_NOT_RECOGNIZED                                          = "RETRIEVE_POLLING_INTERVAL_400";

    public final static String RETRIEVE_POLLING_INTERVAL_INVALID_TICKET                                          = "USER_REQU_401";

    public final static String RETRIEVE_POLLING_INTERVAL_UNAUTHORIZED                                            = "USER_REQU_403";

    public final static String TRANSACTION_SUCCESS                                                               = "TRANSACTION_PAID";

    public final static String TRANSACTION_FAILURE                                                               = "ERROR";

    public final static String TRANSACTION_INVALID_REQUEST                                                       = "ERROR";

    public final static String TRANSACTION_SYSTEM_ERROR                                                          = "ERROR";

    public final static String TRANSACTION_NOT_FOUND                                                             = "TRANSACTION_NOT_FOUND";

    public final static String TRANSACTION_ON_HOLD                                                               = "TRANSACTION_ON_HOLD";

    public final static String TRANSACTION_UNPAID                                                                = "TRANSACTION_UNPAID";

    public final static String TRANSACTION_CANCELLED                                                             = "TRANSACTION_CANCELLED";

    public final static String TRANSACTION_PARAMETER_NOT_FOUND                                                   = "ERROR";

    public final static String TRANSACTION_STATION_NOT_ENABLED                                                   = "STATION_NOT_ENABLED";

    public final static String TRANSACTION_PUMP_NOT_VALID                                                        = "PUMP_NOT_VALID";

    public final static String ADMIN_UPDATE_CITIES_INVALID_TICKET                                                = "USER_REQU_401";

    public final static String ADMIN_UPDATE_CITIES_UNAUTHORIZED                                                  = "USER_REQU_403";

    public final static String ADMIN_UPDATE_CITIES_SUCCESS                                                       = "ADMIN_UPDATE_CITIES_200";

    public final static String ADMIN_UPDATE_CITIES_DATA_EMPTY                                                    = "ADMIN_UPDATE_CITIES_300";

    public final static String ADMIN_REMOVE_TICKET_SUCCESS                                                       = "ADMIN_REMOVE_TICKET_200";

    public final static String ADMIN_REMOVE_TICKET_INVALID_PARAMETERS                                            = "ADMIN_REMOVE_TICKET_REQU_301";

    public final static String ADMIN_REMOVE_TICKET_FAILURE                                                       = "ADMIN_REMOVE_TICKET_300";

    public final static String ADMIN_REMOVE_TICKET_INVALID_TICKET                                                = "ADMIN_REQU_401";

    public final static String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_SUCCESS                                       = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_200";

    public final static String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_FAILURE                                       = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_300";

    public final static String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_INVALID_TICKET                                = "USER_REQU_401";

    public final static String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_UNAUTHORIZED                                  = "USER_REQU_403";

    public final static String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_MAX_REACHED                                   = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_401";

    public final static String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_ALREADY_USED                                  = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_402";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_VALID                                         = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_500";

    public final static String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_NOT_VALID                                     = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_501";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_EXPIRED                                       = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_502";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_SPENT                                         = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_503";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_CANCELED                                      = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_504";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_TO_CONFIRM                                    = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_505";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_INEXISTENT                                    = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_506";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_REMOVED                                       = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_507";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_UNKNOW                                        = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_508";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_NOT_ASSIGNED                                  = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_509";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_EVENT_NOT_ACTIVE                              = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_510";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_EVENT_EXPIRED                                 = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_511";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_USER_NOT_ASSIGNED                             = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_512";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_NOT_PROMO_FOR_CODE                            = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_513";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_ERROR_USER_CODE_NOT_VALID                     = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_514";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_VOUCHER_CODE_NOT_VALID                        = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_515";

    public static final String USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_TO_RETRY                                      = "USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_517";

    public final static String USER_V2_SOCIAL_AUTH_SUCCESS                                                       = "SOCIAL_USER_AUTH_200";

    public final static String USER_V2_SOCIAL_AUTH_FAILURE                                                       = "SOCIAL_USER_AUTH_300";

    public final static String USER_V2_SOCIAL_AUTH_INVALID_TICKET                                                = "USER_REQU_401";

    public final static String USER_V2_SOCIAL_AUTH_LOGIN_ERROR                                                   = "SOCIAL_USER_AUTH_400";

    public final static String USER_V2_SOCIAL_AUTH_MISSING_EMAIL                                                 = "SOCIAL_USER_AUTH_401";

    public final static String USER_V2_SOCIAL_AUTH_WRONG_SOCIAL                                                  = "SOCIAL_USER_AUTH_402";

    public final static String USER_V2_SOCIAL_AUTH_NEW_USER                                                      = "SOCIAL_USER_AUTH_201";

    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_SUCCESS                                            = "EXTERNAL_PROVIDER_USER_AUTH_200";

    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_FAILURE                                            = "EXTERNAL_PROVIDER_USER_AUTH_300";

    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_INVALID_TICKET                                     = "USER_REQU_401";

    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_LOGIN_ERROR                                        = "EXTERNAL_PROVIDER_USER_AUTH_400";

    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_MISSING_EMAIL                                      = "EXTERNAL_PROVIDER_USER_AUTH_401";

    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_WRONG                                              = "EXTERNAL_PROVIDER_USER_AUTH_402";

    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_USER_EXISTS                                        = "EXTERNAL_PROVIDER_USER_AUTH_403";

    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_NO_FISCAL_CODE                                     = "EXTERNAL_PROVIDER_USER_AUTH_404";

    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_NEW_USER                                           = "EXTERNAL_PROVIDER_USER_AUTH_201";

    public final static String USER_V2_EXTERNAL_PROVIDER_LOGON_TICKET_ALREADY_IN_USE                             = "EXTERNAL_PROVIDER_USER_AUTH_202";

    public static final String PARKING_RETRIEVE_PARKING_ZONES_SUCCESS                                            = "PARKING_RETRIEVE_PARKING_ZONES_200";

    public static final String PARKING_RETRIEVE_PARKING_ZONES_FAILURE                                            = "PARKING_RETRIEVE_PARKING_ZONES_300";

    public static final String PARKING_RETRIEVE_PARKING_ZONES_NOT_FOUND                                          = "PARKING_RETRIEVE_PARKING_ZONES_400";

    public final static String PARKING_RETRIEVE_PARKING_ZONES_INVALID_TICKET                                     = "USER_REQU_401";

    public final static String PARKING_RETRIEVE_PARKING_ZONES_UNAUTHORIZED                                       = "USER_REQU_403";

    public static final String PARKING_ESTIMATE_PARKING_PRICE_SUCCESS                                            = "PARKING_ESTIMATE_PARKING_PRICE_200";

    public static final String PARKING_ESTIMATE_PARKING_PRICE_FAILURE                                            = "PARKING_ESTIMATE_PARKING_PRICE_300";

    public final static String PARKING_ESTIMATE_PARKING_PRICE_INVALID_TICKET                                     = "USER_REQU_401";

    public final static String PARKING_ESTIMATE_PARKING_PRICE_UNAUTHORIZED                                       = "USER_REQU_403";

    public static final String PARKING_ESTIMATE_PARKING_PRICE_NO_PAYMENT_METHOD_FOUND                            = "PARKING_ESTIMATE_PARKING_PRICE_400";

    public static final String PARKING_ESTIMATE_PARKING_PRICE_PIN_NOT_FOUND                                      = "PARKING_ESTIMATE_PARKING_PRICE_401";

    public static final String PARKING_ESTIMATE_PARKING_PRICE_INVALID_REQUESTED_END_TIME                         = "PARKING_ESTIMATE_PARKING_PRICE_402";

    public static final String PARKING_ESTIMATE_PARKING_PRICE_AMOUNT_OVER_THRESHOLD                              = "PARKING_ESTIMATE_PARKING_PRICE_403";

    public static final String PARKING_ESTIMATE_PARKING_PRICE_AMOUNT_ZERO                                        = "PARKING_ESTIMATE_PARKING_PRICE_404";

    public static final String PARKING_APPROVE_PARKING_TRANSACTION_SUCCESS                                       = "PARKING_APPROVE_PARKING_TRANSACTION_200";

    public static final String PARKING_APPROVE_PARKING_TRANSACTION_FAILURE                                       = "PARKING_APPROVE_PARKING_TRANSACTION_300";

    public final static String PARKING_APPROVE_PARKING_TRANSACTION_INVALID_TICKET                                = "USER_REQU_401";

    public final static String PARKING_APPROVE_PARKING_TRANSACTION_UNAUTHORIZED                                  = "USER_REQU_403";

    public final static String PARKING_APPROVE_PARKING_TRANSACTION_PAYMENT_WRONG                                 = "PARKING_APPROVE_PARKING_TRANSACTION_400";

    public final static String PARKING_APPROVE_PARKING_TRANSACTION_ERROR_PIN                                     = "PARKING_APPROVE_PARKING_TRANSACTION_401";

    public final static String PARKING_APPROVE_PARKING_TRANSACTION_ERROR_PIN_LAST_ATTEMPT                        = "PARKING_APPROVE_PARKING_TRANSACTION_402";

    public final static String PARKING_APPROVE_PARKING_TRANSACTION_ERROR_PIN_ATTEMPTS_LEFT                       = "PARKING_APPROVE_PARKING_TRANSACTION_403";

    public static final String PARKING_APPROVE_PARKING_TRANSACTION_AUTH_KO                                       = "PARKING_APPROVE_PARKING_TRANSACTION_404";

    public static final String PARKING_APPROVE_PARKING_TRANSACTION_NOT_FOUND                                     = "PARKING_APPROVE_PARKING_TRANSACTION_405";

    public static final String PARKING_APPROVE_PARKING_TRANSACTION_INVALID_STATUS                                = "PARKING_APPROVE_PARKING_TRANSACTION_406";

    public static final String PARKING_APPROVE_PARKING_TRANSACTION_AMOUNT_OVER_THRESHOLD                         = "PARKING_APPROVE_PARKING_TRANSACTION_407";

    public final static String PARKING_APPROVE_PARKING_TRANSACTION_INVALID_STALL_CODE                            = "PARKING_APPROVE_PARKING_TRANSACTION_408";

    public final static String PARKING_APPROVE_PARKING_TRANSACTION_PAYMENT_COMUNICATION_FAILURE                  = "PARKING_APPROVE_PARKING_TRANSACTION_409";

    public final static String PARKING_APPROVE_PARKING_TRANSACTION_PLATE_NUMBER_ALREADY_IN_USE                   = "PARKING_APPROVE_PARKING_TRANSACTION_410";

    public static final String PARKING_APPROVE_PARKING_TRANSACTION_INSUFFICIENT_AMOUNT                           = "PARKING_APPROVE_PARKING_TRANSACTION_411";

    public static final String PARKING_APPROVE_PARKING_TRANSACTION_INSUFFICIENT_VOUCHER_CREDIT                   = "PARKING_APPROVE_PARKING_TRANSACTION_412";

    public final static String PARKING_TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS                     = "PARKING_TRANSACTION_PRE_AUTH_CONSUME_VOUCHER_200";

    public final static String PARKING_TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE                     = "PARKING_TRANSACTION_PRE_AUTH_CONSUME_VOUCHER_300";

    public final static String PARKING_TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_NOT_FOUND                   = "PARKING_TRANSACTION_PRE_AUTH_CONSUME_VOUCHER_400";

    public final static String PARKING_TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_SYSTEM_ERRROR               = "PARKING_TRANSACTION_PRE_AUTH_CONSUME_VOUCHER_301";

    public final static String PARKING_TRANSACTION_PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER = "PARKING_TRANSACTION_PRE_AUTH_CONSUME_VOUCHER_302";

    public final static String PARKING_TRANSACTION_NO_VALID_VOUCHER_FOUND                                        = "PARKING_TRANSACTION_NO_VALID_VOUCHER_FOUND_309";

    public final static String PARKING_TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS              = "PARKING_TRANSACTION_CANCEL_PRE_AUTH_CONSUME_VOUCHER_200";

    public final static String PARKING_TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE              = "PARKING_TRANSACTION_CANCEL_PRE_AUTH_CONSUME_VOUCHER_300";

    public final static String PARKING_TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_SYSTEM_ERRROR        = "PARKING_TRANSACTION_CANCEL_PRE_AUTH_CONSUME_VOUCHER_301";

    public final static String PARKING_TRANSACTION_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_NOT_FOUND            = "PARKING_TRANSACTION_CANCEL_PRE_AUTH_CONSUME_VOUCHER_400";

    public final static String PARKING_TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_SUCCESS                              = "PARKING_TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_200";

    public final static String PARKING_TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_FAILURE                              = "PARKING_TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_300";

    public final static String PARKING_TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_SYSTEM_ERROR                         = "PARKING_TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_301";

    public final static String PARKING_TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_NOT_FOUND                            = "PARKING_TRANSACTION_CONSUME_VOUCHER_PRE_AUTH_400";

    public static final String PARKING_END_PARKING_SUCCESS                                                       = "PARKING_END_PARKING_200";

    public static final String PARKING_END_PARKING_FAILURE                                                       = "PARKING_END_PARKING_300";

    public static final String PARKING_END_PARKING_PAYMENT_COMUNICATION_FAILURE                                  = "PARKING_END_PARKING_301";

    public static final String PARKING_END_PARKING_PAYMENT_MOV_KO                                                = "PARKING_END_PARKING_302";

    public static final String PARKING_END_PARKING_USER_LOCKED                                                   = "PARKING_END_PARKING_303";

    public static final String PARKING_END_PARKING_PAYMENT_CAN_KO                                                = "PARKING_END_PARKING_304";

    public static final String PARKING_END_PARKING_PAYMENT_CAN_ERROR                                             = "PARKING_END_PARKING_305";

    public final static String PARKING_END_PARKING_INVALID_TICKET                                                = "USER_REQU_401";

    public final static String PARKING_END_PARKING_UNAUTHORIZED                                                  = "USER_REQU_403";

    public static final String PARKING_ESTIMATE_END_PARKING_PRICE_SUCCESS                                        = "PARKING_ESTIMATE_END_PARKING_PRICE_200";

    public static final String PARKING_ESTIMATE_END_PARKING_PRICE_FAILURE                                        = "PARKING_ESTIMATE_END_PARKING_PRICE_300";

    public final static String PARKING_ESTIMATE_END_PARKING_PRICE_INVALID_TICKET                                 = "USER_REQU_401";

    public final static String PARKING_ESTIMATE_END_PARKING_PRICE_UNAUTHORIZED                                   = "USER_REQU_403";

    public static final String PARKING_ESTIMATE_END_PARKING_ALREADY_CLOSED                                       = "PARKING_ESTIMATE_END_PARKING_PRICE_400";

    public static final String PARKING_ESTIMATE_EXTEND_PARKING_PRICE_SUCCESS                                     = "PARKING_ESTIMATE_EXTEND_PARKING_PRICE_200";

    public static final String PARKING_ESTIMATE_EXTEND_PARKING_PRICE_FAILURE                                     = "PARKING_ESTIMATE_EXTEND_PARKING_PRICE_300";

    public static final String PARKING_ESTIMATE_EXTEND_PARKING_PRICE_INVALID_REQUESTED_END_TIME                  = "PARKING_ESTIMATE_EXTEND_PARKING_PRICE_400";

    public static final String PARKING_ESTIMATE_EXTEND_PARKING_PRICE_EXCEEDED                                    = "PARKING_ESTIMATE_EXTEND_PARKING_PRICE_401";

    public static final String PARKING_ESTIMATE_EXTEND_PARKING_ALREADY_CLOSED                                    = "PARKING_ESTIMATE_EXTEND_PARKING_PRICE_402";

    public final static String PARKING_ESTIMATE_EXTEND_PARKING_PRICE_INVALID_TICKET                              = "USER_REQU_401";

    public final static String PARKING_ESTIMATE_EXTEND_PARKING_PRICE_UNAUTHORIZED                                = "USER_REQU_403";

    public static final String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_SUCCESS                                = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_200";

    public static final String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_FAILURE                                = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_300";

    public final static String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_INVALID_TICKET                         = "USER_REQU_401";

    public final static String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_UNAUTHORIZED                           = "USER_REQU_403";

    public final static String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_PAYMENT_WRONG                          = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_400";

    public final static String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_CREATE_ERROR_PIN                       = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_401";

    public final static String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_ERROR_PIN_LAST_ATTEMPT                 = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_402";

    public final static String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_PIN_ATTEMPTS_LEFT                      = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_403";

    public static final String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_AUTH_KO                                = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_404";

    public static final String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_COMUNICATION_FAILURE                   = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_405";

    public static final String PARKING_APPROVE_EXTEND_USER_LOCKED                                                = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_406";

    public static final String PARKING_APPROVE_EXTEND_PARKING_INSUFFICIENT_AMOUNT                                = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_407";

    public static final String PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_ALREADY_CLOSE                          = "PARKING_APPROVE_EXTEND_PARKING_TRANSACTION_408";

    public static final String PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_SUCCESS                              = "RETRIEVE_PENDING_PARKING_TRANSACTION_200";

    public static final String PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_FAILURE                              = "RETRIEVE_PENDING_PARKING_TRANSACTION_300";

    public static final String PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_NOT_FOUND                            = "RETRIEVE_PENDING_PARKING_TRANSACTION_400";

    public final static String PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_INVALID_TICKET                       = "USER_REQU_401";

    public final static String PARKING_RETRIEVE_PENDING_PARKING_TRANSACTION_UNAUTHORIZED                         = "USER_REQU_403";

    public static final String PARKING_GETCITIES_SUCCESS                                                         = "PARKING_GETCITIES_200";

    public static final String PARKING_GETCITIES_FAILURE                                                         = "PARKING_GETCITIES_300";

    public static final String PARKING_GETCITIES_NOT_FOUND                                                       = "PARKING_GETCITIES_400";

    public final static String PARKING_GETCITIES_INVALID_TICKET                                                  = "USER_REQU_401";

    public final static String PARKING_GETCITIES_UNAUTHORIZED                                                    = "USER_REQU_403";

    public final static String USER_V2_ADD_PLATE_NUMBER_SUCCESS                                                  = "USER_ADD_PLATE_NUMBER_200";

    public final static String USER_V2_ADD_PLATE_NUMBER_FAILURE                                                  = "USER_ADD_PLATE_NUMBER_300";

    public final static String USER_V2_ADD_PLATE_NUMBER_FOUND                                                    = "USER_ADD_PLATE_NUMBER_400";

    public final static String USER_V2_ADD_PLATE_NUMBER_INVALID_FORMAT                                           = "USER_ADD_PLATE_NUMBER_401";

    public final static String USER_V2_ADD_PLATE_NUMBER_INVALID_TICKET                                           = "USER_REQU_401";

    public final static String USER_V2_ADD_PLATE_NUMBER_UNAUTHORIZED                                             = "USER_REQU_403";

    public final static String USER_V2_REMOVE_PLATE_NUMBER_SUCCESS                                               = "USER_REMOVE_PLATE_NUMBER_200";

    public final static String USER_V2_REMOVE_PLATE_NUMBER_FAILURE                                               = "USER_REMOVE_PLATE_NUMBER_300";

    public final static String USER_V2_REMOVE_PLATE_NUMBER_WRONG                                                 = "USER_REMOVE_PLATE_NUMBER_400";

    public final static String USER_V2_REMOVE_PLATE_NUMBER_INVALID_TICKET                                        = "USER_REQU_401";

    public final static String USER_V2_REMOVE_PLATE_NUMBER_UNAUTHORIZED                                          = "USER_REQU_403";

    public final static String USER_V2_SET_DEFAULT_PLATE_NUMBER_SUCCESS                                          = "USER_SET_DEFAULT_PLATE_NUMBER_200";

    public final static String USER_V2_SET_DEFAULT_PLATE_NUMBER_FAILURE                                          = "USER_SET_DEFAULT_PLATE_NUMBER_300";

    public final static String USER_V2_SET_DEFAULT_PLATE_NUMBER_WRONG                                            = "USER_SET_DEFAULT_PLATE_NUMBER_400";

    public final static String USER_V2_SET_DEFAULT_PLATE_NUMBER_INVALID_TICKET                                   = "USER_REQU_401";

    public final static String USER_V2_SET_DEFAULT_PLATE_NUMBER_UNAUTHORIZED                                     = "USER_REQU_403";

    public static final String PARKING_GET_PARKING_TRANSACTION_DETAIL_SUCCESS                                    = "GET_PARKING_TRANSACTION_DETAIL_200";

    public static final String PARKING_GET_PARKING_TRANSACTION_DETAIL_FAILURE                                    = "GET_PARKING_TRANSACTION_DETAIL_300";

    public static final String PARKING_GET_PARKING_TRANSACTION_DETAIL_NOT_FOUND                                  = "GET_PARKING_TRANSACTION_DETAIL_400";

    public final static String PARKING_GET_PARKING_TRANSACTION_DETAIL_INVALID_TICKET                             = "USER_REQU_401";

    public final static String PARKING_GET_PARKING_TRANSACTION_DETAIL_UNAUTHORIZED                               = "USER_REQU_403";

    public final static String USER_V2_GUEST_CREATE_SUCCESS                                                      = "USER_V2_GUEST_CREATE_200";

    public final static String USER_V2_GUEST_CREATE_INVALID_TICKET                                               = "USER_REQU_401";

    public final static String USER_V2_GUEST_CREATE_FAILURE                                                      = "USER_V2_GUEST_CREATE_300";

    public final static String USER_V2_GUEST_CREATE_INVALID_CAPTCHA                                              = "USER_V2_GUEST_CREATE_400";

    public final static String ADMIN_SYSTEM_CHECK_SUCCESS                                                        = "ADMIN_SYSTEM_CHECK_200";

    public final static String ADMIN_SYSTEM_CHECK_FAILURE                                                        = "ADMIN_SYSTEM_CHECK_300";

    public final static String ADMIN_SYSTEM_CHECK_INVALID_TICKET                                                 = "USER_REQU_401";

    public final static String ADMIN_SYSTEM_CHECK_UNAUTHORIZED                                                   = "USER_REQU_403";

    public final static String STATION_UPDATE_STAGING_SUCCESS                                                    = "STATION_UPDATE_STAGING_200";

    public final static String STATION_UPDATE_STAGING_STATIONS_LIST_EMPTY                                        = "STATION_UPDATE_STAGING_300";

    public final static String STATION_UPDATE_STAGING_REQUEST_ID_ALREADY_EXISTS                                  = "STATION_UPDATE_STAGING_301";

    public final static String STATION_UPDATE_MASTER_SUCCESS                                                     = "STATION_UPDATE_MASTER_200";

    public final static String STATION_UPDATE_MASTER_FAILURE                                                     = "STATION_UPDATE_MASTER_300";

    public final static String USER_BUSINESS_UPDATE_BUSINESS_DATA_SUCCESS                                        = "USER_BUSINESS_UPDATE_200";

    public final static String USER_BUSINESS_UPDATE_BUSINESS_DATA_FAILURE                                        = "USER_BUSINESS_UPDATE_300";

    public final static String USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_TICKET                                 = "USER_REQU_401";

    public final static String USER_BUSINESS_UPDATE_BUSINESS_DATA_UNAUTHORIZED                                   = "USER_REQU_403";

    public final static String USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_DATA                                   = "USER_BUSINESS_UPDATE_301";

    public final static String USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_VAT_NUMBER                             = "USER_BUSINESS_UPDATE_302";
    
    public final static String USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_FISCAL_CODE                            = "USER_BUSINESS_CHECK_312";

    public final static String USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_PROVINCE                               = "USER_BUSINESS_UPDATE_303";

    public final static String USER_BUSINESS_RECOVER_USERNAME_SUCCESS                                            = "USER_RECOVER_USERNAME_200";

    public final static String USER_BUSINESS_RECOVER_USERNAME_FAILURE                                            = "USER_RECOVER_USERNAME_300";

    public final static String USER_BUSINESS_RECOVER_USERNAME_INVALID_TAX_CODE                                   = "USER_RECOVER_USERNAME_301";

    public final static String USER_BUSINESS_RECOVER_USERNAME_INVALID_REQUEST                                    = "USER_REQU_400";

    public final static String USER_BUSINESS_RECOVER_USERNAME_UNAUTHORIZED                                       = "USER_REQU_403";

    public final static String USER_BUSINESS_RESCUE_PASSWORD_SUCCESS                                             = "USER_RESCUE_PASSWORD_200";

    public final static String USER_BUSINESS_RESCUE_PASSWORD_INVALID_TICKET                                      = "USER_REQU_401";

    public final static String USER_BUSINESS_RESCUE_PASSWORD_UNAUTHORIZED                                        = "USER_REQU_403";

    public final static String USER_BUSINESS_RESCUE_PASSWORD_NOT_EXISTS                                          = "USER_BUSINESS_RESCUE_PASSWORD_501";

    public final static String USER_BUSINESS_CLONE_PAYMENT_METHOD_SUCCESS                                        = "USER_BUSINESS_CLONE_PAYMENT_METHOD_200";

    public final static String USER_BUSINESS_CLONE_PAYMENT_METHOD_INVALID_TICKET                                 = "USER_REQU_401";

    public final static String USER_BUSINESS_CLONE_PAYMENT_METHOD_INVALID_SOURCE_TOKEN                           = "USER_BUSINESS_CLONE_PAYMENT_METHOD_300";

    public final static String USER_BUSINESS_CLONE_PAYMENT_METHOD_INVALID_PIN                                    = "USER_BUSINESS_CLONE_PAYMENT_METHOD_302";

    public final static String USER_BUSINESS_CLONE_PAYMENT_METHOD_CREDIT_CARD_NOT_FOUND                          = "USER_BUSINESS_CLONE_PAYMENT_METHOD_301";

    public final static String USER_REQUEST_RESEND_PIN_SUCCESS                                                   = "USER_REQUEST_RESEND_PIN_200";

    public final static String USER_REQUEST_RESEND_PIN_INVALID_TICKET                                            = "USER_REQU_401";

    public final static String USER_REQUEST_RESEND_PIN_UNAUTHORIZED                                              = "USER_REQU_403";

    public final static String USER_REQUEST_RESEND_PIN_FAILURE                                                   = "USER_REQUEST_RESEND_PIN_300";

    public final static String USER_BUSINESS_CREATE_INVALID_SOURCE_TOKEN                                         = "USER_BUSINESS_CREATE_300";

    public final static String USER_BUSINESS_CREATE_INVALID_SOURCE_USER_FISCALCODE                               = "USER_BUSINESS_CREATE_301";

    public final static String USER_BUSINESS_CREATE_INVALID_SOURCE_USER_EMAIL                                    = "USER_BUSINESS_CREATE_302";

    public final static String USER_BUSINESS_CREATE_INVALID_VAT_NUMBER                                           = "USER_BUSINESS_CHECK_302";

    public final static String USER_BUSINESS_CREATE_INVALID_PROVINCE                                             = "USER_BUSINESS_CHECK_305";

    public final static String USER_BUSINESS_AUTH_INVALID_ENISTATION_USER                                        = "USER_BUSINESS_AUTH_300";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_SUCCESS                                      = "USER_MULTICARD_INSPAY_200";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_FAILED                                       = "USER_MULTICARD_INSPAY_300";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_NOT_FOUND                                    = "USER_MULTICARD_INSPAY_404";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_CONNECTION_ERROR                             = "USER_MULTICARD_INSPAY_405";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_INVALID_PIN                                  = "USER_MULTICARD_INSPAY_406";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_WRONG_PIN                                    = "USER_MULTICARD_INSPAY_407";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_UNAUTHORIZED                                 = "USER_REQU_403";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_INVALID_TICKET                               = "USER_REQU_401";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_PIN_EXISTS                                   = "USER_MULTICARD_INSPAY_408";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_ERROR_PIN_ATTEMPTS_LEFT                      = "USER_MULTICARD_INSPAY_409";

    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_ERROR_PIN_LAST_ATTEMPT                       = "USER_MULTICARD_INSPAY_410";
    
    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_EXISTS                                       = "USER_MULTICARD_INSPAY_411";

    public final static String MULTICARD_STATUS_NOTIFICATION_SUCCESS                                             = "MESSAGE_RECEIVED_200";
    
    public final static String MULTICARD_STATUS_NOTIFICATION_USER_NOT_FOUND                                      = "USER_NOT_FOUND_404";
    
    public final static String MULTICARD_STATUS_NOTIFICATION_DPAN_NOT_FOUND                                      = "DPAN_NOT_FOUND_404";

    public final static String MULTICARD_STATUS_NOTIFICATION_FAILURE                                             = "INTERNAL_ERROR_500";

    public final static String MULTICARD_STATUS_NOTIFICATION_INVALID_REQUEST                                     = "ERROINTERNAL_ERROR_500R";

    public final static String USER_V2_EXTERNAL_AUTH_SUCCESS                                                     = "EXTERNAL_USER_AUTH_200";

    public final static String USER_V2_EXTERNAL_AUTH_FAILURE                                                     = "EXTERNAL_USER_AUTH_300";

    public final static String USER_V2_EXTERNAL_AUTH_INVALID_TICKET                                              = "USER_REQU_401";

    public final static String USER_V2_EXTERNAL_AUTH_LOGIN_ERROR                                                 = "EXTERNAL_USER_AUTH_400";

    public final static String USER_V2_EXTERNAL_AUTH_MISSING_EMAIL                                               = "EXTERNAL_USER_AUTH_401";

    public final static String USER_V2_EXTERNAL_AUTH_WRONG_EXTERNAL                                              = "EXTERNAL_USER_AUTH_402";

    public final static String USER_V2_EXTERNAL_AUTH_NEW_USER                                                    = "EXTERNAL_USER_AUTH_201";
    
    public final static String USER_SET_ACTIVE_DPAN_SUCCESS                                                      = "USER_SET_ACTIVE_DPAN_200";
    
    public final static String USER_SET_ACTIVE_DPAN_FAILURE                                                      = "USER_SET_ACTIVE_DPAN_300";
    
    public final static String USER_SET_ACTIVE_DPAN_INVALID_TICKET                                               = "USER_REQU_401";
    
    public final static String ADMIN_COUNT_PENDING_TRANSACTIONS_INVALID_TICKET                                   = "ADMIN_REQU_401";
    
    public final static String ADMIN_COUNT_PENDING_TRANSACTIONS_SUCCESS                                          = "ADMIN_COUNT_PENDING_TRANSACTIONS_200";
    
    
    public ResponseHelper() {

    }
}
