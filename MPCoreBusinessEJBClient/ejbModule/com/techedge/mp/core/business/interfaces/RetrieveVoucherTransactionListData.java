package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.voucher.VoucherTransaction;

public class RetrieveVoucherTransactionListData implements Serializable {

    /**
	 * 
	 */
    private static final long        serialVersionUID       = 3307851412746064243L;
    private String                   statusCode;
    private List<VoucherTransaction> voucherTransactionList = new ArrayList<VoucherTransaction>(0);

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<VoucherTransaction> getVoucherTransactionList() {
        return voucherTransactionList;
    }

    public void setVoucherTransactionList(List<VoucherTransaction> voucherTransactionList) {
        this.voucherTransactionList = voucherTransactionList;
    }

}
