package com.techedge.mp.core.business.interfaces.loyalty;


public enum ProductIdType {
    GG("11"),
    SP("24"),
    BD("32"),
    GP("26"),
    BS("29"),
    MT("39");
    
    private String code;
    
    
    private ProductIdType(String code) {
        this.code = code;
    }

    public static ProductIdType getProduct(String value) {
        for (ProductIdType product : ProductIdType.values()) {
            if (product.getName().equals(value) || product.getCode().equals(value)) {
                return product;
            }
        }
        
        return null;
    }
    
    
    public String getName() {
        return name();
    }
    
    
    public String getCode() {
        return code;
    }
}
