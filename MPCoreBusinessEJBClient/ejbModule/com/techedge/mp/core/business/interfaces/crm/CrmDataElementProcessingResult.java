package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;

public class CrmDataElementProcessingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4550950926962536917L;

    private final String fieldSeparator = "|";

    public CrmDataElementProcessingResult() {

    }

    public String codFiscale;

    public String categoryDesc;

    public String deliveryId;

    public String cdCliente;

    public String cdCarta;

    public String codIniziativa;

    public String codOfferta;

    public String treatmentCode;

    public String canale;

    public String tmsInserimento;

    public String toString() {

        String output = this.codFiscale + this.fieldSeparator + this.categoryDesc + this.fieldSeparator + this.deliveryId + this.fieldSeparator + this.cdCliente
                + this.fieldSeparator + this.cdCarta + this.fieldSeparator + this.codIniziativa + this.fieldSeparator + this.codOfferta + this.fieldSeparator + this.treatmentCode
                + this.fieldSeparator + this.canale + this.fieldSeparator + this.tmsInserimento;

        return output;
    }
}
