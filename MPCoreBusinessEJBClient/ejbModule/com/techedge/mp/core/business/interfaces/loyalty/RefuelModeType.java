package com.techedge.mp.core.business.interfaces.loyalty;


public enum RefuelModeType {
    Servito("S"),
    Iperself("F");
    
    private String code;
    
    
    private RefuelModeType(String code) {
        this.code = code;
    }

    public static RefuelModeType getRefuelMode(String value) {
        for (RefuelModeType refuelModeType : RefuelModeType.values()) {
            if (refuelModeType.getName().equals(value) || refuelModeType.getCode().equals(value)) {
                return refuelModeType;
            }
        }
        
        return null;
    }
    
    
    public String getName() {
        return name();
    }
    
    public String getCode() {
        return code;
    }
}
