package com.techedge.mp.core.business.interfaces.crm;

public enum CommandType {
    GET_OFFERS("getOffers"),
    GET_OFFERS_FOR_MULTIPLE_INTERACTION_POINTS("getOffersForMultipleInteractionPoints"),
    START_SESSION("startSession"),
    END_SESSION("endSession"),
    POST_EVENT("postEvent");
    
    
    private String command;
    
    private CommandType(String command) {
        this.command = command;
    }
    
    public static CommandType getCommand(String value) {
        for (CommandType commandType : CommandType.values()) {
            if (commandType.getName().equals(value) || commandType.getValue().equals(value)) {
                return commandType;
            }
        }
        
        return null;
    }
    
    
    public String getName() {
        return name();
    }
    
    public String getValue() {
        return command;
    }
    
}
