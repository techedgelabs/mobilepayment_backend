package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class RedemptionAwardResponse implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 5299456291709189263L;
    private String            statusCode;
    private String            voucher;
    private String            messageCode;
    private Integer           checkPinAttemptsLeft;
    private Long              orderId;
    

    public Integer getCheckPinAttemptsLeft() {
    
        return checkPinAttemptsLeft;
    }

    public void setCheckPinAttemptsLeft(Integer checkPinAttemptsLeft) {
    
        this.checkPinAttemptsLeft = checkPinAttemptsLeft;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getVoucher() {
    
        return voucher;
    }

    public void setVoucher(String voucher) {
    
        this.voucher = voucher;
    }

    public String getMessageCode() {
    
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
    
        this.messageCode = messageCode;
    }

    public Long getOrderId() {
    
        return orderId;
    }

    public void setOrderId(Long orderId) {
    
        this.orderId = orderId;
    }

}
