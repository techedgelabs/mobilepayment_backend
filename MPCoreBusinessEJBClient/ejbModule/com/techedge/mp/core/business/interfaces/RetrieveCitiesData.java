package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveCitiesData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2188655080578344888L;
	
	private String statusCode;
	private List<CityInfo> cities = new ArrayList<CityInfo>(0);
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public List<CityInfo> getCities() {
		return cities;
	}
	public void setCities(List<CityInfo> cities) {
		this.cities = cities;
	}
}
