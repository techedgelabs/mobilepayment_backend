package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class ProductStatisticsInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8641639348460566382L;

    private String            productDescription;

    private Double            quantity;

    private Double            previousQuantity;
    
    private Double            amount;

    private Double            previousAmount;

    public String getProductDescription() {

        return productDescription;
    }

    public void setProductDescription(String productDescription) {

        this.productDescription = productDescription;
    }

    public Double getQuantity() {

        return quantity;
    }

    public void setQuantity(Double quantity) {

        this.quantity = quantity;
    }

    public Double getPreviousQuantity() {

        return previousQuantity;
    }

    public void setPreviousQuantity(Double previousQuantity) {

        this.previousQuantity = previousQuantity;
    }

    public Double getAmount() {
    
        return amount;
    }

    public void setAmount(Double amount) {
    
        this.amount = amount;
    }

    public Double getPreviousAmount() {
    
        return previousAmount;
    }

    public void setPreviousAmount(Double previousAmount) {
    
        this.previousAmount = previousAmount;
    }

}
