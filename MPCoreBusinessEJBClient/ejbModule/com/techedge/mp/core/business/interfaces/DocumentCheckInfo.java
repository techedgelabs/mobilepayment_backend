package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class DocumentCheckInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5368791347752378078L;
	
	private String id;
	private Integer position;
	private boolean mandatory;
	private String conditionText;
	private String subTitle;
	private boolean accepted;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	
	public String getConditionText() {
		return conditionText;
	}
	public void setConditionText(String conditionText) {
		this.conditionText = conditionText;
	}
	
    public boolean isAccepted() {
        return accepted;
    }
    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }
    
    public String getSubTitle() {
    
        return subTitle;
    }
    public void setSubTitle(String subTitle) {
    
        this.subTitle = subTitle;
    }
}
