package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class MulticardPaymentResponse implements Serializable {

 
    /**
     * 
     */
    private static final long serialVersionUID = -8722702307749546496L;
    private String            statusCode;
    private String            operationID;
    private Long              paymentMethodId;
    private String            paymentMethodType;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getOperationID() {
    
        return operationID;
    }

    public void setOperationID(String operationID) {
    
        this.operationID = operationID;
    }

    public String getPaymentMethodType() {
    
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
    
        this.paymentMethodType = paymentMethodType;
    }

    public Long getPaymentMethodId() {
    
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
    
        this.paymentMethodId = paymentMethodId;
    }
    
    
}
