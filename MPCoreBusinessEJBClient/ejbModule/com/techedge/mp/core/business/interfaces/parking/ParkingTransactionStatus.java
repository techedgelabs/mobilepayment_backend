package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;
import java.util.Date;

public class ParkingTransactionStatus implements Serializable {

    /**
     * 
     */
    private static final long  serialVersionUID = -6031039353281728189L;

    private long               id;

    private ParkingTransaction parkingTransaction;

    private Integer            sequenceID;

    private Date               timestamp;

    private String             status;

    private String             subStatus;

    private String             subStatusDescription;

    private String             requestID;

    private String             level;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public ParkingTransaction getParkingTransaction() {

        return parkingTransaction;
    }

    public void setParkingTransaction(ParkingTransaction parkingTransaction) {

        this.parkingTransaction = parkingTransaction;
    }

    public Integer getSequenceID() {

        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {

        this.sequenceID = sequenceID;
    }

    public Date getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(Date timestamp) {

        this.timestamp = timestamp;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getSubStatus() {

        return subStatus;
    }

    public void setSubStatus(String subStatus) {

        this.subStatus = subStatus;
    }

    public String getSubStatusDescription() {

        return subStatusDescription;
    }

    public void setSubStatusDescription(String subStatusDescription) {

        this.subStatusDescription = subStatusDescription;
    }

    public String getRequestID() {

        return requestID;
    }

    public void setRequestID(String requestID) {

        this.requestID = requestID;
    }

    public String getLevel() {

        return level;
    }

    public void setLevel(String level) {

        this.level = level;
    }

}
