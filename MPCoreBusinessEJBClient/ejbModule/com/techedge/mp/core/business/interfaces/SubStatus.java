package com.techedge.mp.core.business.interfaces;

public class SubStatus {

	private String subStatusID;
	private String subStatusDescription;
	private Status status;	
	
	public final String getSubStatusID() {
		return subStatusID;
	}
	public final void setSubStatusID(String subStatusID) {
		this.subStatusID = subStatusID;
	}
	
	public final String getSubStatusDescription() {
		return subStatusDescription;
	}
	public final void setSubStatusDescription(String subStatusDescription) {
		this.subStatusDescription = subStatusDescription;
	}
	
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
}
