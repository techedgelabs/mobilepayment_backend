package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.regex.Pattern;

public class CrmSfDataElement implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7598677101265693152L;
    
    private static final String CD_CLIENTE_BROADCAST = "9999999";

    private final String   fieldSeparator = "|";

    //private static String[] fieldList      = { "Contact_code_c", "Card_code_c", "First_name_c", "Last_name_c", "Phone_c", "Email_c", "Channel_c", "Initiative_Code_c",
    //        "Offert_code_c", "Id", "Fiscal_code_c", "CreatedDate", "Type_c", "Voucher_code_c", "Voucher_amount_c", "Parameter1_c", "Parameter2_c", "Parameter3_c", "Parameter4_c"};

    private static String[] fieldList      = { "contactCode", "firstName", "lastName", "phone", "email", "channel", "initiativeCode",
        "offertCode", "id", "fiscalCode", "createDate", "type", "voucherCode", "voucherAmount", "parameter1", "parameter2", "parameter3", "parameter4"};

    public CrmSfDataElement() {

    }
    
    public Boolean isBroadcastType() {
        
        if (this.contactCode.equals(CrmSfDataElement.CD_CLIENTE_BROADCAST)) {
            return Boolean.TRUE;
        }
        else {
            return Boolean.FALSE;
        }
    }

    public Boolean process(String line) {

        String fields[] = line.split(Pattern.quote(fieldSeparator));
        for (int i = 0; i < fields.length; i++) {

            String fieldName = fieldList[i];
            Field f1;
            try {
                f1 = this.getClass().getField(fieldName);
                f1.set(this, fields[i]);
            }
            catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            catch (SecurityException e) {
                e.printStackTrace();
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return Boolean.TRUE;
    }

    public String contactCode;

    public String cardCode;

    public String firstName;

    public String lastName;

    public String phone;

    public String email;

    public String channel;

    public String initiativeCode;

    public String offertCode;

    public String id;

    public String fiscalCode;

    public String createDate;

    public String type;

    public String voucherCode;

    public String voucherAmount;

    public String parameter1;

    public String parameter2;

    public String parameter3;

    public String parameter4;

}
