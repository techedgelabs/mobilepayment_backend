package com.techedge.mp.core.business.interfaces;

public enum AdminRoleEnum {

    ROLE_SUPERADMIN("ROLE_SUPERADMIN"), 
    ROLE_REFUELING_MANAGER("ROLE_REFUELING_MANAGER"),
    ROLE_REMOTE_SYSTEM_CHECK("ROLE_REMOTE_SYSTEM_CHECK");

    private String role;

    private AdminRoleEnum(String role) {

        this.role = role;
    }

    public String getRole() {

        return role;
    }

    public void setRole(String role) {

        this.role = role;
    }

}
