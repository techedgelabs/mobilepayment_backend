package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class CreateApplePayRefuelResponse implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 7504943511307366455L;
    private String statusCode; 
	private String tokenValue;
	private String transactionID;
	private String shopLogin;
	private String acquirerID;
	private String currency;
	private String paymentType;
	private Double maxAmount;
	private Double thresholdAmount; 
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getTokenValue() {
		return tokenValue;
	}
	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	public String getShopLogin() {
		return shopLogin;
	}
	public void setShopLogin(String shopLogin) {
		this.shopLogin = shopLogin;
	}
	
	public String getAcquirerID() {
		return acquirerID;
	}
	public void setAcquirerID(String acquirerID) {
		this.acquirerID = acquirerID;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	public Double getMaxAmount() {
    
        return maxAmount;
    }
    public void setMaxAmount(Double maxAmount) {
    
        this.maxAmount = maxAmount;
    }
    
    public Double getThresholdAmount() {
    
        return thresholdAmount;
    }
    public void setThresholdAmount(Double thresholdAmount) {
    
        this.thresholdAmount = thresholdAmount;
    }
}
