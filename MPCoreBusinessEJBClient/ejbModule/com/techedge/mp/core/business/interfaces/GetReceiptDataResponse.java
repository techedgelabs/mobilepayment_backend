package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class GetReceiptDataResponse implements Serializable {

    /**
     * 
     */
    private static final long    serialVersionUID = -7525978868180582909L;

    private String               statusCode;

    private GetReceiptDataDetail getReceiptDataDetail;// = new GetReceiptDataDetail();

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public GetReceiptDataDetail getGetReceiptDataDetail() {

        return getReceiptDataDetail;
    }

    public void setGetReceiptDataDetail(GetReceiptDataDetail getReceiptDataDetail) {

        this.getReceiptDataDetail = getReceiptDataDetail;
    }

}
