package com.techedge.mp.core.business.interfaces.pushnotification;


public enum PushNotificationSourceType {

    CRM_INBOUND,
    CRM_OUTBOUND,
    LOYALTY,
    FUNCTION,
    CRM_ES;
    
}
