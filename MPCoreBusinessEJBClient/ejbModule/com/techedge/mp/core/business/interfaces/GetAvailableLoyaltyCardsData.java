package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetAvailableLoyaltyCardsData implements Serializable {

    private static final long serialVersionUID = -2603449863869190424L;
    private String statusCode;
    private List<LoyaltyCard> loyaltyCards     = new ArrayList<LoyaltyCard>(0);
    private Integer balance;

    public List<LoyaltyCard> getLoyaltyCards() {
        return loyaltyCards;
    }

    public void setLoyaltyCards(List<LoyaltyCard> loyaltyCards) {
        this.loyaltyCards = loyaltyCards;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
