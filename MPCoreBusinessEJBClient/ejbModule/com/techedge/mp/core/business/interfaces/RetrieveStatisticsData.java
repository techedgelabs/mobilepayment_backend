package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveStatisticsData implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -544338837008405278L;
	
	private String statusCode;
	private List<StatisticInfo> statisticsList = new ArrayList<StatisticInfo>(0);
	
	
	public RetrieveStatisticsData() {
		
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public List<StatisticInfo> getStatisticsList() {
		return statisticsList;
	}

	public void setStatisticsList(List<StatisticInfo> statisticsList) {
		this.statisticsList = statisticsList;
	}
	
	
}
