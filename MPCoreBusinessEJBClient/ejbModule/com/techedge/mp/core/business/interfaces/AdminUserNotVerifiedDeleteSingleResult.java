package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class AdminUserNotVerifiedDeleteSingleResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5626487222339332053L;

    private Long              id;

    private String            message;

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }

}
