package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;


public class ReceiptData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6245103661282230811L;
	
	private String label;
	private String unit;
	private String key;
	private String value;
	private Integer position;
	private String parent;
	private String link;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
}
