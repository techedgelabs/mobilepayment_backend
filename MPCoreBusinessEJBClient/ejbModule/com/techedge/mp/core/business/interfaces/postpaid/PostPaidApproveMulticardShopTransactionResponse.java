package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;

public class PostPaidApproveMulticardShopTransactionResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6284030530688342739L;

    private String            statusCode;

    private String            messageCode;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getMessageCode() {

        return messageCode;
    }

    public void setMessageCode(String messageCode) {

        this.messageCode = messageCode;
    }

}
