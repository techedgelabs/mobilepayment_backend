package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;
import java.util.Date;

public class EstimateParkingPriceResult extends BaseParkingResult implements Serializable {

    /**
     * 
     */
    private static final long         serialVersionUID = -5882483253739906037L;

    private Date                      requestedEndTime;
    private String parkingTimeCorrection;

    public Date getRequestedEndTime() {
        return requestedEndTime;
    }

    public void setRequestedEndTime(Date requestedEndTime) {
        this.requestedEndTime = requestedEndTime;
    }

    public String getParkingTimeCorrection() {
        return parkingTimeCorrection;
    }

    public void setParkingTimeCorrection(String parkingTimeCorrection) {
        this.parkingTimeCorrection = parkingTimeCorrection;
    }

}
