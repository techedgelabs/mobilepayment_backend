package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class PostPaidLoadLoyaltyCreditsVoucher implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3815408322390470975L;

    private long               id;

    private String             code;

    private PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits;

    private String             status;

    private String             type;

    private Double             value;

    private Double             initialValue;

    private Double             consumedValue;

    private Double             voucherBalanceDue;

    private Date               expirationDate;

    private String             promoCode;

    private String             promoDescription;

    private String             promoDoc;

    private Double             minQuantity;

    private Double             minAmount;

    private String             validPV;

    private String             isCombinable;

    private String             promoPartner;

    private String             icon;

    public PostPaidLoadLoyaltyCreditsVoucher() {

    }

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public PostPaidLoadLoyaltyCredits getPostPaidLoadLoyaltyCredits() {
    
        return postPaidLoadLoyaltyCredits;
    }

    public void setPostPaidLoadLoyaltyCredits(PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits) {
    
        this.postPaidLoadLoyaltyCredits = postPaidLoadLoyaltyCredits;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public Double getValue() {

        return value;
    }

    public void setValue(Double value) {

        this.value = value;
    }

    public Double getInitialValue() {

        return initialValue;
    }

    public void setInitialValue(Double initialValue) {

        this.initialValue = initialValue;
    }

    public Double getConsumedValue() {

        return consumedValue;
    }

    public void setConsumedValue(Double consumedValue) {

        this.consumedValue = consumedValue;
    }

    public Double getVoucherBalanceDue() {

        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Double voucherBalanceDue) {

        this.voucherBalanceDue = voucherBalanceDue;
    }

    public Date getExpirationDate() {

        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {

        this.expirationDate = expirationDate;
    }

    public String getPromoCode() {

        return promoCode;
    }

    public void setPromoCode(String promoCode) {

        this.promoCode = promoCode;
    }

    public String getPromoDescription() {

        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {

        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {

        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {

        this.promoDoc = promoDoc;
    }

    public Double getMinQuantity() {

        return minQuantity;
    }

    public void setMinQuantity(Double minQuantity) {

        this.minQuantity = minQuantity;
    }

    public Double getMinAmount() {

        return minAmount;
    }

    public void setMinAmount(Double minAmount) {

        this.minAmount = minAmount;
    }

    public String getValidPV() {

        return validPV;
    }

    public void setValidPV(String validPV) {

        this.validPV = validPV;
    }

    public String getIsCombinable() {

        return isCombinable;
    }

    public void setIsCombinable(String isCombinable) {

        this.isCombinable = isCombinable;
    }

    public String getPromoPartner() {

        return promoPartner;
    }

    public void setPromoPartner(String promoPartner) {

        this.promoPartner = promoPartner;
    }

    public String getIcon() {

        return icon;
    }

    public void setIcon(String icon) {

        this.icon = icon;
    }

}
