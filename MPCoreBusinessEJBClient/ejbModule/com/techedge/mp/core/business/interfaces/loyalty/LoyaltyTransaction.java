package com.techedge.mp.core.business.interfaces.loyalty;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class LoyaltyTransaction implements Serializable {

    /**
     * 
     */
    private static final long                 serialVersionUID = -6947342787701334203L;

    private String                            paymentMode;


    private HashSet<LoyaltyTransactionRefuel> refuelDetails    = new HashSet<LoyaltyTransactionRefuel>(0);

    private HashSet<LoyaltyTransactionNonOil> nonOilDetails    = new HashSet<LoyaltyTransactionNonOil>(0);

    public LoyaltyTransaction() {

    }

    public String getPaymentMode() {

        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {

        this.paymentMode = paymentMode;
    }

    public HashSet<LoyaltyTransactionRefuel> getRefuelDetails() {

        return refuelDetails;
    }

    public void setRefuelDetails(HashSet<LoyaltyTransactionRefuel> refuelDetails) {

        this.refuelDetails = refuelDetails;
    }

    public Set<LoyaltyTransactionNonOil> getNonOilDetails() {

        return nonOilDetails;
    }

    public void setNonOilDetails(HashSet<LoyaltyTransactionNonOil> nonOilDetails) {

        this.nonOilDetails = nonOilDetails;
    }

}
