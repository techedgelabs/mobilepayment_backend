package com.techedge.mp.core.business.interfaces;

public class Status {
	
	private String statusID;
	private String statusDescription;
	private String statusType;
	
	public final String getStatusID() {
		return statusID;
	}
	public final void setStatusID(String statusID) {
		this.statusID = statusID;
	}
	
	public final String getStatusDescription() {
		return statusDescription;
	}
	public final void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	
	public final String getStatusType() {
		return statusType;
	}
	public final void setStatusType(String statusType) {
		this.statusType = statusType;
	}
}
