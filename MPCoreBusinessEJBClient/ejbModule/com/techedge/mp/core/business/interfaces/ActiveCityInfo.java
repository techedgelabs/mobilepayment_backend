package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class ActiveCityInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1243458919700720859L;
    
    private String city;
    private String province;
    private String country;
    private String latitude;
    private String longitude;
    private Integer stationCount;
    private Boolean currentCity;
    
    public String getCity() {
        return city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getProvince() {
        return province;
    }
    
    public void setProvince(String province) {
        this.province = province;
    }
    
    public String getCountry() {
        return country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    
    public String getLatitude() {
        return latitude;
    }
    
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    
    public String getLongitude() {
        return longitude;
    }
    
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    
    public Integer getStationCount() {
        return stationCount;
    }
    
    public void setStationCount(Integer stationCount) {
        this.stationCount = stationCount;
    }
    
    public Boolean getCurrentCity() {
        return currentCity;
    }
    
    public void setCurrentCity(Boolean currentCity) {
        this.currentCity = currentCity;
    }
    
}
