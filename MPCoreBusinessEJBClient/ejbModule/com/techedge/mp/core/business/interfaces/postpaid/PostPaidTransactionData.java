package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.user.User;

public class PostPaidTransactionData implements Serializable{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 23070955314870942L;
	private String transactionID;
	private String requestID;
	private String source;
	private String sourceID;
	private String srcTransactionID;
	private String mpTransactionID;
	private String bankTansactionID;
	private String authorizationCode;
	private String token;
	private String paymentType;
	private Double amount;
	private String currency;
	private String productType;  // OIL||NON_OIL||MIXED
	private Set<PostPaidCartData> cartData = new HashSet<PostPaidCartData>(0);  
	private Set<PostPaidRefuelData> refuelData = new HashSet<PostPaidRefuelData>(0);  
	private String statusType;  //Reconciliation-Standard
	private String mpTransactionStatus;
	private String srcTransactionStatus;
	private Date creationTimestamp;
	private Date lastModifyTimestamp;	
	private Set<PostPaidTransactionEventData> postPayedTransactionEventData = new HashSet<PostPaidTransactionEventData>(0);
	private Set<PostPaidTransactionPaymentEventData> postPayedTransactionPaymentEventData = new HashSet<PostPaidTransactionPaymentEventData>(0);
	private String shopLogin;
	private String acquirerID;
	private String paymentMode;
	private Long paymentMethodId;
	private String paymentMethodType;
	private Boolean notificationCreated;
	private Boolean notificationPaid;
	private Boolean notificationUser;
	private User userData;
	private Station stationData;
	

	public PostPaidTransactionData() {}


	public String getTransactionID() {
		return transactionID;
	}


	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}


	public String getRequestID() {
		return requestID;
	}


	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}


	public String getSource() {
		return source;
	}


	public void setSource(String source) {
		this.source = source;
	}


	public String getSourceID() {
		return sourceID;
	}


	public void setSourceID(String sourceID) {
		this.sourceID = sourceID;
	}


	public String getSrcTransactionID() {
		return srcTransactionID;
	}


	public void setSrcTransactionID(String srcTransactionID) {
		this.srcTransactionID = srcTransactionID;
	}


	public String getMpTransactionID() {
		return mpTransactionID;
	}


	public void setMpTransactionID(String mpTransactionID) {
		this.mpTransactionID = mpTransactionID;
	}


	public String getBankTansactionID() {
		return bankTansactionID;
	}


	public void setBankTansactionID(String bankTansactionID) {
		this.bankTansactionID = bankTansactionID;
	}


	public String getAuthorizationCode() {
		return authorizationCode;
	}


	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public String getPaymentType() {
		return paymentType;
	}


	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getProductType() {
		return productType;
	}


	public void setProductType(String productType) {
		this.productType = productType;
	}


	public Set<PostPaidCartData> getCartData() {
		return cartData;
	}


	public void setCartData(Set<PostPaidCartData> cartData) {
		this.cartData = cartData;
	}


	public Set<PostPaidRefuelData> getRefuelData() {
		return refuelData;
	}


	public void setRefuelData(Set<PostPaidRefuelData> refuelData) {
		this.refuelData = refuelData;
	}


	public String getStatusType() {
		return statusType;
	}


	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}


	public String getMpTransactionStatus() {
		return mpTransactionStatus;
	}


	public void setMpTransactionStatus(String mpTransactionStatus) {
		this.mpTransactionStatus = mpTransactionStatus;
	}


	public String getSrcTransactionStatus() {
		return srcTransactionStatus;
	}


	public void setSrcTransactionStatus(String srcTransactionStatus) {
		this.srcTransactionStatus = srcTransactionStatus;
	}


	public Date getCreationTimestamp() {
		return creationTimestamp;
	}


	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}


	public Date getLastModifyTimestamp() {
		return lastModifyTimestamp;
	}


	public void setLastModifyTimestamp(Date lastModifyTimestamp) {
		this.lastModifyTimestamp = lastModifyTimestamp;
	}


	public Set<PostPaidTransactionEventData> getPostPayedTransactionEventData() {
		return postPayedTransactionEventData;
	}


	public void setPostPayedTransactionEventData(
			Set<PostPaidTransactionEventData> postPayedTransactionEventData) {
		this.postPayedTransactionEventData = postPayedTransactionEventData;
	}


	public Set<PostPaidTransactionPaymentEventData> getPostPayedTransactionPaymentEventData() {
		return postPayedTransactionPaymentEventData;
	}


	public void setPostPayedTransactionPaymentEventData(
			Set<PostPaidTransactionPaymentEventData> postPayedTransactionPaymentEventData) {
		this.postPayedTransactionPaymentEventData = postPayedTransactionPaymentEventData;
	}


	public String getShopLogin() {
		return shopLogin;
	}


	public void setShopLogin(String shopLogin) {
		this.shopLogin = shopLogin;
	}


	public String getAcquirerID() {
		return acquirerID;
	}


	public void setAcquirerID(String acquirerID) {
		this.acquirerID = acquirerID;
	}


	public String getPaymentMode() {
		return paymentMode;
	}


	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}


	public Long getPaymentMethodId() {
		return paymentMethodId;
	}


	public void setPaymentMethodId(Long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}


	public String getPaymentMethodType() {
		return paymentMethodType;
	}


	public void setPaymentMethodType(String paymentMethodType) {
		this.paymentMethodType = paymentMethodType;
	}


	public Boolean getNotificationCreated() {
		return notificationCreated;
	}


	public void setNotificationCreated(Boolean notificationCreated) {
		this.notificationCreated = notificationCreated;
	}


	public Boolean getNotificationPaid() {
		return notificationPaid;
	}


	public void setNotificationPaid(Boolean notificationPaid) {
		this.notificationPaid = notificationPaid;
	}


	public Boolean getNotificationUser() {
		return notificationUser;
	}


	public void setNotificationUser(Boolean notificationUser) {
		this.notificationUser = notificationUser;
	}


	public User getUser() {
		return userData;
	}


	public void setUser(User user) {
		this.userData = user;
	}


	public Station getStationData() {
		return stationData;
	}


	public void setStationData(Station station) {
		this.stationData = station;
	}
	
		
	
}
