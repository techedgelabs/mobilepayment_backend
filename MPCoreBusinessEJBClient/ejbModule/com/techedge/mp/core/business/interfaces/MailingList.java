package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class MailingList implements Serializable {

    public static final Integer MAILING_LIST_PENDING = 0;

    public static final Integer MAILING_LIST_SENT    = 1;

    public static final Integer MAILING_LIST_ERROR   = 2;

    /**
     * 
     */
    private static final long   serialVersionUID     = 9047275302792236437L;

    private long                id;

    private String              email;
    
    private String              name;

    private Integer             status;

    private Date                timestamp;

    private String              template;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getName() {
    
        return name;
    }

    public void setName(String name) {
    
        this.name = name;
    }

    public Integer getStatus() {

        return status;
    }

    public void setStatus(Integer status) {

        this.status = status;
    }

    public Date getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(Date timestamp) {

        this.timestamp = timestamp;
    }

    public String getTemplate() {

        return template;
    }

    public void setTemplate(String template) {

        this.template = template;
    }

}
