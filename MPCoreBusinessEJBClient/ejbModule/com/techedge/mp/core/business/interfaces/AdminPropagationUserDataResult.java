package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class AdminPropagationUserDataResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4471182199010346934L;
    
    private String fiscalCode;
    private String statusCode;
    private String statusMessage;
    
    public String getFiscalCode() {
    
        return fiscalCode;
    }
    public void setFiscalCode(String fiscalCode) {
    
        this.fiscalCode = fiscalCode;
    }
    public String getStatusCode() {
    
        return statusCode;
    }
    public void setStatusCode(String statusCode) {
    
        this.statusCode = statusCode;
    }
    public String getStatusMessage() {
    
        return statusMessage;
    }
    public void setStatusMessage(String statusMessage) {
    
        this.statusMessage = statusMessage;
    }
    
    

}
