package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class TransactionUseVoucherResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 967579865153309724L;

    private String    transactionID;
    private boolean   voucherUsed;
    private Double    initialAmount;
    private Double    voucherTotalAmount;
    private Double    finalAmount;



    public String getTransactionID() {
        return transactionID;
    }
    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }
    public boolean isVoucherUsed() {
        return voucherUsed;
    }
    public void setVoucherUsed(boolean voucherUsed) {
        this.voucherUsed = voucherUsed;
    }
    public Double getInitialAmount() {
        return initialAmount;
    }
    public void setInitialAmount(Double initialAmount) {
        this.initialAmount = initialAmount;
    }
    public Double getVoucherTotalAmount() {
        return voucherTotalAmount;
    }
    public void setVoucherTotalAmount(Double voucherTotalAmount) {
        this.voucherTotalAmount = voucherTotalAmount;
    }
    public Double getFinalAmount() {
        return finalAmount;
    }
    public void setFinalAmount(Double finalAmount) {
        this.finalAmount = finalAmount;
    }


}
