package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PollingIntervalData implements Serializable {
    /**
     * 
     */
    private static final long     serialVersionUID = 833638561032445046L;

    /**
     * 
     */

    private String                statusCode;

    private List<PollingInterval> pollingInterval  = new ArrayList<PollingInterval>(0);

    public PollingIntervalData() {

    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<PollingInterval> getPollingInterval() {

        return pollingInterval;
    }

    public void setPollingInterval(List<PollingInterval> pollingInterval) {

        this.pollingInterval = pollingInterval;
    }

}
