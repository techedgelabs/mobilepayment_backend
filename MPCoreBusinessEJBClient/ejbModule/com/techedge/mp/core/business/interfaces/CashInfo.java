package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class CashInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2932558526948100965L;

	private String cashId;
	private String description;
	private String number;

	
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getCashId() {
		return cashId;
	}
	public void setCashId(String cashId) {
		this.cashId = cashId;
	}
	
	
}
