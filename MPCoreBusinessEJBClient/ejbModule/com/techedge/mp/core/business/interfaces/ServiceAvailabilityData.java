package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class ServiceAvailabilityData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1616119354086884852L;

    private String            statusCode;

    private String            statusMessage;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getStatusMessage() {

        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {

        this.statusMessage = statusMessage;
    }

}
