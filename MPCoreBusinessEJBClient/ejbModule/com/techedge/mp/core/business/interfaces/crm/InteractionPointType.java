package com.techedge.mp.core.business.interfaces.crm;

public enum InteractionPointType {

    REGISTRATION(""),
    LOYALTY_CREDITS(""),
    PROMO4ME_6(""),
    PROMO4ME_5(""),
    PROMO4ME_4(""),
    PROMO4ME_3(""),
    PROMO4ME_2(""),
    PROMO4ME_1(""),
    REFUELING(""),
    CREDIT_CARD(""),
    HPC(""),
    MISSION("");
    
    private String point;
    
    private InteractionPointType(String point) {
        this.point = point;
    }
    
    public void setPoint(String point) {
        this.point = point;
    }
    
    public String getPoint() {
        return point;
    }
}
