package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Response implements Serializable {

    /**
     * 
     */
    private static final long          serialVersionUID = 5456715841046782912L;

    private ArrayList<AdvisoryMessage> advisoryMessages = new ArrayList<AdvisoryMessage>();

    private ArrayList<OfferList>       allOfferLists    = new ArrayList<OfferList>();

    private String                     apiVersion;

    private OfferList                  offerList;

    private HashMap<String, Object>    profileRecord    = new HashMap<String, Object>();

    private String                     sessionID;

    private StatusCode                 statusCode;

    private HashMap<String, Object>    eventParameters  = new HashMap<String, Object>();

    
    public ArrayList<AdvisoryMessage> getAdvisoryMessages() {

        return advisoryMessages;
    }

    public void setAdvisoryMessages(ArrayList<AdvisoryMessage> advisoryMessages) {

        this.advisoryMessages = advisoryMessages;
    }

    public ArrayList<OfferList> getAllOfferLists() {

        return allOfferLists;
    }

    public void setAllOfferLists(ArrayList<OfferList> allOfferLists) {

        this.allOfferLists = allOfferLists;
    }

    public String getApiVersion() {

        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {

        this.apiVersion = apiVersion;
    }

    public OfferList getOfferList() {

        return offerList;
    }

    public void setOfferList(OfferList offerList) {

        this.offerList = offerList;
    }

    public HashMap<String, Object> getProfileRecord() {

        return profileRecord;
    }

    public void setProfileRecord(HashMap<String, Object> profileRecord) {

        this.profileRecord = profileRecord;
    }

    public String getSessionID() {

        return sessionID;
    }

    public void setSessionID(String sessionID) {

        this.sessionID = sessionID;
    }

    public StatusCode getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {

        this.statusCode = statusCode;
    }

    public HashMap<String, Object> getEventParameters() {

        return eventParameters;
    }
    /*
    public String getEventParametersToString(String separator) {

        StringBuffer sb = new StringBuffer();
        boolean isFirst = true;
        
        for (String key : eventParameters.keySet()) {
            if (!isFirst) {
                sb.append(separator);
            }
            
            sb.append(key + ":" + eventParameters.get(key));
            
            if (isFirst) {
                isFirst = false;
            }
        }
        
        return sb.toString();
    }
    */
    public void setEventParameters(HashMap<String, Object> eventParameters) {

        this.eventParameters = eventParameters;
    }

}
