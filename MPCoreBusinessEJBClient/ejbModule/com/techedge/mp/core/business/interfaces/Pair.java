package com.techedge.mp.core.business.interfaces;

public class Pair<L, R> {

    private L left;

    private R right;

    public Pair(L left, R right) {

        this.left = left;
        this.right = right;
    }

    public L getLeft() {

        return left;
    }

    public R getRight() {

        return right;
    }

    public void setLeft(L left) {

        this.left = left;
    }

    public void setRight(R right) {

        this.right = right;
    }

    public String toString() {

        String leftString = "";
        String rightString = "";

        if (this.left != null) {
            leftString = this.left.toString();
        }

        if (this.right != null) {
            rightString = this.right.toString();
        }

        return "\"" + leftString + "\": \"" + rightString + "\"";
    }

    @Override
    public int hashCode() {

        return toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        if (left.hashCode() != ((Pair) obj).getLeft().hashCode() || right.hashCode() != ((Pair) obj).getRight().hashCode()) {
            return false;
        }

        return true;
    }
}
