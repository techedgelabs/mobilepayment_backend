package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.List;

public class AdminRolesResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5969436220305094256L;

    private String            statusCode;

    private List<String>      adminRoles;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<String> getAdminRoles() {

        return adminRoles;
    }

    public void setAdminRoles(List<String> adminRoles) {

        this.adminRoles = adminRoles;
    }

}
