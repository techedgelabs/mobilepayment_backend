package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class GetPromoPopupDataResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -881288945716534775L;

    private String         statusCode;

    private PromoPopupData promoPopupData;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public PromoPopupData getPromoPopupData() {

        return promoPopupData;
    }

    public void setPromoPopupData(PromoPopupData promoPopupData) {

        this.promoPopupData = promoPopupData;
    }

}
