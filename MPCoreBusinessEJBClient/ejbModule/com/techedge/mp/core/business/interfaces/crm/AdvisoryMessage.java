package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;

public class AdvisoryMessage implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4433313814394101671L;

    private String            detailMessage;

    private String            message;

    private int               messageCode;

    private StatusLevel       statusLevel;

    public String getDetailMessage() {
        return detailMessage;
    }

    public void setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }

    public StatusLevel getStatusLevel() {
        return statusLevel;
    }

    public void setStatusLevel(StatusLevel statusLevel) {
        this.statusLevel = statusLevel;
    }

}
