package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.user.User;

public class TransactionHistory implements Serializable {

    /**
	 * 
	 */
    private static final long              serialVersionUID                  = -1431001047418710543L;

    private long                           id;

    private String                         transactionID;

    private Date                           archivingDate;

    private User                           user;

    private Station                        station;

    private Boolean                        useVoucher;

    private Date                           creationTimestamp;

    private Date                           endRefuelTimestamp;

    private Double                         initialAmount;

    private Double                         finalAmount;

    private Double                         fuelQuantity;

    private Double                         fuelAmount;
    
    private Double                         unitPrice;

    private String                         pumpID;

    private Integer                        pumpNumber;

    private String                         productID;

    private String                         productDescription;

    private String                         currency;

    private String                         shopLogin;

    private String                         acquirerID;

    private String                         bankTansactionID;

    private String                         authorizationCode;

    private String                         token;

    private String                         paymentType;

    private String                         finalStatusType;

    private Boolean                        GFGNotification;

    private Boolean                        confirmed;

    private Set<TransactionStatusHistory>  transactionStatusData             = new HashSet<TransactionStatusHistory>(0);

    private Set<TransactionEventHistory>   transactionEventData              = new HashSet<TransactionEventHistory>(0);

    private Long                           paymentMethodId;

    private String                         paymentMethodType;

    private String                         serverName;

    private Integer                        statusAttemptsLeft;

    private String                         outOfRange;

    private String                         refuelMode;

    private Boolean                        voucherReconciliation;

    private Set<PrePaidConsumeVoucher>     prePaidConsumeVoucherList         = new HashSet<PrePaidConsumeVoucher>(0);

    private Set<PrePaidLoadLoyaltyCredits> prePaidLoadLoyaltyCreditsBeanList = new HashSet<PrePaidLoadLoyaltyCredits>(0);

    private Integer                        reconciliationAttemptsLeft;

    private String                         voucherHistoryStatus;

    private String                         srcTransactionID;

    private String                         encodedSecretKey;

    private String                         groupAcquirer;

    private String                         gfgElectronicInvoiceID;

    private TransactionCategoryType        transactionCategory;
    
    private Set<TransactionHistoryAdditionalData> transactionHistoryAdditionalDataList = new HashSet<TransactionHistoryAdditionalData>(0);

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getTransactionID() {

        return transactionID;
    }

    public void setTransactionID(String transactionID) {

        this.transactionID = transactionID;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public Station getStation() {

        return station;
    }

    public void setStation(Station station) {

        this.station = station;
    }

    public Boolean getUseVoucher() {

        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {

        this.useVoucher = useVoucher;
    }

    public Date getCreationTimestamp() {

        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {

        this.creationTimestamp = creationTimestamp;
    }

    public Date getEndRefuelTimestamp() {

        return endRefuelTimestamp;
    }

    public void setEndRefuelTimestamp(Date endRefuelTimestamp) {

        this.endRefuelTimestamp = endRefuelTimestamp;
    }

    public Double getInitialAmount() {

        return initialAmount;
    }

    public void setInitialAmount(Double initialAmount) {

        this.initialAmount = initialAmount;
    }

    public Double getFinalAmount() {

        return finalAmount;
    }

    public void setFinalAmount(Double finalAmount) {

        this.finalAmount = finalAmount;
    }

    public Double getFuelQuantity() {

        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {

        this.fuelQuantity = fuelQuantity;
    }

    public Double getFuelAmount() {

        return fuelAmount;
    }

    public void setFuelAmount(Double fuelAmount) {

        this.fuelAmount = fuelAmount;
    }

    public String getPumpID() {

        return pumpID;
    }

    public void setPumpID(String pumpID) {

        this.pumpID = pumpID;
    }

    public Integer getPumpNumber() {

        return pumpNumber;
    }

    public void setPumpNumber(Integer pumpNumber) {

        this.pumpNumber = pumpNumber;
    }

    public String getProductID() {

        return productID;
    }

    public void setProductID(String productID) {

        this.productID = productID;
    }

    public String getProductDescription() {

        return productDescription;
    }

    public void setProductDescription(String productDescription) {

        this.productDescription = productDescription;
    }

    public String getCurrency() {

        return currency;
    }

    public void setCurrency(String currency) {

        this.currency = currency;
    }

    public String getShopLogin() {

        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {

        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {

        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {

        this.acquirerID = acquirerID;
    }

    public String getBankTansactionID() {

        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {

        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {

        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {

        this.authorizationCode = authorizationCode;
    }

    public String getToken() {

        return token;
    }

    public void setToken(String token) {

        this.token = token;
    }

    public String getPaymentType() {

        return paymentType;
    }

    public void setPaymentType(String paymentType) {

        this.paymentType = paymentType;
    }

    public String getFinalStatusType() {

        return finalStatusType;
    }

    public void setFinalStatusType(String finalStatusType) {

        this.finalStatusType = finalStatusType;
    }

    public Boolean getGFGNotification() {

        return GFGNotification;
    }

    public void setGFGNotification(Boolean gFGNotification) {

        GFGNotification = gFGNotification;
    }

    public Boolean getConfirmed() {

        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {

        this.confirmed = confirmed;
    }

    public Set<TransactionStatusHistory> getTransactionStatusHistoryData() {

        return transactionStatusData;
    }

    public void setTransactionStatusHistoryData(Set<TransactionStatusHistory> transactionStatusData) {

        this.transactionStatusData = transactionStatusData;
    }

    /**
     * @return the archivingDate
     */
    public Date getArchivingDate() {

        return archivingDate;
    }

    /**
     * @param archivingDate
     *            the archivingDate to set
     */
    public void setArchivingDate(Date archivingDate) {

        this.archivingDate = archivingDate;
    }

    public Set<TransactionEventHistory> getTransactionEventHistoryData() {

        return transactionEventData;
    }

    public void setTransactionEventHistoryData(Set<TransactionEventHistory> transactionEventData) {

        this.transactionEventData = transactionEventData;
    }

    //	public Set<TransactionStatusHistory> getTransactionStatusData() {
    //		return transactionStatusData;
    //	}
    //	public void setTransactionStatusData(
    //			Set<TransactionStatusHistory> transactionStatusData) {
    //		this.transactionStatusData = transactionStatusData;
    //	}

    //	public Set<TransactionEventHistory> getTransactionEventData() {
    //		return transactionEventData;
    //	}
    //	public void setTransactionEventData(
    //			Set<TransactionEventHistory> transactionEventData) {
    //		this.transactionEventData = transactionEventData;
    //	}

    public Long getPaymentMethodId() {

        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {

        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {

        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {

        this.paymentMethodType = paymentMethodType;
    }

    public String getServerName() {

        return serverName;
    }

    public void setServerName(String serverName) {

        this.serverName = serverName;
    }

    public Integer getStatusAttemptsLeft() {

        return statusAttemptsLeft;
    }

    public void setStatusAttemptsLeft(Integer statusAttemptsLeft) {

        this.statusAttemptsLeft = statusAttemptsLeft;
    }

    public String getOutOfRange() {

        return outOfRange;
    }

    public void setOutOfRange(String outOfRange) {

        this.outOfRange = outOfRange;
    }

    public String getRefuelMode() {

        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {

        this.refuelMode = refuelMode;
    }

    public Boolean getVoucherReconciliation() {

        return voucherReconciliation;
    }

    public void setVoucherReconciliation(Boolean voucherReconciliation) {

        this.voucherReconciliation = voucherReconciliation;
    }

    public Set<PrePaidConsumeVoucher> getPrePaidConsumeVoucherList() {

        return prePaidConsumeVoucherList;
    }

    public void setPrePaidConsumeVoucherBeanList(Set<PrePaidConsumeVoucher> prePaidConsumeVoucherList) {

        this.prePaidConsumeVoucherList = prePaidConsumeVoucherList;
    }

    public Integer getReconciliationAttemptsLeft() {

        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {

        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public Set<TransactionStatusHistory> getTransactionStatusData() {

        return transactionStatusData;
    }

    public void setTransactionStatusData(Set<TransactionStatusHistory> transactionStatusData) {

        this.transactionStatusData = transactionStatusData;
    }

    public Set<TransactionEventHistory> getTransactionEventData() {

        return transactionEventData;
    }

    public void setTransactionEventData(Set<TransactionEventHistory> transactionEventData) {

        this.transactionEventData = transactionEventData;
    }

    public String getVoucherHistoryStatus() {

        return voucherHistoryStatus;
    }

    public void setVoucherHistoryStatus(String voucherHistoryStatus) {

        this.voucherHistoryStatus = voucherHistoryStatus;
    }

    public void setPrePaidConsumeVoucherList(Set<PrePaidConsumeVoucher> prePaidConsumeVoucherList) {

        this.prePaidConsumeVoucherList = prePaidConsumeVoucherList;
    }

    public String getSrcTransactionID() {

        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {

        this.srcTransactionID = srcTransactionID;
    }

    public String getEncodedSecretKey() {

        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {

        this.encodedSecretKey = encodedSecretKey;
    }

    public String getGroupAcquirer() {

        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {

        this.groupAcquirer = groupAcquirer;
    }

    public Set<PrePaidLoadLoyaltyCredits> getPrePaidLoadLoyaltyCreditsBeanList() {

        return prePaidLoadLoyaltyCreditsBeanList;
    }

    public void setPrePaidLoadLoyaltyCreditsBeanList(Set<PrePaidLoadLoyaltyCredits> prePaidLoadLoyaltyCreditsBeanList) {

        this.prePaidLoadLoyaltyCreditsBeanList = prePaidLoadLoyaltyCreditsBeanList;
    }

    public static TransactionHistory moveTransaction(Transaction transaction) {

        TransactionHistory transactionHistory = new TransactionHistory();
        transactionHistory.setId(transaction.getId());
        transactionHistory.setAcquirerID(transaction.getAcquirerID());
        transactionHistory.setAuthorizationCode(transaction.getAuthorizationCode());
        transactionHistory.setBankTansactionID(transaction.getBankTansactionID());
        transactionHistory.setConfirmed(transaction.getConfirmed());
        transactionHistory.setCreationTimestamp(transaction.getCreationTimestamp());
        transactionHistory.setCurrency(transaction.getCurrency());
        transactionHistory.setEndRefuelTimestamp(transaction.getEndRefuelTimestamp());
        transactionHistory.setFinalAmount(transaction.getFinalAmount());
        transactionHistory.setFinalStatusType(transaction.getFinalStatusType());
        transactionHistory.setFuelAmount(transaction.getFuelAmount());
        transactionHistory.setFuelQuantity(transaction.getFuelQuantity());
        transactionHistory.setGFGNotification(transaction.getGFGNotification());
        transactionHistory.setInitialAmount(transaction.getInitialAmount());
        transactionHistory.setPaymentMethodId(transaction.getPaymentMethodId());
        transactionHistory.setPaymentType(transaction.getPaymentType());
        transactionHistory.setProductDescription(transaction.getProductDescription());
        transactionHistory.setPumpID(transaction.getPumpID());
        transactionHistory.setProductID(transaction.getProductID());
        transactionHistory.setPumpNumber(transaction.getPumpNumber());
        transactionHistory.setServerName(transaction.getServerName());
        transactionHistory.setShopLogin(transaction.getShopLogin());
        transactionHistory.setStation(transaction.getStation());
        transactionHistory.setStatusAttemptsLeft(transaction.getStatusAttemptsLeft());
        transactionHistory.setToken(transaction.getToken());
        transactionHistory.setOutOfRange(transaction.getOutOfRange());
        transactionHistory.setRefuelMode(transaction.getRefuelMode());
        transactionHistory.setSrcTransactionID(transaction.getSrcTransactionID());
        transactionHistory.setEncodedSecretKey(transaction.getEncodedSecretKey());
        transactionHistory.setGroupAcquirer(transaction.getGroupAcquirer());
        transactionHistory.setGFGElectronicInvoiceID(transaction.getGFGElectronicInvoiceID());
        transactionHistory.setTransactionCategory(transaction.getTransactionCategory());

        Set<TransactionEventHistory> transactionEventHistoryList = new HashSet<TransactionEventHistory>(0);
        Set<TransactionEvent> transactionEvent = transaction.getTransactionEventData();
        TransactionEventHistory teh;
        for (TransactionEvent te : transactionEvent) {
            teh = new TransactionEventHistory();
            teh.setErrorCode(te.getErrorCode());
            teh.setErrorDescription(te.getErrorDescription());
            teh.setEventAmount(te.getEventAmount());
            teh.setEventType(te.getEventType());
            teh.setId(te.getId());
            teh.setSequenceID(te.getSequenceID());
            teh.setTransaction(transactionHistory);
            teh.setTransactionResult(te.getTransactionResult());
            transactionEventHistoryList.add(teh);
        }
        transactionHistory.setTransactionEventHistoryData(transactionEventHistoryList);

        transactionHistory.setTransactionID(transaction.getTransactionID());
        //transactionHistory.setTransactionStatusData(transactionStatusData);

        Set<TransactionStatusHistory> transactionStatusHistory = new HashSet<TransactionStatusHistory>(0);
        Set<TransactionStatus> transactionStatus = transaction.getTransactionStatusData();
        TransactionStatusHistory tsh;
        for (TransactionStatus ts : transactionStatus) {
            tsh = new TransactionStatusHistory();
            tsh.setId(ts.getId());
            tsh.setRequestID(ts.getRequestID());
            tsh.setSequenceID(ts.getSequenceID());
            tsh.setStatus(ts.getStatus());
            tsh.setSubStatus(ts.getSubStatus());
            tsh.setSubStatusDescription(ts.getSubStatusDescription());
            tsh.setTimestamp(ts.getTimestamp());
            tsh.setTransactionHistory(transactionHistory);
            transactionStatusHistory.add(tsh);

        }
        transactionHistory.setTransactionStatusHistoryData(transactionStatusHistory);

        transactionHistory.setUser(transaction.getUser());
        transactionHistory.setUseVoucher(transaction.getUseVoucher());

        if (transaction.getPrePaidConsumeVoucherList() != null) {
            for (PrePaidConsumeVoucher prePaidConsumeVoucher : transaction.getPrePaidConsumeVoucherList()) {
                transactionHistory.getPrePaidConsumeVoucherList().add(prePaidConsumeVoucher);

            }
        }

        if (transaction.getPrePaidLoadLoyaltyCreditsBeanList() != null) {
            for (PrePaidLoadLoyaltyCredits prePaidLoadLoyaltyCredits : transaction.getPrePaidLoadLoyaltyCreditsBeanList()) {
                transactionHistory.getPrePaidLoadLoyaltyCreditsBeanList().add(prePaidLoadLoyaltyCredits);

            }
        }
        
        if (transaction.getTransactionAdditionalDataList() != null) {
            for (TransactionAdditionalData transactionAdditionalData : transaction.getTransactionAdditionalDataList()) {
                TransactionHistoryAdditionalData transactionHistoryAdditionalData = new TransactionHistoryAdditionalData(transactionAdditionalData);
                transactionHistory.getTransactionHistoryAdditionalDataList().add(transactionHistoryAdditionalData);

            }
        }

        return transactionHistory;
    }

    public String getGFGElectronicInvoiceID() {

        return gfgElectronicInvoiceID;
    }

    public void setGFGElectronicInvoiceID(String gfgElectronicInvoiceID) {

        this.gfgElectronicInvoiceID = gfgElectronicInvoiceID;
    }

    public TransactionCategoryType getTransactionCategory() {

        return transactionCategory;
    }
    
    public void setTransactionCategory(TransactionCategoryType transactionCategory) {

        this.transactionCategory = transactionCategory;
    }

    public Double getUnitPrice() {
    
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
    
        this.unitPrice = unitPrice;
    }

    public Set<TransactionHistoryAdditionalData> getTransactionHistoryAdditionalDataList() {
    
        return transactionHistoryAdditionalDataList;
    }

    public void setTransactionHistoryAdditionalDataList(Set<TransactionHistoryAdditionalData> transactionHistoryAdditionalDataList) {
    
        this.transactionHistoryAdditionalDataList = transactionHistoryAdditionalDataList;
    }
    
}
