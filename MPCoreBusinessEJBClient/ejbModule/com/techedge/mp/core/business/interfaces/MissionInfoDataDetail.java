package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class MissionInfoDataDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8690656214370476074L;

    private String            description;

    private String            code;

    private String            name;

    private String            type;

    private Integer           stepCompleted;

    private Integer           stepObjective;

    private AppLink           appLink;

    private Date              startDate        = new Date();

    private Date              endDate          = new Date();


    public AppLink getAppLink() {
    
        return appLink;
    }

    public void setAppLink(AppLink appLink) {
    
        this.appLink = appLink;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public Integer getStepCompleted() {

        return stepCompleted;
    }

    public void setStepCompleted(Integer stepCompleted) {

        this.stepCompleted = stepCompleted;
    }

    public Integer getStepObjective() {

        return stepObjective;
    }

    public void setStepObjective(Integer stepObjective) {

        this.stepObjective = stepObjective;
    }

    public Date getStartDate() {

        return startDate;
    }

    public void setStartDate(Date startDate) {

        this.startDate = startDate;
    }

    public Date getEndDate() {

        return endDate;
    }

    public void setEndDate(Date endDate) {

        this.endDate = endDate;
    }

}
