package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class AdminAuthenticationResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3076180365005019635L;
	
	private String statusCode;
	private String ticketId;
	private Admin admin;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
}
