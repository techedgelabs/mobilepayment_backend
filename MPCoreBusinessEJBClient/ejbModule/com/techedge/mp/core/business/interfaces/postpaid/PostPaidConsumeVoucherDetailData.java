package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;
import java.util.Date;

public class PostPaidConsumeVoucherDetailData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6414146177592902191L;

    private String            voucherCode;
    private String            voucherStatus;
    private String            voucherType;
    private Double            voucherValue;
    private Double            initialValue;
    private Double            consumedValue;
    private Double            voucherBalanceDue;
    private Date              expirationDate;
    private String            promoCode;
    private String            promoDescription;
    private String            promoDoc;

    public PostPaidConsumeVoucherDetailData() {}

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public Double getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(Double voucherValue) {
        this.voucherValue = voucherValue;
    }

    public Double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Double initialValue) {
        this.initialValue = initialValue;
    }

    public Double getConsumedValue() {
        return consumedValue;
    }

    public void setConsumedValue(Double consumedValue) {
        this.consumedValue = consumedValue;
    }

    public Double getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Double voucherBalanceDue) {
        this.voucherBalanceDue = voucherBalanceDue;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {
        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {
        this.promoDoc = promoDoc;
    }

}
