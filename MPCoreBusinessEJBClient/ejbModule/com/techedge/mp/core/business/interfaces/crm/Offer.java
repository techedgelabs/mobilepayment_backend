package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Offer implements Serializable {

    /**
     * 
     */
    private static final long       serialVersionUID     = -1592147654321141432L;

    private HashMap<String, Object> additionalAttributes = new HashMap<String, Object>();

    private String                  description;

    private ArrayList<String>       offerCode = new ArrayList<String>();

    private String                  offerName;

    private int                     score;

    private String                  treatmentCode;

    
    public HashMap<String, Object> getAdditionalAttributes() {   
        return additionalAttributes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getOfferCode() {
        return offerCode;
    }

    public String getOfferCodeToString(String separator) {
        String value = "";
        
        for (int i = 0; i < offerCode.size(); i++) {
            if (i > 0) {
                value += separator;
            }
            
            value += offerCode.get(i);
        }
        
        return value;
    }
    
    public void setOfferCode(ArrayList<String> offerCode) {
        this.offerCode = offerCode;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getTreatmentCode() {
        return treatmentCode;
    }

    public void setTreatmentCode(String treatmentCode) {
        this.treatmentCode = treatmentCode;
    }


}
