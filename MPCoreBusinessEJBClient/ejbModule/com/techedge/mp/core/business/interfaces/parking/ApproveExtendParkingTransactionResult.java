package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;

public class ApproveExtendParkingTransactionResult extends FullParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6694975416474472398L;
    
    Integer pinCheckMaxAttempts;

    public Integer getPinCheckMaxAttempts() {
    
        return pinCheckMaxAttempts;
    }

    public void setPinCheckMaxAttempts(Integer pinCheckMaxAttempts) {
    
        this.pinCheckMaxAttempts = pinCheckMaxAttempts;
    }
}
