package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;

public class ProcessEventResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1641141083690951854L;

    private StatusCodeEnum statusCode;

    private String         operationId;

    public StatusCodeEnum getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(StatusCodeEnum statusCode) {

        this.statusCode = statusCode;
    }

    public String getOperationId() {
    
        return operationId;
    }

    public void setOperationId(String operationId) {
    
        this.operationId = operationId;
    }

}
