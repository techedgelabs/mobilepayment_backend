package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AdminRetrieveDataResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5969436220305094256L;

    private String            statusCode;

    private List<Admin>       admins           = new ArrayList<>(0);

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<Admin> getAdmins() {

        return admins;
    }

    public void setAdmins(List<Admin> admins) {

        this.admins = admins;
    }

}
