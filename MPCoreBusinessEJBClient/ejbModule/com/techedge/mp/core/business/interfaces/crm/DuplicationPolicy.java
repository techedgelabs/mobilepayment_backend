package com.techedge.mp.core.business.interfaces.crm;

public enum DuplicationPolicy {
    NO_DUPLICATION(1),
    ALLOW_DUPLICATION(2);
    
    private Integer value;
    
    private DuplicationPolicy(Integer value) {
        this.value = value;
    }
    
    public Integer getValue() {
        return value;
    }
}
