package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


public class Manager implements Serializable {

  private static final long serialVersionUID = 3059386197620182434L;

  public static final Integer TYPE_STANDARD = 1;
  
  public static final Integer STATUS_BLOCKED  = 0;
  public static final Integer STATUS_ACTIVE = 2;
  public static final Integer STATUS_TEMPORARY_PASSWORD = 6;

  private long id;
  private String username;
  private String password;
  private String email;
  private Integer type;
  private String firstName;
  private String lastName;
  private Integer status;
  private Set<Station> stations = new HashSet<Station>(0);
  
  public Manager() {}
  
  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Set<Station> getStations() {
    return stations;
  }

  public void setStations(Set<Station> stations) {
    this.stations = stations;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  
}
