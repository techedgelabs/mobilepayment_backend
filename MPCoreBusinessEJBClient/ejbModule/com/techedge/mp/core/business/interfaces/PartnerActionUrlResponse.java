package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PartnerActionUrlResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1164417285802888066L;

    private String            statusCode;

    private String            partnerActionUrl;

    private String            key;

    private int               actionForwardId;

    public String getKey() {

        return key;
    }

    public void setKey(String key) {

        this.key = key;
    }

    public int getActionForwardId() {

        return actionForwardId;
    }

    public void setActionForwardId(int actionForwardId) {

        this.actionForwardId = actionForwardId;
    }

    public String getPartnerActionUrl() {

        return partnerActionUrl;
    }

    public void setPartnerActionUrl(String partnerActionUrl) {

        this.partnerActionUrl = partnerActionUrl;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

}
