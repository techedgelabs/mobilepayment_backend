package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveParamsData implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -544338837008405278L;
	
	private String statusCode;
	private List<ParamInfo> paramList = new ArrayList<ParamInfo>(0);
	
	
	public RetrieveParamsData() {
		
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public List<ParamInfo> getParamsList() {
		return paramList;
	}

	public void setParamsList(List<ParamInfo> paramList) {
		this.paramList = paramList;
	}
	
	
}
