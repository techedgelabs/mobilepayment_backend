package com.techedge.mp.core.business.interfaces.crm;


public enum CRMEventSourceType {

    PREPAID("PREPAID"),
    POSTPAID("POSTPAID"),
    EVENT_NOTIFICATION("EVENT_NOTIFICATION"),
    PAYMENT_INFO("PAYMENT_INFO"),
    USER("USER");
    
    private String value;
    
    private CRMEventSourceType(String value) {
        this.value = value;
    }
    
    public static CRMEventSourceType getSourceType(String value) {
        if (value != null) {
            for (CRMEventSourceType sourceType : CRMEventSourceType.values()) {
                if (sourceType.getName().equals(value) || sourceType.getValue().equals(value)) {
                    return sourceType;
                }
            }
        }
        return null;
    }
    
    
    public String getName() {
        return name();
    }
    
    public String getValue() {
        return value;
    }
}
