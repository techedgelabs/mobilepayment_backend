package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MappingErrorData implements Serializable {

    /**
     * 
     */
    private static final long           serialVersionUID      = -5605732846366638810L;

    private String                      statusCode;
    private List<RefuelingMappingError> refuelingMappingError = new ArrayList<RefuelingMappingError>(0);

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<RefuelingMappingError> getRefuelingMappingError() {
        return refuelingMappingError;
    }

    public void setRefuelingMappingError(List<RefuelingMappingError> refuelingMappingError) {
        this.refuelingMappingError = refuelingMappingError;
    }

}
