package com.techedge.mp.core.business.interfaces.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserTypeData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 9146707782144197520L;

    private String            statusCode;
    private List<UserType>    userTypes        = new ArrayList<UserType>(0);

    public UserTypeData() {}

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<UserType> getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(List<UserType> userTypes) {
        this.userTypes = userTypes;
    }

}
