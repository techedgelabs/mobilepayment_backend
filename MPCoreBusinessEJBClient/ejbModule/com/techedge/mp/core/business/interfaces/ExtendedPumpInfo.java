package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExtendedPumpInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2932558526948100965L;

	private String pumpId;
	private String description;
	private String number;
	private List<String> fuelType = new ArrayList<String>(0);
	private String status;
	private String refuelMode;
	
	public String getPumpId() {
		return pumpId;
	}
	public void setPumpId(String pumpId) {
		this.pumpId = pumpId;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	public List<String> getFuelType() {
		return fuelType;
	}
	public void setFuelType(List<String> fuelType) {
		this.fuelType = fuelType;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
    public String getRefuelMode() {
        return refuelMode;
    }
    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }
	
}
