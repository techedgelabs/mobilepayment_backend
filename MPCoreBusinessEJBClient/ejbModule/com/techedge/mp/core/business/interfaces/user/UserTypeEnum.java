package com.techedge.mp.core.business.interfaces.user;

public enum UserTypeEnum {

    CUSTOMER(1),
    SERVICE(2),
    TESTER(3),
    REFUELING(4),
    VOUCHER_TESTER(5),
    NEW_ACQUIRER_TESTER(6),
    REFUELING_NEW_ACQUIRER(7),
    GUEST(8),
    BUSINESS_PERSON(9),
    BUSINESS_COMPANY(10);
    
    private Integer type;
    
    private UserTypeEnum(Integer value) {
        this.type = value;
    }
    
    public Integer getValue() {
        return type;
    }
    
    public static UserTypeEnum valueOf(Integer value) {
        UserTypeEnum type = null;
        
        if (value != null) {
            for (UserTypeEnum userTypeEnum : UserTypeEnum.values()) {
                if (userTypeEnum.getValue().equals(value)) {
                    type = userTypeEnum;
                    break;
                }
            }
        }
        
        return type;
    }
}
