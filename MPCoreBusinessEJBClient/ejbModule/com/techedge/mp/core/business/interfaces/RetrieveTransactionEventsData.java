package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveTransactionEventsData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2188655080578344888L;
	
	private String statusCode;
	private String messageCode;
	private PaymentRefuelDetailResponse paymentRefuelDetailResponse;
	private List<EventInfo> transactionEvents = new ArrayList<EventInfo>(0);
	
	private String acquirerID;
	private String paymentMode;
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public List<EventInfo> getTransactionEvents() {
		return transactionEvents;
	}
	public void setTransactionEvents(List<EventInfo> transactionEvents) {
		this.transactionEvents = transactionEvents;
	}
	public PaymentRefuelDetailResponse getPaymentRefuelDetailResponse() {
		return paymentRefuelDetailResponse;
	}
	public void setPaymentRefuelDetailResponse(PaymentRefuelDetailResponse paymentRefuelDetailResponse) {
		this.paymentRefuelDetailResponse = paymentRefuelDetailResponse;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getAcquirerID() {
		return acquirerID;
	}
	public void setAcquirerID(String acquirerID) {
		this.acquirerID = acquirerID;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	
	
	
	
}
