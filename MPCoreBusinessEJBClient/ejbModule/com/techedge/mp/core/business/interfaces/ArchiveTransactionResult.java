package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class ArchiveTransactionResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8350277486484561436L;
    private String id;
    private String type;
    private String statusCode;

    public ArchiveTransactionResult(String id, String type, String statusCode) {
        this.id = id;
        this.type = type;
        this.statusCode = statusCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
