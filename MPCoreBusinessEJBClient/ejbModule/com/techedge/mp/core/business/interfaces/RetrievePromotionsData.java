package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrievePromotionsData implements Serializable {

    /**
     * 
     */
    private static final long   serialVersionUID = -4301849319345462606L;

    private String              statusCode;
    private List<PromotionInfo> promotionList    = new ArrayList<PromotionInfo>(0);

    public RetrievePromotionsData() {

    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<PromotionInfo> getPromotionList() {
        return promotionList;
    }

    public void setPromotionList(List<PromotionInfo> promotionList) {
        this.promotionList = promotionList;
    }

}
