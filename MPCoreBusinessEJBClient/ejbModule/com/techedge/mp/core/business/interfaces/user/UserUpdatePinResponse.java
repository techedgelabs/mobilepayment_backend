package com.techedge.mp.core.business.interfaces.user;

import java.io.Serializable;

public class UserUpdatePinResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8016242247572821407L;
	
	
	private Integer pinCheckMaxAttempts;
	private String statusCode; 
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public Integer getPinCheckMaxAttempts() {
		return pinCheckMaxAttempts;
	}
	public void setPinCheckMaxAttempts(Integer pinCheckMaxAttempts) {
		this.pinCheckMaxAttempts = pinCheckMaxAttempts;
	}
}
