package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;



public class ParamInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7127236969095611145L;
	private String key;
	private String value;
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	
}
