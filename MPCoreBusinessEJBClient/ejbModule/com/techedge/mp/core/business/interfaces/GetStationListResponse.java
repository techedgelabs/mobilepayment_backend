package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetStationListResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4777629592658214824L;
	
	private String statusCode;
	private List<GetStationInfo> stationInfoList = new ArrayList<GetStationInfo>(0);
	private List<ActiveCityInfo> activeCityInfoList = new ArrayList<ActiveCityInfo>(0);

	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public List<GetStationInfo> getStationInfoList() {
		return this.stationInfoList;
	}
	public void setStationInfoList(List<GetStationInfo> stationInfoList) {
		this.stationInfoList = stationInfoList;
	}
	
    public List<ActiveCityInfo> getActiveCityInfoList() {
        return activeCityInfoList;
    }
    
    public void setActiveCityInfoList(List<ActiveCityInfo> activeCityInfoList) {
        this.activeCityInfoList = activeCityInfoList;
    }


}
