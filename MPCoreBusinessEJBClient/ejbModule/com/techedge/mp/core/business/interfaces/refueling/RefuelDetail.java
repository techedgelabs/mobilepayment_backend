package com.techedge.mp.core.business.interfaces.refueling;

import java.io.Serializable;

public class RefuelDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2130977492835831456L;
	
	private String timestampStartRefuel;
	private String timestampEndRefuel;
	private String authorizationCode;
	private Double amount;
	private String fuelType;
	private Double fuelQuantity;
	private String productID;
	private String productDescription;
	
	
	public String getTimestampStartRefuel() {
        return timestampStartRefuel;
    }
    public void setTimestampStartRefuel(String timestampStartRefuel) {
        this.timestampStartRefuel = timestampStartRefuel;
    }
    
	public String getTimestampEndRefuel() {
		return timestampEndRefuel;
	}
	public void setTimestampEndRefuel(String timestampEndRefuel) {
		this.timestampEndRefuel = timestampEndRefuel;
	}
	
	public String getAuthorizationCode() {
    
        return authorizationCode;
    }
    public void setAuthorizationCode(String authorizationCode) {
    
        this.authorizationCode = authorizationCode;
    }
    
    public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	
	public Double getFuelQuantity() {
		return fuelQuantity;
	}
	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}
	
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	
    public String toString(){
        
        StringBuilder builder = new StringBuilder();
        builder.append("timestampStartRefuel: ").append(getTimestampStartRefuel());
        builder.append(" - timestampEndRefuel: ").append(getTimestampEndRefuel());
        builder.append(" - amount: ").append(String.valueOf(getAmount()));
        builder.append(" - fuelType: ").append(getFuelType());
        builder.append(" - fuelQuantity: ").append(String.valueOf(getFuelQuantity()));
        builder.append(" - productID: ").append(getProductID());
        builder.append(" - productDescription: ").append(getProductDescription());
        
        return builder.toString();
        
    }
}
