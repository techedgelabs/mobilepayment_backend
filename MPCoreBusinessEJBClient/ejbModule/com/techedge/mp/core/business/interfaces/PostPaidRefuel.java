package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class PostPaidRefuel implements Serializable {

    /**
     * 
     */
    private static final long   serialVersionUID = 5419964974167706486L;

    private long                id;
    private String              fuelType;
    private String              productId;
    private String              productDescription;
    private Double              fuelQuantity;
    private Double              fuelAmount;
    private Double              unitPrice;
    private String              pumpId;
    private Integer             pumpNumber;
    private String              refuelMode;
    private Date                timestampEndRefuel;
    private PostPaidTransaction postPaidTransaction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public Double getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(Double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public Double getUnitPrice() {
    
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
    
        this.unitPrice = unitPrice;
    }

    public String getPumpId() {
        return pumpId;
    }

    public void setPumpId(String pumpId) {
        this.pumpId = pumpId;
    }

    public Integer getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(Integer pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    public Date getTimestampEndRefuel() {
        return timestampEndRefuel;
    }

    public void setTimestampEndRefuel(Date timestampEndRefuel) {
        this.timestampEndRefuel = timestampEndRefuel;
    }

    public PostPaidTransaction getPostPaidTransaction() {
        return postPaidTransaction;
    }

    public void setPostPaidTransaction(PostPaidTransaction postPaidTransaction) {
        this.postPaidTransaction = postPaidTransaction;
    }

}
