package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;


public class AdminPoPArchiveResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -154559506173679976L;
	
	
	private Integer archivedPoPTransaction;
	private String statusCode;


	public Integer getArchivedPoPTransaction() {
		return archivedPoPTransaction;
	}


	public void setArchivedPoPTransaction(Integer archivedPoPTransaction) {
		this.archivedPoPTransaction = archivedPoPTransaction;
	}


	public String getStatusCode() {
		return statusCode;
	}


	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
}
