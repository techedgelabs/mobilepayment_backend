package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class CheckAvailabilityAmountData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2626457123473180564L;

    private String            statusCode;
    private Double            maxAmount;
    private Double            thresholdAmount;
    private Boolean           allowResize;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public Double getMaxAmount() {

        return maxAmount;
    }

    public void setMaxAmount(Double maxAmount) {

        this.maxAmount = maxAmount;
    }

    public Double getThresholdAmount() {

        return thresholdAmount;
    }

    public void setThresholdAmount(Double thresholdAmount) {

        this.thresholdAmount = thresholdAmount;
    }

    public Boolean getAllowResize() {
    
        return allowResize;
    }

    public void setAllowResize(Boolean allowResize) {
    
        this.allowResize = allowResize;
    }
}
