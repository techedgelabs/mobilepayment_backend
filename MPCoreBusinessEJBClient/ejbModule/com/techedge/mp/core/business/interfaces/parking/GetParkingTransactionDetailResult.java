package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;

public class GetParkingTransactionDetailResult implements Serializable {

    /**
     * 
     */
    private static final long  serialVersionUID = 1393488766695588970L;

    private ParkingTransaction parkingTransaction;
    
    private String statusCode;

    public ParkingTransaction getParkingTransaction() {

        return parkingTransaction;
    }

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public void setParkingTransaction(ParkingTransaction parkingTransaction) {

        this.parkingTransaction = parkingTransaction;
    }

}
