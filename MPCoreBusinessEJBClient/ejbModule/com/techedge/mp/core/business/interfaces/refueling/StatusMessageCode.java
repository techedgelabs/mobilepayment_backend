package com.techedge.mp.core.business.interfaces.refueling;

import java.io.Serializable;

public class StatusMessageCode implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -6345003504492439295L;
    
    private String statusCode;
    private String messageCode;

    public StatusMessageCode() {

        super();
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

}