package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PromoPopupData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1386631169451813846L;

    private String  url;

    private String  type;

    private String  body;

    private String  bannerUrl;

    private Boolean showAlways;

    private String  promoId;

    public PromoPopupData() {

    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public String getBody() {

        return body;
    }

    public void setBody(String body) {

        this.body = body;
    }

    public String getBannerUrl() {

        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {

        this.bannerUrl = bannerUrl;
    }

    public Boolean getShowAlways() {

        return showAlways;
    }

    public void setShowAlways(Boolean showAlways) {

        this.showAlways = showAlways;
    }

    public String getPromoId() {

        return promoId;
    }

    public void setPromoId(String promoId) {

        this.promoId = promoId;
    }

}
