package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PaymentV2Response implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -720643065172476661L;

    private String            statusCode;

    private String            shopLogin;

    private String            redirectUrl;

    private Integer           pinCheckAttemptsLeft;

    private Long              paymentMethodId;

    private String            paymentMethodType;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getShopLogin() {

        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {

        this.shopLogin = shopLogin;
    }

    public String getRedirectUrl() {
    
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
    
        this.redirectUrl = redirectUrl;
    }

    public Long getPaymentMethodId() {

        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {

        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {

        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {

        this.paymentMethodType = paymentMethodType;
    }

    public Integer getPinCheckAttemptsLeft() {

        return pinCheckAttemptsLeft;
    }

    public void setPinCheckAttemptsLeft(Integer pinCheckAttemptsLeft) {

        this.pinCheckAttemptsLeft = pinCheckAttemptsLeft;
    }

}
