package com.techedge.mp.core.business.interfaces.refueling;



public class RefuelingGetMPTransactionStatusResponse extends StatusMessageCode {

    /**
     * 
     */
    private static final long serialVersionUID = -6921139968626203438L;

    private String              mpTransactionID;
    private String              mpTransactionStatus;
    private RefuelDetail        refuelDetail;
    
    public String getMpTransactionID() {
    
        return mpTransactionID;
    }
    
    public void setMpTransactionID(String mpTransactionID) {
    
        this.mpTransactionID = mpTransactionID;
    }
    
    public String getMpTransactionStatus() {
    
        return mpTransactionStatus;
    }
    
    public void setMpTransactionStatus(String mpTransactionStatus) {
    
        this.mpTransactionStatus = mpTransactionStatus;
    }
    
    public RefuelDetail getRefuelDetail() {
    
        return refuelDetail;
    }
    
    public void setRefuelDetail(RefuelDetail refuelDetail) {
    
        this.refuelDetail = refuelDetail;
    }
    
    
}
