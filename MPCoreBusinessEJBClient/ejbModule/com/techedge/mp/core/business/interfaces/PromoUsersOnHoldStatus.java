package com.techedge.mp.core.business.interfaces;

public enum PromoUsersOnHoldStatus {
    //1 = new, 2 = assigned, 6 = invalid
    NEW(1), ASSIGNED(2), INVALID(6);

    private final Integer value;

    PromoUsersOnHoldStatus(final Integer newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }


}
