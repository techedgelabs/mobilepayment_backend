package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class PostPaidTransactionPaymentEvent implements Serializable {

    /**
     * 
     */
    private static final long   serialVersionUID = -6446667113223805184L;

    private long                id;
    private Integer             sequence;
    private String              eventType;
    private String              transactionResult;
    private String              authorizationCode;
    private String              errorCode;
    private String              errorDescription;
    private PostPaidTransaction postPaidTransaction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {
        this.transactionResult = transactionResult;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public PostPaidTransaction getPostPaidTransaction() {
        return postPaidTransaction;
    }

    public void setPostPaidTransaction(PostPaidTransaction postPaidTransaction) {
        this.postPaidTransaction = postPaidTransaction;
    }

}
