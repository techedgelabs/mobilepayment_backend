package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class TransactionConsumeVoucherPreAuthResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5728759516148797241L;

    private String                     statusCode;
    private String                     fidelityStatusCode;
    private String                     fidelityStatusMessage;
    private FidelityConsumeVoucherData fidelityConsumeVoucherData;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getFidelityStatusCode() {
    
        return fidelityStatusCode;
    }

    public void setFidelityStatusCode(String fidelityStatusCode) {
    
        if (fidelityStatusCode != null && fidelityStatusCode.equals("00"))
            fidelityStatusCode = "0";
        
        this.fidelityStatusCode = fidelityStatusCode;
    }

    public String getFidelityStatusMessage() {
    
        return fidelityStatusMessage;
    }

    public void setFidelityStatusMessage(String fidelityStatusMessage) {
    
        this.fidelityStatusMessage = fidelityStatusMessage;
    }

    public FidelityConsumeVoucherData getFidelityConsumeVoucherData() {

        return fidelityConsumeVoucherData;
    }

    public void setFidelityConsumeVoucherData(FidelityConsumeVoucherData fidelityConsumeVoucherData) {

        this.fidelityConsumeVoucherData = fidelityConsumeVoucherData;
    }

}
