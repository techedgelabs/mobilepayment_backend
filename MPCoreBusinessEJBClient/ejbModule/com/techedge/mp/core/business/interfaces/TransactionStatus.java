package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.sql.Timestamp;

public class TransactionStatus implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -8549655914974740149L;

    private long              id;

    private Transaction       transaction;

    private Integer           sequenceID;

    private Timestamp         timestamp;

    private String            status;

    private String            subStatus;

    private String            subStatusDescription;

    private String            requestID;

    private String            level;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public Transaction getTransaction() {

        return transaction;
    }

    public void setTransaction(Transaction transaction) {

        this.transaction = transaction;
    }

    public Integer getSequenceID() {

        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {

        this.sequenceID = sequenceID;
    }

    public Timestamp getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {

        this.timestamp = timestamp;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getSubStatus() {

        return subStatus;
    }

    public void setSubStatus(String subStatus) {

        this.subStatus = subStatus;
    }

    public String getSubStatusDescription() {

        return subStatusDescription;
    }

    public void setSubStatusDescription(String subStatusDescription) {

        this.subStatusDescription = subStatusDescription;
    }

    public String getRequestID() {

        return requestID;
    }

    public void setRequestID(String requestID) {

        this.requestID = requestID;
    }

    public String getLevel() {

        return level;
    }

    public void setLevel(String level) {

        this.level = level;
    }

}
