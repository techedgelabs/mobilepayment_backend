package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.user.UserCategory;

public class LandingData implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -7265442736103785009L;

    private long              id;
    
    private String            title;

    private String            url;

    private Boolean           showAlways;

    private String            status;

    private UserCategory      userCategory;

    private String            sectionID;

    public LandingData() {

    }

    public long getId() {
    
        return id;
    }


    public void setId(long id) {
    
        this.id = id;
    }


    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    public Boolean getShowAlways() {

        return showAlways;
    }

    public void setShowAlways(Boolean showAlways) {

        this.showAlways = showAlways;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public UserCategory getUserCategory() {

        return userCategory;
    }

    public void setUserCategory(UserCategory userCategory) {

        this.userCategory = userCategory;
    }

    public String getSectionID() {

        return sectionID;
    }

    public void setSectionID(String sectionID) {

        this.sectionID = sectionID;
    }

}
