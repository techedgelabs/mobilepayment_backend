package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class PromotionInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 9051928077860194385L;

    private String            code;
    private String            description;
    private Date              startData;
    private Date              endData;
    private Integer           status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartData() {
        return startData;
    }

    public void setStartData(Date startData) {
        this.startData = startData;
    }

    public Date getEndData() {
        return endData;
    }

    public void setEndData(Date endData) {
        this.endData = endData;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
