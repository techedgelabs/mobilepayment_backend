package com.techedge.mp.core.business.interfaces.voucher;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.Voucher;

public class RetrieveVoucherTransactionDataResponse extends VoucherTransaction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8828142986534339092L;

    private String            statusCode;

    private String            messageCode;

    private String            sourceType;

    private String            sourceStatus;

    private String            transactionType;

    private String            panCode;

    private String            status;

    private String            statusTitle;

    private String            statusDescription;

    private String            subStatus;

    private String            surveyUrl;
    
    private Voucher           voucher;  

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public String getMessageCode() {

        return messageCode;
    }

    public void setMessageCode(String messageCode) {

        this.messageCode = messageCode;
    }

    public String getSourceType() {

        return sourceType;
    }

    public void setSourceType(String sourceType) {

        this.sourceType = sourceType;
    }

    public String getSourceStatus() {

        return sourceStatus;
    }

    public void setSourceStatus(String sourceStatus) {

        this.sourceStatus = sourceStatus;
    }

    public String getTransactionType() {

        return transactionType;
    }

    public void setTransactionType(String transactionType) {

        this.transactionType = transactionType;
    }

    public String getPanCode() {

        return panCode;
    }

    public void setPanCode(String panCode) {

        this.panCode = panCode;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getStatusTitle() {

        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {

        this.statusTitle = statusTitle;
    }

    public String getStatusDescription() {

        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {

        this.statusDescription = statusDescription;
    }

    public String getSubStatus() {

        return subStatus;
    }

    public void setSubStatus(String subStatus) {

        this.subStatus = subStatus;
    }

    public String getSurveyUrl() {

        return surveyUrl;
    }

    public void setSurveyUrl(String surveyUrl) {

        this.surveyUrl = surveyUrl;
    }

    public Voucher getVoucher() {

        return voucher;
    }

    public void setVoucher(Voucher voucher) {

        this.voucher = voucher;
    }
}
