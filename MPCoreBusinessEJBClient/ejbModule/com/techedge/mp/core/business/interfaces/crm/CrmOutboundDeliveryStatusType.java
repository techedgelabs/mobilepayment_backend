package com.techedge.mp.core.business.interfaces.crm;


public enum CrmOutboundDeliveryStatusType {
    
    CREATED("0"),
    SENT("1"),
    READ("2"),
    ERROR_PUSH("3"),
    ERROR_VOUCHER("4");

    
    private final String code;

    CrmOutboundDeliveryStatusType(final String code) {
        this.code = code;
    }
    
    public static CrmOutboundDeliveryStatusType getStatusType(String code) {
        for (CrmOutboundDeliveryStatusType statusType : CrmOutboundDeliveryStatusType.values()) {
            if (statusType.getCode().equals(code)) {
                return statusType;
            }
        }
        
        return null;
    }

    public String getCode() {
        return code; 
    }

    public String getName() {
        return name();
    }


}
