package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.user.User;

public class AuthenticationBusinessResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5903217597842792367L;
	
	private String statusCode;
	private String ticketId;
    private String sourceToken;
	private User user;
	
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public String getSourceToken() {

        return sourceToken;
    }
	
	public void setSourceToken(String sourceToken) {

        this.sourceToken = sourceToken;
    }
}
