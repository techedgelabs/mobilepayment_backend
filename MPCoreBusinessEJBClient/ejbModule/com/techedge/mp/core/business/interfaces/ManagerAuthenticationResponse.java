package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class ManagerAuthenticationResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5903217597842792367L;
	
	private String statusCode;
	private String ticketId;
	private Manager manager;
	
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public Manager getManager() {
		return manager;
	}
	
	public void setManager(Manager manager) {
		this.manager = manager;
	}
}
