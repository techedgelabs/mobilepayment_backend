package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveParkingZonesByCityResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID    = 5527071852908111918L;

    List<ParkingCityZoneData> parkingCityZoneData = new ArrayList<ParkingCityZoneData>(0);

    public List<ParkingCityZoneData> getParkingCityZoneData() {

        return parkingCityZoneData;
    }

    public void setParkingCityZoneData(List<ParkingCityZoneData> parkingCityZoneData) {

        this.parkingCityZoneData = parkingCityZoneData;
    }

    private String statusCode;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

}
