package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;

public class RetrievePendingParkingTransactionResult implements Serializable {

    /**
     * 
     */
    private static final long  serialVersionUID = 737367460034513949L;

    private ParkingTransaction parkingTransaction;

    private String             statusCode;

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public ParkingTransaction getParkingTransaction() {

        return parkingTransaction;
    }

    public void setParkingTransaction(ParkingTransaction parkingTransaction) {

        this.parkingTransaction = parkingTransaction;
    }

}
