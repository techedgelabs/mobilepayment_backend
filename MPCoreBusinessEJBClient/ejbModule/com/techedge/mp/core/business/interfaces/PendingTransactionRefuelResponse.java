package com.techedge.mp.core.business.interfaces;

import java.util.ArrayList;
import java.util.List;

public class PendingTransactionRefuelResponse {

    private String       statusCode;
    private List<Transaction> transactionList = new ArrayList<Transaction>(0);

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionIDList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

}
