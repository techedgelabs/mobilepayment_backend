package com.techedge.mp.core.business.interfaces.crm;

import java.io.Serializable;
import java.util.HashMap;

public class GetOfferRequest implements Serializable {

    /**
     * 
     */
    private static final long       serialVersionUID = 6933890431071068136L;

    private DuplicationPolicy       duplicationPolicy;

    private String                  interactionPoint;

    private Integer                 numberRequested;

    private HashMap<String, Object> offerAttributes = new HashMap<String, Object>();
    

    public DuplicationPolicy getDuplicationPolicy() {
        return duplicationPolicy;
    }

    public void setDuplicationPolicy(DuplicationPolicy duplicationPolicy) {
        this.duplicationPolicy = duplicationPolicy;
    }

    public String getInteractionPoint() {
        return interactionPoint;
    }

    public void setInteractionPoint(String interactionPoint) {
        this.interactionPoint = interactionPoint;
    }

    public Integer getNumberRequested() {
        return numberRequested;
    }

    public void setNumberRequested(Integer numberRequested) {
        this.numberRequested = numberRequested;
    }

    public HashMap<String, Object> getOfferAttributes() {
        return offerAttributes;
    }

    public void setOfferAttributes(HashMap<String, Object> offerAttributes) {
        this.offerAttributes = offerAttributes;
    }

}
