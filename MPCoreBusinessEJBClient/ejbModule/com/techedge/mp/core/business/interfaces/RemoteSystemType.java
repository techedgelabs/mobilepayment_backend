package com.techedge.mp.core.business.interfaces;

public enum RemoteSystemType {
    LOYALTY,
    GFG,
//    ENJOY,
    ENJOY2,
    DWH,
//    CRM,
    CRM_SF_NOTIFYEVENT,
    CRM_SF_GETOFFERS,
    MYCICERO
}
