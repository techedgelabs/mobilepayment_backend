package com.techedge.mp.core.business.interfaces.reconciliation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.techedge.mp.core.business.interfaces.StatusHelper;

public class ReconciliationTransactionSummary implements Serializable {
        
    /**
     * 
     */
    private static final long serialVersionUID = -5727339930484232860L;
    
    public static final String SUMMARY_PREPAID_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL = StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL;
    public static final String SUMMARY_PREPAID_MISSING_PAYAUTH_DELETE_AFTER_REFUEL = StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_AFTER_REFUEL;
    public static final String SUMMARY_PREPAID_MISSING_PAYMENT = StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT;
    public static final String SUMMARY_PREPAID_ERROR = StatusHelper.FINAL_STATUS_TYPE_ERROR;
    public static final String SUMMARY_PREPAID_MISSING_NOTIFICATION = StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL;
    public static final String SUMMARY_PREPAID_ABEND = StatusHelper.FINAL_STATUS_TYPE_ABEND;
    public static final String SUMMARY_PREPAID_VOUCHER_ERROR = StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL;
    public static final String SUMMARY_PREPAID_LOYALTY_ERROR = StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL;
    
    public static final String SUMMARY_POSTPAID_PAYMENT_ERROR = StatusHelper.POST_PAID_FINAL_STATUS_UNPAID;
    public static final String SUMMARY_POSTPAID_VOUCHER_ERROR = StatusHelper.POST_PAID_STATUS_VOUCHER_REVERSE;
    public static final String SUMMARY_POSTPAID_LOYALTY_ERROR = StatusHelper.POST_PAID_STATUS_LOYALTY_REVERSE;

    public static final String SUMMARY_VOUCHER_PAYMENT_AUTH_ERROR = StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_AUTH;
    public static final String SUMMARY_VOUCHER_PAYMENT_SETTLE_ERROR = StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_MOV;
    public static final String SUMMARY_VOUCHER_VOUCHER_CREATE_ERROR = StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE;
    public static final String SUMMARY_VOUCHER_VOUCHER_DELETE_ERROR = StatusHelper.VOUCHER_FINAL_STATUS_ERROR_DELETE;
    
    public static final String SUMMARY_STATUS_NOT_IDENTIFIED = "STATUS_NOT_IDENTIFIED";
    
    
    @SuppressWarnings("serial")
    HashMap<String, List<ReconciliationInfo>> summary = new HashMap<String, List<ReconciliationInfo>>() {
        {
            put(SUMMARY_PREPAID_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_PREPAID_MISSING_PAYAUTH_DELETE_AFTER_REFUEL, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_PREPAID_MISSING_PAYMENT, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_PREPAID_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_PREPAID_MISSING_NOTIFICATION, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_PREPAID_ABEND, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_PREPAID_VOUCHER_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_PREPAID_LOYALTY_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_POSTPAID_PAYMENT_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_POSTPAID_VOUCHER_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_POSTPAID_LOYALTY_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_VOUCHER_PAYMENT_AUTH_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_VOUCHER_PAYMENT_SETTLE_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_VOUCHER_VOUCHER_CREATE_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_VOUCHER_VOUCHER_DELETE_ERROR, new ArrayList<ReconciliationInfo>(0));
            put(SUMMARY_STATUS_NOT_IDENTIFIED, new ArrayList<ReconciliationInfo>(0));
        }
    };
    
    String statusCode;
    
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<ReconciliationInfo> getSummary(String type) {
        if (!summary.containsKey(type)) {
            return null;
        }
        
        return summary.get(type);
    }

    public boolean addToSummary(String type, ReconciliationInfo reconciliationInfo) {
        List<ReconciliationInfo> reconciliationInfoList;
        
        if (!summary.containsKey(type)) {
            return false;
        }
        
        reconciliationInfoList = summary.get(type);
        reconciliationInfoList.add(reconciliationInfo);
        summary.put(type, reconciliationInfoList);
        return true;
    }
    
    public Integer getProcessed() {
        return new Integer(summary.get(SUMMARY_PREPAID_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL).size() + 
                summary.get(SUMMARY_PREPAID_MISSING_PAYAUTH_DELETE_AFTER_REFUEL).size() + 
                summary.get(SUMMARY_PREPAID_MISSING_PAYMENT).size() + 
                summary.get(SUMMARY_PREPAID_ERROR).size() +
                summary.get(SUMMARY_PREPAID_MISSING_NOTIFICATION).size() +
                summary.get(SUMMARY_PREPAID_ABEND).size() +
                summary.get(SUMMARY_PREPAID_VOUCHER_ERROR).size() +
                summary.get(SUMMARY_PREPAID_LOYALTY_ERROR).size() +
                summary.get(SUMMARY_POSTPAID_PAYMENT_ERROR).size() +
                summary.get(SUMMARY_POSTPAID_VOUCHER_ERROR).size() +
                summary.get(SUMMARY_POSTPAID_LOYALTY_ERROR).size() +
                summary.get(SUMMARY_VOUCHER_PAYMENT_AUTH_ERROR).size() +
                summary.get(SUMMARY_VOUCHER_PAYMENT_SETTLE_ERROR).size() +
                summary.get(SUMMARY_VOUCHER_VOUCHER_CREATE_ERROR).size() +
                summary.get(SUMMARY_VOUCHER_VOUCHER_DELETE_ERROR).size() +
                summary.get(SUMMARY_STATUS_NOT_IDENTIFIED).size());
    }
}
