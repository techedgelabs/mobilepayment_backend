package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AdminUpdateParamsResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8101394102610606426L;
	
	private String statusCode;
	private List<ParamUpdateInfo> paramList = new ArrayList<ParamUpdateInfo>(0);
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public List<ParamUpdateInfo> getParamList() {
		return paramList;
	}
	public void setParamList(List<ParamUpdateInfo> paramList) {
		this.paramList = paramList;
	}
}
