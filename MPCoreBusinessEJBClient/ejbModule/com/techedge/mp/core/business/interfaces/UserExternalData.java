package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.user.User;

public class UserExternalData implements Serializable {


    /**
     * 
     */
    private static final long serialVersionUID = 5322051642090706000L;

    private long              id;

    private String            provider;

    private String            uuid;

    private User              user;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getProvider() {

        return provider;
    }

    public void setProvider(String provider) {

        this.provider = provider;
    }

    public String getUuid() {

        return uuid;
    }

    public void setUuid(String uuid) {

        this.uuid = uuid;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

}
