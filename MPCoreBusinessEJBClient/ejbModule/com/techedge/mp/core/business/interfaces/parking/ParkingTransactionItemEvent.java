package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;

public class ParkingTransactionItemEvent implements Serializable {

    /**
	 * 
	 */
    private static final long      serialVersionUID = -4852600717112130277L;

    private long                   id;

    private ParkingTransactionItem parkingTransactionItem;

    private Integer                sequenceID;

    private String                 eventType;

    private Double                 eventAmount;

    private String                 transactionResult;

    private String                 errorCode;

    private String                 errorDescription;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public ParkingTransactionItem getParkingTransactionItem() {

        return parkingTransactionItem;
    }

    public void setParkingTransactionItem(ParkingTransactionItem parkingTransactionItem) {

        this.parkingTransactionItem = parkingTransactionItem;
    }

    public Integer getSequenceID() {

        return sequenceID;
    }

    public void setSequenceID(Integer sequenceID) {

        this.sequenceID = sequenceID;
    }

    public String getEventType() {

        return eventType;
    }

    public void setEventType(String eventType) {

        this.eventType = eventType;
    }

    public Double getEventAmount() {

        return eventAmount;
    }

    public void setEventAmount(Double eventAmount) {

        this.eventAmount = eventAmount;
    }

    public String getTransactionResult() {

        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {

        this.transactionResult = transactionResult;
    }

    public String getErrorCode() {

        return errorCode;
    }

    public void setErrorCode(String errorCode) {

        this.errorCode = errorCode;
    }

    public String getErrorDescription() {

        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {

        this.errorDescription = errorDescription;
    }

}
