package com.techedge.mp.core.business.interfaces;

public enum TransactionCategoryType {
    TRANSACTION_BUSINESS("BUSINESS"),
    TRANSACTION_CUSTOMER("CUSTOMER");
    
    private final String value;

    TransactionCategoryType(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value; 
    }
    
    public static TransactionCategoryType getValueOf(String value) {
        for (TransactionCategoryType transactionCategoryType : TransactionCategoryType.values()) {
            if (transactionCategoryType.getValue().equals(value)) {
                return transactionCategoryType;
            }
        }
        
        return null;
    }
}
