package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;

public class ProvinceInfo implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 5566369320288679133L;

    private String            name;

    private String            region;
    
    private String            area;
    
    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getRegion() {

        return region;
    }

    public void setRegion(String region) {

        this.region = region;
    }

    public String getArea() {

        return area;
    }
    
    public void setArea(String area) {

        this.area = area;
    }
}
