package com.techedge.mp.core.business.interfaces.postpaid;

import java.io.Serializable;

public class PostPaidCreateShopTransactionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3535296326931310736L;
	
	private String statusCode;
	private String messageCode;
	private String mpTransactionID;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMpTransactionID() {
		return mpTransactionID;
	}
	public void setMpTransactionID(String mpTransactionID) {
		this.mpTransactionID = mpTransactionID;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	
	
	
	
	
}
