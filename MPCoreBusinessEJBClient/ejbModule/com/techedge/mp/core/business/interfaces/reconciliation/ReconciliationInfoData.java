package com.techedge.mp.core.business.interfaces.reconciliation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ReconciliationInfoData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8065814682228627470L;
	
	private String statusCode;
	private List<ReconciliationInfo> reconciliationInfoList = new ArrayList<ReconciliationInfo>();
	
	
	
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public List<ReconciliationInfo> getReconciliationInfoList() {
		return reconciliationInfoList;
	}
	public void setReconciliationInfoList(List<ReconciliationInfo> reconciliationInfoList) {
		this.reconciliationInfoList = reconciliationInfoList;
	}
}
