package com.techedge.mp.core.business.interfaces.voucher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.techedge.mp.core.business.interfaces.user.User;

public class VoucherTransaction implements Serializable {

    /**
     * 
     */
    private static final long                 serialVersionUID             = -8469081497930088606L;

    private Long                              id;

    private String                            voucherCode;

    private String                            voucherTransactionID;

    private User                              user;

    private String                            acquirerID;

    private Double                            amount;

    private String                            authorizationCode;

    private String                            bankTansactionID;

    private Boolean                           confirmed;

    private Date                              creationTimestamp;

    private String                            currency;

    private Date                              endTimestamp;

    private String                            finalStatusType;

    private Long                              paymentMethodId;

    private String                            paymentMethodType;

    private String                            paymentType;

    private Integer                           reconciliationAttemptsLeft;

    private String                            shopLogin;

    private String                            srcTransactionID;

    private String                            token;

    private String                            voucherStatus;

    private List<VoucherTransactionOperation> voucherOperationList         = new ArrayList<VoucherTransactionOperation>();

    private List<VoucherTransactionEvent>     voucherTransactionEventList  = new ArrayList<VoucherTransactionEvent>();

    private List<VoucherTransactionStatus>    voucherTransactionStatusList = new ArrayList<VoucherTransactionStatus>();

    private PaymentTokenPackage               paymentTokenPackage;
    
    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getVoucherCode() {

        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {

        this.voucherCode = voucherCode;
    }

    public String getAcquirerID() {

        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {

        this.acquirerID = acquirerID;
    }

    public Double getAmount() {

        return amount;
    }

    public void setAmount(Double amount) {

        this.amount = amount;
    }

    public String getAuthorizationCode() {

        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {

        this.authorizationCode = authorizationCode;
    }

    public String getBankTansactionID() {

        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {

        this.bankTansactionID = bankTansactionID;
    }

    public Boolean getConfirmed() {

        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {

        this.confirmed = confirmed;
    }

    public Date getCreationTimestamp() {

        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {

        this.creationTimestamp = creationTimestamp;
    }

    public String getCurrency() {

        return currency;
    }

    public void setCurrency(String currency) {

        this.currency = currency;
    }

    public Date getEndTimestamp() {

        return endTimestamp;
    }

    public void setEndTimestamp(Date endTimestamp) {

        this.endTimestamp = endTimestamp;
    }

    public String getFinalStatusType() {

        return finalStatusType;
    }

    public void setFinalStatusType(String finalStatusType) {

        this.finalStatusType = finalStatusType;
    }

    public Long getPaymentMethodId() {

        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {

        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {

        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {

        this.paymentMethodType = paymentMethodType;
    }

    public String getPaymentType() {

        return paymentType;
    }

    public void setPaymentType(String paymentType) {

        this.paymentType = paymentType;
    }

    public Integer getReconciliationAttemptsLeft() {

        return reconciliationAttemptsLeft;
    }

    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {

        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }

    public String getShopLogin() {

        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {

        this.shopLogin = shopLogin;
    }

    public String getSrcTransactionID() {

        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {

        this.srcTransactionID = srcTransactionID;
    }

    public String getToken() {

        return token;
    }

    public void setToken(String token) {

        this.token = token;
    }

    public String getVocherTransactionID() {

        return voucherTransactionID;
    }

    public void setVoucherTransactionID(String voucherTransactionID) {

        this.voucherTransactionID = voucherTransactionID;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public String getVoucherStatus() {

        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {

        this.voucherStatus = voucherStatus;
    }

    public List<VoucherTransactionOperation> getVoucherOperationList() {

        return voucherOperationList;
    }

    public void setVoucherOperationList(List<VoucherTransactionOperation> voucherOperationList) {

        this.voucherOperationList = voucherOperationList;
    }

    public List<VoucherTransactionEvent> getVoucherTransactionEventList() {

        return voucherTransactionEventList;
    }

    public void setVoucherTransactionEventList(List<VoucherTransactionEvent> voucherTransactionEventList) {

        this.voucherTransactionEventList = voucherTransactionEventList;
    }

    public List<VoucherTransactionStatus> getVoucherTransactionStatusList() {

        return voucherTransactionStatusList;
    }

    public void setVoucherTransactionStatusList(List<VoucherTransactionStatus> voucherTransactionStatusList) {

        this.voucherTransactionStatusList = voucherTransactionStatusList;
    }

    public PaymentTokenPackage getPaymentTokenPackage() {
    
        return paymentTokenPackage;
    }

    public void setPaymentTokenPackage(PaymentTokenPackage paymentTokenPackage) {
    
        this.paymentTokenPackage = paymentTokenPackage;
    }

}
