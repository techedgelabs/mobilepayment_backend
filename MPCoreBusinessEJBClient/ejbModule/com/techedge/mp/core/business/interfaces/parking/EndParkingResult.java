package com.techedge.mp.core.business.interfaces.parking;

import java.io.Serializable;
import java.math.BigDecimal;

public class EndParkingResult extends BaseParkingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6631420076533119275L;

    private String            parkingId;

    private BigDecimal        finalPrice;
    

    public String getParkingId() {

        return parkingId;
    }

    public void setParkingId(String parkingId) {

        this.parkingId = parkingId;
    }

    public BigDecimal getFinalPrice() {

        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {

        this.finalPrice = finalPrice;
    }

}
