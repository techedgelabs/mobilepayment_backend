package com.techedge.mp.core.business.interfaces.crm;

import java.util.Date;

public class CRMSfNotifyEvent {

    private String  requestId;

    private String  fiscalCode;

    private Date    date;

    private String  stationId;

    private String  productId;

    private Boolean paymentFlag;

    private Integer credits;

    private Double  quantity;

    private String  refuelMode;

    private String  firstName;

    private String  lastName;

    private String  email;

    private Date    birthDate;

    private Boolean notificationFlag;

    private Boolean paymentCardFlag;

    private String  brand;

    private String  cluster;

    private Double  amount;

    private Boolean privacyFlag1;

    private Boolean privacyFlag2;

    private String  mobilePhone;

    private String  loyaltyCard;

    private String  eventType;

    private String  parameter1;

    private String  parameter2;

    private String  paymentMode;

    public CRMSfNotifyEvent() {

    }
    
    public CRMSfNotifyEvent(String requestId, String fiscalCode, Date date, String stationId, String productId, Boolean paymentFlag, Integer credits, Double quantity, String refuelMode, String firstName, String lastName, String email, Date birthDate, Boolean notificationFlag, Boolean paymentCardFlag, String brand, String cluster, Double amount, Boolean privacyFlag1, Boolean privacyFlag2, String mobilePhone, String loyaltyCard, String eventType, String parameter1, String parameter2, String paymentMode) {

        this.requestId = requestId;
        this.fiscalCode = fiscalCode;
        this.date = date;
        this.stationId = stationId;
        this.productId = productId;
        this.paymentFlag = paymentFlag;
        this.credits = credits;
        this.quantity = quantity;
        this.refuelMode = refuelMode;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthDate = birthDate;
        this.notificationFlag = notificationFlag;
        this.paymentCardFlag = paymentCardFlag;
        this.brand = brand;
        this.cluster = cluster;
        this.amount = amount;
        this.privacyFlag1 = privacyFlag1;
        this.privacyFlag2 = privacyFlag2;
        this.mobilePhone = mobilePhone;
        this.loyaltyCard = loyaltyCard;
        this.eventType = eventType;
        this.parameter1 = parameter1;
        this.parameter2 = parameter2;
        this.paymentMode = paymentMode;
    }



    public String getRequestId() {

        return requestId;
    }

    public void setRequestId(String requestId) {

        this.requestId = requestId;
    }

    public String getFiscalCode() {

        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {

        this.fiscalCode = fiscalCode;
    }

    public Date getDate() {

        return date;
    }

    public void setDate(Date date) {

        this.date = date;
    }

    public String getStationId() {

        return stationId;
    }

    public void setStationId(String stationId) {

        this.stationId = stationId;
    }

    public String getProductId() {

        return productId;
    }

    public void setProductId(String productId) {

        this.productId = productId;
    }

    public Boolean getPaymentFlag() {

        return paymentFlag;
    }

    public void setPaymentFlag(Boolean paymentFlag) {

        this.paymentFlag = paymentFlag;
    }

    public Integer getCredits() {

        return credits;
    }

    public void setCredits(Integer credits) {

        this.credits = credits;
    }

    public Double getQuantity() {

        return quantity;
    }

    public void setQuantity(Double quantity) {

        this.quantity = quantity;
    }

    public String getRefuelMode() {

        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {

        this.refuelMode = refuelMode;
    }

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    public String getLastName() {

        return lastName;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public Date getBirthDate() {

        return birthDate;
    }

    public void setBirthDate(Date birthDate) {

        this.birthDate = birthDate;
    }

    public Boolean getNotificationFlag() {

        return notificationFlag;
    }

    public void setNotificationFlag(Boolean notificationFlag) {

        this.notificationFlag = notificationFlag;
    }

    public Boolean getPaymentCardFlag() {

        return paymentCardFlag;
    }

    public void setPaymentCardFlag(Boolean paymentCardFlag) {

        this.paymentCardFlag = paymentCardFlag;
    }

    public String getBrand() {

        return brand;
    }

    public void setBrand(String brand) {

        this.brand = brand;
    }

    public String getCluster() {

        return cluster;
    }

    public void setCluster(String cluster) {

        this.cluster = cluster;
    }

    public Double getAmount() {

        return amount;
    }

    public void setAmount(Double amount) {

        this.amount = amount;
    }

    public Boolean getPrivacyFlag1() {

        return privacyFlag1;
    }

    public void setPrivacyFlag1(Boolean privacyFlag1) {

        this.privacyFlag1 = privacyFlag1;
    }

    public Boolean getPrivacyFlag2() {

        return privacyFlag2;
    }

    public void setPrivacyFlag2(Boolean privacyFlag2) {

        this.privacyFlag2 = privacyFlag2;
    }

    public String getMobilePhone() {

        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {

        this.mobilePhone = mobilePhone;
    }

    public String getLoyaltyCard() {

        return loyaltyCard;
    }

    public void setLoyaltyCard(String loyaltyCard) {

        this.loyaltyCard = loyaltyCard;
    }

    public String getEventType() {

        return eventType;
    }

    public void setEventType(String eventType) {

        this.eventType = eventType;
    }

    public String getParameter1() {

        return parameter1;
    }

    public void setParameter1(String parameter1) {

        this.parameter1 = parameter1;
    }

    public String getParameter2() {

        return parameter2;
    }

    public void setParameter2(String parameter2) {

        this.parameter2 = parameter2;
    }

    public String getPaymentMode() {

        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {

        this.paymentMode = paymentMode;
    }

}
