package com.techedge.mp.core.business.interfaces;

public class StatusHelper {

    public final static String  PUMP_ERROR_PUMP_BUSY                                                 = "PUMP_BUSY_401";
    public final static String  PUMP_ERROR_PUMP_NOT_AVAILABLE                                        = "PUMP_NOT_AVAILABLE_501";
    public final static String  PUMP_ERROR_PUMP_STATUS_NOT_AVAILABLE                                 = "PUMP_STATUS_NOT_AVAILABLE_502";
    public final static String  PUMP_ERROR_PUMP_NOT_FOUND_404                                        = "PUMP_NOT_FOUND_404";
    public final static String  PUMP_ERROR_STATION_NOT_FOUND                                         = "STATION_NOT_FOUND_404";
    public final static String  PUMP_ERROR_STATION_PUMP_NOT_FOUND                                    = "STATION_PUMP_NOT_FOUND_404";

    public final static String  PUMP_AVAILABLE_PUMP_AVAILABLE                                        = "PUMP_AVAILABLE_200";

    public final static String  PAYMENT_AUTHORIZED_TRANSACTION_RESULT                                = "TransactionResult=OK";

    public final static String  PAYMENT_NOT_AUTHORIZED_TRANSACTION_RESULT                            = "TransactionResult=KO";

    public final static String  PAYMENT_AUTHORIZATION_DELETED_TRANSACTION_RESULT                     = "TransactionResult=OK";

    public final static String  PAYMENT_AUTHORIZATION_NOT_DELETED_TRANSACTION_RESULT                 = "TransactionResult=KO";

    public final static String  PUMP_ENABLED_PUMP_ENABLED                                            = "PUMP_ENABLED_200";

    public final static String  PUMP_NOT_ENABLED_PUMP_BUSY                                           = "PUMP_BUSY_401";
    public final static String  PUMP_NOT_ENABLED_PUMP_NOT_AVAILABLE                                  = "PUMP_NOT_AVAILABLE_501";
    public final static String  PUMP_NOT_ENABLED_PUMP_STATUS_NOT_AVAILABLE                           = "PUMP_STATUS_NOT_AVAILABLE_502";
    public final static String  PUMP_NOT_ENABLED_PUMP_NOT_FOUND                                      = "PUMP_NOT_FOUND_404";
    public final static String  PUMP_NOT_ENABLED_STATION_NOT_FOUND                                   = "STATION_NOT_FOUND_404";
    public final static String  PUMP_NOT_ENABLED_STATION_PUMP_NOT_FOUND                              = "STATION_PUMP_NOT_FOUND_404";

    public final static String  TRANSACTION_STATUS_REQUEST_OK_REFUEL_NOT_STARTED                     = "REFUEL_NOT_STARTED_400";
    public final static String  TRANSACTION_STATUS_REQUEST_OK_REFUEL_INPROGRESS                      = "REFUEL_INPROGRESS_401";
    public final static String  TRANSACTION_STATUS_REQUEST_OK_REFUEL_TERMINATED                      = "REFUEL_TERMINATED_200";
    
    public final static String  TRANSACTION_RECONCILIATION_STATUS_REFUEL_ENABLED                     = "REFUEL_ENABLED_200";
    public final static String  TRANSACTION_RECONCILIATION_STATUS_REFUEL_STARTED                     = "REFUEL_STARTED_201";
    public final static String  TRANSACTION_RECONCILIATION_STATUS_REFUEL_ENDED                       = "REFUEL_ENDED_202";
    public final static String  TRANSACTION_RECONCILIATION_STATUS_REFUEL_ENABLED_TIMEOUT             = "TIMEOUT_AFTER_PUMP_ENABLE_400";
    public final static String  TRANSACTION_RECONCILIATION_STATUS_REFUEL_STARTED_TIMEOUT             = "TIMEOUT_AFTER_START_REFUEL_400";
    

    public final static String  TRANSACTION_STATUS_REQUEST_KO_TRANSACTION_NOT_RECOGNIZED             = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PAYMENT_TRANSACTION_CLOSED_TRANSACTION_RESULT                        = "TransactionResult=OK";

    public final static String  PAYMENT_TRANSACTION_NOT_CLOSED_TRANSACTION_RESULT                    = "TransactionResult=KO";

    /*******************************/
    public final static Boolean CHECK_UNDO_OPERATION_FOUND                                           = true;
    public final static Boolean CHECK_UNDO_OPERATION_NOT_FOUND                                       = false;

    public final static String  OPERATION_USER_CANCEL_REFUEL_PREPAYMENT                              = "USER_CANCEL_REFUEL_PREPAYMENT";
    public final static String  OPERATION_USER_CANCEL_REFUEL_POSTPAYMENT                             = "USER_CANCEL_REFUEL_POSTPAYMENT";

    public final static Boolean UPDATE_TRANSACTION_OK                                                = true;
    public final static Boolean UPDATE_TRANSACTION_KO                                                = false;

    public final static String  PERSIST_START_REFUEL_RECEIVED_SUCCESS                                = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_START_REFUEL_RECEIVED_ERROR                                  = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PERSIST_END_REFUEL_RECEIVED_SUCCESS                                  = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_END_REFUEL_RECEIVED_ERROR                                    = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PERSIST_TRANSACTION_STATUS_REQUEST_KO_SUCCESS                        = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_TRANSACTION_STATUS_REQUEST_KO_ERROR                          = "REFUEL_STATUS_NOT_AVAILABLE_500";

    public final static String  PERSIST_NEW_STATUS_SUCCESS                                           = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_NEW_STATUS_ERROR                                             = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PERSIST_PUMP_AVAILABILITY_STATUS_SUCCESS                             = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_PUMP_AVAILABILITY_STATUS_ERROR                               = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PERSIST_PUMP_ENABLE_STATUS_SUCCESS                                   = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_PUMP_ENABLE_STATUS_ERROR                                     = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PERSIST_PUMP_GENERIC_FAULT_STATUS_SUCCESS                            = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_PUMP_GENERIC_FAULT_STATUS_ERROR                              = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PERSIST_SEND_PAYMENT_TRANSACTION_STATUS_SUCCESS                      = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_SEND_PAYMENT_TRANSACTION_STATUS_ERROR                        = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PERSIST_PAYMENT_AUTHORIZATION_STATUS_SUCCESS                         = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_PAYMENT_AUTHORIZATION_STATUS_ERROR                           = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PERSIST_PAYMENT_DELETION_STATUS_SUCCESS                              = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_PAYMENT_DELETION_STATUS_ERROR                                = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PERSIST_PAYMENT_COMPLETION_STATUS_SUCCESS                            = "MESSAGE_RECEIVED_200";
    public final static String  USER_TRANSACTION_AUTH_FAILURE                                        = "TRANSACTION_UNAUTHORIZED_300";
    public final static String  PERSIST_PAYMENT_COMPLETION_STATUS_ERROR                              = "TRANSACTION_NOT_RECOGNIZED_400";
    
    public final static String  PERSIST_TRANSACTION_OPERATION_SUCCESS                                = "MESSAGE_RECEIVED_200";
    public final static String  PERSIST_TRANSACTION_OPERATION_ERROR                                  = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  GFG_NOTIFICATION_SUCCESS                                             = "MESSAGE_RECEIVED_200";
    public final static String  GFG_NOTIFICATION_MESSAGE_REJECTED                                    = "MESSAGE_REJECTED_500";
    public final static String  GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE                         = "SOURCE_STATUS_NOT_AVAILABLE_500";
    public final static String  GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED                          = "TRANSACTION_NOT_RECOGNIZED_400";
    public final static String  GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE                       = "SOURCE_RESPONSE_NOT_AVAILABLE_500";
    public final static String  GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED                       = "TRANSACTION_CANCELLED_500";
    public final static String  GFG_NOTIFICATION_MESSAGE_ERROR                                       = "MESSAGE_ERROR_500";

    public final static String  POST_PAID_TRANSACTION_STATUS_OK                                      = "MESSAGE_RECEIVED_200";
    public final static String  POST_PAID_TRANSACTION_STATUS_NOT_RECOGNIZED                          = "TRANSACTION_NOT_RECOGNIZED_400";
    public final static String  POST_PAID_TRANSACTION_STATUS_NOT_AVAILABLE                           = "SOURCE_STATUS_NOT_AVAILABLE_500";
    public final static String  POST_PAID_TRANSACTION_STATUS_KO                                      = "MESSAGE_REJECTED_500";

    public final static String  STATION_DETAIL_STATUS_OK                                             = "MESSAGE_RECEIVED_200";
    public final static String  STATION_DETAIL_STATUS_KO                                             = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  ENABLE_PUMP_STATUS_OK                                                = "MESSAGE_RECEIVED_200";
    public final static String  ENABLE_PUMP_STATUS_KO                                                = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  TRANSACTION_RECONCILIATION_STATUS_OK                                 = "MESSAGE_RECEIVED_200";
    public final static String  TRANSACTION_RECONCILIATION_STATUS_KO                                 = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  SEND_PAYMENT_TRANSACTION_STATUS_OK                                   = "MESSAGE_RECEIVED_200";
    public final static String  SEND_PAYMENT_TRANSACTION_STATUS_KO                                   = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  PAYMENT_STATUS_OK                                                    = "MESSAGE_RECEIVED_200";
    public final static String  PAYMENT_STATUS_KO                                                    = "TRANSACTION_NOT_RECOGNIZED_400";

    /********************************/
    public final static String  STATUS_NEW_REFUELING_REQUEST                                         = "0001";
    public final static String  STATUS_PAYMENT_AUTHORIZED                                            = "0002";
    public final static String  STATUS_PAYMENT_NOT_AUTHORIZED                                        = "0003";
    public final static String  STATUS_DELETE_REFUELING_REQUEST                                      = "0004";
    public final static String  STATUS_PAYMENT_AUTHORIZATION_DELETED                                 = "0005";
    public final static String  STATUS_PAYMENT_AUTHORIZATION_NOT_DELETED                             = "0006";
    public final static String  STATUS_PUMP_ENABLED                                                  = "0007";
    public final static String  STATUS_PUMP_NOT_ENABLED                                              = "0008";
    public final static String  STATUS_START_REFUEL_RECEIVED                                         = "0009";
    public final static String  STATUS_END_REFUEL_RECEIVED                                           = "0010";
    public final static String  STATUS_TRANSACTION_STATUS_REQUEST_OK                                 = "0011";
    public final static String  STATUS_TRANSACTION_STATUS_REQUEST_KO                                 = "0012";
    public final static String  STATUS_PAYMENT_TRANSACTION_CLOSED                                    = "0013";
    public final static String  STATUS_PAYMENT_TRANSACTION_NOT_CLOSED                                = "0014";
    public final static String  STATUS_PAYMENT_TRANSACTION_DELETED                                   = "0015";
    public final static String  STATUS_PAYMENT_TRANSACTION_NOT_DELETED                               = "0016";
    public final static String  STATUS_PUMP_GENERIC_FAULT                                            = "0017";
    public final static String  STATUS_TRANSACTION_ABEND                                             = "0018";

    public final static String  SUBSTATUS_PUMP_AVAILABLE                                             = "PUMP_AVAILABLE_200";

    public final static String  SUBSTATUS_PUMP_BUSY                                                  = "PUMP_BUSY_401";
    public final static String  SUBSTATUS_PUMP_NOT_AVAILABLE                                         = "PUMP_NOT_AVAILABLE_501";
    public final static String  SUBSTATUS_PUMP_STATUS_NOT_AVAILABLE                                  = "PUMP_STATUS_NOT_AVAILABLE_500";
    public final static String  SUBSTATUS_PUMP_NOT_FOUND                                             = "PUMP_NOT_FOUND_404";
    public final static String  SUBSTATUS_STATION_NOT_FOUND                                          = "STATION_NOT_FOUND_404";
    public final static String  SUBSTATUS_STATION_PUMP_NOT_FOUND                                     = "STATION_PUMP_NOT_FOUND_404";
    public final static String  SUBSTATUS_STATION_PUMP_GENERIC_FAULT                                 = "GENERIC_FAULT_500";

    public final static String  SUBSTATUS_REFUEL_NOT_STARTED                                         = "REFUEL_NOT_STARTED_400";
    public final static String  SUBSTATUS_REFUEL_INPROGRESS                                          = "REFUEL_INPROGRESS_401";
    public final static String  SUBSTATUS_REFUEL_TERMINATED                                          = "REFUEL_TERMINATED_200";

    public final static String  SUBSTATUS_PUMP_ENABLED                                               = "PUMP_ENABLED_200";

    public final static String  SUBSTATUS_PUMP_GENERIC_FAULT                                         = "GENERIC_FAULT_500";

    public final static String  SUBSTATUS_SEND_PAYMENT_TRANSACTION_STATUS_OK                         = "MESSAGE_RECEIVED_200";
    public final static String  SUBSTATUS_SEND_PAYMENT_TRANSACTION_STATUS_KO                         = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_INSUFFICIENT_CREDIT                  = "PAY_AUTHORIZATION_INSUFFICIENT_CREDIT";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_REFUSED                              = "PAY_AUTHORIZATION_REFUSED";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_FAULT                                = "PAY_AUTHORIZATION_FAULT";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_CAP_LIMIT_REACH                      = "PAY_AUTHORIZATION_CAP_LIMIT_REACH";

    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_PUMP_ERROR                   = "PAYMENT_AUTHORIZATION_DELETED_PUMP_ERROR";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_PUMP_ERROR_DESCRIPTION       = "Payment aythorization deleted pump error";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_USER_REQUEST                 = "PAYMENT_AUTHORIZATION_DELETED_USER_REQUEST";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_USER_REQUEST_DESCRIPTION     = "Payment aythorization deleted user request";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_FAULT                        = "PAYMENT_AUTHORIZATION_DELETED_FAULT";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_FAULT_DESCRIPTION            = "Fault (application error or system error)";
    public final static String  SUBSTATUS_PAYMENT_FAULT_DESCRIPTION                                  = "Fault (application error or system error)";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_REFUSED                      = "PAYMENT_AUTHORIZATION_DELETED_REFUSED";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_DELETED_REFUSED_DESCRIPTION          = "Payment aythorization refused";

    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_NOT_DELETED_REFUSED                  = "PAYMENT_AUTHORIZATION_NOT_DELETED_REFUSED";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_NOT_DELETED_REFUSED_DESCRIPTION      = "Negative Response (TransactionResult=KO )";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_NOT_DELETED_FAULT                    = "PAYMENT_AUTHORIZATION_NOT_DELETED_FAULT";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_NOT_DELETED_FAULT_DESCRIPTION        = "Fault (application error or system error)";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_NOT_DELETED_USER_REQUEST             = "PAYMENT_AUTHORIZATION_NOT_DELETED_USER_REQUEST";
    public final static String  SUBSTATUS_PAYMENT_AUTHORIZATION_NOT_DELETED_USER_REQUEST_DESCRIPTION = "Payment aythorization not deleted user request";

    public final static String  SUBSTATUS_PAYMENT_TRANSACTION_DELETED_REFUSED                        = "PAY_TRANSACTION_DEL_REFUSED";
    public final static String  SUBSTATUS_PAYMENT_TRANSACTION_DELETED_FAULT                          = "PAY_TRANSACTION_DEL_FAULT";

    public final static String  SUBSTATUS_PAYMENT_TRANSACTION_NOT_CLOSED_REFUSED                     = "PAY_TRANSACTION_NOT_CLOSED_REFUSED";
    public final static String  SUBSTATUS_PAYMENT_TRANSACTION_NOT_CLOSED_FAULT                       = "PAY_TRANSACTION_DEL_FAULT";
    public final static String  SUBSTATUS_PAYMENT_TRANSACTION_CONSUME_VOUCHER_FAULT                  = "PAY_TRANSACTION_CONSUME_VOUCHER_FAULT";

    public final static String  SUBSTATUS_PAYMENT_TRANSACTION_RECONCILE                              = "PAY_TRANSACTION_RECONCILE";

    public final static String  SUBSTATUS_MESSAGE_RECEIVED                                           = "MESSAGE_RECEIVED_200";

    public final static String  FINAL_STATUS_TYPE_SUCCESSFUL                                         = "SUCCESSFUL";
    public final static String  FINAL_STATUS_TYPE_FAILED                                             = "FAILED";
    public final static String  FINAL_STATUS_TYPE_ERROR                                              = "ERROR";
    public final static String  FINAL_STATUS_TYPE_RUNNING                                            = "RUNNING";
    public final static String  FINAL_STATUS_TYPE_MISSING_NOTIFICATION                               = "MISSING_NOTIFICATION";
    public final static String  FINAL_STATUS_TYPE_MISSING_PAYMENT                                    = "MISSING_PAYMENT";
    public final static String  FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL               = "MISSING_PAYAUTH_DELETE_BEFORE_REFUEL";
    public final static String  FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_AFTER_REFUEL                = "MISSING_PAYAUTH_DELETE_AFTER_REFUEL";
    public final static String  FINAL_STATUS_TYPE_NOT_RECONCILIABLE                                  = "NOT_RECONCILIABLE";
    public final static String  FINAL_STATUS_TYPE_ABEND                                              = "ABEND";
    public final static String  FINAL_STATUS_TYPE_REVERSED                                           = "REVERSED";

    public final static String  POST_PAID_EVENT_GFG_CREATE                                           = "GFG_CREATE";
    public final static String  POST_PAID_EVENT_GFG_ENABLE                                           = "GFG_ENABLE";
    public final static String  POST_PAID_EVENT_MA_PAY                                               = "MA_PAY";
    public final static String  POST_PAID_EVENT_MA_REJECT                                            = "MA_REJECT";
    public final static String  POST_PAID_EVENT_BE_AUTH                                              = "BE_AUTH";
    public final static String  POST_PAID_EVENT_BE_AUTH_DEL                                          = "BE_AUTH_DEL";
    public final static String  POST_PAID_EVENT_BE_MOV                                               = "BE_MOV";
    //public final static String  POST_PAID_EVENT_BE_MOV_DEL                                           = "BE_MOV_DEL";
    public final static String  POST_PAID_EVENT_GFG_REVERSE                                          = "GFG_REVERSE";                                   //Storno
    public final static String  POST_PAID_EVENT_BE_REVERSE                                           = "BE_REVERSE";
    public final static String  POST_PAID_EVENT_BE_NOTIFICATION                                      = "BE_NOTIFICATION";
    public final static String  POST_PAID_EVENT_BE_RECONCILIATION                                    = "BE_RECONCILIATION";
    public final static String  POST_PAID_EVENT_BE_LOYALTY                                           = "BE_LOYALTY";
    public final static String  POST_PAID_EVENT_BE_LOYALTY_REVERSE                                   = "BE_LOYALTY_REVERSE";
    public final static String  POST_PAID_EVENT_BE_VOUCHER                                           = "BE_VOUCHER";
    public final static String  POST_PAID_EVENT_BE_VOUCHER_PREAUTH                                   = "BE_VOUCHER_PREAUTH";
    public final static String  POST_PAID_EVENT_BE_VOUCHER_PREAUTH_DEL                               = "BE_VOUCHER_PREAUTH_DEL";
    public final static String  POST_PAID_EVENT_BE_CREATE_AND_CONSUME_VOUCHER                        = "BE_CREATE_AND_CONSUME_VOUCHER";
    public final static String  POST_PAID_EVENT_BE_VOUCHER_REVERSE                                   = "BE_VOUCHER_REVERSE";
    public final static String  POST_PAID_EVENT_BE_CREATE_AND_CONSUME_VOUCHER_REVERSE                = "BE_CREATE_AND_CONSUME_VOUCHER_REVERSE";

    public final static String  POST_PAID_EVENT_TYPE_STANDARD                                        = "STANDARD";
    public final static String  POST_PAID_EVENT_TYPE_RECONCILIATION                                  = "RECONCILIATION";

    public final static String  POST_PAID_STATUS_CREATED                                             = "0001";
    public final static String  POST_PAID_STATUS_PAY_ABLE                                            = "0002";
    public final static String  POST_PAID_STATUS_PAY_REQU                                            = "0003";
    public final static String  POST_PAID_STATUS_PAY_AUTH                                            = "0004";
    public final static String  POST_PAID_STATUS_CANCELED                                            = "0005";
    public final static String  POST_PAID_STATUS_REFUND                                              = "0006";
    public final static String  POST_PAID_STATUS_PAY_AUTH_DELETE                                     = "0007";
    public final static String  POST_PAID_STATUS_PAY_MOV                                             = "0008";
    public final static String  POST_PAID_STATUS_PAY_MOV_DELETE                                      = "0009";
    public final static String  POST_PAID_STATUS_PAY_MOV_REVERSE                                     = "0010";
    public final static String  POST_PAID_STATUS_PAY_NO_VOUCHER                                      = "0011";
    public final static String  POST_PAID_STATUS_RECONCILIATION_REQU                                 = "0012";
    //public final static String  POST_PAID_STATUS_PAY_DEL_AUTH                                        = "0008";
    //public final static String  POST_PAID_STATUS_PAY_DEL_AUTH_2                                      = "0009";
    //public final static String  POST_PAID_STATUS_PAY_INCOMPLETE_CANCEL                               = "0010";
    //public final static String  POST_PAID_STATUS_PAY_INCOMPLETE_MOV                                  = "0011";
    public final static String  POST_PAID_STATUS_PAY_COMPLETE                                        = "0013";
    public final static String  POST_PAID_STATUS_CREDITS_LOAD                                        = "0014";
    public final static String  POST_PAID_STATUS_PAY_FULL_VOUCHER                                    = "0015";
    public final static String  POST_PAID_STATUS_PAY_PARTIAL_VOUCHER                                 = "0016";
    public final static String  POST_PAID_STATUS_PAY_VOUCHER_FAIL                                    = "0017";
    public final static String  POST_PAID_STATUS_PAY_AUTH_MISSING                                    = "0018";
    public final static String  POST_PAID_STATUS_NOTIFICATION_FAIL                                   = "0019";
    public final static String  POST_PAID_STATUS_VOUCHER_REVERSE                                     = "0020";
    public final static String  POST_PAID_STATUS_LOYALTY_REVERSE                                     = "0021";
    public final static String  POST_PAID_STATUS_NOTIFICATION_SENT_AGAIN                             = "0022";
    public final static String  POST_PAID_STATUS_PAY_AUTH_REFUSED                                    = "0023";
    public final static String  POST_PAID_STATUS_PAY_AUTH_DELETE_MISSING                             = "0024";
    public final static String  POST_PAID_STATUS_VOUCHER_CREATED_AND_CONSUMED                        = "0025";
    public final static String  POST_PAID_STATUS_VOUCHER_PREAUTH                                     = "0026";
    public final static String  POST_PAID_STATUS_VOUCHER_PREAUTH_DEL                                 = "0027";
    
    
    public final static String  POST_PAID_FINAL_STATUS_PAID                                          = "PAID";                                          //0013
    public final static String  POST_PAID_FINAL_STATUS_ONHOLD                                        = "ONHOLD";                                        //0010
    public final static String  POST_PAID_FINAL_STATUS_UNPAID                                        = "UNPAID";                                        //0014*
    public final static String  POST_PAID_FINAL_STATUS_CANCELLED                                     = "CANCELLED";                                     //0015
    public final static String  POST_PAID_FINAL_STATUS_REVERSED                                      = "REVERSED";                                      //0015
    public final static String  POST_PAID_FINAL_STATUS_ABEND                                         = "ABEND";                                         //0016
    public final static String  POST_PAID_FINAL_STATUS_NOT_RECONCILIABLE                             = "NOT_RECONCILIABLE";

    
    public final static String  VOUCHER_STATUS_OK                                                    = "I0001";
    public final static String  VOUCHER_STATUS_OK_AFTER_RECONCILE                                    = "IR001";
    public final static String  VOUCHER_STATUS_NOT_APPLICABLE                                        = "I0002";
    public final static String  VOUCHER_STATUS_NOT_APPLICABLE_AFTER_RECONCILE                        = "IR002";
    public final static String  VOUCHER_STATUS_NOT_FOUND                                             = "I0003";
    public final static String  VOUCHER_STATUS_TO_BE_VERIFIED                                        = "W0001";
    public final static String  VOUCHER_STATUS_ERROR                                                 = "ER001";

    public final static String  LOYALTY_STATUS_OK                                                    = "L0001";
    public final static String  LOYALTY_STATUS_OK_AFTER_RECONCILE                                    = "LR001";
    public final static String  LOYALTY_STATUS_NOT_APPLICABLE                                        = "L0002";
    public final static String  LOYALTY_STATUS_NOT_APPLICABLE_AFTER_RECONCILE                        = "LR002";
    public final static String  LOYALTY_STATUS_NOT_FOUND                                             = "L0003";
    public final static String  LOYALTY_STATUS_TO_BE_VERIFIED                                        = "Y0001";
    public final static String  LOYALTY_STATUS_ERROR                                                 = "FR001";
    
    public final static String  POST_PAID_VOUCHER_ABEND                                              = "ABEND";
    
    public final static String  VOUCHER_FINAL_STATUS_FAILED_PAY_AUTH                                 = "FAILED_PAY_AUTH";
    public final static String  VOUCHER_FINAL_STATUS_ERROR_PAY_AUTH                                  = "ERROR_PAY_AUTH";
    public final static String  VOUCHER_FINAL_STATUS_ERROR_CREATE                                    = "ERROR_CREATE";
    public final static String  VOUCHER_FINAL_STATUS_SUCCESSFUL                                      = "SUCCESSFUL";
    public final static String  VOUCHER_FINAL_STATUS_CANCELED                                        = "CANCELED";
    public final static String  VOUCHER_FINAL_STATUS_ERROR_PAY_MOV                                   = "ERROR_PAY_MOV";
    public final static String  VOUCHER_FINAL_STATUS_ERROR_DELETE                                    = "ERROR_DELETE";
    public final static String  VOUCHER_FINAL_STATUS_ERROR_PAY_CAN                                   = "ERROR_PAY_CAN";
    public final static String  VOUCHER_FINAL_STATUS_FAILED_CREATE                                   = "FAILED_CREATE";
    public final static String  VOUCHER_FINAL_STATUS_FAILED_PAY_MOV                                  = "FAILED_PAY_MOV";
    public final static String  VOUCHER_FINAL_STATUS_NOT_RECONCILIABLE                               = "NOT_RECONCILIABLE";
    
    public final static String  VOUCHER_STATUS_PURCHASE_START                                        = "0000";
    public final static String  VOUCHER_STATUS_AUTH_OK                                               = "0001";
    public final static String  VOUCHER_STATUS_AUTH_KO                                               = "0002";
    public final static String  VOUCHER_STATUS_AUTH_ERROR                                            = "0003";
    public final static String  VOUCHER_STATUS_CREATE_OK                                             = "0004";
    public final static String  VOUCHER_STATUS_CREATE_KO                                             = "0005";
    public final static String  VOUCHER_STATUS_CREATE_ERROR                                          = "0006";
    public final static String  VOUCHER_STATUS_MOV_OK                                                = "0007";
    public final static String  VOUCHER_STATUS_MOV_KO                                                = "0008";
    public final static String  VOUCHER_STATUS_MOV_ERROR                                             = "0009";
    public final static String  VOUCHER_STATUS_DELETE_OK                                             = "0010";
    public final static String  VOUCHER_STATUS_DELETE_ERROR                                          = "0011";
    public final static String  VOUCHER_STATUS_CAN_OK                                                = "0012";
    public final static String  VOUCHER_STATUS_CAN_ERROR                                             = "0013";
    public final static String  VOUCHER_STATUS_STO_OK                                                = "0014";
    public final static String  VOUCHER_STATUS_STO_KO                                                = "0015";
    public final static String  VOUCHER_STATUS_STO_ERROR                                             = "0016";
    public final static String  VOUCHER_STATUS_RECONCILIATION                                        = "0017";
    
    public final static String  DWH_EVENT_STATUS_SUCCESS                                             = "SUCCESS";
    public final static String  DWH_EVENT_STATUS_ERROR                                               = "ERROR";
    public final static String  DWH_EVENT_STATUS_FAILED                                              = "FAILED";
    
    public final static String  PUSH_NOTIFICATION_STATUS_DELIVERED                                   = "0";
    public final static String  PUSH_NOTIFICATION_STATUS_DELETED                                     = "1";
    public final static String  PUSH_NOTIFICATION_STATUS_PENDING                                     = "2";
    public final static String  PUSH_NOTIFICATION_STATUS_ERROR                                       = "3";
    public final static String  PUSH_NOTIFICATION_STATUS_ARN_NOT_FOUND                               = "4";

    public final static String  CRM_EVENT_STATUS_SUCCESS                                             = "SUCCESS";
    public final static String  CRM_EVENT_STATUS_ERROR                                               = "ERROR";
    public final static String  CRM_EVENT_STATUS_FAILED                                              = "FAILED";
    public static final String  CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR                                  = "ERROR";
    public static final String  CRM_OFFER_VOUCHER_PROMOTIONAL_SUCCESS                                = "SUCCESS";
    
    public final static String  REDEMPTION_STATUS_SUCCESS                                            = "SUCCESS";
    public final static String  REDEMPTION_STATUS_ERROR                                              = "ERROR";
    
    public final static String  PARKING_STATUS_CREATED                                               = "CREATED";
    public final static String  PARKING_STATUS_STARTED                                               = "STARTED";
    public final static String  PARKING_STATUS_ENDED                                                 = "ENDED";
    public final static String  PARKING_STATUS_FAILED                                                = "FAILED";
    public final static String  PARKING_STATUS_NOT_RECONCILIABLE                                     = "NOT_RECONCILIABLE";
    
    public final static String  GFG_ELECTRONIC_INVOICE_ID_SUCCESS                                    = "MESSAGE_RECEIVED_200";
    public final static String  GFG_LECTRONIC_INVOICE_ID_TRANSACTION_NOT_RECOGNIZED                  = "TRANSACTION_NOT_RECOGNIZED_400";

    public final static String  REFUELING_SUBSCRIPTION_SUCCESS                                       = "USR-NTFY-200";
    public final static String  REFUELING_SUBSCRIPTION_ERROR                                         = "USR-NTFY-500";
    public final static String  REFUELING_SUBSCRIPTION_REQUEST_ALREADY_PROCESSED                     = "USR-NTFY-412";
}
