package com.techedge.mp.core.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionDetail;

public class AwardDetailDataResponse implements Serializable {

    /**
     * 
     */
    private static final long            serialVersionUID            = 8136519937145522882L;

    private String                       statusCode;

    private List<AwardDetailData>        awardDetailList             = new ArrayList<AwardDetailData>(0);

    private List<BrandDataDetail>        brandDataDetailList         = new ArrayList<BrandDataDetail>(0);

    private List<CategoryBurnDataDetail> categoryBurnDataDetailList  = new ArrayList<CategoryBurnDataDetail>(0);

    private List<RedemptionDetail>       redemptionDetailList        = new ArrayList<RedemptionDetail>(0);
    
    private List<RedemptionDetail>       parkingRedemptionDetailList = new ArrayList<RedemptionDetail>(0);

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<AwardDetailData> getAwardDetailList() {

        return awardDetailList;
    }

    public void setAwardDetailList(List<AwardDetailData> awardDetailList) {

        this.awardDetailList = awardDetailList;
    }

    public List<BrandDataDetail> getBrandDataDetailList() {

        return brandDataDetailList;
    }

    public void setBrandDataDetailList(List<BrandDataDetail> brandDataDetailList) {

        this.brandDataDetailList = brandDataDetailList;
    }

    public List<CategoryBurnDataDetail> getCategoryBurnDataDetailList() {

        return categoryBurnDataDetailList;
    }

    public void setCategoryBurnDataDetailList(List<CategoryBurnDataDetail> categoryBurnDataDetailList) {

        this.categoryBurnDataDetailList = categoryBurnDataDetailList;
    }

    public List<RedemptionDetail> getRedemptionDetailList() {
    
        return redemptionDetailList;
    }

    public void setRedemptionDetailList(List<RedemptionDetail> redemptionDetailList) {
    
        this.redemptionDetailList = redemptionDetailList;
    }

    public List<RedemptionDetail> getParkingRedemptionDetailList() {
    
        return parkingRedemptionDetailList;
    }

    public void setParkingRedemptionDetailList(List<RedemptionDetail> parkingRedemptionDetailList) {
    
        this.parkingRedemptionDetailList = parkingRedemptionDetailList;
    }

}
