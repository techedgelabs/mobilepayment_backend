package com.techedge.mp.core.business.utilities;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

class Copier {
    public static void copyFromTo(final Object source, final Object dest, ArrayList<String> skipFields) {
        final Class sourceClass;
        final Class destClass;
        final Field[] sourceFields;

        sourceClass = source.getClass();
        destClass = dest.getClass();
        sourceFields = sourceClass.getDeclaredFields();

        for (final Field field : sourceFields) {
            if (skipFields != null && skipFields.contains(field.getName())) {
                continue;
            }
            copyField(field, source, dest, destClass);
        }
    }

    public static void copyFromTo(final Object source, final Object dest) {
        copyFromTo(source, dest, null);
    }
    
    private static void copyField(final Field field, final Object source, final Object dest, final Class destClass) {
        final String fieldName;
        final String methodName;

        fieldName = field.getName();
        methodName = "set" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);

        try {
            final Method method;

            method = destClass.getMethod(methodName, field.getType());
            field.setAccessible(true);
            method.invoke(dest, field.get(source));
        }
        catch (final NoSuchMethodException ex) {
            // ignore it
            ex.printStackTrace();
        }
        catch (final IllegalAccessException ex) {
            // ignore it
            ex.printStackTrace();
        }
        catch (final IllegalArgumentException ex) {
            // ignore it
            ex.printStackTrace();
        }
        catch (final InvocationTargetException ex) {
            // ignore it
            ex.printStackTrace();
        }
    }
}