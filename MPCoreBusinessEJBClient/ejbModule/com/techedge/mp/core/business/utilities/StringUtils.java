package com.techedge.mp.core.business.utilities;

import java.util.List;

public class StringUtils {

    public StringUtils() {

    }

    public static String convertStringArrayToString(List<String> array, String pipeSymbol) {

        String textVal = "";
        try {

            for (String elem : array) {
                textVal += elem.concat(pipeSymbol);
           }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.err.println(ex!=null ? ex.getMessage() : "Nullpointer exception");
        }
        return textVal;
    }

    public static String capitalizeString(String input) {

        // check the input string
        if ((input == null) || (input.isEmpty())) {
            return "";
        }

        input = input.toLowerCase();

        char[] chars = input.toCharArray();

        // all ways make first char a cap
        chars[0] = Character.toUpperCase(chars[0]);

        // then capitalize if space on left.
        for (int x = 1; x < chars.length; x++) {
            if (chars[x - 1] == ' ') {
                chars[x] = Character.toUpperCase(chars[x]);
            }
        }

        String output = new String(chars);

        return output;
    }

}
