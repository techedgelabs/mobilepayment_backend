package com.techedge.mp.core.business.utilities.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.techedge.mp.core.business.utilities.EncryptionAES;

public class EncryptionAESTest {

    @Before
    public void setUp() {
    }

    @Test
    public void testCapitalize() {

        String message   = "messaggio di prova";
        String secretKey = "12345678901234567890123456789012";
        
        EncryptionAES encryptionAES = new EncryptionAES();
        
        try {
            encryptionAES.loadSecretKey(secretKey);
            String encryptedMessage = encryptionAES.encrypt(message);
            String decryptedMessage = encryptionAES.decrypt(encryptedMessage);
            
            assertTrue(message.equals(decryptedMessage));
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
