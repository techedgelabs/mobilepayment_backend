package com.techedge.mp.core.business.utilities.test;

import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.techedge.mp.core.business.interfaces.crm.CrmDataElement;
import com.techedge.mp.core.business.utilities.CrmDataProcessor;
import com.techedge.mp.core.business.utilities.FTPSService;

public class CrmDataProcessorTest {

    FTPSService service = null;

    @Before
    public void setUp() {
    }

    @Test
    public void testLogin() {

        String data = "CD_CLIENTE|CD_CARTA|NOME|COGNOME|TEL_CELLULARE|EMAIL|SESSO|DATA_NASCITA|MSG|CANALE|COD_INIZIATIVA|COD_OFFERTA|TREATMENT_CODE|FLAG_ELAB|CODICE_FISCALE|C_2|C_3|C_4|C_5|TMS_INSERIMENTO|OBIETTIVO_PUNTI|OBIETTIVO_LITRI|C_6|C_7|C_8|C_9|C_10\n8056889|536200004770165318|MARTINA|BENCUROVA||martina.bencurova@eni.com|||Prova Messaggio Puntuale App|app|C000000588-000001381||000010198|P|BNCMTN84H44Z155X|||||2017-05-22 18:27:53.0|||||||\n";
        
        List<CrmDataElement> dataElementList = CrmDataProcessor.process(data, 1, 1);
        
        assertFalse(dataElementList.isEmpty());
    }

}
