package com.techedge.mp.core.business.utilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

public class FTPSService extends AbstractFTPService {
    
    private FTPSClient client = null;
    
    public FTPSService(String host,
            String port,
            String username,
            String password,
            String proxyHost,
            String proxyPort) {
        
        super(host, port, username, password, proxyHost, proxyPort);
    }
    
    
    protected Boolean init() {
        
        System.out.println("FTPSService->init");
        
        this.client = new FTPSClient(false);
        
        // Connect to host
        try {
            this.client.connect(this.host, Integer.valueOf(this.port));
        }
        catch (SocketException e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
        catch (IOException e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
        
        int reply = this.client.getReplyCode();
        
        if (FTPReply.isPositiveCompletion(reply)) {
        
            return Boolean.TRUE;
        }
        
        return Boolean.FALSE;
    
    }
    
    
    protected Boolean doLogin() {
        
        System.out.println("FTPSService->doLogin");
        
        // Login
        try {
            if (this.client.login(username, password)) {

                // Set protection buffer size
                this.client.execPBSZ(0);
                System.out.println("FTPSService->execPBSZ ok");
                
                // Set data channel protection to private
                this.client.execPROT("P");
                System.out.println("FTPSService->execPROT ok");
                
                // Enter local passive mode
                this.client.enterLocalPassiveMode();
                System.out.println("FTPSService->enterLocalPassiveMode ok");
                
                // Set binary transfer mode
                this.client.setFileTransferMode(FTP.BINARY_FILE_TYPE);
                System.out.println("FTPSService->setFileTransferMode ok");
                
                return Boolean.TRUE;
            }
            else {
                
                return Boolean.FALSE;
            }
        }
        catch (SSLException e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
        catch (IOException e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }
    
    
    protected Boolean closeConnection() {
        
        System.out.println("FTPSService->closeConnection");
        
        // Logout
        try {
            this.client.logout();
            this.client.disconnect();
            return Boolean.TRUE;
        }
        catch (IOException e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }
    
    
    public String getFile(String remote) {
        
        System.out.println("FTPSService->getFile");
        
        this.setProxy();
        
        String outputData = "";
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            Boolean loginResult = this.doLogin();
            
            if (loginResult) {
                
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                
                boolean retrieveFileResult = false;
                
                try {
                    retrieveFileResult = this.client.retrieveFile(remote, baos);
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
                
                if (retrieveFileResult) {
                    
                    try {
                        outputData = baos.toString("UTF-8");
                    }
                    catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    // TODO
                }
                
                this.closeConnection();
            }
            else {
                // TODO
            }
        }
        else {
            // TODO
        }
        
        this.unsetProxy();
        
        return outputData;
    }
    
    
    public Boolean putFile(String data, String remote) {
        
        System.out.println("FTPSService->putFile");
        
        Boolean storeFileResult = Boolean.FALSE;
        
        this.setProxy();
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            Boolean loginResult = this.doLogin();
            
            if (loginResult) {
                
                ByteArrayInputStream bais = new ByteArrayInputStream(data.getBytes());
                
                try {
                    storeFileResult = this.client.storeFile(remote, bais);
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
                
                this.closeConnection();
            }
            else {
                // TODO
            }
        }
        else {
            // TODO
        }
        
        this.unsetProxy();
        
        return storeFileResult;
    }
    
    
    public List<String> openDirectory(String path) {
        
        System.out.println("FTPSService->openDirectory");
        
        List<String> files = new ArrayList<String>(0);
        
        this.setProxy();
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            Boolean loginResult = this.doLogin();
            
            if (loginResult) {
                
                try {
                    
                    System.out.println("Working directory: " + this.client.printWorkingDirectory());
                    
                    // Set root path
                    this.client.changeWorkingDirectory(path);
                    System.out.println("FTPSService->changeWorkingDirectory ok");
                    
                    System.out.println("Working directory: " + this.client.printWorkingDirectory());
                    
                    // read directory
                    FTPFile[] ftpFileList = this.client.listFiles();
                    System.out.println("FTPSService->listFiles ok");
                    
                    for (FTPFile ftpFile : ftpFileList) {
                        
                        if(ftpFile.isFile()) {
                            
                            String file = ftpFile.getName();
                            files.add(file);
                        }
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                
                this.closeConnection();
            }
        }
        
        this.unsetProxy();
        
        return files;
    }
    
    
    public List<String> listDirectory(String path) {
        
        System.out.println("FTPSService->listDirectory");
        
        List<String> files = new ArrayList<String>(0);
        
        this.setProxy();
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            Boolean loginResult = this.doLogin();
            
            if (loginResult) {
                
                try {
                    
                    FTPFile[] ftpFileList = this.client.listDirectories();
                    
                    for (FTPFile ftpFile : ftpFileList) {
                        
                        String file = ftpFile.getName();
                        files.add(file);
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                
                this.closeConnection();
            }
        }
        
        this.unsetProxy();
        
        return files;
    }
    
    
    public Boolean moveFile(String remoteSource, String remoteDest) {
        
        System.out.println("FTPSService->moveFile");
        
        this.setProxy();
        
        Boolean result = Boolean.FALSE;
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            Boolean loginResult = this.doLogin();
            
            if (loginResult) {
                
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                
                boolean retrieveFileResult = false;
                
                try {
                    retrieveFileResult = this.client.retrieveFile(remoteSource, baos);
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
                
                if (retrieveFileResult) {
                    
                    try {
                        String outputData = baos.toString("UTF-8");
                        
                        ByteArrayInputStream bais = new ByteArrayInputStream(outputData.getBytes());
                        
                        Boolean storeFileResult = this.client.storeFile(remoteDest, bais);
                        
                        if (storeFileResult) {
                            
                            Boolean deleteFileResult = this.client.deleteFile(remoteSource);
                            
                            result = deleteFileResult;
                        }
                    }
                    catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                else {
                    // TODO
                }
                
                this.closeConnection();
            }
            else {
                // TODO
            }
        }
        else {
            // TODO
        }
        
        this.unsetProxy();
        
        return result;
    }


    @Override
    public Boolean deleteFile(String remote) {

        System.out.println("FTPSService->deleteFile");
        
        Boolean deleteFileResult = Boolean.FALSE;
        
        this.setProxy();
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            Boolean loginResult = this.doLogin();
            
            if (loginResult) {
                
                try {
                    deleteFileResult = this.client.deleteFile(remote);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    return Boolean.FALSE;
                }
                
                this.closeConnection();
            }
            else {
                // TODO
            }
        }
        else {
            // TODO
        }
        
        this.unsetProxy();
        
        return deleteFileResult;
    }
}
