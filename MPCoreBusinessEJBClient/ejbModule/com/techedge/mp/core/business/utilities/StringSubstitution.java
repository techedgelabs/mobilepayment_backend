package com.techedge.mp.core.business.utilities;

import java.util.ArrayList;
import java.util.HashMap;

public class StringSubstitution {

    private HashMap<String, ArrayList<String>> structure = new HashMap<String, ArrayList<String>>();
    private final String KEYS_SEPARATOR = "\\|";
    private final String KEY_SEPARATOR = ":";
    private final String SUBSTITUTIONS_SEPARATOR = "\\,";
    
    
    public StringSubstitution(String pattern) {
        if (pattern == null) {
            System.out.println("Pattern is null. Nothing to initialize");
            return;
        }
        
        String[] keys = pattern.split(KEYS_SEPARATOR);
        
        for (int i = 0; i < keys.length; i++) {
            ArrayList<String> substitutions = new ArrayList<String>();
            String key = keys[i].trim().substring(0, keys[i].indexOf(KEY_SEPARATOR));
            String keyValues = keys[i].trim().substring(keys[i].indexOf(KEY_SEPARATOR) + 1);
            
            String[] values = keyValues.split(SUBSTITUTIONS_SEPARATOR);
            
            for (int j = 0; j < values.length; j++) {
                substitutions.add(values[j].trim());
                System.out.println("[StringSubstitution] key: " + key + " - value: " + values[j].trim());
            }
            
            structure.put(key, substitutions);
            
        }
        
        System.out.println("StringSubstitution initialized!");
    }
    
    public String getValue(String key, int index) {
        ArrayList<String> substitutions = structure.get(key);
        
        if (substitutions == null || substitutions.size() < index) {
            System.out.println("Substitutions is null or his length < " + index);
            return null;
        }
        
        String value = substitutions.get(index - 1);
        
        return value;
    }
}
