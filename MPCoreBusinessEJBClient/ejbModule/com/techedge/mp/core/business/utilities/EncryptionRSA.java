package com.techedge.mp.core.business.utilities;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class EncryptionRSA {

    /**
     * String to hold name of the encryption algorithm.
     */
    private static final String ALGORITHM                   = "RSA";
    private static final String ALGORITHM_ENCRYPT_MODE        = "RSA/ECB/PKCS1Padding";
    private static final String ALGORITHM_DECRYPT_MODE        = "RSA/ECB/PKCS1Padding";
    
    private RSAPrivateKey privateKey = null;
    
    private RSAPublicKey publicKey = null;
    
    private BASE64Encoder encoder = new BASE64Encoder();
    
    private BASE64Decoder decoder = new BASE64Decoder();
    
    
    public EncryptionRSA() {
        //java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }
    
    public void loadPrivateKey(String pemPrivateKey) throws Exception {
        if (pemPrivateKey == null || pemPrivateKey.trim().isEmpty()) {
            throw new InvalidKeyException("Invalid PEM private key string: null or empty");
        }

        // Encrypt the string using the public key
        pemPrivateKey = pemPrivateKey.replaceAll("(-+BEGIN RSA PRIVATE KEY-+\\r?\\n|-+END RSA PRIVATE KEY-+\\r?\\n?)", "");
        byte[] keyBytes = decoder.decodeBuffer(pemPrivateKey);

        // generate private key
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
        this.privateKey = (RSAPrivateKey) keyFactory.generatePrivate(spec);
    }

    public void loadPublicKey(String pemPublicKey) throws Exception {
        if (pemPublicKey == null || pemPublicKey.trim().isEmpty()) {
            throw new InvalidKeyException("Invalid PEM public key string: null or empty");
        }
        
        // Encrypt the string using the public key
        pemPublicKey = pemPublicKey.replaceAll("(-+BEGIN PUBLIC KEY-+\\r?\\n|-+END PUBLIC KEY-+\\r?\\n?)", "");
        byte[] keyBytes = decoder.decodeBuffer(pemPublicKey);

        // generate public key
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);                
        this.publicKey = (RSAPublicKey) keyFactory.generatePublic(spec);
    }

    public void generate(String publicKeyFilename, String privateFilename) throws Exception {

            // Create the public and private keys
            KeyPairGenerator generator = KeyPairGenerator.getInstance(ALGORITHM, "BC");

            SecureRandom random = createFixedRandom();
            generator.initialize(1024, random);

            KeyPair pair = generator.generateKeyPair();
            this.publicKey = (RSAPublicKey) pair.getPublic();
            this.privateKey = (RSAPrivateKey) pair.getPrivate();

            System.out.println("publicKey : " + encoder.encode(publicKey.getEncoded()));
            System.out.println("privateKey : " + encoder.encode(privateKey.getEncoded()));

            BufferedWriter out = new BufferedWriter(new FileWriter(publicKeyFilename));
            out.write(encoder.encode(publicKey.getEncoded()));
            out.close();

            out = new BufferedWriter(new FileWriter(privateFilename));
            out.write(encoder.encode(privateKey.getEncoded()));
            out.close();
    }

    private SecureRandom createFixedRandom()
    {
        return new FixedRand();
    }

    private static class FixedRand extends SecureRandom {

        /**
         * 
         */
        private static final long serialVersionUID = 3362240133712035226L;
        MessageDigest sha;
        byte[] state;

        FixedRand() {
            try
            {
                this.sha = MessageDigest.getInstance("SHA-1");
                this.state = sha.digest();
            }
            catch (NoSuchAlgorithmException e)
            {
                throw new RuntimeException("can't find SHA-1!");
            }
        }

        public void nextBytes(byte[] bytes){

            int    off = 0;

            sha.update(state);

            while (off < bytes.length)
            {                
                state = sha.digest();

                if (bytes.length - off > state.length)
                {
                    System.arraycopy(state, 0, bytes, off, state.length);
                }
                else
                {
                    System.arraycopy(state, 0, bytes, off, bytes.length - off);
                }

                off += state.length;

                sha.update(state);
            }
        }
    }    
    
    public String encrypt(String text) throws Exception {
            if (publicKey == null) {
                throw new InvalidKeyException("Invalid public key: null");
            }
            /*
            AsymmetricBlockCipher cipher = new RSAEngine();
            cipher.init(true, new RSAKeyParameters(true, publicKey.getModulus(), publicKey.getPublicExponent()));

            byte[] messageBytes = text.getBytes();
            byte[] hexEncodedCipher = cipher.processBlock(messageBytes, 0, messageBytes.length);
            */
            Cipher cipher = Cipher.getInstance(ALGORITHM_ENCRYPT_MODE);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            
            byte[] textBytes = text.getBytes();
            byte[] hexEncodedCipher = cipher.doFinal(textBytes);

            //String encryptedText = getHexString(hexEncodedCipher);
            String encryptedText = encoder.encode(hexEncodedCipher);

            System.out.println("Original: " + text);
            System.out.println("Encrypted: " + encryptedText);

            return encryptedText;
    }
    
    public String decrypt(String text) throws Exception {
        if (privateKey == null) {
            throw new InvalidKeyException("Invalid private key: null");
        }
        /*
        AsymmetricBlockCipher cipher = new RSAEngine();
        cipher.init(false, new RSAKeyParameters(true, privateKey.getModulus(), privateKey.getPrivateExponent()));

        //byte[] messageBytes = hexStringToByteArray(text);
        byte[] messageBytes = decoder.decodeBuffer(text);
        byte[] hexEncodedCipher = cipher.processBlock(messageBytes, 0, messageBytes.length);
        */
        Cipher cipher = Cipher.getInstance(ALGORITHM_DECRYPT_MODE);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        
        byte[] decodedText = decoder.decodeBuffer(text);
        byte [] hexEncodedCipher = cipher.doFinal(decodedText);
        
        String decryptedText = new String(hexEncodedCipher);

        System.out.println("Original: " + text);
        //System.out.println("Decrypted: " + new String(cipherText));
        System.out.println("Decrypted: " + decryptedText);
       
        return decryptedText;
    }
    /*
    private byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    
    private String getHexString(byte[] b) throws Exception {
        String result = "";
        for (int i=0; i < b.length; i++) {
            result +=
                Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
        }
        return result;
    }
    */
    public RSAPrivateKey getPrivateKey() {

        return privateKey;
    }
    
    public RSAPublicKey getPublicKey() {

        return publicKey;
    }
 
}
