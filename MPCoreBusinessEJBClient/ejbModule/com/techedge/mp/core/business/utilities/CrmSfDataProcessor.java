package com.techedge.mp.core.business.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

import com.techedge.mp.core.business.interfaces.crm.CrmSfDataElement;
import com.techedge.mp.core.business.interfaces.crm.CrmSfDataElementProcessingResult;

public class CrmSfDataProcessor {

    private final static String lineSeparator = "\n";

    public CrmSfDataProcessor() {

    }

    public static List<CrmSfDataElement> process(String data, int maxDataPerThread, int maxThreads) {

        List<CrmSfDataElement> elements = new ArrayList<CrmSfDataElement>(0);

        String[] lines = data.split(Pattern.quote(lineSeparator));
        
        // Faccio lo skyip della header della prima riga 
        lines = Arrays.copyOfRange(lines, 1, lines.length);
        
        int totalDataToProcess = lines.length;
        int threadCount = 1;
        
        System.out.println("Dati lettura CSV: " + totalDataToProcess);

        if (totalDataToProcess > maxDataPerThread) {
            threadCount = totalDataToProcess / maxDataPerThread;
            
            if (totalDataToProcess % maxDataPerThread > 0) {
                threadCount++;
            }
        }
        else {
            maxDataPerThread = totalDataToProcess;
        }

        ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
        
        for (int i = 0; i < threadCount; i++) {
            
            int start = i * maxDataPerThread;
            int end = start + maxDataPerThread;
            
            if (end > totalDataToProcess) {
                end = totalDataToProcess;
            }
            
            System.out.println("THREAD LETTURA CSV N " + (i + 1) + " indice inizio: " + start + "  valore: " + lines[start]);
            System.out.println("THREAD LETTURA CSV N " + (i + 1) + " indice fine: " + end + "  valore: " + lines[end - 1]);
            
            CRMDataElementProcess crmDataElementProcess = new CRMDataElementProcess(i + 1, lines, start, end, elements);
            
            executor.submit(crmDataElementProcess);
        }
        
        executor.shutdown();

        while (!executor.isTerminated()) {}

        System.out.println("THREADS LETTURA CSV TERMINATI");
        
        return elements;
    }

    public static CrmSfDataElementProcessingResult createCrmDataElementProcessingResult(CrmSfDataElement crmSfDataElement, String categoryDesc, String deliveryId) {

        CrmSfDataElementProcessingResult crmSfDataElementProcessingResult = new CrmSfDataElementProcessingResult();
        
        crmSfDataElementProcessingResult.fiscalCode = crmSfDataElement.fiscalCode ;
        crmSfDataElementProcessingResult.deliveryId = deliveryId;
        crmSfDataElementProcessingResult.contactCode = crmSfDataElement.contactCode;
        crmSfDataElementProcessingResult.cardCode = crmSfDataElement.cardCode;
        crmSfDataElementProcessingResult.initiativeCode = crmSfDataElement.initiativeCode;
        crmSfDataElementProcessingResult.offerCode = crmSfDataElement.offertCode;
        crmSfDataElementProcessingResult.id = crmSfDataElement.id;
        crmSfDataElementProcessingResult.channel = crmSfDataElement.channel;
        crmSfDataElementProcessingResult.createdDate = crmSfDataElement.createDate;

        return crmSfDataElementProcessingResult;
    }

    public static String generateOutput(List<CrmSfDataElementProcessingResult> crmSfDataElementProcessingResultList) {

        String output = "Fiscal_code__c|Delivery_Id__c|Contact_code__c|Initiative_Code__c|Offer_code__c|Id|Channel__c|CreatedDate";
        
        for (CrmSfDataElementProcessingResult crmDataElementProcessingResult : crmSfDataElementProcessingResultList) {
            output = output + crmDataElementProcessingResult.toString() + lineSeparator;
        }

        return output;
    }

    private static class CRMDataElementProcess implements Runnable {
        
        private String[] lines;
        private int start;
        private int end;
        private int threadID;
        private List<CrmSfDataElement> elements;
        
        
        public CRMDataElementProcess(int threadID, String[] lines, int start, int end, List<CrmSfDataElement> elements) {
            this.threadID = threadID;
            this.lines = lines;
            this.start = start;
            this.end = end;
            this.elements = elements;
        }

        @Override
        public void run() {

            System.out.println("THREAD LETTURA CSV N " + threadID + " start");
            
            for (int i = start; i < end; i++) {
                CrmSfDataElement crmDataElement = new CrmSfDataElement();
                Boolean result = crmDataElement.process(lines[i]);
                if (result) {
                    elements.add(crmDataElement);
                }
                else {
                  System.err.println("THREAD LETTURA CSV N " + threadID + " dato non valido. Che si fa?? " + lines[i]);
                }
            }
            
            System.out.println("THREAD LETTURA CSV N " + threadID + " end");

        }
        
    }

}
