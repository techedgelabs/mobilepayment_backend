package com.techedge.mp.core.business.utilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SFTPService extends AbstractFTPService {

    private JSch jsch = null;
    private Session session = null;
    private Channel channel = null;
    private ChannelSftp c = null;

    
    public SFTPService(String host,
            String port,
            String username,
            String password,
            String proxyHost,
            String proxyPort) {
        
        super(host, port, username, password, proxyHost, proxyPort);
    }
    
    
    protected Boolean init() {
        
        System.out.println("SFTPService->init");
        
        try {
            
            System.out.println("Initializing jsch");
            
            this.jsch = new JSch();
            
            this.jsch.addIdentity(this.password, "");
            
            this.session = jsch.getSession(this.username, this.host, Integer.valueOf(this.port));

            // Java 6 version
            //this.session.setPassword(this.password.getBytes(Charset.forName("ISO-8859-1")));
            
            // Java 5 version
            // session.setPassword(password.getBytes("ISO-8859-1"));

            System.out.println("Jsch set to StrictHostKeyChecking=no");
            
            Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            this.session.setConfig(config);

            System.out.println("Connecting to " + this.host + ":" + this.port);
            this.session.connect();
            System.out.println("Connected !");

            // Initializing a channel
            System.out.println("Opening a channel ...");
            this.channel = this.session.openChannel("sftp");
            this.channel.connect();
            this.c = (ChannelSftp) this.channel;
            System.out.println("Channel sftp opened");

        }
        catch (JSchException e) {
            System.out.println("SFTPService->init error");
            e.printStackTrace();
            
            return Boolean.FALSE;
        }
        
        return Boolean.TRUE;
    }
    
    
    protected Boolean closeConnection() {
        
        System.out.println("SFTPService->closeConnection");
        
        if (this.c != null) {
            System.out.println("Disconnecting sftp channel");
            this.c.disconnect();
        }
        if (this.channel != null) {
            System.out.println("Disconnecting channel");
            this.channel.disconnect();
        }
        if (this.session != null) {
            System.out.println("Disconnecting session");
            this.session.disconnect();
        }
        
        return Boolean.TRUE;
    }
    
    
    public String getFile(String remote) {
        
        System.out.println("SFTPService->getFile");
        
        this.setProxy();
        
        String outputData = "";
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            if (this.c == null || this.session == null || !this.session.isConnected() || !this.c.isConnected()) {
                System.out.println("Connection to server is closed. Open it first.");
                return outputData;
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            try {
                System.out.println("Downloading file from server");
                this.c.get(remote, baos);
                System.out.println("Download successfull.");
                outputData = baos.toString("UTF-8");
                System.out.println("File retrieved.");
            }
            catch (SftpException e) {
                e.printStackTrace();
                return outputData;
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return outputData;
            }
            
            this.closeConnection();
        }
        else {
            return outputData;
        }
        
        this.unsetProxy();
        
        return outputData;
    }
    
    
    public Boolean putFile(String data, String remote) {
        
        System.out.println("SFTPService->putFile");
        
        this.setProxy();
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            if (this.c == null || this.session == null || !this.session.isConnected() || !this.c.isConnected()) {
                System.out.println("Connection to server is closed. Open it first.");
                return Boolean.FALSE;
            }
            
            ByteArrayInputStream bais = new ByteArrayInputStream(data.getBytes());
            
            try {
                System.out.println("Uploading file to server");
                this.c.put(bais, remote);
                System.out.println("Upload successfull.");
            }
            catch (SftpException e) {
                e.printStackTrace();
                return Boolean.FALSE;
            }
            
            this.closeConnection();
        }
        else {
            return Boolean.FALSE;
        }
        
        this.unsetProxy();
        
        return Boolean.TRUE;
    }
    
    
    @SuppressWarnings("unchecked")
    public List<String> openDirectory(String path) {
        
        System.out.println("SFTPService->openDirectory");
        
        List<String> files = new ArrayList<String>(0);
        
        this.setProxy();
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            System.out.println("SFTPService->changeWorkingDirectory");
            try {
                this.c.cd(path);
            }
            catch (SftpException e1) {
                System.out.println("SFTPService->changeWorkingDirectory ko");
                e1.printStackTrace();
                return files;
            }
            System.out.println("SFTPService->changeWorkingDirectory ok");
            
            System.out.println("SFTPService->readContentDirectory");
            Vector<ChannelSftp.LsEntry> ftpsFiles;
            try {
                ftpsFiles = this.c.ls("*");
            }
            catch (SftpException e2) {
                System.out.println("SFTPService->readContentDirectory ko");
                e2.printStackTrace();
                return files;
            }
            System.out.println("SFTPService->readContentDirectory ok");
            for(ChannelSftp.LsEntry entry : ftpsFiles) {
                if(!entry.getAttrs().isDir()) {
                    String file = entry.getFilename();
                    files.add(file);
                }
            }
        }
        
        this.unsetProxy();
        
        return files;
    }
    
    
    @SuppressWarnings("unchecked")
    public List<String> listDirectory(String path) {
        
        System.out.println("SFTPService->listDirectory");
        
        List<String> files = new ArrayList<String>(0);
        
        this.setProxy();
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            System.out.println("SFTPService->changeWorkingDirectory");
            try {
                this.c.cd(path);
            }
            catch (SftpException e1) {
                System.out.println("SFTPService->changeWorkingDirectory ko");
                e1.printStackTrace();
                return files;
            }
            System.out.println("SFTPService->changeWorkingDirectory ok");
            
            System.out.println("SFTPService->readContentDirectory");
            Vector<ChannelSftp.LsEntry> ftpsFiles;
            try {
                ftpsFiles = this.c.ls("*");
            }
            catch (SftpException e2) {
                System.out.println("SFTPService->readContentDirectory ko");
                e2.printStackTrace();
                return files;
            }
            System.out.println("SFTPService->readContentDirectory ok");
            for(ChannelSftp.LsEntry entry : ftpsFiles) {
                if(!entry.getAttrs().isDir()) {
                    String file = entry.getFilename();
                    files.add(file);
                }
            }
        }
        
        this.unsetProxy();
        
        return files;
    }
    
    
    public Boolean moveFile(String remoteSource, String remoteDest) {
        
        System.out.println("SFTPService->moveFile");
        
        this.setProxy();
        
        Boolean result = Boolean.FALSE;
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            if (this.c == null || this.session == null || !this.session.isConnected() || !this.c.isConnected()) {
                System.out.println("Connection to server is closed. Open it first.");
                return result;
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            try {
                System.out.println("Downloading file from server");
                this.c.get(remoteSource, baos);
                System.out.println("Download successfull.");
                
                String outputData = baos.toString("UTF-8");
                
                ByteArrayInputStream bais = new ByteArrayInputStream(outputData.getBytes());
                
                System.out.println("Uploading file to server");
                this.c.put(bais, remoteDest);
                System.out.println("Upload successfull.");
                
                System.out.println("Deleting source file from server");
                this.c.rm(remoteSource);
                System.out.println("Deleting successfull.");
            }
            catch (SftpException e) {
                e.printStackTrace();
                return Boolean.FALSE;
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return Boolean.FALSE;
            }
            
            this.closeConnection();
        }
        else {
            // TODO
        }
        
        this.unsetProxy();
        
        return Boolean.TRUE;
    }


    @Override
    public Boolean deleteFile(String remote) {

        System.out.println("SFTPService->deleteFile");
        
        this.setProxy();
        
        Boolean initResult = this.init();
        
        if (initResult) {
            
            if (this.c == null || this.session == null || !this.session.isConnected() || !this.c.isConnected()) {
                System.out.println("Connection to server is closed. Open it first.");
                return Boolean.FALSE;
            }

            try {
                this.c.rm(remote);
            }
            catch (SftpException e) {
                e.printStackTrace();
                return Boolean.FALSE;
            }
            
            this.closeConnection();
        }
        else {
            return Boolean.FALSE;
        }
        
        this.unsetProxy();
        
        return Boolean.TRUE;
    }
}
