package com.techedge.mp.core.business.utilities.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.techedge.mp.core.business.utilities.StringUtils;

public class StringUtilsTest {

    @Before
    public void setUp() {
    }

    @Test
    public void testCapitalize() {

        String stringaDaConvertire = "a eekfem oeuehd98 uhnd2o 1uiqdsdb udoi' 198n";
        String risultatoAtteso1    = "A Eekfem Oeuehd98 Uhnd2o 1uiqdsdb Udoi' 198n";
        
        assertTrue(risultatoAtteso1.equals(StringUtils.capitalizeString(stringaDaConvertire)));
        
        String stringaVuotaDaConvertire = "";
        String risultatoAtteso2         = "";
        
        assertTrue(risultatoAtteso2.equals(StringUtils.capitalizeString(stringaVuotaDaConvertire)));
        
        String stringaNullDaConvertire = null;
        String risultatoAtteso3        = "";
        
        assertTrue(risultatoAtteso3.equals(StringUtils.capitalizeString(stringaNullDaConvertire)));
    }

}
