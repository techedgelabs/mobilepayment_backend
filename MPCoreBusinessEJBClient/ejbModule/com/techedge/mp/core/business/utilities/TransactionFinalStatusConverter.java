package com.techedge.mp.core.business.utilities;

import java.util.HashMap;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.crm.CommandType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.interfaces.station.StationChangeLogType;
import com.techedge.mp.core.business.interfaces.user.DWHOperationType;

public class TransactionFinalStatusConverter {

    @SuppressWarnings("serial")
    private static HashMap<String, String>   reportMap  = new HashMap<String, String>() {
                                                            {
                                                                // PostPaid PAID
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID, "Transazione Completata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID + "_" 
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_OK", "Transazione Completata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_COMPLETE, "Transazione Completata");
                                                                
                                                                // PostPaid UNPAID
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_NOTIFICATION_FAIL, "Transazione In Elaborazione");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_VOUCHER + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER + "_ERROR", "Transazione In Elaborazione");

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_VOUCHER + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER + "_KO", "Transazione Annullata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_CANCELED, "Transazione Annullata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_AUTH_MISSING, "Transazione In Elaborazione");

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_AUTH_DELETE_MISSING, "Transazione In Elaborazione");

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_VOUCHER_FAIL, "Transazione Annullata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_REVERSE + ""
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE + "_OK", "Transazione Annullata");

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_REVERSE + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE, "Transazione In Elaborazione");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_MOV + "_KO", "Transazione Annullata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_MOV + "_ERROR", "Transazione In Elaborazione");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_OK", "Transazione Annullata");

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_ERROR", "Transazione In Elaborazione");
                                                                
                                                                // PostPaid CANCELLED
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED, "Transazione Annullata");
                                                                
                                                                
                                                                // PostPaid REVERSED
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED, "Transazione Stornata");
                                                                
                                                                // PrePaid 
                                                                put(StatusHelper.FINAL_STATUS_TYPE_ERROR, "Transazione In Elaborazione");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT, "Transazione In Elaborazione");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_FAILED, "Transazione Negata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_FAILED + "_" + StatusHelper.STATUS_PAYMENT_NOT_AUTHORIZED, "Transazione Negata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_FAILED + "_" + StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED, 
                                                                        "Transazione Annullata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_FAILED + "_" + StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED, 
                                                                        "Transazione Annullata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_AFTER_REFUEL, "Transazione In Elaborazione");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL, "Transazione In Elaborazione");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL, "Transazione Completata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "_" + StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED, 
                                                                        "Transazione Annullata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "_" + StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED, 
                                                                        "Transazione Annullata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE, "Transazione non riconciliabile");

                                                            
                                                                // Voucher Carburante
                                                                //put(StatusHelper.VOUCHER_FINAL_STATUS_SUCCESSFUL, "Transazione Completata");  //Duplicato
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_AUTH, "Transazione In Elaborazione");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE, "Transazione In Elaborazione");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_MOV, "Transazione In Elaborazione");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_PAY_AUTH, "Transazione Negata");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_PAY_MOV, "Transazione Annullata");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_CREATE, "Transazione Annullata");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_CANCELED, "Transazione Annullata");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_NOT_RECONCILIABLE, "Transazione non riconciliabile");
                                                            
                                                                // Aggiornamento PV
                                                                put(StationChangeLogType.Added.name(), "Inserimento");
                                                                put(StationChangeLogType.Changed.name(), "Modifica");
                                                                put("acquirer_true", "Pagamento con carta abilitato");
                                                                put("acquirer_false", "Pagamento con carta disabilitato");
                                                                put("acquirer", "");
                                                                put("business_true", "Attivo");
                                                                put("business_false", "Disattivo");
                                                                put("payment_true", "Attivo");
                                                                put("payment_false", "Disattivo");
                                                                put("refueling_true", "Attivo");
                                                                put("refueling_false", "Disattivo");
                                                                
                                                            }
                                                        };

    @SuppressWarnings("serial")
    private static HashMap<String, String>   reconciliationTransactionMap  = new HashMap<String, String>() {
                                                            {
                                                                
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS, "Completata Con Successo");
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE, "Fallita");
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY, "Da Riprovare");
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_CANCELLED, "Cancellata");
                                                                                                                                
                                                                //PostPaid RICONCILIATION
                                                                put("POSTPAID_PAYMENT_ERROR", "Errore pagamento post-paid");
                                                                put("POSTPAID_VOUCHER_ERROR", "Errore consumo voucher post-paid");
                                                                put("POSTPAID_LOYALTY_ERROR", "Errore caricamento punti carta loyalty");
                                                                
                                                                // PostPaid PAID
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID, "Transazione completata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID + "_" 
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_OK", "Transazione completata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_COMPLETE, "Transazione completata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_LOYALTY + "_OK", "Caricamento punti carta loyalty eseguito");

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_LOYALTY + "_KO", "Caricamento punti carta loyalty fallito");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_LOYALTY + "_ERROR", "Errore caricamento punti carta loyalty");
                                                                                                                                
                                                                // PostPaid UNPAID
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_NOTIFICATION_FAIL, "Notifica GFG non inviata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_VOUCHER + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER + "_ERROR", "Errore consumo voucher post-paid");
    
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_VOUCHER + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER + "_KO", "Consumo voucher post-paid fallito");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_CANCELED, "Transazione annullata");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_AUTH_MISSING, "Autorizzazione pagamento post-paid mancante");
    
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_AUTH_DELETE_MISSING, 
                                                                        "Cancellazione autorizzazione pagamento post-paid mancante");
    
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_VOUCHER_FAIL, "Consumo voucher post-paid fallito");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_REVERSE + ""
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE + "_OK", "Pagamento stornato");
    
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_REVERSE + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE, "Storno pagamento in elaborazione");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_MOV + "_KO", "Pagamento post-paid fallito");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_MOV + "_KO", "Pagamento post-paid fallito");

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_LOYALTY + "_KO", "Caricamento punti carta loyalty fallito");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_LOYALTY + "_OK", "Caricamento punti carta loyalty eseguito");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_LOYALTY + "_ERROR", "Errore caricamento punti carta loyalty");
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_OK", "Transazione annullata");
    
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_ERROR", "Transazione in elaborazione");
                                                                
                                                                // PostPaid CANCELLED
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED, "Transazione annullata");
                                                                
                                                                
                                                                // PostPaid REVERSED
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED, "Transazione stornata");
                                                                
                                                                // PrePaid 
                                                                put("PREPAID_ABEND", "Transazione interrotta");
                                                                put("PREPAID_ERROR", "Errore generico");
                                                                put("PREPAID_VOUCHER_ERROR", "Errore consumo voucher");
                                                                put("PREPAID_LOYALTY_ERROR", "Errore caricamento punti");
                                                                put("PREPAID_MISSING_NOTIFICATION", "Notifica GFG non inviata");
                                                                put("PREPAID_MISSING_PAYAUTH_DELETE_AFTER_REFUEL", "Errore cancellazione autorizzazione post riforimento nullo");
                                                                put("PREPAID_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL", "Errore cancellazione autorizzazione pre riforimento");
                                                                put("PREPAID_MISSING_PAYMENT", "Errore pagamento/consumo voucher pre-paid");

                                                                put(StatusHelper.FINAL_STATUS_TYPE_ERROR, "Transazione In Elaborazione");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT, "Errore pagamento/consumo voucher pre-paid");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_FAILED, "Transazione negata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_FAILED + "_" + StatusHelper.STATUS_PAYMENT_NOT_AUTHORIZED, "Transazione negata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_FAILED + "_" + StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED, 
                                                                        "Transazione annullata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_FAILED + "_" + StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED, 
                                                                        "Transazione annullata");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_AFTER_REFUEL, 
                                                                        "Errore cancellazione autorizzazione post riforimento nullo");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL, 
                                                                        "Errore cancellazione autorizzazione pre riforimento");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL, "Pagamento/consumo completato con successo");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "_" + StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED, 
                                                                        "Pagamento/consumo annullato");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "_" + StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED, 
                                                                        "Pagamento/consumo annullato");
                                                                put(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE, "Transazione non riconciliabile");
                                                                
                                                                // Voucher Carburante
                                                                put("VOUCHER_PAYMENT_AUTH_ERROR", "Errore autorizzazione acquisto voucher");
                                                                put("VOUCHER_CREATE_ERROR", "Errore creazione voucher");
                                                                put("VOUCHER_PAYMENT_SETTLE_ERROR", "Errore pagamento acquisto voucher");
                                                                put("VOUCHER_DELETE_ERROR", "Errore eliminazione voucher");
                                                                
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_SUCCESSFUL + "_" + StatusHelper.VOUCHER_STATUS_MOV_OK, 
                                                                        "Pagamento acquisto voucher completato con successo");
                                                                
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_NOT_RECONCILIABLE, "Transazione non riconciliabile");
                                                                
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_PAY_AUTH, "Autorizzazione acquisto voucher fallita");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_PAY_MOV, "Pagamento acquisto voucher fallito");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_CREATE, "Creazione voucher fallita");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_CANCELED, "Acquisto voucher annullato");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_DELETE, "Errore eliminazione voucher");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_AUTH, "Errore autorizzazione acquisto voucher");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_CAN, "Errore cancellazione autorizzazione acquisto voucher");
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_MOV, "Errore pagamento acquisto voucher");
                                                            }
                                                        };
    
    @SuppressWarnings("serial")
    private static HashMap<String, String>   reconciliationUserMap  = new HashMap<String, String>() {
                                                            {
                                                                
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS, "Completata con successo");
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE, "Fallita");
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY, "Da riprovare");
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_CANCELLED, "Cancellata");
                                                                                                                                
                                                                put(DWHOperationType.SET_USER_DATA.name(), "Propagazione anagrafica");
                                                                put(DWHOperationType.CHANGE_PASSWORD.name(), "Modifica password");
                                                                put("PUBLISH_MESSAGE", "Notifica push");
                                                                put("REDEMPTION", "Conversione punti loyalty");
                                                                put(CommandType.GET_OFFERS.getName(), "Richiesta offerta");
                                                                put(CommandType.START_SESSION.getName(), "Richiesta offerta");
                                                                put(CommandType.GET_OFFERS_FOR_MULTIPLE_INTERACTION_POINTS.getName(), "Richiesta offerta per endpoint multipli");
                                                                put("CRM_OFFER_VOUCHER_PROMOTIONAL", "Assegnazione voucher promozionale");

                                                                put("notifyEvent", "Notifica evento CRM SF");
                                                                put("500", "Errore nell'invio");
                                                                put("SUCCESS", "Invio effettuato con successo");
                                                                
                                                                put(com.techedge.mp.core.business.interfaces.crm.StatusCode.ERROR.name() + "_" + CommandType.GET_OFFERS.name(), 
                                                                        "Errore nella richiesta di offerta");
                                                                put(com.techedge.mp.core.business.interfaces.crm.StatusCode.SUCCESS.name() + "_" + CommandType.GET_OFFERS.name(), 
                                                                        "Richiesta offerta inviata");

                                                                put(StatusHelper.DWH_EVENT_STATUS_ERROR + "_" + DWHOperationType.SET_USER_DATA.name(), "Errore nella propagazione");
                                                                put(StatusHelper.DWH_EVENT_STATUS_SUCCESS + "_" + DWHOperationType.SET_USER_DATA.name(), "Propagazione eseguita");
                                                                put(StatusHelper.DWH_EVENT_STATUS_FAILED + "_" + DWHOperationType.SET_USER_DATA.name(), "Propagazione fallita");
    
                                                                put(PushNotificationStatusType.ERROR.name() + "_PUBLISH_MESSAGE", "Errore nell'invio");
                                                                put(PushNotificationStatusType.DELIVERED.name() + "_PUBLISH_MESSAGE", "Messaggio consegnato ad AWS");

                                                                put(StatusHelper.CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR + "_CRM_OFFER_VOUCHER_PROMOTIONAL", 
                                                                        "Errore nella creazione del voucher promozionale");
                                                                put(StatusHelper.CRM_OFFER_VOUCHER_PROMOTIONAL_SUCCESS + "_CRM_OFFER_VOUCHER_PROMOTIONAL", 
                                                                        "Creazione del voucher promozionale");

                                                                put(StatusHelper.REDEMPTION_STATUS_ERROR + "_REDEMPTION", "Errore nella conversione punti loyalty in voucher");
                                                                put(StatusHelper.REDEMPTION_STATUS_SUCCESS + "_REDEMPTION", "Conversione punti loyalty in voucher eseguita");
                                                            }
                                                        };
    
    @SuppressWarnings("serial")
    private static HashMap<String, String>   reconciliationParkingMap  = new HashMap<String, String>() {
                                                            {
                                                                
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS, "Completata con successo");
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE, "Fallita");
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY, "Da riprovare");
                                                                put(ResponseHelper.RECONCILIATION_TRANSACTION_CANCELLED, "Cancellata");

                                                                put(StatusHelper.PARKING_STATUS_ENDED, "Transazione completata con successo");
                                                                put(StatusHelper.PARKING_STATUS_FAILED, "Transazione annullata");
                                                                put(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE, "Transazione non riconciliabile");
                                                                
                                                                put(StatusHelper.PARKING_STATUS_ENDED + "_ERROR", "Errore nel pagamento");
                                                                put(StatusHelper.PARKING_STATUS_ENDED + "_SUCCESS", "Pagamento effettuato con successo");
                                                                put(StatusHelper.PARKING_STATUS_FAILED + "_ERROR", "Errore nella cancellazione del pagamento");
                                                                put(StatusHelper.PARKING_STATUS_FAILED + "_SUCCESS", "Cancellazione pagamento effettuata con successo");
            }
        };
                                                        
    @SuppressWarnings("serial")
    private static HashMap<String, String[]> mailMap    = new HashMap<String, String[]>() {
                                                            {
                                                                //PostPaid PAID
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID,
                                                                        new String[] {
            "Transazione Completata",
            "in allegato i dettagli della tua transazione." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID + "_" 
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_OK",
                                                                        new String[] {
            "Transazione Completata",
            "in allegato i dettagli della tua transazione." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_COMPLETE,
                                                                        new String[] {
            "Transazione Completata",
            "in allegato i dettagli della tua transazione." });

                                                                // PostPaid UNPAID
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_NOTIFICATION_FAIL,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_CANCELED,
                                                                        new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� scalato. In allegato i dettagli della tua transazione." });
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_VOUCHER + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER + "_ERROR", 
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_VOUCHER + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER + "_KO", 
                                                                        new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� scalato. In allegato i dettagli della tua transazione." });
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_AUTH_MISSING,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_AUTH_DELETE_MISSING,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_VOUCHER_FAIL + "_KO_4007",
                                                                        new String[] {
            "Transazione Annullata",
            "il servizio non � attivo su questo impianto." });
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_REVERSE + ""
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE + "_OK",
                                                                        new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� scalato.<br />In allegato i dettagli della tua transazione." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_REVERSE + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_MOV + "_KO",
                                                                        new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� scalato.<br />In allegato i dettagli della tua transazione." });
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_MOV + "_ERROR",
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_OK",
                                                                        new String[] {
            "Transazione Annullata",
            "La tua transazione � stata annullata. Nessun importo verr� scalato.<br />In allegato i dettagli della tua transazione." });
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_ERROR",
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });

                                                                // PostPaid CANCELLED
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED,
                                                                        new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� scalato.<br />In allegato i dettagli della tua transazione." });

                                                                // PostPaid REVERSED
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED,
                                                                        new String[] {
            "Transazione Stornata",
            "la tua transazione � stata stornata. A breve riceverai una comunicazione e il tuo credito verr� reso nuovamente disponibile secondo i tempi previsti.<br />In allegato i dettagli della tua transazione." });

                                                                //PrePaid
                                                                put(StatusHelper.FINAL_STATUS_TYPE_ERROR, new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });
                                                                
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT, new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });
                                                                
                                                                put(StatusHelper.FINAL_STATUS_TYPE_FAILED,
                                                                        new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� scalato.<br />In allegato i dettagli della tua transazione." });
                                                                
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_AFTER_REFUEL,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });
                                                                
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });
                                                                
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL, new String[] {
            "Transazione Completata",
            "in allegato i dettagli della tua transazione."                                                  });
                                                                
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "_" + StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED,
                                                                        new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� scalato.<br />In allegato i dettagli della tua transazione." });
            
            // Inizio stati transazioni sosta
                                                                put(StatusHelper.PARKING_STATUS_ENDED, new String[] {
            "Transazione Completata",
            "in allegato i dettagli della tua transazione."                                                  });
            
                                                                put(StatusHelper.PARKING_STATUS_FAILED, new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� scalato.<br />In allegato i dettagli della tua transazione."                                                  });
                                                                
                                                                put(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE, new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione."                                                  });
            // Fine stati transazioni sosta
                                                                
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "_" + StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED,
                                                                        new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� scalato.<br />In allegato i dettagli della tua transazione." });

                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_PAY_AUTH,
                                                                        new String[] {
            "Transazione Negata",
            "l'autorizzazione per il pagamento � stata negata dall'Acquirer. Verifica che la tua carta non sia scaduta e ci sia credito a sufficienza, oppure contatta l'istituto bancario o finanziario emettitore della tua carta per richiedere informazioni.<br />In allegato i dettagli della tua transazione." });

                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_CREATE,
                                                                        new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� addebitato sulla tua carta. Un eventuale importo bloccato verr� reso nuovamente disponibile secondo i tempi previsti dall'istituto bancario o finanziario emettitore della carta.<br />In allegato i dettagli della tua transazione." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_PAY_MOV,
                                                                        new String[] {
            "Transazione Annullata",
            "la tua transazione � stata annullata. Nessun importo verr� addebitato sulla tua carta. Un eventuale importo bloccato verr� reso nuovamente disponibile secondo i tempi previsti dall'istituto bancario o finanziario emettitore della carta.<br />In allegato i dettagli della tua transazione." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_AUTH,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_MOV,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_DELETE,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata.<br />In allegato i dettagli della tua transazione." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_CAN,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "" });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_CANCELED, new String[] {
            "Transazione Annullata", 
            "La tua transazione � stata annullata. Nessun importo verr� addebitato sulla tua carta. Un eventuale importo bloccato verr� reso nuovamente disponibile secondo i tempi previsti dall'istituto bancario o finanziario emettitore della carta." });
                                                            }
                                                        };

    @SuppressWarnings("serial")
    private static HashMap<String, String[]> receiptMap = new HashMap<String, String[]>() {
                                                            {
                                                                //PostPaid PAID
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID,
                                                                        new String[] {
            "Transazione Completata",
            "Transazione Completata" });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID + "_" 
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_OK",
                                                                        new String[] {
            "Transazione Completata",
            "Transazione Completata" });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_PAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_COMPLETE,
                                                                        new String[] {
            "Transazione Completata",
            "Transazione Completata" });

                                                                // PostPaid UNPAID
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_NOTIFICATION_FAIL,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata." });
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_CANCELED,
                                                                        new String[] {
            "Transazione Annullata",
            "La tua transazione � stata annullata. Nessun importo verr� scalato." });
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_VOUCHER + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER + "_ERROR", 
                                                                        new String[] {
            "Transazione In Elaborazione",
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_VOUCHER + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER + "_KO", 
                                                                        new String[] {
            "Transazione Annullata",
            "La tua transazione � stata annullata. Nessun importo verr� scalato." });
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_AUTH_MISSING,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_AUTH_DELETE_MISSING,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_VOUCHER_FAIL,
                                                                        new String[] {
            "Transazione Annullata",
            "La tua transazione � stata annullata. Nessun importo verr� scalato." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_REVERSE + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE + "_OK",
                                                                        new String[] {
            "Transazione Annullata",
            "La tua transazione � stata annullata. Nessun importo verr� scalato." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_REVERSE + "_"
                                                                        + StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE,
                                                                        new String[] {
            "Transazione In Elaborazione",
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_MOV + "_KO",
                                                                        new String[] {
            "Transazione Annullata",
            "La tua transazione � stata annullata. Nessun importo verr� scalato." });
                                                                
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_MOV + "_ERROR",
                                                                        new String[] {
            "Transazione In Elaborazione",
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata." });

                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_OK",
                                                                        new String[] {
            "Transazione Annullata",
            "La tua transazione � stata annullata. Nessun importo verr� scalato." });
                                                                
                                                               put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_ERROR",
                                                                        new String[] {
            "Transazione In Elaborazione",
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata." });

                                                                // PostPaid CANCELLED
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED,
                                                                        new String[] {
            "Transazione Annullata",
            "La tua transazione � stata annullata. Nessun importo verr� scalato." });

                                                                // PostPaid REVERSED
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED,
                                                                        new String[] {
            "Transazione Stornata",
            "La tua transazione � stata stornata. A breve riceverai una comunicazione e il tuo credito verr� reso nuovamente disponibile secondo i tempi previsti." });

                                                                // PostPaid LOYALTY REVERSED
                                                                put(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID + "_"
                                                                        + StatusHelper.POST_PAID_EVENT_BE_LOYALTY_REVERSE + "_KO",
                                                                        new String[] {
            "Transazione Annullata",
            "La tua transazione � stata annullata. Nessun importo verr� scalato." });
                                                                
                                                                //PrePaid
                                                                put(StatusHelper.FINAL_STATUS_TYPE_ABEND, new String[] {
            "Transazione In Elaborazione", 
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata."                        });
                                                                put(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE, new String[] {
            "Transazione In Elaborazione", 
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata."                        });
                                                                put(StatusHelper.FINAL_STATUS_TYPE_ERROR, new String[] {
            "Transazione In Elaborazione", 
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata."                        });
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT, new String[] {
            "Transazione In Elaborazione", 
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata."                        });
                                                                put(StatusHelper.FINAL_STATUS_TYPE_FAILED, new String[] {
            "Transazione Annullata", 
            "La tua transazione � stata annullata. Nessun importo verr� scalato."  });
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_AFTER_REFUEL, new String[] {
            "Transazione In Elaborazione", 
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata."  });
                                                                put(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL, new String[] {
            "Transazione In Elaborazione", 
            "Si � verificato un problema con la tua transazione. Sono in corso delle verifiche e riceverai una comunicazione appena la transazione verr� elaborata."   });
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL, new String[] {
            "Transazione Completata", "Transazione Completata"                               });
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "_" + StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED, new String[] {
            "Transazione Annullata", 
            "La tua transazione � stata annullata. Nessun importo verr� scalato."  });
                                                                put(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL + "_" + StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED, new String[] {
            "Transazione Annullata", 
            "La tua transazione � stata annullata. Nessun importo verr� scalato."   });

                                                                
                                                                // Voucher Carburante
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_AUTH, new String[] {
            "Transazione In Elaborazione", 
            "Si � verificato un problema con la tua transazione. A breve riceverai una comunicazione e l'importo bloccato sulla tua carta verr� reso nuovamente disponibile secondo i tempi previsti dall'istituto bancario o finanziario emettitore della tua carta." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE, new String[] {
            "Transazione In Elaborazione", 
            "Si � verificato un problema con la tua transazione. A breve riceverai una comunicazione e l'importo bloccato sulla tua carta verr� reso nuovamente disponibile secondo i tempi previsti dall'istituto bancario o finanziario emettitore della tua carta." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_MOV, new String[] {
            "Transazione In Elaborazione", 
            "Si � verificato un problema con la tua transazione. A breve riceverai una comunicazione e l'importo bloccato sulla tua carta verr� reso nuovamente disponibile secondo i tempi previsti dall'istituto bancario o finanziario emettitore della tua carta." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_PAY_AUTH, new String[] {
            "Transazione Negata", 
            "L'autorizzazione per il pagamento � stata negata dall'Acquirer. Verifica che la tua carta non sia scaduta e ci sia credito a sufficienza, oppure contatta l'istituto bancario o finanziario emettitore della tua carta per richiedere informazioni." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_PAY_MOV, new String[] {
            "Transazione Annullata", 
            "La tua transazione � stata annullata. Nessun importo verr� addebitato sulla tua carta. Un eventuale importo bloccato verr� reso nuovamente disponibile secondo i tempi previsti dall'istituto bancario o finanziario emettitore della carta." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_FAILED_CREATE, new String[] {
            "Transazione Annullata", 
            "La tua transazione � stata annullata. Nessun importo verr� addebitato sulla tua carta. Un eventuale importo bloccato verr� reso nuovamente disponibile secondo i tempi previsti dall'istituto bancario o finanziario emettitore della carta." });
                                                                put(StatusHelper.VOUCHER_FINAL_STATUS_CANCELED, new String[] {
            "Transazione Annullata", 
            "La tua transazione � stata annullata. Nessun importo verr� addebitato sulla tua carta. Un eventuale importo bloccato verr� reso nuovamente disponibile secondo i tempi previsti dall'istituto bancario o finanziario emettitore della carta." });
                                                            }
                                                        };

    public static String getReconciliationTransactionText(String status, String event) {
        return getReconciliationTransactionText(status, event, null, null);
    }
    
    public static String getReconciliationTransactionText(String status, String event, String eventCode, String eventResult) {
        return getText(reconciliationTransactionMap, status, event, eventCode, eventResult);
    }

    public static String getReconciliationUserText(String status) {
        return getReconciliationUserText(status, null);
    }

    public static String getReconciliationUserText(String status, String event) {
        return getText(reconciliationUserMap, status, event, null, null);
    }
    
    public static String getReconciliationParkingText(String status) {
        return getReconciliationParkingText(status, null);
    }

    public static String getReconciliationParkingText(String status, String event) {
        return getText(reconciliationParkingMap, status, event, null, null);
    }

    public static String getReportText(String status, String event) {
        return getReportText(status, event, null, null);
    }
    
    public static String getReportText(String status, String event, String eventCode, String eventResult) {
        return getText(reportMap, status, event, eventCode, eventResult);
    }
    
    public static String getReceiptTitleText(String status, String event) {
        return getReceiptTitleText(status, event, null, null);
    }
    
    public static String getReceiptTitleText(String status, String event, String eventCode, String eventResult) {
        return getText(receiptMap, 0, status, event, eventCode, eventResult);
    }
    
    public static String getReceiptDescriptionText(String status, String event) {
        return getReceiptDescriptionText(status, event, null, null);
    }
    
    public static String getReceiptDescriptionText(String status, String event, String eventCode, String eventResult) {
        return getText(receiptMap, 1, status, event, eventCode, eventResult);
    }

    public static String getMailTransactionStatusText(String status, String event) {
        return getMailTransactionStatusText(status, event, null, null);
    }
    
    public static String getMailTransactionStatusText(String status, String event, String eventCode, String eventResult) {
        return getText(mailMap, 0, status, event, eventCode, eventResult);
    }
        
    public static String getMailTransactionMessageText(String status, String event) {
        return getMailTransactionMessageText(status, event, null, null);
    }
    
    public static String getMailTransactionMessageText(String status, String event, String eventCode, String eventResult) {
        return getText(mailMap, 1, status, event, eventCode, eventResult);
    }
    
    private static String getText(HashMap<String, String[]> map, int mapIndex, String status, String event, String eventCode, String eventResult) {
        String keyStatus = status;

        if (event != null && eventCode != null && eventResult != null) {
            String keyStatusAndEvent = keyStatus + "_" + event;
            String keyStatusAndEventResult = keyStatus + "_" + eventResult;
            String keyStatusAndEventAndEventCode = keyStatusAndEvent + "_" + eventCode;
            String keyStatusAndEventAndEventResult = keyStatusAndEvent + "_" + eventResult;
            String keyStatusAndEventAndEventCodeAndEventResult = keyStatusAndEventAndEventCode + "_" + eventResult;
            
            if (map.containsKey(keyStatusAndEventAndEventCodeAndEventResult)) {
                return map.get(keyStatusAndEventAndEventCodeAndEventResult)[mapIndex];
            }
            
            if (map.containsKey(keyStatusAndEventAndEventResult)) {
                return map.get(keyStatusAndEventAndEventResult)[mapIndex];
            }

            if (map.containsKey(keyStatusAndEventAndEventCode)) {
                return map.get(keyStatusAndEventAndEventCode)[mapIndex];
            }

            if (map.containsKey(keyStatusAndEvent)) {
                return map.get(keyStatusAndEvent)[mapIndex];
            }

            if (map.containsKey(keyStatusAndEventResult)) {
                return map.get(keyStatusAndEventResult)[mapIndex];
            }

            if (map.containsKey(keyStatus)) {
                return map.get(keyStatus)[mapIndex];
            }
        }
        
        if (event != null && eventCode != null && eventResult == null) {
            String keyStatusAndEvent = keyStatus + "_" + event;
            String keyStatusAndEventAndEventCode = keyStatusAndEvent + "_" + eventCode;
            
            if (map.containsKey(keyStatusAndEventAndEventCode)) {
                return map.get(keyStatusAndEventAndEventCode)[mapIndex];
            }

            if (map.containsKey(keyStatusAndEvent)) {
                return map.get(keyStatusAndEvent)[mapIndex];
            }

            if (map.containsKey(keyStatus)) {
                return map.get(keyStatus)[mapIndex];
            }
        }
        
        if (event != null && eventCode == null && eventResult != null) {
            String keyStatusAndEvent = keyStatus + "_" + event;
            String keyStatusAndEventAndEventResult = keyStatusAndEvent + "_" + eventResult;
            
            if (map.containsKey(keyStatusAndEventAndEventResult)) {
                return map.get(keyStatusAndEventAndEventResult)[mapIndex];
            }

            if (map.containsKey(keyStatusAndEvent)) {
                return map.get(keyStatusAndEvent)[mapIndex];
            }

            if (map.containsKey(keyStatus)) {
                return map.get(keyStatus)[mapIndex];
            }
        }
        
        if (event != null && eventCode == null && eventResult == null) {
            String keyStatusAndEvent = keyStatus + "_" + event;
            
            if (map.containsKey(keyStatusAndEvent)) {
                return map.get(keyStatusAndEvent)[mapIndex];
            }

            if (map.containsKey(keyStatus)) {
                return map.get(keyStatus)[mapIndex];
            }
        }
        
        if (event == null && eventCode == null && eventResult == null) {

            if (map.containsKey(keyStatus)) {
                return map.get(keyStatus)[mapIndex];
            }
        }
        
        return keyStatus;
    }
    
    private static String getText(HashMap<String, String> map, String status, String event, String eventCode, String eventResult) {
        String keyStatus = status;
        
        if (event != null && eventCode != null && eventResult != null) {
            String keyStatusAndEvent = keyStatus + "_" + event;
            String keyStatusAndEventResult = keyStatus + "_" + eventResult;
            String keyStatusAndEventAndEventCode = keyStatusAndEvent + "_" + eventCode;
            String keyStatusAndEventAndEventResult = keyStatusAndEvent + "_" + eventResult;
            String keyStatusAndEventAndEventCodeAndEventResult = keyStatusAndEventAndEventCode + "_" + eventResult;
            
            if (map.containsKey(keyStatusAndEventAndEventCodeAndEventResult)) {
                return map.get(keyStatusAndEventAndEventCodeAndEventResult);
            }
            
            if (map.containsKey(keyStatusAndEventAndEventResult)) {
                return map.get(keyStatusAndEventAndEventResult);
            }

            if (map.containsKey(keyStatusAndEventAndEventCode)) {
                return map.get(keyStatusAndEventAndEventCode);
            }

            if (map.containsKey(keyStatusAndEvent)) {
                return map.get(keyStatusAndEvent);
            }

            if (map.containsKey(keyStatusAndEventResult)) {
                return map.get(keyStatusAndEventResult);
            }

            if (map.containsKey(keyStatus)) {
                return map.get(keyStatus);
            }
        }
        
        if (event != null && eventCode != null && eventResult == null) {
            String keyStatusAndEvent = keyStatus + "_" + event;
            String keyStatusAndEventAndEventCode = keyStatusAndEvent + "_" + eventCode;
            
            if (map.containsKey(keyStatusAndEventAndEventCode)) {
                return map.get(keyStatusAndEventAndEventCode);
            }

            if (map.containsKey(keyStatusAndEvent)) {
                return map.get(keyStatusAndEvent);
            }

            if (map.containsKey(keyStatus)) {
                return map.get(keyStatus);
            }
        }
        
        if (event != null && eventCode == null && eventResult != null) {
            String keyStatusAndEvent = keyStatus + "_" + event;
            String keyStatusAndEventAndEventResult = keyStatusAndEvent + "_" + eventResult;
            
            if (map.containsKey(keyStatusAndEventAndEventResult)) {
                return map.get(keyStatusAndEventAndEventResult);
            }

            if (map.containsKey(keyStatusAndEvent)) {
                return map.get(keyStatusAndEvent);
            }

            if (map.containsKey(keyStatus)) {
                return map.get(keyStatus);
            }
        }
        
        if (event != null && eventCode == null && eventResult == null) {
            String keyStatusAndEvent = keyStatus + "_" + event;
            
            if (map.containsKey(keyStatusAndEvent)) {
                return map.get(keyStatusAndEvent);
            }

            if (map.containsKey(keyStatus)) {
                return map.get(keyStatus);
            }
        }
        
        if (event == null && eventCode == null && eventResult == null) {

            if (map.containsKey(keyStatus)) {
                return map.get(keyStatus);
            }
        }
        
        return keyStatus;
    }    
    
} 
