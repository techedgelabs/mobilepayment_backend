package com.techedge.mp.core.business.utilities;

import java.util.List;

public abstract class AbstractFTPService {
    
    public final static String separator = "/";
    
    protected String host;
    protected String port;
    protected String username;
    protected String password;
    protected String proxyHost;
    protected String proxyPort;

    
    public AbstractFTPService(String host,
            String port,
            String username,
            String password,
            String proxyHost,
            String proxyPort) {
        
        this.host      = host;
        this.port      = port;
        this.username  = username;
        this.password  = password;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
    }
    
    
    protected void setProxy() {
        System.out.println("FTPSService->setProxy");
        if (this.proxyHost != null && !this.proxyHost.equals("")) {
            System.setProperty("http.proxyHost", this.proxyHost);
            System.setProperty("http.proxyPort", this.proxyPort);
            System.setProperty("https.proxyHost", this.proxyHost);
            System.setProperty("https.proxyPort", this.proxyPort);
            System.setProperty("ftp.proxyHost", this.proxyHost);
            System.setProperty("ftp.proxyPort", this.proxyPort);
        }
        else {
            unsetProxy();
        }
    }
    
    
    protected void unsetProxy() {
        
        System.out.println("FTPSService->unsetProxy....(http/https disabled)");
        //System.clearProperty("http.proxyHost");
        //System.clearProperty("http.proxyPort");
        //System.clearProperty("https.proxyHost");
        //System.clearProperty("https.proxyPort");
        System.clearProperty("ftp.proxyHost");
        System.clearProperty("ftp.proxyPort");
    }
    
    
    protected abstract Boolean init();
    
    protected abstract Boolean closeConnection();
    
    public abstract String getFile(String remote);
    
    public abstract Boolean putFile(String data, String remote);
    
    public abstract List<String> openDirectory(String path);
    
    public abstract List<String> listDirectory(String path);
    
    public abstract Boolean moveFile(String remoteSource, String remoteDest);
    
    public abstract Boolean deleteFile(String remote);
}
