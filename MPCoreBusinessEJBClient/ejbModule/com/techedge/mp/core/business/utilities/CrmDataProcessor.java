package com.techedge.mp.core.business.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

import com.techedge.mp.core.business.interfaces.crm.CrmDataElement;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElementProcessingResult;

public class CrmDataProcessor {

    private final static String lineSeparator = "\n";

    public CrmDataProcessor() {

    }

    public static List<CrmDataElement> process(String data, int maxDataPerThread, int maxThreads) {

        List<CrmDataElement> elements = new ArrayList<CrmDataElement>(0);

        String[] lines = data.split(Pattern.quote(lineSeparator));
        
        // Faccio lo skyip della header della prima riga 
        lines = Arrays.copyOfRange(lines, 1, lines.length);
        
        int totalDataToProcess = lines.length;
        int threadCount = 1;
        
        System.out.println("Dati lettura CSV: " + totalDataToProcess);

        if (totalDataToProcess > maxDataPerThread) {
            threadCount = totalDataToProcess / maxDataPerThread;
            
            if (totalDataToProcess % maxDataPerThread > 0) {
                threadCount++;
            }
        }
        else {
            maxDataPerThread = totalDataToProcess;
        }

        ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
        
        for (int i = 0; i < threadCount; i++) {
            
            int start = i * maxDataPerThread;
            int end = start + maxDataPerThread;
            
            if (end > totalDataToProcess) {
                end = totalDataToProcess;
            }
            
            System.out.println("THREAD LETTURA CSV N " + (i + 1) + " indice inizio: " + start + "  valore: " + lines[start]);
            System.out.println("THREAD LETTURA CSV N " + (i + 1) + " indice fine: " + end + "  valore: " + lines[end - 1]);
            
            CRMDataElementProcess crmDataElementProcess = new CRMDataElementProcess(i + 1, lines, start, end, elements);
            
            executor.submit(crmDataElementProcess);
        }
        
        executor.shutdown();

        while (!executor.isTerminated()) {}

        System.out.println("THREADS LETTURA CSV TERMINATI");
        
        return elements;
    }

    public static CrmDataElementProcessingResult createCrmDataElementProcessingResult(CrmDataElement crmDataElement, String categoryDesc, String deliveryId) {

        CrmDataElementProcessingResult crmDataElementProcessingResult = new CrmDataElementProcessingResult();
        crmDataElementProcessingResult.canale = crmDataElement.canale;
        crmDataElementProcessingResult.categoryDesc = categoryDesc;
        crmDataElementProcessingResult.cdCarta = crmDataElement.cdCarta;
        crmDataElementProcessingResult.cdCliente = crmDataElement.cdCliente;
        crmDataElementProcessingResult.codFiscale = crmDataElement.codiceFiscale;
        crmDataElementProcessingResult.codIniziativa = crmDataElement.codIniziativa;
        crmDataElementProcessingResult.codOfferta = crmDataElement.codOfferta;
        crmDataElementProcessingResult.deliveryId = deliveryId;
        crmDataElementProcessingResult.tmsInserimento = crmDataElement.tmsInserimento;
        crmDataElementProcessingResult.treatmentCode = crmDataElement.treatmentCode;
        return crmDataElementProcessingResult;
    }

    public static String generateOutput(List<CrmDataElementProcessingResult> crmDataElementProcessingResultList) {

        String output = "";

        for (CrmDataElementProcessingResult crmDataElementProcessingResult : crmDataElementProcessingResultList) {
            output = output + crmDataElementProcessingResult.toString() + lineSeparator;
        }

        return output;
    }

    private static class CRMDataElementProcess implements Runnable {
        
        private String[] lines;
        private int start;
        private int end;
        private int threadID;
        private List<CrmDataElement> elements;
        
        
        public CRMDataElementProcess(int threadID, String[] lines, int start, int end, List<CrmDataElement> elements) {
            this.threadID = threadID;
            this.lines = lines;
            this.start = start;
            this.end = end;
            this.elements = elements;
        }

        @Override
        public void run() {

            System.out.println("THREAD LETTURA CSV N " + threadID + " start");
            
            for (int i = start; i < end; i++) {
                CrmDataElement crmDataElement = new CrmDataElement();
                Boolean result = crmDataElement.process(lines[i]);
                if (result) {
                    elements.add(crmDataElement);
                }
                else {
                  System.err.println("THREAD LETTURA CSV N " + threadID + " dato non valido. Che si fa?? " + lines[i]);
                }
            }
            
            System.out.println("THREAD LETTURA CSV N " + threadID + " end");

        }
        
    }

}
