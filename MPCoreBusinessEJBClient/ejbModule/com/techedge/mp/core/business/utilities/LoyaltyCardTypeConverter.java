package com.techedge.mp.core.business.utilities;

import java.util.HashMap;

public class LoyaltyCardTypeConverter {

    @SuppressWarnings("serial")
    private static final HashMap<String, String> cardType = new HashMap<String, String>() {
         {
             put("R", "02");    //Carta Rossa
             put("B", "03");    //Carta Blu
             put("Y", "01");    //Carta Gialla
             put("G", "99");    //Altre
         }
     };

    public LoyaltyCardTypeConverter() {}

    public static String toFrontendAdapter(String value) {

        if (value == null || !cardType.containsKey(value)) {
            return null;
        }

        return cardType.get(value);
    }

}
