package com.techedge.mp.core.business.utilities;

public enum EncryptionAlgorithmType {
    AES("AES"),
    RSA("RSA");
    
    private final String value;

    EncryptionAlgorithmType(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value; 
    }
}
