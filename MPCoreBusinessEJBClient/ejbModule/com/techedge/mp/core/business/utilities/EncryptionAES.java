package com.techedge.mp.core.business.utilities;

import java.security.InvalidKeyException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class EncryptionAES {
    
    private byte[] secretKey;

    private static final String ALGORITHM = "AES/ECB/PKCS5Padding";
    private static final String ALGORITHM_KEY = "AES";

    private BASE64Encoder encoder = new BASE64Encoder();
    
    private BASE64Decoder decoder = new BASE64Decoder();
    
    public EncryptionAES() {
        //java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }
    
    public void loadSecretKey(String secretKey) throws Exception {
        if (secretKey == null) {
            throw new InvalidKeyException("Invalid secret key: null");
        }

        this.secretKey = secretKey.getBytes();
    }

    /**
     * Encrypts the given plain text
     *
     * @param plainText The plain text to encrypt
     */
    public String encrypt(String plainText) throws Exception
    {
        if (secretKey == null) {
            throw new InvalidKeyException("Invalid secret key: null");
        }
        
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, ALGORITHM_KEY);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        
        byte[] hexEncodedCipher = cipher.doFinal(plainText.getBytes());
        String encryptedText = encoder.encode(hexEncodedCipher);

        //System.out.println("Original: " + plainText);
        //System.out.println("Encrypted: " + encryptedText);
        
        return encryptedText;
    }

    /**
     * Decrypts the given byte array
     *
     * @param cipherText The data to decrypt
     */
    public String decrypt(String cipherText) throws Exception
    {
        if (secretKey == null) {
            throw new InvalidKeyException("Invalid secret key: null");
        }
        
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, ALGORITHM_KEY);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        
        byte[] decodedText = decoder.decodeBuffer(cipherText);
        byte [] hexEncodedCipher = cipher.doFinal(decodedText);
        
        String decryptedText = new String(hexEncodedCipher);
        
        //System.out.println("Original: " + cipherText);
        //System.out.println("Decrypted: " + new String(decryptedText));
        
        return decryptedText;
    }

}
