package com.techedge.mp.core.business.utilities;


public class Proxy {

    private String host;
    private String port;
    private String noHosts;
    //private HashMap<String, String> backup = new HashMap<String, String>();
    
    public Proxy(String host, String port, String noHosts) {
        this.host = host;
        this.port = port;
        this.noHosts = noHosts;
    }
    
    public Proxy() {
        this(null, null, null);
    }
    
    public void setHttp() {
        System.out.println("Setting Proxy Http/Https....");
        
        String httpProxyHost = System.getProperty("http.proxyHost");
        String httpsProxyHost = System.getProperty("https.proxyHost");

        if (host != null) {
            // Impostazione proxy
            if (!host.equals(httpProxyHost)) {
                System.setProperty("http.proxyHost", host);
                System.setProperty("http.proxyPort", port);
                System.setProperty("http.nonProxyHosts", noHosts);
            }
            
            if (!host.equals(httpsProxyHost)) {
                System.setProperty("https.proxyHost", host);
                System.setProperty("https.proxyPort", port);
                System.setProperty("https.nonProxyHosts", noHosts);
            }
            
            
        }
    }

    public void unsetHttp() {
        System.out.println("Unsetting Proxy Http/Https....(disabled)");
        // Ripristino proxy
        //System.clearProperty("http.proxyHost");
        //System.clearProperty("http.proxyPort");
        //System.clearProperty("http.nonProxyHosts");
        //System.clearProperty("https.proxyHost");
        //System.clearProperty("https.proxyPort");
        //System.clearProperty("https.nonProxyHosts");
    }
        
    /*
    public void setHttp() {
        System.out.println("Setting Proxy Http/Https....");
        
        backup.clear();
        
        String httpProxyHost = System.getProperty("http.proxyHost");
        String httpProxyPort = System.getProperty("http.proxyPort");
        String httpsProxyHost = System.getProperty("https.proxyHost");
        String httpsProxyPort = System.getProperty("https.proxyPort");
        
        if (httpProxyHost != null) {
            backup.put("http.proxyHost", httpProxyHost);
        }

        if (httpProxyPort != null) {
            backup.put("http.proxyPort", httpProxyPort);
        }

        if (httpsProxyHost != null) {
            backup.put("https.proxyHost", httpsProxyHost);
        }

        if (httpsProxyPort != null) {
            backup.put("https.proxyPort", httpsProxyPort);
        }

        if (host != null) {
            // Impostazione proxy
            if (!host.equals(httpProxyHost)) {
                System.setProperty("http.proxyHost", host);
                System.setProperty("http.proxyPort", port);
                
                //System.out.println("Setting Proxy Http Host: " + host);
                //System.out.println("Setting Proxy Http Port: " + port);
            }
            
            if (!host.equals(httpsProxyHost)) {
                System.setProperty("https.proxyHost", host);
                System.setProperty("https.proxyPort", port);
                
                //System.out.println("Setting Proxy Https Host: " + host);
                //System.out.println("Setting Proxy Https Port: " + port);
            }
        }
        else {
            System.clearProperty("http.proxyHost");
            System.clearProperty("http.proxyPort");
            
            //System.out.println("Clearing Proxy Http Host");
            //System.out.println("Clearing Proxy Http Port");
            
            System.clearProperty("https.proxyHost");
            System.clearProperty("https.proxyPort");
            
            //System.out.println("Clearing Proxy Https Host");
            //System.out.println("Clearing Proxy Https Port");
        }
    }
    
    public void unsetHttp(boolean forceClear) {
        System.out.println("Unsetting Proxy Http/Https....");
        // Ripristino proxy
        if (!forceClear && backup.get("http.proxyHost") != null) {
            if (!backup.get("http.proxyHost").equals(System.getProperty("http.proxyHost"))) {
                System.setProperty("http.proxyHost", backup.get("http.proxyHost"));
                System.setProperty("http.proxyPort", backup.get("http.proxyPort"));
                
                //System.out.println("Restoring Proxy Http Host: " + backup.get("http.proxyHost"));
                //System.out.println("Restoring Proxy Http Port: " + backup.get("http.proxyPort"));
            }
        }
        else {
            System.clearProperty("http.proxyHost");
            System.clearProperty("http.proxyPort");
            
            //System.out.println("Clearing Proxy Http Host");
            //System.out.println("Clearing Proxy Http Port");
        }

        if (!forceClear && backup.get("https.proxyHost") != null) {
            if (!backup.get("https.proxyHost").equals(System.getProperty("https.proxyHost"))) {
                System.setProperty("https.proxyHost", backup.get("https.proxyHost"));
                System.setProperty("https.proxyPort", backup.get("https.proxyPort"));
                
                //System.out.println("Restoring Proxy Https Host: " + backup.get("https.proxyHost"));
                //System.out.println("Restoring Proxy Https Port: " + backup.get("https.proxyPort"));
            }
        }
        else {
            System.clearProperty("https.proxyHost");
            System.clearProperty("https.proxyPort");
            
            //System.out.println("Clearing Proxy Https Host");
            //System.out.println("Clearing Proxy Https Port");
        }
    }
    
    public void unsetHttp() {
        unsetHttp(false);
    }    
    */
}
