package com.techedge.mp.core.business.utilities.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.techedge.mp.core.business.utilities.FTPSService;

public class FTPServiceTest {

    FTPSService service = null;

    @Before
    public void setUp() {

        String host      = "mpsquid.enimp.pri";
        String port      = "2222";
        String username  = "Techedge001";
        String password  = "Eni2017,";
        String proxyHost = null;
        String proxyPort = "0";

        this.service = new FTPSService(host, port, username, password, proxyHost, proxyPort);
    }

    //@Test
    public void testLogin() {

        String path = "/";

        List<String> files = service.openDirectory(path);
        
        assertFalse(files.isEmpty());
    }
    
    //@Test
    public void testListDirectory() {

        String path = "/";

        List<String> files = service.listDirectory(path);
        
        assertFalse(files.isEmpty());
    }
    
    //@Test
    public void testGetFile() {

        String remote = "/read/C000000588-000001381_20170522-182855_app_TargetList.csv";

        String data = service.getFile(remote);
        
        assertNotNull(data);
    }
    
    @Test
    public void testPutFile() {

        String data   = "data test";
        String remote = "/output.txt";

        Boolean result = service.putFile(data, remote);
        
        assertTrue(result);
    }
    
    //@Test
    public void testMoveFile() {

        String remoteSource = "/read/C000000588-000001381_20170522-182855_app_TargetList.csv";
        String remoteDest   = "/archive/C000000588-000001381_20170522-182855_app_TargetList.csv";

        Boolean result = service.moveFile(remoteSource, remoteDest);
        
        assertTrue(result);
    }

    /*
     * @SuppressWarnings("deprecation")
     * 
     * @Test(expected = PromotionException.class)
     * public void testUpdateEndDateBefore() throws PromotionException {
     * try {
     * promoBean.update("NewPromotion", "Updated Description", new Date(2016, 4, 12), new Date(2016, 2, 12));
     * }
     * catch (PromotionException e) {
     * String message = e.getMessage();
     * assertEquals(PromotionBean.START_DATE_OR_END_DATE_OF_PROMOTION_ARE_INCORRECT, message);
     * throw e;
     * }
     * }
     * 
     * @Test(expected = PromotionException.class)
     * public void testAddPromoVouchersWithCodeExist() throws PromotionException {
     * 
     * try {
     * 
     * promoBean.addPromoVouchers(promoVoucherInput);
     * }
     * catch (PromotionException e) {
     * 
     * String message = e.getMessage();
     * assertEquals("Promotion with same code already exist", message);
     * throw e;
     * }
     * }
     */
}
