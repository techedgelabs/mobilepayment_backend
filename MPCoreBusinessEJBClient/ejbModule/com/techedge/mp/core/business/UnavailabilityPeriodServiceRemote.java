package com.techedge.mp.core.business;

import java.util.Date;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;

@Remote
public interface UnavailabilityPeriodServiceRemote {

    public void refreshData();

    public ServiceAvailabilityData retrieveServiceAvailability(String operation, Date time);
}
