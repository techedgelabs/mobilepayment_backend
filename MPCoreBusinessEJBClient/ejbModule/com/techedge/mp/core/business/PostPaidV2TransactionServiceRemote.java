package com.techedge.mp.core.business;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.GetStationResponse;
import com.techedge.mp.core.business.interfaces.PostPaidTransaction;
import com.techedge.mp.core.business.interfaces.postpaid.GetSourceDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveMulticardShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;

@Remote
public interface PostPaidV2TransactionServiceRemote {

    public GetSourceDetailResponse createPopTransaction(String requestID, String ticketID, String sourceID);

    public PostPaidApproveShopTransactionResponse approvePopTransaction(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentMethodType,
            String encodedPin);

    public PostPaidApproveShopTransactionResponse approvePopTransactionBusiness(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentMethodType,
            String encodedPin);

    public GetSourceDetailResponse getSourceDetail(String requestID, String ticketID, String codeType, String sourceID, Double userPositionLatitude, Double userPositionLongitude);

    public GetStationResponse getStation(String requestID, String ticketID, Double userPositionLatitude, Double userPositionLongitude, String stationID, Boolean clearTransaction, Boolean refresh);
    
    public GetStationResponse retrieveStation(String requestID, String ticketID, String codeType, String beaconCode, Double userPositionLatitude, Double userPositionLongitude);
    
    public PostPaidApproveMulticardShopTransactionResponse approveMulticardPopTransaction(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentCryptogram);
    
    public PostPaidTransaction getPopTransactionDetail(String requestId, String mpTransactionID);
}
