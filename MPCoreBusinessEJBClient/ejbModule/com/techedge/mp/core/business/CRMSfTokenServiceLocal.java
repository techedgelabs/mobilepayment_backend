package com.techedge.mp.core.business;

import javax.ejb.Local;

@Local
public interface CRMSfTokenServiceLocal {

    public String manageToken(String newToken);
}
