package com.techedge.mp.core.business;
import java.util.ArrayList;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.station.StationUpdateDetail;

@Remote
public interface StationServiceRemote {
 
    public String updateStaging(String requestID, ArrayList<StationUpdateDetail> stationList);
    
    public String updateMaster(String requestID);
}
