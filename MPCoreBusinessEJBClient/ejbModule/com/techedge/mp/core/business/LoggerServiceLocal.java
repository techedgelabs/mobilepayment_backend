package com.techedge.mp.core.business;

import javax.ejb.Local;

import com.techedge.mp.core.business.interfaces.ErrorLevel;

@Local
public interface LoggerServiceLocal {

	public void setLogLevel(ErrorLevel level);
	public void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message);
}
