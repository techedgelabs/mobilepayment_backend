package com.techedge.mp.core.business;

import java.util.Date;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.parking.ApproveExtendParkingTransactionResult;
import com.techedge.mp.core.business.interfaces.parking.ApproveParkingTransactionResult;
import com.techedge.mp.core.business.interfaces.parking.Authentication;
import com.techedge.mp.core.business.interfaces.parking.EndParkingResult;
import com.techedge.mp.core.business.interfaces.parking.EstimateEndParkingPriceResult;
import com.techedge.mp.core.business.interfaces.parking.EstimateExtendedParkingPriceResult;
import com.techedge.mp.core.business.interfaces.parking.EstimateParkingPriceResult;
import com.techedge.mp.core.business.interfaces.parking.GetCitiesResult;
import com.techedge.mp.core.business.interfaces.parking.GetParkingTransactionDetailResult;
import com.techedge.mp.core.business.interfaces.parking.RetrieveParkingZonesByCityResult;
import com.techedge.mp.core.business.interfaces.parking.RetrieveParkingZonesResult;
import com.techedge.mp.core.business.interfaces.parking.RetrievePendingParkingTransactionResult;

@Remote
public interface ParkingTransactionV2ServiceRemote {

    public RetrieveParkingZonesResult retrieveParkingZones(String ticketId, String requestId, double latitude, double longitude);

    public GetCitiesResult getCities(String ticketId, String requestId);

    public RetrieveParkingZonesByCityResult retrieveParkingZonesByCity(String ticketId, String requestId, String cityId);

    public EstimateParkingPriceResult estimateParkingPrice(String ticketId, String requestId, String plateNumber, Date requestedEndTime, String parkingZoneId, String stallCode, String cityId);

    public ApproveParkingTransactionResult approveParkingTransaction(String ticketId, String requestId, String transactionId, Long paymentMethidId, String paymentMethodType,
            String encodedPin);

    public EndParkingResult endParking(String ticketId, String requestId, String transactionId);

    public EstimateEndParkingPriceResult estimateEndParkingPrice(String ticketId, String requestId, String transactionId);

    public EstimateExtendedParkingPriceResult estimateExtendedParkingPrice(String ticketId, String requestId, String transactionId, Date requestedEndTime);

    public ApproveExtendParkingTransactionResult approveExtendParkingTransaction(String ticketId, String requestId, String transactionId, Long paymentMethodId,
            String paymentMethodType, String encodedPin);

    public RetrievePendingParkingTransactionResult retrievePendingParkingTransaction(String ticketId, String requestId);

	public Authentication persistAuthToken(Authentication auth);

    public Authentication checkAccessTokenState();
    
    public GetParkingTransactionDetailResult getParkingTransactionDetail(String ticketId, String requestId, String parkingTransactionId);
}
