package com.techedge.mp.core.business;

import java.util.List;

import javax.ejb.Local;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ParamInfo;

@Local
public interface ParametersServiceLocal {

	public String getParamValue(String paramName) throws ParameterNotFoundException;
	
	public String getParamValueNoCache(String paramName) throws ParameterNotFoundException;
	
	public List<ParamInfo> refreshParameters();
}
