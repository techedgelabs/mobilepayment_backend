package com.techedge.mp.core.business;

import java.util.Date;

import javax.ejb.Local;

import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;

@Local
public interface UnavailabilityPeriodServiceLocal {

    public void refreshData();

    public ServiceAvailabilityData retrieveServiceAvailability(String operation, Date time);
}
