package com.techedge.mp.core.business;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.Survey;
import com.techedge.mp.core.business.interfaces.SurveyAnswers;

@Remote
public interface SurveyServiceRemote {

    public Survey surveyGet(String ticketId, String requestId, String code);
    
    public String surveySubmit(String ticketId, String requestId, String code, String key, SurveyAnswers answers);
}

