package com.techedge.mp.core.business;
import java.util.ArrayList;

import javax.ejb.Local;

import com.techedge.mp.core.business.interfaces.station.StationUpdateDetail;

@Local
public interface StationServiceLocal {
    
    public String updateStaging(String requestID, ArrayList<StationUpdateDetail> stationList);
    
    public String updateMaster(String requestID);
}
