package com.techedge.mp.core.business;

import java.util.List;

import javax.ejb.Remote;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ParamInfo;

@Remote
public interface ParametersServiceRemote {

	public String getParamValue(String paramName) throws ParameterNotFoundException;

    public String getParamValueNoCache(String paramName) throws ParameterNotFoundException;
	
	public List<ParamInfo> refreshParameters();
}
