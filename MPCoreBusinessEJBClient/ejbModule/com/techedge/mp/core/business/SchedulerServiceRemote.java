package com.techedge.mp.core.business;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.CrmOutboundProcessResponse;

@Remote
public interface SchedulerServiceRemote {

    public void jobUserNewClear();

    public void jobUserNewResendEmail();

    public void jobReconciliateTransaction();

    public void jobReconciliateUser();

    public void jobReconciliateParking();

    public void jobFinalizePendingTransaction();

    public void jobDailyStatisticsReport();

    public void jobDailyConsumeVoucherReport();

    public void listSchedulers();

    public void cancelSchedulers();

    public void jobPromotion();

    public void jobRemoveOnHoldTransaction();

    public void jobRemoveOldPayment();

    public void jobSmsPendingRetry();

    public void jobRefuelingCheckConnection();

    public void jobFidelityCheckConnection();

    public void jobForecourtCheckConnection();

    public void jobProcessMailingList();

    public CrmOutboundProcessResponse jobProcessCrmOutboundInterface();

    public void jobProcessResultCrmOutboundInterface();
    
    public CrmOutboundProcessResponse jobProcessResultClusterCrmOutboundInterface();
    
    public CrmOutboundProcessResponse jobProcessSubscribeCrmOutboundInterface();

    public CrmOutboundProcessResponse jobProcessCrmSfOutboundInterface();
    
    public void jobProcessResultCrmSfOutboundInterface();
    
    public void jobCRMCheckConnection();
    
    public void jobCRMSfNotifyEventCheckConnection();
    
    public void jobCRMSfGetOffersCheckConnection();

    public void jobDWHCheckConnection();

    public void jobRemovePendingMobilePhone();

    public void jobWeeklyStatisticsReport();
    
    public void jobWeeklyStatisticsReportBusiness();

    public void jobSessionCleaner();

    public void jobUpdateDWHEarnData();

    public void jobUpdateDWHBurnData();

    public void jobUpdateCrmPromotionsData();
    
    public void jobUpdateCrmSfPromotionsData();

    public void jobClosePendingParkingTransaction();
    
    public void jobUpdateStations();

    public void jobMyCiceroUsersRefuelingReport();

    public void jobDailyStatisticsReportBusiness();
    
    public void jobPrepareDailyStatisticsReport();
    
    public void jobSendingDailyStatisticsReport();
    
    public void jobPrepareDailyStatisticsReportBusiness();
    
    public void jobSendingDailyStatisticsReportBusiness();
    
    public void jobCreateVoucherEsPromotion();
}
