package com.techedge.mp.core.business;

import java.util.List;

import javax.ejb.Local;

import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationDetail;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfoData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationParkingData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationParkingSummary;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationTransactionSummary;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserSummary;

@Local
public interface ReconciliationServiceLocal {
    public ReconciliationInfoData reconciliationReconcileTransactions(String adminTicketId, String requestId, List<String> transactionsID);

    public ReconciliationDetail getReconciliationDetail(String adminTicketId, String requestId, String transactionsID);

    public ReconciliationTransactionSummary reconciliationTransaction(List<String> transactionsIDList);

    public ReconciliationUserSummary reconciliationUser(List<ReconciliationUserData> userReconciliationInterfaceIDList);
    
    public ReconciliationParkingSummary reconciliationParking(List<ReconciliationParkingData> parkingReconciliationInterfaceIDList);
    
}
