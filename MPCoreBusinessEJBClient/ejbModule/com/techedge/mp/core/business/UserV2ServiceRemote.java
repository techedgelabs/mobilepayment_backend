package com.techedge.mp.core.business;

import java.util.HashMap;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.AuthenticationBusinessResponse;
import com.techedge.mp.core.business.interfaces.AuthenticationResponse;
import com.techedge.mp.core.business.interfaces.AwardDetailDataResponse;
import com.techedge.mp.core.business.interfaces.BrandDataDetailResponse;
import com.techedge.mp.core.business.interfaces.CategoryEarnDataDetailResponse;
import com.techedge.mp.core.business.interfaces.CreateUserGuestResult;
import com.techedge.mp.core.business.interfaces.ExternalAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ExternalProviderAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.GetHomePartnerListResult;
import com.techedge.mp.core.business.interfaces.GetPromoPopupDataResponse;
import com.techedge.mp.core.business.interfaces.GetReceiptDataResponse;
import com.techedge.mp.core.business.interfaces.LandingDataResponse;
import com.techedge.mp.core.business.interfaces.LoadVoucherEventCampaignResponse;
import com.techedge.mp.core.business.interfaces.MissionDetailDataResponse;
import com.techedge.mp.core.business.interfaces.PartnerActionUrlResponse;
import com.techedge.mp.core.business.interfaces.PartnerListInfoDataResponse;
import com.techedge.mp.core.business.interfaces.PaymentV2Response;
import com.techedge.mp.core.business.interfaces.RedemptionAwardResponse;
import com.techedge.mp.core.business.interfaces.RetrieveTermsOfServiceData;
import com.techedge.mp.core.business.interfaces.SocialAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.VirtualizationAttemptsLeftActionResponse;
import com.techedge.mp.core.business.interfaces.user.PersonalDataBusiness;
import com.techedge.mp.core.business.interfaces.user.User;

@Remote
public interface UserV2ServiceRemote {

    public AuthenticationResponse authentication(String username, String passwordHash, String passwordEncrypted, String requestId, String deviceId, String deviceName,
            String deviceToken);

    public String updatePassword(String ticketId, String requestId, String oldPassword, String newPassword);

    public String updateUsersData(String ticketId, String requestId, HashMap<String, Object> usersData);

    public String notificationVirtualization(String ticketId, String requestId);

    public VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftAction(String ticketId, String requestId, String action);

    public PaymentV2Response insertPaymentMethod(String ticketId, String requestId, String paymentMethodType, String newPin);

    public String updateUserPaymentData(String transactionType, String transactionResult, String shopTransactionID, String bankTransactionID, String authorizationCode,
            String currency, String amount, String country, String buyerName, String buyerEmail, String errorCode, String errorDescription, String alertCode,
            String alertDescription, String TransactionKey, String token, String tokenExpiryMonth, String tokenExpiryYear, String cardBin, String TDLevel, String pan, 
            String brand, String hash);

    public LandingDataResponse getLandingData(String ticketId, String requestId, String sectionID);

    public RetrieveTermsOfServiceData retrieveTermsOfService(String ticketId, String requestId, Boolean isOptional);

    public RetrieveTermsOfServiceData retrieveTermsOfServiceByGroup(String ticketId, String requestId, String group);

    public String createUser(String ticketId, String requestId, User user, Long loyaltyCardId);

    public String skipVirtualization(String ticketId, String requestId);

    public LoadVoucherEventCampaignResponse loadVoucherEventCampaign(String ticketId, String requestId, String voucherCode);

    public GetPromoPopupDataResponse getPromoPopupData(String ticketId, String requestId, String sectionID);

	public CreateUserGuestResult createUserGuest(String ticketID, String requestID, String deviceId, String captchaCode);
	
	public SocialAuthenticationResponse socialAuthentication(String accessToken,String socialProvider, String deviceId, String deviceName, String deviceToken, String string);
	
	public ExternalProviderAuthenticationResponse externalProviderAuthentication(String username,String password,String accessToken,String socialProvider, String deviceId, 
	        String deviceName, String deviceToken, String string);
    
    public AuthenticationResponse refreshUserData(String ticketId, String loyaltySessionId, String requestId, String deviceId, String deviceName);

    public String createSocialUser(String accessToken, String socialProvider, String ticketId, String requestId, User user, Long loyaltyCardId);
    
    public PartnerListInfoDataResponse getPartnerListInfo(String requestId, String ticketId);
    
    public GetHomePartnerListResult getHomePartnerListInfo(String requestId, String ticketId);

    public CategoryEarnDataDetailResponse getCategoryDetailList(String requestId, String ticketId);

    public BrandDataDetailResponse getBrandDetailList(String requestId, String ticketId);

    public AwardDetailDataResponse getAwardList(String requestId, String ticketId);

    public RedemptionAwardResponse getRedemptionAward(String ticketId, String requestId, String awardId, String cardPartner, String pin);

    public PartnerActionUrlResponse getPartnerActionUrl(String requestId, String ticketId, String partnerId);
    
    public MissionDetailDataResponse getMissionList(String requestId, String ticketId);
    
    public String addPlateNumber(String requestId, String ticketId, String plateNumber, String description);
    
    public String removePlateNumber(String requestId, String ticketId, Long id);
    
    public String setDefaultPlateNumber(String requestId, String ticketId, Long id);
    
    public GetReceiptDataResponse getReceiptResponse(String requestId, String ticketId,String type, String transactionId);
    
    public AuthenticationBusinessResponse authenticationBusiness(String email, String passwordHash, String requestId, String deviceId, String deviceName, String source);
    
    public String createUserBusiness(String ticketId, String requestId, User user);
    
    public String updateBusinessData(String ticketId, String requestId, PersonalDataBusiness personalDataBusiness);
    
    public RetrieveTermsOfServiceData retrieveTermsOfServiceBusiness(String ticketId, String requestId, Boolean isOptional);
    
    public AuthenticationBusinessResponse refreshUserDataBusiness(String ticketId, String requestId);
    
    public String recoverUsernameBusiness(String ticketId, String requestId, String fiscalcode);
    
    public String rescuePasswordBusiness(String ticketId, String requestId, String i_mail);
    
    public String requestResetPin(String ticketId, String requestId);
    
    public String cloneUserPaymentMethod(String ticketId, String requestId, String encodedPin);
    
    public ExternalAuthenticationResponse externalAuthentication(String accessToken, String provider, String requestId, String deviceId, String deviceName, String deviceToken);
    
    public String setActiveDpan(String requestID, String ticketID, String dpan, String status);
}
