package com.techedge.mp.core.business;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.CreateMulticardRefuelResponse;
import com.techedge.mp.core.business.interfaces.CreateRefuelResponse;

@Remote
public interface TransactionV2ServiceRemote {

    public CreateRefuelResponse createRefuel(String ticketID, String requestID, String encodedPin, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Long paymentMethodId, String paymentMethodType, String outOfRange, String refuelMode);

    public CreateRefuelResponse createRefuelBusiness(String ticketID, String requestID, String encodedPin, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Long paymentMethodId, String paymentMethodType, String outOfRange, String refuelMode);
    
    public CreateMulticardRefuelResponse createMulticardRefuel(String ticketID, String requestID, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Long paymentMethodId, String paymentCryptogram, String outOfRange, String refuelMode);

}
