package com.techedge.mp.core.business;

import javax.ejb.Local;

import com.techedge.mp.core.business.interfaces.Survey;
import com.techedge.mp.core.business.interfaces.SurveyAnswers;

@Local
public interface SurveyServiceLocal {

    public Survey surveyGet(String ticketId, String requestId, String code);
    
    public String surveySubmit(String ticketId, String requestId, String code, String key, SurveyAnswers answers);
}
