package com.techedge.mp.core.business;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.GetStationListResponse;
import com.techedge.mp.core.business.interfaces.GetStationResponse;
import com.techedge.mp.core.business.interfaces.postpaid.GetSourceDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCreateShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidEnableShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetPendingTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRejectShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidReverseShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.RetrieveTransactionHistoryResponse;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShopRefuelDetail;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShoppingCartDetail;

@Remote
public interface PostPaidTransactionServiceRemote {

    public PostPaidCreateShopTransactionResponse createShopTransaction(String requestID, String source, // "CASH-REGISTER"
            String sourceID, String sourceNumber, String stationID, String srcTransactionID, Double amount, String productType, // OIL
                                                                                                                                // NON_OIL
                                                                                                                                // MIXED
            // String currency,
            List<ShoppingCartDetail> shoppingCartList, List<ShopRefuelDetail> shopRefuelDetail);

    public PostPaidEnableShopTransactionResponse enableShopTransaction(String requestID, String mpTransactionID);

    public PostPaidApproveShopTransactionResponse approveShopTransaction(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentMethodType,
            String encodedPin, Boolean useVoucher);

    public PostPaidRejectShopTransactionResponse rejectShopTransaction(String requestID, String ticketID, String mpTransactionID);

    public PostPaidGetShopTransactionResponse getShopTransaction(String requestID, String mpTransactionID);

    public PostPaidGetTransactionDetailResponse getTransactionDetail(String requestID, String ticketID, String mpTransactionID);

    public PostPaidReverseShopTransactionResponse reverseShopTransaction(String requestID, String mpTransactionID);

    public GetSourceDetailResponse getSourceDetail(String requestID, String ticketID, String codeType, String sourceID, String beaconCode, Double userPositionLatitude,
            Double userPositionLongitude);

    public GetStationResponse getStation(String requestID, String ticketID, String codeType, String beaconCode, Double userPositionLatitude, Double userPositionLongitude);

    public GetStationListResponse getStationList(String requestID, String ticketID, Double userPositionLatitude, Double userPositionLongitude);

    public RetrieveTransactionHistoryResponse retrieveTransactionHistory(String requestID, String ticketID, Date startDate, Date endDate, int itemsLimit, int pageOffset);

    public PostPaidGetPendingTransactionResponse retrievePoPPendingTransaction(String requestID, String ticketID, String loyaltySessionID);

    public String archiveTransactions();

    public String confirmTransaction(String requestID, String ticketID, String transactionID);
}
