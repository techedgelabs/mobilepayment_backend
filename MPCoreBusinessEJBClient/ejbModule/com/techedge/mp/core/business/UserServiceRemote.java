package com.techedge.mp.core.business;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.AuthenticationResponse;
import com.techedge.mp.core.business.interfaces.CheckAvailabilityAmountData;
import com.techedge.mp.core.business.interfaces.GetActiveVouchersData;
import com.techedge.mp.core.business.interfaces.GetAvailableLoyaltyCardsData;
import com.techedge.mp.core.business.interfaces.MulticardPaymentResponse;
import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;
import com.techedge.mp.core.business.interfaces.PaymentResponse;
import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.core.business.interfaces.RetrieveCitiesData;
import com.techedge.mp.core.business.interfaces.RetrieveDocumentData;
import com.techedge.mp.core.business.interfaces.RetrieveTermsOfServiceData;
import com.techedge.mp.core.business.interfaces.RetrieveUserDeviceData;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.ValidatePaymentMethodResponse;
import com.techedge.mp.core.business.interfaces.loyalty.InfoRedemptionResponse;
import com.techedge.mp.core.business.interfaces.loyalty.RedemptionResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserUpdatePinResponse;

@Remote
public interface UserServiceRemote {

    public String createSystemUser();

    public String createUser(String ticketId, String requestId, User user, Long loyaltyCardId);

    public String updateUser(String ticketId, String requestId, User user);

    public AuthenticationResponse authentication(String username, String password, String requestId, String deviceId, String deviceName, String deviceToken);

    public String logout(String ticketId, String requestId);

    public String validateField(String ticketId, String requestId, String deviceId, String verificationType, String verificationField, String verificationCode);

    public ValidatePaymentMethodResponse validatePaymentMethod(String ticketId, String requestId, Long cardId, String cardType, Double verificationAmount);

    public String updatePassword(String ticketId, String requestId, String oldPassword, String newPassword);

    public UserUpdatePinResponse updatePin(String ticketId, String requestId, Long cardId, String cardType, String oldPin, String newPin);

    public PaymentResponse insertPaymentMethod(String ticketId, String requestId, String paymentMethodType, String newPin);

    public String removePaymentMethod(String ticketId, String requestId, Long paymentMethodId, String paymentMethodType);

    public String setDefaultPaymentMethod(String ticketId, String requestId, Long paymentMethodId, String paymentMethodType);

    public PaymentInfoResponse retrievePaymentData(String ticketId, String requestId, Long cardId, String cardType);

    public String updateUserPaymentData(String transactionType, String transactionResult, String shopTransactionID, String bankTransactionID, String authorizationCode,
            String currency, String amount, String country, String buyerName, String buyerEmail, String errorCode, String errorDescription, String alertCode,
            String alertDescription, String TransactionKey, String token, String tokenExpiryMonth, String tokenExpiryYear, String cardBin, String TDLevel);

    public String checkAuthorization(String ticketID, Integer operationType);

    public RetrieveCitiesData retrieveCities(String ticketID, String requestID, String searchKey);

    public String checkTransactionAuthorization(Double amount, String shopTransactionID, String valuta, String token, String operation);

    public String updateAvailableCap(Double amount, String shopTransactionID, String valuta, String token);

    public String updateEffectiveCap(Double amount, String shopTransactionID);

    public String refundAvailableCap(String shopTransactionID);

    public String rescuePassword(String ticketId, String requestId, String i_mail);

    public String recoverUsername(String ticketId, String requestId, String fiscalcode);

    public RetrieveTermsOfServiceData retrieveTermsOfService(String ticketId, String requestId, Boolean isOptional);

    public RetrieveDocumentData retrieveDocument(String ticketId, String requestId, String documentID);

    public String loadVoucher(String ticketId, String requestId, String voucherCode);

    public String loadPromoVoucher(String voucherCode, Long userId, String verificationValue);

    public GetActiveVouchersData getActiveVouchers(String ticketId, String requestId, Boolean refresh, String loyaltySessionID);

    public String removeVoucher(String ticketId, String requestId, String voucherCode);

    public String setDefaultLoyaltyCard(String ticketId, String requestId, String panCode, String eanCode);

    public GetAvailableLoyaltyCardsData getAvailableLoyaltyCards(String ticketId, String requestId, String fiscalcode);

    public String setUseVoucher(String ticketId, String requestId, boolean useVoucher);

    public String resetPin(String ticketId, String requestId, String password, String newPin);

    public String skipPaymentMethodConfiguration(String ticketId, String requestId);

    public PrefixNumberResult retrieveAllPrefixNumber(String ticketId, String requestId);

    public String cancelMobilePhoneUpdate(String ticketId, String requestId, Long id);

    public String updateMobilePhone(String ticketId, String requestId, String oldNumber, String newNumber);

    public String userResendValidation(String ticketId, String requestId, String id, String validitationType);

    public CheckAvailabilityAmountData userCheckAvailabilityAmount(String ticketID, String requestID, Double amount, String loyaltySessionID);

    public String updateTermsOfService(String ticketID, String requestID, List<TermsOfService> termsOfServiceList);

    public String updateSmsLog(String correlationID, String destinationAddress, String message, String mtMessageID, Integer statusCode, Integer responseCode, Integer reasonCode,
            String responseMessage, Date operatorTimestamp, Date providerTimestamp, String operator, boolean firstUpdate);

    public String checkLoyaltySession(String sessionId, String fiscalCode);

    public InfoRedemptionResponse infoRedemption(String ticketId, String requestId);

    public RedemptionResponse redemption(String ticketId, String requestId, Integer redemptionCode, String pin);

    public RetrieveUserDeviceData retrieveActiveDevice(String ticketId, String requestId);

    public String removeActiveUserDevice(String ticketId, String requestId, String userDeviceID);

    public MulticardPaymentResponse insertMulticardPaymentMethod(String ticketId, String requestId);
}
