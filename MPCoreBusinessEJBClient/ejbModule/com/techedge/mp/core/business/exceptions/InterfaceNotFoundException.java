package com.techedge.mp.core.business.exceptions;

public class InterfaceNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8830944981038276518L;
	
	private String jndiString;
	
	public InterfaceNotFoundException(String jndiString) {
		
		this.jndiString = jndiString;
	}
	
	public String getJndiString()
	{
		return jndiString;
	}
	
	public String toString()
	{
		return this.getClass().toString() + ": " + this.jndiString;
	}
}
