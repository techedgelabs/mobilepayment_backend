package com.techedge.mp.core.business.exceptions;

public class ParameterNotFoundException extends Exception {

	private static final long serialVersionUID = -2672406584745769637L;
	private String parameterName;
	
	public ParameterNotFoundException() {}
	
	public ParameterNotFoundException(String message, String parameterName) {
		
		super(message);
		if (parameterName != null) {
		    this.parameterName = parameterName;
		}
	}
	
	public String getParameterName() {
        return parameterName;
    }
}
