package com.techedge.mp.core.business;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.ErrorLevel;

@Remote
public interface LoggerServiceRemote {

	public void setLogLevel(ErrorLevel level);
	public void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message);
}
