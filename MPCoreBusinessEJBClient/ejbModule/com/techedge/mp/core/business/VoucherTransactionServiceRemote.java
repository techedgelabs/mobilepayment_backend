package com.techedge.mp.core.business;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.voucher.CreateApplePayVoucherTransactionResult;
import com.techedge.mp.core.business.interfaces.voucher.CreateVoucherTransactionResult;
import com.techedge.mp.core.business.interfaces.voucher.RetrieveVoucherTransactionDataResponse;

@Remote
public interface VoucherTransactionServiceRemote {

    public CreateVoucherTransactionResult createVoucherTransaction(String requestID, String ticketID, String pin, Double amount, Long paymentMethodID, String paymentMethodType);
    
    public CreateApplePayVoucherTransactionResult createApplePayVoucherTransaction(String requestID, String ticketID, Double amount, String applePayPKPaymentToken);

    public RetrieveVoucherTransactionDataResponse retrieveVoucherTransactionDetail(String requestId, String ticketID, String voucherTransactionID);
}
