package com.techedge.mp.core.business;

import java.util.Date;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.CreateApplePayRefuelResponse;
import com.techedge.mp.core.business.interfaces.CreateMulticardRefuelResponse;
import com.techedge.mp.core.business.interfaces.CreateRefuelResponse;
import com.techedge.mp.core.business.interfaces.PaymentRefuelDetailResponse;
import com.techedge.mp.core.business.interfaces.PaymentRefuelHistoryResponse;
import com.techedge.mp.core.business.interfaces.PendingRefuelResponse;
import com.techedge.mp.core.business.interfaces.PendingTransactionRefuelResponse;
import com.techedge.mp.core.business.interfaces.RetrieveTransactionEventsData;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionCancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.TransactionConsumeVoucherPreAuthResponse;
import com.techedge.mp.core.business.interfaces.TransactionExistsResponse;
import com.techedge.mp.core.business.interfaces.TransactionPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.TransactionUseVoucherResponse;

@Remote
public interface TransactionServiceRemote {

    public CreateRefuelResponse createRefuel(String ticketID, String requestID, String encodedPin, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Double amountVoucher, Boolean useVoucher, Long cardId, String cardType, String outOfRange, String refuelMode);

    public Boolean checkUndoOperation(String transactionID);

    public String persistUndoRefuelStatus(String ticketID, String requestID, String refuelID);

    public String confirmRefuel(String ticketID, String requestID, String refuelID);

    public PendingRefuelResponse retrievePendingRefuel(String requestID, String ticketID);

    public PaymentRefuelDetailResponse retrieveRefuelPaymentDetail(String requestID, String ticketID, String refuelID);

    public String persistStartRefuelReceivedStatus(String requestID, String transactionID, String source);

    public String persistEndRefuelReceivedStatus(String requestID, String transactionID, Double amount, Double fuelQuantity, String fuelType, String productDescription,
            String productID, String timestampEndRefuel, Double unitPrice, String source);

    public String persistTransactionStatusRequestKoStatus(String requestID, String transactionID, String subStatus, String subStatusDescription);

    public String persistNewStatus(String statusCode, String messageCode, String transactionID);

    public String persistPumpAvailabilityStatus(String statusCode, String messageCode, String transactionID, String requestID);

    public String persistPumpEnableStatus(String statusCode, String messageCode, String transactionID, String requestID);

    public String persistPumpGenericFaultStatus(String transactionID, String requestID);

    public String setGFGNotification(String transactionID, boolean flag_value, String electronicInvoiceID);

    public String persistPaymentAuthorizationStatus(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId,
            String authorizationCode);

    public String persistPaymentDeletionStatus(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId);

    public String persistPaymentCompletionStatus(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId,
            Double effectiveAmount);

    public TransactionExistsResponse transactionExists(String transactionID);

    public String refreshStationInfo(String ticketID, String stationID, String address, String city, String country, String province, Double latitude, Double longitude);

    public PaymentRefuelHistoryResponse retrieveRefuelPaymentHistory(String requestID, String ticketID, Integer pageNumber, Integer itemForPage, Date startDate, Date enddate,
            Boolean detail);
    
    public RetrieveTransactionEventsData retrieveEventList(String requestID, String transactionID);

    public String retrieveStationId(String beaconCode);
    
    public int updateReconciliationAttempts(String transactionID, Integer attemptsLeft);

    public String retrieveStationIdByCoords(Double latitude, Double longitude);
    
    public PendingTransactionRefuelResponse finalizePendingRefuel();
    //	public String persistPaymentRefundCheckCartaStatus(String statusCode, String subStatusCode, String messageCode, String transactionID, String bankTransactionId, Double effectiveAmount);
    //	public String persistPaymentRefundReconciliationStatus(String statusCode, String subStatusCode, String messageCode, String transactionID, String bankTransactionId, Double effectiveAmount);
    
    public Transaction getTransactionDetail(String requestId, String mpTransactionID);
    
    public TransactionUseVoucherResponse transactionUseVoucher(String mpTransactionID, Double amount);
    
    public TransactionPreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucher(String transactionID, Double amount);
    
    public TransactionCancelPreAuthorizationConsumeVoucherResponse cancelPreAuthorizationConsumeVoucher(String transactionID);
    
    public TransactionConsumeVoucherPreAuthResponse consumeVoucherPreAuth(String transactionID, Double amount);
    
    public TransactionPreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucherNoTransaction(Long userID, String mpTransactionID, String stationID, Double amount);
    
    public CreateApplePayRefuelResponse createApplePayRefuel(String ticketID, String requestID, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Double amountVoucher, Boolean useVoucher, String applePayPKPaymentToken, String outOfRange, String refuelMode);
    
    //public String setGFGElectronicInvoiceID(String transactionID, String value);
    
    public String persistTransactionOperation(String requestID, String transactionID, String code, String message, String status, String remoteTransactionId, String operationType, String operationId, Long requestTimestamp, Integer amount);
}
