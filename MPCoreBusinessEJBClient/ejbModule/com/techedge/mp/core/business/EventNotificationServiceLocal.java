package com.techedge.mp.core.business;

import java.util.ArrayList;
import java.util.Date;

import javax.ejb.Local;

import com.techedge.mp.core.business.interfaces.CheckTransactionResponse;
import com.techedge.mp.core.business.interfaces.DpanStatus;
import com.techedge.mp.core.business.interfaces.MulticardStatusNotificationResponse;
import com.techedge.mp.core.business.interfaces.loyalty.EventNotification;
import com.techedge.mp.core.business.interfaces.pushnotification.NotificationResponse;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;

@Local
public interface EventNotificationServiceLocal {

    public EventNotification getTransactionDetail(String ticketId, String requestId, Long eventNotificationId);
    
    public String updatePushNotication(Long pushNotificationID, String publishMessageID, String endpoint, String title, String message, Date sendingTimestamp, PushNotificationSourceType source, 
            PushNotificationContentType contentType, PushNotificationStatusType statusCode, String statusMessage, Long userID);
    
    public NotificationResponse retrieveNotification(String ticketId, String requestId, Long notificationId);

    public String setEventNotification(EventNotification eventNotification);
    
    public CheckTransactionResponse checkTransaction(String operationID, Long requestTimestamp, String stationID, String pumpNumber);
    
    public MulticardStatusNotificationResponse multicardStatusNotification(String operationID, Long requestTimestamp, String userID, ArrayList<DpanStatus> dpanStatus);
    
    public MulticardStatusNotificationResponse multicardRefuelingStatusNotification(String operationID, Long requestTimestamp, String userID, String serverSerialNumber, ArrayList<DpanStatus> dpanStatus);
}
