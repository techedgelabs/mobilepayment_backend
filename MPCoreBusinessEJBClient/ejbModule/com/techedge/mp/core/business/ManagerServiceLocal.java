package com.techedge.mp.core.business;

import java.util.Date;

import javax.ejb.Local;

import com.techedge.mp.core.business.interfaces.ManagerAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ManagerRetrieveTransactionsResponse;
import com.techedge.mp.core.business.interfaces.ManagerRetrieveVouchersResponse;
import com.techedge.mp.core.business.interfaces.ManagerStationDetailsResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;

@Local
public interface ManagerServiceLocal {

  public ManagerAuthenticationResponse authentication(String username, String password, String requestId, String deviceId, String deviceName);

  public String logout(String ticketId, String requestId);

  public String updatePassword(String ticketId, String requestId, String oldPassword, String newPassword);

  public String rescuePassword(String ticketId, String requestId, String email);

  public ManagerStationDetailsResponse stationDetails(String ticketId, String requestId, String stationId);
  
  public ManagerRetrieveTransactionsResponse retrieveTransactions(String ticketId, String requestId, String stationID, Integer pumpMaxTransactions);
  
  public ManagerRetrieveVouchersResponse retrieveVouchers(String ticketId, String requestId, String stationID, Date startDate, Date endDate);
  
  public PostPaidGetTransactionDetailResponse getTransactionDetail(String requestID, String ticketID, String mpTransactionID);

}
