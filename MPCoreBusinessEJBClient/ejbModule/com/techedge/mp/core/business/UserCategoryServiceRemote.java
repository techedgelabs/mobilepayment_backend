package com.techedge.mp.core.business;

import java.util.List;

import javax.ejb.Remote;

@Remote
public interface UserCategoryServiceRemote {

    public void refreshData();

    public Boolean isUserTypeInUserCategory(Integer userType, String userCategory);

    public List<Integer> getUserTypeByCategory(String userCategory);

}
